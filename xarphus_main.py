#!/usr/bin/env python
#-*- coding:utf-8 -*-

# nochmal von vorn

# This is only needed for Python v2 but is harmless for Python v3.
import sip
#   We have to load sip before loaing PyQt4
try:
    sip.setapi('QVariant', 2)
except ValueError:
    sip.setapi('QVariant', 1)

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

FILE_NAME = "xarphus_main.py"

#   Import all Built-in Functions
from sys import argv, exc_info, exit
from time import sleep, time
from traceback import format_exc

import argparse
from glob import glob
from os import path, chdir, error, remove, unlink
from tempfile import NamedTemporaryFile
#    Import all PyQt bindings
from PyQt4.QtGui import QApplication, QPixmap, qApp, QSplashScreen, QProgressBar, QStyleFactory, \
    QMessageBox, QWizard, QLabel, QIcon, QWidget, QPushButton
from PyQt4.QtCore import Qt, QFile, QTextStream, QTranslator, QString, QCoreApplication, QEvent, \
    pyqtSignal, QThread, QObject

from PyQt4 import QtCore
from PyQt4.uic import loadUi

#   Import all forms
try:
    from xarphus.frm_mdi import MDI_Window
    from xarphus.frm_error_wizard import MainMissingProfileWizard
except ImportError:
    from frm_mdi import MDI_Window
    from frm_error_wizard import MainMissingProfileWizard
#   Import core codes
try:

    from xarphus.core.config import ConfigurationSaverDict
    from xarphus.core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile
    from xarphus.core.manage_logging import configure_logging
    from xarphus.core.manage_time import get_time_now
    from xarphus.core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, is_folder_exists, is_folder_empty, create_folder
    from xarphus.core.manage_file import find_files, delete_file
    from xarphus.info import info_app
    from xarphus.core.set_settings import to_set_general_settings
    from xarphus.core.manage_qt import custom_message_box
    from xarphus.frm_profiles_list import ProfileList_Window
    from xarphus.languages.german import Language
    from xarphus.collection_paths_image import ImagePathnCollection
except ImportError:
    from core.config import ConfigurationSaverDict
    from core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile
    from core.manage_logging import configure_logging
    from core.manage_time import get_time_now
    from core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, is_folder_exists, is_folder_empty, create_folder
    from core.manage_file import find_files, delete_file
    from xarphus.info import info_app
    from core.set_settings import to_set_general_settings
    from core.manage_qt import custom_message_box
    from frm_profiles_list import ProfileList_Window
    from languages.german import Language
    from collection_paths_image import ImagePathnCollection

BASE_PATH = path.dirname(path.abspath(__file__))
#BASE_PATH = path.dirname(path.abspath(__file__))
#print "BASE_PATH na los", BASE_PATH
##BASE_PATH = os.path.dirname(os.path.abspath(FILE_NAME))
##SETTING_FOLDER_PATH = os.path.join(BASE_PATH, 'settings')
##CONFIG_PATH = os.path.join(BASE_PATH, 'settings', 'config.ini')
##ZIP_FILE_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
##LINCENCE_PATH = os.path.join(BASE_PATH, 'LICENCE_GPL_v2.txt')
##LOG_FOLDER_PATH = os.path.join(BASE_PATH, 'log')

                 #default_ini_file = None,
                 #configuration_saver_dict = None,
                 #about_app = None,
                 #image_path_collection = None,

class App(QApplication):

    def __init__(self,
                 parent = None,
                 **kwargs):
        QApplication.__init__(self, parent)

        self._kwargs = kwargs
        #self.default_ini_file = self._kwargs.get('default_ini_file')
        #self.configuration_saver_dict = self._kwargs.get('configuration_saver_dict')
        #self.about_app = self._kwargs.get('about_app')
        #self.image_path_collection = self._kwargs.get('image_path_collection')

        self.mdi_widget = None

        #parser = argparse.ArgumentParser()
        #parser.add_argument('-i','--input', help='Input file name',required=True)
        #args = parser.parse_args()
        #print "Input file: %s" % args.input


        self.BASE_PATH = path.dirname(path.abspath(__file__))
        self.PROFILES_FOLDER_PATH = path.join(self.BASE_PATH, 'profiles')
        self.SETTING_FOLDER_PATH = path.join(self.BASE_PATH, 'settings')
        self.CONFIG_PATH = path.join(self.BASE_PATH, 'settings', 'config.ini')
        self.ZIP_FILE_PATH = path.join(self.BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
        self.LOG_FOLDER_PATH = path.join(self.BASE_PATH, 'log')

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.create_splash_screen(self._kwargs)

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def get_execute_file(self):
        EXECUTE_FILE_PATH = ""
        BASE_PATH = path.join(self.BASE_PATH)
        chdir(BASE_PATH)

        for exe_file in glob("*.exe"):
            EXECUTE_FILE_PATH = BASE_PATH = path.join(self.BASE_PATH, exe_file)

        self.configuration_saver_dict.dict_set_general_settings["PathToExecuteFile"] = EXECUTE_FILE_PATH



    def load_translator_file(self):
        '''
           SUMMARY
           =======
           This method is used to load and install the QTranslator()-object.
           Returns TRUE if the file is loaded successfully; otherwise returns FALSE.
           That means it doesn't matter if the translate file could be loaded,
           the program continues without error. This method will display
           the original language on the screen: its English. This is the reason why
           no exception is handled.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of QTranslator()")

        FILE_EXTENSION = ".qm"

        self.translator = QTranslator()

        logger.info("The instance of the class of QTranslator() is created successfully")

        path = ":/translators/" + self.configuration_saver_dict.dict_set_general_settings["language"]+ FILE_EXTENSION

        logger.info("Installing the QTranslator()-object")

        self.installTranslator(self.translator)

        logger.info("The translator is installed successfully")

        logger.info("Loading translate file for translation")

        self.translator.load(path)

        logger.info("Translate file is loaded successfully")

        return

    def load_stylesheet(self):
        '''
           SUMMARY
           =======
           This method is used to load and set the Style Sheet.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Reading and setting the style sheet")

        CSS_FILE_EXTENSION = "qss"

        #   First: Get the current css stylesheet and setup stylesheet
        #   Here its takes a look into the dictionary and checks
        #   if the value tied to 'key' (SchemeColor) evaluates to not True,
        #   do nothing -just pass", because the value is empty.

        #   The value tied to 'key' (SchemeColor) evaluates to True.
        scheme_color = self.configuration_saver_dict.dict_set_general_settings["SchemeColor"].lower()

        #   Create a path to the css file completly
        css_path = ":/{scheme_color}/{scheme_color}.{CSS_FILE_EXTENSION}".format(
            scheme_color = scheme_color,
            css_file_extension = CSS_FILE_EXTENSION)

        self.image_path_collection = ImagePathnCollection(theme = path.splitext(path.basename(css_path))[0])

        #   Creating a constructor. It is an I/O device for reading and writing text.
        #   The variable as a file name (css_path) is passed in the constructor
        fd = QtCore.QFile(css_path)
        fd.open(QtCore.QIODevice.ReadOnly | QtCore.QFile.Text)

        '''
            For reading and writting the data it is using QTextStream.
        '''
        style = QtCore.QTextStream(fd).readAll()
        fd.close()

        '''
            Setting the style sheet.
        '''
        self.setStyleSheet(style)

        return

    def load_style(self):
        '''
           SUMMARY
           =======
           This method is used to load and set the style of QStyleFactory()-object.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Load and set the GUI-style")

        style_type = None

        #  Here its checks if the value tied to key (GUIStyle) evaluates not to True, that means its empty,
        #   than do nothing here. Set the standard style.
        if  not self.configuration_saver_dict.dict_set_general_settings["GUIStyle"] or \
                self.configuration_saver_dict.dict_set_general_settings["GUIStyle"].lower() == "automatically":
            style_type = qApp.style().objectName()
        else:
            #   The value tied to key (GUIStyle) evaluates to True - isn't empty
            style_type = self.configuration_saver_dict.dict_set_general_settings["GUIStyle"]

        #   Finally its styles the GUI.
        self.setStyle(QStyleFactory.create(style_type))

        return

    def create_missing_profile_wizard(self):
        '''
           SUMMARY
           =======
            This method is used to create an Wizard-instance. For example the user
            starts this programs at the first time or the profile folder, in which all settings
            and stuffs are saved, is empy or damaged, the user will get a Wizard-window - is just
            like a assistant-window.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of MainMissingProfileWizard()")

        '''
            Set the buttons in a sequence and safe it in a list
        '''
        self.main_missing_profile_wizard = MainMissingProfileWizard(**self._kwargs)

        self.main_missing_profile_wizard.show()

        return

    def create_mdi_window(self):
        '''
           SUMMARY
           =======
           This method is used to create an MDI-instance and show this as a window.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of MDI)")

        try:
            logger.info("Trying to an instance of the class of MDI_Window()")
            window = MDI_Window(self._kwargs.get('configuration_saver_dict'),
                                self._kwargs.get('about_app'),
                                self._kwargs.get('image_path_collection'),
                                self._kwargs.get('default_ini_file'),
                                app = self)
            window.showMaximized()

            self.mdi_widget = window

        except Exception:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def create_error_messagebox(self,
                                widget,
                                title,
                                text):
        '''
           SUMMARY
           =======
           This method is used to create a message box it is shows
           the error text.

           PARAMETERS
           ==========
           :widget (class): QMessageBox.critical needs to pass a QObject/QWidget object.

            :title (str):   Message box main text.

            :text (str):    Message box informative text.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create an error message box.")

        QMessageBox.critical(widget, title, text)

        return


    def open_ini_file(self, splash_screen_obj):

        with open(self.CONFIG_PATH, 'r') as ini_file:
            try:
                '''
                Ok, the ini file already exists. Next stept: let's check wheter the ini file is empty.
                '''
                result_file_size = file_is_empty(self.CONFIG_PATH)
                if result_file_size:
                    '''
                    The boolean True means, that ini file is empty
                    '''
                    splash_screen_obj.finish(None)
                    self.create_error_messagebox(
                        widget = self.mdi_widget,
                        title = self.tr("Error"),
                        text = self.tr("The INI-file appears to have "
                                         "been damaged or modified. You "
                                         "have the ability record a "
                                         "secure INI-file or create a new "
                                         "INI-file."))

                else:
                    '''
                    The ini file isn't empty.
                    '''
                    #TODO: In this case I have to validate the ini file wether all OPtIONS and SECTIONS are fine
                    get_configuration(self.CONFIG_PATH, self.configuration_saver_dict, self.default_ini_file)

                    self.load_translator_file()

                    self.load_stylesheet()
                    self.load_style()

                    '''
                    Let us finish the splash scrren, remove the event filter and start the MDI-Form
                    '''
                    splash_screen_obj.finish(self.create_mdi_window())
                    self.remove_event_filter(splash_screen_obj)

            except:
                desired_trace = format_exc(exc_info())

    def create_profile_list_window(self, splash_object, image_path_collection):
        '''
        NOTICE:
        =======

        PARAMETERS:
        ===========
        :return     -   Its returns nothing, the statement 'return'
                        terminates a function. That makes sure that the
                        function is definitely finished.
        '''
        try:
            logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

            profile_list_form = ProfileList_Window(image_path_collection = image_path_collection, parent = self)

            #profile_list_form.close_app_signal.connect(lambda: exit() )
            #profile_list_form.close_app_signal.connect(lambda: splash_object.setVisible(True) )

            logger.info("Connecting all signales of PersonProfile_Window()-class")

            logger.info("Show the new subwindow maximized")

            profile_list_form.show()#.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_messagebox(
                widget = self.mdi_widget,
                title = self.tr("Critical error"),
                text = desired_trace)

    def create_splash_screen(self, kwargs):

        logger.info("Create splash screen")

        #    We have to save the root path  of this running program for later at runtime.
        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"] = self.BASE_PATH


        to_set_general_settings(self.CONFIG_PATH, self.SETTING_FOLDER_PATH, self.ZIP_FILE_PATH,
                                             self._kwargs.get('configuration_saver_dict'))



        # Create and display the splash screen
        SplashPic = ":/img_512x512_default/default/img_512x512/splash_loading.png"
        splash_pix = QPixmap(SplashPic)
        splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)

        splash.installEventFilter(self)

        copy_right_content = "{product_copyright_characters} {product_copyright} 2014-2016 {product_by} {product_author}".format(product_copyright_characters = self._kwargs.get('about_app').dict_info["product_copyright_characters"],
                                                                                       product_copyright = self._kwargs.get('about_app').dict_info["product_copyright"],
                                                                                       product_by = self._kwargs.get('about_app').dict_info["product_by"],
                                                                                       product_author = self._kwargs.get('about_app').dict_info["product_author"],
                                                                                       exe_name =self._kwargs.get('about_app').dict_info["exe_name"],
                                                                                       product_version = self._kwargs.get('about_app').dict_info["product_version"]
                                                                                       )

        version_content = "{exe_name} Version ({product_version})".format(exe_name = self._kwargs.get('about_app').dict_info["exe_name"],
                                                                          product_version = self._kwargs.get('about_app').dict_info["product_version"]
                                                                                       )


        copy_right_label = QLabel(splash)
        copy_right_label.setAlignment(Qt.AlignRight)
        copy_right_label.setText(copy_right_content)
        copy_right_label.setStyleSheet('QLabel{color: #778899;}')


        version_label = QLabel(splash)
        version_label.setAlignment(Qt.AlignRight)
        version_label.setText(version_content)
        version_label.setStyleSheet('QLabel{color: #778899;}')

        # add a Label to the splash screen
        #copy_right_label.setGeometry(splash.width()/3.1, 8.55*splash.height()/10,
        #                            9.3*splash.width()/9.9, splash.height()/18)
        # First: Left, right
        # Second: up, down
        version_label.move(375, 10)
        copy_right_label.move(330, 280)

        # add a progressbar to the splashscreen
        progressBar = QProgressBar(splash)
        progressBar.setAlignment(Qt.AlignCenter)
        #                             # at first, the width of progressbar
        #                                                 # here the height of progressbar
        progressBar.setGeometry(splash.width()/56, 9.3*splash.height()/10,
                                    9.3*splash.width()/9.9, splash.height()/18)

        #
        splash.setMask(splash_pix.mask())
        splash.show()

        for i in range(0, 100):
            progressBar.setValue(i)
            t = time()
            while time() < t + 0.1:
                self.processEvents()

            self.processEvents()

        self.processEvents()
        # # Simulate something that takes time
        sleep(1)

        try:

          task_thread = QThread(self)
          task_thread.work = AppWorker(parent = None, **kwargs)

          task_thread.work.moveToThread(task_thread)

          # task_thread.work.show_process_text_signal.connect(lambda text:
          #   self.add_items_model(text = text))

          task_thread.work.create_missing_profile_wizard_signal.connect(self.create_missing_profile_wizard)
          task_thread.work.finish_splash_signal.connect(lambda: splash.finish(None))

          task_thread.work.create_profile_list_window_signal.connect(
            lambda:
            self.create_profile_list_window(splash_object = splash, image_path_collection = self.image_path_collection))

          task_thread.work.finish_thread_signal.connect(task_thread.quit)

           #self.finish.connect(task_thread.work.stop)

          task_thread.started.connect(task_thread.work.init_checking)

          task_thread.finished.connect(task_thread.deleteLater)

          task_thread.start()

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            #splash.finish(None)
            splash.setVisible(False)

            self.create_error_messagebox(
                widget = self.mdi_widget,
                title = self.tr("Critical error"),
                text = desired_trace)

            self.close()

    def eventFilter(self, obj, event):
        '''
            NOTICE:
            =======
            This eventFilter()-function disables the mouse click event - right and left.

            During the QSplashScreen()-class is showing for a while the user can't
            click the SplashScreen to make it disappear. Without this event filter when the
            user clicks the SplashScreen it disappears until the main window shows up.
            Thats why we disable the possibility of clicking on the splash screen.

            The eventFilter() function must return true if the event should be filtered,
            (i.e. stopped); otherwise it must return false.

            PARAMETERS:
            ===========
            :obj        -

            :event      -

            :return     -   Call Base Class Method to Continue Normal Event Processing.

        '''
        if event.type() in (QEvent.MouseButtonPress,
                            QEvent.MouseButtonDblClick):
            if event.button() == Qt.LeftButton:
                return True
            if event.button() == Qt.RightButton:
                return True

        return QApplication.eventFilter(self, obj, event)

    def remove_event_filter(self, widget):
        '''
            NOTICE:
            =======
                This method removes the installed filter event.

                We don't need this filter event anymore.

            PARAMETERS:
            ===========
                :widget     -       Here we except an given widget from
                                    where the eventFilters should be removed.

                :return     -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''

        widget.removeEventFilter(self)

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event :     By default the given event is accepted and the widget is closed. We
                        get this event when Qt receives a window close request for a top-level
                        widget from the window system. That means, first it sends the widget a QCloseEvent.
                        The widget is closed if it accepts the close event. The default implementation of
                        QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        #   No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
        #   That means, the window is not a subwindow.
        self.close()

class AppWorker(QObject):

    create_missing_profile_wizard_signal = pyqtSignal()
    create_profile_list_window_signal = pyqtSignal()
    finish_thread_signal = pyqtSignal()
    finish_splash_signal = pyqtSignal()

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)


        self._kwargs = kwargs
        self.BASE_PATH = path.dirname(path.abspath(__file__))

    def check_general_profile_folder_is_empty(self,
                                              folder_path):
        logger.info("Check if the profile folder is empty")
        try:

            folder_path_result, folder_name_result = is_folder_empty(folder_path)
            #print "folder_name_result", folder_name_result
            #print "folder_path_result", folder_path_result
            # Let us look, if the general profil folder is NOT empty
            if folder_path_result:
              print "Fine, the profile folder isn't empty"

              #print "folder_name_result", folder_name_result

              if len(folder_name_result) > 0:
                  print "More than one profile are there"
                  self.create_profile_list_window_signal.emit()
                  self.finish_splash_signal.emit()

            else:

              self.create_missing_profile_wizard_signal.emit()
              self.finish_splash_signal.emit()

              print "The general top profile folder is empty"

        except OSError as e:

          # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

    def init_checking(self):
        '''
           SUMMARY
           =======
           This method is used to initialize the check.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initializes the check.")

        #   Let us try just create a top folder named profile
        try:
            #  When no exception was raised that means we created a folder named profile, because there wasn't this one.
            # No, there isn't a folder of profiles. We have to create the wizard
            create_folder(folder_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings['BasePath'])

            #   So we have to emit the signal for showing the wizard of rcreating a profile name
            self.create_missing_profile_wizard_signal.emit()

            #   At this time we need to finish the splash scrren by calling the given emit-signal.
            self.finish_splash_signal.emit()

        except OSError as exc:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)


            if desired_trace.find("Error 183") != -1 :
                # 'Error 183' means the top folder named profile already exixts.
                # Next step we have to check if there are more subfolder(s) with name(s) of profile(s).
                self.check_general_profile_folder_is_empty(
                    folder_path = path.join(self._kwargs.get('configuration_saver_dict').dict_set_general_settings['BasePath'], 'profiles'))

        finally:
            #   It doesn't matter what is happend, we always want to finish this openend thread.
            self.finish_thread_signal.emit()

        return

def create_generally_crritcal_msg(image_path_collection):
    app = QApplication(argv )

    msg = custom_message_box(app, text = "Xarphus is currently running.",
                                  detail_text = "NOTICE:\n" \
                                  "Did you close Xarphus, but it is still running in Task Manager? There may be times, when the executable continues to run." \
                                  "\n"
                                  "In this case you have to open the Task Manager and kill the exe-file of Xarphus.",
                                  title = "Already running",
                                  icon = image_path_collection.warning_48x48_default,
                                  set_flag = True,
                                  title_icon = QIcon(image_path_collection.warning_32x32_default))

    #msg.setIcon(QMessageBox.Critical)

    setIc_OK = QIcon(image_path_collection.check_icon)

    msg.addButton(
        QPushButton(setIc_OK, " Ok"),
        msg.AcceptRole)  # An "OK" button defined with the AcceptRole
        #mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.
    msg.exec_()

    exit()

def main():

    about_app = info_app()

    image_path_collection = ImagePathnCollection()

    file_prefix = '{program_name}_runs_'.format(program_name=about_app.dict_info["product_name"]).lower()
    file_suffixc = '.{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    file_extension = '{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    #MAIN_PATH = path.dirname(path.abspath(__file__))

    '''
        NOTICE:
        =======
        In general, the application will only be started once.

        We will create a temporary file (NamedTemporaryFile) to ensure that the application
        has been started only once. We use the context manage which ensures that the
        created temporary file is actually closed and thus immediately deleted.

        First, we take a look (find_files), if there is a temp file with a special extension.
        We get back a list. If there is such a file the returned list isn't empty - we know that.
        But, when the returned list is empty that means, this program has not been started.
        So we can start this program. On the other hand, we break the procedure.
    '''

    result_glob = find_files(file_path=BASE_PATH, file_extension=file_extension)

    if not result_glob:

        with NamedTemporaryFile(prefix=file_prefix, suffix=file_suffixc, dir=BASE_PATH) as temp:

            default_ini_file = DefaultINIFile()

            configuration_saver_dict = ConfigurationSaverDict()


            convert_current_time = get_time_now()

            LOG_FILE_PATH = path.join(BASE_PATH, 'log', convert_current_time + '.log')

            configure_logging(LOG_FILE_PATH)

            logger.info("Starting the program {exe_name} (Version: ({product_version})".format(product_version=about_app.dict_info["product_version"],
                                                                                               exe_name=about_app.dict_info["exe_name"]))

            try:
                #import qdarkstyle
                logger.info("Trying to create the App()-objectName")
                QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

                    # create the application if necessary
                app = None
                if ( not QApplication.instance() ):

                    app = App(default_ini_file = default_ini_file,
                              configuration_saver_dict = configuration_saver_dict,
                              about_app = about_app,
                              image_path_collection = image_path_collection,
                              parent = argv)

                    # setup stylesheet
                    #app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))
                    app.setQuitOnLastWindowClosed(False)

                    #   execute the application if we've created it
                    exit(app.exec_())

            except Exception as ex:
                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)
    else:
        #   Well we have a problem, the application has been started!
        #   Let us delete the found file.
        return_value = delete_file(file_list = result_glob)

        if not return_value:

            create_generally_crritcal_msg(image_path_collection = image_path_collection)



if __name__ == "__main__":
    main()
