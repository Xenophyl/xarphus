from os import path as os_path
from sys import argv, path as sys_path


from distutils.core import setup
import py2exe
import glob

import shutil
import pymysql
import sqlalchemy
import sqlalchemy.util

import sqlalchemy.log
import logging

import zipfile


import requests.certs
import requests
from xarphus.info import info_app

from zipping_dist import zip_dir

BASE_PATH = os_path.dirname(os_path.abspath(__file__))
MICROSOFT_VC90_CRT_PATH = os_path.join(BASE_PATH, 'dll_for_exe', 'Microsoft.VC90.CRT')

argv.append('py2exe')
argv.append("-q")

if len(argv) == 1:
    argv.append("py2exe")

#   Also note that despite all the above, py2exe will complain that it cannot find MSVCP90.dll.
#   You must edit your setup.py to add the path to the dlls to the sys.path, e.g.
sys_path.append(MICROSOFT_VC90_CRT_PATH)
LICENCE_GPL_v1_PATH = os_path.join(BASE_PATH, 'Xarphus', 'licence', 'LICENCE_GPL_v1.txt')
PROG_Pfad = os_path.dirname(argv[0])


PROG_info = info_app()
PROG_file = 'xarphus_main.py'
PROG_icon = 'favicon.ico'
PROG_name = PROG_info.dict_info["product_name"].lower() + "_" + PROG_info.dict_info["product_version"]
PROG_exe_name = PROG_info.dict_info["product_name"]

excludes = []
includes = ["sip", "PyQt4.QtNetwork", 'PyQt4.QtCore','PyQt4.QtGui', 'PyQt4.uic', 'six']

setup(
    name=PROG_exe_name,
    version=PROG_info.dict_info["product_version"],
    description=PROG_info.dict_info["product_description"],
    author=PROG_info.dict_info["product_author"],
    company_name = PROG_info.dict_info["product_name"] + "Inc.",
    copyright='(C)',

    data_files = [
        ('imageformats', [
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qico4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qgif4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qjpeg4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qmng4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qsvg4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qtga4.dll',
        r'C:\Python27\Lib\site-packages\PyQt4\plugins\imageformats\qtiff4.dll'
        ]),
##        ('PyQt4\graphicssystems', [
##	r'C:\Python27\Lib\site-packages\PyQt4\plugins\graphicssystems\qglgraphicssystem4.dll'
##        ]),

       ('', [
	         LICENCE_GPL_v1_PATH
             #r'D:\Dan\Python\xarphus\LICENCE_GPL_v2.txt'
            ]
        ),

        ('dll_for_exe\Microsoft.VC90.CRT', [
		r'dll_for_exe\Microsoft.VC90.CRT\msvcm90.dll',
        r'dll_for_exe\Microsoft.VC90.CRT\msvcp90.dll',
        r'dll_for_exe\Microsoft.VC90.CRT\msvcr90.dll',
        r'dll_for_exe\Microsoft.VC90.CRT\Microsoft.VC90.CRT.manifest.manifest'
        ]),
        ('', [
		r'dll_for_exe\msvcr90.dll',
        r'dll_for_exe\vcredist_x86.exe'
        ]),
##	('PyQt4\designer', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\phononwidgets.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\pyqt4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\pythonplugin.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\qaxwidget.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\qdeclarativeview.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\qscintillaplugin.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\QVTKWidgetPlugin.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\qwebview.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\designer\qwt_designer_plugin5.dll'
##	]),
##	('PyQt4\iconengines', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\iconengines\qsvgicon4.dll'
##        ]),
##	('PyQt4\phonon_backend', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\phonon_backend\phonon_ds94.dll'
##        ]),
##	('PyQt4\sqldrivers', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\sqldrivers\qsqlite4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\sqldrivers\qsqlmysql4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\sqldrivers\qsqlodbc4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\sqldrivers\qsqlpsql4.dll'
##        ]),
##	('PyQt4\\accessible', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\accessible\qtaccessiblewidgets4.dll',
##        ]),
##	('PyQt4\codecs', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\codecs\qcncodecs4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\codecs\qjpcodecs4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\codecs\qkrcodecs4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\codecs\qtwcodecs4.dll'
##        ]),
##	('PyQt4\\bearer', [
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\bearer\\qgenericbearer4.dll',
##        r'C:\Python27\Lib\site-packages\PyQt4\plugins\bearer\\qnativewifibearer4.dll'
##        ]),
        ],

    #windows
    console=[
        {
            'script':PROG_file,
            "dest_base" : PROG_exe_name.lower(), # its exe name
            'icon_resources': [(0, PROG_icon)]

        }],

# I can combine more script, that means I create from script one an exe-file and from script two also an exe-file.
# At the end I have two exe-files.
##        windows=[{'script':'w1.py'},{'script':'w2.py'}],
##        console=[{'script':'c1.py'},{'script':'c2.py'}],

	zipfile = PROG_Pfad + PROG_exe_name.lower() + ".zip",

	options={'py2exe': {'bundle_files': 3,
                            'unbuffered': True,
                            'optimize': 2,
                            'excludes': excludes,
                            'compressed': True,
                            'dist_dir': PROG_name,
                            'xref': False,
                            'skip_archive': False,
                            'ascii': False,
                            'custom_boot_script': '',
                            'dll_excludes': ['w9xpopen.exe', "mswsock.dll", "POWRPROF.dll"],
                            'includes': includes,
                            'packages': ['os', 'logging', 'sqlalchemy', 'sqlalchemy.dialects.mysql', 'pymysql'],
                            }
                 },

	),

zipf = zipfile.ZipFile(PROG_name+".zip", 'w', zipfile.ZIP_DEFLATED)
zip_dir(PROG_name, zipf)

# Remove the build file_path_front_cover
shutil.rmtree("build", ignore_errors=True)
shutil.rmtree(PROG_name, ignore_errors=True)

