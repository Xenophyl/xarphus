#!/usr/bin/env python
# -*- coding:utf-8 -*-

FILE_NAME = __name__

import configparser
from cStringIO import StringIO
import os

class DefaultINIFile(object):
    '''Doc - Inside Class '''
    def __init__(self):
        '''Doc - __init__ Constructor'''

        self.DEFAULT_INI = """
[General_Configuration]
language = german_sie
language_id = 1
database_type = SQLite
ask_befor_close = True
dir_path_temp_folder = D:\\Dan\\Python\\xarphus\\temp
url_update = http://
time_out_update = 5
time_out_version_history = 5
url_version = http://www.web.de
last_folder_path_remeber = None
visible_tool_bar = True
visible_status_bar = True

[General_Design]
gui_style = automatically
gui_stiyle_id = 0
scheme_color = none
scheme_color_id = 0

[Category_Manager]
visible_movie = True
visible_book = True
visible_music = True
visible_coin = True
visible_person = True
visible_contact = True
visible_note = True
visible_video_game = True
visible_quotation = True
visible_stamp = True
visible_wine = True
visible_rent = True

[Download_Manager]
has_downloaded = None
                            """

    def user_ini(self, dict_custom_config):
        USER_INI = """[General_Configuration]
language = """ + dict_custom_config.dict_set_general_settings["language"] + """
language_id = """ + dict_custom_config.dict_set_general_settings["language_id"] + """
database_type = """ + dict_custom_config.dict_set_general_settings["Database"] + """
ask_befor_close = """ + dict_custom_config.dict_set_general_settings["AskBeforClose"] + """
dir_path_temp_folder = """ + dict_custom_config.dict_set_general_settings["PathToTempFolder"] + """
url_update = """ + dict_custom_config.dict_set_general_settings["URLServerUpdate"] + """
time_out_update = """ + dict_custom_config.dict_set_general_settings["TimeOutUpdate"] + """
time_out_version_history = """ + dict_custom_config.dict_set_general_settings["TimeOutGetVersionHistory"] + """
url_version = """ + dict_custom_config.dict_set_general_settings["URLServerVersion"] + """
last_folder_path_remeber = """ + dict_custom_config.dict_set_general_settings["LastPathFolderDialog"] + """
visible_tool_bar = """ + dict_custom_config.dict_set_general_settings["VisibleToolBar"] + """
visible_status_bar = """ + dict_custom_config.dict_set_general_settings["VisibleStatusBar"] + """

[General_Design]
gui_style = """ + dict_custom_config.dict_set_general_settings["GUIStyle"] + """
gui_style_id = """ + dict_custom_config.dict_set_general_settings["GUIStiyleID"] + """
scheme_color = """ + dict_custom_config.dict_set_general_settings["SchemeColor"] + """
scheme_color_id = """ + dict_custom_config.dict_set_general_settings["SchemeColorID"] + """

[Category_Manager]
visible_movie = """ + dict_custom_config.dict_set_general_settings["VisibleMovie"] + """
visible_book = """ + dict_custom_config.dict_set_general_settings["VisibleBook"] + """
visible_music = """ + dict_custom_config.dict_set_general_settings["VisibleMusic"] + """
visible_coin = """ + dict_custom_config.dict_set_general_settings["VisibleCoin"] + """
visible_person = """ + dict_custom_config.dict_set_general_settings["VisiblePerson"] + """
visible_contact = """ + dict_custom_config.dict_set_general_settings["VisibleContact"] + """
visible_note = """ + dict_custom_config.dict_set_general_settings["VisibleNote"] + """
visible_video_game = """ + dict_custom_config.dict_set_general_settings["VisibleVideoGame"] + """
visible_quotation = """ + dict_custom_config.dict_set_general_settings["VisibleQuotation"] + """
visible_stamp = """ + dict_custom_config.dict_set_general_settings["VisibleStamp"] + """
visible_wine = """ + dict_custom_config.dict_set_general_settings["VisibleWine"] + """
visible_rent = """ + dict_custom_config.dict_set_general_settings["VisibleRent"] + """

[Download_Manager]
has_downloaded = """ + dict_custom_config.dict_set_general_settings["HasDownloaded"] + """
"""

        return USER_INI

def generate_constants_sections_options(operate_mod=None):
    dict_sections = {"SECTION_GENERAL": "General_Configuration",
                     "SECTION_GUI": "General_Design",
                     "SECTION_CATEGORY_MANAGER": "Category_Manager",
                     "SECTION_DOWNLOADED_MANAGER": "Download_Manager",
    }

    dict_options = {"OPTION_LANGUAGE": "language",
                    "OPTION_LANGUAGE_ID": "language_id",
                    "OPTION_DATABASE_TYPE": "database_type",
                    "OPTION_ASK_BEFORE_CLOSE": "ask_befor_close",
                    "OPTION_DIR_PATH_TEMP_FOLDER": "dir_path_temp_folder",
                    "OPTION_URL_UPDATE": "url_update",
                    "OPTION_TIME_OUT_UPDATE": "time_out_update",
                    "OPTION_TIME_OUT_VERSIONHISTORY": "time_out_version_history",
                    "OPTION_URL_VERSION": "url_version",
                    "OPTION_LAST_FOLDER_PATH_REMEMBER": "last_folder_path_remeber",
                    "OPTION_LOG_FOLDER_PATH": "log_folder_path",
                    "OPTION_VISIBLE_TOOL_BAR": "visible_tool_bar",
                    "OPTION_VISIBLE_MOVIE": "visible_movie",
                    "OPTION_VISIBLE_BOOK": "visible_book",
                    "OPTION_VISIBLE_MUSIC": "visible_music",
                    "OPTION_VISIBLE_COIN": "visible_coin",
                    "OPTION_VISIBLE_PERSON": "visible_person",
                    "OPTION_VISIBLE_CONTACT": "visible_contact",
                    "OPTION_VISIBLE_NOTE": "visible_note",
                    "OPTION_VISIBLE_VIDEO_GAME": "visible_video_game",
                    "OPTION_VISIBLE_QUOTATION": "visible_quotation",
                    "OPTION_VISIBLE_STAMP": "visible_stamp",
                    "OPTION_VISIBLE_WINE": "visible_wine",
                    "OPTION_VISIBLE_RENT": "visible_rent",
                    "OPTION_VISIBLE_STATUS_BAR": "visible_status_bar",
                    "OPTION_GUI_STYLE": "gui_style",
                    "OPTION_GUI_STYLE_ID": "gui_style_id",
                    "OPTION_SCHEME_COLOR": "scheme_color",
                    "OPTION_SCHEME_COLOR_ID": "scheme_color_id",
                    "OPTION_HAS_DOWNLOADED": "has_downloaded",
    }


    if operate_mod == "sections":
        return dict_sections

    if operate_mod == "options":
        return dict_options

def read_configuration(filename, default_ini_file, read_only_default=None):
    parser = configparser.SafeConfigParser()

    if read_only_default:
        parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
    else:
        parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
        parser.read(filename)
    return parser

def css_setting(path_config):

    default_ini_file = DefaultINIFile()
    configuration = read_configuration(path_config, default_ini_file, None)
    
    result_sections = generate_constants_sections_options(operate_mod="sections")
    result_options = generate_constants_sections_options(operate_mod="options")

    '''
    Section for generally configuration
    '''
    return configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_SCHEME_COLOR"]))

def style_factory_setting(path_config):

    default_ini_file = DefaultINIFile()
    configuration = read_configuration(path_config, default_ini_file, None)
    
    result_sections = generate_constants_sections_options(operate_mod="sections")
    result_options = generate_constants_sections_options(operate_mod="options")

    '''
    Section for generally configuration
    '''
    return configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_GUI_STYLE"]))


def get_configuration(path_config, set_get_settings, default_ini_file, read_only_default=None):

    configuration = read_configuration(path_config, default_ini_file, read_only_default)  

    result_sections = generate_constants_sections_options(operate_mod="sections")

    result_options = generate_constants_sections_options(operate_mod="options")

    '''
    Section for generally configuration
    '''
    set_get_settings.dict_set_general_settings["language"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LANGUAGE"]))

    set_get_settings.dict_set_general_settings["language_id"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LANGUAGE_ID"]))

    set_get_settings.dict_set_general_settings["Database"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_DATABASE_TYPE"]))
    
    set_get_settings.dict_set_general_settings["AskBeforClose"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_ASK_BEFORE_CLOSE"]))

    set_get_settings.dict_set_general_settings["PathToTempFolder"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_DIR_PATH_TEMP_FOLDER"]))

    set_get_settings.dict_set_general_settings["URLServerUpdate"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_URL_UPDATE"]))

    set_get_settings.dict_set_general_settings["TimeOutUpdate"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_TIME_OUT_UPDATE"]))

    set_get_settings.dict_set_general_settings["TimeOutGetVersionHistory"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_TIME_OUT_VERSIONHISTORY"]))

    set_get_settings.dict_set_general_settings["URLServerVersion"] =  configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_URL_VERSION"]))

    set_get_settings.dict_set_general_settings["LastPathFolderDialog"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LAST_FOLDER_PATH_REMEMBER"]))

    set_get_settings.dict_set_general_settings["VisibleToolBar"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_VISIBLE_TOOL_BAR"]))

    set_get_settings.dict_set_general_settings["VisibleStatusBar"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_VISIBLE_STATUS_BAR"]))


    '''
    Section for designing gui
    '''
    set_get_settings.dict_set_general_settings["GUIStyle"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_GUI_STYLE"]))

    set_get_settings.dict_set_general_settings["GUIStiyleID"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_GUI_STYLE_ID"]))

    set_get_settings.dict_set_general_settings["SchemeColor"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_SCHEME_COLOR"]))

    set_get_settings.dict_set_general_settings["SchemeColorID"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_SCHEME_COLOR_ID"]))

    '''
    Section for viewing category
    '''
    set_get_settings.dict_set_general_settings["VisibleMovie"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_MOVIE"]))

    set_get_settings.dict_set_general_settings["VisiblePerson"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_PERSON"]))

    set_get_settings.dict_set_general_settings["VisibleBook"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_BOOK"]))

    set_get_settings.dict_set_general_settings["VisibleNote"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_NOTE"]))

    set_get_settings.dict_set_general_settings["VisibleStamp"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_STAMP"]))

    set_get_settings.dict_set_general_settings["VisibleRent"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_RENT"]))

    set_get_settings.dict_set_general_settings["VisibleMusic"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_MUSIC"]))

    set_get_settings.dict_set_general_settings["VisibleQuotation"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_QUOTATION"]))

    set_get_settings.dict_set_general_settings["VisibleVideoGame"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_VIDEO_GAME"]))

    set_get_settings.dict_set_general_settings["VisibleWine"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_WINE"]))

    set_get_settings.dict_set_general_settings["VisibleCoin"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_COIN"]))

    set_get_settings.dict_set_general_settings["VisibleContact"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_CONTACT"]))

    '''
    Section for download manager
    '''
    set_get_settings.dict_set_general_settings["HasDownloaded"] = configuration.get(result_sections["SECTION_DOWNLOADED_MANAGER"],
                                                        (result_options["OPTION_HAS_DOWNLOADED"]))

    return

def create_write_ini_file(dict_custom_config, default_ini_file):
    print "PATH TO INI", dict_custom_config.dict_set_general_settings["PathToINISetting"]
    '''
    This function will create a config-ini-file with sections followed by options
    that will contain some value. Also note that when it writes the config to disk,
    it uses the “w” flag to write it. This is not required, because
    for text files its enough to use plain 'w'. But it’s how the official documentation example does it.

    NOTICE:
    '''
    with open(dict_custom_config.dict_set_general_settings["PathToINISetting"], "w") as config_file:
        config_file.write(str(StringIO(default_ini_file.user_ini(dict_custom_config)).read().replace("                            ", "")))
    return True

def file_is_empty(path):
    return os.stat(path).st_size==0

##def copyLargeFile(src, dest, buffer_size=16000):
##    with open(src, 'rb') as fsrc:
##        with open(dest, 'wb') as fdest:
##            shutil.copyfileobj(fsrc, fdest, buffer_size)

if __name__ == '__main__':
    from config import ConfigurationSaver

    dict_custom_config = ConfigurationSaver()
    default_ini_file = DefaultINIFile()

    user_ini_file = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    dict_custom_config.dict_set_general_settings["PathToINISetting"] = user_ini_file

    error_msg = "The ini file may have been modified or destroyed. There load default settings."
    try:
        get_configuration(user_ini_file, dict_custom_config, default_ini_file, read_only_default=False)
    except configparser.MissingSectionHeaderError as ex:
        get_configuration(user_ini_file, dict_custom_config, default_ini_file, read_only_default=True)
        print error_msg
    finally:
        create_write_ini_file(dict_custom_config, default_ini_file)

    #return
