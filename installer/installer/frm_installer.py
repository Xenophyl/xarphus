#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QDialog
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile
from PyQt4 import QtCore

'''
Private modules are imported.
'''
try:
    from gui import ui_rc
except ImportError:
    '''
    Private modules are imported.
    '''   
    ## Import compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

BASE_PATH = path.dirname(path.abspath(__file__))
#UI_PATH = path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
#self.path_mdi_form = path.abspath(".")
#self.getPath_about = path.join(path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

class Installer_Window(QDialog):
    '''
        NOTICE:
        =======
        This class loads and shows interfaces for an about form.

    '''
    def __init__(self,
                 sys_argv,
                 parent=None):
        '''
            PARAMETERS
            ==========
            :app_info                        -  We except a created instance to access the dictionary.

            :parent                         -   This class does support passing a parent argument, because 
                                                we know that we will need the parent argument - its more flexible.
        '''
        QDialog.__init__(self, parent)

        self._sys_argv = sys_argv

        print "sys_argv", self._sys_argv

        UI_PATH = QFile(":/ui_file/installer.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.load_ui_file(UI_PATH)
        self.init_gui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_installer = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_gui(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently the title 
            of the window is set and the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Inistialize the GUI")

        self.ui_installer.setWindowModality(Qt.ApplicationModal)

        #flags = flag_dialog_titlehint_customizewondowhint()
        #self.ui_installer.setWindowFlags(flags)
        return

      
if __name__ == "__main__":
    from sys import argv as sys_argv, exit as sys_exit
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    
    app = QApplication(sys_argv)
    
    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)
    
    window = Installer_Window(sys_argv)
    window.show()
    sys_exit(app.exec_())
