#!/usr/bin/env python
#-*- coding:utf-8 -*-

import zipfile

def test_zip_file(file_descriptor):
    '''
    Take a path to a zipfile and checks if it is a valid zip file
    or check the integrity of the file.

    NOTICE:
    =======
    Read all the files in the archive and check their CRC’s and file headers.
    Return the name of the first bad file, or else return None.
    Calling testzip() on a closed ZipFile will raise a RuntimeError.
    '''
    with zipfile.ZipFile(file_descriptor,'r') as inzipfile:
        if inzipfile.testzip() is not None:
            '''
            The zip file is bad
            '''
            return False
        else:
            '''
            The zip file is NOT bad
            '''
            return True

def valid_zip_file(file_descriptor):
    '''
    Returns True if filename is a valid ZIP file
    based on its magic number, otherwise returns False.
    filename may be a file or file-like object too.
    '''
    
    with zipfile.ZipFile(file_descriptor,'r') as inzipfile:
        ret = zipfile.is_zipfile(file_descriptor)
        print "ret", ret
        if ret is not None:
            '''
            The zip file is a valid pkzip file"
            '''
            return True
        else:
            '''
            The zip file is NOT a valid pkzip file
            '''
            return False

