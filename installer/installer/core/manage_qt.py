from PyQt4.QtCore import Qt
from PyQt4.QtGui import QMessageBox
def flag_dialog_titlehint_customizewondowhint():
    '''
    This window has no additional buttons in the title bar:
    That means no:
        - minimize-button
        - maximize-button
        - help-button
        - close-button
    '''
    return Qt.Dialog | Qt.WindowTitleHint | Qt.CustomizeWindowHint

def custom_message_box(self, text_error, m_title, mIcon):
    mBox = QMessageBox()
    mBox.setWindowTitle(unicode(m_title))
    mBox.setText(unicode(text_error))
    mBox.setWindowFlags(self.flags)
    mBox.setIcon(mIcon)
    return mBox # Returns the object.

def custom_message_box_1(text_error, m_title, mIcon):
    mBox = QMessageBox()
    mBox.setWindowTitle(unicode(m_title))
    mBox.setText(unicode(text_error))
    mBox.setWindowFlags(flags)
    mBox.setIcon(mIcon)
    return mBox # Returns the object.
