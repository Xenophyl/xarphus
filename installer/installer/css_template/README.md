# README #
---------------------------
## Requirements ##
==================
### In which version of python is ``xarphus`` written? ###
Python  2.7.10 [Download](https://www.python.org/downloads/release/python-2710/)

### In which version of PyQT (Python bindings for the Qt cross-platform application) is ``xarphus`` written? ###
PyQt 4.11.3 [Package Index](https://pypi.python.org/pypi/PyQt4)

### Which version of QT (UI framework) is used in ``xarphus``? ###
Qt 4.8.6 [Download](https://download.qt.io/archive/qt/4.8/4.8.6/)

### Which libraries are needed in ``xarphus``? ####
NOTE: There are several ways to obtain the dependent libraries. For safety reasons, the required libraries are added to the project. 
You can find the libraries directly in the folder named ``Third_Party_Libraries``. Well, you can just download the dependent libraries from here (by clicking on url's) 
or you visit the manufacturer's website and download it from there. There is also a requirements file named ``requirements.txt``. Its your choice. 

* BeautifulSoup (4.3.2) [Package Index](https://pypi.python.org/pypi/beautifulsoup4)

* mechanize (0.2.5) [Package Index](https://pypi.python.org/pypi/mechanize/0.2.5)

* requests (2.7.0) [Package Index](https://pypi.python.org/pypi/requests)

* Pillow (2.8.2) [Package Index](https://pypi.python.org/pypi/Pillow/2.8.2)

* MySQLdb (1.2.5) [Package Index](https://pypi.python.org/pypi/MySQL-python/1.2.5)

* SQLAlchemy (1.0.12) [Package Index](https://pypi.python.org/pypi/SQLAlchemy)


## Assistance (additional notes) ##
===================================
### How to install the libraries in a windows system? ####

#### --> First possible option ####

Now you want to install all dependent libraries in a windows system?

Imagine, you have downloaded the library BeautifulSoup4, unzipped and stored it in the following location:
e.g.:
```python
C:\downloads\beautifulsoup4-4.3.2
```

STEP 1:
Open the command prompt or PowerShell.

STEP 2:
We need the following command line:
```python
python setup.py C:\downloads\beautifulsoup4-4.3.2\setup.py install
```
!!! Assuming that you have added Python to your system PATH - 
if you haven't and you don't want to you can run:

```python
C:\Python27\python C:\downloads\beautifulsoup4-4.3.2\setup.py install
```
(We assume that you have installed Python under C drive correctly.)

#### --> Second possible option ####

Do you have a command line tool named pip?

Here I can offer you two options.

First of all, you can use the ``requirements`` file with pip-tool.

Well, if you want to install all third party libraries with the pip-tool, you can run the following command line in your Shell/PowerShell:  

``pip install -r requirements.txt``

But, when you need to install the third party libraries separately and manually, you can go the following way.

Run the command in your console, not in the Python interpreter:
```python
pip install beautifulsoup4
```

You may have to use the full path:
```python
C:\Python27\Scripts\pip install beautifulsoup4
```

or even
```python
C:\Python27\Scripts\pip.exe install beautifulsoup4
```
Windows will then execute the pip program and that will use Python to install the package.

### How to install the libraries in a linux system? ####

Lets see how to install it in detail.

If you’re using a recent version of Debian or Ubuntu Linux, you can install BeautifulSoup with the system package manager:
```python
apt-get install python-bs4
```
NOTE: As root!!

If its a red hat based Linux then
```python
yum install python-bs4
```

Beautiful Soup 4 is published through PyPi, so if you can’t install it with the system packager, you can install 
it with easy_install or pip. The package name is beautifulsoup4, and the same package works on Python 2 and Python 3.
```python
easy_install beautifulsoup4
```
or
```python
pip install beautifulsoup4 
```
After successfully installed these required libraries on the system import the modules with this command in the Python interpreter.

```python
from bs4 import BeautifulSoup
```
in your python script.
Now you have successfully installed and good to go.

### How to start ``xarphus``? ####

After downloaded and successfully installed all the required libraries you can just 
start xarphus with xarphus_main.bat, xarphus_main.py, or xarphus_main.pyw

### What license is used? ####

``xarphus`` is released under the simplified GPL v2 license (see the [LICENCE_GPL_v2](https://bitbucket.org/Xenophyl/xarphus/raw/30f39a18ca8bbc2d309a583cff9d3d6a7325a7ea/xarphus/licence/LICENCE_GPL_v2.txt) file for
details).