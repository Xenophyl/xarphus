#!/usr/bin/env python
#-*- coding:utf-8 -*-

# This is only needed for Python v2 but is harmless for Python v3.
import sip
#   We have to load sip before loaing PyQt4
try:
    sip.setapi('QVariant', 2)
except ValueError:
    sip.setapi('QVariant', 1)

try:
    import logging
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
except IOError:
    pass



#   Import all Built-in Functions
from sys import argv, exc_info, exit
from time import sleep, time
from traceback import format_exc

try:
    #from configparser import ConfigParser
    from configparser import SafeConfigParser, MissingSectionHeaderError
except ImportError:
    from ConfigParser import SafeConfigParser  # ver. < 3.0

import argparse
from glob import glob
from os import path, chdir, error, remove, unlink
from tempfile import NamedTemporaryFile, TemporaryFile

#    Import all PyQt bindings
from PyQt4.QtGui import QApplication, QPixmap, qApp, QSplashScreen, QProgressBar, QStyleFactory, \
    QMessageBox, QWizard, QLabel, QIcon, QWidget, QPushButton
from PyQt4.QtCore import Qt, QFile, QTextStream, QTranslator, QString, QCoreApplication, QEvent, \
    pyqtSignal, QThread, QObject

from PyQt4 import QtCore
from PyQt4.uic import loadUi

#   Import all forms
from xarphus.frm_mdi import MDI_Window

from xarphus.frm_error_wizard import MainMissingProfileWizard, MainMissingINIFileWizard

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################

#   Import core codes
try:

    from xarphus.core.config import ConfigurationSaverDict
    from xarphus.core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile, read_in_configuration
    from xarphus.core.manage_logging import configure_logging
    from xarphus.core.manage_time import get_time_now
    from xarphus.core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, is_folder_exists, is_folder_empty, create_folder
    from xarphus.core.manage_file import find_files, delete_file
    from xarphus.info import info_app
    from xarphus.core.set_settings import to_set_general_settings
    from xarphus.core.manage_qt import custom_message_box
    from xarphus.core.manage_file import file_exists, read_in_chunks

    from xarphus.frm_profiles_list import ProfileList_Window

    from xarphus.languages.german import Language

    from xarphus.core.proxy_model import ProxyModel
    from xarphus.core.standard_item_model import StandardItemModel

    from xarphus.collection_paths_image import ImagePathnCollection

    from xarphus.core.managed_safe_config_parser import set_up_default_in_config, store_config_parser_values_in_custom_config_dict

except ImportError: #   When the module is started as a program

    from core.config import ConfigurationSaverDict
    from core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile, read_in_configuration
    from core.manage_logging import configure_logging
    from core.manage_time import get_time_now
    from core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, is_folder_exists, is_folder_empty, create_folder
    from core.manage_file import find_files, delete_file
    from info import info_app
    from core.set_settings import to_set_general_settings
    from core.manage_qt import custom_message_box
    from core.manage_file import file_exists, read_in_chunks

    from frm_profiles_list import ProfileList_Window

    from languages.german import Language

    from core.proxy_model import ProxyModel
    from core.standard_item_model import StandardItemModel

    from collection_paths_image import ImagePathnCollection

    from core.managed_safe_config_parser import set_up_default_in_config, store_config_parser_values_in_custom_config_dict

BASE_PATH = path.dirname(path.abspath(argv[0]))


class App(QApplication):

    test_app_signal = pyqtSignal(object)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QApplication.__init__(self, parent)

        self._kwargs = kwargs

        self.safe_config_parser = SafeConfigParser()

        self.mdi_widget = None

        self._standard_item_model = StandardItemModel(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'))
        self._general_proxy_model = ProxyModel()

        self.BASE_PATH = path.dirname(path.abspath(argv[0]))
        self.PROFILES_FOLDER_PATH = path.join(self.BASE_PATH, 'profiles')
        self.SETTING_FOLDER_PATH = path.join(self.BASE_PATH, 'settings')
        self.CONFIG_PATH = path.join(self.BASE_PATH, 'settings', 'config.ini')
        self.ZIP_FILE_PATH = path.join(self.BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
        self.LOG_FOLDER_PATH = path.join(self.BASE_PATH, 'log')

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        #self.get_execute_file()

        self.create_splash_screen()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def get_execute_file(self):
        EXECUTE_FILE_PATH = ""
        BASE_PATH = path.join(self.BASE_PATH)
        chdir(BASE_PATH)

        for exe_file in glob("*.exe"):
            EXECUTE_FILE_PATH = BASE_PATH = path.join(self.BASE_PATH, exe_file)

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToExecuteFile"] = EXECUTE_FILE_PATH

    def load_translator_file(self):
        '''
           SUMMARY
           =======
           This method is used to load and install the QTranslator()-object.
           Returns TRUE if the file is loaded successfully; otherwise returns FALSE.
           That means it doesn't matter if the translate file could be loaded,
           the program continues without error. This method will display
           the original language on the screen: its English. This is the reason why
           no exception is handled.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of QTranslator()")

        FILE_EXTENSION = ".qm"

        self.translator = QTranslator()

        logger.info("The instance of the class of QTranslator() is created successfully")

        path = ":/translators/" + self._kwargs.get('configuration_saver_dict').dict_set_general_settings["language"]+ FILE_EXTENSION

        logger.info("Installing the QTranslator()-object")

        self.installTranslator(self.translator)

        logger.info("The translator is installed successfully")

        logger.info("Loading translate file for translation")

        self.translator.load(path)

        logger.info("Translate file is loaded successfully")

        return

    def load_stylesheet(self):
        '''
           SUMMARY
           =======
           This method is used to load and set the Style Sheet.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Reading and setting the style sheet")

        CSS_FILE_EXTENSION = "qss"

        #   First: Get the current css stylesheet and setup stylesheet
        #   Here its takes a look into the dictionary and checks
        #   if the value tied to 'key' (SchemeColor) evaluates to not True,
        #   do nothing -just pass", because the value is empty.

        #   The value tied to 'key' (SchemeColor) evaluates to True.
        scheme_color = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["SchemeColor"].lower()

        #   Create a path to the css file completly
        css_path = ":/{scheme_color}/{scheme_color}.{CSS_FILE_EXTENSION}".format(
            scheme_color = scheme_color,
            css_file_extension = CSS_FILE_EXTENSION)

        print "css_path", css_path[0]

        #self._kwargs.get('image_path_collection') = ImagePathnCollection(theme = path.splitext(path.basename(css_path))[0])

        #   Creating a constructor. It is an I/O device for reading and writing text.
        #   The variable as a file name (css_path) is passed in the constructor
        fd = QtCore.QFile(css_path)
        fd.open(QtCore.QIODevice.ReadOnly | QtCore.QFile.Text)

        '''
            For reading and writting the data it is using QTextStream.
        '''
        style = QtCore.QTextStream(fd).readAll()
        fd.close()

        '''
            Setting the style sheet.
        '''
        self.setStyleSheet(style)

        return

    def load_style(self):
        '''
           SUMMARY
           =======
           This method is used to load and set the style of QStyleFactory()-object.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Load and set the GUI-style")

        style_type = None

        #  Here its checks if the value tied to key (GUIStyle) evaluates not to True, that means its empty,
        #   than do nothing here. Set the standard style.
        if  not self._kwargs.get('configuration_saver_dict').dict_set_general_settings["GUIStyle"] or \
                self._kwargs.get('configuration_saver_dict').dict_set_general_settings["GUIStyle"].lower() == "automatically":
            style_type = qApp.style().objectName()
        else:
            #   The value tied to key (GUIStyle) evaluates to True - isn't empty
            style_type = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["GUIStyle"]

        #   Finally its styles the GUI.
        self.setStyle(QStyleFactory.create(style_type))

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :set_flag (bool):        For removing close button from QMessageBox.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.
        exit()

    def create_missing_ini_file_wizard(self,
                                       profile_name,
                                       configuration_saver_dict,
                                       path_ini_file,
                                       safe_config_parser):
        '''
           SUMMARY
           =======
           This method is used to create an Wizard-instance. For example the user
           starts this programs at the first time or the ini file
           in which all settings and stuffs are saved, is empy, lost or damaged, the user will get a Wizard-window - is just
           like a assistant-window.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of MainMissingINIFileWizard()")

        '''
            Set the buttons in a sequence and safe it in a list
        '''
        self.main_missing_ini_file_wizard = MainMissingINIFileWizard(
                                                configuration_saver_dict = configuration_saver_dict,
                                                profile_name = profile_name,
                                                safe_config_parser = safe_config_parser,
                                                path_ini_file = path_ini_file,
                                                about_app = self._kwargs.get('about_app'),
                                                default_ini_file = self._kwargs.get('default_ini_file'))

        self.main_missing_ini_file_wizard.show()

        return

    def create_missing_profile_wizard(self,
                                       profile_name,
                                       configuration_saver_dict,
                                       path_ini_file,
                                       safe_config_parser):
        '''
           SUMMARY
           =======
           This method is used to create an Wizard-instance. For example the user
           starts this programs at the first time or the profile folder, in which all settings
           and stuffs are saved, is empy or damaged, the user will get a Wizard-window - is just
           like a assistant-window.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of MainMissingProfileWizard()")

        '''
            Set the buttons in a sequence and safe it in a list
        '''
        self.main_missing_profile_wizard = MainMissingProfileWizard(
                                                configuration_saver_dict = configuration_saver_dict,
                                                profile_name = profile_name,
                                                safe_config_parser = safe_config_parser,
                                                path_ini_file = path_ini_file,
                                                default_ini_file = self._kwargs.get('default_ini_file'))

        self.main_missing_profile_wizard.show()

        return

    def create_mdi_window(self):
        '''
           SUMMARY
           =======
           This method is used to create an MDI-instance and show this as a window.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Creating an instance of the class of MDI)")

        try:

            logger.info("Trying to an instance of the class of MDI_Window()")

            window = MDI_Window(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                info_app = self._kwargs.get('about_app'),
                                image_path_collection = self._kwargs.get('image_path_collection'),
                                default_ini_file = self._kwargs.get('default_ini_file'),
                                safe_config_parser = self.safe_config_parser,
                                app = self)#,
                                #parent = self)

            window.showMaximized()

            self.mdi_widget = window

        except Exception:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def create_error_messagebox(self,
                                widget,
                                title,
                                text):
        '''
           SUMMARY
           =======
           This method is used to create a message box it is shows
           the error text.

           PARAMETERS
           ==========
           :widget (class): QMessageBox.critical needs to pass a QObject/QWidget object.

            :title (str):   Message box main text.

            :text (str):    Message box informative text.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create an error message box.")

        QMessageBox.critical(widget, title, text)

        return


    def open_ini_file(self, splash_screen_obj):

        with open(self.CONFIG_PATH, 'r') as ini_file:
            try:
                '''
                Ok, the ini file already exists. Next stept: let's check wheter the ini file is empty.
                '''
                result_file_size = file_is_empty(self.CONFIG_PATH)
                if result_file_size:
                    '''
                    The boolean True means, that ini file is empty
                    '''
                    splash_screen_obj.finish(None)
                    self.create_error_messagebox(
                        widget = self.mdi_widget,
                        title = self.tr("Error"),
                        text = self.tr("The INI-file appears to have "
                                         "been damaged or modified. You "
                                         "have the ability record a "
                                         "secure INI-file or create a new "
                                         "INI-file."))

                else:
                    '''
                    The ini file isn't empty.
                    '''
                    #TODO: In this case I have to validate the ini file wether all OPtIONS and SECTIONS are fine
                    get_configuration(self.CONFIG_PATH, self._kwargs.get('configuration_saver_dict'), self._kwargs.get('default_ini_file'))

                    self.load_translator_file()

                    self.load_stylesheet()
                    self.load_style()

                    '''
                    Let us finish the splash scrren, remove the event filter and start the MDI-Form
                    '''
                    splash_screen_obj.finish(self.create_mdi_window())
                    self.remove_event_filter(splash_screen_obj)

            except:
                desired_trace = format_exc(exc_info())
                print "desired_trace:", desired_trace


    def create_profile_list_window(self,
                                   splash_screen,
                                   folder_path_list,
                                   folder_name_list = None):
        '''
           SUMMARY
           =======
           This method is used to create a form of profile list.

           PARAMETERS
           ==========
           :splash_screen (class):    We need the splash screen to hide or unhide it later.

           :folder_path_list (str):   We get list which contains all paths of folders.

           :folder_name_list (list):  Its also a list, but its contains all namens of folders.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        try:
            logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

            profile_list_form = ProfileList_Window(folder_name_list = folder_name_list,
                                                   folder_path_list = folder_path_list,
                                                   image_path_collection = self._kwargs.get('image_path_collection'),
                                                   configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                   parent = self)

            #profile_list_form.close_app_signal.connect(lambda: exit() )
            profile_list_form.visible_splash_screen_signal.connect(lambda bool_value: splash_screen.setVisible(bool_value) )

            profile_list_form.return_profile_name_signal.connect(
              lambda
              profile_name:
              self.start_on_thread(
                splash_screen = splash_screen,
                profile_name = profile_name,
                base_path = self.BASE_PATH,
                profile_list_form = profile_list_form))

            logger.info("Connecting all signales of PersonProfile_Window()-class")

            logger.info("Show the new subwindow maximized")

            profile_list_form.show()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_messagebox(
                widget = self.mdi_widget,
                title = self.tr("Critical error"),
                text = desired_trace)


    def start_on_thread(self,
                        splash_screen,
                        base_path = '',
                        profile_list_form = None,
                        profile_name = None):
        '''
           SUMMARY
           =======
           This method is used to start a thread of AppWorker.

           PARAMETERS
           ==========
           :splash_screen (class):        We need the splash screen to hide or unhide it later.

           :base_path (str):              We get the base path, to create a complete path.

           :profile_list_form (class):    We need the instance of the Profile form, that we can hide it later.

           :profile_name (str):           By default its None. When the user selected a profile, the name is transmitted
                                          to create a complete path.

           RETURNS
           =======
           :return:                       Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                          me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start thread of AppWorker")

        config_path = path.join(base_path, "profiles",  unicode(profile_name), 'settings', 'config.ini')

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings['current_profile'] = unicode(profile_name)

        try:

          task_thread = QThread(self)
          task_thread.work = AppWorker(
            default_ini_file = self._kwargs.get('default_ini_file'),
            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
            safe_config_parser = self.safe_config_parser,
            path_ini_file = config_path,
            profile_name = profile_name,
            image_path_collection = self._kwargs.get('image_path_collection'))

          task_thread.work.moveToThread(task_thread)

          task_thread.work.create_missing_profile_wizard_signal.connect(
            lambda:
            self.create_missing_profile_wizard(
              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
              profile_name = path.join(self.BASE_PATH, "profiles",  unicode(profile_name)),
              path_ini_file = config_path,
              safe_config_parser = self.safe_config_parser))

          task_thread.work.create_missing_ini_file_wizard_signal.connect(
            lambda:
            self.create_missing_ini_file_wizard(
              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
              profile_name = path.join(self.BASE_PATH, "profiles",  unicode(profile_name)),
              path_ini_file = config_path,
              safe_config_parser = self.safe_config_parser))

          task_thread.work.finish_splash_signal.connect(lambda: splash_screen.finish(None))

          task_thread.work.create_profile_list_window_signal.connect(
            lambda
            folder_name_list,
            folder_path_list:
            self.create_profile_list_window(
                splash_screen = splash_screen,
                folder_name_list = folder_name_list,
                folder_path_list = folder_path_list))

          task_thread.work.send_enerally_crritcal_msg_siggnal.connect(
            lambda
            title_text,
            conn_err,
            icon,
            set_flag,:
            self.create_generally_crritcal_msg(
              err_title = title_text,
              err_msg = conn_err,
              set_flag = set_flag,
              icon = icon))

          task_thread.work.quit_thread_signal.connect(task_thread.quit)
          task_thread.work.wait_thread_signal.connect(task_thread.wait)

          task_thread.started.connect(task_thread.work.init_checking)
          task_thread.work.create_mdi_form_signal.connect(self.create_mdi_window)

          if not profile_list_form is None:
              task_thread.work.close_profile_list_signal.connect(profile_list_form.hide)

          task_thread.finished.connect(task_thread.deleteLater)

          task_thread.start()

        except AttributeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            if unicode(desired_trace).find("'NoneType' object has no attribute") != -1:
              pass

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            #splash.finish(None)
            splash_screen.setVisible(False)

            self.create_error_messagebox(
                widget = self.mdi_widget,
                title = self.tr("Critical error"),
                text = desired_trace)

            self.close()

        return

    def create_splash_screen(self):
        '''
           SUMMARY
           =======
           This method is used to create an show the splash screen.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create splash screen")

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"] = self.BASE_PATH


        to_set_general_settings(self.CONFIG_PATH, self.SETTING_FOLDER_PATH, self.ZIP_FILE_PATH,
                                             self._kwargs.get('configuration_saver_dict'))



        # Create and display the splash screen
        SplashPic = ":/img_512x512_default/default/img_512x512/splash_loading.png"
        splash_pix = QPixmap(SplashPic)
        splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)

        splash.installEventFilter(self)

        copy_right_content = "{product_copyright_characters} {product_copyright} 2014-2016 {product_by} {product_author}".format(product_copyright_characters=self._kwargs.get('about_app').dict_info["product_copyright_characters"],
                                                                                       product_copyright=self._kwargs.get('about_app').dict_info["product_copyright"],
                                                                                       product_by= self._kwargs.get('about_app').dict_info["product_by"],
                                                                                       product_author= self._kwargs.get('about_app').dict_info["product_author"],
                                                                                       exe_name=self._kwargs.get('about_app').dict_info["exe_name"],
                                                                                       product_version=self._kwargs.get('about_app').dict_info["product_version"]
                                                                                       )

        version_content = "{exe_name} Version ({product_version})".format(exe_name=self._kwargs.get('about_app').dict_info["exe_name"],
                                                                          product_version=self._kwargs.get('about_app').dict_info["product_version"]
                                                                                       )


        copy_right_label = QLabel(splash)
        copy_right_label.setAlignment(Qt.AlignRight)
        copy_right_label.setText(copy_right_content)
        copy_right_label.setStyleSheet('QLabel{color: #778899;}')


        version_label = QLabel(splash)
        version_label.setAlignment(Qt.AlignRight)
        version_label.setText(version_content)
        version_label.setStyleSheet('QLabel{color: #778899;}')

        # add a Label to the splash screen
        #copy_right_label.setGeometry(splash.width()/3.1, 8.55*splash.height()/10,
        #                            9.3*splash.width()/9.9, splash.height()/18)
        # First: Left, right
        # Second: up, down
        version_label.move(375, 10)
        copy_right_label.move(330, 280)

        # add a progressbar to the splashscreen
        progressBar = QProgressBar(splash)
        progressBar.setAlignment(Qt.AlignCenter)
        #                             # at first, the width of progressbar
        #                                                 # here the height of progressbar
        progressBar.setGeometry(splash.width()/56, 9.3*splash.height()/10,
                                    9.3*splash.width()/9.9, splash.height()/18)

        #
        splash.setMask(splash_pix.mask())
        splash.show()

        for i in range(0, 100):
            progressBar.setValue(i)
            t = time()
            while time() < t + 0.1:
                self.processEvents()

            self.processEvents()

        self.processEvents()
        # # Simulate something that takes time
        sleep(1)

        self.start_on_thread(splash_screen = splash)

    def eventFilter(self, splash_screen_widget, event):
        '''
           SUMMARY
           =======
           This eventFilter()-function disables the mouse click event - right and left.

           During the QSplashScreen()-class is showing for a while the user can't
           click the SplashScreen to make it disappear. Without this event filter when the
           user clicks the SplashScreen it disappears until the main window shows up.
           Thats why we disable the possibility of clicking on the splash screen.

           The eventFilter() function must return true if the event should be filtered,
           (i.e. stopped); otherwise it must return false.

           PARAMETERS:
           ===========
           :splash_screen_widget (class): The splash screen widget is necessary, so we are able to Continue
                                          the normal processing

           :event                (class): We need the event object to check if
                                          a button of a mouse is pressed

           RETURNS
           =======
           :return:   Call Base Class Method to Continue Normal Event Processing.
        '''
        if event.type() in (QEvent.MouseButtonPress,
                            QEvent.MouseButtonDblClick):
            if event.button() == Qt.LeftButton:
                return True
            if event.button() == Qt.RightButton:
                return True

        return QApplication.eventFilter(self, splash_screen_widget, event)

    def remove_event_filter(self, widget):
        '''
           SUMMARY
           =======
           This method removes the installed filter event.

           We don't need this filter event anymore.

           PARAMETERS
           ==========
           :widget:   Here we except an given widget from
                      where the eventFilters should be removed.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Remove indtalled filter event.")

        widget.removeEventFilter(self)

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event:      By default the given event is accepted and the widget is closed. We
                        get this event when Qt receives a window close request for a top-level
                        widget from the window system. That means, first it sends the widget a QCloseEvent.
                        The widget is closed if it accepts the close event. The default implementation of
                        QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        #   No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
        #   That means, the window is not a subwindow.
        self.close()

class AppWorker(QObject):

    create_missing_profile_wizard_signal = pyqtSignal()
    create_missing_ini_file_wizard_signal = pyqtSignal()
    send_enerally_crritcal_msg_siggnal = pyqtSignal(str, str, str, bool)
    create_profile_list_window_signal = pyqtSignal(list, list)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal= pyqtSignal()
    finish_splash_signal = pyqtSignal()
    create_mdi_form_signal = pyqtSignal()
    close_profile_list_signal = pyqtSignal()

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)

        self.BASE_PATH = path.dirname(path.abspath(argv[0]))

        self._kwargs = kwargs

    def configure_logging_data(self):
        '''
           SUMMARY
           =======
           This method chekcs if sub folder(s) are exist in the top level folder named profiles.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Check if there are sub folder(s) in the top level folder named profile")

        try:
            profile_name = self._kwargs.get('configuration_saver_dict').dict_set_general_settings['current_profile']
            #print "profile_name", profile_name
            #print "type(), profile_name", type(profile_name)

            if not profile_name == 'None':
                #print "YEAAAAH, profile is selected!!!!"
                convert_current_time = get_time_now()
                LOG_FILE_PATH = path.join(BASE_PATH, 'profiles', profile_name, 'log', convert_current_time + '.log')

                print "LOG_FILE_PATH", LOG_FILE_PATH
                configure_logging(log_file_path = LOG_FILE_PATH, reconfigure_logging_handler = True)

        except OSError:

          # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def check_general_profile_folder_is_empty(self,
                                              folder_path):
        '''
           SUMMARY
           =======
           This method chekcs if sub folder(s) are exist in the top level folder named profiles.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Check if there are sub folder(s) in the top level folder named profile")

        try:

            folder_path_result, folder_name_result = is_folder_empty(folder_path)

            # Let us look, if the top level profile folder is NOT empty
            if folder_path_result:

              # Yep, the top folder profile folder isn't empty, let us take a look
              # if there are more than zero profile folders - at least one.
              if len(folder_name_result) > 0:

                  self.create_profile_list_window_signal.emit(folder_name_result, folder_path_result)
                  self.finish_splash_signal.emit()

            else:
              # Ok, the top level folder is empty
              self.create_missing_profile_wizard_signal.emit()
              self.finish_splash_signal.emit()

        except OSError as e:

          # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def is_file_empty(self,
                     file_name,
                     config_parser,
                     custom_config_dict):
        '''
           SUMMARY
           =======
           In this method we take a look, if the given file is really empty.

           DETAILS:
           ========
           The reason why we read the given file is that we don't want to
           test from outside whether a file is actually empty, by checking the size of the file.
           Because a file with only full spaces, tabs and line breaks is still larger than 0 kilobytes.

           PARAMETERS
           ==========
           :file_name (str):    Its a path to the given file.

           RETURNS
           =======
           :return:             We return the result. True means, its a empty file.
                                Otherwise, False means isn't a empty file.
        '''
        logger.info("Read a given file")

        is_empty = True #   By default we have to set the variable on True, that means,
                        #   we assume that the file is empty.
                        #   Because if the file is absolutely empty,
                        #   there is no iteration after opening the file.
                        #   As soon as the file is completely empty, it is not
                        #   possible to set a flag within the for loop.

        try:
            with open(unicode(file_name), 'r') as file_object:
                for char in read_in_chunks(file_object = file_object):

                    content = char.replace(' ', '').replace('', '').replace('\t', '').replace('\n', '').replace('\r', '').replace('\r\n', '').replace('\v', '')
                    if not content:
                        #   The file is empty
                        is_empty = True
                    elif content:
                        # Now we know the given file isn't empty. Let us read the file in.
                        self.read_ini_file(
                          file_name = file_name,
                          config_parser = config_parser,
                          custom_config_dict = custom_config_dict)
                        #   The file is  NOT empty
                        is_empty = False
                        break # We can stop here because we found a
                              # character in the file.
                              # That means the file is NOT empty.

            if is_empty:
              self.close_profile_list_signal.emit()
              #   No, there isn't a folder of profiles. We have to create the wizard
              self.create_missing_ini_file_wizard_signal.emit()

        except (IOError, TypeError):

          # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)


            #   No, there isn't a folder of profiles. We have to create the wizard
            self.create_missing_ini_file_wizard_signal.emit()

    def read_ini_file(self,
                      config_parser,
                      custom_config_dict,
                      file_name):
        '''
           SUMMARY
           =======
           This method is used to read in the file.

           PARAMETERS
           ==========
           :config_parser (class): We need the Configuration object so that we can save the (default) settings there.

           :file_name (str):        Need to know where we should create the file.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Read in the file')

        try:
            read_in_configuration(config_parser = config_parser,
                                  file_name = file_name)
            # For testing, late we can delete it
            store_config_parser_values_in_custom_config_dict(
              config_parser = config_parser,
              custom_config_dict = custom_config_dict)

            self.create_mdi_form_signal.emit()
            self.close_profile_list_signal.emit()

        except MissingSectionHeaderError:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            if unicode(desired_trace).find('File contains no section headers.') != -1:
              self.close_profile_list_signal.emit()
              #   No, there isn't a folder of profiles. We have to create the wizard
              self.create_missing_ini_file_wizard_signal.emit()

        except EnvironmentError: # parent of IOError, OSError *and* WindowsError where available
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            if unicode(desired_trace).find('OSError: [Errno 2] No such file or directory:') != -1:

                #   No, there isn't a folder of profiles. We have to create the wizard
                self.create_missing_ini_file_wizard_signal.emit()

            self.finish_splash_signal.emit()
            self.finish_thread()

        return

    def create_temp_file(self,
                         folder_path,
                         custom_config_dict = None,
                         config_parser = None,
                         profile_name = None):
        '''
           SUMMARY
           =======
           This method is used to ensure that a temporary nameless file is created in the top-level
           folder belonging to Profile. If it isn't possible to manage a temp file in this folder,
           we know that the folder doesn't belong, the folder belongs.
           Otherwise, the created temporary file is opened as long as it is insulted until all other works are done.
           This prevents the top-level folder from being removed from participants.

           PARAMETERS
           ==========
           :folder_path (str):  Need to know the path to the top level folder.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Try to create a temporary file")

        try:
            with TemporaryFile(dir = folder_path) as temp:

                # Let us take a look if there is a profile name
                if not profile_name is None:

                  self.is_file_empty(
                    file_name = path.join(self.BASE_PATH, 'profiles', unicode(profile_name), 'settings', 'config.ini'),
                    config_parser = config_parser,
                    custom_config_dict = custom_config_dict)

                elif profile_name is None:
                  # Ok there is no profile name , so we have to take look, if the top level folder 'profile' exixts,
                  # but we also have to check if there a oder more subfolder(s) with name(s)of profile(s).
                  self.check_general_profile_folder_is_empty(folder_path = folder_path)

        except EnvironmentError: # parent of IOError, OSError *and* WindowsError where available
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            if unicode(desired_trace).find('OSError: [Errno 2] No such file or directory:') != -1:
                #   No, there isn't a folder of profiles. We have to create the wizard
                self.create_missing_profile_wizard_signal.emit()
                self.finish_splash_signal.emit()

            if unicode(desired_trace).find("[Errno 17] No usable temporary file name found") != -1 or\
            unicode(desired_trace).find("IOError: [Errno 13] Permission denied") !=-1:

                title_text = self.tr('No permission')
                conn_err = self.tr("We are sorry, but you don't have enough permission to modify the profile folder. \
                  \n\nContact the admistrator to obtain permission.")

                self.send_enerally_crritcal_msg_siggnal.emit(title_text, conn_err, self._kwargs.get('image_path_collection').warning_48x48_default, True)
                self.finish_splash_signal.emit()
        except:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('Unexpected error')
            conn_err = self.tr("We are sorry, but an unexpected system error has occurred. \
                  \nAdditional details have been looged. Please try again. \
                  \n\nIf the issue continues, please visit our technical support.")

            self.send_enerally_crritcal_msg_siggnal.emit(title_text, conn_err, self._kwargs.get('image_path_collection').warning_48x48_default, True)
            self.finish_splash_signal.emit()

        finally:
            self.finish_thread()

        return

    def init_checking(self):
        '''
           SUMMARY
           =======
           This method is considered to be a general method for starting the checks.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('The check is initialized')

        self.configure_logging_data()

        # First of all, we set up all default settings in the configuration object
        set_up_default_in_config(config_parser = self._kwargs.get('safe_config_parser'))

        # Second we try to create a temp file in the top level folder named profile
        self.create_temp_file(folder_path = path.join(self.BASE_PATH, 'profiles'),
                              profile_name = self._kwargs.get('profile_name', None),
                              custom_config_dict = self._kwargs.get('configuration_saver_dict'),
                              config_parser = self._kwargs.get('safe_config_parser'))

        theme = self._kwargs.get('configuration_saver_dict').dict_set_general_settings.get('SchemeColor')
        self._kwargs.get('image_path_collection').set_theme(theme = theme)

        self.finish_thread()

        return

    def finish_thread(self):
        '''
           SUMMARY
           =======
           This method is considered to be a general method for starting the checks.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Quit the thread.')

        self.quit_thread_signal.emit()
        self.wait_thread_signal.emit()

        return

def create_generally_crritcal_msg(image_path_collection):
    app = QApplication(argv )

    msg = custom_message_box(app, text = "Xarphus is currently running.",
                                  detail_text = "NOTICE:\n" \
                                  "Did you close Xarphus, but it is still running in Task Manager? There may be times, when the executable continues to run." \
                                  "\n"
                                  "In this case you have to open the Task Manager and kill the exe-file of Xarphus.",
                                  title = "Already running",
                                  icon = image_path_collection.warning_48x48_default,
                                  set_flag = True,
                                  title_icon = QIcon(image_path_collection.warning_32x32_default))

    #msg.setIcon(QMessageBox.Critical)

    setIc_OK = QIcon(image_path_collection.check_icon)

    msg.addButton(
        QPushButton(setIc_OK, " Ok"),
        msg.AcceptRole)  # An "OK" button defined with the AcceptRole
        #mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.
    msg.exec_()

    exit()

def main():

    about_app = info_app()

    image_path_collection = ImagePathnCollection(theme = 'default')

    file_prefix = '{program_name}_runs_'.format(program_name=about_app.dict_info["product_name"]).lower()
    file_suffixc = '.{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    file_extension = '{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    #MAIN_PATH = path.dirname(path.abspath(__file__))

    '''
        NOTICE:
        =======
        In general, the application will only be started once.

        We will create a temporary file (NamedTemporaryFile) to ensure that the application
        has been started only once. We use the context manage which ensures that the
        created temporary file is actually closed and thus immediately deleted.

        First, we take a look (find_files), if there is a temp file with a special extension.
        We get back a list. If there is such a file the returned list isn't empty - we know that.
        But, when the returned list is empty that means, this program has not been started.
        So we can start this program. On the other hand, we break the procedure.
    '''

    result_glob = find_files(file_path=BASE_PATH, file_extension=file_extension)

    if not result_glob:

        with NamedTemporaryFile(prefix=file_prefix, suffix=file_suffixc, dir=BASE_PATH) as temp:

            default_ini_file = DefaultINIFile()

            configuration_saver_dict = ConfigurationSaverDict()


            convert_current_time = get_time_now()

            LOG_FILE_PATH = path.join(BASE_PATH, 'log', convert_current_time + '.log')
            GENEREL_LOG_FOLDER_PATH = path.join(BASE_PATH, 'log')

            # We have to create an top folder for log files,
            # because we are able to save errors from the start
            # without knowing the user's profile.
            try:
                create_folder(folder_path = GENEREL_LOG_FOLDER_PATH)
            except:
                pass

            # And now we save the log file in the top folder named log
            try:
                configure_logging(LOG_FILE_PATH)
            except IOError:
                pass

            logger.info("Starting the program {exe_name} (Version: ({product_version})".format(product_version=about_app.dict_info["product_version"],
                                                                                               exe_name=about_app.dict_info["exe_name"]))

            try:
                #import qdarkstyle
                logger.info("Trying to create the App()-objectName")
                QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

                    # create the application if necessary
                app = None
                if ( not QApplication.instance() ):

                    app = App(default_ini_file = default_ini_file,
                              configuration_saver_dict = configuration_saver_dict,
                              image_path_collection = image_path_collection,
                              about_app = about_app,
                              parent = argv)

                    # setup stylesheet
                    #app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))
                    app.setQuitOnLastWindowClosed(False)

                    #   execute the application if we've created it
                    exit(app.exec_())

            except Exception as ex:
                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)
    else:
        #   Well we have a problem, the application has been started!
        #   Let us delete the found file.
        return_value = delete_file(file_list = result_glob)

        if not return_value:

            create_generally_crritcal_msg(image_path_collection = image_path_collection)



if __name__ == "__main__":
    main()
