# -*- mode: python -*-

block_cipher = None


a = Analysis(['xarphus_main.py'],
             pathex=['D:\\Dan\\Python\\Xarphus'],
             binaries=[],
             datas=[],
             hiddenimports=['requests', 'Pillow', 'configparser', 'setuptools', 'PyMySQL', 'SQLAlchemy', 'SQLAlchemy.dialects.mysql', 'sip', 'six', 'PyQt4.QtNetwork', 'PyQt4.QtCore','PyQt4.QtGui', 'PyQt4.uic'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='xarphus.exe',
          debug=True,
          strip=False,
          upx=True,
          console=True , 
		  icon='favicon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='xarphus')
