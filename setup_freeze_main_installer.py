# =============================================================================
#     Author: Sophus
#     Date:   Jul 09, 2015
#     Taken From: ''
#     File:  setup.py
#     Description: This is the cx_Freeze setup file for creating an exe program
# =============================================================================

 
# NOTE: you can include any other necessary external imports here aswell
import glob
import shutil
import sys
from xarphus.info import info_app
import sys
import os

from cx_Freeze import setup, Executable

import requests.certs

PROG_info = info_app()

base = None
# NOTE: when I am creating a GUI instead of a console app!
if sys.platform == "win32":
    base = "Win32GUI"#"Win32GUI"
else:
    base = 'Console'

excludes = []
includefiles = ['dll_for_exe/Microsoft.VC90.CRT/',
                r'LICENCE_GPL_v2.txt',
                r'D:\Dan\Python\xarphus\dll_for_exe\vcredist_x86.exe',
                r'D:\Dan\Python\xarphus\dll_for_exe\Microsoft.VC90.CRT\msvcm90.dll']

exe = Executable(
         # what to build
        script="xarphus_installer.py",
        initScript=None,
        compress=True,
        copyDependentFiles=True,
        appendScriptToExe=True,
        appendScriptToLibrary=True,
        icon = "favicon.ico",
        targetName="xarphus_installer.exe",
        base=base)

setup(
        name = "xarphus_installer",
        version = "0.0.1",
        description = "Install all updates of xarphus",
        author = PROG_info.dict_info["product_author"],
        author_email = "",

        options={'build_exe': {'excludes':excludes,
                               'include_files':includefiles,
                               'packages': [],
                               'path': sys.path,
                               'compressed':True,
                               'create_shared_zip':True,
                               'append_script_to_exe':False,
                               'include_in_shared_zip':True,
                               'optimize':2,
                               'build_exe':"cx_freeze_xarphus_installer",
                               }
                 },

        
        executables = [exe])

# Remove the build file_path_front_cover
shutil.rmtree("build", ignore_errors=True)
