# =============================================================================
#     Author: Sophus
#     Date:   Jul 09, 2015
#     Taken From: ''
#     File:  setup.py
#     Description: This is the cx_Freeze setup file for creating an exe program
# =============================================================================

 
# NOTE: you can include any other necessary external imports here aswell
import glob
import shutil
import sys
from xarphus.info import info_app
import sys
import os

from cx_Freeze import setup, Executable

import requests.certs

PROG_info = info_app()

# Remove the build folder
shutil.rmtree("build", ignore_errors=True)

# do the same for dist folder
shutil.rmtree("cx_freeze_installer", ignore_errors=True)

base = None
# NOTE: when I am creating a GUI instead of a console app!
if sys.platform == "win32":
    base = "Win32GUI"#"Win32GUI"
else:
    base = 'Console'



excludes = ['tkinter', 'PyQt4.QtSql', 'PyQt5', 'scipy.lib.lapack.flapack',
            'numpy.core._dotblas', 'sqlite3', 'tcl', '_gtkagg', 'Tkinter',
            '_tkagg', 'bsddb', 'curses', 'pywin.debugger', 'pywin.debugger.dbgcon',
            'pywin.dialogs', 'Tkconstants', 'PyQt4.Sql', '_ssl', 'pyreadline',
            'difflib', 'doctest', 'optparse', 'pickle']


##                                 
##                                  "PyQt4.QtNetwork",
##                                  "PyQt4.QtScript",
                                 

includefiles = ['dll_for_exe/Microsoft.VC90.CRT/',
                r'D:\Dan\Python\xarphus\dll_for_exe\vcredist_x86.exe',
                r'D:\Dan\Python\xarphus\dll_for_exe\Microsoft.VC90.CRT\msvcm90.dll',
                (requests.certs.where(),'cacert.pem')]

exe = Executable(
         # what to build
        script="installer.py",
        initScript=None,
        compress=True,
        copyDependentFiles=True,
        appendScriptToExe=True,
        appendScriptToLibrary=True,
        icon = "favicon.ico",
        targetName="installer" + ".exe",
        base=base)

setup(
        name = "installer",
        version = PROG_info.dict_info["product_version"],
        description = PROG_info.dict_info["product_description"],
        author = PROG_info.dict_info["product_author"],
        author_email = "",

        options={'build_exe': {'excludes':excludes,
                               'include_files':includefiles,
                               'packages': ['sqlalchemy.dialects.mysql', 'pymysql'],
                               'path': sys.path,
                               'compressed':True,
                               'create_shared_zip':True,
                               'append_script_to_exe':False,
                               'include_in_shared_zip':True,
                               'optimize':3,
                               'build_exe':"cx_freeze_installer",
                               }
                 },

        
        executables = [exe])

# Remove the build file_path_front_cover
shutil.rmtree("build", ignore_errors=True)

# Remove a *-dll-file
os.remove("cx_freeze_installer\QtWebKit4.dll")
