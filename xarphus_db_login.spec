# -*- mode: python -*-

block_cipher = None


a = Analysis(['D:\\Dan\\Python\\Xarphus\\xarphus\\frm_db_login.py'],
             pathex=['D:\\Dan\\Python\\Xarphus'],
             binaries=[],
             datas=[],
             hiddenimports=['pymysql', 'sqlalchemy', 'sqlalchemy.dialects.mysql', 'sip', 'six', 'PyQt4.QtNetwork', 'PyQt4.QtCore','PyQt4.QtGui', 'PyQt4.uic'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='db_login.exe',
          debug=True,
          strip=False,
          upx=True,
          console=True , 
		  icon='favicon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='db_login')
