import sys
from esky import bdist_esky
from distutils.core import setup

from esky.bdist_esky import Executable

executables = [Executable('installer.py', icon='favicon.ico', gui_only=True)]

setup(
    name = 'installer',
    version = '0.1',
    
    options = {"bdist_esky": {
                "freezer_module":"cx_freeze"
	      }},
    scripts = executables,
)

