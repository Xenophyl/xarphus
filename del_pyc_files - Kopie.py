##Use os.chdir to change directory . Use glob.glob to generate
##a list of file names which end it '.bak'. The elements of the
##list are just strings.
##
##Then you could use os.unlink to remove the files.
##(PS. os.unlink and os.remove are synonyms for the same function.)

import glob
import os
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
XARPHUS_INSTALLER_PATH = os.path.join(BASE_PATH, 'installer')
XARPHUS_INSTALLER_UI_PATH = os.path.join(BASE_PATH, 'installer', 'gui')
XARPHUS_INSTALLER_CORE_PATH = os.path.join(BASE_PATH, 'installer', 'core')
XARPHUS_INSTALLER_CSSTEMPLATE_PATH = os.path.join(BASE_PATH, 'installer', 'css_template')
XARPHUS_PATH = os.path.join(BASE_PATH, 'xarphus')
LANGUAGES_PATH = os.path.join(BASE_PATH, 'xarphus', 'languages')
IMAGES_PATH = os.path.join(BASE_PATH, 'xarphus', 'images')
CORE_PATH = os.path.join(BASE_PATH, 'xarphus', 'core')
CSS_PATH = os.path.join(BASE_PATH, 'xarphus', 'css_template')
LICENCE_PATH = os.path.join(BASE_PATH, 'xarphus', 'licence')
UI_PATH = os.path.join(BASE_PATH, 'xarphus', 'gui')
LOCALIZATION_FILE = os.path.join(BASE_PATH, 'xarphus', 'localization_files')

def delete_pyc_files(pyc_file):

    #   Map ist eine Funktion mit zwei Argumenten:
    #   Das erste Argument func ist eine Funktion und das zweite eine Sequenz (z.B. eine Liste) seq.
    #   map wendet die Funktion func auf alle Elemente von seq an und schreibt die Ergebnisse in eine neue Liste. 

    map(os.remove, glob.glob("*pyc"))


def iter_dict():
    path_list = [XARPHUS_INSTALLER_PATH,
                 XARPHUS_INSTALLER_UI_PATH,
                 XARPHUS_INSTALLER_CORE_PATH,
                 XARPHUS_INSTALLER_CSSTEMPLATE_PATH,
                 XARPHUS_PATH,
                 LANGUAGES_PATH,
                 IMAGES_PATH,
                 CORE_PATH,
                 CSS_PATH,
                 LICENCE_PATH,
                 UI_PATH,
                 LOCALIZATION_FILE
                 ]
    for element in path_list:
        delete_pyc_files(element)
        
def delete_pyc_xarphus_installer_core():
    os.chdir(XARPHUS_INSTALLER_CORE_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", XARPHUS_INSTALLER_UI_PATH,")"

def delete_pyc_xarphus_installer_css_template():
    os.chdir(XARPHUS_INSTALLER_CSSTEMPLATE_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", XARPHUS_INSTALLER_UI_PATH,")"   

def delete_pyc_xarphus_installer_ui():
    os.chdir(XARPHUS_INSTALLER_UI_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", XARPHUS_INSTALLER_UI_PATH,")"
    
def delete_pyc_xarphus_installer():
    os.chdir(XARPHUS_INSTALLER_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", XARPHUS_INSTALLER_PATH,")"
    
def delete_pyc_licence():
    os.chdir(LICENCE_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", LICENCE_PATH,")"
    
def delete_pyc_gui():
    os.chdir(UI_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", UI_PATH,")"

def delete_pyc_languages():
    os.chdir(LANGUAGES_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", LANGUAGES_PATH,")"

def delete_pyc_images():
    os.chdir(IMAGES_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", IMAGES_PATH,")"
    
def delete_pyc_core():
    os.chdir(CORE_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", CORE_PATH,")"
    
def delete_pyc_css():
    os.chdir(CSS_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", CSS_PATH,")"

def delete_pyc_xarphus():
    os.chdir(XARPHUS_PATH)
    files=glob.glob('*.pyc')
    for filename in files:
        os.unlink(filename)
    print "STATUS [OK] All *.pyc-files are deleted in directory (", XARPHUS_PATH,")"

def delete_all_pyc_files():
    iter_dict()
##    delete_pyc_xarphus()
##    delete_pyc_core()
##    delete_pyc_images()
##    delete_pyc_languages()
##    delete_pyc_licence()
##    delete_pyc_css()
##    delete_pyc_gui()
##    delete_pyc_xarphus_installer()
##    delete_pyc_xarphus_installer_ui()
##    delete_pyc_xarphus_installer_core()
##    delete_pyc_xarphus_installer_css_template()
    
if __name__ == '__main__':
    delete_all_pyc_files()
