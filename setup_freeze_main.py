# =============================================================================
#     Author: Sophus
#     Date:   Jul 09, 2015
#     Taken From: ''
#     File:  setup.py
#     Description: This is the cx_Freeze setup file for creating an exe program
# =============================================================================

 
# NOTE: you can include any other necessary external imports here aswell

import sys
from cx_Freeze import setup, Executable

import platform

import glob
import shutil
import zipfile
import sys
from xarphus.info import info_app
from zipping_dist import zip_dir
import os
import requests.certs

targetDir = "./build/"

PROG_info = info_app()

PROG_name = PROG_info.dict_info["product_name"].lower() + "_" + PROG_info.dict_info["product_version"]

# Remove the build folder
shutil.rmtree("build", ignore_errors=True)

# do the same for dist folder
shutil.rmtree(PROG_name, ignore_errors=True)

base = None
# NOTE: when I am creating a GUI instead of a console app!
if sys.platform == "win32":
    base = "Win32GUI"
else:
    base = 'Console'
 				 	
excludes = ['tkinter', 'PyQt4.QtSql', 'PyQt5', 'scipy.lib.lapack.flapack',
            'numpy.core._dotblas', 'sqlite3', 'tcl', '_gtkagg', 'Tkinter',
            '_tkagg', 'bsddb', 'curses', 'pywin.debugger', 'pywin.debugger.dbgcon',
            'pywin.dialogs', 'Tkconstants', 'PyQt4.Sql', '_ssl', 'pyreadline',
            'difflib', 'doctest', 'optparse', 'pickle', 'json', 'OpenSSL', 'queue', 
			'reprlib', 'simplejson', 'test', 'unittest', 'winreg', 'xml']


##                                 
##                                  "PyQt4.QtNetwork",
##                                  "PyQt4.QtScript",
                                 

includefiles = ['dll_for_exe/Microsoft.VC90.CRT/',
                #r'LICENCE_GPL_v2.txt',
                r'dll_for_exe\vcredist_x86.exe',
                r'dll_for_exe\Microsoft.VC90.CRT\msvcm90.dll',
                (requests.certs.where(),'cacert.pem')]

exe = Executable(
         # what to build
        script="xarphus_main.py",
        initScript=None,
        #compress=True,
        #copyDependentFiles=True,
        #appendScriptToExe=True,
        #appendScriptToLibrary=True,
        icon = "favicon.ico",
        targetName=PROG_info.dict_info["exe_name"] + ".exe",
		#targetDir=targetDir,
        base=base)

build_exe_options = {
     #"icon": "/assets/images/icon.ico",
     "build_exe": PROG_name,
     "packages": ['sqlalchemy.dialects.mysql', 'pymysql'], 
     "includes": ["re", "os", "atexit"],
     "include_files": includefiles,
     "excludes": excludes,
     "optimize": True,
     #"compressed": True,
     #"create_shared_zip":True,
     #"append_script_to_exe":False,
     #"include_in_shared_zip":True,
	 "path": sys.path
    }	

setup(
        name = PROG_info.dict_info["product_name"],
        version = PROG_info.dict_info["product_version"],
        description = PROG_info.dict_info["product_description"],
        author = PROG_info.dict_info["product_author"],
        author_email = "",

        options = {"build_exe": build_exe_options},

        
        #executables = [exe])

		executables = [ Executable("xarphus_main.py", 
                                   targetName="xarphus_main.exe",
                                   #targetDir=targetDir,
                                   base=base) ]
		)


# Remove a *-dll-file
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
DLL_PATH = os.path.join(BASE_PATH, PROG_name, 'QtWebKit4.dll')
os.remove(DLL_PATH)

# Zipping the frozen app-folder
zipf = zipfile.ZipFile(PROG_name+".zip", 'w', zipfile.ZIP_DEFLATED)
zip_dir(PROG_name, zipf)

# Remove the build file_path_front_cover
shutil.rmtree("build", ignore_errors=True)
shutil.rmtree(PROG_name, ignore_errors=True)
