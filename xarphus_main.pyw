#!/usr/bin/env python
#-*- coding:utf-8 -*-

# This is only needed for Python v2 but is harmless for Python v3.
import sip

try:
    sip.setapi('QVariant', 2)
except ValueError:
    sip.setapi('QVariant', 1)

FILE_NAME = "xarphus_main.py"

#   Import all Built-in Functions
from sys import argv, exc_info, exit
from time import sleep, time
from traceback import format_exc
import argparse
from glob import glob
from os import path, chdir, error
from tempfile import NamedTemporaryFile
#    Import all PyQt bindings
from PyQt4.QtGui import QApplication, QPixmap, qApp, QSplashScreen, QProgressBar, QStyleFactory, \
    QMessageBox, QWizard, QLabel
from PyQt4.QtCore import Qt, QFile, QTextStream, QTranslator, QString, QCoreApplication, QEvent
from PyQt4 import QtCore
from PyQt4.uic import loadUi

#   Import all forms
from xarphus.frm_mdi import MDI_Window

from xarphus.frm_error_wizard import MainWizard

#   Import core codes
from xarphus.core.config import ConfigurationSaverDict
from xarphus.core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile
from xarphus.core.manage_logging import configure_logging
from xarphus.core.manage_time import get_time_now
from xarphus.core import manage_folder
from xarphus.core.manage_file import find_files
from xarphus.info import info_app
from xarphus.core.set_settings import to_set_general_settings

from xarphus.languages.german import Language


from xarphus.collection_paths_image import ImagePathnCollection

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

BASE_PATH = path.dirname(path.abspath(FILE_NAME))
##BASE_PATH = os.path.dirname(os.path.abspath(FILE_NAME))
##SETTING_FOLDER_PATH = os.path.join(BASE_PATH, 'settings')
##CONFIG_PATH = os.path.join(BASE_PATH, 'settings', 'config.ini')
##ZIP_FILE_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
##LINCENCE_PATH = os.path.join(BASE_PATH, 'LICENCE_GPL_v2.txt')
##LOG_FOLDER_PATH = os.path.join(BASE_PATH, 'log')

class App(QApplication):
    def __init__(self, default_ini_file,
                 configuration_saver_dict,
                 about_app,
                 image_path_collection,
                 parent=None):
        QApplication.__init__(self, parent)

        self.default_ini_file = default_ini_file
        self.configuration_saver_dict = configuration_saver_dict
        self.about_app = about_app
        self.image_path_collection = image_path_collection

        self.mdi_widget = None

        #parser = argparse.ArgumentParser()
        #parser.add_argument('-i','--input', help='Input file name',required=True)
        #args = parser.parse_args()
        #print "Input file: %s" % args.input 


        self.BASE_PATH = path.dirname(path.abspath(FILE_NAME))
        self.SETTING_FOLDER_PATH = path.join(self.BASE_PATH, 'settings')
        self.CONFIG_PATH = path.join(self.BASE_PATH, 'settings', 'config.ini')
        self.ZIP_FILE_PATH = path.join(self.BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
        self.LOG_FOLDER_PATH = path.join(self.BASE_PATH, 'log')    

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.get_execute_file()
        
        self.create_splash_screen()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''
        logger.info("Returning the name of the current class")
        return self.__class__.__name__

    def get_execute_file(self):
        EXECUTE_FILE_PATH = ""
        BASE_PATH = path.join(self.BASE_PATH)
        chdir(BASE_PATH)
        
        for exe_file in glob("*.exe"):
            EXECUTE_FILE_PATH = BASE_PATH = path.join(self.BASE_PATH, exe_file)

        self.configuration_saver_dict.dict_set_general_settings["PathToExecuteFile"] = EXECUTE_FILE_PATH
            

    def load_translator_file(self):
        '''
            NOTICE:
            =======
            This method is used to load and install the QTranslator()-object.
            Returns TRUE if the file is loaded successfully; otherwise returns FALSE.
            That means it doesn't matter if the tranlte file could be loaded, 
            the program continues without error. This method will display 
            the original language on the screen: its English. 
            This is the reason why no exception is handled.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''        
        logger.info("Creating an instance of the class of QTranslator()")
        
        self.translator = QTranslator()

        logger.info("The instance of the class of QTranslator() is created successfully")

        file_extension = ".qm"
        path = ":/translators/" + self.configuration_saver_dict.dict_set_general_settings["language"]+ file_extension

        logger.info("Installing the QTranslator()-object")
        
        self.installTranslator(self.translator)

        logger.info("The translator is installed successfully")
        
        logger.info("Loading translate file for translation")

        self.translator.load(path)
        
        logger.info("Translate file is loaded successfully")
        
        return
        
    def load_stylesheet(self):
        '''
            NOTICE:
            =======
            This method is used to load and set the Style Sheet.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''      

        logger.info("Reading and setting the style sheet")
        
        '''
            First: Get the current css stylesheet and setup stylesheet
            Here its takes a look into the dictionary and checks 
            if the value tied to 'key' (SchemeColor) evaluates to not True, 
            do nothing -just pass", because the value is empty.
        '''
        if  not self.configuration_saver_dict.dict_set_general_settings["SchemeColor"] or self.configuration_saver_dict.dict_set_general_settings["SchemeColor"].lower() == "none":
            pass

        else:
            '''
                The value tied to 'key' (SchemeColor) evaluates to True.
            '''
            css_file_extension = ".qss"
            scheme_color = self.configuration_saver_dict.dict_set_general_settings["SchemeColor"].lower()      
            
            css_path = ":/{scheme_color}/{scheme_color}.{css_file_extension}".format(scheme_color=scheme_color,
                                                                css_file_extension=css_file_extension) 

            '''
                Creating a constructor. It is an I/O device for reading and writing text.
                The variable as a file name (css_path) is passed in the constructor
            '''
            fd = QtCore.QFile(css_path)
            fd.open(QtCore.QIODevice.ReadOnly | QtCore.QFile.Text)

            ''' 
                For reading and writting the data it is using QTextStream.
            '''
            style = QtCore.QTextStream(fd).readAll()
            fd.close()

            '''
                Setting the style sheet.
            '''
            self.setStyleSheet(style)
        
        return

    def load_style(self):
        '''
            NOTICE:
            =======
            This method is used to load and set the style of QStyleFactory()-object.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        ''' 
        logger.info("Load and set the GUI-style")

        style_type = None

        '''
            Here its checks if the value tied to key (GUIStyle) evaluates not to True, that means its empty, 
            than do nothing here. Set the standard style.
        '''
        if  not self.configuration_saver_dict.dict_set_general_settings["GUIStyle"] or self.configuration_saver_dict.dict_set_general_settings["GUIStyle"].lower() == "automatically":
            style_type = qApp.style().objectName()
        else:
            '''
                The value tied to key (GUIStyle) evaluates to True - isn't empty
            '''
            style_type = self.configuration_saver_dict.dict_set_general_settings["GUIStyle"]

        '''
            Finally its styles the GUI.
        '''
        self.setStyle(QStyleFactory.create(style_type))

        return

    def string_converter(self, convert_int):
            try:
                function_dict = {'0': 0,
                                 '1': 1,
                                 '2': 2,
                            }
                return function_dict[convert_int]
            except Exception as ex:
                print ex


    def create_no_ini_file_window(self):
        '''
            NOTICE:
            =======
            This method is used to create an Wizard-instance.
            For example the user starts this programs at the first time
            or the ini file, where all settings are saved, is empy
            or damaged, the user will get a Wizard-window - is just
            like a assistant-window.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        ''' 
        
        logger.info("Creating an instance of the class of MainWizard()")
        
        '''
            Set the buttons in a sequence and safe it in a list
        '''
        btnList = [QWizard.Stretch, QWizard.CancelButton, QWizard.BackButton,
                       QWizard.NextButton, QWizard.FinishButton]
        
        self.main_wizard = MainWizard(btnList,
                         self.configuration_saver_dict,
                         self.about_app,
                         logger,
                         self.default_ini_file)
        
        self.main_wizard.show()

        return
            

    def create_mdi_window(self):
        '''
            NOTICE:
            =======
            This method is used to create an MDI-instance
            and show this as a window.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        ''' 

        try:
            logger.info("Trying to an instance of the class of MDI_Window()")
            window = MDI_Window(self.configuration_saver_dict,
                                self.about_app,
                                self.image_path_collection,
                                self.default_ini_file)
            window.showMaximized()

            self.mdi_widget = window

        except Exception as ex:
            print ex
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def create_error_messagebox(self):
        '''
            NOTICE:
            =======
            This method is used to create a message box it is shows
            the error text. In this case when the ini file is missing,
            empty or is damage the user is shown the window, 
            which indicates that a file is missing, is empty, or is damage. 

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        ''' 

        w = self.mdi_widget
        
        QMessageBox.critical(w, self.tr("Error"), self.tr("The INI-file appears to have "
                                         "been damaged or modified. You "
                                         "have the ability record a "
                                         "secure INI-file or create a new "
                                         "INI-file."))

        self.create_no_ini_file_window()

        return

    def open_ini_file(self, splash_screen_obj):
        
        with open(self.CONFIG_PATH, 'r') as ini_file:
            try:
                '''
                Ok, the ini file already exists. Next stept: let's check wheter the ini file is empty.
                '''
                result_file_size = file_is_empty(self.CONFIG_PATH)
                if result_file_size:
                    '''
                    The boolean True means, that ini file is empty
                    '''
                    splash_screen_obj.finish(None)
                    self.create_error_messagebox()
                    
                else:
                    '''
                    The ini file isn't empty.
                    '''
                    #TODO: In this case I have to validate the ini file wether all OPtIONS and SECTIONS are fine
                    get_configuration(self.CONFIG_PATH, self.configuration_saver_dict, self.default_ini_file)

                    # convert_current_time = get_time_now()

                    # self.LOG_FILE_PATH = os.path.join(self.BASE_PATH, 'log', convert_current_time + '.log')

                    # configure_logging(self.LOG_FILE_PATH)

                    # logger.info(self.about_app.dict_info["exe_name"] + " Version (" + self.about_app.dict_info["product_version"] + ")")

                    self.load_translator_file()
                    
                    self.load_stylesheet()
                    self.load_style()
                    
                    '''
                    Let us finish the splash scrren, remove the event filter and start the MDI-Form
                    '''
                    splash_screen_obj.finish(self.create_mdi_window())
                    self.remove_event_filter(splash_screen_obj)

            except:
                desired_trace = format_exc(exc_info())
                print "desired_trace:", desired_trace

    def create_missing_folders(self, base_path):
        path = manage_folder.generator_top_sub_folder(base_path)
        for i in path:
            print "i", i
            try:
                manage_folder.mkdir_top_sub_folder(i)
            except error, e:
                print "e", e
                pass

    def create_splash_screen(self):

        logger.info("Create splash screen")
        
        self.configuration_saver_dict.dict_set_general_settings["BasePath"] = self.BASE_PATH

        #self.create_missing_folders(self.BASE_PATH)

        to_set_general_settings(self.CONFIG_PATH, self.SETTING_FOLDER_PATH, self.ZIP_FILE_PATH,
                                             self.configuration_saver_dict)

        

        # Create and display the splash screen
        SplashPic = ":/img_512x512_default/default/img_512x512/splash_loading.png"
        splash_pix = QPixmap(SplashPic)
        splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)

        splash.installEventFilter(self)

        copy_right_content = "{product_copyright_characters} {product_copyright} 2014-2016 {product_by} {product_author}".format(product_copyright_characters=self.about_app.dict_info["product_copyright_characters"],
                                                                                       product_copyright=self.about_app.dict_info["product_copyright"],
                                                                                       product_by= self.about_app.dict_info["product_by"],
                                                                                       product_author= self.about_app.dict_info["product_author"],
                                                                                       exe_name=self.about_app.dict_info["exe_name"],
                                                                                       product_version=self.about_app.dict_info["product_version"]
                                                                                       )

        version_content = "{exe_name} Version ({product_version})".format(exe_name=self.about_app.dict_info["exe_name"],
                                                                          product_version=self.about_app.dict_info["product_version"]
                                                                                       )


        copy_right_label = QLabel(splash)
        copy_right_label.setAlignment(Qt.AlignRight)
        copy_right_label.setText(copy_right_content)
        copy_right_label.setStyleSheet('QLabel{color: #778899;}')


        version_label = QLabel(splash)
        version_label.setAlignment(Qt.AlignRight)
        version_label.setText(version_content)
        version_label.setStyleSheet('QLabel{color: #778899;}')

        # add a Label to the splash screen
        #copy_right_label.setGeometry(splash.width()/3.1, 8.55*splash.height()/10,
        #                            9.3*splash.width()/9.9, splash.height()/18)
        # First: Left, right
        # Second: up, down 
        version_label.move(375, 10)
        copy_right_label.move(330, 280)
        
        # add a progressbar to the splashscreen
        progressBar = QProgressBar(splash)
        progressBar.setAlignment(Qt.AlignCenter)
        #                             # at first, the width of progressbar
        #                                                 # here the height of progressbar
        progressBar.setGeometry(splash.width()/56, 9.3*splash.height()/10,
                                    9.3*splash.width()/9.9, splash.height()/18)

        #
        splash.setMask(splash_pix.mask())
        splash.show()

        for i in range(0, 100):
            progressBar.setValue(i)
            t = time()
            while time() < t + 0.1:
                self.processEvents()

            self.processEvents()

        self.processEvents()
        # # Simulate something that takes time
        sleep(1)

        try:
            self.open_ini_file(splash)
        except (IOError, OSError):
            logger.warning("There aren't a ini-file - (" +  FILE_NAME +")")
            
            logger.info("Finish splash screen - (" +  FILE_NAME +")")
            splash.finish(self.create_no_ini_file_window())
            logger.info("Splash screen is finished successfully- (" +  FILE_NAME +")")

        return


    def eventFilter(self, obj, event):
        '''
            NOTICE:
            =======
            This eventFilter()-function disables the mouse click event - right and left.

            During the QSplashScreen()-class is showing for a while the user can't
            click the SplashScreen to make it disappear. Without this event filter when the
            user clicks the SplashScreen it disappears until the main window shows up.
            Thats why we disable the possibility of clicking on the splash screen.

            The eventFilter() function must return true if the event should be filtered, 
            (i.e. stopped); otherwise it must return false.

            PARAMETERS:
            ===========
            :obj        -

            :event      -

            :return     -   Call Base Class Method to Continue Normal Event Processing.

        '''
        if event.type() in (QEvent.MouseButtonPress,
                            QEvent.MouseButtonDblClick):
            if event.button() == Qt.LeftButton:
                return True
            if event.button() == Qt.RightButton:
                return True

        return QApplication.eventFilter(self, obj, event)

    def remove_event_filter(self, widget):
        '''
            NOTICE:
            =======
                This method removes the installed filter event.

                We don't need this filter event anymore.

            PARAMETERS:
            ===========
                :widget     -       Here we except an given widget from 
                                    where the eventFilters should be removed.

                :return     -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        
        widget.removeEventFilter(self)

        return
    
def create_missing_folders(base_path):
    path = manage_folder.generator_top_sub_folder(base_path)

    for i in path:
        try:
            manage_folder.mkdir_top_sub_folder(i)
        except error, e:
            pass

def main():

    about_app = info_app()

    file_prefix = '{program_name}_runs_'.format(program_name=about_app.dict_info["product_name"]).lower()
    file_suffixc = '.{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    file_extension = '{program_name}'.format(program_name=about_app.dict_info["product_name"]).lower()

    #MAIN_PATH = path.dirname(path.abspath(__file__))

    '''
        NOTICE:
        =======
        In general, the application will only be started once. 

        We will create a temporary file (NamedTemporaryFile) to ensure that the application 
        has been started only once. We use the context manage which ensures that the 
        created temporary file is actually closed and thus immediately deleted. 

        First, we take a look (find_files), if there is a temp file with a special extension.
        We get back a list. If there is such a file the returned list isn't empty - we know that.
        But, when the returned list is empty that means, this program has not been started.
        So we can start this program. On the other hand, we break the procedure.
    '''

    result_glob = find_files(file_path=BASE_PATH, file_extension=file_extension)

    if not result_glob:

        with NamedTemporaryFile(prefix=file_prefix, suffix=file_suffixc, dir=BASE_PATH) as temp:

            default_ini_file = DefaultINIFile()

            configuration_saver_dict = ConfigurationSaverDict()
            
            image_path_collection = ImagePathnCollection()
            
            create_missing_folders(BASE_PATH)

            convert_current_time = get_time_now()

            LOG_FILE_PATH = path.join(BASE_PATH, 'log', convert_current_time + '.log')

            configure_logging(LOG_FILE_PATH)

            logger.info("Starting the program {exe_name} (Version: ({product_version})".format(product_version=about_app.dict_info["product_version"],
                                                                                               exe_name=about_app.dict_info["exe_name"]))
            
            try:
                import qdarkstyle
                logger.info("Trying to create the App()-objectName")
                QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))
                app = App(default_ini_file,
                          configuration_saver_dict,
                          about_app,
                          image_path_collection,
                          argv)
                # setup stylesheet
                #app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=False))
                app.setQuitOnLastWindowClosed(False)
                exit(app.exec_())

            except Exception as ex:
                desired_trace = format_exc(sys.exc_info())
                logger.error(desired_trace)
    else:
        pass



if __name__ == "__main__":
    main()
