"""create account table

Revision ID: f2049ffc0442
Revises: 
Create Date: 2017-05-26 00:49:00.917000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2049ffc0442'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'genre_film',
        sa.Column('description', sa.Unicode(200)),
    )


def downgrade():
    op.drop_table('genre_film')
