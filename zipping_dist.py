import zipfile
import os

def zip_dir(path, ziph):
    print ""
    print "zipping file"
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))

    return
