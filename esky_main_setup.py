import sys
from esky import bdist_esky
from distutils.core import setup

from esky.bdist_esky import Executable

executables = [Executable('xarphus_main.py', icon='favicon.ico', gui_only=True)]

setup(
    name = 'xarphus',
    version = '0.1',
    
    options = {"bdist_esky": {
                "freezer_module":"cx_freeze"
	      }},
    scripts = executables,
)

