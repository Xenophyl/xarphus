##Use os.chdir to change directory . Use glob.glob to generate
##a list of file names which end it '.bak'. The elements of the
##list are just strings.
##
##Then you could use os.unlink to remove the files.
##(PS. os.unlink and os.remove are synonyms for the same function.)

import glob
import os
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
XARPHUS_INSTALLER_PATH = os.path.join(BASE_PATH, 'installer')
XARPHUS_INSTALLER_UI_PATH = os.path.join(BASE_PATH, 'installer', 'gui')
XARPHUS_INSTALLER_CORE_PATH = os.path.join(BASE_PATH, 'installer', 'core')
XARPHUS_INSTALLER_CSSTEMPLATE_PATH = os.path.join(BASE_PATH, 'installer', 'css_template')
XARPHUS_PATH = os.path.join(BASE_PATH, 'xarphus')
LANGUAGES_PATH = os.path.join(BASE_PATH, 'xarphus', 'languages')
IMAGES_PATH = os.path.join(BASE_PATH, 'xarphus', 'images')
CORE_PATH = os.path.join(BASE_PATH, 'xarphus', 'core')
CSS_PATH = os.path.join(BASE_PATH, 'xarphus', 'css_template')
LICENCE_PATH = os.path.join(BASE_PATH, 'xarphus', 'licence')
UI_PATH = os.path.join(BASE_PATH, 'xarphus', 'gui')
LOCALIZATION_FILE = os.path.join(BASE_PATH, 'xarphus', 'localization_files')

def delete_pyc_files(pyc_file):
    os.chdir(pyc_file)

    #   Map ist eine Funktion mit zwei Argumenten:
    #   Das erste Argument func ist eine Funktion und das zweite eine Sequenz (z.B. eine Liste) seq.
    #   map wendet die Funktion func auf alle Elemente von seq an und schreibt die Ergebnisse in eine neue Liste. 

    map(os.unlink, glob.glob("*pyc"))

def iter_dict():
    path_list = [XARPHUS_INSTALLER_PATH,
                 XARPHUS_INSTALLER_UI_PATH,
                 XARPHUS_INSTALLER_CORE_PATH,
                 XARPHUS_INSTALLER_CSSTEMPLATE_PATH,
                 XARPHUS_PATH,
                 LANGUAGES_PATH,
                 IMAGES_PATH,
                 CORE_PATH,
                 CSS_PATH,
                 LICENCE_PATH,
                 UI_PATH,
                 LOCALIZATION_FILE
                 ]
    for element in path_list:
        ## try to delete file ##
        try:
                delete_pyc_files(element)
        except OSError, e:  ## if failed, report it back to the user ##
                print ("Error: %s - %s." % (e.filename,e.strerror))
    print "Done! All files are deleted."
        
        
def delete_all_pyc_files():
    iter_dict()
    
if __name__ == '__main__':
    delete_all_pyc_files()
