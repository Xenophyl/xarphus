# A DESCRIPTION OF ``xarphus`` #
---------------------------
### What is ``xarphus``? ###

In short, it is a management system.

What exactly do you mean?

Detailed: the idea of xarphus is you can manage almost everything what you want.
Currently the following media, to be managed by xarphus be contemplated:

* book (magazine / comic / news)
* music (alben / single) 
* movie (movie / serie)
* stamps
* video games
* quotations
* notes

... and maybe?

* wine
* Model railway

... (More ideas will follow)

You can see xarphus isn't designed only for movies, but also for other matters and media.
I couldn't find anything on the Web that would suit all my needs to manage all my collections. 
You can find on the web a lot of programs that manage only one category: either only movies, only 
stamps, only videogames, just books etc. For each collection you have to purchase and install 
another program. At the end you still haven't removed your chaos - you've made it worse. Wouldn't 
it be better to manage everything in one program? Moreover, I thought it was repeatedly annoying 
that you couldn't reasonably and orderly manage the media. There is one more thing which is also 
very annoying me: Many programs claim they are a management program, while they are much more 
likely catalogs.

Next:

There also shall be possible that you can select your database. It's your choice. You can save your 
data on a MySQL server, on a Microsoft server, on an access database, on a postgreSQL or on a SQLite.

[Website](http://www.xarphus.de)
