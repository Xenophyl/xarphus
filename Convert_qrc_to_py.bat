rem *** Used to convert a resource file (.qrc) in python-module

rem ***** create the module: images.qrc --> images_rc.py
 
pyrcc4 -o xarphus\images\images_rc.py xarphus\images\images.qrc
 
rem ***** DONE


rem *** create the module: ui.qrc --> ui_rc.py 

pyrcc4 -o xarphus\gui\ui_rc.py xarphus\gui\ui.qrc
 
rem ***** DOME

rem *** create the module: licence.qrc --> licence_rc.py 

pyrcc4 -o xarphus\licence\licence_rc.py xarphus\licence\licence.qrc
 
rem ***** DOME

rem *** create the module: ui.qrc --> ui_rc.py 

pyrcc4 -o installer\gui\ui_rc.py installer\gui\ui.qrc
 
rem ***** DOME

rem *** create the module: qt_css_style.qrc --> qt_css_style_rc.py 

pyrcc4 -o xarphus\css_template\qt_css_style_rc.py xarphus\css_template\qt_css_style.qrc

rem ***** DOME

rem *** create the module: translate_files.qrc --> translate_files_rc.py 

pyrcc4 -o xarphus\localization_files\translate_files_rc.py xarphus\localization_files\translate_files.qrc 

rem ***** DOME

rem **** pause so we can see the exit codes

pause "done...hit a key to exit"
