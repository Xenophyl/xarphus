#-*- coding:latin1 -*-

FILE_NAME = __name__

import os
import sys

from PyQt4.QtCore import Qt, QRegExp

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QIcon, QFileDialog, \
    QPixmap, QValidator, QRegExpValidator, QTabWidget, QGridLayout, QTextEdit, QToolButton, QSpacerItem, \
    QSizePolicy

from xarphus.frm_pushbutton_save_close import PushbuttonSaveClose_Window
from xarphus.frm_label_explain_master_data import LabelExplainMasterData_Window
from xarphus.frm_master_data import MasterData_Window
from xarphus.frm_one_line_edit import LineEditOne_Window

from xarphus.frm_ratings import AddRatings_Window
from xarphus.frm_combi_land import ComboLand_Window

from xarphus.core.manange_logging import set_custom_logger

from xarphus.frm_movie_general_cover import MovieGeneralCover_Window

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineRatingsClassification_Window(QWidget):
    def __init__(self, func_up_to_date,
                         update_sublist,
                         info_app,
                         windows_title,
                         rating_type,
                         parent):

        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.info_app = info_app
        self.windows_title = windows_title
        self.rating_type = rating_type

        self.custom_logger = set_custom_logger(self.info_app)

        self.setWindowTitle(self.windows_title)
#        self.setWindowIcon('')

        self.film_title_name = ""

        ## activate all necessary functions
        self.chosen_form(self.rating_type)


    def chosen_form(self, chosen):
        dict_forms = {"general_rating": self.create_general_rating,
                      "classification_rating": self.create_classification_rating}

        dict_forms[chosen]()

    def create_general_rating(self):
        print "create_general_rating"
        self.custom_logger.info(
        "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")
        self.custom_logger.info("--"*10)
        self.custom_logger.info("The instance of the class of MovieGeneral_Window() is creating")
        self.frm_add_rating = AddRatings_Window(self.func, self.up_to_date_sublist, self.info_app, self)

        self.custom_logger.info(
        "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")
        self.custom_logger.info("--"*10)
        self.custom_logger.info("The instance of the class of MovieGeneral_Window() is creating")
        self.frm_combo_land = ComboLand_Window(self.func, self.up_to_date_sublist, self.info_app, self)

        v_layout = QVBoxLayout()
        v_layout.addWidget(self.frm_add_rating)
        v_layout.addWidget(self.frm_combo_land)
        #v_layout.addWidget(self.frm_line_edit_one)
        #v_layout.addWidget(self.frm_pushbutton_save_close)
        self.setLayout(v_layout)


        h_1 = self.frm_add_rating.height()
        h_2 = self.frm_combo_land.height()
        h_4 = ((h_1) + (h_2 * 1.5))

        self.resize(self.frm_add_rating.width(), h_4)

        self.frm_add_rating.verticalLayout_3.addWidget(self.frm_combo_land)
        self.init_general_pushbuttons()

    def create_classification_rating(self):
        print "create_general_rating"
        self.custom_logger.info(
        "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")
        self.custom_logger.info("--"*10)
        self.custom_logger.info("The instance of the class of MovieGeneral_Window() is creating")
        self.frm_add_rating = AddRatings_Window(self.func, self.up_to_date_sublist, self.info_app, self)

        v_layout = QVBoxLayout()
        v_layout.addWidget(self.frm_add_rating)
        self.setLayout(v_layout)


        h_1 = self.frm_add_rating.height()
        h_4 = ((h_1 * 1.190))

        self.resize(self.frm_add_rating.width(), h_4)

        self.init_general_pushbuttons()

    def get_class_name(self):
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")
        return self.__class__.__name__


    def init_general_pushbuttons(self):
        self.frm_add_rating.pushButton_close.clicked.connect(self.close_form)


    def closeEvent(self, event):
        pass

    def close_form(self):
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")