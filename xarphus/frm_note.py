#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt, QTranslator
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QApplication

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class Note_Window(QWidget):
    def __init__(self):

        QWidget.__init__(self, parent=None)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/note.ui")


        self.load_ui_file(UI_PATH)
        self.init_clicked_signal_push_buttons()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_note = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")
        self.ui_note.pushButton_close.clicked.connect(self.close_form)

    def closeEvent(self, event):
        pass

    def close_form(self):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_note.close()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication

    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now

    from core.manage_logging import configure_logging
   
    '''
        Import all compiled files
    '''
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    dict_custom_config = ConfigurationSaverDict()

    convert_current_time = get_time_now()

    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    configure_logging(LOG_FILE_PATH)

    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Note_Window()

    window.showMaximized()
    sys.exit(app.exec_())
