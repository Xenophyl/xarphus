#!/usr/bin/env python2
#-*- coding:latin1 -*-

try:
    from xarphus.info import info_app
except ImportError:
    from info import info_app
#--------------------------------------------------------------------------------------

class Language(object):

#--------------------------------------------------------------------------------------
        about_app = info_app()
#--------------------------------------------------------------------------------------
##        dict_manager_info = {
##                    "movie": "Filme, Serien",
##                    "books": "B�cher, Comic-Hefte, Zeitschriften, Zeitungen",
##                    "musics": "Schallplatten, Alben, Singles, Einzellieder",
##                    "coins": "M�nzen",
##                    "contacts": "Kontakte",
##                    "notes": "Notizen",
##                    "videogames": "Videospiele",
##                    "quotations": "Zitate",
##                    "stamps": "Briefmarken",
##                    "idioms": "Redensarten"
##        }
        dict_ui_mdi = {

                                # QLabel in Statusbar
                                "m_statusMiddle": "Database: "
        }
#--------------------------------------------------------------------------------------
        dict_mdi_toolbar = {
                            "newExit" : about_app.dict_info["product_name"] + "beenden",
        }
#--------------------------------------------------------------------------------------
        # dict_msgBox_unload_mdi = {
        #                 "msgBox_unload_mdi_title" : about_app.dict_info["product_name"] + "schlie�en...",
        #                 "msgBox_unload_mdi.yes" : "Ja",
        #                 "msgBox_unload_mdi.no" : "Nein",
        #                 "msgBox_unload_mdi.cancel" : "Abbrechen",
        #                 "msgBox_unload_mdi_text" : "M�chten sie das Programm beenden?"
        # }
#--------------------------------------------------------------------------------------
        # dict_msgBox_restart = {
        #                 "msgBox_restart_title" : about_app.dict_info["product_name"] + "neustarten...",
        #                 "msgBox_restart.yes" : "Ja",
        #                 "msgBox_restart.no" : "Nein",
        #                 "msgBox_restart.cancel" : "Abbrechen",
        #                 "msgBox_restart_text" : "Beim Neustart werden alle Vorg�nge abgebrochen. M�chten Sie " + about_app.dict_info["product_name"] + " neustarten?"
        # }
#--------------------------------------------------------------------------------------
        dict_msgBox_error = {
                        "msgBox_error_title": " Fehler ",
                        "msgBox_error.yes": " Ok",
                        "msgBox_error_text": "Die R�ckmeldung konnte nicht gesendet werden. \n"
                                             "Es wurden nicht alle Textfelder korrekt ausgef�llt."
        }
#--------------------------------------------------------------------------------------
        # dict_ui_movie = {
        #                 "movie_title": " Film ",
        #                 "label_genre": "Genre",
        #                 "label_color_format": "Farb-Format",
        #                 "label_original_title": "Originaltitle",
        #                 "label_translate_title": "(�bersetzter) Titel",
        #                 "label_production_year": "Produktionsjahr",
        #                 "label_country_of_production": "Herstellungsland",
        #                 "label_total_evaluation": "Gesamtbewertung",
        #                 "tabWidget_details": " Detaillierte Information(en) ",
        #                 "tabWidget_more_detail_cast_crew": " Besetzung / Filmstab ",
        #                 "tabWidget_more_detail_sync": " Synchronisation ",
        #                 "tabWidget_more_detail_production": " Produktion / Filmstudio ",
        #                 "tabWidget_more_detail_soundtrack": " Filmmusik ",
        #                 "tabWidget_more_detail_content": " Inhaltsangabe ",
        #                 "tabWidget_more_detail_comment": " Kommentar ",
        #                 "tabWidget_more_detail_review": " Rezension ",
        #                 "tabWidget_cover_front": " Vorderseite ",
        #                 "tabWidget_cover_back": " R�ckseite ",
        #                 "tabWidget_version_edition": " Fassung(en) / Edition(en) ",
        #                 "groupBox_general_info": " Allgemeine Information(en) ",
        #                 "groupBox_film_cast": " Besetzung ",
        #                 "groupBox_crew": " Filmstab ",
        #                 "groupBox_voice_actor": " SynchronsprecherInnen ",
        #                 "groupBox_film_production_company": " Produktionsfirmen ",
        #                 "groupBox_filmstudio": " Filmstudio ",
        #                 "groupBox_film_version": " Version / Fassung ",
        #                 "groupBox_film_edition": " Edition / Ver�ffentlichung ",
        #                 "groupBox_cover": " DVD-H�lle ",
        #                 "pushButton_close": "Schlie�en",
        #                 "pushButton_print_film": "Drucken",
        #                 "pushButton_save_film": "Speichern",
        #                 "pushButton_delete_film": "L�schen",
        #                 #"star_one": "Mangelhaft",
        #                 #"star_two": "Ausreichend",
        #                 #"star_three": "Befriedigend",
        #                 #"star_four": "Gut",
        #                 #"star_five": "Sehr gut",
        # }
#--------------------------------------------------------------------------------------
        # dict_ui_register = {
        #                     "register_title": " Registrierung ",
        #                     "labelRegisterExplain": "Sie k�nnen sich ein kostenloses Konto anlegen.",
        #                     "groupBoxRegister": " Informationen f�r die Registrierung ",
        #                     "labelNickname": "Benutzername:",
        #                     "labelPassword": "Passwort:",
        #                     "labelPasswortConfirm": "Passwort wiedeholen:",
        #                     "labelMailAddress": "E-Mail-Adresse:",
        #                     "labelMailAddressConfirm": "E-Mail-Adresse wiederholen:",
        #                     "labelGeneralTerms": "Indem du auf <b>Registrieren</b> klickst, erkl�rst du dich "
        #                                 "mit unseren <b>Nutzungsbedingungen</b> einverstanden und best�tigst, "
        #                                 "dass du unsere <b>Datenrichtlinie</b> gelesen hast..",
        #                     "pushButtonClose": " Schlie�en ",
        #                     "pushButtonRegist": " Registrieren ",
        #                     "pushButtonReset": " Zur�cksetzen "
        # }
#--------------------------------------------------------------------------------------
        # dict_ui_customer_login = {
        #                     "customer_login_title": " Anmeldung ",
        #                     "label_login_explain": "Mit Ihrem Konto anmelden.",
        #                     "groupBox_logIn": " Daten f�r die Anmeldung ",
        #                     "label_nickname": "Benutzername:",
        #                     "label_password": "Passwort:",
        #                     "pushButton_logIn": " Anmelden",
        #                     "pushButton_close": " Schlie�en"
        # }
#--------------------------------------------------------------------------------------
        # dict_ui_feedback = {
        #                     "feedback_title": " R�ckmeldung ",
        #                     "labelFeedbackExplain": "Kritik und Verbesserungsvorschl�ge sind bei uns immer gern gesehen. "
        #                                             "Hier haben Sie die M�glichkeit uns ihre R�ckmeldung zu senden. "
        #                                             "Bitte ber�cksichtigen Sie, dass nur R�ckmeldungen �ber dieses "
        #                                             "Formular von unserem Team entgegengenommen werden k�nnen.",
        #                     "labelSubject": "Betreff:",
        #                     "labelFeedback": "Ihre R�ckmeldung / Kritik / Verbesserungsvorschl�ge:",
        #                     "pushButtonSend": " Senden",
        #                     "pushButtonClose": " Schlie�en"
        # }
#--------------------------------------------------------------------------------------
##        dict_ui_about = {
##                            #"about_title":" �ber...",
##                            "textEdit_philosophy": about_app.dict_info["product_name"] +
##                                                " verfolgt einen Grundsatz: Ordnung ist das halbe Leben (zitiert von Heinrich B�ll). " +
##                                                "Dieses Programm erhebt den Anspruch FAST alles zu verwalten, was man "
##                                                "verwalten kann. "
##                                                "In dieser Version lassen sich folgende Medien verwalten: "
##                                                "<br>"
##                                                "- " + dict_manager_info["movie"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["books"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["musics"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["coins"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["contacts"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["notes"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["videogames"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["quotations"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["stamps"] +
##                                                "<br>"
##                                                "- " + dict_manager_info["idioms"] +
##                                                ""
##                                                "<br><br> "
####                                                "Dar�ber hinaus versteht sich dieses Programm in erster Linie als reine Verwaltungssoftware. "
##                                                "Der Anwender bekommt die komplette Kontrolle �ber die Verwaltung. "
##                                                "Das bedeutet, dass der Anwender die Daten manuell verwaltet. "
##                                                "Jedoch ist das Pogramm lernf�hig. "
##                                                "Stammdaten werden ein einziges Mal in die Datenbank gepflegt. Je umfangreicher "
##                                                "die Stammdaten, desto schneller lassen sich die  Medien und Daten verwalten. "
##                                                "Durch die strikte manuelle Pflege beh�lt der Anwender den �berblick.",
                            #"pushButton_close": "Schlie�enelere",
                            #"tab_version": "Version",
                            #"tab_authors": "Autoren",
                            #"tab_thanks": "Danksagung",
                            #"tab_licence": "Lizenz",
                            #"tab_philosophy": "Philosophie",
                            #"label_gui_inferface": "Benutzeroberfl�che (QT):",
##                            "label_homepage": "Internetseite:",
##                            "label_homepage_url": "www.meine_domain.de",
##        }
#--------------------------------------------------------------------------------------
        dict_ui_pp_check = {
                            "pp_check_title_contact": " hinzuf�gen",
                            "pushButtonSearch": "Suchen",
                            "pushButtonCancel": "Abbrechen",
        }
#--------------------------------------------------------------------------------------
        # dict_ui_about_version = {
        #                     #"about_version_title_current_version": " Erneuerungen ",
        #                     #"about_version_title_version_hitory": " Versionsgeschichte ",
        #                     #"label_explain_current_version": " Was gibt es Neues?",
        #                     #"label_explain_version_hitory": " Die Entwicklung der Versionen im �berblick",
        #                     #"error_text": "FEHLER: Der Text konnte nicht geladen werden. "
        #                     #              "\n"
        #                     #              "\nM�gliche Ursachen:"
        #                     #              "\n1. Ihre Internetverbindung wurde unterbrochen"
        #                     #              "\n2. Der Server ist zur Zeit nicht erreichbar"
        #                     #              "\n"
        #                     #              "\nBitte �berpr�fen Sie ihre Internetverbindung oder versuchen Sie es sp�ter nocheinmal.",
        #                     #"normal_text": "Der Text wird geladen...",
        #                     #"pushButton_close": " Abbrechen ",
        # }
#--------------------------------------------------------------------------------------
        # dict_ui_add_person_address_book = {
        #                     "add_person_address_book_title": " Auswahl... ",
        #                     "label_you_want_text": "Sie m�chten eine...",
        #                     "radioButton_new_person": "... neue Person in das Adressbuch eintragen.",
        #                     "radioButton_existing_person": "... vorhandene Person in das Adressbuch eintragen.",
        #                     #"pushButton_next": "Weiter",
        #                     #"pushButton_close": "Abbrechen",
        # }
#--------------------------------------------------------------------------------------
        dict_ui_login = {
                            #"login_titel": " Anmeldung ",
                            #"groupBox_access": "Zugangsdaten zum Server: ",
                            #"pushButton_connect": " Verbinden ",
                            #"pushButton_clear": " Zur�cksetzen ",
                            #"pushButton_cancel": " Abbrechen ",
                            #"label_port": "Port:",
                            #"label_dataabase_name": "Datenbankname:",
                            #"label_password": "Passwort:",
                            #"label_user": "Benutzername:",
                            #"label_server_host": "Server:",
        }
# #--------------------------------------------------------------------------------------
#         dict_ui_create_user = {
#                             "ui_create_user_title": " Benutzerverwaltung",
#                             "label_username": "Benutzername:",
#                             "label_password": "Passwort:",
#                             "label_password_confirm": "Passwort wiederholen:",
#                             "label_database": "Datenbankname:",
#                             "label_host": "Host",
#                             "pushButton_create_user": " Benutzer anlegen ",
#                             "pushButton_reset": "Zur�cksetzen",
#                             "PushButton_cancel": " Abbrechen ",
#                             "checkBox_all_privileges": "All Rechte",
#                             "checkBox_select": "SELECT",
#                             "checkBox_create": "CREATE",
#                             "checkBox_alter": "ALTER",
#                             "checkBox_show_view": "SHOW VIEW",
#                             "checkBox_insert": "INSERT",
#                             "checkBox_drop": "DROP",
#                             "checkBox_create_temporary_tables": "CREATE TEMPORARY TABLES",
#                             "checkBox_create_routine": "CREATE ROUTINE",
#                             "checkBox_update": "UPDATE",
#                             "checkBox_references": "REFERENCES",
#                             "checkBox_lock_tables": "LOCK TABLES",
#                             "checkBox_alter_routine": "ALTER ROUTINE",
#                             "checkBox_delete": "DELETE",
#                             "checkBox_index": "INDEX",
#                             "checkBox_create_view": "CREATE VIEw",
#                             "checkBox_execute": "EXECUTE",
#                             "checkBox_grant_option": "GRANT OPTION",
#                             "checkBox_event": "EVENT",
#                             "checkBox_create_user": "CREATE USER",
#                             "checkBox_usage": "USAGE",
#                             "checkBox_super": "SUPER",
#                             "checkBox_shutdown": "SHUTDOWN",
#                             "checkBox_reload": "RELOAD",
#                             "checkBox_process": "PROCESS",
#                             "checkBox_trigger": "TRIGGER",
#                             "checkBox_file": "FILE",
#                             "checkBox_show_database": "SHOW DATABASE",
#                             "checkBox_replication_client": "REPLICATION CLIENT",
#                             "checkBox_replication_slave": "REPLICATION SLAVE",
#                             "GroupBox_create_restricted_user": " Benutzerdaten ",
#                             "GroupBox_privileges": " Rechteverwaltung "
#         }
# #--------------------------------------------------------------------------------------

        #dict_ui_setting = {
                            #"pp_setting_title": "Einstellungen... ",
                            #"pushButtonOk": "�bernehmen",
                            #"pushButtonCancel": " Schlie�en",
                            #"tabWidgetSetting_general": "Allgemein",
                            #"tabWidgetSetting_database": "Datenbank",
                            #"tabWidgetSetting_update": "Update",
                            #"radioButton_ms_access": "Microsoft Access",
                            #"radioButton_sqlite": "SQLite",
                            #"labelExplainVersion": "Es wird empfohlen die Standart-Server-Adresse "
                            #                              "nicht zu ver�ndern.",
                            #"labelDownloadFolder": "Pfad Aktualisierungs-Datei: ",
                            #"labelDownloadServer": "Server-Adresse: ",
                            #"labelExplainDownloadServer": "Es wird empfohlen die Standart-Server-Adresse "
                            #                              "nicht zu ver�ndern.",
                            #"labelExplainDownloadFolder": "Sie k�nnen dieses Feld auch leer lassen.",
                            #"textEditLocalDB": "Bei einer Desktop-Datenbank befindet sich  die Datei des "
                            #                    "Datenbankmanagementsystem (DBMS) auf ihrem Rechner."
                            #                    "<html><b><br><br>Hinweis</b</html>: Auf ihrem Rechner muss keine "
                            #                    "Datenbankanwendung installiert sein. Es gen�gt die Datenbankdatei.",
                            #"groupBoxServerDB": " Server-Datenbank ",
                            #"radioButton_mssql": "Microsoft SQL",
                            #"radioButton_postgre": "PostgreSQL",
                            #"radioButton_mysql": "MySQL",
                            #"groupBoxLocalDB": "Desktop-Datenbank",
                            #"groupBoxDownloadOptions": " Optionen des Herunterladens ",
                            #"groupBoxCkeckOptions": " �berpr�fungsoptionen ",
                            #"checkBoxAskBeforClose": "Immer nachfragen, ob das Programm beeendet werden soll",
                            #"folderDialogTitle": " Ordner auswh�len ",
        #}
#--------------------------------------------------------------------------------------
        dict_ui_search = {
                            #"pp_search_title": " Suchen ",
                            #"pushButtonSearch": " Suchen ",
                            #"pushButtonClose": " Schlie�en ",
                            #"labelDatabase": "Datenbank:",
                            #"labelCriterion": "Kriterium:",
                            #"labelSearch": "Suchgebgriff:",
                            "comboBoxDatabase": ["", "Film", "Musik", "Buch",
                                                "Kontakt", "Ausleih", "M�nze", "Videospiel"],
                            "comboBox_criteria_movie": ["", "Genre", "Personen", "FSK",
                                                "Jahr", "Regional Code", "Laufzeit", "Fassungstyp",
                                                "Editionen", "Aufbewahrungsort", "Produktionsfirmen"],
                            "comboBoxCriterion": "",
                            #"groupBoxHead": " Suchkriterien ",
                            #"groupBoxResults": " Suchergebnisse "

        }
#--------------------------------------------------------------------------------------
        # dict_ui_cover_viewer = {
        #                     "pp_moviecoverviewer_title": " Cover ",
        #                     "labelExplain": "<br> "
        #                                     "Sie k�nnen das Bild vergr��ern und verkleinern,"
        #                                     "indem Sie die Gr��e des Fensters mit der Maus ver�ndern. "
        #                                     "<br>",
        #                     "pushButton_close": " Schlie�en ",
        # }
#--------------------------------------------------------------------------------------
        dict_ui_pp_History = {
                            "pp_history_title": " Versionshinweis ",
                            "labelExplain": "<br> "
                                            "Sie k�nnen das Bild vergr��ern und verkleinern,"
                                            "indem Sie die Gr��e des Fensters mit der Maus ver�ndern. "
                                            "<br>",
                            "pushButtonClose": " Schlie�en ",
        }
#--------------------------------------------------------------------------------------
        dict_setShortcut = {
                        "   actionContact" : "Ctrl+F",
        }
#--------------------------------------------------------------------------------------

###############################
####### TestDialog ############
###############################
        # Dict_TestDialog = {
        #               "Label1" : " Ihr Text: ",
        #               "cmd_Beenden" : "Abbrechen",
        #               "cmd_Ok" : "Ok",
        #               "Title" : "Ein neues Fenster",
        #               "actionAddNote" : "Notizen verwalten",
        #               "actionAddVideogame" : "Videospiele verwalten",
        #               "actionAddStamp" : "Briefmarken verwalten",
        #               "actionAddClose" : "Programm beenden",
        #               ###############################################
        #               "menuSysTray" : "Systray-Icon minimieren",
        #               "menuClose" : "Beenden",
        #               "menuEdit" : "Bearbeiten",
        #               "menuAddSearch" : "Suchen",
        #               "menuOption" : "Option",
        #               "menuAddBackUp" : "Sicherung",
        #               "menuAddPassword" : "Passwort",
        #               "menuLanguage" : "Sprache",
        #               "menuAddGerman" : "Deutsch",
        #               "menuAddEnglish" : "Englisch",
        #               "menuInfo" : "&?",
        #               "menuAddHelp" : "Hilfe",
        #               "menuAddShortcut" : "Tastenk�rzel",
        #               "menuAddFeedback" : "R�ckmeldung senden",
        #               "menuAddSupport" : "Unterst�tzung",
        #               "menuAddAbout" : "�ber Xarphus",
        #                 }

###############################
####### Message ###############
###############################
# Nur die Menuleiste und die StatusbarTip
#         Dict_Message = {
#                       "MessageTitle" : " Nachricht ",
#                       "MessageText" : " Sie sind gerade dabei das Programm zu beenden. Sind Sie sicher, dass Sie das Programm beenden wollen?",
#                       "menuAddMovie" : "Add Movie",
#                       "menuAddSerie" : "Add Serie",
#                         }

        # Dict_Message_WindowExists = {
        #               "MessageTitle" : " Nachricht ",
        #               "MessageText" : " Fenster existiert bereits.",
        #               "menuAddMovie" : "Add Movie",
        #               "menuAddSerie" : "Add Serie",
        #                 }

###############################
####### TestDialog ############
###############################
# Nur die Menuleiste und die StatusbarTip
#         Dict_TestDialog = {
#                       "Title" : " Nachricht ",
#                       "Label1" : " Text: ",
#                       "cmd_Ok" : "Ok",
#                       "cmd_Beenden" : "Abbrechen",
#                         }
