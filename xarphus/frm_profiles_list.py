#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
from os import path as os_path
from sys import argv, exc_info, exit
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt, QTranslator, pyqtSignal, QThread, QObject, QVariant, QTimer, QSize
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QApplication, QDialog, QStandardItemModel, QStandardItem, \
                        QIcon, QSizePolicy, QPixmap

try:

  from xarphus.q_line_edit_button_right import ButtonLineEdit
  from xarphus.core.manage_qt import PushButtonRightIcon
  from xarphus.core.standard_item_model import StandardItemModel
  from xarphus.core.proxy_model import ProxyModel
  from xarphus.core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate
  from xarphus.core.manage_folder import generator_top_sub_folder, create_folder

except ImportError: #   When the module is started as a program
  from q_line_edit_button_right import ButtonLineEdit
  from core.manage_qt import PushButtonRightIcon
  from core.standard_item_model import StandardItemModel
  from core.proxy_model import ProxyModel
  from core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate
  from core.manage_folder import generator_top_sub_folder, create_folder


BASE_PATH = os.path.dirname(os.path.abspath(__file__))

class ProfileWorker(QObject):

    finish_thread_signal = pyqtSignal()
    show_process_text_signal = pyqtSignal(unicode, unicode)
    counter_profiles_signal = pyqtSignal()

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)

        self._kwargs = kwargs

        print "self._kwargs.get('folder_name_list')", self._kwargs.get('folder_name_list')

    def init_start_thread(self):

        # We have to Iterate over multiple lists simultaneously
        for folder_name, folder_path in zip(self._kwargs.get('folder_name_list'), self._kwargs.get('folder_path_list')):

            self.show_process_text_signal.emit(folder_name, folder_path)

            self.counter_profiles_signal.emit()

        self.finish_thread_signal.emit()


class ProfileList_Window(QDialog):
    '''
       SUMMARY
       =======
       Its shows a list of profiles in the QTreeView.
    '''

    visible_splash_screen_signal = pyqtSignal(bool)
    return_profile_name_signal = pyqtSignal(unicode)

    def __init__(self,
                 parent,
                 **kwargs):

        QDialog.__init__(self, parent = None)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self._standard_item_model = StandardItemModel(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'))
        self._general_proxy_model = ProxyModel()

        self._count_profiles = 0

        UI_PATH = QFile(":/ui_file/profiles_list.ui")

        self.load_ui_file(ui_path = UI_PATH)
        self.init_timer()
        self.init_ui(image_path_collection = self._kwargs.get('image_path_collection'))
        self.set_icon_on_windows_taskbar(image_path_collection = self._kwargs.get('image_path_collection') )
        self.init_line_edit()
        self.init_clicked_signal_push_buttons()
        self.init_tree_view()

        # set the selectionChanged to QTreeView()
        self.selection_changed()

        self.on_start_profile_worker(folder_name_list = self._kwargs.get('folder_name_list'),
                                     folder_path_list = self._kwargs.get('folder_name_list'),
                                     label_widget = self.ui_profile_list.label_profiles_result)

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        self.ui_profile_list = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self,
               image_path_collection):
        '''
           SUMMARY
           =======
           This method initializes the user interface of profile list.

           PARAMETERS
           ==========
           :image_path_collection (str):  There is the path to the compiled image.

           RETURNS
           =======

           :return:                       Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                          me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.ui_profile_list.pushButton_create_profile.setIcon(QIcon(image_path_collection.person_16x16))

        pixmap_profile_image = QPixmap(image_path_collection.profile_image_64x64)

        self.ui_profile_list.label_profile_image.setPixmap(pixmap_profile_image)

        self.ui_profile_list.label_profile_general_info.setFocus()

        self.ui_profile_list.pushButton_next = PushButtonRightIcon(
          self.tr('Next'),
          icon = QIcon(image_path_collection.double_right_aarrow_16x16),
          iconSize = QSize(16, 16))

        # #create_profile_text = self.tr('Create profile')
        # create_profile_text = self.tr('Create a very new profile')
        # total = len(create_profile_text)
        # average = total * (float(28) / 100)
        # print "total", total
        # print "average", average
        # create_profile_texted = create_profile_text + ' '*int(average)
        # self.ui_profile_list.pushButton_create_profile = PushButtonRightIcon(
        #   create_profile_texted,
        #   icon = QIcon(image_path_collection.person_16x16),
        #   iconSize = QSize(16, 16))

        self.ui_profile_list.pushButton_next.setEnabled(False)

        self.ui_profile_list.horizontalLayout.addWidget(self.ui_profile_list.pushButton_next)

        self.ui_profile_list.pushButton_next.setSizePolicy(
            QSizePolicy.Preferred,
            QSizePolicy.Preferred)

        self.ui_profile_list.pushButton_close.setIcon(QIcon(image_path_collection.cancel_16x16))

        return

    def set_icon_on_windows_taskbar(self,
                                    image_path_collection):
        '''
           SUMMARY
           =======
           This method sets the icon in windows taskbar.

           PARAMETERS
           ==========
           :image_path_collection (str):  There is the path to the compiled image.

           RETURNS
           =======

           :return:                       Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                          me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.ui_profile_list.setWindowIcon(QIcon(image_path_collection.person_16x16))

        return

    def init_timer(self):
        '''
           SUMMARY
           =======
           This method initializes all QTimer.

           RETURNS
           =======

           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize all tiers")

        self.search_delay_timer_profile_list = QTimer()
        self.search_delay_timer_profile_list.setSingleShot(True)
        self.search_delay_timer_profile_list.setInterval(1000)
        self.search_delay_timer_profile_list.timeout.connect(lambda:
            self.filter_update(
              line_edit_widget = self.ui_profile_list.lineEdit_search_profile_list,
              image_path_collection = self._kwargs.get('image_path_collection'),
              label_found_counter = self.ui_profile_list.label_count_found_profile_list,
              certain_proxy_model = self._general_proxy_model.proxy_model_profile_list,
              column = 2))

    def init_line_edit(self):
        '''
           SUMMARY
           =======
           This function is used to upload file.

           EXTENTED DISCRIPTION
           ====================
           In this custom QLineEdit there is a QPushButton on right site for clear the content.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the custom QLineEdit with QPushButton on right site")

        self.ui_profile_list.lineEdit_search_profile_list = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_profile_list.lineEdit_search_profile_list.setObjectName("lineEdit_search_profile_list")
        self.ui_profile_list.lineEdit_search_profile_list.setPlaceholderText("Search the table")
        self.ui_profile_list.lineEdit_search_profile_list.set_default_widget_signal.connect(lambda:
            self.filter_update(
              line_edit_widget = self.ui_profile_list.lineEdit_search_profile_list,
              label_found_counter = self.ui_profile_list.label_count_found_profile_list,
              certain_proxy_model = self._general_proxy_model.proxy_model_profile_list,
              set_default = True,
              column = 2))
        self.ui_profile_list.horizontalLayout_80.addWidget(self.ui_profile_list.lineEdit_search_profile_list)

        self.ui_profile_list.lineEdit_search_profile_list.returnPressed.connect(lambda:
            self.handle_timer(
                timer_widget = self.search_delay_timer_profile_list))

        return

    def init_tree_view(self):
        '''
           SUMMARY
           =======
           This method initializes the QTreeView. In this case we use the model to iterate over the items later.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initializes the QTreeView")

        self.ui_profile_list.tree_view_profiles.setColumnHidden(1, True)

        self.ui_profile_list.tree_view_profiles.doubleClicked.connect(
            lambda
            new_index:
            self.get_id_tree_view(
                new_index = new_index,
                work_mode = "double_clicked",
                treeview_widget = self.ui_profile_list.tree_view_profiles))

        self.set_model(
                 widget_tree_view = self.ui_profile_list.tree_view_profiles,
                 certain_proxy_model = self._general_proxy_model.proxy_model_profile_list)

        return

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method initializes the PushButton signals: connect a slot to the clicked event.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        self.ui_profile_list.pushButton_close.clicked.connect(self.close)
        self.ui_profile_list.pushButton_next.clicked.connect(self.return_profile_name)

        return

    def create_all_subfolders(
      self,
      folder_path,
      profile_name):
        '''
           SUMMARY
           =======
           This method returns the selected profile name to the QApplication.

           PARAMETERS
           ==========
           :timer_widget (class):   We need a time object to be able to treat it.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return'
                    isn't necessary, but this statement gives me a better
                    feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Try to create all subfolders in given profile folder')

        # well its testing. We want to create all folders in the
        generator_top_sub_folder_1 = generator_top_sub_folder(folder_path = folder_path, profile_name = profile_name)

        for folder in generator_top_sub_folder_1:
          try:
              print "folder", folder
              create_folder(folder)

          except OSError as e:
            # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

          except:
            # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def return_profile_name(self):
        '''
           SUMMARY
           =======
           This method returns the selected profile name to the QApplication.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return'
                    isn't necessary, but this statement gives me a better
                    feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Return the selected profile name')

        #self.return_profile_name_signal.emit(self.selected_treeview_item)

        # profil_path = os_path.join(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"], 'profiles')

        # self.create_all_subfolders(
        #   folder_path = unicode(profil_path),
        #   profile_name = unicode(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"]))

        #self.visible_splash_screen_signal.emit(False)

        return

    def handle_timer(self,
                     timer_widget = None):
        '''
           SUMMARY
           =======
           This method starts and stops the given timer.

           PARAMETERS
           ==========
           :timer_widget (class):   We need a time object to be able to treat it.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return'
                                    isn't necessary, but this statement gives me a better
                                    feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Start and stop the given timer')

        timer_widget.stop()
        timer_widget.start()

        return

    def count_profiles(self,
                        label_widget,
                        result_test = "Profile(s)"):
        '''
           SUMMARY
           =======
           This method starts and stops the given timer.

           PARAMETERS
           ==========
           :label_widget (class):   We need a widget of QLabel for updating it.
           :result_test (str):      By default we have a text.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''

        self._count_profiles += 1

        label_widget.setText("{result_text}: {result_number}".format(result_text = result_test, result_number = self._count_profiles))

        return

    def add_items_model(self, folder_name, folder_path):
        '''
           SUMMARY
           =======
           This method adds the given items to the model.

           PARAMETERS
           ==========
           :text (unicode):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Add data to model')

        two_columns_item = [QStandardItem(unicode(folder_name)), QStandardItem(unicode(folder_path))]

        self._standard_item_model.standard_item_model_profile_list.appendRow(two_columns_item)

        self.ui_profile_list.tree_view_profiles.setColumnHidden(1, True)

        return

    def set_model(self,
                 widget_tree_view,
                 certain_proxy_model):
        '''
           SUMMARY
           =======
           Sets the model for the view to present.

           PARAMETERS
           ==========
           :widget (Qt-object):     we except a widget for the view.

           :model (QStandardItemModel):      We required a model to set it to the widget.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set given model on given Qt-object")

        try:

            #   Assign a base model to the proxy:
            certain_proxy_model.setSourceModel(self._standard_item_model.standard_item_model_profile_list)

            #   Assign the proxy to the table instead of the model:
            widget_tree_view.setModel(certain_proxy_model)

        except AttributeError:

            # log and stop here
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def filter_update(self,
                      **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the thread for updating the filter")

        self.task_thread = QThread()
        self.task_thread.work = TaskFilterUpdate(parent = None,
                                                **kwargs)
        # self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.result_filter_update_signal.connect(
            lambda result_number:
            kwargs.get("label_found_counter").setText("{found_text} {result_number}".format(found_text = "Found:", result_number = result_number)))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_thread)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def selection_changed(self):

        self.ui_profile_list.tree_view_profiles.selectionModel().selectionChanged.connect(
            lambda
            new_index:
            self.get_id_tree_view(
                new_index = new_index,
                work_mode = "selection_changed"))

        return

    def get_id_tree_view(self,
                         new_index,
                         work_mode,
                         index_number = 0,
                         treeview_widget = None):
        '''
           SUMMARY
           =======
           This method called whenever the selection changes.
           The change in the selection is represented as an item selection of deselected items
           and an item selection of selected items.

           PARAMETERS
           ==========
           :new_index (class):      By single clicking we ge a new QItemSelection and by double clicking we get q new QqodelIndex.

           :work_mode (str):        We need to know how to work with data.

           :index_number (int):

           :treeview_widget (class):    When the user clicks double on QTreeView we get the widget of them.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Change the selection")

        if work_mode == "selection_changed":

            try:
                if not new_index is None:

                    self.selected_treeview_item = new_index.indexes()[index_number].data()#.toPyObject()

                    self._kwargs['current_profile'] = new_index.indexes()[index_number].data()#.toPyObject()

                    self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"] = new_index.indexes()[index_number].data()#.toPyObject()

                    #print u'The data of the QString is: {}'.format(self.selected_treeview_item)

                    self.ui_profile_list.pushButton_next.setEnabled(True)

            except IndexError:
                # log and stop here
                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

        elif work_mode == "double_clicked":

            try:
                if not new_index is None:
                    item = treeview_widget.selectedIndexes()[index_number]

                    #   Gets the data from the 'QModelIndex', but 'QModelIndex' returns a QString,
                    #   so that why we use the data()-method of this object
                    selected_item = item.data()

                    self._kwargs['current_profile'] = item.data()

                    self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"] = item.data()

                    self.return_profile_name_signal.emit(selected_item)

                    #print u'The data of the QModelIndex is: {}'.format(data)

            except IndexError:
                # log and stop here
                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

    def on_start_profile_worker(self, **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the Thread")

        task_thread = QThread(self)
        task_thread.work = ProfileWorker(**kwargs)

        task_thread.work.moveToThread(task_thread)

        task_thread.work.show_process_text_signal.connect(
            lambda
            folder_name,
            folder_path:
          self.add_items_model(folder_name = folder_name, folder_path = folder_path))

        task_thread.work.counter_profiles_signal.connect(
            lambda:
            self.count_profiles(label_widget = kwargs.get("label_widget")))

        task_thread.work.finish_thread_signal.connect(task_thread.quit)

        task_thread.started.connect(task_thread.work.init_start_thread)

        task_thread.finished.connect(task_thread.deleteLater)

        task_thread.start()

    def closeEvent(self, event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event (class):  By default the given event is accepted and the widget is closed. We
                            get this event when Qt receives a window close request for a top-level
                            widget from the window system. That means, first it sends the widget a QCloseEvent.
                            The widget is closed if it accepts the close event. The default implementation of
                            QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_profile_list.close()
            exit()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication

    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now

    from core.manage_logging import configure_logging

    '''
        Import all compiled files
    '''
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    dict_custom_config = ConfigurationSaverDict()

    convert_current_time = get_time_now()

    #dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus_project\\xarphus\\log\\", convert_current_time + '.log')
    configure_logging(LOG_FILE_PATH)

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = ProfileList_Window()

    window.showMaximized()
    exit(app.exec_())
