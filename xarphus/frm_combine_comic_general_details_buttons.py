#-*- coding:latin1 -*-

from os import path
import inspect

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QRegExp

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QIcon, QFileDialog, \
    QPixmap, QValidator, QRegExpValidator, QTabWidget, QGridLayout, QTextEdit, QIcon

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window

from xarphus.frm_comic_general import ComicGeneral_Window
from xarphus.frm_comic_general_details import ComicGeneralDetails_Window


BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineComicGeneralDetailsButtons_Window(QWidget):
    def __init__(self,
                 view_mode,
                 cover_viewer,
                 movie_detail,
                 form_title,
                 image_path_collection,
                 parent=None):
        '''
        NOTICE:
        =======
        This method combines and shows the form for comoc book.

        PARAMETERS:
        ===========
        :view_mode:                 -   You get a number as a integer. The numbers
                                        are 0 and 1. The number 0 says, when a user
                                        decides to add a new comic data. Otherwise, 1
                                        says,the user wants to take a look a existing comic data
                                        and maybe he wants to edit the comic data.

        :cover_viewer:              -   You get an access to the function un the moduke
                                        named (mdi.py). This function starts the cover-window.

        :movie_detail:              -

        :form_title:                -   This new created window get a title.

        :save_config:               -   You get an access to the dictionary that is saved in
                                        module named (config.py). There, all settings are saved.

        :image_path_collection:     -   In the module named (collection_paths_image.py). There
                                        are all paths of images or icons. Notice: All the paths
                                        of icons or images are used repeatedly. That's the reason
                                        why the paths were outsourced.

        :parent:                    -   
        '''

        QWidget.__init__(self, parent)

        self.create_cover_viewer = cover_viewer
        self.movie_detail = movie_detail
        
        self.view_mode = view_mode
        self.form_title = form_title

        self.image_path_collection = image_path_collection

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.create_combined_form()
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def create_combined_form(self):
        '''
            NOTICE:
            =======
            A new window is created from several subclasses.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Try to create an instance of ComicGeneral_Window()")

        self.comic_general = ComicGeneral_Window(self)

        logger.info("Try to create an instance of ComicGeneralDetails_Window()")

        self.comic_general_details = ComicGeneralDetails_Window(self)

        logger.info("Try to create an instance of PushbuttonsClosePrintDeleteSave_Window()")

        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self)

        logger.info("Try to create an instance of QVBoxLayout()")

        vertical_layout = QVBoxLayout()

        logger.info("The instance of the class of  QVBoxLayout() is created successfully")

        logger.info("Try to add all the given widgets to QVBoxLayout()")

        vertical_layout.addWidget(self.comic_general)
        vertical_layout.addWidget(self.comic_general_details)
        vertical_layout.addWidget(self.pushbutton_close_print_delete_save)

        logger.info("All given widgets are added to to QVBoxLayout() successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        textEdit_view = QTextEdit(self)

        logger.info("The instance of the class of  QTextEdit() is created successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        logger.info("Try to set the instance of QTextEdit() in read-only mode (True)")

        textEdit_view.setReadOnly(True)

        logger.info("The instance of QTextEdit is set in read-only mode (True) successfully")

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_view = QGridLayout(self)

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_view.addWidget(textEdit_view, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        logger.info("Try to create an instance of QTabWidget()")

        tab_widget = QTabWidget(self)

        logger.info("The instance of QTabWidget() successfully")

        logger.info("Try to create an instances of QWidget()")

        tab_serie_editing = QWidget(self)
        tab_serie_total_view = QWidget(self)

        logger.info("The instances of QWidget() successfully")

        logger.info("Try to set the layout manager for given widget to QVBoxLayout()")

        tab_serie_editing.setLayout(vertical_layout)

        logger.info("The given widget is set to QVBoxLayout() successfully")

        logger.info("Try to set the layout manager for given widget to QGridLayout()")

        tab_serie_total_view.setLayout(gridLayout_view)

        logger.info("The given widget is set to QGridLayout() successfully")

        logger.info("Try to add tabs with the given page and label to QTabWidget()")

        tab_widget.addTab(tab_serie_editing, self.tr("Editing"))
        tab_widget.addTab(tab_serie_total_view, self.tr("Total view"))

        logger.info("All given tabs with the given page and label are added to QTabWidget() successfully")

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_1 = QGridLayout(self)

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_1.addWidget(tab_widget, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.init_clicked_signal_push_buttons()
        self.set_window_icon(image_path=self.image_path_collection.getPath_addComic_icon)
        self.set_window_title(title=self.form_title)

        return

    def set_window_title(self, 
                         title=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowTitle(title)

        return

    def set_window_icon(self,
                         image_path=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowIcon(QIcon(image_path))

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object(s)")


        self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close)

        return

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return



