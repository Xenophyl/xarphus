#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''

import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QFile
from PyQt4.QtGui import QWidget, QLineEdit, QMdiSubWindow, QIcon, QToolTip, QCursor
from PyQt4.uic import loadUi

from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
from xarphus.core.manage_generator import list_object_generator
from xarphus.core.manage_string import is_string_blank, count_it 

from xarphus.gui import ui_rc

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'register.ui')
#UI_PATH = QFile(":/ui_file/register.ui")

class ConnectionProfiles_Window(QWidget):
    def __init__(self, 
                 image_path_collection=None,
                 parent=None):

        QWidget.__init__(self, parent)

        # Path to the *.ui-file
        #self.getPath_register = os.path.abspath(os.path.join('files', "qt_ui", 'register.ui'))

        # Call classes
        UI_PATH = QFile(":/ui_file/connection_profiles.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        '''
            Create instance variables.
        '''
        self.image_path_collection = image_path_collection

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_connection_profiles = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):
        '''
            NOTE:
            ======
            This method initializes the user interface of master_data-window. 

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        standard_window_icon = self.image_path_collection.connection_profile_icon_data

        self.set_window_icon(image=standard_window_icon)
        self.init_clicked_signal_push_buttons()

        return

    def set_window_icon(self, image=None):
        '''
            NOTICE:
            =======
            This method is used to set icon of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowIcon(QIcon(image))

        return

    def init_clicked_signal_push_buttons(self):

        self.ui_connection_profiles.pushButton_close.clicked.connect(self.close)

        self.ui_connection_profiles.pushButton_profile_name_info.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('Type a name for the connection.')))

        self.ui_connection_profiles.pushButton_server_host_info.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('Name or IP address of the server host.')))

        self.ui_connection_profiles.pushButton_user_name_info.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('Name or the user to connect with.'))) 

        self.ui_connection_profiles.pushButton_data_base_info.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('The name of the databae to use with.')))

        self.ui_connection_profiles.pushButton_port_info.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('Port of the server. Standard port is: 3306.')))

        return

    def show_tool_tip_text(self, text):
        '''
        NOTICE:
        =======
            This method is used to show the tip ballroon and to follow the cursor. 

        PARAMETERS:
        ===========
            :text       -       We need a given text to put it in the tool tip.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        
        logger.info("Fill the value of the dictionary")

        QToolTip.showText(QCursor.pos(), unicode(text))

        return

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''

        self._master_data_manipulation = None
        logger.info("Close the current form.")

        #self.stop_current_task_thread(stop_mode="interrupt")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return