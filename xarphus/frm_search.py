#!/usr/bin/env python
# -*- coding:Latin-1 -*-

FILE_NAME = "frm_search.py"

'''
The modules of required libraries are imported.
'''
import os
import sys
import inspect
import traceback

from subprocess import CalledProcessError

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtCore import Qt, QFile
from PyQt4.QtGui import QIcon, QWidget, QMdiSubWindow, \
    QMessageBox, QPushButton
from PyQt4.uic import loadUi


'''
Private modules are imported.
'''
from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint

from xarphus.core.manage_popen import execute_exe_file

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'search.ui')
#UI_PATH = QFile(":/ui_file/search.ui")

class Search_Window(QWidget):
    def __init__(self,
                 image_path_collection,
                 save_configuration,
                 custom_logger,
                 create_error_form,
                 parent):

        QWidget.__init__(self, parent)

        self.custom_logger = custom_logger

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created successfully - (" + __name__ + ")")

        self.image_path_collection = image_path_collection
        self.create_error_form = create_error_form

        UI_PATH = QFile(":/ui_file/search.ui")

        self.save_configuration = save_configuration

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.load_ui_file(UI_PATH)
        self.init_ComboBox()
        self.init_PushButton()

        self.lineEdit_changed()



        #self.ui_search.pushButton.clicked.connect(self.pope_exe_program)

    def get_class_name(self):
        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        try:
            self.custom_logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")
            UI_PATH.open(QFile.ReadOnly)
            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is opened - (" + __name__ + ")")
            self.custom_logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")
            self.ui_search = loadUi(UI_PATH, self)

        except Exception as ex:
            self.custom_logger.info("An error has occurred.. - (" + __name__ + ")")

            desired_trace = traceback.format_exc(sys.exc_info())
            self.custom_logger.error(desired_trace)

            explain_text = self.tr("A critical internal application error has occurred.")

            self.create_error_form(report_mode="critical",
                                   title_text=self.tr("Error"),
                                   detail_mode=desired_trace,
                                   custom_text=explain_text)

        finally:
            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")
            self.custom_logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")

            UI_PATH.close()

            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed - (" + __name__ + ")")


        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return


    def lineEdit_changed(self):
        self.ui_search.lineEditSearch.textChanged.connect(self.on_text_change)

    def init_ComboBox(self):
        self.ui_search.comboBox_medium.activated.connect(self.combobox_medium_changed)
        self.ui_search.comboBox_sub_category.activated.connect(self.combobox_sub_category_changed)

    def init_PushButton(self):
        self.ui_search.pushButton_close.clicked.connect(self.close_form)

    def create_generally_crritcal_msg(self, err_msg):
        mBox = custom_message_box(self, err_msg, self.tr('Fehler'), QMessageBox.Critical)
        setIc_OK = QIcon(self.image_path_collection.check_icon)
        mBox.addButton(
            QPushButton(setIc_OK, self.tr(" Ok")),
            mBox.AcceptRole)  # An "OK" button defined with the AcceptRole

        mBox.exec_()  # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.     

    def pope_exe_program(self):

        executed_file = unicode("D:\\Dan\\Python\\xarphus\\xarphus_installer\\xarphus_installer.exe")
        input_path = unicode("C:\\Users\\Sophus\\Desktop\\Desktop.zip")
        output_path = unicode("C:\Users\Sophus\Desktop\hier_her")
        config_file_path = unicode("D:\\Dan\\Python\\xarphus\\settings\\config.ini")
        exe_file = unicode(self.save_configuration.dict_set_general_settings["PathToExecuteFile"])

        try:
            execute_exe_file(executed_file, input_path, output_path, config_file_path, exe_file)
            sys.exit()
        except CalledProcessError as err:
            self.create_generally_crritcal_msg(unicode(self.tr("An internal critical error has occurred.") +
                                                       self.tr("\n\nDetails: \n") +
                                                       err.output +
                                                       self.tr("\nReturncode: ") +
                                                       str(err.returncode) +
                                                       "\n\nPlease send us your log file so we can assess the error and provide you with the best solution."))

    def on_text_change(self):
        if not self.ui_search.lineEditSearch.text() != "":
            #self.ui_search.lineEditSearch.setStyleSheet(self.css.set_LineEdit_bcolor_yellow)  # yellow
            pass
        else:
            pass
            #self.ui_search.lineEditSearch.setStyleSheet(self.css.set_LineEdit_bcolor_green)  # green

    #--------------------------------------------------------------------------------------

    def clear_combobox(self, combobox_to_clear=None):
        print "clear_combobox is called"
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        for combo_obj in combobox_to_clear:
            combo_obj.clear()

        return

    def change_comboBox_category(self, combobox_to_clean=None,
                                 combobox_sub_category=None,
                                 items_sub_category=None):

        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        '''
        NOTICE:
        =======


        :param combobox_to_clean:           Let me know, which ComboBox object shall be cleared?

        :param combobox_sub_category:       Let me know, which is the next ComboBox object
                                            shall be enabled or disabled?

        :param items_sub_category:          This argument should contains a list. If this argument contains
                                            no list, then get a None-Value

        :return:                            Its returns nothing, the statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        if not combobox_to_clean == None:
            self.clear_combobox(combobox_to_clean)

        if items_sub_category:
            self.ui_search.comboBox_sub_category.addItems(items_sub_category)
            combobox_sub_category.setEnabled(True)
        else:
            combobox_sub_category.setEnabled(False)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def combobox_sub_category_changed(self, current_combo_index):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        '''
        NOTICE:
        =======
        In this function, the functions are stored in a dictionary.

        :param current_combo_index:
        :return:
        '''

        function_dict = {
                        # Nothing selected - When the user select the first item in ComboBox.
                        # First item in comboBox starts in Python with zero.
                        0: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion],
                                                                 combobox_sub_category=self.ui_search.comboBox_sub_category),

                        #   Movie
                         1: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Film"),
                                                                               self.tr("Series")]),

                        #   Book
                         2: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Magazine"),
                                                                               self.tr("Book"),
                                                                               self.tr("newspaper")]),

                        }

        function_dict[current_combo_index]()

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

    def combobox_medium_changed(self, current_combo_index):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        '''
        NOTICE:
        =======
        In this function, the functions are stored in a dictionary.

        :param current_combo_index:
        :return:
        '''

        function_dict = {
                        # Nothing selected - When the user select the first item in ComboBox.
                        # First item in comboBox starts in Python with zero.
                        0: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                             self.ui_search.comboBox_sub_category],
                                                                 combobox_sub_category=self.ui_search.comboBox_sub_category),

                        #   Movie
                         1: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Film"),
                                                                               self.tr("Series")]),

                        #   Book
                         2: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Magazine"),
                                                                               self.tr("Book"),
                                                                               self.tr("newspaper")]),

                        # Music
                         3: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Album"),
                                                                               self.tr("Single"),
                                                                               self.tr("Record")]),

                         # Coin
                         4: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Coin")]),

                         # Contact
                         5: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Name"),
                                                                               self.tr("Lastname")]),

                         # Note
                         6: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Subject"),
                                                                               self.tr("Content")]),

                         # Video Game
                         7: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Titel Videogame"),
                                                                               self.tr("Content")]),

                         # Quotation
                         8: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Title quotation"),
                                                                               self.tr("Quotation")]),

                         # Stamp
                         9: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                              self.ui_search.comboBox_sub_category],
                                                                  combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                  items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Title stamp"),
                                                                               self.tr("Stamp")]),

                         # Lending
                         10: lambda: self.change_comboBox_category(combobox_to_clean=[self.ui_search.comboBox_criterion,
                                                                               self.ui_search.comboBox_sub_category],
                                                                   combobox_sub_category=self.ui_search.comboBox_sub_category,
                                                                   items_sub_category=[self.tr("No selection"),
                                                                               self.tr("Title lending"),
                                                                               self.tr("Lending")]),
                        }

        function_dict[current_combo_index]()

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def closeEvent(self, event):
        pass

    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()