#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path
from sys import exit, argv, exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################
try:

    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.manage_qt import ProgressBarText

except ImportError:

    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manage_qt import ProgressBarText

from PyQt4.QtCore import Qt, pyqtSignal
from PyQt4.QtGui import QProgressBar, QDialog, QDesktopWidget, QVBoxLayout, QLabel, QMdiSubWindow, QFont, \
						QApplication, QLineEdit, QPushButton, QHBoxLayout, QSpacerItem, QSizePolicy

class Waiting_Window(QDialog):

    count_progress_signal = pyqtSignal(int)
    quite_thread_signal = pyqtSignal()

    remove_reference_signal = pyqtSignal()

    def __init__(self,
                 parent,
                 show_progress_bar = True,
                 show_pushbutton = True,
                 resize_width = 300,
                 resize_height = 120,
                 text_message = None):
        QDialog.__init__(self, parent = None)

        self.resize_width = resize_width
        self.resize_height = resize_height
        self._text_message = text_message
        self.show_progress_bar = show_progress_bar
        self.show_pushbutton = show_pushbutton

        try:

            parent.count_progress_signal.connect(
                lambda
                value:
                self.set_maximum_value(
                    value = value))

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

            parent.update_progress_bar_signal.connect(
                lambda
                process_value,
                final_count:
                self.update_progress_bar(
                    process_value = process_value,
                    final_count = final_count))

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

            parent.set_enable_widget_signal.connect(self.set_enable)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

            parent.close_wait_message_signal.connect(self.close)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        self.init_ui()


    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.setWindowModality(Qt.ApplicationModal)

        self.setWindowTitle(" ")

        self.resize(self.resize_width, self.resize_height)

        # sizePolicy = QSizePolicy(
        #     QSizePolicy.Expanding, QSizePolicy.Expanding)

        # sizePolicy.setHeightForWidth(True)

        # self.setSizePolicy(sizePolicy)

        flags = flag_dialog_titlehint_customizewondowhint()
        self.setWindowFlags(flags)

        #	NOTE: A One-Liner like self.label.setFont(QtGui.QFont().setBold(True))
        #	doesn't work either, because setBold returns None.
        #	have to first create the QFont object.
        myFont = QFont()
        #	Then set it to bold.
        myFont.setBold(True)

        self.label = QLabel()
        self.label.setText(self._text_message)

        # Then set it as the label's font.
        self.label.setFont(myFont)

        #   We align the widget in the center by setting the alignment of the widget to be AlignCenter.
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setWordWrap(True)

        # self.busy_progress_bar = ProgressBarText(self)
        # self.busy_progress_bar.setObjectName("busy_progress_bar")
        # self.busy_progress_bar.setRange(0, 0)

        spacer = QSpacerItem(20, 8, QSizePolicy.Minimum, vPolicy = QSizePolicy.Fixed)
        hbox = QHBoxLayout()

        if self.show_pushbutton:
            self.chancel_loading_and_close = QPushButton(self.tr('Close'))

            hbox.addStretch(1)
            hbox.addWidget(self.chancel_loading_and_close)

        vbox = QVBoxLayout()
        vbox.addItem(spacer)
        vbox.addWidget(self.label)
        vbox.addItem(spacer)

        if self.show_progress_bar:
            self.busy_progress_bar = ProgressBarText(self)
            self.busy_progress_bar.setObjectName("busy_progress_bar")
            self.busy_progress_bar.setRange(0, 0)
            vbox.addWidget(self.busy_progress_bar)
            self.set_text(widget = self.busy_progress_bar, text = 'Waiting for...')
        # else:
        #     vbox.removeWidget(self.busy_progress_bar)
        #     self.busy_progress_bar.deleteLater()
        #     self.busy_progress_bar = None

            #self.busy_progress_bar.hide()
        vbox.addItem(spacer)
        vbox.addStretch()
        vbox.addLayout(hbox)
        vbox.addStretch()

        self.setLayout(vbox)

        self.center_window_on_screen()

        self.init_clicked_signal_push_buttons()

        return

    def set_enable(self,
                   Boolean):

        self.chancel_loading_and_close.setEnabled(Boolean)

        return

    def set_maximum_value(self,
                          value):
        '''
           SUMMARY
           =======
           This method is used to set maximum value of QProgressBar().

           EXTENTED DISCRIPTION
           ====================
           And its also used to show a QProgressbar()-object in busy mode with text instead of a
           percenttage of steps, because minimum and maximum are both set to 0.

           PARAMETERS
           ==========
           :value (bool):       Get a vbalue to set the maximum value of progress bar, it
                                can be any int and it will be automatically converted to x/100% values
                                e.g. max_value = 3, current_value = 1, the progress bar will show 33%.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set maximun on busy_progress_bar, value: {}".format(value))

        try:

            self.busy_progress_bar.setMaximum(value)

        except AttributeError:

            pass

        return

    def update_progress_bar(self,
                            process_value = 0,
                            final_count = 0):
        '''
           SUMMARY
           =======
           The given QProgressBar()-object will update its value, so it will suppose
           to progress while the items are populate.

           EXTENTED DISCRIPTION
           ====================
           This method is also used to set range to minimum = 0 and to maximum = final_count.

           PARAMETERS
           ==========
           :progress_bar_object (class):    Let me know which QProgressbar should be updated.

           :process_value (int):            Current number of processes.

           :final_count (int):              Total number of rows in a table. This method is also
                                            used to stop the busy indicator of given
                                            QProgressBar()-object by using setValue()-method.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary,
                                            but this statement gives me a better feeling, that says: 'return'
                                            terminates a function definitely.
        '''
        logger.info("Update the QProgressbar")

        self._proccessed_progressbar = process_value
        try:

            self.busy_progress_bar .setRange(0, final_count)

            self.busy_progress_bar.setValue(process_value)

        except (TypeError, AttributeError):

            pass

        return

    def set_text(self,
                 widget = None,
                 text = None):
        '''
           SUMMARY
           =======
           Updates the input each time a key is pressed.

           PARAMETERS
           ==========
           :widget (Qt-Object): On which qt object should be shown the text?

           :text (unicode):     We need text to set it.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window set_text is called successfully"
        print "MasterData_Window in set_text / text: -< \n {}".format(text)

        logger.info("Set the text given Qt-object.")

        if isinstance(widget, QLineEdit):
            widget.setText(text)
        elif isinstance(widget, QProgressBar):
            widget.setText(text)
        elif isinstance(widget, QLabel):
            widget.setText(text)

        return

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots for QpuschButton()-object(s).

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        #self.chancel_loading_and_close.clicked.connect(self.close)

        return

    def center_window_on_screen (self):
        '''center_window_on_screen() Centers the window on the screen.

            center_window_on_screen() works by taking the desktop’s
            current resolution and dividing it by 2 to get half of the
            viewport. Next, it takes half of the window size (including the border)
            and subtracts that from the viewport half.
            This provides an origin for the upper-left corner of the window
            to be located such that the window itself is fully “centered.

            Window borders can range from fairly minor (4 pixels around each
            side adding a total of 8 pixels horizontally and vertically) to
            imposing (16 pixels or more). Unfortunately, QWidget.geometry()
            and friends only return a QSize object describing the geometry of
            the contained window rather than the containing window. It seems
            insignificant, sure, but if you want pixel-perfect alignment,
            you have to use something else: QWidget.frameSize(), because
            frameSize() will return the size of the entire window, border included
        '''
        resolution = QDesktopWidget().screenGeometry()
        self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
                  (resolution.height() / 2) - (self.frameSize().height() / 2))

        return

    def keyPressEvent(self, event):
        '''
        Did the user press the Escape key?
        Note: QtCore.Qt.Key_Escape is a value that equates to
        what the operating system passes to python from the
        keyboard when the escape key is pressed.
        '''
        pass

    def closeEvent(self, event):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info('Close the window... laaaaaaaaaaaaaaaaaaaaaaa')

        self.quite_thread_signal.emit()

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:

            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return
