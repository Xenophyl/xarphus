#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_movie_.py"

import os

from PyQt4.QtCore import Qt, QFile

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout

from xarphus.frm_note import Note_Window

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineNoteButtons_Window(QWidget):
    def __init__(self, func_up_to_date, update_sublist, parent):

        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        self.note = Note_Window(self.func, self.up_to_date_sublist, self)
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.func, self.up_to_date_sublist, self)

        layout = QVBoxLayout()
        layout.addWidget(self.note)
        layout.addWidget(self.pushbutton_close_print_delete_save)
        self.setLayout(layout)

        # activate all necessary functions
        self.set_action_pushbutton_prominent()
        self.set_gui_note()

    def set_gui_note(self):
        pass
        #print "set_gui_note is called"
        #self.note.setWindowTitle(self.tr("Notiz"))

    def set_gui_prominent(self):
        pass

    def set_action_pushbutton_profile(self):
        pass

    def set_action_pushbutton_prominent(self):
        self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close_form)
        self.pushbutton_close_print_delete_save.pushButton_save.clicked.connect(self.do_something)

    def do_something(self):
        pass

    def closeEvent(self, event):
        pass

    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        print "STATUS [OK]  (", FILE_NAME, "): The function (close_form) was called"

