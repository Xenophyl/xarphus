<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>About_Window</name>
    <message>
        <location filename="xarphus/frm_about.py" line="194"/>
        <source>User interface (QT): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_about.py" line="196"/>
        <source> verfolgt einen Grundsatz: Ordnung ist das halbe Leben (zitiert von Heinrich Boell). Dieses Programm erhebt den Anspruch FAST alles zu verwalten, was man verwalten kann. In dieser Version lassen sich folgende Medien verwalten: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_about.py" line="196"/>
        <source>Darueber hinaus versteht sich dieses Programm in erster Linie als reine Verwaltungssoftware. Sie bekommen die komplette Kontrolle ueber die Verwaltung. Das bedeutet, dass Sie die Daten manuell verwalten. Jedoch ist das Pogramm lernfaehig. Stammdaten werden ein einziges Mal in die Datenbank gepflegt. Je umfangreicher die Stammdaten, desto schneller lassen sich die (neuen) Medien und Daten verwalten. Durch die strikte manuelle Pflege behalten Sie den ueberblick.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckUpdate_Thread</name>
    <message>
        <location filename="xarphus/frm_update.py" line="86"/>
        <source>New update is available..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="89"/>
        <source>No new update are available. 
This program is up to date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="94"/>
        <source>Error - Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="109"/>
        <source>An error has occurred: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="97"/>
        <source>Error - Too many redirects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="99"/>
        <source>Error - HTTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="101"/>
        <source>Error - Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="103"/>
        <source>Error - URL required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="105"/>
        <source>Error - Connect Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="107"/>
        <source>Error - Read Timeout</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="xarphus/gui/note.ui" line="33"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14"/>
        <source>Manage master data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="35"/>
        <source>Search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="107"/>
        <source>Film - Soundsystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="395"/>
        <source>Soundsystem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24929"/>
        <source>Result(s): 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="126"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25051"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="95"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25090"/>
        <source>Refresh list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25116"/>
        <source>Online Data Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25160"/>
        <source>Cnancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25183"/>
        <source>Apply change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="502"/>
        <source>NOTICE: To download and update the master data of soundsystem you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25244"/>
        <source>Status message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25281"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25301"/>
        <source>Abort update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25324"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="634"/>
        <source>Film - Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14374"/>
        <source>Genre:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="983"/>
        <source>NOTICE: To download and update the master data of genre of film you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="1115"/>
        <source>Film - Regional code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="1145"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25133"/>
        <source>Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/pushbuttons_close_print_delete_save.ui" line="351"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="1645"/>
        <source>NOTICE: To download and update the master data of film regional code you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="1777"/>
        <source>Film - Aspect ration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="1807"/>
        <source>Aspect ration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2200"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2139"/>
        <source>Aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2307"/>
        <source>NOTICE: To download and update the master data of aspect ratio you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2439"/>
        <source>Film - Audioformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2727"/>
        <source>Audioformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2834"/>
        <source>NOTICE: To download and update the master data of audioformat you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="2966"/>
        <source>Film - Audio channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="3254"/>
        <source>Audio channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="3361"/>
        <source>NOTICE: To download and update the master data of audio channel you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="3493"/>
        <source>Film - Colour format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="3781"/>
        <source>Colour format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="3888"/>
        <source>NOTICE: To download and update the master data of colour format you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="4020"/>
        <source>Film - Videonorm </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="4308"/>
        <source>Videonorm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="4415"/>
        <source>NOTICE: To download and update the master data of videonorm you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="4882"/>
        <source>Film resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="4938"/>
        <source>Film_presenting_format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5045"/>
        <source>NOTICE: To download and update the master data of film resolution you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5177"/>
        <source>Film - Videoformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5465"/>
        <source>Videoformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5572"/>
        <source>NOTICE: To download and update the master data of videoformat you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5704"/>
        <source>Film - Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="5992"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="6099"/>
        <source>NOTICE: To download and update the master data of version you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="6231"/>
        <source>Film - Dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="6519"/>
        <source>Dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="6626"/>
        <source>NOTICE: To download and update the master data of dimension you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="6758"/>
        <source>Film - Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="7046"/>
        <source>Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="7153"/>
        <source>NOTICE: To download and update the master data of edition you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="7555"/>
        <source>Film production company</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="7634"/>
        <source>NOTICE: To download and update the master data of film production company you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="8036"/>
        <source>Film distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="8115"/>
        <source>NOTICE: To download and update the master data of film distribution you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="8517"/>
        <source>Film rentals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="8596"/>
        <source>NOTICE: To download and update the master data of film rentals you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="8998"/>
        <source>Film studio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9077"/>
        <source>NOTICE: To download and update the master data of film studio you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9215"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9226"/>
        <source>Music - Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9575"/>
        <source>NOTICE: To download and update the master data of genre of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9707"/>
        <source>Music - Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="9977"/>
        <source>Style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10056"/>
        <source>NOTICE: To download and update the master data of style of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10188"/>
        <source>Music - Studio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10458"/>
        <source>Studio:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10537"/>
        <source>NOTICE: To download and update the master data of studio of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10669"/>
        <source>Music - Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="10939"/>
        <source>Publisher:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11018"/>
        <source>NOTICE: To download and update the master data of publisher of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11150"/>
        <source>Music - Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11172"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11420"/>
        <source>Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11499"/>
        <source>NOTICE: To download and update the master data of label of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11631"/>
        <source>Music - Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11653"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11901"/>
        <source>Distribution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="11980"/>
        <source>NOTICE: To download and update the master data of distribution of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12112"/>
        <source>Music - SPARS Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12134"/>
        <source>SPARS Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12382"/>
        <source>SPARS Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12461"/>
        <source>NOTICE: To download and update the master data of SPARS code of music you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12599"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="12610"/>
        <source>Book - Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13070"/>
        <source>NOTICE: To download and update the master data of genre of book you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13091"/>
        <source>Book - Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13361"/>
        <source>Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13551"/>
        <source>NOTICE: To download and update the master data of publisher of book you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13572"/>
        <source>Book - Binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13594"/>
        <source>Binding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="13842"/>
        <source>Binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14032"/>
        <source>NOTICE: To download and update the master data of binding of book you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14059"/>
        <source>Book - Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14076"/>
        <source>Book - Color format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14093"/>
        <source>Video game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14104"/>
        <source>Video game - Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14453"/>
        <source>NOTICE: To download and update the master data of genre you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14591"/>
        <source>Person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14602"/>
        <source>Person - Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14888"/>
        <source>Gender:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="14974"/>
        <source>Person - Salutation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="386"/>
        <source>Salutation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="15323"/>
        <source>Person - Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="491"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="15672"/>
        <source>Person - Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="3176"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="16021"/>
        <source>NOTICE: To download and update the master data of nationality you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="16153"/>
        <source>Person - Eye color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="3088"/>
        <source>Eye color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="16502"/>
        <source>Person - Hair color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2992"/>
        <source>Hair color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="16851"/>
        <source>Person - Religion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="17114"/>
        <source>Religion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="17200"/>
        <source>Person - Relationship Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="1790"/>
        <source>Relationship status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="17549"/>
        <source>Person - Relationship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2310"/>
        <source>Relationship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="17898"/>
        <source>NOTICE: To download and update the master data of relationship you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18036"/>
        <source>Stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18053"/>
        <source>Stamp - Blahh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18070"/>
        <source>Coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18081"/>
        <source>Coin - Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18351"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18541"/>
        <source>NOTICE: To download and update the master data of type of coin you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18562"/>
        <source>Coin - Alloy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="18832"/>
        <source>Alloy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19503"/>
        <source>NOTICE: To download and update the master data of alloy of coin you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19043"/>
        <source>Coin - Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19065"/>
        <source>Currency:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19313"/>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19524"/>
        <source>Coin - Manufacturing procedure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="19859"/>
        <source>Manufacturing procedure:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20548"/>
        <source>Shortcut:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20022"/>
        <source>NOTICE: To download and update the master data of manufacturing procedure of coin you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20154"/>
        <source>Coin - Degree of preservation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20492"/>
        <source>Degree of preservation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20655"/>
        <source>NOTICE: To download and update the master data of degree of preservation of coin you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="20787"/>
        <source>Coin - Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21125"/>
        <source>Mint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21181"/>
        <source>Mint mark:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21288"/>
        <source>NOTICE: To download and update the master data of degree mint of coin you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21420"/>
        <source>General - Age release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21690"/>
        <source>Age release:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21769"/>
        <source>NOTICE: To download and update the master data of age release of general you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="21901"/>
        <source>General - Packaging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22177"/>
        <source>Packaging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22256"/>
        <source>NOTICE: To download and update the master data of packaging you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22394"/>
        <source>Place of store</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22405"/>
        <source>General - Media type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22675"/>
        <source>Media type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22754"/>
        <source>NOTICE: To download and update the master data of media type you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="22886"/>
        <source>General - Media manufacturer /-brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="23156"/>
        <source>Media manufacturer /-brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="23235"/>
        <source>NOTICE: To download and update the master data of media manufacturer /-brand you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="23367"/>
        <source>General - Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2498"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="23817"/>
        <source>NOTICE: To download and update the master data of language you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="23854"/>
        <source>General - Award</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24136"/>
        <source>Award</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24215"/>
        <source>NOTICE: To download and update the master data of award you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24347"/>
        <source>General - Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24680"/>
        <source>Country:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24737"/>
        <source>NOTICE: To download and update the master data of country you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="24828"/>
        <source>General - Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25212"/>
        <source>NOTICE: To download and update the master data of region you need to be logged in your support account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/master_data.ui" line="25350"/>
        <source>Collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="64"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="218"/>
        <source>Created on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/pushbuttons_close_print_delete_save.ui" line="242"/>
        <source>Text Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="244"/>
        <source>Changed on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="19"/>
        <source> Registration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="124"/>
        <source>Server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="228"/>
        <source>User name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="278"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="325"/>
        <source>Database name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="401"/>
        <source>Port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="462"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="487"/>
        <source>You have two options. First, click on the connection profile in the upper combobox and select the database connection that you have saved. Second, if you haven&apos;t saved any connections, so click on browse and select an SQLite database. After all this, then click on next to connect to the database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="542"/>
        <source>Connection profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="612"/>
        <source>Update Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="688"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/db_login.ui" line="706"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/db_login.ui" line="773"/>
        <source>Einen Augenblick - die Anmeldung läuft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/feedback.ui" line="19"/>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/feedback.ui" line="64"/>
        <source>Your Criticism, suggestions and your constructive feedback about this program are always welcome. Here you can send us your personal matter.

Please note to things: 
- First: Onlye use this form to send us your feedback. Our team will only accept feedback from this form . 
 
- Second: Due to the high volume of feedback we receive, we can only guarantee responses to GitKraken Pro support emails. However, we do read and review all feedback messages!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/feedback.ui" line="86"/>
        <source>Your feedback / criticism / suggestions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/feedback.ui" line="108"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/feedback.ui" line="153"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/getting_finiehd_wizard.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/getting_finiehd_wizard.ui" line="20"/>
        <source>Details:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="3298"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="148"/>
        <source>Subject:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/note.ui" line="168"/>
        <source>Note:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="14"/>
        <source>Person Profil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="62"/>
        <source>General information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="84"/>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="113"/>
        <source>Additional  middle names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="204"/>
        <source>Birth name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="295"/>
        <source>Last name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="594"/>
        <source>Artist&apos;s name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="692"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="792"/>
        <source>Name normally used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="898"/>
        <source>Body height (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="983"/>
        <source>Birthplace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="1070"/>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2697"/>
        <source>Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2702"/>
        <source>01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2707"/>
        <source>02</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2712"/>
        <source>03</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2717"/>
        <source>04</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2722"/>
        <source>05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2727"/>
        <source>06</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2732"/>
        <source>07</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2737"/>
        <source>08</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2742"/>
        <source>09</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2747"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2752"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2757"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2762"/>
        <source>13</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2767"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2772"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2777"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2782"/>
        <source>17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2787"/>
        <source>18</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2792"/>
        <source>19</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2797"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2802"/>
        <source>21</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2807"/>
        <source>22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2812"/>
        <source>23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2817"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2822"/>
        <source>25</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2827"/>
        <source>26</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2832"/>
        <source>27</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2837"/>
        <source>28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2842"/>
        <source>29</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2847"/>
        <source>30</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2852"/>
        <source>31</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2870"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2875"/>
        <source>January</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2880"/>
        <source>February</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2885"/>
        <source>March</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2890"/>
        <source>April</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2895"/>
        <source>May</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2900"/>
        <source>June</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2905"/>
        <source>July</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2910"/>
        <source>August</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2915"/>
        <source>September</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2920"/>
        <source>October</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2925"/>
        <source>November</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2930"/>
        <source>December</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2965"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="1432"/>
        <source>death-day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="1883"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="1985"/>
        <source>religious preference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2062"/>
        <source>Additional general information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="3441"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2375"/>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="2633"/>
        <source>Wedding anniversary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/person_profile.ui" line="3333"/>
        <source>Photo</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="xarphus/gui/about.ui" line="19"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/about.ui" line="137"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/about.ui" line="292"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/about.ui" line="460"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/about.ui" line="490"/>
        <source>Philosophy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/about.ui" line="517"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="465"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="14"/>
        <source>Connection profile manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="20"/>
        <source>Manage connection profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="134"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="169"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="204"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="239"/>
        <source>Profile Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="373"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="427"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="447"/>
        <source>Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="476"/>
        <source>Search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="491"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="561"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="579"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="610"/>
        <source>Refresh list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="645"/>
        <source>Genre:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="672"/>
        <source>Cnancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="695"/>
        <source>Apply change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/connection_profiles.ui" line="724"/>
        <source>Xarphus stores information about your connections. The passwords aren&apos;t saved for security reasons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="29"/>
        <source>General information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="61"/>
        <source>Medium-Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="88"/>
        <source>Medium-Brand:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="117"/>
        <source>Medium-Package:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="147"/>
        <source>Depository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="171"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="210"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="218"/>
        <source>The following objects are stored on this medium:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="253"/>
        <source>Personal information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="267"/>
        <source>The medium is lent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="296"/>
        <source>The medium is in possession</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="316"/>
        <source>To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="362"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="404"/>
        <source>Until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/medium.ui" line="444"/>
        <source>Memo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/update.ui" line="19"/>
        <source>Updater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/update.ui" line="89"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/update.ui" line="117"/>
        <source>What&apos;s new?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/update.ui" line="145"/>
        <source>Cnacel loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/update.ui" line="160"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="77"/>
        <source>Value side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="278"/>
        <source>General  information</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="327"/>
        <source>Welchen originalen Titel trägt der Film?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="347"/>
        <source>Inscription of the edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="907"/>
        <source>Weches Farbformat hat dieser Film?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="434"/>
        <source>Type of coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1269"/>
        <source>Anlagemünze</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1274"/>
        <source>Anlagemünzensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1279"/>
        <source>Banknote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1284"/>
        <source>Banknotensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1289"/>
        <source>Gedenkmünze</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1294"/>
        <source>Gedenkmünzensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1299"/>
        <source>Kursmünze</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1304"/>
        <source>Kursmünzensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1309"/>
        <source>Medaille</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1314"/>
        <source>Medaillensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1319"/>
        <source>Numisblatt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1324"/>
        <source>Numisbrief</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1329"/>
        <source>Sondermünze</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1334"/>
        <source>Sondermünzensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1339"/>
        <source>Umlaufmünze</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1344"/>
        <source>Umlaufmünzensatz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="606"/>
        <source>Material / Legierung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="641"/>
        <source>Aluminium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="646"/>
        <source>Bronze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="651"/>
        <source>Silber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="656"/>
        <source>Gold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="661"/>
        <source>Kupfer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="666"/>
        <source>Kupfernickel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="671"/>
        <source>Messing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="676"/>
        <source>Neusilber</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="681"/>
        <source>Palladium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="686"/>
        <source>Platin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="691"/>
        <source>Stahl</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/movie_general.ui" line="1113"/>
        <source>Wie würden Sie die Genre bewerten? Passt der Film gut in dieses Genre?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="762"/>
        <source>processing method</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="863"/>
        <source>Welchen Titel trägt der Film in Ihrer Sprache?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="883"/>
        <source>label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="926"/>
        <source>In welchem Jahr wurde der Film produziert (Produktionsjahr)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="946"/>
        <source>Par</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1035"/>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/coin_general.ui" line="1114"/>
        <source>Das Ausgabeland der Münze oder Medaille, wie zum Beispiel Deutschland, Österreich, der Schweiz etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1140"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general.ui" line="1241"/>
        <source>Alloy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="46"/>
        <source>Detailed information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="78"/>
        <source>Overviews of coin(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="90"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="95"/>
        <source>Year of minting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="173"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="191"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/coin_general_details.ui" line="208"/>
        <source>Review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="32"/>
        <source> General Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/movie_general.ui" line="1519"/>
        <source>Welchen Titel trägt der Film in Ihrer Sprache? Geben Sie den offiziellen in ihrer Sprache  übersezten Titel des Films ein.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="78"/>
        <source>Title (Translated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="336"/>
        <source>country of production</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="589"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="789"/>
        <source>color Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="951"/>
        <source>Tonformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="1063"/>
        <source>Genre  Evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="1298"/>
        <source>Total Evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/movie_general.ui" line="1348"/>
        <source>Wie würden Sie den Film insgesamt bewerten?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="1547"/>
        <source>Original Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="1802"/>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="2006"/>
        <source>More general information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="2180"/>
        <source>Alternative Title (Translated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="2541"/>
        <source>Alternative Title (Original)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/movie_general.ui" line="2814"/>
        <source>Film-Poster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="153"/>
        <source>Always ask if the program is to be closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="182"/>
        <source>Show toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="195"/>
        <source>Show statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="226"/>
        <source>Select the language that should be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="240"/>
        <source>German (Germany - Sie)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="250"/>
        <source>German (Germany - Du)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="260"/>
        <source>English (UK)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="270"/>
        <source>English (US)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="315"/>
        <source>Style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="345"/>
        <source>Automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="381"/>
        <source>Scheme color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="411"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="416"/>
        <source>Classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="421"/>
        <source>Dark (modern)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="426"/>
        <source>Dark (old)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="431"/>
        <source>Dark (metal)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="436"/>
        <source>Dracula</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="441"/>
        <source>ida-skin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="466"/>
        <source>Always ask if data items should be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="546"/>
        <source>Pick up the appropriate category which you want to include in your management.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="577"/>
        <source>Select all Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="80"/>
        <source>Film</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="616"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="623"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="630"/>
        <source>Coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="637"/>
        <source>Stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="644"/>
        <source>Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="651"/>
        <source>Person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="658"/>
        <source>Video game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="665"/>
        <source>Quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="672"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="679"/>
        <source>Wine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="692"/>
        <source>Lending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="722"/>
        <source>Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="764"/>
        <source> Desktop-Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="815"/>
        <source>SQLite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="839"/>
        <source>Path to the SQLite file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="933"/>
        <source>Use an existing SQLite database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="949"/>
        <source>Create a new SQLite database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="998"/>
        <source>Microsoft Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1018"/>
        <source>Create a new Microsoft Access database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1034"/>
        <source>Use an existing Microsoft Access database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1057"/>
        <source>Path to the Microsoft Access file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1185"/>
        <source> Server-Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1234"/>
        <source>MySQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1267"/>
        <source>PostgreSQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1300"/>
        <source>Microsoft SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1337"/>
        <source>Verification options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1359"/>
        <source>Ask whether the update file to install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1388"/>
        <source>Install the update file after downloading automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1417"/>
        <source>Install the update file on the next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1470"/>
        <source>Settings for downloading updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1521"/>
        <source>Save updates in the following folder temporarily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1623"/>
        <source>Address of the update server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1700"/>
        <source>Waiting for response from the server (timeout)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1755"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1831"/>
        <source>Server-version-address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/setting.ui" line="1958"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="356"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="37"/>
        <source>Search criteria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="55"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="75"/>
        <source>No selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="90"/>
        <source>Buch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="100"/>
        <source>Musik</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/search.ui" line="110"/>
        <source>Münze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="120"/>
        <source>Kontakt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="130"/>
        <source>Notiz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="140"/>
        <source>Videospiel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="150"/>
        <source>Zitat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="160"/>
        <source>Briefmarke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="170"/>
        <source>Ausleihe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="193"/>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="248"/>
        <source>Criterion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="304"/>
        <source>Search item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="323"/>
        <source>Enter a search term...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/search.ui" line="419"/>
        <source>Search result(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadDataItem</name>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1770"/>
        <source>Error...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1907"/>
        <source>We are sorry for this error, but your connect attempt failed! 

Unable to connect to the database. 

Be sure your are connected to the database. Please, connect to the database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1905"/>
        <source>No database connection!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1919"/>
        <source>Ups, an error has occurred. We are sorry about, but your query attempt failed! 
 Unable to connect to the database. This program losts the connection. 

 Please... 
 ... check that MySQL is still running. 
... check that your network connection online. 
 ... restart your MySQL server and try again. 
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1928"/>
        <source>ERROR: Lost connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="2366"/>
        <source>We are sorry, but your enter attempt failed! 

Reason: You aren&apos;t connected to the database. Please, connect to the database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="2352"/>
        <source>We are sorry, but your enter attempt failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="2157"/>
        <source>We are sorry, but your enter attempt failed! 
There is a duplicate entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="2161"/>
        <source>We are sorry, but your enter attempt failed! 

The field named &apos;name normally used&apos; must be filled! 

Please fill the missing field and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="2373"/>
        <source>We are sorry, but your enter attempt failed! 

Unable to delete an item. You must select an item to delete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="90"/>
        <source>Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="94"/>
        <source>Contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="105"/>
        <source>Film</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="120"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="134"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="153"/>
        <source>Coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="164"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="175"/>
        <source>Video game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="186"/>
        <source>Quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="197"/>
        <source>Stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="208"/>
        <source>Lending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="219"/>
        <source>Person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="230"/>
        <source>Wine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="259"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="266"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="283"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="293"/>
        <source>Migrate database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="310"/>
        <source>Extras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="322"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="372"/>
        <source>Add contact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="382"/>
        <source>Add film</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="392"/>
        <source>Add series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="402"/>
        <source>Add album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="412"/>
        <source>Add single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="422"/>
        <source>Add record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="432"/>
        <source>Minimize to the system tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="442"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="452"/>
        <source>Add comic book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="462"/>
        <source>Add book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="472"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="482"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="495"/>
        <source>Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="508"/>
        <source>Send feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="518"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="528"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="538"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="548"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="558"/>
        <source>Add single song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="568"/>
        <source>Add magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="578"/>
        <source>Add newspaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="588"/>
        <source>Add coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="598"/>
        <source>Add note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="608"/>
        <source>Add video game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="618"/>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="628"/>
        <source>Sign up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="701"/>
        <source>Sign in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="648"/>
        <source>Frequently asked questions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="658"/>
        <source>Release notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="668"/>
        <source>Add quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="678"/>
        <source>Close all windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="688"/>
        <source>Add stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="946"/>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="716"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="721"/>
        <source>Packaging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="726"/>
        <source>Regional code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="731"/>
        <source>FSK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="736"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="746"/>
        <source>Aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="751"/>
        <source>Type of version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="756"/>
        <source>Manufacturer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="766"/>
        <source>Depository </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="776"/>
        <source>Audio-format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="781"/>
        <source>Media type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="791"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="801"/>
        <source>Award</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="811"/>
        <source>Land</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="816"/>
        <source>Eye color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="821"/>
        <source>Hair color </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="826"/>
        <source>Release / Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="839"/>
        <source>User account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="852"/>
        <source>Sign out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="860"/>
        <source>Migrating to ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="865"/>
        <source>SQLite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="870"/>
        <source>Microsoft Access (*.mdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="875"/>
        <source>MySQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="880"/>
        <source>Microsoft SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="885"/>
        <source>PostgreSQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="890"/>
        <source>Microsoft Access (*.accdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="895"/>
        <source>Add miscellany</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="900"/>
        <source>
Add journal article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="906"/>
        <source>Add newspaper article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="911"/>
        <source>Add eBook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="916"/>
        <source>Add audio book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="926"/>
        <source>Add lending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="936"/>
        <source>Add person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="996"/>
        <source>Age rating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="956"/>
        <source>Add wine</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/mdi.ui" line="961"/>
        <source>Kollektion [Film] hinzufügen</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/mdi.ui" line="966"/>
        <source>Kollektion [Serie] hinzufügen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="976"/>
        <source>Collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="1001"/>
        <source>show_sub_window_list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="1006"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="1016"/>
        <source>Master Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="xarphus/gui/mdi.ui" line="1021"/>
        <source>Veröffentlichung</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="1026"/>
        <source>Kollektion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/gui/mdi.ui" line="1036"/>
        <source>Connection profiles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MasterData_Window</name>
    <message>
        <location filename="xarphus/frm_master_data.py" line="218"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="115"/>
        <source>Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="312"/>
        <source>Award</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="316"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="308"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="123"/>
        <source>General manufacturer /-brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="125"/>
        <source>Media Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="292"/>
        <source>Packaging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="288"/>
        <source>Age release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Salutation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="139"/>
        <source>Hair Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="141"/>
        <source>Eye Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Religion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="145"/>
        <source>Relationship Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Relationship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="259"/>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>Studio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="251"/>
        <source>Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>SPARS Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Film production company</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="167"/>
        <source>Film rentals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="169"/>
        <source>Film distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Film studio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="171"/>
        <source>Film resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="172"/>
        <source>Film presenting format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="174"/>
        <source>Film rgional code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="175"/>
        <source>film region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="177"/>
        <source>Film aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="178"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="180"/>
        <source>Film dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="182"/>
        <source>Film edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="184"/>
        <source>Film version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="186"/>
        <source>Film videonorm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="188"/>
        <source>Film audioformat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="190"/>
        <source>Film audio channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="192"/>
        <source>Film colour format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="194"/>
        <source>Film video format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="196"/>
        <source>Film sound system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="251"/>
        <source>Binding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Alloy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="208"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Manufacturing procedure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="214"/>
        <source>Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Degree of preservation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Mint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="217"/>
        <source>Mint mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Film / Serie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Sound system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Regional code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Audio format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Audio channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="251"/>
        <source>Color format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>TV Norm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Video format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Type of version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Version / Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>film distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="222"/>
        <source>Movie rentals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="241"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="251"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="251"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="259"/>
        <source>Video game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Eye color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Hair color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="263"/>
        <source>Relationship status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="275"/>
        <source>Stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="279"/>
        <source>Type of coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="296"/>
        <source>Place of storage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="300"/>
        <source>Type of media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="304"/>
        <source>media manufacturer /-brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="320"/>
        <source>Region / Place</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="1735"/>
        <source> Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="1843"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="1849"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="1881"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="2502"/>
        <source>Searching for records. This should only take a moment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="6247"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="6251"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="13007"/>
        <source>Error Deleting an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="7262"/>
        <source>We are sorry, but your enter attempt failed! 

Unable to edit an item. You must select an item to edit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12114"/>
        <source>Result(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12658"/>
        <source>Confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12193"/>
        <source>The item has been saved into the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12989"/>
        <source>Conformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12989"/>
        <source>Are you sure you want to permamently delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="12989"/>
        <source>NOTICE: By deleting, the selected record will be irrevocably removed! Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_master_data.py" line="13011"/>
        <source>We are sorry, but your enter attempt failed! 

Unable to delete an item.. You must select an item to delete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PersonProfile_Window</name>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="661"/>
        <source>Artist name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="664"/>
        <source>Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="667"/>
        <source>Name normally used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="670"/>
        <source>Additional middle names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="673"/>
        <source>Birthname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="676"/>
        <source>Lastname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="679"/>
        <source>Salutation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="682"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="685"/>
        <source>Religion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="688"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="691"/>
        <source>Body height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="694"/>
        <source>Birthplace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="697"/>
        <source>Birthday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="700"/>
        <source>Deathday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="703"/>
        <source>Relationship status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="706"/>
        <source>Relationship</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="709"/>
        <source>Nationality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="712"/>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="715"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="718"/>
        <source>Eye color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="721"/>
        <source>Hair color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="724"/>
        <source>Wedding anniversary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="737"/>
        <source>Foto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="933"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="939"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="967"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="984"/>
        <source> Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1285"/>
        <source>Prominat und Kontakt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1301"/>
        <source>Schauspieler/in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1315"/>
        <source>Adressbuch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1340"/>
        <source>Person Profil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1428"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1429"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1860"/>
        <source>Confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1822"/>
        <source>The person has been saved into the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1822"/>
        <source>Do you want to save another people?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1860"/>
        <source>The person has been changed into the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1922"/>
        <source>Conformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1922"/>
        <source>Are you sure you want to permamently delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1922"/>
        <source>NOTICE: By deleting, the selected item will be irrevocably removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1940"/>
        <source>Error Deleting an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_person_profile.py" line="1944"/>
        <source>We are sorry, but your enter attempt failed! 

Unable to delete an item.. You must select an item to delete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Search_Window</name>
    <message>
        <location filename="xarphus/frm_search.py" line="92"/>
        <source>A critical internal application error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="94"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="127"/>
        <source>Fehler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="129"/>
        <source> Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="149"/>
        <source>An internal critical error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="149"/>
        <source>

Details: 
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="149"/>
        <source>
Returncode: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="358"/>
        <source>No selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="285"/>
        <source>Film</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="285"/>
        <source>Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="293"/>
        <source>Magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="293"/>
        <source>Book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="293"/>
        <source>newspaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="302"/>
        <source>Album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="302"/>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="302"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="311"/>
        <source>Coin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="318"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="318"/>
        <source>Lastname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="326"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="334"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="334"/>
        <source>Titel Videogame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="342"/>
        <source>Title quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="342"/>
        <source>Quotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="350"/>
        <source>Title stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="350"/>
        <source>Stamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="358"/>
        <source>Title lending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_search.py" line="358"/>
        <source>Lending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskDataManipulation</name>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="222"/>
        <source>Error...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="318"/>
        <source>Please wait while record(s) is/are being fetched.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="337"/>
        <source>RRRRRRELOOOOAD!! Please wait while fetching record(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="487"/>
        <source>Processing records for population.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="514"/>
        <source>Populating all missing records.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="518"/>
        <source>The records have been populated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1392"/>
        <source>You can&apos;t work without a database connection. Please connect to the database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1393"/>
        <source>No database connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="636"/>
        <source>Oops! We are sorry, something went wrong. An internal error has occurred. 
The error has been logged to the log-file. 

Choose &apos;Ignore&apos; to continue running in an inconsistent state. Choose &apos;Break&apos; to halt the application.  By choosing &apos;Break&apos; it will result in the loss of all unsaved data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="642"/>
        <source>An internal error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="686"/>
        <source>The missing records were saved to the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1507"/>
        <source>We are sorry, but your enter attempt failed! 
There is a duplicate entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="712"/>
        <source>An error has occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="789"/>
        <source>Connecting to the support-server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="790"/>
        <source>Try to log in with username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="801"/>
        <source>Logged in successfully with username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="802"/>
        <source>Connected to the support-server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="804"/>
        <source>Online records are loaded and compared with the records in your database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="824"/>
        <source>Matching the records is done.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="925"/>
        <source>There is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="926"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="927"/>
        <source>record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="941"/>
        <source>Start updating the master data of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1441"/>
        <source>Please wait while manipulating record(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="986"/>
        <source>Connection error when connecting to the HTTPS site. Unable to connect to remote host.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="990"/>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1002"/>
        <source>Timeout</source>
        <comment>Request timed out. The remote host did not respond timely.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1006"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1018"/>
        <source>Unable to connect to remote host because of a SSL error. It is likely that your system cannot verify the validityof the certificate. The remote certificate is either self-signed, or the remote server uses SNI. See the wiki for more information on this topic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1039"/>
        <source>SSL Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1051"/>
        <source>A valid URL is required to make a request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1055"/>
        <source>Missing URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1067"/>
        <source>The URL provided is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1071"/>
        <source>Invalid URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1083"/>
        <source>There is a URL without HTTP or HTTPS schema!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1087"/>
        <source>Invalid Schema</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1107"/>
        <source>Remote server error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1113"/>
        <source>Unauthorized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1117"/>
        <source>Something is wrong. The log-in to your support account has failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1117"/>
        <source>Maybe the username and/or password you provided are incorrect. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1125"/>
        <source>Local client error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1136"/>
        <source>Request raised HTTP error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1166"/>
        <source>There are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1167"/>
        <source>records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1169"/>
        <source>Checking is done.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1171"/>
        <source>new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1171"/>
        <source>in the online data service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1175"/>
        <source>Saving all missing records to the database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1192"/>
        <source>No updates for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1193"/>
        <source>availble</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1482"/>
        <source>Confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1309"/>
        <source>The movie has been saved into the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1310"/>
        <source>Would you like to save another movie?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1638"/>
        <source>We are sorry, but your enter attempt failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1538"/>
        <source>Please fill out the missing field and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1339"/>
        <source>Error: Duplicate entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1511"/>
        <source>We are sorry, but your enter attempt failed! 

The field named &apos;name normally used&apos; must be filled! 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1517"/>
        <source>We are sorry, but your enter attempt failed! 

The enter may not be empty or contain only spaces. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1356"/>
        <source>Error: Database is blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1358"/>
        <source>We are sorry, but your enter attempt failed! 
This application can&apos;t insert your record, because the database (or one of its segments) is locked. That means, the database isn&apos;t accessible.

Its possible that another application is accessing this database currently. Please verify you have write permissions to this database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1658"/>
        <source>We are sorry, but your enter attempt failed! 

Reason: You aren&apos;t connected to the database. Please, connect to the database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1577"/>
        <source>We are sorry, but an unknown error has occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1578"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1483"/>
        <source>The record has been changed into the database successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1545"/>
        <source>We are sorry, but your enter attempt failed! 

You aren&apos;t connected to the database. Please, connect to the database and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1551"/>
        <source>We are sorry, but your enter attempt failed! 

An input is invalid. The enter may not be empty or contain only spaces. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1558"/>
        <source>Oops, something went wrong. 

An internal error was experienced. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/subclass_master_data_load_data_item.py" line="1667"/>
        <source>We are sorry, but your enter attempt failed! 

Unable to delete an item.. You must select an item to delete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Update_Window</name>
    <message>
        <location filename="xarphus/frm_update.py" line="312"/>
        <source>Ein Fehler ist aufgetreten.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="316"/>
        <source> Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="588"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="406"/>
        <source>The update file is password protected. Please enter the password in following window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="428"/>
        <source> Message </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="418"/>
        <source>The update file has been downloaded successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="428"/>
        <source>The update file is corrupted. 
This can have several reasons: 
(1): The update file is defective. 
(2): There are incorrect files in the update file.(3): The update file has not been downloaded fully 

To fix the error: 
Stellen Sie sicher, dass Sie &#xc3;&#x83;&#xc2;&#xbc;ber eine stabile Make sure that you have a stable internet connection and download the update file again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="443"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="446"/>
        <source>An error occurred while downloading the update file. 

Possible reasons: 
(1): Your Internet connect is currently very unstable. In this case, make sure you have a stable internet connection and download the update file (in a later stage) again.
(2): The update server has just failed. In this case, you should try downloading the update file again later. 
(3): Connection timed out with the update server. The update server reacts / responds very much delayed. 
(4): It is quite possible that a firewall or a virus scanner on their system blocked to download the update file. Check your firewall or  virus scanner. 

Would you like to try again?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="460"/>
        <source>Try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="463"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="537"/>
        <source>Renewal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="537"/>
        <source>What&apos;s new?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="xarphus/frm_update.py" line="588"/>
        <source>Downloading was canceled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
