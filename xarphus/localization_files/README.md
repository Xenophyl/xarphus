# README #
---------------------------

## Requirements ##
==================
### Do you want to help us to translate ``xarphus``, because you know and speak a language well that ``xarphus`` doesn't offer?? ###

First, you need a program called Qt Linguist - its open source and free. Please download and install it.

[download_qt_liguist]: http://download.qt.io/official_releases/qt/4.8/4.8.7/ "Qt Liguist"

Qt Linguist  4.8.6 [download][download_qt_liguist] 


## Instructions (additional notes) ##
===================================

All the texts of ``xarphus`` are here. You can download the file, apply your translations and send us via mail (to xxx@xxx.com). So your translations can be integrated into ``xarphus``.

### What I have to do? ####

IMPORTANT: All files with the extension ``*.ts`` are important for you - no other files.

#### Step 1 ####

You pick the appropriate language up that you want to translate.
(For example , the new text of the language is Spanish and you want to translate it. You pick the ``spanisch.ts``-file up.)

#### Step 2 ####

You click on the appropriate translate-file. On the next page you will see the content (source code) of this file.
Don't be scare off. It is just an XML-file. 

#### Step 3 ####

You click on the button named ``raw`` - above the content / source code. You will get the raw content of this file.

#### Step 4 ####

Click with the right-mouse-button in the window and go to ``save as``.

#### Step 5 ####

Now you can save the file on you machine. But be careful. Maybe its possible that your machine tries to save the file as a text file.
(For example: spanish.ts.txt)
In this case you have to delete the ``*.txt``- extension and save the file as ``spanish.ts``.

#### Step 6 ####

Well you managed it. You can open this file with Qt Linguist.

__IMPORTANT:__ Don't release it. Just save it, when you are done with translation. 

### How to use Qt Linguist? ####

[youtube_qt_linguist]: https://www.youtube.com/watch?v=GjWCnzpxRzs/ "How to update Qt Translation (.ts) files"

When you work for the first time with Qt Liguist on YouTube there are some tutorials that shows you how to use Qt Linguist. 
This clip "[How to update Qt Translation (.ts) files][youtube_qt_linguist]" shows you very quickly and easily in a short time how to work with Qt Liguist.
