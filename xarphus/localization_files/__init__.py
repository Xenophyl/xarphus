# import logging
#
# class Central_Logger(object):
#     def __init__(self):
#
#         LOG_FILENAME = 'PATH/TO/YOUR/LOGFILE.txt'
#         logging.basicConfig(filename=LOG_FILENAME,
#                             level=logging.DEBUG,
#                             level=logging.WARNING,
#                             level=logging.INFO,
#                             level=logging.ERROR,
#                             level=logging.CRITICAL
#                             )
#
#         self.logr = logging.getLogger('root')
#
#         with open(LOG_FILENAME, 'a') as log_file:
#             data = log_file.write(self.logr)
#
#             # do something with data
#
#
# from log_conf import Central_Logger
#
# def do_somethink(a, b):
#     Central_Logger.logr.info("The function (do_somethink is called")
#     Central_Logger.logr.info("The calculation starts")
#     result = a*b
#     Central_Logger.logr.info("The calculation is complete. The result is %s") & result
#     Central_Logger.logr.info("Return the result of the calculation") & result
#     return result