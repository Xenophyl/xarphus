#!/usr/bin/env python
#-*- coding:latin1 -*-


FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import webbrowser
import inspect

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QDialog
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile
from PyQt4 import QtCore

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################
'''
Private modules are imported.
'''
#from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
#self.path_mdi_form = os.path.abspath(".")
#self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

class Medium_Window(QDialog):
    def __init__(self,
                 save_config,
                 info_app,
                 set_custom_logger,
                 parent):
        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/medium.ui")

        self.info_app = info_app

        self.custom_logger = set_custom_logger

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created successfully - (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END CLASS (" + Medium_Window.__name__ + ") - (" + FILE_NAME + ")")

        self.set_get_settings = save_config
        self.get_app_info = info_app

        self.custom_logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        UI_PATH.open(QFile.ReadOnly)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened successfully - (" +  FILE_NAME +")")
        self.custom_logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        self.ui_medium = loadUi(UI_PATH, self)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded successfully - (" +  FILE_NAME +")")

        UI_PATH.close()

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed successfully - (" +  FILE_NAME +")")


        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_medium.setWindowFlags(flags)

        self.init_actions_button()

    def get_class_name(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return self.__class__.__name__


    def init_actions_button(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.ui_medium.pushButton_close.clicked.connect(self.close_window)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return


    def close_window(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.close()

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return
      
if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    '''
    Private modules are imported.
    '''
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manange_logging import set_custom_logger, create_log_file
    from core.config import ConfigurationSaver
    from info import info_app
    from core.manage_time import get_time_now
    
    ## Import compiled files
    from gui import ui_rc
    from images import images_rc
    from localization_files import translate_files_rc

    dict_custom_config = ConfigurationSaver()
    program_info = info_app()

    FILE_NAME = "frm_medium.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    create_log_file(LOG_FILE_PATH, program_info)
    
    app = QApplication(sys.argv)
    
    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)
                 
    window = Medium_Window(dict_custom_config,
                          program_info,
                          set_custom_logger(program_info),
                          None)
    window.show()
    sys.exit(app.exec_())
