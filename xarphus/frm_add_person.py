#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import inspect

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QMdiSubWindow
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile

'''
Private modules are imported.
'''
from xarphus.core.manange_logging import set_custom_logger


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
#self.path_mdi_form = os.path.abspath(".")
#self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')
class AddPerson_Window(QWidget):

    def __init__(self,
                 func_up_to_date,
                 update_sublist,
                 add_person,
                 select_person,
                 save_config,
                 info_app,
                 parent):

        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.add_person = add_person
        self.select_person = select_person
        self.get_app_info = info_app

        self.set_get_settings = save_config

        UI_PATH = QFile(":/ui_file/add_person.ui")

        self.custom_logger = set_custom_logger(info_app)

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created - (" + __name__ + ")")
        self.custom_logger.info(
            "-------------------------------------- END CLASS (" + AddPerson_Window.__name__ + ") - (" + __name__ + ")")

        UI_PATH.open(QFile.ReadOnly)
        self.ui_add_person = loadUi(UI_PATH, self)
        UI_PATH.close()

        self.init_gui()
        self.set_actions_radiobuttons()
        self.set_language_add_person()
        self.init_actions_button()

    def init_gui(self):
        pass

    def set_language_add_person(self):
        # Title
        self.ui_add_person.setWindowTitle(self.set_language.dict_ui_add_person_address_book["add_person_address_book_title"])
        # QLabel
        self.ui_add_person.label_you_want_text.setText(self.set_language.dict_ui_add_person_address_book["label_you_want_text"])
        # QradioButton
        self.ui_add_person.radioButton_new_person.setText(self.set_language.dict_ui_add_person_address_book["radioButton_new_person"])
        self.ui_add_person.radioButton_existing_person.setText(self.set_language.dict_ui_add_person_address_book["radioButton_existing_person"])
        # QpushButton
        self.ui_add_person.pushButton_close.setText(self.set_language.dict_ui_add_person_address_book["pushButton_close"])
        self.ui_add_person.pushButton_next.setText(self.set_language.dict_ui_add_person_address_book["pushButton_next"])
#--------------------------------------------------------------------------------------
    def list_all_radiobutton(self):
        list_radiobutton = (self.ui_add_person.radioButton_new_person,
                            self.ui_add_person.radioButton_existing_person
                            )
        return  list_radiobutton
#--------------------------------------------------------------------------------------
    def set_actions_radiobuttons(self):
        self.ui_add_person.radioButton_new_person.toggled.connect(self.set_pushbutton_enable)
        self.ui_add_person.radioButton_existing_person.toggled.connect(self.set_pushbutton_enable)
#--------------------------------------------------------------------------------------
    def set_pushbutton_enable(self, enabled):
        if enabled:
            self.ui_add_person.pushButton_next.setEnabled(True)
        else:
            self.ui_add_person.pushButton_next.setEnabled(False)

    def selected_next(self):
        if self.ui_add_person.radioButton_new_person.isChecked():
            self.add_person()
            self.close_parent()
        if self.ui_add_person.radioButton_existing_person.isChecked():
            self.select_person()
            self.close_parent()


#--------------------------------------------------------------------------------------
    def init_actions_button(self):
        self.ui_add_person.pushButton_close.clicked.connect(self.close_parent)
        self.ui_add_person.pushButton_next.clicked.connect(self.selected_next)

    def close_parent(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + __name__ + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        return
