
#!/usr/bin/env python
#-*- coding:utf-8 -*-

from time import time
from sqlalchemy.exc import SQLAlchemyError

try:
    from core.managed_defined_session import *

    from xarphus.core.manage_db_connection import GeneralRegion

except ImportError:
    from core.managed_defined_session import *

    from core.manage_db_connection import GeneralRegion

#   Provide a transactional scope around a series of operations.
DBSession = None
N = 0

def interts_bulk_save_objects(given_list = None):
    N = 0
    t0 = time()

    DBSession = ScopedSession()

    object_list = []

    for element in given_list:

        customer = GeneralRegion()
        customer.region = element.strip()#.decode("utf8", errors="replace").strip()
        object_list.append(customer)

        N += 1

    #DBSession.add_all(object_list)
    DBSession.bulk_save_objects(object_list)
    DBSession.commit()

    print "SqlAlchemy ORM: Total time for inserting " + str(N) + " records " + str(time() - t0) + " secs"


def test_sqlalchemy_orm(given_list = None):
    N = 0
    DBSession = ScopedSession()
    t0 = time()
    
    for element in given_liste:#for i in range(n):
        customer = GeneralRegion()
        customer.region = element.strip()#.decode("utf8", errors="replace").strip()
        
        try:
            DBSession.add(customer)
            DBSession.commit()
        except SQLAlchemyError as err:
            # Before we rise an exception, we call close()
            # on the current session which, has the effect of 
            # releasing any connection/transactional resources 
            # owned by the Session first, then discarding the 
            # Session itself. After that, we empty the attribute,
            # in next step we save the class of sqlalchemy.orm.session.Session
            # in the attribute.

            DBSession.close()
##                DBSession = None
##                DBSession = ScopedSession()


        N += 1

    print "SqlAlchemy ORM: Total time for " + str(N) + " records " + str(time() - t0) + " secs"