#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_update.py"

import os
import sys
import requests
import inspect
from inspect import currentframe

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QDialog


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class PasswordRequired_Window(QDialog):
    def __init__(self, zip_file, save_config, parent=None):

        QDialog.__init__(self, parent)

        self.zip_file_p = zip_file

        self.set_get_settings = save_config

        logger.info("The instance of the class of " + self.get_class_name() + "() is created - (" +  __name__ +")")

        UI_PATH = QFile(":/ui_file/password_required.ui")


        try:
            logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            UI_PATH.open(QFile.ReadOnly)
            logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  __name__ +")")
            self.ui_password_required = loadUi(UI_PATH, self)
            logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded - (" +  __name__ +")")
            UI_PATH.close()
            logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
        except ImportError as ImpErr:
            logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            logger.warning(ImpErr, " - (" +  __name__ +")")
        except Exception as ex:
            logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            logger.warning(str(ex), " - (" + str(__name__) +")")
        except:
            logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            logger.info(sys.exc_info()[0], " - (" +  __name__ +")")
            raise

        self.set_gui()
        self.set_action_buttons()

    def set_gui(self):
        self.ui_password_required.setWindowModality(Qt.ApplicationModal)
        self.ui_password_required.lineEdit_password.returnPressed.connect(self.pushButton_ok.click)

    def get_pwd(self):
        send_pwd = unicode(self.ui_password_required.lineEdit_password.text())
        return send_pwd

    def zwischen_schritt(self):
        me = unicode(self.get_pwd())
        self.set_get_settings.dict_set_settings["zip_pwd"] = me
        self.zip_file_p()
        self.close()

    def set_action_buttons(self):
        #self.ui_movie_general_details.pushButton_ok.clicked.connect(lambda: self.zip_file(self.get_pwd))
        self.ui_password_required.pushButton_ok.clicked.connect(self.zwischen_schritt)

    def get_class_name(self):
            return self.__class__.__name__

    def get_line_number(self):
        cf = currentframe()
        return cf.f_back.f_lineno - 1

