#-*- coding:latin1 -*-

FILE_NAME = __name__

from os import path

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QIcon, QFileDialog, \
    QPixmap, QTabWidget, QGridLayout, QTextEdit, QMenu,\
    QToolButton, QPushButton, QLabel, QFrame

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window

from xarphus.frm_coin_general_details import CoinGeneralDetails_Window
from xarphus.frm_coin_general import CoinGeneral_Window

from xarphus.core.manage_calculation import calculate_cover_poster



BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineCoinGeneralDetailsButtons_Window(QWidget):
    def __init__(self,
                 view_mode,
                 create_cover_viewer,
                 create_select_details_form,
                 create_coin_detail_form,
                 form_title,
                 image_path_collection,
                 configuration_saver_dict,
                 coin_id=None,
                 parent=None):
        '''
        NOTICE:
        =======
        This class loads and combines interfaces. Here loads three interfaces in total.

        PARAMETERS:
        ============

        :view_mode                  -   You get a number as a integer. The numbers
                                        are 0 and 1. The number 0 says, when a user
                                        decides to add a new coin data. Otherwise, 1
                                        says,the user wants to take a look an existing
                                        coin data and maybe he wants to edit or delete
                                        the data of a coin.

        :create_cover_viewer        -   You get an access to the function un the moduke
                                        named (mdi.py). This function starts the cover-window.

        :create_select_details_form -

        :create_coin_detail_form    -   Create a form, when the user wants to add details.

        :form_title                 -   This new created window get a title.

        :image_path_collection      -   In the module named (collection_paths_image.py). There
                                        are all paths of images or icons. Notice: All the paths
                                        of icons or images are used repeatedly. That's the reason
                                        why the paths were outsourced.

        :configuration_saver_dict   -   We have to create a protocol.

        :parent                     -

        :return                     -   None
        '''

        QWidget.__init__(self, parent)

        self.create_cover_viewer = create_cover_viewer
        self.create_select_details_form = create_select_details_form
        self.view_mode = view_mode
        self.create_coin_detail_form = create_coin_detail_form
        self.form_title = form_title
        self.image_path_collection = image_path_collection
        self.configuration_saver_dict = configuration_saver_dict
        self.coin_id = coin_id

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.film_title_name = ""

        self.create_combined_form()
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def create_combined_form(self):
        '''
            NOTICE:
            =======
            A new window is created from several subclasses.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Try to create an instance of CoinGeneral_Window()")

        self.coin_general = CoinGeneral_Window(self)

        logger.info("Try to create an instance of CoinGeneral_Window()")

        self.coin_general_details = CoinGeneralDetails_Window(self.create_coin_detail_form,
                                                              self)

        logger.info("Try to create an instance of PushbuttonsClosePrintDeleteSave_Window()")

        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self)

        '''
        This is a new window that combines two existing windows.
        That why we need a vertical layout. We add
        the given widget
        '''
        logger.info("Try to create an instance of QVBoxLayout()")

        vertical_layout = QVBoxLayout()

        logger.info("The instance of the class of  QVBoxLayout() is created successfully")

        logger.info("Try to add all the given widgets to QVBoxLayout()")

        vertical_layout.addWidget(self.coin_general)
        vertical_layout.addWidget(self.coin_general_details)
        vertical_layout.addWidget(self.pushbutton_close_print_delete_save)

        logger.info("All given widgets are added to to QVBoxLayout() successfully")

        '''
        We need a textEdit-widget that displays the
        user all  data of the coin in a overview
        '''

        logger.info("Try to add all the given widgets to QTextEdit()")

        textEdit_view = QTextEdit(self)
        '''
        The user shouldn't be able to edit all data here.
        In read-only mode, the user can still copy the text
        to the clipboard, or drag and drop the text. That
        is why the user can only read it
        '''
        logger.info("The instance of the class of  QTextEdit() is created successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        logger.info("Try to set the instance of QTextEdit() in read-only mode (True)")

        textEdit_view.setReadOnly(True)

        logger.info("The instance of QTextEdit is set in read-only mode (True) successfully")

        '''
        We need QGridLayout and create it. We add
        the given widget (textEdit_view). The top-left
        position is (0, 0) by default.
        '''

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_view = QGridLayout(self)

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_view.addWidget(textEdit_view, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        '''
        First we create QTabWidget()-class,
        then we need two tabs.
        '''
        logger.info("Try to create an instance of QTabWidget()")

        tab_widget = QTabWidget(self)

        logger.info("The instance of QTabWidget() successfully")

        logger.info("Try to create an instances of QWidget()")

        tab_coin_editing = QWidget(self)
        tab_coin_total_view = QWidget(self)

        logger.info("The instances of QWidget() successfully")

        '''
        We have to manage this tabs. so we set
        the layout manager for this widget to layout.
        First tab is set the layout that passed vertical_layout
        and the second tab is set layout, that is
        passed gridLayout_view
        '''

        logger.info("Try to set the layout manager for given widget to QVBoxLayout()")

        tab_coin_editing.setLayout(vertical_layout)

        logger.info("The given widget is set to QVBoxLayout() successfully")

        logger.info("Try to set the layout manager for given widget to QGridLayout()")

        tab_coin_total_view.setLayout(gridLayout_view)

        logger.info("The given widget is set to QGridLayout() successfully")

        '''
        Now we add tabs with the given page (tab_coin_editing and tab_coin_total_view) and label
        to the tab widget, and returns the index of the tab in the tab bar.
        '''
        logger.info("Try to add tabs with the given page and label to QTabWidget()")

        tab_widget.addTab(tab_coin_editing, self.tr("Information"))
        tab_widget.addTab(tab_coin_total_view, self.tr("Total view"))

        logger.info("All given tabs with the given page and label are added to QTabWidget() successfully")

        '''
        Finally we create a other one QGridLayout()-class,
        and we add the given widget (tab_widget). This
        layout is for the current form.
        '''
        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_combine_coin_general_details_buttons_window = QGridLayout(self)

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_combine_coin_general_details_buttons_window.addWidget(tab_widget, 0, 0)

        logger.info("All given widgets are added to QGridLayout() successfully")

        self.remove_tab(tab_widget)

        return

    def remove_tab(self, tab_obj):
        '''
            NOTICE:
            =======
            This method removes the tab at position index from this stack of widgets. 
            The page widget itself is not deleted..

            PARAMETERS:
            ===========
            :tab_obj    -   The given argumnent says which tabs at the position 
                            should be removed.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Revmove the tab with index 1")

        if self.coin_id is None:
            '''
                No existing movie was selected. The user wants to add a new
                movie to database. That is the reason why the user doesn't need the
                the tab, that shows the overview like a print report.
            '''
            tab_obj.removeTab(1)

        return

    def hide_widget(self):
        '''
            NOTICE:
            =======
            This method is used to hide a widget.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Hide the QFrame()object")

        frames = self.pushbutton_close_print_delete_save.findChildren(QFrame)

        for frame in frames:
            frame.close()

        return

    def set_font_color_label(self):
        '''
            NOTICE:
            =======
            This method is used to set color of text of a QLabel()-object.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Set color of text of QLabel")

        labels = self.pushbutton_close_print_delete_save.findChildren(QLabel)
        
        for label in labels:
            label.setStyleSheet("QLabel {color : red; }")

        return

    def set_enabled_push_button(self, 
                                 except_push_button=None):
        '''
            NOTICE:
            =======
            This method sets all QPushButton of the specified parentWidget. 

            PARAMETERS:
            ===========
            :except_push_button     -   This keyword argument gets a psuhbutton-widget
                                        that is not to be deactivated. As default there is 
                                        the keyword None.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set enabled for all push buttons")

        for push_button in self.pushbutton_close_print_delete_save.findChildren(QPushButton):
            push_button.setEnabled(False)

        if not except_push_button == None:

            for button_push in except_push_button:
                button_push.setEnabled(True)

        return


    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.set_window_title(self.form_title)

        self.set_window_icon(self.image_path_collection.coin_icon)

        self.init_clicked_signal_push_buttons()

        self.init_ToolButton_Menu()

        if self.coin_id is None:

            self.hide_widget()
            self.set_window_title(self.form_title)
            self.set_enabled_push_button(except_push_button=[self.pushbutton_close_print_delete_save.pushButton_close])

        else:

            self.set_font_color_label()
            self.set_window_title(self.tr("Coin..."))

        return

    def set_window_title(self, title=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowTitle(title)

        return

    def set_window_icon(self, image=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowIcon(QIcon(image))

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        logger.info("Set the signal and slots for QpuschButton()-object(s)")

        self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close)

        '''
        letst take a look what the user wants.
        '''
        if self.view_mode == 0:
            '''
            The user just wants to add a new record of a coin.
            '''
            self.set_enabled_push_button(except_push_button=[self.pushbutton_close_print_delete_save.pushButton_close])
        else:
            '''
            Maybe the user only wants take a look or
            the user wants to edit or delete the existing record.
            '''
            pass

        return

    def test_func(self):
        print "Test it"

    def create_select_details_window(self):
        self.create_select_details_form()
        return

    def init_ToolButton_Menu(self):
        #  we've created a menu
        menu = QMenu()
        '''
        addAction that will add menu items to your newly created QMenu.
        The actions are created by calling addAction and giving.
        It a string that will be used as the menu item
        text and a method (self.Action1 or self.Action2) that
        will be called if that menu item is selected.
        '''
        menu.addAction('View').setEnabled(False)
        menu.addSeparator()
        menu.addAction(QIcon(self.image_path_collection.details_view_icon_230x211), 'Detaills (No Symbols)', self.test_func)
        menu.addAction(QIcon(self.image_path_collection.details_view_pic_icon_259x218), 'Details (Symbols)', self.test_func)
        menu.addAction(QIcon(self.image_path_collection.setting_icon_16x16), 'Details ausw�hlen...', self.create_select_details_window)
        '''
        setPopupMode:               This property describes the way that
                                    popup menus are used with tool buttons

        TQToolButton.InstantPopup:  he menu is displayed, without delay,
                                    when the tool button is pressed.
                                    In this mode, the button's own action
                                    is not triggered.
        '''
        self.coin_general_details.toolButton_view.setMenu(menu)
        self.coin_general_details.toolButton_view.setPopupMode(QToolButton.InstantPopup)


    def resizeEvent(self,
                    resizeEvent):
        '''
            NOTICE:
            =======
            This method is reimplemented and modified to receive widget resize events which are passed in the event parameter. 
            When the widget is resized it already has its new geometry.

            Here its looks if the variable named (self.is_focused) is NOT True. By default this variable has the value None.
            If there is None do nothing.

            Next step its calls the function named (calculate_cover_poster), that returns a result, and
            sets the width (w) and the height (h) of the widget everytime: setFixedSize(width, height) 

            PARAMETERS:
            ===========
            :event      -       This method gets a resize events.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the fixed size of the widget")

        #if not self.is_focused:
        #    self.set_focus()

        result_groupBox_value_side = calculate_cover_poster(int(self.coin_general.width()), 6)

        self.coin_general.groupBox_value_side.setFixedSize(result_groupBox_value_side, self.coin_general.tabWidget_general_information.height())

        return

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        self.configuration_saver_dict.dict_set_general_settings["TempImagePathCoinValueSide"]=""

        return