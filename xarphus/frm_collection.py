#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys
from os.path import expanduser

from PyQt4.QtCore import Qt, QFile
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QWidget

from xarphus.core import get_news_in_version



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class Collection_Window(QWidget):
    def __init__(self, choice_function, func_up_to_date, update_sublist, cover_viewer, save_config, parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/overview_collection.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.cover_viewer = cover_viewer
        self.choice_function = choice_function

        self.set_get_settings = save_config

        from xarphus.css_template.css import Css_Syle
        self.css = Css_Syle()

        try:
            UI_PATH.open(QFile.ReadOnly)
            self.ui_overview_collection = loadUi(UI_PATH, self)
            UI_PATH.close()
        except Exception as ex:
            pass
        except:
            raise

        self.init_pushbutton()

    def init_pushbutton(self):
        self.ui_overview_collection.pushButton_close.clicked.connect(self.close_form)

    def closeEvent(self, event):
        pass
    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        print "STATUS [OK]  (", FILE_NAME, "): The function (close_form) was called"
    #--------------------------------------------------------------------------------------