#-*- coding:latin1 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import inspect

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QIcon, QMdiSubWindow, QHeaderView
from PyQt4.uic import loadUi
from PyQt4.QtCore import QFile
'''
Private modules are imported.
'''

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")

class CoinDetail_Window(QWidget):
    def __init__(self, choice_function,
                 form_title_movie,
                 type_detail,
                 img_paths,
                 custom_logger,
                 parent):
        QWidget.__init__(self, parent)

        self.choice_function = choice_function
        self.form_title_movie = form_title_movie
        self.type_detail = type_detail

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        UI_PATH = QFile(":/ui_file/coin_details.ui")

        self.img_paths = img_paths

        self.load_ui_file(UI_PATH)
        self.init_ui_coin_detail()
        self.init_PushButton()


    def load_ui_file(self, UI_PATH):
        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        try:
            logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")

            UI_PATH.open(QFile.ReadOnly)

            logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is opened - (" + __name__ + ")")
            logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")

            self.ui_coin_detail = loadUi(UI_PATH, self)

            logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")
        finally:
            logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")

            UI_PATH.close()

            logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed - (" + __name__ + ")")

        logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def get_class_name(self):
        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + __name__ + ")")
        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
        logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        return self.__class__.__name__

    def init_ui_add_new_movie_detail(self):
        self.ui_coin_detail.setWindowTitle(self.form_title_movie)
        self.ui_coin_detail.pushButton_delete.setEnabled(False)
        self.ui_coin_detail.pushButton_save.setEnabled(False)
    #--------------------------------------------------------------------------------------
    def ini_ui_show_movie(self):
        self.ui_coin_detail.setWindowTitle(self.form_title_movie)
    #--------------------------------------------------------------------------------------
    def init_ui_coin_detail(self):
        self.ui_coin_detail.setWindowIcon(QIcon(self.img_paths.coin_icon))

    def init_PushButton(self):
        self.ui_coin_detail.pushButton_close.clicked.connect(self.close_form)
        #self.ui_movie_detail.pushButton_add_film_version.clicked.connect(self.show_movie_Detail)
    #--------------------------------------------------------------------------------------
    def lineEdit_change(self):
        self.ui_coin_detail.lineEdit_title_translated.textChanged.connect(self.title_form_change)
    #--------------------------------------------------------------------------------------
    def init_label_edition(self):
        self.ui_coin_detail.label_type_detail.setText(self.tr("Veröffentlichung / Edition"))

    def init_label_version(self):
        self.ui_coin_detail.label_type_detail.setText(self.tr("Fassung / Version"))

    def create_type_list(self, type_number):
        print "type_number", type_number
        list_functions = [
            self.init_label_version,
            self.init_label_edition
                            ]

        list_functions[type_number]()
    #--------------------------------------------------------------------------------------
    def type_of_detail(self):
        self.create_type_list(self.type_detail)
    #--------------------------------------------------------------------------------------
    def create_function_list(self, choice_number):
        list_functions = [self.init_ui_add_new_movie_detail,
                          self.ini_ui_show_movie
                            ]

        list_functions[choice_number]()
    #--------------------------------------------------------------------------------------
    def chosen_function(self):
        self.create_function_list(self.choice_function)
    #--------------------------------------------------------------------------------------
    def do_something(self):
        pass
    #--------------------------------------------------------------------------------------
    def closeEvent(self, event):
        pass
    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
        print "STATUS [OK]  (", FILE_NAME, "): The function (close_form) was called"
    #--------------------------------------------------------------------------------------
