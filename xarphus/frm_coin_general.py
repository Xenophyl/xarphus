#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path
from sys import argv, exit

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt, QString, QThread, pyqtSignal
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QLabel, QFontMetrics, QMessageBox

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class CoinGeneral_Window(QWidget):
    def __init__(self,
                 parent=None):

        QWidget.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/coin_general.ui")

        self.load_ui_file(UI_PATH)

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_coin_general = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def closeEvent(self, event):
        '''
            NOTICE:
            =======
                This method closes the form.

            PARAMETERS:
            ===========
                :return                         -       Nothing is returned. The statement 'return'
                                                        terminates a function. That makes sure that the
                                                        function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_coin_general.close()

        return

if __name__ == "__main__":

    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    '''
    Private modules are imported.
    '''
    from core.manange_logging import set_custom_logger, create_log_file
    from info import info_app

    from core.manage_time import get_time_now

    ## Import compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    program_info = info_app()
    custom_logger = set_custom_logger(program_info)

    FILE_NAME = "frm_coin_general.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    create_log_file(LOG_FILE_PATH, program_info)

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = CoinGeneral_Window(custom_logger,
                          None)
    window.show()
    exit(app.exec_())