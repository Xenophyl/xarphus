#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''

import os
from traceback import format_exc
from sys import exit, exc_info

from mechanize import Browser, HTTPError, URLError
from requests.exceptions import ConnectionError, Timeout, SSLError, RequestException, \
                                HTTPError, URLRequired, InvalidURL

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QRegExp, QFile, QTimer, QThread, QObject, pyqtSignal

from PyQt4.QtGui import QDialog, QValidator, QRegExpValidator, QLineEdit, QMessageBox, \
     QPushButton

from PyQt4.uic import loadUi

try:

    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.manage_generator import list_object_generator
    from xarphus.core.manage_string import is_string_blank, count_it
    from xarphus.core.managed_mechanize import user_exists_online_support, register_user_support
    from xarphus.gui import ui_rc
    from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from xarphus.collection_paths_image import ImagePathnCollection
    from xarphus.core.manage_input_controller import InputController

except ImportError:

    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manage_input_controller import InputController
    from localization_files import translate_files_rc
    from core.manage_generator import list_object_generator
    from core.manage_string import is_string_blank, count_it
    from core.managed_mechanize import user_exists_online_support, register_user_support
    from core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from collection_paths_image import ImagePathnCollection
    from core.config import ConfigurationSaverDict
    from gui import ui_rc
    from images import images_rc

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'register.ui')
#UI_PATH = QFile(":/ui_file/register.ui")

class TaskWorker(QObject):

    notify_user_exists_signal = pyqtSignal(bool)
    notify_register = pyqtSignal(int)
    error_message_signal = pyqtSignal(unicode, unicode)

    def __init__(self,
                 user_name = None,
                 password = None,
                 secret_key = None,
                 time_out = 10,
                 configuration_saver_dict = None,
                 parent = None):
        QObject.__init__(self, parent)

        self._time_out = time_out
        self._user_name = user_name
        self._password = password
        self._secret_key = secret_key
        self._configuration_saver_dict = configuration_saver_dict
        self.item_counter = 0

    def start_user_exists_online(self):

        create_original_url =self._configuration_saver_dict.dict_set_general_settings["URLCheckUserSuppoer"]

        try:

            self.notify_user_exists_signal.emit(user_exists_online_support(url = create_original_url, user_name = unicode(self._user_name).lower()))

        except ConnectionError:

            desired_trace = format_exc(exc_info())

            connection_message = self.tr("Connection error when connecting to the HTTPS site. Unable to connect to remote host.")
            self.error_message_signal.emit("Connection Error", connection_message)
            logger.error(desired_trace)

        except Timeout:

            desired_trace = format_exc(exc_info())

            timeout_message = self.tr("Timeout", "Request timed out. The remote host did not respond timely.")
            self.error_message_signal.emit(timeout_message)
            logger.error(desired_trace)

        except SSLError as e:

            desired_trace = format_exc(exc_info())

            message = self.tr("Unable to connect to remote host because of a SSL error. "
                        "It is likely that your system cannot verify the validity"
                        "of the certificate. The remote certificate is either "
                        "self-signed, or the remote server uses SNI. See the wiki for "
                        "more information on this topic.")

            if kwargs["verify"]:

                self.error_message_signal.emit("SSL Error", message)
                logger.error(desired_trace)

            else:

                certificate_message = ("SSL error raised during connection, with certificate "
                                      "verification turned off: {e}".format(e = unicode(e)))

                self.error_message_signal.emit("SSL Error", certificate_message)
                logger.error(desired_trace)

        except URLRequired as e:

            desired_trace = format_exc(exc_info())

            url_required_message = self.tr("A valid URL is required to make a request.")

            self.error_message_signal.emit("Missing URL", url_required_message)
            logger.error(desired_trace)

        except InvalidURL as e:

            desired_trace = format_exc(exc_info())

            invalid_url_message = self.tr("The URL provided was somehow invalid.")
            self.error_message_signal.emit("Invalid URL", invalid_url_message)
            logger.error(desired_trace)

        except HTTPError as e:

            desired_trace = format_exc(exc_info())

            title = "HTTP Error"

            if e.response is not None:

                if e.response.status_code >= 500:

                    self.error_message_signal.emit(title, self.tr("Remote server error"))

                elif e.response.status_code >= 400:

                    self.error_message_signal.emit(title, self.tr("Local client error"))
                else:
                    # I don't think we will end up here, but for completeness
                    self.error_message_signal.emit(title, "Unknow error: {e}".format(e = e))

                logger.error(desired_trace)

            else:

                raised_http_error_message = self.tr("Request raised HTTP error.")
                self.error_message_signal.emit(title, raised_http_error_message)
                logger.error(desired_trace)

        except RequestException as e:

            desired_trace = format_exc(exc_info())

            title = "Unknown Error"

            raised_exception_message = "Request raised exception: {e}".format(e = e)
            self.error_message_signal.emit(title, raised_exception_message)

            logger.error(desired_trace)


    def start_register(self):

        desired_trace = None
        title = None

        try:

            result = register_user_support(url = self._configuration_saver_dict.dict_set_general_settings["URLServerRegistration"],
                                           user_name = self._user_name,
                                           password = self._password,
                                           secret_key = self._secret_key)

            self.notify_register.emit(int(result))

        except (HTTPError, URLError) as e:

            desired_trace = format_exc(exc_info())

            if  unicode(desired_trace).find('11004') != -1 and unicode(desired_trace).find('URLError') != -1:

                title = "URL error"
                self.error_message_signal.emit(title, self.tr("A connection atttempt as failed. "
                                                              "The server my not exist or it isn't operational at this time. "
                                                              "Check your network connection, and try again."))


        except ValueError as ex:

            desired_trace = format_exc(exc_info())

            if  unicode(desired_trace).find('invalid literal for int()') != -1:

                title = "Registration error"

                self.error_message_signal.emit(title, self.tr("A connection atttempt as failed. "
                                                              "The server my not exist or it isn't operational at this time. "
                                                              "Check your network connection, and try again."))


        desired_trace = format_exc(exc_info())

class Register_Window(QDialog):

    resized = pyqtSignal()

    def __init__(self,
                 configuration_saver_dict = None,
                 info_app = None,
                 image_path_collection = None,
                 parent = None):
        QDialog.__init__(self, parent)

        # Path to the *.ui-file
        #self.getPath_register = os.path.abspath(os.path.join('files', "qt_ui", 'register.ui'))

        UI_PATH = QFile(":/ui_file/register.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self._user_name = None
        self._user_exists = None

        self._password = None
        self._confirm_password = None

        self._configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = ImagePathnCollection()

        self.input_controller = InputController()

        self.load_ui_file(UI_PATH)
        self.init_gui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_register = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_gui(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently
            all windows are blocked by using the setWindowModality()-method,
            that contains ApplicationModal. That means, the user cannot
            relegate this window in the background because its WindowModality
            is set to ApplicationModal.
            Next, this method describes how to control the minimum and maximum sizes of a widget,
            by using setFixedSize()-method.
            Last but not least: the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.ui_register.label_user_exists_message.setVisible(False)

        self.ui_register.setWindowModality(Qt.ApplicationModal)
        self.ui_register.setFixedSize(self.ui_register.width(), self.ui_register.height())

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_register.setWindowFlags(flags)

        self.init_clicked_signal_push_buttons()
        self.init_textChanged_signal_line_edit()
        self.init_timer()

        self.resized.connect(self.resizeEvent)

        return

    def init_timer(self, set_single_shot = True, set_intervall = 800):

        self.timer = QTimer()

        self.timer.setSingleShot(set_single_shot)
        self.timer.setInterval(set_intervall)
        self.timer.timeout.connect(self.start_user_check)

    def init_textChanged_signal_line_edit(self):
        #self.ui_register.lineEdit_nickname.textChanged.connect(lambda: self.check_string(str(self.ui_register.lineEdit_nickname.text())))
        self.ui_register.lineEdit_nickname.textChanged.connect(lambda text: self.handle_changed_text(text = text))

        self.ui_register.lineEdit_password.textChanged.connect(lambda text:
                                                               self.handle_password(text = text, password_type = 'password'))
        self.ui_register.lineEdit_password_confirm.textChanged.connect(lambda text:
                                                               self.handle_password(text = text, password_type = 'confirm_password'))

        #self.ui_register.lineEdit_nickname.textChanged.connect(lambda:
        #    self.handle_text_changed(line_edit_obj = self.ui_register.lineEdit_nickname,
        #                              pushButton_regist = self.ui_register.pushButton_regist,
        #                              max_length = 10))


    def hide_widget(self):

        self.ui_register.pushButton_regist.hide()
        self.ui_register.pushButton_reset.hide()

    def change_page(self, page_number = 0):

        self.ui_register.stackedWidget.setCurrentIndex(page_number)

        self.hide_widget()

    def sign_up_user(self):

        task_thread = QThread(self)

        task_thread.work = TaskWorker(configuration_saver_dict = self._configuration_saver_dict,
                                      user_name = self._user_name,
                                      password = self._confirm_password,
                                      secret_key = self.ui_register.lineEdit_secret_key.text())

        task_thread.work.moveToThread(task_thread)

        task_thread.work.notify_register.connect(lambda page:
                                                 self.change_page(page_number = page))

        task_thread.work.error_message_signal.connect(lambda title, error_msg:
                                                      self.create_critical_message_box(title = title,
                                                                                       text = error_msg,
                                                                                       icon = self.image_path_collection.warning_48x48_default))


        task_thread.started.connect(task_thread.work.start_register)
        task_thread.finished.connect(task_thread.deleteLater)
        task_thread.start()

    def start_user_check(self):

        task_thread = QThread(self)
        task_thread.work = TaskWorker(configuration_saver_dict = self._configuration_saver_dict,
                                      user_name = self._user_name,
                                      time_out = 10)

        task_thread.work.moveToThread(task_thread)

        task_thread.work.notify_user_exists_signal.connect(lambda bool_value:
            self.support_user_online_exits(result = bool_value))

        task_thread.work.error_message_signal.connect(lambda title, error_msg:
                                                      self.create_critical_message_box(title = title,
                                                                                       text = error_msg,
                                                                                       icon = self.image_path_collection.warning_48x48_default))


        task_thread.started.connect(task_thread.work.start_user_exists_online)
        task_thread.finished.connect(task_thread.deleteLater)
        task_thread.start()

    def support_user_online_exits(self, result = None):

        if result:
            #   User name already exists
            self._user_exists = True

            self.set_style_sheet(widget = self.ui_register.label_user_exists_message,
                                 widget_class = "QLabel",
                                 font_color = "#ff3333")

            self.set_style_sheet(widget = self.ui_register.lineEdit_nickname,
                                 widget_class = "QLineEdit",
                                 background_color = '#ff3333')

            self.set_text(widget = self.ui_register.label_user_exists_message,
                            text = "There already exists a user with this username")

            self.ui_register.label_user_exists_message.setVisible(True)
        else:
            #   available
            self._user_exists = False
            self.set_text(widget = self.ui_register.label_user_exists_message,
                            text = "Available")

            self.set_style_sheet(widget = self.ui_register.lineEdit_nickname,
                                 widget_class = "QLineEdit",
                                 background_color = '#47d147')

            self.set_style_sheet(widget = self.ui_register.label_user_exists_message,
                                 widget_class = "QLabel",
                                 font_color = '#47d147')

            self.ui_register.label_user_exists_message.setVisible(True)

            try:
                self.ui_register.pushButton_regist.setEnabled(self.compair_passwords())
            except TypeError:
                pass

    def set_cursor_position(self,
                            widget = None,
                            pos = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :widget  -

            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(widget, QLineEdit):
            widget.setCursorPosition(pos)

        return

    def set_text(self,
                 widget = None,
                 text = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -

            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(widget, QLineEdit):
            widget.setText(text)
        elif isinstance(widget, QProgressBar):
            widget.setText(text)

        return

    def handle_text_changed(self,
                            max_length,
                            line_edit_obj = None,
                            pushButton_regist = None,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False,
                            check_selected_id = True):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj              -   Here, we want a QWidget()-object: QLineEdit.
                                            Well, we need this object to get the current position
                                            of the cursor.

            :push_button_obj_save       -   specific pushButton()-object for saving data.

            :push_button_obj_delete     -   specific pushButton()-object for deleting data.

            :push_button_obj_edit       -   specific pushButton()-object for editing data.

            :push_button_obj_refresh    -   specific pushButton()-object for refreshing data.


            :change_windows_title       -   In this keyword argument we except a special value: boolean value.
                                            By default, the value is set to False, because we don't want the
                                            window title to change.

            :allow_trailing_spaces      -   We except a boolean value. By default, its set to True.
                                            True means that spaces between the each words are allowed.
                                            It is very rare that we want to ban trailing space.

            :allow_only_number          -   We except a boolean value. When we get True we
                                            want to know we shoukd allow only numbers. By default
                                            the value is set to False, because we want the user to
                                            enter all characters.

            :max_length                 -   This keyword-argument gets an integer for  setting a maximum length
                                            of a given text.

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text = unicode(line_edit_obj.text()),
                                                                      allow_trailing_spaces = allow_trailing_spaces,
                                                                      allow_only_number = allow_only_number,
                                                                      max_length = 8)

        self.set_text(widget = line_edit_obj, text = get_text)
        self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        #if get_text:
        #    '''
        #        No, the string isn't empty
        #    '''
        self.handle_changed_text(text = get_text.strip())


        return


    def compair_passwords(self):

        if self._password == self._confirm_password:
            if not self._user_exists is None:
                if not self._user_exists:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    def handle_password(self, text = None, password_type = None):

        if password_type == 'password':
            if unicode(text).strip():
                self._password = text
            else:
                self._password = 1

            try:
                self.ui_register.pushButton_regist.setEnabled(self.compair_passwords())
            except TypeError:
                pass


        if password_type == 'confirm_password':
            if unicode(text).strip():
                self._confirm_password = text
            else:
                self._confirm_password = 2


            try:
                self.ui_register.pushButton_regist.setEnabled(self.compair_passwords())
            except TypeError:
                pass


    def handle_changed_text(self, text = None):

        if not text:
            self.timer.stop()
            self._user_name = None
            self._user_exists = None
            self.ui_register.label_user_exists_message.setVisible(False)

            self.set_style_sheet(widget = self.ui_register.lineEdit_nickname,
                                 widget_class = "QLineEdit",
                                 font_color = '#000000')

            self.ui_register.pushButton_regist.setEnabled(False)


        else:

            if len(text) > 2:

                self.timer.stop()

                self._user_name = None
                self._user_name = text
                self.timer.start()

                try:
                    self.ui_register.pushButton_regist.setEnabled(self.compair_passwords())

                except TypeError:
                    pass

            else:

                self.set_style_sheet(widget = self.ui_register.label_user_exists_message,
                                     widget_class = "QLabel")

                self.set_visible(widget = self.ui_register.label_user_exists_message,
                                 bool_value = False)

                self.set_style_sheet(widget = self.ui_register.lineEdit_nickname,
                                     widget_class = "QLineEdit",
                                     background_color = '#ffffff')



    def set_visible(self, widget = None, bool_value = None):
        widget.setVisible(bool_value)
        return

    def create_critical_message_box(self,
                                    title = None,
                                    text = None,
                                    detail_text = None,
                                    icon = None):

        mBox = mBox = custom_message_box(self, text = text, title = title, icon = icon)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        if not detail_text is None:
            mBox.setInformativeText(detail_text)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def set_text(self, widget = None, text = None):

        widget.setText(text)

        return

    def set_style_sheet(self, widget = None, widget_class = None, background_color = None, font_color = "#000000"):

        if background_color is None:
            widget.setStyleSheet( widget_class + "{ opacity: 0.5;; font: bold; color : " + font_color + ";}")

        #widget.setStyleSheet("QLineEdit { background-color : {color}; }".format(color = background_color))
        else:
            widget.setStyleSheet( widget_class + "{ background-color :" + background_color + "; font: bold; color : " + font_color + ";}")

    def init_clicked_signal_push_buttons(self):
        # Slots and Signals
        self.ui_register.pushButton_regist.clicked.connect(self.sign_up_user)
        self.ui_register.pushButton_reset.clicked.connect(self.clear_widgets)
        self.ui_register.pushButton_close.clicked.connect(self.close)

    def find_Children_object(self,
                            widget_obj = None,
                            qt_obj = None):
        '''
            NOTICE:
            =======
            Note: that findChild and findChildren are methods of QObject -
            so if your form does not have them, it cannot be a QWidget
            (because all widgets inherit QObject). this method returns
            the childs of this object that can be cast.

            PARAMETERS:
            ===========
            :widget_obj -   We need a the specified parentWidget to find a list of child QWidgets
            :qt_obj     -   This parameter tells us the given name of the widget,  that are children of parentWidget.
            :return     -   The function returns a list that contains all
                            children of this object. If there are no such objects
                            its returns an empty list.
        '''
        logger.info("Return a list of all children of this object")

        return widget_obj.findChildren(qt_obj)

    def clear_widgets(self):

        widgets = self.find_Children_object(widget_obj = self.ui_register.stackedWidget,
                                            qt_obj = QLineEdit)
        for widget in widgets:
            widget.clear()

    def resizeEvent(self, evt=None):
        self.image_path_collection.look_it()


if __name__ == "__main__":
    from os import path
    from sys import argv

    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    configuration_saver_dict = ConfigurationSaverDict()


    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)
    h = 100
    w = 100
    window = Register_Window(configuration_saver_dict = configuration_saver_dict)
    window.resize(w, h)
    window.show()
    #window.showMaximized()
    exit(app.exec_())
