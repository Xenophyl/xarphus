# -------------------------------------------------
# Project created by QtCreator 2012-01-06T16:14:30
# -------------------------------------------------
QT += core \
    gui
QT += declarative
TARGET = QtButtonStyling
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp
HEADERS += mainwindow.h
FORMS += mainwindow.ui
RESOURCES += Resources.qrc
OTHER_FILES += Styles/CSS/styles.css
