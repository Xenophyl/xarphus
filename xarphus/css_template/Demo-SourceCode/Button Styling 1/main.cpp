#include <QtGui/QApplication>
#include "mainwindow.h"

#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    // Set global Stylesheet
    QFile cssFile(":CSS/styles");
    cssFile.open(QFile::ReadOnly);
    QString sheet = QLatin1String(cssFile.readAll());
    w.setStyleSheet(sheet);

    w.show();
    return a.exec();
}
