#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


from os.path import expanduser
from os import path
from sys import argv, exit, exc_info
from traceback import format_exc
from datetime import date, datetime

from PyQt4.QtCore import Qt, QFile, QString, QThread, pyqtSignal, QObject, QTimer, pyqtSlot, QEvent, QSize, QRect
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QFileDialog, QPixmap, QIcon, QWidget, QSpacerItem, QSizePolicy, QLabel, QFontMetrics, \
                        QToolButton, QMenu, QStandardItemModel, QStandardItem, QTreeView, QMessageBox, QPushButton, QTreeWidgetItem, \
                        QLineEdit, QComboBox, QPlainTextEdit, QTabWidget, QToolTip, QCursor, QSizePolicy

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
from xarphus.frm_person_contact import PersonContact_Window
from xarphus.frm_person_prominent import PersonProminent_Window
from xarphus.frm_person_prominent_contact import PersonProminentContact_Window
from xarphus.core.manage_input_controller import InputController
from xarphus.core.managed_drag_drop import ManagedDragDrop

from xarphus.core.manage_data_manipulation_master_data import MasterDataManipulation
import xarphus.core.managed_defined_session
from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel

from xarphus.subclass_master_data_load_data_item import LoadDataItem, TaskDataManipulation
from xarphus.core.manage_file import get_file_extension

from xarphus.core import get_news_in_version
from xarphus.core.managed_dates import create_date_format
from xarphus.core.manage_prase import parsing_str_to_bool



BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

def output(text):
    print "Output:",  text

class StorePersonData(object):
    def __init__(self):

        self._salutation = None
        self._title = None
        self._gender = None
        self._religion = None
        self._eye_color = None
        self._hair_color = None
        self._relationship_status = None

    def salutation(self, salutation=None):

        self._salutation = unicode(salutation)

        return

    def title(self, title=None):
        
        self._title = unicode(title)

        return

    def gender(self, gender=None):
        
        self._gender = unicode(gender)

        return

    def religion(self, religion=None):
        
        self._religion = unicode(religion)

        return

    def eye_color(self, eye_color=None):
        
        self._eye_color = unicode(eye_color)

        return

    def hair_color(self, hair_color=None):
        
        self._hair_color = unicode(hair_color)

        return

    def relationship_status(self, relationship_status=None):
        
        self._relationship_status = unicode(relationship_status)

        return

class PersonProfile_Window(QWidget):

    open_master_data_signal = pyqtSignal(int)
    open_cover_viewer_signal = pyqtSignal(unicode, unicode)

    enter_signal = pyqtSignal()

    interrupt_working_person_salutation_thread_signal = pyqtSignal()
    #interrupt_working_person_title_thread_signal = pyqtSignal()

    sotp_working_thread_signal = pyqtSignal()

    finish = pyqtSignal()

    def __init__(self, 
                 form_condition = None,
                 func_up_to_date = None,
                 image_path_collection = None,
                 configuration_saver_dict = None,
                 person_id = None,
                 standard_item_model = None,
                 parent = None):

        QWidget.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.func = func_up_to_date
        self.form_condition = form_condition

        self._configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = image_path_collection
        self._person_id = person_id

        self.messagebox_called = False

        self.flags = None
        self._list_threads = []

        self.salutatio_activated_thread = None
        self.title_activated_thread = None

        # Create an instance of imported class.
        self.input_controller = InputController()
        self.store_person_data = StorePersonData()

        # Crreate an attributes
        self._image_path = None


        self._birth_day = None
        self._birth_month = None
        self._birth_year = None
        self._death_day = None
        self._death_month = None
        self._death_year = None
        self._wedding_day = None
        self._wedding_month = None
        self._wedding_year = None
        self._file_extension = None
        self._new_on_image = False

        self._managed_drag_drop = None



        self.fm = QFontMetrics(self.font())

        self.mText = QString()
        self.mText = {}


        #self._proxy_model_comboBox_salutation = None
        #self.set_proxy_model()

        self.standard_item_model = standard_item_model

        self._standard_item_model_comboBox_salutation = None
        self._standard_item_model_comboBox_title = None
        self._standard_item_model_comboBox_gender = None
        self._standard_item_model_comboBox_religion = None
        self._standard_item_model_comboBox_eye_color = None
        self._standard_item_model_comboBox_hair_color = None
        self._standard_item_model_comboBox_relationship_status = None

        # category_list = ['person_salutation',
        #                  'person_title',
        #                  'person_gender',
        #                  'person_religion',
        #                  'person_eye_color',
        #                  'person_hair_color',
        #                  'person_relationship_status']

        # for category in category_list:
        #     self.standard_item_model(category=category, parent=self)


        self.set_standard_item_model()



        UI_PATH = QFile(":/ui_file/person_profile.ui")

        self.load_ui_file(UI_PATH = UI_PATH)

        self.init_ui()

        #self.set_source_model(model=self._standard_item_model, mode="database")

        #self.set_header_data(list_header_data=["ko", "Genre"])

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH = None):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_person_profile = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):

        self.flags = flag_dialog_titlehint_customizewondowhint()

        #self.start_threads()
        
        self.init_widget_with_model()

        self.init_selectionChanged_signal_treeview()
        
        self.init_tool_button_menu()

        self.chosen_function()
        
        self.init_profile_clicked_signal_push_buttons()
        self.init_profile_clicked_tool_button()

        self.init_currentIndexChanged_signal_combo_box()

        self.init_returnPressed_signal_line_edit()
        self.init_textChanged_signal_line_edit()

        self._managed_drag_drop = ManagedDragDrop(widget = self.ui_person_profile.label_photo)

        self.ui_person_profile.setAcceptDrops(True)

        self.ui_person_profile.label_photo.setScaledContents(False)

        #self.pixmap = QPixmap(":/img_150x200_default/default/img_150x200/default_photo.png")

        #self.ui_person_profile.label_photo.setPixmap(self.pixmap)

        # this ensures the label can also re-size downwards
        self.ui_person_profile.label_photo.setMinimumSize((QSize(1, 1)))
        self.ui_person_profile.label_photo.setAlignment(Qt.AlignCenter)

        #self.ui_person_profile.label_photo.setGeometry(QRect(70, 80, 100, 100)) #(x, y, height, width)

        #self.scaled_image(widget = self.ui_person_profile.label_photo)

        #self.set_fixed_size(widget = self.ui_person_profile.label_photo)

        if self._person_id:
            self.load_entire_selection(category = 'person_profile',
                                       query_by_id = True,
                                       data_id = self._person_id)

        
        return

    def scaled_image(self, widget = None):

        try:

            widget.setPixmap(self.pixmap.scaled(
                widget.size(), Qt.KeepAspectRatio,
                transformMode = Qt.SmoothTransformation))

        except AttributeError:

            pass
            
    def init_textChanged_signal_line_edit(self):
        '''
            NOTICE:
            =======
            This method is used to set signal and slots of textChanged for QLineEdit()-objects. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the signal and slots for QLineEdit()-object(s)")

        self.ui_person_profile.lineEdit_birth_year.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_person_profile.lineEdit_birth_year,
                                     allow_only_number = True,
                                     max_length = 4,
                                     check_selected_id = False))

        self.ui_person_profile.lineEdit_death_year.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_person_profile.lineEdit_death_year,
                                     allow_only_number = True,
                                     max_length = 4,
                                     check_selected_id = False))

        self.ui_person_profile.lineEdit_wedding_year.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_person_profile.lineEdit_wedding_year,
                                     allow_only_number = True,
                                     max_length = 4,
                                     check_selected_id = False))

        self.ui_person_profile.lineEdit_birth_year.textChanged.connect(lambda:
            self.handle_date_format(year = self. ui_person_profile.lineEdit_birth_year.text(), category = "birth"))

        self.ui_person_profile.lineEdit_death_year.textChanged.connect(lambda:
            self.handle_date_format(year = self. ui_person_profile.lineEdit_death_year.text(), category = "death"))

        self.ui_person_profile.lineEdit_wedding_year.textChanged.connect(lambda:
            self.handle_date_format(year = self. ui_person_profile.lineEdit_wedding_year.text(), category = "wedding"))

    def set_fixed_size(self, 
                       widget = None, 
                       width = None, 
                       height = None):

        widget.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

        if isinstance(width, type(None)) and isinstance(height, type(None)):
            print "width and height is None"
            
            widget.setFixedSize(int(widget.width()), int(widget.height())) # (int w, int h)

        else:
            print "Is NOT None"#

            widget.setFixedSize(int(width), int(height)) # (int w, int h)

        widget.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        return

    def handle_text_changed(self,
                            line_edit_obj = None,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False, 
                            max_length = None,
                            check_selected_id = True):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj              -   Here, we want a QWidget()-object: QLineEdit.
                                            Well, we need this object to get the current position
                                            of the cursor.

            :change_windows_title       -   In this keyword argument we except a special value: boolean value.
                                            By default, the value is set to False, because we don't want the
                                            window title to change.

            :allow_trailing_spaces      -   We except a boolean value. By default, its set to True.
                                            True means that spaces between the each words are allowed.
                                            It is very rare that we want to ban trailing space.

            :allow_only_number          -   We except a boolean value. When we get True we
                                            want to know we shoukd allow only numbers. By default
                                            the value is set to False, because we want the user to
                                            enter all characters.    

            :max_length                 -   This keyword-argument gets an integer for  setting a maximum length 
                                            of a given text.

            :check_selected_id          -   For later...blaaaa

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text = unicode(line_edit_obj.text()),
                                                                      widget_object = line_edit_obj, 
                                                                      allow_trailing_spaces = allow_trailing_spaces,
                                                                      allow_only_number = allow_only_number,
                                                                      max_length = max_length)



        self.set_text(line_edit_obj = line_edit_obj, text = get_text)
        self.set_cursor_position(line_edit_obj = line_edit_obj, pos = get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if get_text:
            '''
                The string isn't empty
            '''
            self.set_text(line_edit_obj = line_edit_obj, text = get_text)
            self.set_cursor_position(line_edit_obj = line_edit_obj, pos = get_pos)

        # else:
        #     '''
        #         Bad news, the string is emptyx.
        #     '''

        #     self.current_selected_id=None


        # if check_selected_id:
        #     '''
        #         Its possible that the variable named (self.selected_be_edited_id) is still
        #         not empty. For instance the user clicked on a TreeView()-object. In 
        #         this case special pushButton()-obejcts must be enabled oder disabled.
        #     '''
        #     if not self.selected_be_edited_id is None:


        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=True)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=True)

        #     else:

        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=False)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=False)

        return

    def set_cursor_position(self, line_edit_obj = None, pos = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -
            
            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(line_edit_obj, QLineEdit):
            line_edit_obj.setCursorPosition(pos)

        return

    def set_text(self, line_edit_obj = None, text = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -

            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(line_edit_obj, QLineEdit):
            line_edit_obj.setText(text)

        return

    def handle_date_format(self, 
                           day = None,
                           month = None,
                           year = None,
                           category = None):
        '''
            NOTICE:
            =======
            This method is used to take date and save it in attributes. 

            PARAMETERS:
            ===========
            :day        -   We get an inter for day

            :month      -   We get an integer for month

            :year       -   We get an integer for year

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        
        logger.info("Take the date and save")

        if category == "birth":
            if not year is None:
                self._birth_year = year
            if not month is None:
                self._birth_month = month
            if not day is None:
                self._birth_day = day
        elif category == "death":
            if not year is None:
                self._death_year = year
            if not month is None:
                self._death_month = month
            if not day is None:
                self._death_day = day
        elif category == "wedding":
            if not year is None:
                self._wedding_year = year
            if not month is None:
                self._wedding_month = month
            if not day is None:
                self._wedding_day = day

        return

    def show_tool_tip_text(self, text = None):
        '''
            NOTICE:
            =======
            This method is used to show the tip ballroon and to follow the cursor. 

            PARAMETERS:
            ===========
            :text       -       We need a given text to put it in the tool tip.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        
        logger.info("Fill the value of the dictionary")

        QToolTip.showText(QCursor.pos(), unicode(text))

        return

    def init_returnPressed_signal_line_edit(self):
        '''
            NOTE:
            ======
            This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).  

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        list_line_edit = self.findChildren(QLineEdit)

        for line_edit in list_line_edit:
            line_edit.returnPressed.connect(self.enter_signal.emit)

    def set_standard_item_model(self):

        category_list = ['person_salutation',
                         'person_title',
                         'person_gender',
                         'person_religion',
                         'person_eye_color',
                         'person_hair_color',
                         'person_relationship_status']

        for category in category_list:
            pass#self.standard_item_model(category = category, parent = self)


    def start_threads(self):

        list_tuple = [ ("person_salutation", self.ui_person_profile.comboBox_salutation, self._standard_item_model_comboBox_salutation), 
                    ("person_title", self.ui_person_profile.comboBox_title, self._standard_item_model_comboBox_title), 
                    ("person_gender", self.ui_person_profile.comboBox_gender, self._standard_item_model_comboBox_gender), 
                    ("person_religion", self.ui_person_profile.comboBox_religion, self._standard_item_model_comboBox_religion),
                    ("person_eye_color", self.ui_person_profile.comboBox_eye_color, self._standard_item_model_comboBox_eye_color),
                    ("person_hair_color", self.ui_person_profile.comboBox_hair_color, self._standard_item_model_comboBox_hair_color),
                    ("person_relationship_status", self.ui_person_profile.comboBox_relationsship_status, self._standard_item_model_comboBox_relationship_status),]


        for category, combobox, model in list_tuple:
            combobox.clear()
            model.clear()

            self.load_entire_selection(category = category,
                                       combo_box = combobox,
                                       model = model)


    def init_profile_clicked_signal_push_buttons(self):

        self.ui_person_profile.pushButton_artist_name.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Artist name")))

        self.ui_person_profile.pushButton_nickname.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Nickname")))        

        self.ui_person_profile.pushButton_name_normally_used.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Name normally used")))   

        self.ui_person_profile.pushButton_additional_middle_names.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Additional middle names")))  

        self.ui_person_profile.pushButton_birth_name.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Birthname")))

        self.ui_person_profile.pushButton_lastname.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Lastname")))

        self.ui_person_profile.pushButton_salutation.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Salutation")))

        self.ui_person_profile.pushButton_title.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Title")))

        self.ui_person_profile.pushButton_religion.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Religion")))

        self.ui_person_profile.pushButton_gender.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Gender")))

        self.ui_person_profile.pushButton_body_height.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Body height")))

        self.ui_person_profile.pushButton_birthplace.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Birthplace")))

        self.ui_person_profile.pushButton_birthday.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Birthday")))

        self.ui_person_profile.pushButton_death_day.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Deathday")))

        self.ui_person_profile.pushButton_relationship_status.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Relationship status")))

        self.ui_person_profile.pushButton_relationship.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Relationship")))

        self.ui_person_profile.pushButton_nationality.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Nationality")))

        self.ui_person_profile.pushButton_family.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Family")))

        self.ui_person_profile.pushButton_language.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Language")))

        self.ui_person_profile.pushButton_eye_color.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Eye color")))

        self.ui_person_profile.pushButton_hair_color.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Hair color")))

        self.ui_person_profile.pushButton_wedding_anniversary.clicked.connect(lambda: 
            self.show_tool_tip_text(text = self.tr("Wedding anniversary")))


        return

    def init_profile_clicked_tool_button(self):

        # self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close)
        self.ui_person_profile.toolButton_add_photo.clicked.connect(self.open_file_dialog)
        # #self.ui_movie_general.pushButton_close.clicked.connect(self.close_form)
        # self.ui_person_profile.pushButton_del_photo.clicked.connect(self.del_photo)

        self.ui_person_profile.toolButton_zoom_photo.clicked.connect(lambda: 
            self.open_cover_viewer_signal.emit(self._image_path, self.tr('Foto')))

        return

    def init_currentIndexChanged_signal_combo_box(self):
        '''
            NOTICE:
            =======
            This method sets all currentIndexChanged()-signale on QComboBox().

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Set currentIndexChanged()-signal on QComboBox()")

        '''
            #######################################################
            ###################  Person Salutation  ###############
            #######################################################
        '''
        self.ui_person_profile.comboBox_salutation.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_salutation"))

        '''
            #######################################################
            ###################  Person Title  ####################
            #######################################################
        '''
        self.ui_person_profile.comboBox_title.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_title"))

        '''
            #######################################################
            ###################  Person Gender  ###################
            #######################################################
        '''
        self.ui_person_profile.comboBox_gender.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_gender"))

        '''
            #######################################################
            ###################  Person Religion  #################
            #######################################################
        '''
        self.ui_person_profile.comboBox_religion.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_religion"))

        '''
            #######################################################
            ###################  Person Eye Color  ################
            #######################################################
        '''
        self.ui_person_profile.comboBox_eye_color.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_eye_color"))

        '''
            #######################################################
            ###################  Person Hair Color  ###############
            #######################################################
        '''
        self.ui_person_profile.comboBox_hair_color.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_hair_color"))

        '''
            #######################################################
            ###################  Person Relationship Status  ######
            #######################################################
        '''
        self.ui_person_profile.comboBox_relationsship_status.currentIndexChanged[str].connect(
            lambda item: self.handle_selection_changed(combo_item = item, category = "person_relationship_status"))

        self.ui_person_profile.comboBox_birth_day.currentIndexChanged.connect(lambda day: self.handle_date_format(day = day, category = "birth"))
        self.ui_person_profile.comboBox_birth_month.currentIndexChanged.connect(lambda month: self.handle_date_format(month = month, category = "birth"))

        self.ui_person_profile.comboBox_death_day.currentIndexChanged.connect(lambda day: self.handle_date_format(day = day, category = "death"))
        self.ui_person_profile.comboBox_death_month.currentIndexChanged.connect(lambda month: self.handle_date_format(month = month, category = "death"))

        self.ui_person_profile.comboBox_wedding_day.currentIndexChanged.connect(lambda day: self.handle_date_format(day = day, category = "wedding"))
        self.ui_person_profile.comboBox_wedding_month.currentIndexChanged.connect(lambda month: self.handle_date_format(month = month, category = "wedding"))

        return

    def set_tab_widget(self, tabs = None, index = None):

        tab_widgets = self.findChildren(QTabWidget)

        for tab in tab_widgets:
            tab.setCurrentIndex(0)

    def set_combobox_default(self):

        list_combobox = self.findChildren(QComboBox)

        for combobox in list_combobox:
            combobox.setCurrentIndex(0)

        return

    def clear_widget(self):

        list_widget = self.findChildren(QLineEdit)
        list_plain_text_edit = self.findChildren(QPlainTextEdit)

        for widget in list_widget:
            widget.clear()

        for plain_text in list_plain_text_edit:
            plain_text.clear()

        return

    def set_tool_button_enabled(self):

        list_tool_button_to_disable = [
                                      self.ui_person_profile.toolButton_del_photo,
                                      self.ui_person_profile.toolButton_delete_person_relationship,
                                      #self.ui_person_profile.toolButton_delete_person_family,
                                      #self.ui_person_profile.toolButton_delete_person_nationality,
                                      #self.ui_person_profile.toolButton_delete_person_language
                                      ]

        for tool_button in list_tool_button_to_disable:
            tool_button.setEnabled(False)

    def set_photo_default(self):

        self.pixmap = QPixmap(self.image_path_collection.standard_photo_150x200_default)
        self.ui_person_profile.label_photo.setPixmap(self.pixmap)

        return


    def set_all_widget_on_default(self):

        self.clear_widget()
        self.set_combobox_default()
        self.set_standard_item_model()
        #self.start_threads()
        
        self.set_photo_default()

        self.set_tool_button_enabled()

        self.set_tab_widget()

        self.ui_person_profile.lineEdit_artist_name.setFocus(Qt.ActiveWindowFocusReason)

        return

    def create_generally_question_msg(self, text = None, title = None, icon = None, detail_text = None):

        mBox = custom_message_box(self, text = text, title = title, icon = icon)
        setIc_OK = QIcon(self.image_path_collection.check_icon_default)
        setIc_No = QIcon(self.image_path_collection.no_default)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_text)

        mBox.addButton(QPushButton(setIc_OK, self.tr("Yes")),
                    mBox.YesRole)  # An "OK" button defined with the AcceptRole
        
        mBox.addButton(
                    QPushButton(setIc_No, self.tr("No")),
                    mBox.NoRole)  # An "OK" button defined with the AcceptRole

        clicked_result = mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        if clicked_result == 0:

            return True

        elif clicked_result == 1:

            return False

    def create_generally_information_msg(self, 
                                         title = None, 
                                         msg = None, 
                                         detail_msg = None,
                                         yes_no_button = False,
                                         only_yes_button = False):
        if not self.messagebox_called:

            if yes_no_button:

                yes_no_msg_box = custom_message_box(self, msg, title, self.image_path_collection.question_32x32_default)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_no_msg_box.setInformativeText(detail_msg)

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Yes")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("No")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.

                # 1 means No
                if result_button == 1:
                    self.close()
                # 0 means Yes
                elif result_button == 0:
                    self._image_path = None
                    self.pixmap = None
                    self.ui_person_profile.label_photo.clear()
                    self.set_all_widget_on_default()
                    self.quite_threads()

            if only_yes_button:

                yes_msg_box = custom_message_box(self, msg, title, self.image_path_collection.question_32x32_default)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_msg_box.setInformativeText(detail_msg)

                yes_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Ok")),
                    #QPushButton(self.tr(" No")),
                    yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
                yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

                self.close()

    def create_generally_crritcal_msg(self, err_title, err_msg, detail_msg):

        if not self.messagebox_called:
            self.messagebox_called = True
            mBox = custom_message_box(self, err_msg, err_title, QMessageBox.Critical)
            #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
            #mBox.setDetailedText(detail_msg)
            mBox.setInformativeText(detail_msg)
            mBox.addButton(
                #QPushButton(setIc_OK, self.tr(" Ok")),
                QPushButton(self.tr(" Ok")),
                mBox.AcceptRole) # An "OK" button defined with the AcceptRole

            mBox.exec_()    # QDialog.exec_ is the way that it blocks window
            # execution and allows a developer to wait for user
            # input before continuing.
            self.messagebox_called = False

    def get_tree_view(self):
        '''
            NOTICE:
            =======
            This method creates certain QTreeView().

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Create certain QTreeView()")

        tree_view = QTreeView(self)
        tree_view.setRootIsDecorated(False)
        tree_view.setWordWrap(True)
        tree_view.setAlternatingRowColors(True)

        tree_view.setSelectionMode(QTreeView.ExtendedSelection)
        
        tree_view.header().hide()

        return tree_view

    def set_combo_box_enable(self, combo_box = None, boolean_value = None):
        '''
            NOTICE:
            =======
            This method sets enabled on True or False for QComboBox().

            PARAMETERS:
            ===========
            :combo_box      -   Given QComboBox()-object
            :boolean_value  -   Get True or False
            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set Enabled on ComboBox()")

        combo_box.setEnabled(boolean_value)

        return

    def handle_selection_changed(self, combo_item = None, new_index = None, old_index = None, category = None):
        '''
            NOTICE:
            =======

            This method gets two QItemSelection, 
            the new selection and the old. 
        '''
        try: 

            if not combo_item is None:
                if category == "person_salutation":
                    self.store_person_data.salutation(salutation = combo_item)
                if category == "person_title":
                    self.store_person_data.title(title = combo_item)
                if category == "person_gender":
                    self.store_person_data.gender(gender = combo_item)
                if category == "person_religion":
                    self.store_person_data.religion(religion = combo_item)
                if category == "person_eye_color":
                    self.store_person_data.eye_color(eye_color = combo_item)
                if category == "person_hair_color":
                    self.store_person_data.hair_color(hair_color = combo_item)
                if category == "person_relationship_status":
                    self.store_person_data.relationship_status(relationship_status = combo_item)

            if not new_index is None:
                if category == "person_salutation":
                    self.store_person_data.salutation(salutation = new_index.indexes()[0].data())
                if category == "person_title":
                    self.store_person_data.title(title = new_index.indexes()[0].data())
                if category == "person_gender":
                    self.store_person_data.gender(gender = new_index.indexes()[0].data())
                if category == "person_religion":
                    self.store_person_data.religion(religion = new_index.indexes()[0].data())
                if category == "person_eye_color":
                    self.store_person_data.eye_color(eye_color = new_index.indexes()[0].data())
                if category == "person_hair_color":
                    self.store_person_data.hair_color(hair_color = new_index.indexes()[0].data())
                if category == "person_relationship_status":
                    self.store_person_data.relationship_status(relationship_status = new_index.indexes()[0].data())

        except Exception as er: 
            pass

    def init_selectionChanged_signal_treeview(self):
        # '''
        #     #######################################################
        #     ###################  Person Salutation  ###############
        #     #######################################################
        # '''
        # self.tree_view_comboBox_salutation.selectionModel().selectionChanged.connect(
        #     lambda new_index, old_index:
        #     self.handle_selection_changed(new_index=new_index, old_index=old_index, category="person_salutation"))

        # '''
        #     #######################################################
        #     ###################  Person Title  ####################
        #     #######################################################
        # '''
        # self.tree_view_comboBox_title.selectionModel().selectionChanged.connect(
        #     lambda new_index, old_index:
        #     self.handle_selection_changed(new_index=new_index, old_index=old_index, category="person_title"))

        return

    def save_active_thread(self, thread_object = None, category = None):
        '''
            NOTICE:
            =======
            This method is used to save the thread_object temporary. 

            PARAMETERS:
            ===========
            :thread_object      -   We except an thread object for saving that 
                                    in an attributes temporary. By default, the method
                                    is saving the value None in an attribute.

            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        logger.info("Save the object of thread")

        if category == "person_salutation":
            self.salutatio_activated_thread = thread_object

        if category == "person_title":
            self.title_activated_thread = thread_object

        return

    def init_widget_with_model(self):
        '''
            NOTICE:
            =======
            Here we initialize the QTreeView()-object.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Initialize the QTreeView()-object.")

        '''
            #######################################################
            ###################  Person Salutation  ###############
            #######################################################
        '''
        self.tree_view_comboBox_salutation = self.get_tree_view()

        self.ui_person_profile.comboBox_salutation.setView(self.ui_person_profile.tree_view_comboBox_salutation)


        self.ui_person_profile.tree_view_comboBox_salutation.setModel(self._standard_item_model_comboBox_salutation)

        self.ui_person_profile.comboBox_salutation.setModel(self._standard_item_model_comboBox_salutation)

        self.ui_person_profile.tree_view_comboBox_salutation.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person Title  ####################
            #######################################################
        '''
        self.tree_view_comboBox_title = self.get_tree_view()

        self.ui_person_profile.comboBox_title.setView(self.ui_person_profile.tree_view_comboBox_title)


        #self.ui_person_profile.tree_view_comboBox_title.setModel(self._standard_item_model_comboBox_title)

        self.ui_person_profile.tree_view_comboBox_title.setModel(self.standard_item_model.standard_item_model_general_region)        

        #self.ui_person_profile.comboBox_title.setModel(self._standard_item_model_comboBox_title)

        self.ui_person_profile.comboBox_title.setModel(self.standard_item_model.standard_item_model_general_region)

        self.ui_person_profile.tree_view_comboBox_title.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person Gender  ###################
            #######################################################
        '''
        self.tree_view_comboBox_gender = self.get_tree_view()

        self.ui_person_profile.comboBox_gender.setView(self.ui_person_profile.tree_view_comboBox_gender)


        self.ui_person_profile.tree_view_comboBox_gender.setModel(self._standard_item_model_comboBox_gender)

        self.ui_person_profile.comboBox_gender.setModel(self._standard_item_model_comboBox_gender)

        self.ui_person_profile.tree_view_comboBox_gender.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person Religion  #################
            #######################################################
        '''
        self.tree_view_comboBox_religion = self.get_tree_view()

        self.ui_person_profile.comboBox_religion.setView(self.ui_person_profile.tree_view_comboBox_religion)


        self.ui_person_profile.tree_view_comboBox_religion.setModel(self._standard_item_model_comboBox_religion)

        self.ui_person_profile.comboBox_religion.setModel(self._standard_item_model_comboBox_religion)

        self.ui_person_profile.tree_view_comboBox_religion.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person Eye Color  ################
            #######################################################
        '''
        self.tree_view_comboBox_eye_color = self.get_tree_view()

        self.ui_person_profile.comboBox_eye_color.setView(self.ui_person_profile.tree_view_comboBox_eye_color)


        self.ui_person_profile.tree_view_comboBox_eye_color.setModel(self._standard_item_model_comboBox_eye_color)

        self.ui_person_profile.comboBox_eye_color.setModel(self._standard_item_model_comboBox_eye_color)

        self.ui_person_profile.tree_view_comboBox_eye_color.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person Hair Color  ###############
            #######################################################
        '''
        self.tree_view_comboBox_hair_color = self.get_tree_view()

        self.ui_person_profile.comboBox_hair_color.setView(self.ui_person_profile.tree_view_comboBox_hair_color)


        self.ui_person_profile.tree_view_comboBox_hair_color.setModel(self._standard_item_model_comboBox_hair_color)

        self.ui_person_profile.comboBox_hair_color.setModel(self._standard_item_model_comboBox_hair_color)

        self.ui_person_profile.tree_view_comboBox_hair_color.sortByColumn(1, Qt.AscendingOrder)

        '''
            #######################################################
            ###################  Person HRelationship Status  #####
            #######################################################
        '''
        self.tree_view_comboBox_relationship_status = self.get_tree_view()

        self.ui_person_profile.comboBox_relationsship_status.setView(self.ui_person_profile.tree_view_comboBox_relationship_status)


        self.ui_person_profile.tree_view_comboBox_relationship_status.setModel(self._standard_item_model_comboBox_relationship_status)

        self.ui_person_profile.comboBox_relationsship_status.setModel(self._standard_item_model_comboBox_relationship_status)

        self.ui_person_profile.tree_view_comboBox_relationship_status.sortByColumn(1, Qt.AscendingOrder)

        return

    def set_ui_language(self):
        my_path = ":/img_16x16/img_16x16/Information.png"

    def selection_change_stackwidget(self,i):
        self.ui_person_prominent_contact.stackedWidget.setCurrentIndex(i)

    def set_prominent_contact(self):
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.func, self)
        self.ui_person_prominent_contact = PersonProminentContact_Window(self.func, self)
        self.ui_person_profile.verticalLayout_8.addWidget(self.ui_person_prominent_contact)
        self.ui_person_profile.verticalLayout_8.addWidget(self.pushbutton_close_print_delete_save)
        self.ui_person_profile.setWindowIcon(QIcon((self.image_path_collection.person_prominent_icon)))

        # setSizePolicy(QSizePolicy::Policy horizontal, QSizePolicy::Policy vertical)
        self.ui_person_profile.tabWidget_person_profile.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Expanding)
        self.ui_person_profile.tabWidget_sub_person_profil.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Expanding)
        
        self.ui_person_profile.setWindowTitle(self.tr("Prominat und Kontakt"))
        self.ui_person_prominent_contact.comboBox_select_communication_private.currentIndexChanged.connect(self.selection_change_stackwidget)

    def set_prominent(self):
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.func, self)
        self.person_prominent = PersonProminent_Window(self.func, self)
        self.ui_person_profile.verticalLayout_8.addWidget(self.person_prominent)
        self.ui_person_profile.verticalLayout_8.addWidget(self.pushbutton_close_print_delete_save)
        
        self.person_prominent.tabWidget_2.removeTab(2)

        # setSizePolicy(QSizePolicy::Policy horizontal, QSizePolicy::Policy vertical)
        self.ui_person_profile.tabWidget_person_profile.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Ignored)#Preferred)
        self.ui_person_profile.tabWidget_sub_person_profil.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Ignored)#Preferred)
        
        self.ui_person_profile.setWindowIcon(QIcon((self.image_path_collection.person_prominent_icon)))
        self.ui_person_profile.setWindowTitle(self.tr("Schauspieler/in"))
    #--------------------------------------------------------------------------------------
    def set_contact(self):
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.func, self)
        self.person_contact = PersonContact_Window(self.func, self)
        self.ui_person_profile.verticalLayout_8.addWidget(self.person_contact)
        self.ui_person_profile.verticalLayout_8.addWidget(self.pushbutton_close_print_delete_save)
        self.ui_person_profile.setWindowIcon(QIcon((self.image_path_collection.person_address_book_icon)))
        
        # setSizePolicy(QSizePolicy::Policy horizontal, QSizePolicy::Policy vertical)
        self.ui_person_profile.tabWidget_person_profile.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        self.ui_person_profile.tabWidget_sub_person_profil.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        
#        self.ui_person_profile.verticalSpacer_2.minimumSize()
        self.ui_person_profile.setWindowTitle(self.tr("Adressbuch"))
    #--------------------------------------------------------------------------------------
    def set_person_profil(self):

        set_enabled_push_button_delete = False if self._person_id is None else True

        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(person_id = self._person_id, 
                                                                                         set_enabled_push_button_delete = set_enabled_push_button_delete)
        self.pushbutton_close_print_delete_save.close_signal.connect(self.close)
        self.pushbutton_close_print_delete_save.save_signal.connect(self.add_record)
        self.pushbutton_close_print_delete_save.delete_signal.connect(lambda: 
            self.delete_record(id = self._person_id,
                               category = "person_profile"))

        self.enter_signal.connect(self.pushbutton_close_print_delete_save.pushButton_save.click)


        self.ui_person_profile.verticalLayout_8.addWidget(self.pushbutton_close_print_delete_save)
        spacer = QSpacerItem(0, 360, QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        
        # setSizePolicy(QSizePolicy::Policy horizontal, QSizePolicy::Policy vertical)
        self.ui_person_profile.tabWidget_person_profile.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        self.ui_person_profile.tabWidget_sub_person_profil.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        
        self.ui_person_profile.verticalLayout_17.addItem(spacer)
        self.ui_person_profile.setWindowTitle(self.tr("Person Profil"))
    #--------------------------------------------------------------------------------------
    def create_function_list(self, choice_number):
        list_functions = [self.set_person_profil,
                          self.set_contact,
                          self.set_prominent,
                          self.set_prominent_contact
                            ]

        list_functions[choice_number]()
    #--------------------------------------------------------------------------------------
    def chosen_function(self):
        self.create_function_list(self.form_condition)
    #--------------------------------------------------------------------------------------
    def open_file_dialog(self):
        self._image_path = unicode(QFileDialog.getOpenFileName(self, " Foto wÃ¤hlen ",
                                                                             path.expanduser(self._configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"]),
                                                                              "*.png *.jpg *.bmp"))


        if unicode(self._image_path) == '':
            self.ui_person_profile.toolButton_zoom_photo.setEnabled(False)
            self.ui_person_profile.toolButton_del_photo.setEnabled(False)
        else:
            self._file_extension = get_file_extension(file_path = unicode(self._image_path))
            self.ui_person_profile.toolButton_zoom_photo.setEnabled(True)
            self.ui_person_profile.toolButton_del_photo.setEnabled(True)
            #self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_front_cover)
            self.show_photo_cover(self._image_path)

            self.pixmap = QPixmap(self._image_path)
            self.ui_person_profile.label_photo.setPixmap(self.pixmap)

            self.scaled_image(widget = self.ui_person_profile.label_photo)

            self._new_on_image = True
    #--------------------------------------------------------------------------------------
    def combo_currentIndexChanged(self, idx):
        print "currentID", self.comboBox_total_evaluation.currentIndex()
    #--------------------------------------------------------------------------------------
    def set_combobox_total_evaluation(self):

        self.items = (
            '',
            (self.set_language.dict_ui_movie["star_one"], ":/stars/stars/one.png"),
            (self.set_language.dict_ui_movie["star_two"], ":/stars/stars/two.png"),
            (self.set_language.dict_ui_movie["star_three"], ":/stars/stars/three.png"),
            (self.set_language.dict_ui_movie["star_four"], ":/stars/stars/four.png"),
            (self.set_language.dict_ui_movie["star_five"], ":/stars/stars/five.png")
        )
        for i in self.items:
            if isinstance(i, tuple):
                text, icon_path = i[0], i[1]
                self.comboBox_total_evaluation.addItem(QIcon(icon_path), text)
            else:
                self.comboBox_total_evaluation.addItem(i)


    def refresh_standard_item_model(self, category = None, combo_box = None, tree_view = None, model = None):

        self.standard_item_model(category = category, parent = self)

        self.load_entire_selection(tree_view_object = tree_view,
                                    category = category,
                                    combo_box = combo_box,
                                    model = model)

    def init_tool_button_menu(self):
        '''
            NOTICE:
            =======
            This method initializes a pop up menu for 
            the QToolButton()-object.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("initialize the PopMenu for QToolButon")

        '''
            First wer save the menu item labels in the variables, because
            this label will often be needed.
        ''' 
        label_first_menu_item = self.tr("Edit") 
        label_second_menu_item = self.tr("Refresh") 

        '''
            We've created menus
        '''
        menu_salutation = QMenu()
        menu_title = QMenu()
        menu_gender = QMenu()
        menu_eye_color = QMenu()
        menu_hair_color = QMenu()
        menu_relationship = QMenu()
        menu_religion = QMenu()           

        '''
        addAction that will add menu items to your newly created QMenu.
        The actions are created by calling addAction and giving.
        Its a string that will be used as the menu item
        text and a method (self.Action1 or self.Action2) that
        will be called if that menu item is selected.
        '''
        menu_salutation.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda: self.open_master_data_signal.emit(30))

        menu_salutation.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item, 
            lambda:self.refresh_standard_item_model(category = "person_salutation", 
                                                    combo_box = self.ui_person_profile.comboBox_salutation,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_salutation,
                                                    model = self._standard_item_model_comboBox_salutation))

        menu_title.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(31))

        menu_title.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item, 
            lambda:self.refresh_standard_item_model(category = "person_title", 
                                                    combo_box = self.ui_person_profile.comboBox_title,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_title,
                                                    model = self._standard_item_model_comboBox_title))

        menu_gender.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(29))

        menu_gender.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item,             
            lambda:self.refresh_standard_item_model(category = "person_gender", 
                                                    combo_box = self.ui_person_profile.comboBox_gender,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_gender,
                                                    model = self._standard_item_model_comboBox_gender))

        menu_eye_color.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(33))

        menu_eye_color.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item,              
            lambda:self.refresh_standard_item_model(category = "person_eye_color", 
                                                    combo_box = self.ui_person_profile.comboBox_eye_color,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_eye_color,
                                                    model = self._standard_item_model_comboBox_eye_color))

        menu_hair_color.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(34))

        menu_hair_color.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item,              
            lambda:self.refresh_standard_item_model(category = "person_hair_color", 
                                                    combo_box = self.ui_person_profile.comboBox_hair_color,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_hair_color,
                                                    model = self._standard_item_model_comboBox_hair_color))

        menu_relationship.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(36))

        menu_relationship.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item,              
            lambda:self.refresh_standard_item_model(category = "person_relationship_status", 
                                                    combo_box = self.ui_person_profile.comboBox_relationsship_status,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_relationship_status,
                                                    model = self._standard_item_model_comboBox_relationship_status))

        menu_religion.addAction(QIcon(self.image_path_collection.edit_default), label_first_menu_item, 
            lambda:self.open_master_data_signal.emit(35))

        menu_religion.addAction(QIcon(self.image_path_collection.refresh_default), label_second_menu_item,              
            lambda:self.refresh_standard_item_model(category = "person_religion", 
                                                    combo_box = self.ui_person_profile.comboBox_religion,
                                                    tree_view = self.ui_person_profile.tree_view_comboBox_religion,
                                                    model = self._standard_item_model_comboBox_religion))

        '''
        setPopupMode:               This property describes the way that
                                    popup menus are used with tool buttons

        TQToolButton.InstantPopup:  he menu is stack_set_current_indexed, without delay,
                                    when the tool button is pressed.
                                    In this mode, the button's own action
                                    is not triggered.
        '''
        self.toolButton_salutation.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_salutation.setMenu(menu_salutation)
        
        self.toolButton_title.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_title.setMenu(menu_title)

        self.toolButton_gender.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_gender.setMenu(menu_gender)

        self.toolButton_eye_color.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_eye_color.setMenu(menu_eye_color)

        self.toolButton_hair_color.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_hair_color.setMenu(menu_hair_color)

        self.toolButton_relationship.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_relationship.setMenu(menu_relationship)

        self.toolButton_religion.setPopupMode(QToolButton.MenuButtonPopup)
        self.toolButton_religion.setMenu(menu_religion)

        return

    def show_photo_cover(self, img_path):
        self.ui_person_profile.label_photo.setPixmap(QPixmap(""))
        self.ui_person_profile.label_photo.setPixmap(QPixmap(img_path))
    #--------------------------------------------------------------------------------------
    def del_photo(self):
        self.ui_person_profile.label_photo.setPixmap(QPixmap(""))
        self._image_path = ""
        self.ui_person_profile.toolButton_zoom_photo.setEnabled(False)
        self.ui_person_profile.toolButton_del_photo.setEnabled(False)
        self.ui_person_profile.label_photo.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))



    def populate_data_item(self, tuple_items, model = None):
        '''
            NOTICE:
            =======
            This method populates the model (which is saved in variable named: 
            self._standard_item_model) with the items.

            PARAMETERS:
            ===========
            :tuple_items    -   we need a tuple of item to unpack it later.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        
        count_items = len(tuple_items)


        if count_items == 2:

            _, item_second = tuple_items

            #img_size = QSize(24, 24)
            #setIcon(QIcon(QPixmap(UI_PATH).scaled(img_size, Qt.KeepAspectRatio)))

            #item2.setIcon(QIcon(QPixmap('rsz_criminal-activities.jpg').scaled(img_size, Qt.KeepAspectRatio)))

            #self._standard_item_model.insertRow(0)

        two_columns_item = [QStandardItem(item_second)]

        #if combo_box_name =="salutation":
        model.appendRow(two_columns_item)

            #self._standard_item_model.setItem (0, 0, str(item1))
            #self._standard_item_model.setItem (0, 1, item2)
            #self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            #self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            # self._standard_item_model.setData(self._standard_item_model.index(0, 1), QIcon("boldText.png"))


        if count_items == 3:

            item_first, item_second, item_third = tuple_items

            self._standard_item_model.insertRow(0)
            self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            self._standard_item_model.setData(self._standard_item_model.index(0, 2), str(item_third))

        return

    def fill_tree_widget(self, i):

        parent = QTreeWidgetItem(self.ui_person_profile.treeWidget_nationality)
        self.ui_person_profile.treeWidget_nationality.addTopLevelItem(parent)
        parent.setText(0, unicode(i))
        parent.setCheckState(0, Qt.Unchecked)
        parent.setFlags(parent.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)

    def convert_string_to_bool(self, value):
        '''
            NOTICE:
            ======
            This method converts a given string in boolean value.
            We need this method for all QCheckBox()-objects. In the ini file, where all 
            settings are saved, there are only strings (for example: 'True' or 'False'). 
            All values aren't boolean value. When we want to change the state of 
            QCheckBox()-object, we have to convert all corresponding strings before.

            PARAMETERS:
            ===========
            :return     -   In the try block its returns the converted value. But, when 
                            the operation fails its returns False. In this case its
                            possible that the user or a other software has manipulated 
                            the contents of the ini file. 
        '''

        try:
            '''
                Let us try to invoke the function by passing an argument. In this case, we access
                to a dictionary at first, because the loaded settings are saved in the dictionary.
                We use the key (its the argument named value) to get the value, that is saved behind
                the key. With the obtained value we try to convert it in the bool-
            '''
            logger.info("Try to invoke a parsing_str_to_bool()-function by passing an {}-argument".format(value))

            return parsing_str_to_bool(value)
        except ValueError as VaErr:
            '''
                Ups, an error has occurred. Maybe the user or other program has changed 
                the content of this ini file. We write this error in the log file and
                return False. That means, the current QCheckBox()-object isn't check. The
                user will see an uncheck QCheckBox()-object.
            '''
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            return False



    def load_entire_selection(self, tree_view_object = None,
                                    category = None,
                                    combo_box = None,
                                    query_by_id = False,
                                    data_id = None,
                                    model = None):

        '''
            NOTICE:
            =======
            In this method we load the entire genre of movies.

            PARAMETERS:
            ===========
            :list_header:                       -

            :tuples_list_push_button_boolean    -

            :label_object                       -

            :return                             -       Nothing is returned. The statement 'return'
                                                        terminates a function. That makes sure that the
                                                        function is definitely finished.
        '''
        logger.info("Start the thread.")

        #self.quite_threads()

        self.task_thread = QThread()

        self.task_thread.work = LoadDataItem(category = category,
                                        query_by_id = query_by_id,
                                        data_id = data_id,
                                        timer_interval = 1)

        ''' # need to store worker and thread '''
        self._list_threads.append((self.task_thread))

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        
        self.task_thread.work.populate_item_signal.connect(lambda item: self.populate_data_item(item, model = model))

        self.task_thread.work.populate_input_signal.connect(lambda item: 
            self.populate_input(record = item))

        self.task_thread.work.combo_box_set_enable_signal.connect(lambda: 
            self.set_combo_box_enable(combo_box = combo_box, boolean_value = True))

        self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)


        self.interrupt_working_person_salutation_thread_signal.connect(self.task_thread.work.abort_task_thread)

        

        self.task_thread.started.connect(self.task_thread.work.init_object)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()
        
        return     
    
    def add_record(self,
                   work = None):

        record_added_day = None
        record_edited_day = None

        if self._person_id is None:

            record_added_day = datetime.strptime(str(date.today()), "%Y-%m-%d").date() 

        else:

            record_edited_day = datetime.strptime(str(date.today()), "%Y-%m-%d").date() 

        #     record_added_day = self.ui_person_profile.label_created_date_time

        #     label_changed_date_time


        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :line_edit_obj      -       We except a QLineEdit()-object for getting
                                        the content, that we want to save in the database.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        birth_date = create_date_format(day = self.ui_person_profile.comboBox_birth_day.currentText(),
                                       month = self.ui_person_profile.comboBox_birth_month.currentIndex(),
                                       year = self.ui_person_profile.lineEdit_birth_year.text())

        death_date = create_date_format(day = self._death_day,
                                       month = self._death_month,
                                       year = self._death_year)

        wedding_date = create_date_format(day = self._wedding_day,
                                       month = self._wedding_month,
                                       year = self._wedding_year)

        self.quite_threads()

        #   I have storing a reference to the thread after it's been created, 
        #   because it will be garbage collected (ie. destroyed) some time after the 
        #   program leaves MainWindows __init__. We need to store it at least as long 
        #   as the thread is running, for example use self.task_thread:

        self.task_thread = QThread()

        self.task_thread.work = TaskDataManipulation(configuration_saver_dict = self._configuration_saver_dict)

        ''' # need to store worker and thread '''
        self._list_threads.append((self.task_thread))

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)

        if self._person_id is None:

            self.task_thread.started.connect(lambda: 
                self.task_thread.work.add_record(
                                            person_name_normally_used = unicode(self.ui_person_profile.lineEdit_name_normally_used.text()),
                                            person_nickname = unicode(self.ui_person_profile.lineEdit_nickname.text()),
                                            person_artist_name = unicode(self.ui_person_profile.lineEdit_artist_name.text()),
                                            person_first_middle_name = unicode(self.ui_person_profile.lineEdit_middle_name.text()),
                                            person_birth_name = unicode(self.ui_person_profile.lineEdit_birthname.text()),
                                            person_birth_date = birth_date,
                                            person_death_date = death_date,
                                            person_wedding_date = wedding_date,
                                            person_last_name = unicode(self.ui_person_profile.lineEdit_lastname.text()),
                                            person_note = unicode(self.ui_person_profile.plainTextEdit_note.toPlainText()),
                                            person_body_height = unicode(self.ui_person_profile.lineEdit_body_height.text()),
                                            combo_person_salutation = unicode(self.ui_person_profile.comboBox_salutation.currentText()),
                                            combo_person_gender = unicode(self.ui_person_profile.comboBox_gender.currentText()),
                                            combo_person_religion = unicode(self.ui_person_profile.comboBox_religion.currentText()),
                                            combo_person_title = unicode(self.ui_person_profile.comboBox_title.currentText()),
                                            combo_person_relationsship_status = unicode(self.ui_person_profile.comboBox_relationsship_status.currentText()),
                                            combo_person_eye_color = unicode(self.ui_person_profile.comboBox_eye_color.currentText()),
                                            combo_person_hair_color = unicode(self.ui_person_profile.comboBox_hair_color.currentText()),
                                            combo_place = unicode(self.ui_person_profile.comboBox_place.currentText()),
                                            person_file_extension_photo = unicode(self._file_extension),
                                            image_path = unicode(self._image_path),
                                            record_added_day = record_added_day,
                                            category='person',
                                            work_area = "person_profile"
                                            ))        

            self.task_thread.work.confirm_saving_successful_signal.connect(lambda: 
                self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                                      msg = self.tr("The person has been saved into the database successfully."),
                                                      detail_msg = self.tr('Do you want to save another people?'),
                                                      yes_no_button = True,
                                                      only_yes_button = False))

        else:

            self.task_thread.started.connect(lambda: 
                self.task_thread.work.edit_record(
                                            id = self._person_id,
                                            person_name_normally_used = unicode(self.ui_person_profile.lineEdit_name_normally_used.text()),
                                            person_nickname = unicode(self.ui_person_profile.lineEdit_nickname.text()),
                                            person_artist_name = unicode(self.ui_person_profile.lineEdit_artist_name.text()),
                                            person_first_middle_name = unicode(self.ui_person_profile.lineEdit_middle_name.text()),
                                            person_birth_name = unicode(self.ui_person_profile.lineEdit_birthname.text()),
                                            person_birth_date = birth_date,
                                            person_death_date = death_date,
                                            person_wedding_date = wedding_date,
                                            person_last_name = unicode(self.ui_person_profile.lineEdit_lastname.text()),
                                            person_note = unicode(self.ui_person_profile.plainTextEdit_note.toPlainText()),
                                            person_body_height = unicode(self.ui_person_profile.lineEdit_body_height.text()),
                                            combo_person_salutation = unicode(self.ui_person_profile.comboBox_salutation.currentText()),
                                            combo_person_gender = unicode(self.ui_person_profile.comboBox_gender.currentText()),
                                            combo_person_religion = unicode(self.ui_person_profile.comboBox_religion.currentText()),
                                            combo_person_title = unicode(self.ui_person_profile.comboBox_title.currentText()),
                                            combo_person_relationsship_status = unicode(self.ui_person_profile.comboBox_relationsship_status.currentText()),
                                            combo_person_eye_color = unicode(self.ui_person_profile.comboBox_eye_color.currentText()),
                                            combo_person_hair_color = unicode(self.ui_person_profile.comboBox_hair_color.currentText()),
                                            combo_place = unicode(self.ui_person_profile.comboBox_place.currentText()),
                                            person_file_extension_photo = unicode(self._file_extension),
                                            image_path = unicode(self._image_path),
                                            save_image = self._new_on_image,
                                            record_edited_day = record_edited_day,
                                            category = 'person_profile'
                                            ))

            self.task_thread.work.confirm_saving_successful_signal.connect(lambda: 
                self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                                      msg = self.tr("The person has been changed into the database successfully."),
                                                      detail_msg = self.tr(''),
                                                      only_yes_button = True,
                                                      yes_no_button = False))


        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def delete_record(self, 
                          id = None,
                          category = None,
                          query_data = None):
        '''
            NOTICE:
            =======
            Here in this method we want delete a record of the database permamently.

            PARAMETERS:
            ===========
            :id                 -   We need an index as an integer. 
                                    With this ID, we want to uniquely identify 
                                    and delete a record.

            :line_edit_obj      -   We except a QLineEdit()-object for getting
                                    the content, that we want to save in the database.

            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        self.task_thread = QThread()

        self.task_thread.work = TaskDataManipulation()

        ''' # need to store worker and thread '''
        self._list_threads.append((self.task_thread))

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)


        self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)
        task_thread.work.remove_current_selected_id_signal.connect(self.remove_current_selected_id)
        task_thread.work.remove_selected_be_edited_id_signal.connect(self.remove_selected_be_edited_id)
        self.task_thread.work.close_window_signal.connect(self.close)
        
        converted_bool = self.convert_string_to_bool(self._configuration_saver_dict.dict_set_general_settings["AskBeforDelete"])

        try:

            int(id)

            if converted_bool:

                result_msg_box = self.create_generally_question_msg(title = self.tr('Conformation'),
                                                                    text = self.tr('Are you sure you want to permamently delete this item?'),
                                                                    detail_text = self.tr('NOTICE: By deleting, the selected item will be irrevocably removed!'),
                                                                    icon = self.image_path_collection.question_32x32_default)

            if result_msg_box:

                self.task_thread.work.delete_record(id = int(id), 
                                               category = category)

            self.task_thread.finished.connect(self.task_thread.deleteLater)

            self.task_thread.start()

        except (ValueError, TypeError):

            desired_trace = format_exc(exc_info())

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or \
            not desired_trace.find("int() argument must be a string or a number") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                self.create_generally_crritcal_msg(title_text, conn_err, "")

        return

    def generator_header_data(self, list_header):

        for header_data in list_header:
            yield header_data

    def set_header_data(self, list_header_data = None):
        '''
            NOTICE:
            =======
            This method sets the data for the given role and section 
            in the header with the specified orientation to the value supplied.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        count_column = 0

        for header_data in self.generator_header_data(list_header_data):
            self._standard_item_model.setHeaderData(count_column, Qt.Horizontal, header_data)

            count_column +=1 

        return

    def set_source_model(self, model = None, mode = None):
        '''
            NOTICE:
            =======
            Set main model as source for the proxy model.

            PARAMETERS:
            ===========
            :model      -   We except a model for the proxy model.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        if mode=="database":
            self._proxy_model.setSourceModel(model)

        return

    def standard_item_model(self, category = None, parent = None):
        '''
            NOTICE:
            =======
            Create an empty model for the TreeViews' data

            This method constructs a new item model that initially 
            has rows and columns, and that has the given parent.

            QTreeView needs a model to manage its data.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        if category == "person_salutation":
            if not self._standard_item_model_comboBox_salutation is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_salutation.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_salutation = QStandardItemModel()

        if category == "person_title":
            if not self._standard_item_model_comboBox_title is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_title.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_title = QStandardItemModel()

        if category == "person_gender":
            if not self._standard_item_model_comboBox_gender is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_gender.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_gender = QStandardItemModel()

        if category == "person_religion":
            if not self._standard_item_model_comboBox_religion is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_religion.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_religion = QStandardItemModel()

        if category == "person_eye_color":
            if not self._standard_item_model_comboBox_eye_color is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_eye_color.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_eye_color = QStandardItemModel()

        if category == "person_hair_color":
            if not self._standard_item_model_comboBox_hair_color is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_hair_color.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_hair_color = QStandardItemModel()

        if category == "person_relationship_status":
            if not self._standard_item_model_comboBox_relationship_status is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_comboBox_relationship_status.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_comboBox_relationship_status = QStandardItemModel()

                

    def set_proxy_model(self):
        '''
            NOTICE:
            =======
            Create proxy model for filtering and main model for data.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        self._proxy_model = MySortFilterProxyModel(self)
        self._proxy_model.setDynamicSortFilter(True)

        return

    def populate_input(self,
                       record = None):
        '''
            NOTICE:
            =======
            The given record from database will update QLineEdit, QComboBox and QPlainTextEdit.

            PARAMETERS:
            ===========

            :record    -   The given data from database.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        #print "ID {}".format(record.id)

        # We neeed this ternary" expressions. For instance, when there is
        # no results or there are more then one result, we get None. 
        # So we have to check if the given arguments is None.

        record_alias_name = "" if record is None else record.alias_name
        self.ui_person_profile.lineEdit_artist_name.setText(record_alias_name)

        record_nickname = "" if record is None else record.nickname        
        self.ui_person_profile.lineEdit_nickname.setText(record_nickname)

        record_name_normally_used = "" if record is None else record.name_normally_used
        self.ui_person_profile.lineEdit_name_normally_used.setText(record_name_normally_used)

        record_first_middle_name = "" if record is None else record.first_middle_name
        self.ui_person_profile.lineEdit_middle_name.setText(record_first_middle_name)

        record_birth_name = "" if record is None else record.birth_name
        self.ui_person_profile.lineEdit_birthname.setText(record_birth_name)

        record_last_name = "" if record is None else record.last_name
        self.ui_person_profile.lineEdit_lastname.setText(record_last_name)

        record_body_height = "" if record is None else record.body_height
        self.ui_person_profile.lineEdit_body_height.setText(record_body_height)

        record_notice = "" if record is None else record.notice
        self.ui_person_profile.plainTextEdit_note.setPlainText(record_notice)

        record_created_day = "" if record is None else record.record_added_day
        self.pushbutton_close_print_delete_save.set_text(added_text = record_created_day)

        record_changed_day = "" if record is None else record.record_edited_day
        self.pushbutton_close_print_delete_save.set_text(changed_text = record_changed_day)

        record_photo_file_extension = None if record is None else record.photo_file_extension
        self._file_extension = record_photo_file_extension

        # Here we need to check if the given record isn't None
        if record is not None:

            birth_day = str(record.birthday)
            death_day = str(record.day_of_death)
            wedding_day = str(record.wedding_anniversary)

            #   Ok, record isn't None, but its possible that we don't
            #   have birth data of the given person. If there is no birth data
            #   we get back "None" as string. In this case we do nothing,
            #   otherwise we show the birth data
            if not birth_day == "None":

                sliced_birth_year = birth_day[:4]
                self.ui_person_profile.lineEdit_birth_year.setText(QString(sliced_birth_year))

                sliced_birth_month = birth_day[5:7]
                self.ui_person_profile.comboBox_birth_month.setCurrentIndex(int(sliced_birth_month))

                sliced_birth_day = birth_day[8:10]
                self.ui_person_profile.comboBox_birth_day.setCurrentIndex(int(sliced_birth_day))

            if not death_day == "None":

                sliced_death_year = death_day[:4]
                self.ui_person_profile.lineEdit_death_year.setText(QString(sliced_death_year))

                sliced_death_month = death_day[5:7]
                self.ui_person_profile.comboBox_death_month.setCurrentIndex(int(sliced_death_month))

                sliced_death_day = death_day[8:10]
                self.ui_person_profile.comboBox_death_day.setCurrentIndex(int(sliced_death_day))

            if not wedding_day == "None":

                sliced_wedding_year = wedding_day[:4]
                self.ui_person_profile.lineEdit_wedding_year.setText(QString(sliced_wedding_year))

                sliced_weeding_month = wedding_day[5:7]
                self.ui_person_profile.comboBox_wedding_month.setCurrentIndex(int(sliced_weeding_month))

                sliced_wedding_day = wedding_day[8:10]
                self.ui_person_profile.comboBox_wedding_day.setCurrentIndex(int(sliced_wedding_day))

        #self.ui_person_profile.lineEdit_death_year.setText(QString(record.day_of_death))
        #self.ui_person_profile.lineEdit_wedding_year.setText(record.wedding_anniversary)

        IMAGE_PATH = None

        try:
            created_file_name = "{id}.{extension}".format(id = record.id, extension = record.photo_file_extension)
            BASE_PATH = self._configuration_saver_dict.dict_set_general_settings["BasePath"]

            IMAGE_PATH = path.join(BASE_PATH, 'images', 'person', created_file_name)
        except AttributeError as e:
            pass

        try:
            with open(IMAGE_PATH) as file:
                self.ui_person_profile.label_photo.setPixmap(QPixmap(IMAGE_PATH))
                self._image_path = IMAGE_PATH
                self.ui_person_profile.toolButton_zoom_photo.setEnabled(True)
        except (IOError, TypeError) as e: #Unable to open file - Does not exist OR no read permissions
                self.ui_person_profile.label_photo.setPixmap(QPixmap(':/img_150x200_default/default/img_150x200/default_photo.png'))

        return
    
    @pyqtSlot()
    def quite_threads(self):
        '''
            Explicitly mark a python method as a Qt slot and specify a C++ signature for it 
            (most commonly in order to select a particular overload). 
            pyqtSlot has a much more pythonic API.
        '''
        self.interrupt_working_person_salutation_thread_signal.emit()

        count_closed_thread_successful = 0
        count_already_closed_thread = 0

        

        #   Using the implicit booleanness of the empty list.
        #   Check if the list is empty.
        if self._list_threads: 
            #   List isn't empty
            for thread in self._list_threads:
                try:
                    # It will quit **as soon as thread event loop unblocks**
                    thread.quit()
                     # We need to wait for it to *actually* quit
                    thread.wait()

                    count_closed_thread_successful += 1 
                except RuntimeError as RunErr:
                    if not str(RunErr).find("QThread has been deleted") == -1:
                        count_already_closed_thread += 1

            self._list_threads[:] = []

    def dragEnterEvent(self, e):
        self._managed_drag_drop.drag_enter_event(e)

        return

    def dropEvent(self, e):
        self._image_path = self._managed_drag_drop.drop_event(e)

        self._new_on_image = True

        if not unicode(self._image_path) == '':
            self._file_extension = get_file_extension(file_path = unicode(self._image_path))

            self.pixmap = QPixmap(self._image_path)
            self.ui_person_profile.label_photo.setPixmap(self.pixmap)

        return

    def eventFilter(self, source, event):
        pass
        # if (source is self.ui_person_profile.label_photo and event.type() == QEvent.Resize):
        #     # re-scale the pixmap when the label resizes
        #     self.ui_person_profile.label_photo.setPixmap(self.pixmap.scaled(
        #         self.ui_person_profile.label_photo.size(), Qt.KeepAspectRatio,
        #         Qt.SmoothTransformation))
        # return super(PersonProfile_Window, self).eventFilter(source, event)

    def resizeEvent(self, event = None):
        '''
            NOTICE:
            =======
            We have to override the resizeEvent method.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        #   There isn't a path of a photo, we don't need to scale it anymore.
        if self._image_path:
            self.scaled_image(widget = self.ui_person_profile.label_photo)

        #self.set_fixed_size(widget = self.ui_person_profile.label_photo)

        #self.ui_person_profile.label_photo.setGeometry(QRect(70, 80, 100, 100)) #(x, y, height, width)

        self.ui_person_profile.lineEdit_artist_name.setFocus(Qt.ActiveWindowFocusReason)

        for key, value in self.mText.iteritems() :
            key.setText(self.fm.elidedText(value, Qt.ElideMiddle, key.width() -10))

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        self._image_path = None
        self._new_on_image = False
        self.quite_threads()
        self._session_scope = None

        logger.info("Close the current form.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return