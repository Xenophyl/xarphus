#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys

from inspect import currentframe

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class MusicAlbumGeneralDetails_Window(QWidget):
    def __init__(self,
                 func_up_to_date,
                 update_sublist,
                 parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/music_album_general_details.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        UI_PATH.open(QFile.ReadOnly)
        self.ui_music_alben_general_details = loadUi(UI_PATH, self)
        UI_PATH.close()

    def closeEvent(self, event):
        pass

    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
