#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_movie_.py"

import os
import sys
from os.path import expanduser

from PyQt4.QtCore import Qt, QFile
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QFileDialog, QPixmap, QIcon, QWidget, QStyleFactory

from xarphus.languages.german import Language
from xarphus.core import get_news_in_version



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class Movie_Window(QWidget):
    def __init__(self, integ, func_up_to_date, update_sublist, cover_viewer, save_config, parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/movie.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.cover_viewer = cover_viewer

        self.set_language = Language()
        self.set_get_settings = save_config

        from xarphus.css_template.css import Css_Syle
        self.css = Css_Syle()

        try:
            UI_PATH.open(QFile.ReadOnly)
            self.ui_movie = loadUi(UI_PATH, self)
            UI_PATH.close()
            print "STATUS [OK]  (", FILE_NAME, "): GUI is loaded from PyQt4"
        except Exception as ex:
            print "STATUS [FAILED]  (", FILE_NAME, "):", ex
        except:
            print "STATUS [FAILED]  (", FILE_NAME, "): Unexpected error:", sys.exc_info()[0]
            raise

        self.set_ui_language()
        self.set_gui()
        self.set_action_combox()
        self.on_lineEdit_changed()
        self.on_textedit_changed()
#        self.set_combobox_total_evaluation()
#         self.ui_movie_general.show()
#
        self.set_action_pushbutton()
#         self.get_news_version()
# #        self.get_update_version()

    def set_ui_language(self):
        # title
        self.ui_movie.setWindowTitle(self.set_language.dict_ui_movie["movie_title"])
        # groupBpx
        self.ui_movie.setStyleSheet(self.css.set_Title_Center_Round_Corner_QGroupBox)
        self.ui_movie.groupBox_general_info.setTitle(self.set_language.dict_ui_movie["groupBox_general_info"])
        self.ui_movie.groupBox_cover.setTitle(self.set_language.dict_ui_movie["groupBox_cover"])
        self.ui_movie.groupBox_film_version.setTitle(self.set_language.dict_ui_movie["groupBox_film_version"])
        self.ui_movie.groupBox_film_edition.setTitle(self.set_language.dict_ui_movie["groupBox_film_edition"])
        self.ui_movie.groupBox_film_cast.setTitle(self.set_language.dict_ui_movie["groupBox_film_cast"])
        self.ui_movie.groupBox_crew.setTitle(self.set_language.dict_ui_movie["groupBox_crew"])
        self.ui_movie.groupBox_film_cast_sync.setTitle(self.set_language.dict_ui_movie["groupBox_film_cast"])
        self.ui_movie.groupBox_voice_actor.setTitle(self.set_language.dict_ui_movie["groupBox_voice_actor"])
        self.ui_movie.groupBox_film_production_company.setTitle(self.set_language.dict_ui_movie["groupBox_film_production_company"])
        self.ui_movie.groupBox_filmstudio.setTitle(self.set_language.dict_ui_movie["groupBox_filmstudio"])
        # QTabWidget
        self.ui_movie.tabWidget_cover.setTabText(0, self.set_language.dict_ui_movie["tabWidget_cover_front"]) # First Tab
        self.ui_movie.tabWidget_cover.setTabText(1, self.set_language.dict_ui_movie["tabWidget_cover_back"]) # Second Tab
        self.ui_movie.tabWidget_details.setTabText(0, self.set_language.dict_ui_movie["tabWidget_details"]) # First Tab
        self.ui_movie.tabWidget_details.setTabText(1, self.set_language.dict_ui_movie["tabWidget_version_edition"]) # Second Tab
        self.ui_movie.tabWidget_more_detail.setTabText(0, self.set_language.dict_ui_movie["tabWidget_more_detail_cast_crew"])
        self.ui_movie.tabWidget_more_detail.setTabText(1, self.set_language.dict_ui_movie["tabWidget_more_detail_sync"])
        self.ui_movie.tabWidget_more_detail.setTabText(2, self.set_language.dict_ui_movie["tabWidget_more_detail_production"])
        self.ui_movie.tabWidget_more_detail.setTabText(3, self.set_language.dict_ui_movie["tabWidget_more_detail_soundtrack"])
        self.ui_movie.tabWidget_more_detail.setTabText(4, self.set_language.dict_ui_movie["tabWidget_more_detail_content"])
        self.ui_movie.tabWidget_more_detail.setTabText(5, self.set_language.dict_ui_movie["tabWidget_more_detail_comment"])
        self.ui_movie.tabWidget_more_detail.setTabText(6, self.set_language.dict_ui_movie["tabWidget_more_detail_review"])
        # QLabel
        #self.ui_movie_general.label_genre.setText(self.set_language.dict_ui_movie["label_genre"])
        #self.ui_movie_general.label_color_format.setText(self.set_language.dict_ui_movie["label_color_format"])
        #self.ui_movie_general.label_original_title.setText(self.set_language.dict_ui_movie["label_original_title"])
        #self.ui_movie_general.label_translate_title.setText(self.set_language.dict_ui_movie["label_translate_title"])
        #self.ui_movie_general.label_production_year.setText(self.set_language.dict_ui_movie["label_production_year"])
        #self.ui_movie_general.label_country_of_production.setText(self.set_language.dict_ui_movie["label_country_of_production"])
        #self.ui_movie_general.label_total_evaluation.setText(self.set_language.dict_ui_movie["label_total_evaluation"])
        # QPushbutton
        self.ui_movie.pushButton_close.setText(self.set_language.dict_ui_movie["pushButton_close"])
        self.ui_movie.pushButton_print_film.setText(self.set_language.dict_ui_movie["pushButton_print_film"])
        self.ui_movie.pushButton_save_film.setText(self.set_language.dict_ui_movie["pushButton_save_film"])
        self.ui_movie.pushButton_delete_film.setText(self.set_language.dict_ui_movie["pushButton_delete_film"])

    def combo_currentIndexChanged(self, idx):
        print "currentID", self.comboBox_total_evaluation.currentIndex()

    def set_combobox_total_evaluation(self):

        self.items = (
            '',
            (self.set_language.dict_ui_movie["star_one"], ":/stars/stars/one.png"),
            (self.set_language.dict_ui_movie["star_two"], ":/stars/stars/two.png"),
            (self.set_language.dict_ui_movie["star_three"], ":/stars/stars/three.png"),
            (self.set_language.dict_ui_movie["star_four"], ":/stars/stars/four.png"),
            (self.set_language.dict_ui_movie["star_five"], ":/stars/stars/five.png")
        )
        for i in self.items:
            if isinstance(i, tuple):
                text, icon_path = i[0], i[1]
                self.comboBox_total_evaluation.addItem(QIcon(icon_path), text)
            else:
                self.comboBox_total_evaluation.addItem(i)

    def show_front_cover(self, img_path, cover_site):
        if cover_site == "front":
            self.ui_movie.label_cover_front.setPixmap(QPixmap(""))
            self.ui_movie.label_cover_front.setPixmap(QPixmap(img_path))
        if cover_site == "back":
            self.ui_movie.label_cover_back.setPixmap(QPixmap(""))
            self.ui_movie.label_cover_back.setPixmap(QPixmap(img_path))

    def del_cover(self, site):
        if site == "front":
            self.ui_movie.label_cover_front.setPixmap(QPixmap(""))
            self.file_path_cover_front = ""
            self.ui_movie.pushButton_zoom_front.setEnabled(False)
            self.ui_movie.pushButton_del_front_cover.setEnabled(False)
            self.ui_movie.label_cover_front.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))
        if site == "back":
            self.ui_movie.label_cover_back.setPixmap(QPixmap(""))
            self.file_path_cover_back = ""
            self.ui_movie.pushButton_zoom_back.setEnabled(False)
            self.ui_movie.pushButton_del_back_cover.setEnabled(False)
            self.ui_movie.label_cover_back.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))

    def create_cover_viewer(self, cover_path):
        self.cover_viewer(cover_path)

    def set_action_pushbutton(self):
        self.ui_movie.pushButton_add_front_cover.clicked.connect(lambda: self.open_file_dialog("front"))
        self.ui_movie.pushButton_add_back_cover.clicked.connect(lambda: self.open_file_dialog("back"))
        self.ui_movie.pushButton_close.clicked.connect(self.close_form)
        self.ui_movie.pushButton_zoom_front.clicked.connect(lambda: self.create_cover_viewer(self.file_path_cover_front))
        self.ui_movie.pushButton_zoom_back.clicked.connect(lambda: self.create_cover_viewer(self.file_path_cover_back))
        self.ui_movie.pushButton_del_front_cover.clicked.connect(lambda: self.del_cover("front"))
        self.ui_movie.pushButton_del_back_cover.clicked.connect(lambda: self.del_cover("back"))

    def set_action_combox(self):
        self.comboBox_total_evaluation.currentIndexChanged.connect(self.combo_currentIndexChanged)

    def open_file_dialog(self, cover_site):
        if cover_site == "front":
            self.file_path_cover_front = unicode(QFileDialog.getOpenFileName(self, self.set_language.dict_ui_setting["folderDialogTitle"],
                                                                             os.path.expanduser(self.set_get_settings.dict_set_settings["LastPathFolderDialog"]),
                                                                              "*.png *.jpg *.bmp"))

            if unicode(self.file_path_cover_front) == '':
                self.ui_movie.pushButton_zoom_front.setEnabled(False)
                self.ui_movie.pushButton_del_front_cover.setEnabled(False)
            else:
                self.ui_movie.pushButton_zoom_front.setEnabled(True)
                self.ui_movie.pushButton_del_front_cover.setEnabled(True)
                self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_cover_front)
                self.show_front_cover(self.file_path_cover_front, "front")

        if cover_site == "back":
            self.file_path_cover_back = unicode(QFileDialog.getOpenFileName(self, self.set_language.dict_ui_setting["folderDialogTitle"],
                                                                             os.path.expanduser(self.set_get_settings.dict_set_settings["LastPathFolderDialog"]),
                                                                              "*.png *.jpg *.bmp"))

            if unicode(self.file_path_cover_back) == '':
                self.ui_movie.pushButton_zoom_back.setEnabled(False)
                self.ui_movie.pushButton_del_back_cover.setEnabled(False)
            else:
                self.ui_movie.pushButton_zoom_back.setEnabled(True)
                self.ui_movie.pushButton_del_back_cover.setEnabled(True)
                self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_cover_back)
                self.show_front_cover(self.file_path_cover_back, "back")

    def set_gui(self):
        pass
        # label_list = self.list_all_qlabel()
        # lineedit_list = self.list_all_qlineedit()
        # combobox_list = self.list_all_qcombobox()
        # pushbutton_list = self.list_all_qpushbutton()
        # textedit_list = self.list_all_qtextedit()
        # groupbox_list = self.list_all_groupbox()
        # tabwidget_list = self.list_all_qtabWidget()
        # treewidget_list = self.list_all_treewidget()
        #
        # self.ui_movie_general.setStyleSheet(self.css.set_css_dialog_background)
        #
        # for element_label in label_list:
        #     element_label.setStyleSheet(self.css.set_css_label_color)
        #
        # for element_lineedit in lineedit_list:
        #     element_lineedit.setStyleSheet(self.css.set_css_text_field_color_normal)
        #
        # for element_combobox in combobox_list:
        #     element_combobox.setStyleSheet(self.css.set_css_combo_field_color)
        #
        # for element_pushbutton in pushbutton_list:
        #     element_pushbutton.setStyleSheet(self.css.set_css_pushbutton_color)
        #
        # for element_edittext in textedit_list:
        #     element_edittext.setStyleSheet(self.css.set_css_textedit_color)
        #
        # for element_groupbox in groupbox_list:
        #     element_groupbox.setStyleSheet(self.css.set_css_groupbox_color)
        #
        # if not self.set_get_settings.dict_set_settings["GUIStyle"] == 'Plastique':
        #     print "NOTHING"
        # else:
        #     for element_tabwidget in tabwidget_list:
        #         element_tabwidget.setStyleSheet(self.css.set_css_tabwidget_color)
        #
        #
        # for element_treewidget in treewidget_list:
        #     element_treewidget.setStyleSheet(self.css.set_css_treewidget_color)


        #if style_factory ==


    def list_all_groupbox(self):
        list_groupbox = (self.ui_movie.groupBox_cover,
                         self.ui_movie.groupBox_general_info,
                         self.ui_movie.groupBox_film_cast,
                         self.ui_movie.groupBox_crew,
                         self.ui_movie.groupBox_film_cast_sync,
                         self.ui_movie.groupBox_voice_actor,
                         self.ui_movie.groupBox_film_production_company,
                         self.ui_movie.groupBox_filmstudio,
                         self.ui_movie.groupBox_review,
                         self.ui_movie.groupBox_comment,
                         self.ui_movie.groupBox_conent,
                         self.ui_movie.groupBox_film_version,
                         self.ui_movie.groupBox_film_edition,
                        )

        return list_groupbox

    def list_all_qtextedit(self):
        list_qedittext = (self.ui_movie.textEdit_review,
                              self.ui_movie.textEdit_comment,
                              self.ui_movie.textEdit_content,

                            )
        return list_qedittext

    def list_all_qpushbutton(self):
        list_qpushbutton = (self.ui_movie.pushButton_del_back_cover,
                            self.ui_movie.pushButton_zoom_back,
                            self.ui_movie.pushButton_add_back_cover,
                            self.ui_movie.pushButton_add_front_cover,
                            self.ui_movie.pushButton_close,
                            self.ui_movie.pushButton_del_front_cover,
                            self.ui_movie.pushButton_delete_film,
                            self.ui_movie.pushButton_print_film,
                            self.ui_movie.pushButton_save_film,
                            self.ui_movie.pushButton_zoom_front,
                            self.ui_movie.pushButton_add_genre,
                            self.ui_movie.pushButton_del_genre,
                            self.ui_movie.pushButton_add_cast,
                            self.ui_movie.pushButton_del_cast,
                            self.ui_movie.pushButton_add_crew,
                            self.ui_movie.pushButton_del_crew,
                            self.ui_movie.pushButton_edit_country_of_production,
                            self.ui_movie.pushButton_edit_color_format,
                            self.ui_movie.pushButton_add_sync,
                            self.ui_movie.pushButton_del_sync,
                            self.ui_movie.pushButton_add_production_company,
                            self.ui_movie.pushButton_del_production_company,
                            self.ui_movie.pushButton_add_film_studio,
                            self.ui_movie.pushButton_del_film_studio,
                            self.ui_movie.pushButton_add_film_version,
                            self.ui_movie.pushButton_del_film_version,
                            self.ui_movie.pushButton_add_film_edition,
                            self.ui_movie.pushButton_del_film_edition,

                            )
        return list_qpushbutton

    def list_all_qtabWidget(self):
        list_tabWidget = (self.ui_movie.tabWidget_details,
                          self.ui_movie.tabWidget_cover,
                          self.ui_movie.tabWidget_more_detail,

                        )
        return list_tabWidget


    def list_all_qcombobox(self):
        list_qcombobox = (self.ui_movie.comboBox_evaluation_genre,
                          self.ui_movie.comboBox_total_evaluation,
                          self.ui_movie.comboBox_country_of_production,
                          self.ui_movie.comboBox_color_format,
                        )

        return list_qcombobox

    def list_all_treewidget(self):
        list_treewidget = (self.ui_movie.treeWidget_cast,
                           self.ui_movie.treeWidget_crew,
                           self.ui_movie.treeWidget_sync_cast,
                           self.ui_movie.treeWidget_sync,
                           self.ui_movie.treeWidget_production_company,
                           self.ui_movie.treeWidget_film_studio,
                           self.ui_movie.treeWidget_film_version,
                           self.ui_movie.treeWidget_film_edition,
                           self.ui_movie.treeWidget_genre,
                            )
        return  list_treewidget

    def list_all_qlineedit(self):
        list_qlineedit = (self.ui_movie.lineEdit_original_title,
                          self.ui_movie.lineEdit_year_production

                        )

        return  list_qlineedit

    def list_all_qlabel(self):
        list_qlabel = (self.ui_movie.label_total_evaluation,
                       self.ui_movie.label_evaluation_genre,
                       self.ui_movie.label_production_year,
                       self.ui_movie.label_original_title,
                       self.ui_movie.label_genre,
                       self.ui_movie.label_color_format,
                       self.ui_movie.label_country_of_production,
                       self.ui_movie.label_translate_title
                        )
        return list_qlabel

    def on_lineEdit_changed(self):
        qline_edit_list = self.list_all_qlineedit()

        for element_line_edit in qline_edit_list:
            element_line_edit.textChanged.connect(self.on_text_change)

    def on_textedit_changed(self):
        qtext_edit_list = self.list_all_qtextedit()

        for element_text_edit in qtext_edit_list:
            element_text_edit.textChanged.connect(self.on_text_edit_change)

    def on_text_change(self):
        self.qline_edit_list = self.list_all_qlineedit()

        for self.element_line_edit in self.qline_edit_list:
            if not self.element_line_edit.text() != "":
                self.element_line_edit.setStyleSheet(self.css.set_css_text_field_color_normal)  # textfield is empty
            else:
                self.element_line_edit.setStyleSheet(self.css.set_css_text_field_color_accept)  # textfield is full

    def on_text_edit_change(self):
        self.qtext_edit_list = self.list_all_qtextedit()

        for self.element_text_edit in self.qtext_edit_list:
            if not self.element_text_edit.toPlainText() != "":
                self.element_text_edit.setStyleSheet(self.css.set_css_text_edit_field_color_normal)  # textfield is empty
            else:
                #pass
                self.element_text_edit.setStyleSheet(self.css.set_css_text_edit_field_color_accept)  # textfield is full

    def closeEvent(self, event):
        pass

    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        print "STATUS [OK]  (", FILE_NAME, "): The function (close_form) was called"

