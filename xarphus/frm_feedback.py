#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_feedback.py"

'''
The modules of required libraries are imported.
'''
import os

from PyQt4.QtCore import QRegExp, Qt, QFile
from PyQt4.QtGui import QValidator, QRegExpValidator, QDialog
from PyQt4.uic import loadUi

from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
from xarphus.languages.german import Language

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'feedback.ui')
UI_PATH = QFile(":/ui_file/feedback.ui")

class Feedback_Window(QDialog):
    def __init__(self, info_app, parent):

        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/feedback.ui")
        #self.getPath_feedback = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'feedback.ui')

        self.set_language = Language()

        self.get_app_info = info_app

        UI_PATH.open(QFile.ReadOnly)
        self.ui_feedback = loadUi(UI_PATH, self)
        UI_PATH.close()

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_feedback.setWindowFlags(flags)

        self.set_gui()
        self.lineEdit_changed()
        self.lineEdit_validator()
        self.set_action_buttons()

    def set_gui(self):
        self.ui_feedback.setWindowModality(Qt.ApplicationModal)

    def lineEdit_changed(self):
        self.ui_feedback.lineEdit_subject.textChanged.connect(self.on_text_change)
        self.ui_feedback.textEdit_feedback.textChanged.connect(self.on_text_change)

    def lineEdit_validator(self):
        self.ui_feedback.lineEdit_subject.setStyleSheet('background-color: #fff79a;')


        self.ui_feedback.lineEdit_subject.setValidator(
            QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9 ]+"), self.lineEdit_subject))


        # self.ui_feedback.textEditFeedback.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9 ]+"), self.textEditFeedback))

    def set_action_buttons(self):
        self.ui_feedback.pushButton_send.clicked.connect(self.check_inputs)
#--------------------------------------------------------------------------------------
    def check_inputs(self):

        edit_fields = [self.ui_feedback.textEdit_feedback,
                       self.textEditTest_2,
                       self.textEditTest]

        line_fields = [self.ui_feedback.lineEdit_subject,
                       self.lineEditTest,
                       self.lineEditLetzter]

        for self.line_edit in line_fields:
            self.var_line_edit = self.line_edit.text()
            if not self.var_line_edit.trimmed():
                self.show_msgbox_error()
                self.line_edit.setStyleSheet('background-color: #f6989d;')
                break

            # def check_inputs(self):

            # edit_fields = [self.ui_feedback.textEditFeedback,
            #                   self.textEditTest_2,
            #                   self.textEditTest]

            #   line_fields = [self.ui_feedback.lineEditSubject,
            #                   self.lineEditTest,
            #                   self.lineEditLetzter]

            #   print edit_fields, line_fields

            #   for line_field in line_fields:
            #       var_line_field = line_field.text()

            #      if not var_line_field.trimmed():
            #          self.show_msgbox_error()
            #          line_field.setStyleSheet('background-color: #f6989d;')  # red
            #          print "Fehler: var_lineField"
            #          break

            #       print "zuende"
            #for self.edit_field in edit_fields:
            #    var_edit_field = self.edit_field.toPlainText()

            #   if not var_edit_field.trimmed():

            #       self.show_msgbox_error()
            #       self.edit_field.setStyleSheet('background-color: #f6989d;')  # red
            #       print "Fehler: var_editField"
            #       break


            #for editField in editFields:
            #    self.var_editfield = editField.toPlainText()

            #   if not self.var_editfield.trimmed():
            #       editField.setStyleSheet('background-color: #f6989d;')  # red
            #       self.show_msgbox_error()
            #       print "Fehler: var_editfield"
            #       break
            #    else:
            #        editField.setStyleSheet('background-color: #c4df9b;')  # green
            #        print "Leer"

            #for lineField in lineFields:

            #   self.var_linefield = lineField.text()

            #   print self.var_linefield

            #   if not self.var_linefield.trimmed():
            #       self.show_msgbox_error()
            #       break
            #   else:
            #       print "Leer"

            #if not self.var_linefield.trimmed() and self.var_editfield.trimmed():
            #    print "Error"
            #else:
            #    print "Done"

            #if not field:
            #    print "No error"
            #else:
            #    self.show_msgbox_error()
            #    break
            #for i in range(len(editFields)):
            #    print i, editFields[i]
            #    if editFields[i].strip() == '':
            #        if not len(editFields) <= i:
            #print "empty", i
            #            print "element %s empty" % i
            #            self.show_msgbox_error()
            #        else:
            #            print "not empty"
            #        break

            #if editFields[i].strip() == 0:
            #    print 'Found element!'
            #else:
            #    print 'Empty element.'
            #    editFields[i].setStyleSheet('background-color: #f6989d;')

            #for field in editFields:
            #    if field.strip() == '':
            #i.setStyleSheet('background-color: #f6989d;')
            #        self.show_msgbox_error()


            #if not len(field.strip()) == 0:

            #if not self.my_string.strip() == '':
            #if not len(self.str_lineEditSubject.strip()) == 0:
            #    print "not blank string"
            #    if self.str_textEditFeedback:
            #        print "not blank string"
            #    else:
            #        print "str_textEditFeedback: blank string"
            #        self.ui_feedback.textEditFeedback.setStyleSheet('background-color: #f6989d;') # red
            #        self.show_msgbox_error()
            #else:
            #    print "blank string"
            #    self.ui_feedback.lineEditSubject.setStyleSheet('background-color: #f6989d;') #red
            #    self.show_msgbox_error()
            #qApp = QApplication(self)
            #for widget in qApp.allWidgets():
            #    if isinstance(widget, QLineEdit):
            #        widget.clear()
#--------------------------------------------------------------------------------------
    def show_msgbox_error(self):
        give_error_message(self)
#--------------------------------------------------------------------------------------
    def on_text_change(self):
        self.ui_feedback.pushButton_send.setEnabled(
            self.lineEdit_subject.text() != ""
            and self.textEdit_feedback.toPlainText() != "")

        if not self.ui_feedback.lineEdit_subject.text() != "":
            self.ui_feedback.lineEdit_subject.setStyleSheet('background-color: #fff79a')  # yellow
        else:
            self.ui_feedback.lineEdit_subject.setStyleSheet('background-color: #c4df9b')  # green

        if not self.ui_feedback.textEdit_feedback.toPlainText() != "":
            self.ui_feedback.textEdit_feedback.setStyleSheet('background-color: #fff79a')  # yellow
        else:
            self.ui_feedback.textEdit_feedback.setStyleSheet('background-color: #c4df9b')  # green
#--------------------------------------------------------------------------------------
    def check_inputs_out(self):
        # -------------------------------------------------------------------------------
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        # -------------------------------------------------------------------------------
        if state == QValidator.Acceptable:
            self.ui_feedback.pushButton_send.setEnabled(
                self.lineEdit_subject.hasAcceptableInput()
                and self.lineEdit_subject.hasAcceptableInput())
            # -------------------------------------------------------------------------------
            color = '#c4df9b'  # green
        # -------------------------------------------------------------------------------
        elif state == QValidator.Intermediate:

            color = '#fff79a'  # yellow
            self.ui_pp_login.pushButton_send.setEnabled(False)

        else:
            color = '#f6989d'  # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)