#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from os.path import expanduser

from PyQt4.QtCore import Qt, QFile, pyqtSignal
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QWidget, QFrame, QLabel, QPushButton


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class PushbuttonsClosePrintDeleteSave_Window(QWidget):
    
    close_signal = pyqtSignal()
    save_signal = pyqtSignal()
    delete_signal = pyqtSignal()

    def __init__(self, 
                 person_id = None, 
                 set_enabled_push_button_delete = False,
                 parent = None):

        QWidget.__init__(self, parent)

        self.person_id = person_id
        self.set_enabled_push_button_delete = set_enabled_push_button_delete

        parent.set_enabled_push_button_signal.connect(self.set_enabled_push_button)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/pushbuttons_close_print_delete_save.ui")

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__


    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_pushbutton_close_print_delete_save = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.ui_pushbutton_close_print_delete_save.pushButton_delete.setEnabled(self.set_enabled_push_button_delete)

        self.init_clicked_signal_push_buttons()

        if self.person_id is None:
            self.hide_status_widget()

        self.ui_pushbutton_close_print_delete_save.pushButton_close.setEnabled(True)

        return

    def set_text(self, 
                 added_text = "", 
                 changed_text = ""):
        '''
            NOTICE:
            =======
            This method is used to set the text in QLabel(). 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the text.")

        #   We check if the given argument is True. Theat means, when the given argument are empty its False,
        #   otherwise its True
        if added_text: 
            self.ui_pushbutton_close_print_delete_save.label_created_date_time.setText(unicode(added_text))

        if changed_text:
            self.ui_pushbutton_close_print_delete_save.label_changed_date_time.setText(unicode(changed_text))

        return

    def manage_save_push_button(self, bool_value):
        '''
            NOTICE:
            =======
            This method is used to enabling the QPushButton()-object. 

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.ui_pushbutton_close_print_delete_save.pushButton_save.setEnabled(bool_value)

        return

    def init_clicked_signal_push_buttons(self):

        #self.ui_pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close)
        self.ui_pushbutton_close_print_delete_save.pushButton_save.clicked.connect(lambda: self.save_signal.emit())
        self.ui_pushbutton_close_print_delete_save.pushButton_delete.clicked.connect(lambda: self.delete_signal.emit())
        self.ui_pushbutton_close_print_delete_save.pushButton_close.clicked.connect(lambda: self.close_signal.emit())

    def hide_status_widget(self):
        '''
            NOTICE:
            =======
            This method is used to hide a widget.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Hide the QFrame()object")

        frames = self.ui_pushbutton_close_print_delete_save.findChildren(QFrame)

        for frame in frames:
            frame.close()

        return

    def set_font_color_label(self):
        '''
            NOTICE:
            =======
            This method is used to set color of text of a QLabel()-object.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Set color of text of QLabel")

        labels = self.ui_pushbutton_close_print_delete_save.findChildren(QLabel)
        
        for label in labels:
            label.setStyleSheet("QLabel {color : red; }")

        return

    def set_enabled_push_button(self,  
                                work_mode,
                                bool_value):
        '''
            NOTICE:
            =======
            This method sets all QPushButton of the specified parentWidget. 

            PARAMETERS:
            ===========
            :work_mode     -   This keyword argument gets a psuhbutton-widget
                                        that is not to be deactivated. As default there is 
                                        the keyword None.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set enabled for all push buttons")

        for push_button in self.ui_pushbutton_close_print_delete_save.findChildren(QPushButton):
            push_button.setEnabled(False)

        self.ui_pushbutton_close_print_delete_save.pushButton_close.setEnabled(True)

        if work_mode == 'add_movie':

            self.ui_pushbutton_close_print_delete_save.pushButton_save.setEnabled(bool_value)

        return

    def closeEvent(self, event):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        self.close_signal.emit()

        '''
             If the python interpreter is running that module (the source file) as the main program,
             then we need the following block. otherwise the upper emit()-method is enough.
        '''

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_pushbutton_close_print_delete_save.close()




        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication

    from core.manage_path import is_specifically_suffix

    from core.manage_ini_file import get_configuration, DefaultINIFile

    from core.config import ConfigurationSaverDict

    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
   
    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()

    if not USER_INI_FILE_PATH:
        print "You entered nothing!"
    else:
        dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

        get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


        LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

        result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

        if not result_suffix == True:
            LOG_FILE_FOLDER_PATH = result_suffix

        else:
            pass

        LOG_FILE_PATH = os.path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
        configure_logging(LOG_FILE_PATH)

        app = QApplication(sys.argv)

        translator = QTranslator()
        path = ":/translators/german_sie.qm"
        translator.load(path)
        app.installTranslator(translator)

        window = PushbuttonsClosePrintDeleteSave_Window(None)

        #window.resize(600, 400)
        window.showMaximized()
        sys.exit(app.exec_())
