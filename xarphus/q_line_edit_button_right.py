#!/usr/bin/env python
#-*- coding:utf-8 -*-

from PyQt4.QtCore import Qt, pyqtSignal, QTimer
from PyQt4.QtGui import QLineEdit, QPushButton, QIcon, QHBoxLayout, QPixmap


class ButtonLineEdit(QLineEdit):
    '''
           SUMMARY
           =======
           Reimplementation of QLineEdit.

           EXTENTED DISCRIPTION
           ====================
           The reimplemented QLineEdit contains a QPushButton on right side.


    '''
    set_default_widget_signal = pyqtSignal()

    def __init__(self,
                 image_path_collection = None,
                 parent = None):
        QLineEdit.__init__(self, parent)

        self.image_path_collection = image_path_collection

        self.icon = QIcon(self.image_path_collection.delete_12x12)
        self.pixmap = QPixmap(self.icon)
        self.delete_push_button_line_edit = QPushButton(self)
        self.delete_push_button_line_edit.setObjectName("delete_push_button_line_edit")
        self.delete_push_button_line_edit.setCursor(Qt.ArrowCursor)
        self.delete_push_button_line_edit.setFocusPolicy(Qt.NoFocus)
        self.delete_push_button_line_edit.clicked.connect(self.clear)
        self.delete_push_button_line_edit.clicked.connect(lambda: self.setFocus(True))
        self.delete_push_button_line_edit.setIcon(self.icon)
        self.delete_push_button_line_edit.setFlat(True)
        self.delete_push_button_line_edit.clicked.connect(self.set_default_widget)
        self.delete_push_button_line_edit.setVisible(False)

        self.textChanged[unicode].connect(self.text_chaned)


        layout = QHBoxLayout(self)
        layout.addWidget(self.delete_push_button_line_edit, 0, Qt.AlignRight) # Put the button on right side

        layout.setSpacing(0)
        layout.setMargin(5)

    def text_chaned(self, text):
        '''
           SUMMARY
           =======
           This method is used whenever the user changes the text.

           PARAMETERS
           ==========
           :text (class):    The text argument is the new text.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        #   In general we want this method to check whether the received text argument is empty.
        if len(unicode(text).strip()) > 0:
            # The given text argument isn't empty, so the button shouldn't be hidden.
            self.delete_push_button_line_edit.setVisible(True)
        else:
            # The button should be hidden as long the QLineEdit is empty.
            self.delete_push_button_line_edit.setVisible(False)

        return

    def set_default_widget(self):
        '''
           SUMMARY
           =======
           This method is used to set on default.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        self.delete_push_button_line_edit.setVisible(False)
        self.set_default_widget_signal.emit()
