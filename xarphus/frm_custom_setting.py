#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_update.py"

import os
import sys

from inspect import currentframe

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow

from xarphus.core.manange_logging import set_custom_logger

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class CustomSetting_Window(QWidget):
    def __init__(self, parent):

        QWidget.__init__(self, parent)


        #self.custom_logger.info("The instance of the class of " + self.get_class_name() + "() is created - (" +  __name__ +")")

        UI_PATH = QFile(":/ui_file/note.ui")


        try:
            #self.custom_logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            UI_PATH.open(QFile.ReadOnly)
            #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            #self.custom_logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  __name__ +")")
            self.ui_note = loadUi(UI_PATH, self)
            #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            #self.custom_logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded - (" +  __name__ +")")
            UI_PATH.close()
            #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
        except ImportError as ImpErr:
            pass
            #self.custom_logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            #self.custom_logger.warning(ImpErr, " - (" +  __name__ +")")
        except Exception as ex:
            pass
            #self.custom_logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            #self.custom_logger.warning(str(ex), " - (" + str(__name__) +")")
        except:
            pass
            #self.custom_logger.warning("The *.ui-files ("+ str(UI_PATH.fileName()) +") can't open/read - (" +  __name__ +") - (Line " + str(self.get_line_number()) + ")")
            #self.custom_logger.info(sys.exc_info()[0], " - (" +  __name__ +")")
            raise

        self.set_gui()
        self.set_action_buttons()

    def set_gui(self):
        pass
        #self.ui_movie_general_details.setWindowModality(Qt.ApplicationModal)
        #self.ui_movie_general_details.lineEdit_password.returnPressed.connect(self.pushButton_ok.click)


    def get_pwd(self):
        pass
        #send_pwd = unicode(self.ui_movie_general_details.lineEdit_password.text())
        #return send_pwd

    def zwischen_schritt(self):
        pass
        #me = unicode(self.get_pwd())
        #self.zip_file_p(me)
        #self.close()

    def set_action_buttons(self):
        pass
        #self.ui_note.pushButton_close.clicked.connect(self.close_form)
        #self.ui_movie_general_details.pushButton_ok.clicked.connect(lambda: self.zip_file(self.get_pwd))
        #self.ui_movie_general_details.pushButton_ok.clicked.connect(self.zwischen_schritt)

    def get_class_name(self):
            return self.__class__.__name__

    def get_line_number(self):
        cf = currentframe()
        return cf.f_back.f_lineno - 1

    def closeEvent(self, event):
        pass
    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
