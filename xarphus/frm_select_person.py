#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import sys

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QMdiSubWindow
from PyQt4.uic import loadUi
from PyQt4.QtCore import QFile
'''
Private modules are imported.
'''
from xarphus.core.config import ConfigurationSaverDict


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
class SelectPerson_Window(QWidget):

    def __init__(self, func_up_to_date, update_sublist, parent):

        QWidget.__init__(self, parent)

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        self.set_get_settings = ConfigurationSaverDict()

        UI_PATH = QFile(":/ui_file/select_person.ui")

        UI_PATH.open(QFile.ReadOnly)
        self.ui_select_person = loadUi(UI_PATH, self)
        UI_PATH.close()


#        self.color_black = "<font color='#000000'>"
#        self.ui_select_person.groupBox.setStyleSheet('border: none solid gray;')
#        self.ui_search.textEditExplain.setStyleSheet(('background-color: #ffffff;')) # white


        #self.ui_search.setWindowModality(Qt.ApplicationModal)
        self.set_gui()
#        self.get_language_add_person()
#        self.set_actions_radiobuttons()
#        self.set_language_add_person()
#        self.ui_select_person.show()
        self.set_actions_button()
#        self.create_actions_label()
#        self.get_licence()
#--------------------------------------------------------------------------------------
    def set_gui(self):
        #pass
        self.ui_select_person.treeWidget_person.setAlternatingRowColors(True)
        self.ui_select_person.lineEdit_search.setPlaceholderText(self.tr("Suchen..."))
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowMaximizeButtonHint)
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint | Qt.WindowMinMaxButtonsHint)

        #self.ui_select_person.setWindowFlags(self.ui_select_person.windowFlags() | Qt.CustomizeWindowHint)
        #self.ui_select_person.setWindowFlags(self.ui_select_person.windowFlags() & ~Qt.WindowMaximizeButtonHint)

    def set_color(self):
        self.color_gray = "<font color='#808080'>"
        self.color_black = "<font color='#000000'>"
#--------------------------------------------------------------------------------------
    def get_language_add_person(self):
        from xarphus.languages.german import Language
        self.set_language = Language()
        # if self.set_get_settings.dict_set_settings["language"] == "German":
        #     from xarphus.languages.german import Language
        #     self.set_language = Language()
#--------------------------------------------------------------------------------------
    def set_language_add_person(self):
        pass
    def list_all_radiobutton(self):
        list_radiobutton = (self.ui_select_person.radioButton_new_person,
                            self.ui_select_person.radioButton_existing_person
                            )
        return  list_radiobutton
#--------------------------------------------------------------------------------------
    def set_actions_radiobuttons(self):
        self.ui_select_person.radioButton_new_person.toggled.connect(self.set_pushbutton_enable)
        self.ui_select_person.radioButton_existing_person.toggled.connect(self.set_pushbutton_enable)
#--------------------------------------------------------------------------------------
    def set_pushbutton_enable(self, enabled):
        if enabled:
            self.ui_select_person.pushButton_next.setEnabled(True)
        else:
            self.ui_select_person.pushButton_next.setEnabled(False)
#--------------------------------------------------------------------------------------
    def create_actions_label(self):
        pass
        #homepage_link = self.get_app_info.dict_info["product_site"]
        #link_tag = self.color_gray + self.set_language.dict_ui_about["label_homepage"] + " <a href='%s'>" % homepage_link + self.set_language.dict_ui_about["label_homepage_url"] + "</a>"
        #self.ui_select_person.label_homepage_url.setText(link_tag)
        #self.ui_select_person.label_homepage_url.linkActivated.connect(self.open_url_homepage)
#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
    def set_actions_button(self):
        self.ui_select_person.pushButton_close.clicked.connect(self.close_parent)
        #self.ui_select_person.pushButton_next.clicked.connect(self.selected_next)

    def close_parent(self):
        #self.open_test()
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
