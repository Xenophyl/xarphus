#!/usr/bin/env python2
# -*- coding: iso-8859-15 -*-

class info_app(object):
    def __init__(self):

        self.dict_css_file_name ={"old_dark": "old_dark.qss",
                                  "black": "black.qss",
                                  "dracula": "dracula.qss",
                                  "wombat": "wombat.qss",
                                  "metal_dark": "metal_dark.qss",
                                  "metal": "metal.qss",
                                  "sandy": "sandy.qss",
                                  "fire": "fire.qss",
                                  "oxygen": "oxygen.qss",
                                  "xmas":"xmas.qss",
                                  "machinery": "machinery.qss",
                                    }
        
        self.dict_info = {
                      "product_name": "Xarphus",
                      "exe_name": "Xarphus",
                      "product_description": "Collection Manager",
                      "product_by": "by",
                      "product_author": "Sophus",
                      "product_team": "Xarphus-Team",
                      "product_copyright": "Copyright",
                      "product_copyright_characters": "�",
                      "product_year_from": "2015",
                      "product_year_till": "2015",
                      "product_version": "0.0.1",
                      "product_GUI_Version": "Qt4",
                      "product_built": "Pre-Alpha",
                      "product_faq": "http://docs.python.org/library/webbrowser.html",
                      "product_site": "http://www.web.de",
                      "product_release_note": "www.web.de",
                      "db_name": "xarphus",
                      "xarphus_installer":"xarphus_installer",
        }

        self.dict_manager_info = {
                    "movie": "Filme, Serien",
                    "books": "B�cher, Comic-Hefte, Zeitschriften, Zeitungen",
                    "musics": "Schallplatten, Alben, Singles, Einzellieder",
                    "coins": "M�nzen",
                    "contacts": "Kontakte",
                    "notes": "Notizen",
                    "videogames": "Videospiele",
                    "quotations": "Zitate",
                    "stamps": "Briefmarken",
                    "idioms": "Redensarten",
                    "persons": "Personen",
                    "articles": "Aufs�tze",
                    "miscellany": "Sammelb�nde"
        }
