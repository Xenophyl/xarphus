#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt, pyqtSignal
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QMenu, QIcon, QToolButton

try:
    from xarphus.q_line_edit_button_right import ButtonLineEdit
except ImportError:
    from q_line_edit_button_right import ButtonLineEdit

BASE_PATH = os.path.dirname(os.path.abspath(__file__))

class MovieGeneralDetails_Window(QWidget):

    init_movie_detail_signal = pyqtSignal(int)

    def __init__(self, image_path_collection, parent=None):

        QWidget.__init__(self, parent)

        self._image_path_collection = image_path_collection

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/movie_general_details.ui")

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__


    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_movie_general_details = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.init_line_edit()
        self.init_clicked_signal_push_buttons()
        self.init_ToolButton_Menu()
        self.init_tool_tip()

        return

    def init_line_edit(self):
        '''
           SUMMARY
           =======
           This method initializes the custom QLineEdit.

           EXTENTED DISCRIPTION
           ====================
           In this custom QLineEdit there is a QPushButton on right site for clear the content.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the custom QLineEdit with QPushButton on right site")

        ###################################################
        #################  Film General Actor  ############
        ###################################################
        self.ui_movie_general_details.lineEdit_search_film_general_actor_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general_details.lineEdit_search_film_general_actor_show.setObjectName("lineEdit_search_film_general_actor_show")
        self.ui_movie_general_details.lineEdit_search_film_general_actor_show.setPlaceholderText("Search by actor")
        #self.ui_movie_general_details.lineEdit_search_film_general_actor_show.set_default_widget_signal.connect(lambda:
        #  self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
        #                     column = 2))

        self.ui_movie_general_details.horizontalLayout_29.addWidget(self.ui_movie_general_details.lineEdit_search_film_general_actor_show)

        self.ui_movie_general_details.lineEdit_search_film_general_actor_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general_details.lineEdit_search_film_general_actor_edit.setObjectName("lineEdit_search_film_general_actor_edit")
        self.ui_movie_general_details.lineEdit_search_film_general_actor_edit.setPlaceholderText("Search by actor")
        #self.ui_movie_general_details.lineEdit_search_film_general_actor_edit.set_default_widget_signal.connect(lambda:
        #  self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
        #                     column = 2))

        self.ui_movie_general_details.horizontalLayout_30.addWidget(self.ui_movie_general_details.lineEdit_search_film_general_actor_edit)

        ###################################################
        #################  Film General Crew  #############
        ###################################################
        self.ui_movie_general_details.lineEdit_search_film_general_crew_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general_details.lineEdit_search_film_general_crew_show.setObjectName("lineEdit_search_film_general_crew_show")
        self.ui_movie_general_details.lineEdit_search_film_general_crew_show.setPlaceholderText("Search by crew")
        #self.ui_movie_general_details.lineEdit_search_film_general_crew_show.set_default_widget_signal.connect(lambda:
        #  self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
        #                     column = 2))

        self.ui_movie_general_details.horizontalLayout_31.addWidget(self.ui_movie_general_details.lineEdit_search_film_general_crew_show)

        self.ui_movie_general_details.lineEdit_search_film_general_crew_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general_details.lineEdit_search_film_general_crew_edit.setObjectName("lineEdit_search_film_general_crew_edit")
        self.ui_movie_general_details.lineEdit_search_film_general_crew_edit.setPlaceholderText("Search by crew")
        #self.ui_movie_general_details.lineEdit_search_film_general_crew_edit.set_default_widget_signal.connect(lambda:
        #  self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
        #                     column = 2))

        self.ui_movie_general_details.horizontalLayout_32.addWidget(self.ui_movie_general_details.lineEdit_search_film_general_crew_edit)

        return

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots for QpuschButton()-object(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        self.ui_movie_general_details.toolButton_add_film_edition.clicked.connect(lambda: self.init_movie_detail_signal.emit(2))

        self.ui_movie_general_details.toolButton_add_film_version.clicked.connect(lambda: self.init_movie_detail_signal.emit(1))

        ###################################################
        #################  Film General Actor  ############
        ###################################################
        self.ui_movie_general_details.pushButton_addpage_general_film_film_actor.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general_details.stackedWidget_general_film_actor,
                           page_index = 1))

        self.ui_movie_general_details.pushButton_back_general_film_film_actor.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general_details.stackedWidget_general_film_actor,
                           page_index = 0))

        ###################################################
        #################  Film General Crew  #############
        ###################################################
        self.ui_movie_general_details.pushButton_addpage_general_film_film_crew.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general_details.stackedWidget_general_film_crew,
                           page_index = 1))

        self.ui_movie_general_details.pushButton_back_general_film_film_crew.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general_details.stackedWidget_general_film_crew,
                           page_index = 0))

        return

    def test_func(self):
        pass

    def init_tool_tip(self):
        '''
           SUMMARY
           =======
           This method is used to set a widget's tool tip.
           Then the tool tip is shown whenever the cursor points at the widget.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set a widget's tool tip")
        # QOushButton
        self.ui_movie_general_details.pushButton_addpage_general_film_film_actor.setToolTip(self.tr('Add more actors'))
        self.ui_movie_general_details.pushButton_del_current_general_film_film_actor.setToolTip(self.tr('Delete the selected actor'))

        self.ui_movie_general_details.pushButton_addpage_general_film_film_crew.setToolTip(self.tr('Add more crews'))
        self.ui_movie_general_details.pushButton_del_current_general_film_film_crew.setToolTip(self.tr('Delete the selected country'))

        return

    def change_page(self,
                    stacked_widget,
                    page_index):
        '''
           SUMMARY
           =======
           This function is used to change the view of stacked widget.

           PARAMETERS
           ==========
           :stacked_widget (class): We except a class of QStackedWidget. We need it because
                                    we want to change the index on current QStackedWidget.

           :page_index (int):       To change the index of current stacked widget we also need
                                    an current index.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Change the view of stacked widget")

        stacked_widget.setCurrentIndex(page_index)

        return

    def init_ToolButton_Menu(self):
        '''
           SUMMARY
           =======
           This method initializes a pop up menu for
           the QToolButton()-object.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("initialize the PopMenu for QToolButon")

        '''
            First wer save the menu item labels in the variables, because
            this label will often be needed.
        '''
        label_first_menu_item = self.tr("ViewBLUBB")
        label_second_menu_item = self.tr("Detaills (No Symbols)")
        label_third_menu_item = self.tr("Details (Symbols)")

        '''
            We've created menus
        '''
        menu_view_cast = QMenu()
        menu_view_crew = QMenu()
        menu_view_voice_actor = QMenu()

        '''
        addAction that will add menu items to your newly created QMenu.
        The actions are created by calling addAction and giving.
        Its a string that will be used as the menu item
        text and a method (self.Action1 or self.Action2) that
        will be called if that menu item is selected.
        '''
        menu_view_cast.addAction(label_first_menu_item).setEnabled(False)
        menu_view_cast.addSeparator()
        menu_view_cast.addAction(QIcon(self._image_path_collection.details_view_icon_230x211), label_second_menu_item, self.test_func)
        menu_view_cast.addAction(QIcon(self._image_path_collection.details_view_pic_icon_259x218), label_third_menu_item, self.test_func)

        menu_view_cast.addAction(QIcon(self._image_path_collection.setting_icon_16x16), 'Select details...', lambda:
        self.create_select_details_window(self.get_list_items('details_view_cast')))

        menu_view_crew.addAction(label_first_menu_item).setEnabled(False)
        menu_view_crew.addSeparator()
        menu_view_crew.addAction(QIcon(self._image_path_collection.details_view_icon_230x211), label_second_menu_item, self.test_func)
        menu_view_crew.addAction(QIcon(self._image_path_collection.details_view_pic_icon_259x218), label_third_menu_item, self.test_func)

        menu_view_crew.addAction(QIcon(self._image_path_collection.setting_icon_16x16), 'Select details...', lambda:
        self.create_select_details_window(self.get_list_items('details_view_crew')))

        menu_view_voice_actor.addAction(label_first_menu_item).setEnabled(False)
        menu_view_voice_actor.addSeparator()
        menu_view_voice_actor.addAction(QIcon(self._image_path_collection.details_view_icon_230x211), label_second_menu_item, self.test_func)
        menu_view_voice_actor.addAction(QIcon(self._image_path_collection.details_view_pic_icon_259x218), label_third_menu_item, self.test_func)

        menu_view_voice_actor.addAction(QIcon(self._image_path_collection.setting_icon_16x16), 'Select details...', lambda:
        self.create_select_details_window(self.get_list_items('details_view_voice_actor')))

        '''
        setPopupMode:               This property describes the way that
                                    popup menus are used with tool buttons

        TQToolButton.InstantPopup:  he menu is stack_set_current_indexed, without delay,
                                    when the tool button is pressed.
                                    In this mode, the button's own action
                                    is not triggered.
        '''
        #self.ui_movie_general_details.toolButton_view_cast.setMenu(menu_view_cast)
        #self.ui_movie_general_details.toolButton_view_cast.setPopupMode(QToolButton.InstantPopup)

        #self.ui_movie_general_details.toolButton_view_crew.setMenu(menu_view_crew)
        #self.ui_movie_general_details.toolButton_view_crew.setPopupMode(QToolButton.InstantPopup)

        return

    def start_threads(self,
                     **kwargs):
        '''
           SUMMARY
           =======
           This method starts all saved thrads or a certain saved threads.

           EXTENTED DISCRIPTION
           ====================
           This function not only starts threads, but also tries to get current
           scoped session for communicating with database.

           RETURNS
           =======
           :return:   Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start all or certain saved thread")

        # kwargs.get('pyqt_signal').emit()

        # dict_fetch_all_threads = {
        #     ###################################################
        #     #################  Actor / Crew  ##################
        #     ###################################################
        #     "fetch_all_general_film_actor": lambda: self.fetch_all(scoped_session = self._scoped_session,
        #                                                            certain_proxy_model = kwargs.get('general_proxy_model').proxy_model_film_general_general_country,
        #                                                            general_proxy_model = kwargs.get('general_proxy_model'),
        #                                                            general_standard_item_model = kwargs.get('general_standard_item_model'),
        #                                                            work_mode = 'fetch_all',
        #                                                            certain_standard_item_model = kwargs.get('general_standard_item_model').standard_item_model_general_country,
        #                                                            widget_tree_view = self.ui_movie_general.treeView_show_film_general_production_country,
        #                                                            show_messagebox = kwargs.get('show_messagebox', True),
        #                                                            hide_column = True,
        #                                                            category = 'general_country'),


        # if not kwargs.get('category') == "all":

        #     dict_fetch_all_threads[category]()

        # elif kwargs.get('category') == "all":

        #     for _, value in dict_fetch_all_threads.iteritems():

        #         value()

        return

    def closeEvent(self, event):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_movie_general_details.close()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication

    from core.manage_path import is_specifically_suffix

    from core.manage_ini_file import get_configuration, DefaultINIFile

    from core.config import ConfigurationSaverDict

    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging

    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()

    if not USER_INI_FILE_PATH:
        print "You entered nothing!"
    else:
        dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

        get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


        LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

        result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

        if not result_suffix == True:
            LOG_FILE_FOLDER_PATH = result_suffix

        else:
            pass

        LOG_FILE_PATH = os.path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
        configure_logging(LOG_FILE_PATH)

        app = QApplication(sys.argv)

        translator = QTranslator()
        path = ":/translators/german_sie.qm"
        translator.load(path)
        app.installTranslator(translator)

        window = MovieGeneralDetails_Window(None)

        #window.resize(600, 400)
        window.showMaximized()
        sys.exit(app.exec_())
