#!/usr/bin/env python
#-*- coding:latin1 -*-

FILE_NAME = __name__

import os
import sys
from inspect import currentframe

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QLabel, QFont, QSizePolicy


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class OverViewRatingsClassification_Window(QWidget):
    def __init__(self, func_up_to_date,
                 update_sublist,
                 info_app,
                 create_rating,
                 create_classification,
                 object_category,
                 parent):

        QWidget.__init__(self, parent)

        self.info_app = info_app

        
        logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")

        UI_PATH = QFile(":/ui_file/overview_rating_classification.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.create_rating = create_rating
        self.create_classification = create_classification
        self.object_category = object_category

        #print "OBJEKT KATEGORIE - OverViewRatingsClassification_Window", self.object_category

        logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + "opened")
        logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + "")

        self.ui_over_view_ratings = loadUi(UI_PATH, self)

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded")
        logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.close()

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed")

        self.font = QFont()
        self.font.setBold(False)

        self.my_label = QLabel()
        self.my_label.setText("Sie haben entweder noch keine Altersfreigaben eingetragen oder Sie haben noch keine Altersfreigaben ausgewählt.")
        self.my_label.setFont(self.font)
        self.my_label.setWordWrap(True)
        self.my_label.setSizePolicy(QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        self.my_label.setAlignment(Qt.AlignCenter)

        self.init_ui()
        self.init_pushButton()

    def test_it(self):
        self.ui_over_view_ratings.treeWidget_rating_classification.setVisible(True)
        self.ui_over_view_ratings.toolButton_edit_classification.setVisible(True)
        self.ui_over_view_ratings.toolButton_add_classification.setVisible(True)
        self.ui_over_view_ratings.toolButton_delete_classification.setVisible(True)
        self.my_label.setVisible(False)

    def init_ui(self):

        self.ui_over_view_ratings.treeWidget_overview_ratings.setColumnWidth(0, 0)
        self.ui_over_view_ratings.treeWidget_rating_classification.setColumnWidth(0, 0)

        self.ui_over_view_ratings.treeWidget_rating_classification.setVisible(False)
        self.ui_over_view_ratings.toolButton_edit_classification.setVisible(False)
        self.ui_over_view_ratings.toolButton_add_classification.setVisible(False)
        self.ui_over_view_ratings.toolButton_delete_classification.setVisible(False)

        self.ui_over_view_ratings.horizontalLayout_2.addWidget(self.my_label)
#        self.ui_over_view_ratings.horizontalLayout_4.setVisible(False)

    def init_pushButton(self):
        self.ui_over_view_ratings.pushButton_close.clicked.connect(self.close_form)
        self.ui_over_view_ratings.toolButton_add_ratings.clicked.connect(self.test_it)

        self.ui_over_view_ratings.toolButton_add_ratings.clicked.connect(lambda: self.create_rating(self.object_category))

        self.ui_over_view_ratings.toolButton_add_classification.clicked.connect(
                                                        lambda: self.create_classification(show_format="ADDING"))
        self.ui_over_view_ratings.toolButton_edit_classification.clicked.connect(
                                                        lambda: self.create_classification(show_format="EDITING"))

    def get_class_name(self):
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")
        return self.__class__.__name__

    def closeEvent(self, event):
        pass

    def close_form(self):
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")