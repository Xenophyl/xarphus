#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#   Change the version of QVariant with sip before importing PyQt
import sip
sip.setapi('QVariant',2)

from sys import exit, argv, exc_info, settrace
from os import path
from traceback import format_exc

try: #  Import all modules from...
    #   ... core
    from xarphus.core.managed_defined_session import *
    #from xarphus.core.manage_data_manipulation_master_data import select_all

    from xarphus.q_line_edit_button_right import ButtonLineEdit

    from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel
    from xarphus.core.manage_input_controller import InputController
    from xarphus.subclass_master_data_load_data_item import LoadDataItem, TaskDataManipulation, TaskModel
    from xarphus.core.manage_qt import ProgressBar
    from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    #from xarphus.collection_paths_image import ImagePathnCollection
    from xarphus.core.manage_prase import parsing_str_to_bool
    from xarphus.frm_waiting import Waiting_Window
    from xarphus.core.normalize_uni_str import normalize_text
    from xarphus.core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate

    from xarphus.core.manage_tracing_calls import tracefunc
except ImportError: #   When the module is started as a program
    #   Import all compiled files
    from gui import ui_rc
    #   Import all modules from core
    from core.managed_defined_session import *
    #from core.manage_data_manipulation_master_data import select_all
    from core.manage_qt import ProgressBarText
    from core.custom_sort_filter_model import MySortFilterProxyModel
    from core.normalize_uni_str import normalize_text
    from core.manage_input_controller import InputController
    from core.manage_prase import parsing_str_to_bool
    #from customsortfiltermodel import MySortFilterProxyModel
    from subclass_master_data_load_data_item import TaskDataManipulation, TaskModel
    from core.config import ConfigurationSaverDict
    from core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from collection_paths_image import ImagePathnCollection
    from frm_waiting import Waiting_Window
    from images import images_rc
    from core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate
    from q_line_edit_button_right import ButtonLineEdit
from sqlalchemy import create_engine

from PyQt4.QtCore import QFile, Qt, QTimer, pyqtSignal, \
                        QThread, QRegExp, \
                        pyqtSlot, QVariant, QString

from PyQt4.uic import loadUi

from PyQt4.QtGui import QWidget, QMdiSubWindow, QIcon, QMessageBox, QPushButton, QTreeWidgetItem, \
                        QLineEdit, QStandardItemModel, QAbstractItemView, QAction, QMenu, \
                        QTextEdit, QPlainTextEdit, QTreeView, QStandardItem, \
                        QProgressBar, QLabel


class MasterData_Window(QWidget):
    '''
           SUMMARY
           =======
           Administration of the user interface for master data.
    '''

    interrupt_working_thread_signal = pyqtSignal()
    set_flat_signal = pyqtSignal(str, bool)

    count_progress_signal = pyqtSignal(int)
    update_progress_bar_signal = pyqtSignal(int, int)
    close_wait_message_signal = pyqtSignal()
    set_enable_widget_signal = pyqtSignal(bool)

    def __init__(self,
                 parent,
                 **kwargs):
        '''
           PARAMETERS
           ==========
           :image_path_collection (dict):               In the given dictionary there a paths to several images.

           :configuration_saver_dict (dict):            We need the saved settings, which they are saved in the
                                                        given dictionary.

           :general_proxy_model (class):                blahh

           :parent (class):                             The class supports passing a parent arguments
                                                        to its base class. Its more felxible. By default,
                                                        the parent is set to None.

           :page_number (int):                          To change the pages. By default
                                                        the page is set to Zero, which means,
                                                        the first page is be shown.

        '''

        QWidget.__init__(self, parent)

        #settrace(tracefunc)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        ''' Store constance '''
        self.LIST_HORIZONTAL_HEADER_GENERAL_REGION = [self.tr("ID"),
                                                      self.tr("Region")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_AWARD = [self.tr("ID"),
                                                     self.tr("Award")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_COUNTRY = [self.tr("ID"),
                                                       self.tr("Country")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_LANGUAGE = [self.tr("ID"),
                                                        self.tr("Language")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_MANUFACTURER = [self.tr("ID"),
                                                            self.tr("General manufacturer /-brand")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_MEDIA_TYPE = [self.tr("ID"),
                                                          self.tr("Media Type")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_PACKAGING = [self.tr("ID"),
                                                         self.tr("Packaging")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_AGE_RELEASE = [self.tr("ID"),
                                                           self.tr("Age release")]
        self.LIST_HORIZONTAL_HEADER_PERSON_GENDER = [self.tr("ID"),
                                                     self.tr("Gender")]
        self.LIST_HORIZONTAL_HEADER_PERSON_NATIONALITY = [self.tr("ID"),
                                                          self.tr("Nationality")]
        self.LIST_HORIZONTAL_HEADER_PERSON_SALUTATION = [self.tr("ID"),
                                                         self.tr("Salutation")]
        self.LIST_HORIZONTAL_HEADER_PERSON_TITLE = [self.tr("ID"),
                                                    self.tr("Title")]
        self.LIST_HORIZONTAL_HEADER_PERSON_HAIR_COLOR = [self.tr("ID"),
                                                         self.tr("Hair Color")]
        self.LIST_HORIZONTAL_HEADER_PERSON_EYE_COLOR = [self.tr("ID"),
                                                        self.tr("Eye Color")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELIGION = [self.tr("ID"),
                                                       self.tr("Religion")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP_STATUS = [self.tr("ID"),
                                                                  self.tr("Relationship Status")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP = [self.tr("ID"),
                                                           self.tr("Relationship")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_GENRE = [self.tr("ID"),
                                                   self.tr("Genre")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_STYLE = [self.tr("ID"),
                                                   self.tr("Style")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_STUDIO = [self.tr("ID"),
                                                    self.tr("Studio")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_PUBLISHER = [self.tr("ID"),
                                                       self.tr("Publisher")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_LABEL = [self.tr("ID"),
                                                   self.tr("Label")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_DISTRIBUTION = [self.tr("ID"),
                                                          self.tr("Distribution")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_SPARS_CODE = [self.tr("ID"),
                                                        self.tr("SPARS Code")]
        self.LIST_HORIZONTAL_HEADER_FILM_GENRE = [self.tr("ID"),
                                                  self.tr("Genre")]
        self.LIST_HORIZONTAL_HEADER_FILM_PRODUCTION_COMPANY = [self.tr("ID"),
                                                               self.tr("Film production company")]
        self.LIST_HORIZONTAL_HEADER_FILM_RENTALS = [self.tr("ID"),
                                                    self.tr("Film rentals")]
        self.LIST_HORIZONTAL_HEADER_FILM_DISTRIBUTION = [self.tr("ID"),
                                                        self.tr("Film distribution")]
        self.LIST_HORIZONTAL_HEADER_FILM_STUDIO = [self.tr("ID"), self.tr("Film studio")]
        self.LIST_HORIZONTAL_HEADER_FILM_RESOLUTION = [self.tr("ID"), self.tr("Film resolution"),
                                                       self.tr("Film presenting format")]
        self.LIST_HORIZONTAL_HEADER_FILM_REGIONAL_CODE = [self.tr("ID"),
                                                          self.tr("Film rgional code"),
                                                          self.tr("film region")]
        self.LIST_HORIZONTAL_HEADER_FILM_ASPECT_RATIO = [self.tr("ID"),
                                                         self.tr("Film aspect ratio"),
                                                         self.tr("format")]
        self.LIST_HORIZONTAL_HEADER_FILM_DIMENSION = [self.tr("ID"),
                                                      self.tr("Film dimension")]
        self.LIST_HORIZONTAL_HEADER_FILM_EDITION = [self.tr("ID"),
                                                    self.tr("Film edition")]
        self.LIST_HORIZONTAL_HEADER_FILM_VERSION = [self.tr("ID"),
                                                    self.tr("Film version")]
        self.LIST_HORIZONTAL_HEADER_FILM_VIDEONORM = [self.tr("ID"),
                                                      self.tr("Film videonorm")]
        self.LIST_HORIZONTAL_HEADER_FILM_AUDIOFORMAT = [self.tr("ID"),
                                                        self.tr("Film audioformat")]
        self.LIST_HORIZONTAL_HEADER_FILM_AUDIOCHANNEL = [self.tr("ID"),
                                                         self.tr("Film audio channel")]
        self.LIST_HORIZONTAL_HEADER_FILM_COLOURFORMAT = [self.tr("ID"),
                                                         self.tr("Film colour format")]
        self.LIST_HORIZONTAL_HEADER_FILM_VIDEOFORMAT = [self.tr("ID"),
                                                        self.tr("Film video format")]
        self.LIST_HORIZONTAL_HEADER_FILM_SOUNDSYSTEM = [self.tr("ID"),
                                                        self.tr("Film sound system")]
        self.LIST_HORIZONTAL_HEADER_BOOK_GENRE = [self.tr("ID"),
                                                  self.tr("Genre")]
        self.LIST_HORIZONTAL_HEADER_BOOK_PUBLISHER = [self.tr("ID"),
                                                      self.tr("Publisher")]
        self.LIST_HORIZONTAL_HEADER_BOOK_BINDING = [self.tr("ID"),
                                                    self.tr("Binding")]
        self.LIST_HORIZONTAL_HEADER_COIN_ALLOY = [self.tr("ID"),
                                                  self.tr("Alloy")]
        self.LIST_HORIZONTAL_HEADER_COIN_CURRENCY = [self.tr("ID"),
                                                     self.tr("Currency")]
        self.LIST_HORIZONTAL_HEADER_COIN_TYPE = [self.tr("ID"),
                                                 self.tr("Type")]
        self.LIST_HORIZONTAL_HEADER_COIN_MANUFACTURING_PROCEDURE = [self.tr("ID"),
                                                                   self.tr("Manufacturing procedure"),
                                                                   (self.tr("Shortcut"))]
        self.LIST_HORIZONTAL_HEADER_COIN_DEGREE_PRESERVATION = [self.tr("ID"),
                                                                self.tr("Degree of preservation"),
                                                                self.tr('Shortcut')]
        self.LIST_HORIZONTAL_HEADER_COIN_MINT = [self.tr("ID"),
                                                 self.tr("Mint"),
                                                 self.tr('Mint mark')]
        self.LIST_HORIZONTAL_HEADER_VIDEOGAME_GENRE = [self.tr("ID"), self.tr("Genre")]

        self.NAVI_DATA = [

            (self.tr("Film / Serie"), [
                (self.tr("Sound system"), []),
                (self.tr("Genre"), []),
                (self.tr("Regional code"), []),
                (self.tr("Aspect ratio"), []),
                (self.tr("Audio format"), []),
                (self.tr("Audio channel"), []),
                (self.tr("Color format"), []),
                (self.tr("TV Norm"), []),
                (self.tr("Resolution"), []),
                (self.tr("Video format"), []),
                (self.tr("Type of version"), []),
                (self.tr("Dimension"), []),
                (self.tr("Version / Edition"), []),
                (self.tr("Film production company"), []),
                (self.tr("film distribution"), []),
                (self.tr("Movie rentals"), []),
                (self.tr("Film studio"), []),
                ]),
            (self.tr("Music"), [
                (self.tr("Genre"), []),
                (self.tr("Style"), []),
                (self.tr("Studio"), []),
                (self.tr("Publisher"), []),
                (self.tr("Label"), []),
                (self.tr("Distribution"), []),
                (self.tr("SPARS Code"), []),
                ]),

            (self.tr("Book"), [
                (self.tr("Genre"), []),
                (self.tr("Publisher"), []),
                (self.tr("Binding"), []),
                (self.tr("Font size"), []),
                (self.tr("Color format"), []),
                ]),

            (self.tr("Video game"), [
                (self.tr("Genre"), []),
                ]),

            (self.tr("Person"), [
                (self.tr("Gender"), []),
                (self.tr("Salutation"), []),
                (self.tr("Title"), []),
                (self.tr("Nationality"), []),
                (self.tr("Eye color"), []),
                (self.tr("Hair color"), []),
                (self.tr("Religion"), []),
                (self.tr("Relationship status"), []),
                (self.tr("Relationship"), []),
                ]),

            (self.tr("Stamp"), [
                ("Blah", []),
                ]),

            (self.tr("Coin"), [
                (self.tr("Type of coin"), []),
                (self.tr("Alloy"), []),
                (self.tr("Currency"), []),
                (self.tr("Manufacturing procedure"), []),
                (self.tr("Degree of preservation"), []),
                (self.tr("Mint"), []),
                ]),

            (self.tr("Age release"), [
                ([], []),
                ]),

            (self.tr("Packaging"), [
                ([], []),
                ]),

            (self.tr("Place of storage"), [
                ([], []),
                ]),

            (self.tr("Type of media"), [
                ([], []),
                ]),

            (self.tr("media manufacturer /-brand"), [
                ([], []),
                ]),

            (self.tr("Language"), [
                ([], []),
                ]),

            (self.tr("Award"), [
                ([], []),
                ]),

            (self.tr("Country"), [
                ([], []),
                ]),

            (self.tr("Region / Place"), [
                ([], []),
                ]),
            ]

        '''
            Create an instance of imported class.
        '''
        #self.input_controller = InputController()

        #   Define pyqtSignal()
        parent.send_current_scoped_session_master_data_signal.connect(self.save_scoped_session)

        ''' Create all attributes '''

        self._kwargs = kwargs
        self.messagebox_called = False
        self._proccessed_progressbar = 0
        self._scoped_session = None

        self.flags = None
        self.current_selected_id = None
        self.selected_be_edited_id = None
        self.activated_thread = None
        self._new_page = None
        self._old_page = None

        self._new_stack_page = None
        self._old_stack_page = None

        self._navi_count = 0

        self._list_threads = []

        self._navigate_proxy_model = None

        self._navigate_standard_model = None

        self.standard_item_model()

        UI_PATH = QFile(":/ui_file/master_data.ui")

        self.load_ui_file(UI_PATH)

        self.init_ui()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_master_data = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.init_line_edit()

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.init_timer()

        self.change_page(index = self._kwargs.get('page_number', 0),
                         stack_widget = self.ui_master_data.stackedWidget,
                         load_data = self._kwargs.get('load_data', False),
                         column_index = 1)

        self.hide_widget(widget = self.ui_master_data.frame_progress_bar,
                         boolen_value = False)



        self.init_tree_view()
        self.init_navi_treeview()
        self.init_clicked_signal_push_buttons()
        self.init_textChanged_signal_line_edit()
        self.init_item_clicked_signal_tree_widget()
        self.init_selectionChanged_signal_treeview()
        self.init_returnPressed_signal_line_edit()
        self.init_text_editor()

        self.set_hide_column(
            list_tree_view_object = self.ui_master_data.stackedWidget.widget(int(self._kwargs.get('page_number', 0))).findChildren(
                      QTreeView),
            column = 0,
            bool_value = True)

        return

    def init_line_edit(self):
        '''
           SUMMARY
           =======
           This function is used to upload file.

           EXTENTED DISCRIPTION
           ====================
           In this custom QLineEdit there is a QPushButton on right site for clear the content.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the custom QLineEdit with QPushButton on right site")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.lineEdit_search_general_region = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_region.setObjectName("lineEdit_search_general_region")
        self.ui_master_data.lineEdit_search_general_region.setPlaceholderText("Search by region / place")
        self.ui_master_data.lineEdit_search_general_region.set_default_widget_signal.connect(lambda:
            self.filter_update(
              line_edit_widget = self.ui_master_data.lineEdit_search_general_region,
              label_found_counter = self.ui_master_data.label_count_found_general_region,
              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_region,
              set_default = True,
              column = 2))
        self.ui_master_data.horizontalLayout_80.addWidget(self.ui_master_data.lineEdit_search_general_region)

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.lineEdit_search_general_country = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_country.setObjectName("lineEdit_search_general_country")
        self.ui_master_data.lineEdit_search_general_country.setPlaceholderText("Search by country")
        self.ui_master_data.lineEdit_search_general_country.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_country,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_master_data_general_country,
                               column = 2))
        self.ui_master_data.horizontalLayout_21.addWidget(self.ui_master_data.lineEdit_search_general_country)

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.lineEdit_search_general_language = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_language.setObjectName("lineEdit_search_general_language")
        self.ui_master_data.lineEdit_search_general_language.setPlaceholderText("Search by language")
        self.ui_master_data.lineEdit_search_general_language.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_language,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_language,
                               column = 2))
        self.ui_master_data.horizontalLayout_35.addWidget(self.ui_master_data.lineEdit_search_general_language)

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.lineEdit_search_general_age_release = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_age_release.setObjectName("lineEdit_search_general_age_release")
        self.ui_master_data.lineEdit_search_general_age_release.setPlaceholderText("Search by age release")
        self.ui_master_data.lineEdit_search_general_age_release.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_age_release,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_age_release,
                               column = 2))
        self.ui_master_data.horizontalLayout_281.addWidget(self.ui_master_data.lineEdit_search_general_age_release)

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.lineEdit_search_person_nationality = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_nationality.setObjectName("lineEdit_search_person_nationality")
        self.ui_master_data.lineEdit_search_person_nationality.setPlaceholderText("Search by nationality")
        self.ui_master_data.lineEdit_search_person_nationality.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_nationality,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_nationality,
                               column = 2))
        self.ui_master_data.horizontalLayout_45.addWidget(self.ui_master_data.lineEdit_search_person_nationality)

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_search_film_genre = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_genre.setObjectName("lineEdit_search_film_genre")
        self.ui_master_data.lineEdit_search_film_genre.setPlaceholderText("Search by film-genre")
        self.ui_master_data.lineEdit_search_film_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_genre,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_genre,
                               column = 2))
        self.ui_master_data.horizontalLayout_3.addWidget(self.ui_master_data.lineEdit_search_film_genre)

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_search_general_award = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_award.setObjectName("lineEdit_search_general_award")
        self.ui_master_data.lineEdit_search_general_award.setPlaceholderText("Search by award")
        self.ui_master_data.lineEdit_search_general_award.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_award,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_award,
                               column = 2))
        self.ui_master_data.horizontalLayout_85.addWidget(self.ui_master_data.lineEdit_search_general_award)

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_search_music_genre = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_genre.setObjectName("lineEdit_search_music_genre")
        self.ui_master_data.lineEdit_search_music_genre.setPlaceholderText("Search by genre")
        self.ui_master_data.lineEdit_search_music_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_genre,
                               line_edit_widget = self.ui_master_data.lineEdit_search_music_genre,
                               column = 2))
        self.ui_master_data.horizontalLayout_102.addWidget(self.ui_master_data.lineEdit_search_music_genre)

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_search_person_gender = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_gender.setObjectName("lineEdit_search_person_gender")
        self.ui_master_data.lineEdit_search_person_gender.setPlaceholderText("Search by gender")
        self.ui_master_data.lineEdit_search_person_gender.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_gender,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_gender,
                               column = 2))
        self.ui_master_data.horizontalLayout_40.addWidget(self.ui_master_data.lineEdit_search_person_gender)

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_search_person_salutation = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_salutation.setObjectName("lineEdit_search_person_salutation")
        self.ui_master_data.lineEdit_search_person_salutation.setPlaceholderText("Search by salutation")
        self.ui_master_data.lineEdit_search_person_salutation.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_salutation,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_salutation,
                               column = 2))
        self.ui_master_data.horizontalLayout_50.addWidget(self.ui_master_data.lineEdit_search_person_salutation)

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_search_person_title = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_title.setObjectName("lineEdit_search_person_title")
        self.ui_master_data.lineEdit_search_person_title.setPlaceholderText("Search by title")
        self.ui_master_data.lineEdit_search_person_title.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_title,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_title,
                               column = 2))
        self.ui_master_data.horizontalLayout_55.addWidget(self.ui_master_data.lineEdit_search_person_title)

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_search_person_eye_color = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_eye_color.setObjectName("lineEdit_search_person_eye_color")
        self.ui_master_data.lineEdit_search_person_eye_color.setPlaceholderText("Search by eye color")
        self.ui_master_data.lineEdit_search_person_eye_color.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_eye_color,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_eye_color,
                               column = 2))
        self.ui_master_data.horizontalLayout_65.addWidget(self.ui_master_data.lineEdit_search_person_eye_color)

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_search_person_hair_color = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_hair_color.setObjectName("lineEdit_search_person_hair_color")
        self.ui_master_data.lineEdit_search_person_hair_color.setPlaceholderText("Search by hair color")
        self.ui_master_data.lineEdit_search_person_hair_color.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_hair_color,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_hair_color,
                               column = 2))
        self.ui_master_data.horizontalLayout_60.addWidget(self.ui_master_data.lineEdit_search_person_hair_color)

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_search_person_religion = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_religion.setObjectName("lineEdit_search_person_religion")
        self.ui_master_data.lineEdit_search_person_religion.setPlaceholderText("Search by religion")
        self.ui_master_data.lineEdit_search_person_religion.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_religion,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_religion,
                               column = 2))
        self.ui_master_data.horizontalLayout_70.addWidget(self.ui_master_data.lineEdit_search_person_religion)

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_search_person_relationship_status = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_relationship_status.setObjectName("lineEdit_search_person_relationship_status")
        self.ui_master_data.lineEdit_search_person_relationship_status.setPlaceholderText("Search by relationship status")
        self.ui_master_data.lineEdit_search_person_relationship_status.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship_status,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship_status,
                               column = 2))
        self.ui_master_data.horizontalLayout_75.addWidget(self.ui_master_data.lineEdit_search_person_relationship_status)

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_search_person_relationship = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_person_relationship.setObjectName("lineEdit_search_person_relationship")
        self.ui_master_data.lineEdit_search_person_relationship.setPlaceholderText("Search by relationship")
        self.ui_master_data.lineEdit_search_person_relationship.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship,
                               line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship,
                               column = 2))
        self.ui_master_data.horizontalLayout_92.addWidget(self.ui_master_data.lineEdit_search_person_relationship)

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_search_general_media_type = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_media_type.setObjectName("lineEdit_search_general_media_type")
        self.ui_master_data.lineEdit_search_general_media_type.setPlaceholderText("Search by media typ")
        self.ui_master_data.lineEdit_search_general_media_type.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_media_type,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_media_type,
                               column = 2))
        self.ui_master_data.horizontalLayout_108.addWidget(self.ui_master_data.lineEdit_search_general_media_type)

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_search_general_packaging = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_packaging.setObjectName("lineEdit_search_general_packaging")
        self.ui_master_data.lineEdit_search_general_packaging.setPlaceholderText("Search by packaging")
        self.ui_master_data.lineEdit_search_general_packaging.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_packaging,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_packaging,
                               column = 2))
        self.ui_master_data.horizontalLayout_113.addWidget(self.ui_master_data.lineEdit_search_general_packaging)

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.lineEdit_search_film_production_company = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_production_company.setObjectName("lineEdit_search_film_production_company")
        self.ui_master_data.lineEdit_search_film_production_company.setPlaceholderText("Search by film production company")
        self.ui_master_data.lineEdit_search_film_production_company.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_production_company,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_production_company,
                               column = 2))
        self.ui_master_data.horizontalLayout_123.addWidget(self.ui_master_data.lineEdit_search_film_production_company)

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.lineEdit_search_film_rentals = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_rentals.setObjectName("lineEdit_search_film_rentals")
        self.ui_master_data.lineEdit_search_film_rentals.setPlaceholderText("Search by film production company")
        self.ui_master_data.lineEdit_search_film_rentals.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_rentals,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_rentals,
                               column = 2))
        self.ui_master_data.horizontalLayout_127.addWidget(self.ui_master_data.lineEdit_search_film_rentals)

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.lineEdit_search_film_distribution = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_distribution.setObjectName("lineEdit_search_film_distribution")
        self.ui_master_data.lineEdit_search_film_distribution.setPlaceholderText("Search by film distribution")
        self.ui_master_data.lineEdit_search_film_distribution.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_distribution,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_distribution,
                               column = 2))
        self.ui_master_data.horizontalLayout_133.addWidget(self.ui_master_data.lineEdit_search_film_distribution)

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.lineEdit_search_film_studio = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_studio.setObjectName("lineEdit_search_film_studio")
        self.ui_master_data.lineEdit_search_film_studio.setPlaceholderText("Search by film studio")
        self.ui_master_data.lineEdit_search_film_studio.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_studio,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_studio,
                               column = 2))
        self.ui_master_data.horizontalLayout_139.addWidget(self.ui_master_data.lineEdit_search_film_studio)

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.lineEdit_search_film_resolution = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_resolution.setObjectName("lineEdit_search_film_resolution")
        self.ui_master_data.lineEdit_search_film_resolution.setPlaceholderText("Search by film resolution")
        self.ui_master_data.lineEdit_search_film_resolution.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_resolution,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_resolution,
                               column = 3))
        self.ui_master_data.horizontalLayout_145.addWidget(self.ui_master_data.lineEdit_search_film_resolution)

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.lineEdit_search_general_manufacturer = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_general_manufacturer.setObjectName("lineEdit_search_general_manufacturer")
        self.ui_master_data.lineEdit_search_general_manufacturer.setPlaceholderText("Search by general manufacturer")
        self.ui_master_data.lineEdit_search_general_manufacturer.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_manufacturer,
                               line_edit_widget = self.ui_master_data.lineEdit_search_general_manufacturer,
                               column = 2))
        self.ui_master_data.horizontalLayout_155.addWidget(self.ui_master_data.lineEdit_search_general_manufacturer)

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.lineEdit_search_film_regional_code = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_regional_code.setObjectName("lineEdit_search_film_regional_code")
        self.ui_master_data.lineEdit_search_film_regional_code.setPlaceholderText("Search by film regional code")
        self.ui_master_data.lineEdit_search_film_regional_code.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_regional_code,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_regional_code,
                               column = 2))
        self.ui_master_data.horizontalLayout_163.addWidget(self.ui_master_data.lineEdit_search_film_regional_code)

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.lineEdit_search_film_aspect_ratio = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_aspect_ratio.setObjectName("lineEdit_search_film_aspect_ratio")
        self.ui_master_data.lineEdit_search_film_aspect_ratio.setPlaceholderText("Search by film aspect ratio")
        self.ui_master_data.lineEdit_search_film_aspect_ratio.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_aspect_ratio,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_aspect_ratio,
                               column = 3))
        self.ui_master_data.horizontalLayout_172.addWidget(self.ui_master_data.lineEdit_search_film_aspect_ratio)

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.lineEdit_search_film_dimension = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_dimension.setObjectName("lineEdit_search_film_dimension")
        self.ui_master_data.lineEdit_search_film_dimension.setPlaceholderText("Search by film dimension")
        self.ui_master_data.lineEdit_search_film_dimension.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_dimension,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_dimension,
                               column = 2))
        self.ui_master_data.horizontalLayout_181.addWidget(self.ui_master_data.lineEdit_search_film_dimension)

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.lineEdit_search_film_edition = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_edition.setObjectName("lineEdit_search_film_edition")
        self.ui_master_data.lineEdit_search_film_edition.setPlaceholderText("Search by film edition")
        self.ui_master_data.lineEdit_search_film_edition.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_edition,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_edition,
                               column = 2))
        self.ui_master_data.horizontalLayout_187.addWidget(self.ui_master_data.lineEdit_search_film_edition)

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.lineEdit_search_film_version = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_version.setObjectName("lineEdit_search_film_version")
        self.ui_master_data.lineEdit_search_film_version.setPlaceholderText("Search by film version")
        self.ui_master_data.lineEdit_search_film_version.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_version,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_version,
                               column = 2))
        self.ui_master_data.horizontalLayout_194.addWidget(self.ui_master_data.lineEdit_search_film_version)

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.lineEdit_search_film_videonorm = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_videonorm.setObjectName("lineEdit_search_film_videonorm")
        self.ui_master_data.lineEdit_search_film_videonorm.setPlaceholderText("Search by film videonorm")
        self.ui_master_data.lineEdit_search_film_videonorm.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videonorm,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_videonorm,
                               column = 2))
        self.ui_master_data.horizontalLayout_201.addWidget(self.ui_master_data.lineEdit_search_film_videonorm)

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.lineEdit_search_film_audioformat = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_audioformat.setObjectName("lineEdit_search_film_audioformat")
        self.ui_master_data.lineEdit_search_film_audioformat.setPlaceholderText("Search by film audioformat")
        self.ui_master_data.lineEdit_search_film_audioformat.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audioformat,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_audioformat,
                               column = 2))
        self.ui_master_data.horizontalLayout_208.addWidget(self.ui_master_data.lineEdit_search_film_audioformat)

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.lineEdit_search_film_audio_channel = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_audio_channel.setObjectName("lineEdit_search_film_audio_channel")
        self.ui_master_data.lineEdit_search_film_audio_channel.setPlaceholderText("Search by film audio channel")
        self.ui_master_data.lineEdit_search_film_audio_channel.set_default_widget_signal.connect(lambda:
            self.filter_update(certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audio_channel,
                               line_edit_widget = self.ui_master_data.lineEdit_search_film_audio_channel,
                               column = 2))
        self.ui_master_data.horizontalLayout_215.addWidget(self.ui_master_data.lineEdit_search_film_audio_channel)

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.lineEdit_search_film_colour_format = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_colour_format.setObjectName("lineEdit_search_film_colour_format")
        self.ui_master_data.lineEdit_search_film_colour_format.setPlaceholderText("Search by film colour format")
        self.ui_master_data.lineEdit_search_film_colour_format.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_colour_format,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_colour_format,
                               column = 2))
        self.ui_master_data.horizontalLayout_222.addWidget(self.ui_master_data.lineEdit_search_film_colour_format)

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.lineEdit_search_film_videoformat = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_videoformat.setObjectName("lineEdit_search_film_videoformat")
        self.ui_master_data.lineEdit_search_film_videoformat.setPlaceholderText("Search by film video format")
        self.ui_master_data.lineEdit_search_film_videoformat.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_videoformat,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videoformat,
                               column = 2))
        self.ui_master_data.horizontalLayout_230.addWidget(self.ui_master_data.lineEdit_search_film_videoformat)

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.lineEdit_search_film_soundsystem = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_film_soundsystem.setObjectName("lineEdit_search_film_soundsystem")
        self.ui_master_data.lineEdit_search_film_soundsystem.setPlaceholderText("Search by film sound system")
        self.ui_master_data.lineEdit_search_film_soundsystem.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_soundsystem,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_soundsystem,
                               column = 2))
        self.ui_master_data.horizontalLayout_239.addWidget(self.ui_master_data.lineEdit_search_film_soundsystem)

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.lineEdit_search_music_studio = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_studio.setObjectName("lineEdit_search_music_studio")
        self.ui_master_data.lineEdit_search_music_studio.setPlaceholderText("Search by music studio")
        self.ui_master_data.lineEdit_search_music_studio.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_studio,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_studio,
                               column = 2))
        self.ui_master_data.horizontalLayout_245.addWidget(self.ui_master_data.lineEdit_search_music_studio)

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.lineEdit_search_music_spars_code = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_spars_code.setObjectName("lineEdit_search_music_spars_code")
        self.ui_master_data.lineEdit_search_music_spars_code.setPlaceholderText("Search by music SPARS code")
        self.ui_master_data.lineEdit_search_music_spars_code.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_spars_code,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_spars_code,
                               column = 2))
        self.ui_master_data.horizontalLayout_251.addWidget(self.ui_master_data.lineEdit_search_music_spars_code)

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.lineEdit_search_music_publisher = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_publisher.setObjectName("lineEdit_search_music_publisher")
        self.ui_master_data.lineEdit_search_music_publisher.setPlaceholderText("Search by music publisher")
        self.ui_master_data.lineEdit_search_music_publisher.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_publisher,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_publisher,
                               column = 2))
        self.ui_master_data.horizontalLayout_257.addWidget(self.ui_master_data.lineEdit_search_music_publisher)

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.lineEdit_search_music_style = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_style.setObjectName("lineEdit_search_music_style")
        self.ui_master_data.lineEdit_search_music_style.setPlaceholderText("Search by music style")
        self.ui_master_data.lineEdit_search_music_style.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_style,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_style,
                               column = 2))
        self.ui_master_data.horizontalLayout_263.addWidget(self.ui_master_data.lineEdit_search_music_style)

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.lineEdit_search_music_label = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_label.setObjectName("lineEdit_search_music_label")
        self.ui_master_data.lineEdit_search_music_label.setPlaceholderText("Search by music label")
        self.ui_master_data.lineEdit_search_music_label.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_label,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_label,
                               column = 2))
        self.ui_master_data.horizontalLayout_269.addWidget(self.ui_master_data.lineEdit_search_music_label)

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.lineEdit_search_music_distribution = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_music_distribution.setObjectName("lineEdit_search_music_distribution")
        self.ui_master_data.lineEdit_search_music_distribution.setPlaceholderText("Search by music distribution")
        self.ui_master_data.lineEdit_search_music_distribution.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_distribution,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_distribution,
                               column = 2))
        self.ui_master_data.horizontalLayout_275.addWidget(self.ui_master_data.lineEdit_search_music_distribution)

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.lineEdit_search_book_genre = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_book_genre.setObjectName("lineEdit_search_book_genre")
        self.ui_master_data.lineEdit_search_book_genre.setPlaceholderText("Search by book genre")
        self.ui_master_data.lineEdit_search_book_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_book_genre,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_genre,
                               column = 2))
        self.ui_master_data.horizontalLayout_287.addWidget(self.ui_master_data.lineEdit_search_book_genre)

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.lineEdit_search_book_publisher = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_book_publisher.setObjectName("lineEdit_search_book_publisher")
        self.ui_master_data.lineEdit_search_book_publisher.setPlaceholderText("Search by book publisher")
        self.ui_master_data.lineEdit_search_book_publisher.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_book_publisher,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_publisher,
                               column = 2))
        self.ui_master_data.horizontalLayout_293.addWidget(self.ui_master_data.lineEdit_search_book_publisher)

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.lineEdit_search_book_binding = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_book_binding.setObjectName("lineEdit_search_book_binding")
        self.ui_master_data.lineEdit_search_book_binding.setPlaceholderText("Search by book binding")
        self.ui_master_data.lineEdit_search_book_binding.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_book_binding,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_binding,
                               column = 2))
        self.ui_master_data.horizontalLayout_299.addWidget(self.ui_master_data.lineEdit_search_book_binding)

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.lineEdit_search_coin_alloy = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_alloy.setObjectName("lineEdit_search_coin_alloy")
        self.ui_master_data.lineEdit_search_coin_alloy.setPlaceholderText("Search by coin alloy")
        self.ui_master_data.lineEdit_search_coin_alloy.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_alloy,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_alloy,
                               column = 2))
        self.ui_master_data.horizontalLayout_305.addWidget(self.ui_master_data.lineEdit_search_coin_alloy)

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.lineEdit_search_coin_currency = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_currency.setObjectName("lineEdit_search_coin_currency")
        self.ui_master_data.lineEdit_search_coin_currency.setPlaceholderText("Search by coin currency")
        self.ui_master_data.lineEdit_search_coin_currency.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_currency,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_currency,
                               column = 2))
        self.ui_master_data.horizontalLayout_311.addWidget(self.ui_master_data.lineEdit_search_coin_currency)

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.lineEdit_search_coin_type = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_type.setObjectName("lineEdit_search_coin_type")
        self.ui_master_data.lineEdit_search_coin_type.setPlaceholderText("Search by coin type")
        self.ui_master_data.lineEdit_search_coin_type.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_type,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_type,
                               column = 2))
        self.ui_master_data.horizontalLayout_317.addWidget(self.ui_master_data.lineEdit_search_coin_type)

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.lineEdit_search_coin_manufacturing_procedure = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_manufacturing_procedure.setObjectName("lineEdit_search_coin_manufacturing_procedure")
        self.ui_master_data.lineEdit_search_coin_manufacturing_procedure.setPlaceholderText("Search by manufacturing procedure")
        self.ui_master_data.lineEdit_search_coin_manufacturing_procedure.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_manufacturing_procedure,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_manufacturing_procedure,
                               column = 3))
        self.ui_master_data.horizontalLayout_331.addWidget(self.ui_master_data.lineEdit_search_coin_manufacturing_procedure)

        ###################################################
        #################  Coin Degree of preservation  ###
        ###################################################
        self.ui_master_data.lineEdit_search_coin_degree_preservation = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_degree_preservation.setObjectName("lineEdit_search_coin_degree_preservation")
        self.ui_master_data.lineEdit_search_coin_degree_preservation.setPlaceholderText("Search by degree of preservation")
        self.ui_master_data.lineEdit_search_coin_degree_preservation.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_degree_preservation,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_degree_preservation,
                               column = 3))
        self.ui_master_data.horizontalLayout_341.addWidget(self.ui_master_data.lineEdit_search_coin_degree_preservation)

        ###################################################
        #################  Coin Min  ######################
        ###################################################
        self.ui_master_data.lineEdit_search_coin_mint = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_coin_mint.setObjectName("lineEdit_search_coin_mint")
        self.ui_master_data.lineEdit_search_coin_mint.setPlaceholderText("Search by coin mint")
        self.ui_master_data.lineEdit_search_coin_mint.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_coin_mint,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_mint,
                               column = 3))
        self.ui_master_data.horizontalLayout_351.addWidget(self.ui_master_data.lineEdit_search_coin_mint)

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.lineEdit_search_videogame_genre = ButtonLineEdit(image_path_collection = self._kwargs.get('image_path_collection'))
        self.ui_master_data.lineEdit_search_videogame_genre.setObjectName("lineEdit_search_videogame_genre")
        self.ui_master_data.lineEdit_search_videogame_genre.setPlaceholderText("Search by videogame genre")
        self.ui_master_data.lineEdit_search_videogame_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_videogame_genre,
                               certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_videogame_genre,
                               column = 2))
        self.ui_master_data.horizontalLayout_323.addWidget(self.ui_master_data.lineEdit_search_videogame_genre)

        return

    def init_timer(self):
        '''
           NOTICE
           ======
           This method initializes all QTimer.

           RETURNS
           =======

           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize all tiers")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.search_delay_timer_general_region = QTimer()
        self.search_delay_timer_general_region.setSingleShot(True)
        self.search_delay_timer_general_region.setInterval(1000)
        self.search_delay_timer_general_region.timeout.connect(lambda:
            self.filter_update(
              line_edit_widget = self.ui_master_data.lineEdit_search_general_region,
              label_found_counter = self.ui_master_data.label_count_found_general_region,
              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_region,
              column = 2))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.search_delay_timer_general_country = QTimer()
        self.search_delay_timer_general_country.setSingleShot(True)
        self.search_delay_timer_general_country.setInterval(1000)
        self.search_delay_timer_general_country.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_country,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_master_data_general_country,
                column = 2))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.search_delay_timer_general_language = QTimer()
        self.search_delay_timer_general_language.setSingleShot(True)
        self.search_delay_timer_general_language.setInterval(1000)
        self.search_delay_timer_general_language.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_language,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_language,
                column = 2))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.search_delay_timer_general_age_release = QTimer()
        self.search_delay_timer_general_age_release.setSingleShot(True)
        self.search_delay_timer_general_age_release.setInterval(1000)
        self.search_delay_timer_general_age_release.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_age_release,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_age_release,
                column = 2))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.search_delay_timer_person_nationality = QTimer()
        self.search_delay_timer_person_nationality.setSingleShot(True)
        self.search_delay_timer_person_nationality.setInterval(1000)
        self.search_delay_timer_person_nationality.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_nationality,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_nationality,
                column = 2))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.search_delay_timer_film_genre = QTimer()
        self.search_delay_timer_film_genre.setSingleShot(True)
        self.search_delay_timer_film_genre.setInterval(1000)
        self.search_delay_timer_film_genre.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_genre,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_genre,
                column = 2))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.search_delay_timer_general_award = QTimer()
        self.search_delay_timer_general_award.setSingleShot(True)
        self.search_delay_timer_general_award.setInterval(1000)
        self.search_delay_timer_general_award.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_award,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_award,
                column = 2))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.search_delay_timer_music_genre = QTimer()
        self.search_delay_timer_music_genre.setSingleShot(True)
        self.search_delay_timer_music_genre.setInterval(1000)
        self.search_delay_timer_music_genre.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_genre,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_genre,
                column = 2))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.search_delay_timer_person_gender = QTimer()
        self.search_delay_timer_person_gender.setSingleShot(True)
        self.search_delay_timer_person_gender.setInterval(1000)
        self.search_delay_timer_person_gender.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_gender,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_gender,
                column = 2))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.search_delay_timer_person_salutation = QTimer()
        self.search_delay_timer_person_salutation.setSingleShot(True)
        self.search_delay_timer_person_salutation.setInterval(1000)
        self.search_delay_timer_person_salutation.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_salutation,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_salutation,
                column = 2))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.search_delay_timer_person_title = QTimer()
        self.search_delay_timer_person_title.setSingleShot(True)
        self.search_delay_timer_person_title.setInterval(1000)
        self.search_delay_timer_person_title.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_title,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_title,
                column = 2))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.search_delay_timer_person_eye_color = QTimer()
        self.search_delay_timer_person_eye_color.setSingleShot(True)
        self.search_delay_timer_person_eye_color.setInterval(1000)
        self.search_delay_timer_person_eye_color.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_eye_color,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_eye_color,
                column = 2))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.search_delay_timer_person_hair_color = QTimer()
        self.search_delay_timer_person_hair_color.setSingleShot(True)
        self.search_delay_timer_person_hair_color.setInterval(1000)
        self.search_delay_timer_person_hair_color.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_hair_color,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_hair_color,
                column = 2))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.search_delay_timer_person_religion = QTimer()
        self.search_delay_timer_person_religion.setSingleShot(True)
        self.search_delay_timer_person_religion.setInterval(1000)
        self.search_delay_timer_person_religion.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_religion,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_religion,
                column = 2))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.search_delay_timer_person_relationship_status = QTimer()
        self.search_delay_timer_person_relationship_status.setSingleShot(True)
        self.search_delay_timer_person_relationship_status.setInterval(1000)
        self.search_delay_timer_person_relationship_status.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship_status,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship_status,
                column = 2))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.search_delay_timer_person_relationship = QTimer()
        self.search_delay_timer_person_relationship.setSingleShot(True)
        self.search_delay_timer_person_relationship.setInterval(1000)
        self.search_delay_timer_person_relationship.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship,
                column = 2))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.search_delay_timer_general_media_type = QTimer()
        self.search_delay_timer_general_media_type.setSingleShot(True)
        self.search_delay_timer_general_media_type.setInterval(1000)
        self.search_delay_timer_general_media_type.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_media_type,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_media_type,
                column = 2))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.search_delay_timer_general_packaging = QTimer()
        self.search_delay_timer_general_packaging.setSingleShot(True)
        self.search_delay_timer_general_packaging.setInterval(1000)
        self.search_delay_timer_general_packaging.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_packaging,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_packaging,
                column = 2))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.search_delay_timer_film_production_company = QTimer()
        self.search_delay_timer_film_production_company.setSingleShot(True)
        self.search_delay_timer_film_production_company.setInterval(1000)
        self.search_delay_timer_film_production_company.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_production_company,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_production_company,
                column = 2))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.search_delay_timer_film_rentals = QTimer()
        self.search_delay_timer_film_rentals.setSingleShot(True)
        self.search_delay_timer_film_rentals.setInterval(1000)
        self.search_delay_timer_film_rentals.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_rentals,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_rentals,
                column = 2))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.search_delay_timer_film_distribution = QTimer()
        self.search_delay_timer_film_distribution.setSingleShot(True)
        self.search_delay_timer_film_distribution.setInterval(1000)
        self.search_delay_timer_film_distribution.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_distribution,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_distribution,
                column = 2))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.search_delay_timer_film_studio = QTimer()
        self.search_delay_timer_film_studio.setSingleShot(True)
        self.search_delay_timer_film_studio.setInterval(1000)
        self.search_delay_timer_film_studio.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_studio,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_studio,
                column = 2))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.search_delay_timer_film_resolution = QTimer()
        self.search_delay_timer_film_resolution.setSingleShot(True)
        self.search_delay_timer_film_resolution.setInterval(1000)
        self.search_delay_timer_film_resolution.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_resolution,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_resolution,
                column = 3))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.search_delay_timer_general_manufacturer = QTimer()
        self.search_delay_timer_general_manufacturer.setSingleShot(True)
        self.search_delay_timer_general_manufacturer.setInterval(1000)
        self.search_delay_timer_general_manufacturer.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_manufacturer,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_manufacturer,
                column = 2))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.search_delay_timer_film_regional_code = QTimer()
        self.search_delay_timer_film_regional_code.setSingleShot(True)
        self.search_delay_timer_film_regional_code.setInterval(1000)
        self.search_delay_timer_film_regional_code.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_regional_code,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_regional_code,
                column = 3))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.search_delay_timer_film_aspect_ratio = QTimer()
        self.search_delay_timer_film_aspect_ratio.setSingleShot(True)
        self.search_delay_timer_film_aspect_ratio.setInterval(1000)
        self.search_delay_timer_film_aspect_ratio.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_aspect_ratio,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_aspect_ratio,
                column = 3))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.search_delay_timer_film_dimension = QTimer()
        self.search_delay_timer_film_dimension.setSingleShot(True)
        self.search_delay_timer_film_dimension.setInterval(1000)
        self.search_delay_timer_film_dimension.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_dimension,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_dimension,
                column = 2))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.search_delay_timer_film_edition = QTimer()
        self.search_delay_timer_film_edition.setSingleShot(True)
        self.search_delay_timer_film_edition.setInterval(1000)
        self.search_delay_timer_film_edition.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_edition,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_edition,
                column = 2))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.search_delay_timer_film_version = QTimer()
        self.search_delay_timer_film_version.setSingleShot(True)
        self.search_delay_timer_film_version.setInterval(1000)
        self.search_delay_timer_film_version.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_version,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_version,
                column = 2))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.search_delay_timer_film_videonorm = QTimer()
        self.search_delay_timer_film_videonorm.setSingleShot(True)
        self.search_delay_timer_film_videonorm.setInterval(1000)
        self.search_delay_timer_film_videonorm.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_videonorm,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videonorm,
                column = 2))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.search_delay_timer_film_audioformat = QTimer()
        self.search_delay_timer_film_audioformat.setSingleShot(True)
        self.search_delay_timer_film_audioformat.setInterval(1000)
        self.search_delay_timer_film_audioformat.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_audioformat,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audioformat,
                column = 2))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.search_delay_timer_film_audio_channel = QTimer()
        self.search_delay_timer_film_audio_channel.setSingleShot(True)
        self.search_delay_timer_film_audio_channel.setInterval(1000)
        self.search_delay_timer_film_audio_channel.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_audio_channel,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audio_channel,
                column = 2))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.search_delay_timer_film_colour_format = QTimer()
        self.search_delay_timer_film_colour_format.setSingleShot(True)
        self.search_delay_timer_film_colour_format.setInterval(1000)
        self.search_delay_timer_film_colour_format.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_colour_format,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_colour_format,
                column = 2))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.search_delay_timer_film_videoformat = QTimer()
        self.search_delay_timer_film_videoformat.setSingleShot(True)
        self.search_delay_timer_film_videoformat.setInterval(1000)
        self.search_delay_timer_film_videoformat.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_videoformat,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videoformat,
                column = 2))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.search_delay_timer_film_soundsystem = QTimer()
        self.search_delay_timer_film_soundsystem.setSingleShot(True)
        self.search_delay_timer_film_soundsystem.setInterval(1000)
        self.search_delay_timer_film_soundsystem.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_soundsystem,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_soundsystem,
                column = 2))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.search_delay_timer_music_studio = QTimer()
        self.search_delay_timer_music_studio.setSingleShot(True)
        self.search_delay_timer_music_studio.setInterval(1000)
        self.search_delay_timer_music_studio.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_studio,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_studio,
                column = 2))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.search_delay_timer_music_spars_code = QTimer()
        self.search_delay_timer_music_spars_code.setSingleShot(True)
        self.search_delay_timer_music_spars_code.setInterval(1000)
        self.search_delay_timer_music_spars_code.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_spars_code,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_spars_code,
                column = 2))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.search_delay_timer_music_publisher = QTimer()
        self.search_delay_timer_music_publisher.setSingleShot(True)
        self.search_delay_timer_music_publisher.setInterval(1000)
        self.search_delay_timer_music_publisher.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_publisher,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_publisher,
                column = 2))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.search_delay_timer_music_style = QTimer()
        self.search_delay_timer_music_style.setSingleShot(True)
        self.search_delay_timer_music_style.setInterval(1000)
        self.search_delay_timer_music_style.timeout.connect(lambda:
            self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_style,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_style,
                column = 2))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.search_delay_timer_music_label = QTimer()
        self.search_delay_timer_music_label.setSingleShot(True)
        self.search_delay_timer_music_label.setInterval(1000)
        self.search_delay_timer_music_label.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_music_label,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_label,
                column = 2))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.search_delay_timer_music_distribution = QTimer()
        self.search_delay_timer_music_distribution.setSingleShot(True)
        self.search_delay_timer_music_distribution.setInterval(1000)
        self.search_delay_timer_music_distribution.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_music_distribution,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_distribution,
                column = 2))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.search_delay_timer_book_genre = QTimer()
        self.search_delay_timer_book_genre.setSingleShot(True)
        self.search_delay_timer_book_genre.setInterval(1000)
        self.search_delay_timer_book_genre.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_book_genre,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_genre,
                column = 2))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.search_delay_timer_book_publisher = QTimer()
        self.search_delay_timer_book_publisher.setSingleShot(True)
        self.search_delay_timer_book_publisher.setInterval(1000)
        self.search_delay_timer_book_publisher.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_book_publisher,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_publisher,
                column = 2))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.search_delay_timer_book_binding = QTimer()
        self.search_delay_timer_book_binding.setSingleShot(True)
        self.search_delay_timer_book_binding.setInterval(1000)
        self.search_delay_timer_book_binding.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_book_binding,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_binding,
                column = 2))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.search_delay_timer_coin_alloy = QTimer()
        self.search_delay_timer_coin_alloy.setSingleShot(True)
        self.search_delay_timer_coin_alloy.setInterval(1000)
        self.search_delay_timer_coin_alloy.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_alloy,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_alloy,
                column = 2))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.search_delay_timer_coin_currency = QTimer()
        self.search_delay_timer_coin_currency.setSingleShot(True)
        self.search_delay_timer_coin_currency.setInterval(1000)
        self.search_delay_timer_coin_currency.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_currency,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_currency,
                column = 2))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.search_delay_timer_coin_type = QTimer()
        self.search_delay_timer_coin_type.setSingleShot(True)
        self.search_delay_timer_coin_type.setInterval(1000)
        self.search_delay_timer_coin_type.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_type,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_type,
                column = 2))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.search_delay_timer_coin_manufacturing_procedure = QTimer()
        self.search_delay_timer_coin_manufacturing_procedure.setSingleShot(True)
        self.search_delay_timer_coin_manufacturing_procedure.setInterval(1000)
        self.search_delay_timer_coin_manufacturing_procedure.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_manufacturing_procedure,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_manufacturing_procedure,
                column = 3))

        ###################################################
        #################  Coin Degree of preservation  ###
        ###################################################
        self.search_delay_timer_coin_degree_preservation = QTimer()
        self.search_delay_timer_coin_degree_preservation.setSingleShot(True)
        self.search_delay_timer_coin_degree_preservation.setInterval(1000)
        self.search_delay_timer_coin_degree_preservation.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_degree_preservation,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_degree_preservation,
                column = 3))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.search_delay_timer_coin_mint = QTimer()
        self.search_delay_timer_coin_mint.setSingleShot(True)
        self.search_delay_timer_coin_mint.setInterval(1000)
        self.search_delay_timer_coin_mint.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_coin_mint,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_mint,
                column = 3))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.search_delay_timer_videogame_genre = QTimer()
        self.search_delay_timer_videogame_genre.setSingleShot(True)
        self.search_delay_timer_videogame_genre.setInterval(1000)
        self.search_delay_timer_videogame_genre.timeout.connect(lambda:
            self.filter_update(
                line_edit_widget = self.ui_master_data.lineEdit_search_videogame_genre,
                certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_videogame_genre,
                column = 2))

        return

    def save_scoped_session(self,
                            scoped_session):
        print ""
        print "MasterData_Window save_scoped_session is called successfully"
        print "MasterData_Window in save_scoped_session / scoped_session: -> \n {}".format(scoped_session)
        self._scoped_session = scoped_session

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_question_msg(self,
                                      text,
                                      title,
                                      icon ,
                                      detail_text,
                                      set_flag = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for question.

           PARAMETERS
           ==========
           :title (str):        We except a text for the title bar of the message box.

           :text (str) :        We need the text for the main message to be displayed.

           :detail_text (str):  When we get a detailed text, the hide/show details" button will be added.

           :icon (str):         We need a icon path to display that on message box.

           RETURNS
           =======

           :return (bool):      When the user clicks on yes-button, the method returns True, otherwise False.

        '''
        logger.info("Create a message box for question")

        mBox = custom_message_box(self, text = text, title = title, icon = icon, set_flag = set_flag)
        mBox.setObjectName("generally_question_msg")
        setIc_OK = QIcon(self._kwargs.get('image_path_collection').check_icon_default)
        setIc_No = QIcon(self._kwargs.get('image_path_collection').no_default)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_text)

        mBox.addButton(QPushButton(setIc_OK, self.tr("Yes")),
                    mBox.YesRole)  # An "OK" button defined with the AcceptRole

        mBox.addButton(
                    QPushButton(setIc_No, self.tr("No")),
                    mBox.NoRole)  # An "OK" button defined with the AcceptRole

        clicked_result = mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        if clicked_result == 0:

            return True

        elif clicked_result == 1:

            return False

    def create_generally_information_msg(self,
                                         title,
                                         msg ,
                                         detail_msg ,
                                         icon,
                                         line_edit_object_get_focus,
                                         set_flag = False,
                                         yes_no_button = False,
                                         only_yes_button = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (str):                            We except a text for the title bar of the message box.

           :msg (str) :                             We need the text for the main message to be displayed.

           :detail_msg (str):                       When we get a detailed text, the hide/show details" button will be added.

           :icon (str):                             We need a icon path to display that on message box.

           :line_edit_object_get_focus (QLineEdit): By closing the message box the given QLineEdit() get focus in.

           :yes_no_button (bool):                   By default its False. We don't show yes and no button, otherwise we show both.

           :only_yes_button (bool):                 By defaul, its False, we don't show onlly yes button, otherwise we show it.

        '''
        logger.info("Create a message box for information")

        if not self.messagebox_called:

            if yes_no_button:

                yes_no_msg_box = custom_message_box(self, text = msg,
                                                          title = title,
                                                          icon = icon,
                                                          set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_no_msg_box.setInformativeText(detail_msg)

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Yes")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("No")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.

                # 1 means No
                if result_button == 1:
                    self.close()
                # 0 means Yes
                elif result_button == 0:
                    self._image_path = None
                    self.pixmap = None
                    self.ui_person_profile.label_photo.clear()
                    self.set_all_widget_on_default()
                    self.quite_threads()

            if only_yes_button:

                yes_msg_box = custom_message_box(self,
                                                  text = msg,
                                                  title = title,
                                                  icon = icon,
                                                  set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_msg_box.setInformativeText(detail_msg)

                yes_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Ok")),
                    #QPushButton(self.tr(" No")),
                    yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
                yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

                self.set_focus(widget_object = line_edit_object_get_focus)

    def init_text_editor(self):
        '''
           SUMMARY
           =======
           This method sets style sheet on QTextEdit.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.

        '''
        logger.info("Set style sheet on QTextEdit")

        appStyle="""
                QTextEdit
                {
                    color: black;
                }
                """
        self.ui_master_data.textEdit_notify_general_region.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_country.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_language.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_age_release.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_person_nationality.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_person_relationship.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_genre.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_award.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_genre.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_packaging.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_production_company.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_media_type.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_rentals.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_distribution.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_studio.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_resolution.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_manufacturer.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_regional_code.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_aspect_ratio.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_dimension.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_edition.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_version.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_videonorm.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_audioformat.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_audio_channel.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_colour_format.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_videoformat.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_soundsystem.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_studio.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_spars_code.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_publisher.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_style.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_label.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_distribution.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_book_genre.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_book_publisher.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_book_binding.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_alloy.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_currency.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_type.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_manufacturing_procedure.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_degree_preservation.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_coin_mint.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_videogame_genre.setStyleSheet(appStyle)

        return

    def init_returnPressed_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize returnPressed-Signal for QLineEdit()")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.lineEdit_general_region.returnPressed.connect(self.ui_master_data.pushButton_save_general_region.click)
        self.ui_master_data.lineEdit_edit_general_region.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_region.click)
        self.ui_master_data.lineEdit_search_general_region.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_region,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.lineEdit_general_country.returnPressed.connect(self.ui_master_data.pushButton_save_general_country.click)
        self.ui_master_data.lineEdit_edit_general_country.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_country.click)
        self.ui_master_data.lineEdit_search_general_country.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_country,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.lineEdit_general_language.returnPressed.connect(self.ui_master_data.pushButton_save_general_language.click)
        self.ui_master_data.lineEdit_edit_general_language.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_language.click)
        self.ui_master_data.lineEdit_search_general_language.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_language,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.lineEdit_general_age_release.returnPressed.connect(self.ui_master_data.pushButton_save_general_age_release.click)
        self.ui_master_data.lineEdit_edit_general_age_release.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_age_release.click)
        self.ui_master_data.lineEdit_search_general_age_release.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_general_age_release,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.lineEdit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_save_person_nationality.click)
        self.ui_master_data.lineEdit_edit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_nationality.click)
        self.ui_master_data.lineEdit_search_person_nationality.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_nationality,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_save_film_genre.click)
        self.ui_master_data.lineEdit_edit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_genre.click)
        self.ui_master_data.lineEdit_search_film_genre.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_genre,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_general_award.returnPressed.connect(self.ui_master_data.pushButton_save_general_award.click)
        self.ui_master_data.lineEdit_edit_general_award.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_award.click)
        self.ui_master_data.lineEdit_search_general_award.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_award,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_music_genre.returnPressed.connect(self.ui_master_data.pushButton_save_music_genre.click)
        self.ui_master_data.lineEdit_edit_music_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_genre.click)
        self.ui_master_data.lineEdit_search_music_genre.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_genre,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_save_person_gender.click)
        self.ui_master_data.lineEdit_edit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_gender.click)
        self.ui_master_data.lineEdit_search_person_gender.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_gender,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_save_person_salutation.click)
        self.ui_master_data.lineEdit_edit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_salutation.click)
        self.ui_master_data.lineEdit_search_person_salutation.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_salutation,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_person_title.returnPressed.connect(self.ui_master_data.pushButton_save_person_title.click)
        self.ui_master_data.lineEdit_edit_person_title.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_title.click)
        self.ui_master_data.lineEdit_search_person_title.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_title,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_eye_color.click)
        self.ui_master_data.lineEdit_edit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_eye_color.click)
        self.ui_master_data.lineEdit_search_person_eye_color.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_eye_color,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_hair_color.click)
        self.ui_master_data.lineEdit_edit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_hair_color.click)
        self.ui_master_data.lineEdit_search_person_hair_color.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_hair_color,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_save_person_religion.click)
        self.ui_master_data.lineEdit_edit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_religion.click)
        self.ui_master_data.lineEdit_search_person_religion.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_religion,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship_status.click)
        self.ui_master_data.lineEdit_edit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship_status.click)
        self.ui_master_data.lineEdit_search_person_relationship_status.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_relationship_status,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship.click)
        self.ui_master_data.lineEdit_edit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship.click)
        self.ui_master_data.lineEdit_search_person_relationship.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_relationship,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_general_media_type.returnPressed.connect(self.ui_master_data.pushButton_save_general_media_type.click)
        self.ui_master_data.lineEdit_edit_general_media_type.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_media_type.click)
        self.ui_master_data.lineEdit_search_general_media_type.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_media_type,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_general_packaging.returnPressed.connect(self.ui_master_data.pushButton_save_general_packaging.click)
        self.ui_master_data.lineEdit_edit_general_packaging.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_packaging.click)
        self.ui_master_data.lineEdit_search_general_packaging.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_packaging,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.lineEdit_film_production_company.returnPressed.connect(self.ui_master_data.pushButton_save_film_production_company.click)
        self.ui_master_data.lineEdit_edit_film_production_company.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_production_company.click)
        self.ui_master_data.lineEdit_search_film_production_company.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_production_company,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.lineEdit_film_rentals.returnPressed.connect(self.ui_master_data.pushButton_save_film_rentals.click)
        self.ui_master_data.lineEdit_edit_film_rentals.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_rentals.click)
        self.ui_master_data.lineEdit_search_film_rentals.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_rentals,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.lineEdit_film_distribution.returnPressed.connect(self.ui_master_data.pushButton_save_film_distribution.click)
        self.ui_master_data.lineEdit_edit_film_distribution.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_distribution.click)
        self.ui_master_data.lineEdit_search_film_distribution.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_distribution,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.lineEdit_film_studio.returnPressed.connect(self.ui_master_data.pushButton_save_film_studio.click)
        self.ui_master_data.lineEdit_edit_film_studio.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_studio.click)
        self.ui_master_data.lineEdit_search_film_studio.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_studio,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.lineEdit_film_resolution.returnPressed.connect(self.ui_master_data.pushButton_save_film_resolution.click)
        self.ui_master_data.lineEdit_edit_film_resolution.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_resolution.click)
        self.ui_master_data.lineEdit_film_presenting_format.returnPressed.connect(self.ui_master_data.pushButton_save_film_resolution.click)
        self.ui_master_data.lineEdit_edit_film_presenting_format.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_resolution.click)
        self.ui_master_data.lineEdit_search_film_resolution.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_resolution,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.lineEdit_general_manufacturer.returnPressed.connect(self.ui_master_data.pushButton_save_general_manufacturer.click)
        self.ui_master_data.lineEdit_edit_general_manufacturer.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_manufacturer.click)
        self.ui_master_data.lineEdit_search_general_manufacturer.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_manufacturer,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.lineEdit_film_regional_code.returnPressed.connect(self.ui_master_data.pushButton_save_film_regional_code.click)
        self.ui_master_data.lineEdit_edit_film_regional_code.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_regional_code.click)
        self.ui_master_data.lineEdit_film_region.returnPressed.connect(self.ui_master_data.pushButton_save_film_regional_code.click)
        self.ui_master_data.lineEdit_edit_film_region.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_regional_code.click)
        self.ui_master_data.lineEdit_search_film_regional_code.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_regional_code,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.lineEdit_film_aspect_ratio.returnPressed.connect(self.ui_master_data.pushButton_save_film_aspect_ratio.click)
        self.ui_master_data.lineEdit_edit_film_aspect_ratio.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_aspect_ratio.click)
        self.ui_master_data.lineEdit_film_aspect_ratio_format.returnPressed.connect(self.ui_master_data.pushButton_save_film_aspect_ratio.click)
        self.ui_master_data.lineEdit_edit_film_aspect_ratio_format.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_aspect_ratio.click)
        self.ui_master_data.lineEdit_search_film_aspect_ratio.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_aspect_ratio,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.lineEdit_film_dimension.returnPressed.connect(self.ui_master_data.pushButton_save_film_dimension.click)
        self.ui_master_data.lineEdit_edit_film_dimension.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_dimension.click)
        self.ui_master_data.lineEdit_search_film_dimension.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_dimension,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.lineEdit_film_edition.returnPressed.connect(self.ui_master_data.pushButton_save_film_edition.click)
        self.ui_master_data.lineEdit_edit_film_edition.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_edition.click)
        self.ui_master_data.lineEdit_search_film_edition.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_edition,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.lineEdit_film_version.returnPressed.connect(self.ui_master_data.pushButton_save_film_version.click)
        self.ui_master_data.lineEdit_edit_film_version.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_version.click)
        self.ui_master_data.lineEdit_search_film_version.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_version,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.lineEdit_film_videonorm.returnPressed.connect(self.ui_master_data.pushButton_save_film_videonorm.click)
        self.ui_master_data.lineEdit_edit_film_videonorm.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_videonorm.click)
        self.ui_master_data.lineEdit_search_film_videonorm.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_videonorm,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.lineEdit_film_audioformat.returnPressed.connect(self.ui_master_data.pushButton_save_film_audioformat.click)
        self.ui_master_data.lineEdit_edit_film_audioformat.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_audioformat.click)
        self.ui_master_data.lineEdit_search_film_audioformat.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_audioformat,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.lineEdit_film_audio_channel.returnPressed.connect(self.ui_master_data.pushButton_save_film_audio_channel.click)
        self.ui_master_data.lineEdit_edit_film_audio_channel.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_audio_channel.click)
        self.ui_master_data.lineEdit_search_film_audio_channel.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_audio_channel,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.lineEdit_film_colour_format.returnPressed.connect(self.ui_master_data.pushButton_save_film_colour_format.click)
        self.ui_master_data.lineEdit_edit_film_colour_format.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_colour_format.click)
        self.ui_master_data.lineEdit_search_film_colour_format.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_colour_format,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.lineEdit_film_videoformat.returnPressed.connect(self.ui_master_data.pushButton_save_film_videoformat.click)
        self.ui_master_data.lineEdit_edit_film_videoformat.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_videoformat.click)
        self.ui_master_data.lineEdit_search_film_videoformat.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_videoformat,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.lineEdit_film_soundsystem.returnPressed.connect(self.ui_master_data.pushButton_save_film_soundsystem.click)
        self.ui_master_data.lineEdit_edit_film_soundsystem.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_soundsystem.click)
        self.ui_master_data.lineEdit_search_film_soundsystem.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_soundsystem,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.lineEdit_music_studio.returnPressed.connect(self.ui_master_data.pushButton_save_music_studio.click)
        self.ui_master_data.lineEdit_edit_music_studio.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_studio.click)
        self.ui_master_data.lineEdit_search_music_studio.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_studio,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.lineEdit_music_spars_code.returnPressed.connect(self.ui_master_data.pushButton_save_music_spars_code.click)
        self.ui_master_data.lineEdit_edit_music_spars_code.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_spars_code.click)
        self.ui_master_data.lineEdit_search_music_spars_code.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_spars_code,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.lineEdit_music_publisher.returnPressed.connect(self.ui_master_data.pushButton_save_music_publisher.click)
        self.ui_master_data.lineEdit_edit_music_publisher.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_publisher.click)
        self.ui_master_data.lineEdit_search_music_publisher.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_publisher,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.lineEdit_music_style.returnPressed.connect(self.ui_master_data.pushButton_save_music_style.click)
        self.ui_master_data.lineEdit_edit_music_style.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_style.click)
        self.ui_master_data.lineEdit_search_music_style.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_style,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.lineEdit_music_label.returnPressed.connect(self.ui_master_data.pushButton_save_music_label.click)
        self.ui_master_data.lineEdit_edit_music_label.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_label.click)
        self.ui_master_data.lineEdit_search_music_label.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_music_label,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.lineEdit_music_distribution.returnPressed.connect(self.ui_master_data.pushButton_save_music_distribution.click)
        self.ui_master_data.lineEdit_edit_music_distribution.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_distribution.click)
        self.ui_master_data.lineEdit_search_music_distribution.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_music_distribution,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.lineEdit_book_genre.returnPressed.connect(self.ui_master_data.pushButton_save_book_genre.click)
        self.ui_master_data.lineEdit_edit_book_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_book_genre.click)
        self.ui_master_data.lineEdit_search_book_genre.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_book_genre,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.lineEdit_book_publisher.returnPressed.connect(self.ui_master_data.pushButton_save_book_publisher.click)
        self.ui_master_data.lineEdit_edit_book_publisher.returnPressed.connect(self.ui_master_data.pushButton_apply_change_book_publisher.click)
        self.ui_master_data.lineEdit_search_book_publisher.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_book_publisher,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.lineEdit_book_binding.returnPressed.connect(self.ui_master_data.pushButton_save_book_binding.click)
        self.ui_master_data.lineEdit_edit_book_binding.returnPressed.connect(self.ui_master_data.pushButton_apply_change_book_binding.click)
        self.ui_master_data.lineEdit_search_book_binding.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_book_binding,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.lineEdit_coin_alloy.returnPressed.connect(self.ui_master_data.pushButton_save_coin_alloy.click)
        self.ui_master_data.lineEdit_edit_coin_alloy.returnPressed.connect(self.ui_master_data.pushButton_apply_change_coin_alloy.click)
        self.ui_master_data.lineEdit_search_coin_alloy.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_coin_alloy,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.lineEdit_coin_currency.returnPressed.connect(self.ui_master_data.pushButton_save_coin_currency.click)
        self.ui_master_data.lineEdit_edit_coin_currency.returnPressed.connect(self.ui_master_data.pushButton_apply_change_coin_currency.click)
        self.ui_master_data.lineEdit_search_coin_currency.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_coin_currency,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.lineEdit_coin_type.returnPressed.connect(self.ui_master_data.pushButton_save_coin_type.click)
        self.ui_master_data.lineEdit_edit_coin_type.returnPressed.connect(self.ui_master_data.pushButton_apply_change_coin_type.click)
        self.ui_master_data.lineEdit_search_coin_type.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_coin_type,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.lineEdit_coin_manufacturing_procedure.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_manufacturing_procedure.click)
        self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_manufacturing_procedure.click)
        self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_manufacturing_procedure.click)
        self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure_shortcut.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_manufacturing_procedure.click)
        self.ui_master_data.lineEdit_search_coin_manufacturing_procedure.returnPressed.connect(lambda:
            self.handle_timer(
                timer_widget = self.search_delay_timer_coin_manufacturing_procedure,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Degree of preservation  ###
        ###################################################
        self.ui_master_data.lineEdit_coin_degree_preservation.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_degree_preservation.click)
        self.ui_master_data.lineEdit_edit_coin_degree_preservation.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_degree_preservation.click)
        self.ui_master_data.lineEdit_coin_degree_preservation_shortcut.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_degree_preservation.click)
        self.ui_master_data.lineEdit_edit_coin_degree_preservation_shortcut.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_degree_preservation.click)
        self.ui_master_data.lineEdit_search_coin_degree_preservation.returnPressed.connect(lambda:
            self.handle_timer(
                timer_widget = self.search_delay_timer_coin_degree_preservation,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.ui_master_data.lineEdit_coin_mint.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_mint.click)
        self.ui_master_data.lineEdit_edit_coin_mint.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_mint.click)
        self.ui_master_data.lineEdit_coin_mint_mark.returnPressed.connect(
            self.ui_master_data.pushButton_save_coin_mint.click)
        self.ui_master_data.lineEdit_edit_coin_mint_mark.returnPressed.connect(
            self.ui_master_data.pushButton_apply_change_coin_mint.click)
        self.ui_master_data.lineEdit_search_coin_mint.returnPressed.connect(lambda:
            self.handle_timer(
                timer_widget = self.search_delay_timer_coin_mint,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.lineEdit_videogame_genre.returnPressed.connect(self.ui_master_data.pushButton_save_videogame_genre.click)
        self.ui_master_data.lineEdit_edit_videogame_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_videogame_genre.click)
        self.ui_master_data.lineEdit_search_videogame_genre.returnPressed.connect(lambda:
            self.handle_timer(timer_widget = self.search_delay_timer_videogame_genre,
                show_wait_message = True,
                text = self.tr("Searching for records. This should only take a moment.")))

        return

    def init_custom_progres_bar(self):
        '''
           SUMMARY
           =======
           This method implements an overridden QProgressBar().

           EXTENTED DISCRIPTION
           ====================
           The overridden ProgressBars Range is set to minimum = 0
           and to maximum = 0 for pulsing. The overwridden ProgressBar
           contains also the setText()-method, so the text is also used.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create a overwritten QProgressbar")

        self.hide_widget(widget = self.ui_master_data.frame_progress_bar, boolen_value = True)

        self.busy_progress_bar = ProgressBarText(self)
        self.busy_progress_bar.setObjectName("busy_progress_bar")
        self.busy_progress_bar.setRange(0, 0)

        logger.info(self.busy_progress_bar.objectName())

        self.set_text(widget = self.busy_progress_bar, text = 'Waiting for...')

        #   add the ProgressBarText()-object to the QLayout
        self.horizontal_Layout = self.ui_master_data.horizontalLayout_progress_bar
        self.horizontal_Layout.addWidget(self.busy_progress_bar)

        return

    def init_tree_view(self):
        '''
           SUMMARY
           =======
           Here we initialize the QTreeView()-object for all categories.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QTreeView for all categories")

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.tree_view_general_country.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_country)
        self.ui_master_data.tree_view_general_country.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_country.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_country.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_country,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_country,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_country:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.tree_view_general_region.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_region)
        self.ui_master_data.tree_view_general_region.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_region.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_region.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_region,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_region,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_region:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.tree_view_general_language.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_language)
        self.ui_master_data.tree_view_general_language.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_language.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_language.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_language,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_language,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_language:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.tree_view_general_age_release.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_age_release)
        self.ui_master_data.tree_view_general_age_release.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_age_release.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_age_release.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_age_release,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_age_release,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_age_release:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Nationality  ##############
        ###################################################
        self.ui_master_data.tree_view_person_nationality.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_nationality)
        self.ui_master_data.tree_view_person_nationality.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_nationality.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_nationality.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_nationality,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_nationality,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_nationality:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.tree_view_film_genre.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_genre)
        self.ui_master_data.tree_view_film_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_genre:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.tree_view_general_award.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_award)
        self.ui_master_data.tree_view_general_award.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_award.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_award.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_award,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_award,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_award:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.tree_view_music_genre.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_genre)
        self.ui_master_data.tree_view_music_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_genre:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.tree_view_person_gender.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_gender)
        self.ui_master_data.tree_view_person_gender.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_gender.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_gender.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_gender,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_gender,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_gender:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.tree_view_person_salutation.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_salutation)
        self.ui_master_data.tree_view_person_salutation.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_salutation.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_salutation.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_salutation,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_salutation,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_salutation:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.tree_view_person_title.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_title)
        self.ui_master_data.tree_view_person_title.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_title.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_title.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_title,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_title,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_title:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.tree_view_person_eye_color.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_eye_color)
        self.ui_master_data.tree_view_person_eye_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_eye_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_eye_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_eye_color,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_eye_color,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_eye_color:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.tree_view_person_hair_color.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_hair_color)
        self.ui_master_data.tree_view_person_hair_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_hair_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_hair_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_hair_color,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_hair_color,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_hair_color:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.tree_view_person_religion.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_religion)
        self.ui_master_data.tree_view_person_religion.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_religion.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_religion.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_religion,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_religion,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_religion:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.tree_view_person_relationship_status.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status)
        self.ui_master_data.tree_view_person_relationship_status.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship_status.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship_status.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_relationship_status,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_relationship_status,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_relationship_status:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.tree_view_person_relationship.setModel(self._kwargs.get('standard_item_model').standard_item_model_person_relationship)
        self.ui_master_data.tree_view_person_relationship.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_relationship,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_relationship,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_relationship:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.tree_view_general_media_type.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_media_type)
        self.ui_master_data.tree_view_general_media_type.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_media_type.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_media_type.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_media_type,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_media_type,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_media_type:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.tree_view_general_packaging.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_packaging)
        self.ui_master_data.tree_view_general_packaging.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_packaging.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_packaging.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_packaging,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_packaging,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_packaging:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.tree_view_film_production_company.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_production_company)
        self.ui_master_data.tree_view_film_production_company.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_production_company.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_production_company.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_production_company,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_production_company,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_production_company:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.tree_view_film_rentals.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_rentals)
        self.ui_master_data.tree_view_film_rentals.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_rentals.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_rentals.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_rentals,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_rentals,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_rentals:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.tree_view_film_distribution.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_distribution)
        self.ui_master_data.tree_view_film_distribution.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_distribution.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_distribution.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_distribution,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_distribution,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_distribution:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.tree_view_film_studio.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_studio)
        self.ui_master_data.tree_view_film_studio.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_studio.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_studio.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_studio,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_studio,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_studio:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.tree_view_film_resolution.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_resolution)
        self.ui_master_data.tree_view_film_resolution.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_film_resolution.header().hideSection(0)
        self.ui_master_data.tree_view_film_resolution.header().show()

        self.ui_master_data.tree_view_film_resolution.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_resolution.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_resolution,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_resolution,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_resolution:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.tree_view_general_manufacturer.setModel(self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer)
        self.ui_master_data.tree_view_general_manufacturer.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_manufacturer.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_manufacturer.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_manufacturer,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_manufacturer,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_manufacturer:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.tree_view_film_regional_code.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_regional_code)
        self.ui_master_data.tree_view_film_regional_code.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_film_regional_code.header().hideSection(0)
        self.ui_master_data.tree_view_film_regional_code.header().show()

        self.ui_master_data.tree_view_film_regional_code.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_regional_code.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_regional_code,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_regional_code,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_regional_code:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.tree_view_film_aspect_ratio.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio)
        self.ui_master_data.tree_view_film_aspect_ratio.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_film_aspect_ratio.header().hideSection(0)
        self.ui_master_data.tree_view_film_aspect_ratio.header().show()

        self.ui_master_data.tree_view_film_aspect_ratio.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_aspect_ratio.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_aspect_ratio,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_aspect_ratio,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_aspect_ratio:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.tree_view_film_dimension.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_dimension)
        self.ui_master_data.tree_view_film_dimension.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_dimension.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_dimension.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_dimension,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_dimension,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_dimension:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.tree_view_film_edition.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_edition)
        self.ui_master_data.tree_view_film_edition.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_edition.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_edition.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_edition,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_edition,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_edition:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.tree_view_film_version.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_version)
        self.ui_master_data.tree_view_film_version.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_version.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_version.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_version,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_version,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_version:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.tree_view_film_videonorm.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_videonorm)
        self.ui_master_data.tree_view_film_videonorm.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_videonorm.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_videonorm.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_videonorm,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_videonorm,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_videonorm:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.tree_view_film_audioformat.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_audioformat)
        self.ui_master_data.tree_view_film_audioformat.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_audioformat.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_audioformat.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_audioformat,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_audioformat,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_audioformat:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.tree_view_film_audio_channel.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel)
        self.ui_master_data.tree_view_film_audio_channel.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_audio_channel.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_audio_channel.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_audio_channel,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_audio_channel,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_audio_channel:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.tree_view_film_colour_format.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_colour_format)
        self.ui_master_data.tree_view_film_colour_format.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_colour_format.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_colour_format.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_colour_format,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_colour_format,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_colour_format:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.tree_view_film_videoformat.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_videoformat)
        self.ui_master_data.tree_view_film_videoformat.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_videoformat.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_videoformat.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_videoformat,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_videoformat,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_videoformat:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.tree_view_film_soundsystem.setModel(self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem)
        self.ui_master_data.tree_view_film_soundsystem.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_soundsystem.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_soundsystem.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_soundsystem,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_soundsystem,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_soundsystem:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.tree_view_music_studio.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_studio)
        self.ui_master_data.tree_view_music_studio.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_studio.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_studio.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_studio,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_studio,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_studio:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.tree_view_music_spars_code.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_spars_code)
        self.ui_master_data.tree_view_music_spars_code.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_spars_code.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_spars_code.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_spars_code,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_spars_code,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_spars_code:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.tree_view_music_publisher.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_publisher)
        self.ui_master_data.tree_view_music_publisher.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_publisher.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_publisher.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_publisher,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_publisher,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_publisher:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.tree_view_music_style.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_style)
        self.ui_master_data.tree_view_music_style.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_style.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_style.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_style,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_style,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_style:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.tree_view_music_label.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_label)
        self.ui_master_data.tree_view_music_label.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_label.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_label.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_label,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_label,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_label:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.tree_view_music_distribution.setModel(self._kwargs.get('standard_item_model').standard_item_model_music_distribution)
        self.ui_master_data.tree_view_music_distribution.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_distribution.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_distribution.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_distribution,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_distribution,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_distribution:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.tree_view_book_genre.setModel(self._kwargs.get('standard_item_model').standard_item_model_book_genre)
        self.ui_master_data.tree_view_book_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_book_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_book_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_book_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_book_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_book_genre:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.tree_view_book_publisher.setModel(self._kwargs.get('standard_item_model').standard_item_model_book_publisher)
        self.ui_master_data.tree_view_book_publisher.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_book_publisher.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_book_publisher.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_book_publisher,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_book_publisher,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_book_publisher:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.tree_view_book_binding.setModel(self._kwargs.get('standard_item_model').standard_item_model_book_binding)
        self.ui_master_data.tree_view_book_binding.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_book_binding.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_book_binding.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_book_binding,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_book_binding,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_book_binding:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.tree_view_coin_alloy.setModel(self._kwargs.get('standard_item_model').standard_item_model_coin_alloy)
        self.ui_master_data.tree_view_coin_alloy.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_coin_alloy.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_alloy.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_alloy,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_alloy,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_alloy:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.tree_view_coin_currency.setModel(self._kwargs.get('standard_item_model').standard_item_model_coin_currency)
        self.ui_master_data.tree_view_coin_currency.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_coin_currency.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_currency.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_currency,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_currency,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_currency:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.tree_view_coin_type.setModel(self._kwargs.get('standard_item_model').standard_item_model_coin_type)
        self.ui_master_data.tree_view_coin_type.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_coin_type.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_type.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_type,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_type,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_type:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.tree_view_coin_manufacturing_procedure.setModel(
            self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure)
        self.ui_master_data.tree_view_coin_manufacturing_procedure.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_coin_manufacturing_procedure.header().hideSection(0)
        self.ui_master_data.tree_view_coin_manufacturing_procedure.header().show()

        self.ui_master_data.tree_view_coin_manufacturing_procedure.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_manufacturing_procedure.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_manufacturing_procedure,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_manufacturing_procedure,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_manufacturing_procedure:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Degree of preservation  ###
        ###################################################
        self.ui_master_data.tree_view_coin_degree_preservation.setModel(
            self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation)
        self.ui_master_data.tree_view_coin_degree_preservation.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_coin_degree_preservation.header().hideSection(0)
        self.ui_master_data.tree_view_coin_degree_preservation.header().show()

        self.ui_master_data.tree_view_coin_degree_preservation.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_degree_preservation.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_degree_preservation,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_degree_preservation,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_degree_preservation:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.ui_master_data.tree_view_coin_mint.setModel(
            self._kwargs.get('standard_item_model').standard_item_model_coin_mint)
        self.ui_master_data.tree_view_coin_mint.sortByColumn(1, Qt.AscendingOrder)
        self.ui_master_data.tree_view_coin_mint.header().hideSection(0)
        self.ui_master_data.tree_view_coin_mint.header().show()

        self.ui_master_data.tree_view_coin_mint.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_coin_mint.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_coin_mint,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_coin_mint,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_coin_mint:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.tree_view_videogame_genre.setModel(self._kwargs.get('standard_item_model').standard_item_model_videogame_genre)
        self.ui_master_data.tree_view_videogame_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_videogame_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_videogame_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_videogame_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_videogame_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_videogame_genre:
        self.open_context_menu(position = position,
                               tree_view_obj = tree_view_obj,
                               pushbutton_object_edit = pushbutton_object_edit,
                               pushbutton_object_delete = pushbutton_object_delete))

        return

    def init_navi_treeview(self):
        '''
           SUMMARY
           =======
           Here we initialize a special QTreeView()-object for the navigation menu.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QTreeView for all navigation menu")

        self.ui_master_data.treeView_navigation.setModel(self._navigate_standard_model)
        self.ui_master_data.treeView_navigation.sortByColumn(1, Qt.AscendingOrder)

        self.populate_navi_data(model_obj = self._navigate_standard_model, data = self.NAVI_DATA)

        self.set_hide_column(tree_view_object = self.ui_master_data.treeView_navigation,
                             column = 1,
                             bool_value = True)

        self.ui_master_data.treeView_navigation.expandAll()

        return

    def init_selectionChanged_signal_treeview(self):
        '''
           SUMMARY
           =======
           We need to initialize the selectionChanged-signals for QTreeView .

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize selectionChanged-signal for QTreeView")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.tree_view_general_region.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_region:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.tree_view_general_country.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_country:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.tree_view_general_language.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_language:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.tree_view_general_age_release.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_age_release:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.tree_view_person_nationality.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_nationality:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.tree_view_film_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_genre:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.tree_view_general_award.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_award:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.tree_view_music_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_genre:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.tree_view_person_gender.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_gender:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.tree_view_person_salutation.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_salutation:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.tree_view_person_title.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_title:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.tree_view_person_eye_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_eye_color:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.tree_view_person_hair_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_hair_color:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.tree_view_person_religion.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_religion:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.tree_view_person_relationship_status.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_relationship_status:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.tree_view_person_relationship.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_relationship:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.tree_view_general_media_type.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_media_type:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.tree_view_general_packaging.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_packaging:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.tree_view_film_production_company.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_production_company:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.tree_view_film_rentals.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_rentals:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.tree_view_film_distribution.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_distribution:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.tree_view_film_studio.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_studio:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.tree_view_film_resolution.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj_resolution = self.ui_master_data.lineEdit_film_resolution,
            edit_line_obj_presenting_format = self.ui_master_data.lineEdit_film_presenting_format:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = [edit_line_obj_resolution,
                                                                                                  edit_line_obj_presenting_format]))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.tree_view_general_manufacturer.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_manufacturer:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.tree_view_film_regional_code.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj_regional_code = self.ui_master_data.lineEdit_film_regional_code,
            edit_line_obj_region = self.ui_master_data.lineEdit_film_region:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = [edit_line_obj_regional_code, edit_line_obj_region]))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.tree_view_film_aspect_ratio.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj_aspect_ratio = self.ui_master_data.lineEdit_film_aspect_ratio,
            edit_line_obj_aspect_ratio_format = self.ui_master_data.lineEdit_film_aspect_ratio_format:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = [edit_line_obj_aspect_ratio, edit_line_obj_aspect_ratio_format]))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.tree_view_film_dimension.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_dimension:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.tree_view_film_edition.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_edition:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.tree_view_film_version.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_version:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.tree_view_film_videonorm.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_videonorm:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.tree_view_film_audioformat.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_audioformat:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.tree_view_film_audio_channel.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_audio_channel:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.tree_view_film_colour_format.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_colour_format:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.tree_view_film_videoformat.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_videoformat:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.tree_view_film_soundsystem.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_soundsystem:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.tree_view_music_studio.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_studio:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.tree_view_music_spars_code.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_spars_code:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.tree_view_music_publisher.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_publisher:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.tree_view_music_style.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_style:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.tree_view_music_label.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_label:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.tree_view_music_distribution.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_distribution:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.tree_view_book_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_book_genre:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.tree_view_book_publisher.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_book_publisher:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.tree_view_book_binding.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_book_binding:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.tree_view_coin_alloy.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_coin_alloy:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.tree_view_coin_currency.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_coin_currency:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.tree_view_coin_type.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_coin_type:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.tree_view_coin_manufacturing_procedure.selectionModel().selectionChanged.connect(
            lambda new_index,
                   old_index,
                   edit_line_obj_coin_manufacturing_procedure = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                   edit_line_obj_coin_manufacturing_procedure_shortcut = self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut:
            self.handle_selection_changed(new_index = new_index,
                                          old_index = old_index,
                                          widget = [edit_line_obj_coin_manufacturing_procedure,
                                                    edit_line_obj_coin_manufacturing_procedure_shortcut]))

        ###################################################
        #################  Coin Degree of preservation  ###
        ###################################################
        self.ui_master_data.tree_view_coin_degree_preservation.selectionModel().selectionChanged.connect(
            lambda new_index,
                   old_index,
                   edit_line_obj_coin_degree_preservation = self.ui_master_data.lineEdit_coin_degree_preservation,
                   edit_line_obj_coin_degree_preservation_shortcut = self.ui_master_data.lineEdit_coin_degree_preservation_shortcut:
            self.handle_selection_changed(new_index = new_index,
                                          old_index = old_index,
                                          widget = [edit_line_obj_coin_degree_preservation,
                                                    edit_line_obj_coin_degree_preservation_shortcut]))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.ui_master_data.tree_view_coin_mint.selectionModel().selectionChanged.connect(
            lambda new_index,
                   old_index,
                   edit_line_obj_coin_mint = self.ui_master_data.lineEdit_coin_mint,
                   edit_line_obj_coin_mint_mark = self.ui_master_data.lineEdit_coin_mint_mark:
            self.handle_selection_changed(new_index = new_index,
                                          old_index = old_index,
                                          widget = [edit_line_obj_coin_mint,
                                                    edit_line_obj_coin_mint_mark]))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.tree_view_videogame_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_videogame_genre:
            self.handle_selection_changed(new_index = new_index,
                                          old_index = old_index,
                                          widget = edit_line_obj))

        return

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots for QpuschButton()-object(s).

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.pushButton_refresh_general_region.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_general_region",
                hide_column = True,
                force_fetch_all = True))

        self.ui_master_data.pushButton_save_general_region.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_general_region",
                hide_column = True))

        self.ui_master_data.pushButton_save_general_region.clicked.connect(
            self.interrupt_working_thread_signal.emit)

        self.ui_master_data.pushButton_apply_change_general_region.clicked.connect(
            lambda: self.manage_methods(
                method_name = "edit_general_region",
                hide_column = True))

        self.ui_master_data.pushButton_delete_general_region.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_general_region",
                hide_column = True))

        self.ui_master_data.pushButton_download_general_region.clicked.connect(lambda:
            self.change_page(index = 2,
                stack_widget = self.ui_master_data.stackedWidget_general_region,
                column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_region.clicked.connect(lambda:
            self.change_page(index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_region,
                column_index = 1))

        self.ui_master_data.pushButton_update_general_region.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_general_region",
                hide_column = True))

        self.ui_master_data.pushButton_edit_general_region.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_general_region,
                lineEdit_object = self.ui_master_data.lineEdit_edit_general_region,
                set_text = self.ui_master_data.lineEdit_general_region.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_region.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_region,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_country.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_general_country",
                hide_column = True,
                force_fetch_all = True))

        self.ui_master_data.pushButton_save_general_country.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_general_country",
                hide_column = True))

        self.ui_master_data.pushButton_save_general_country.clicked.connect(
            self.interrupt_working_thread_signal.emit)

        self.ui_master_data.pushButton_apply_change_general_country.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_general_country",
                hide_column = True))

        self.ui_master_data.pushButton_delete_general_country.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_general_country",
                hide_column = True))

        self.ui_master_data.pushButton_download_general_country.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_general_country,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_general_country.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_country,
                column_index = 1))

        self.ui_master_data.pushButton_update_general_country.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_general_country"))

        self.ui_master_data.pushButton_edit_general_country.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_general_country,
                lineEdit_object = self.ui_master_data.lineEdit_edit_general_country,
                set_text = self.ui_master_data.lineEdit_general_country.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_country.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_country,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        self.ui_master_data.pushButton_master_data_close.clicked.connect(
            self.close)

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_language.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_general_language",
                hide_column = True))

        self.ui_master_data.pushButton_save_general_language.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_general_language",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_general_language.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_general_language",
                hide_column = True))

        self.ui_master_data.pushButton_delete_general_language.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_general_language",
                hide_column = True))

        self.ui_master_data.pushButton_download_general_language.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_general_language,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_general_language.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_language,
                column_index = 1))

        self.ui_master_data.pushButton_update_general_language.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_general_language",
                hide_column = True))

        self.ui_master_data.pushButton_edit_general_language.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_general_language,
                lineEdit_object = self.ui_master_data.lineEdit_edit_general_language,
                set_text = self.ui_master_data.lineEdit_general_language.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_language.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_language,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.pushButton_refresh_general_age_release.clicked.connect(lambda:
            self.manage_methods(method_name = "fetch_all_general_age_release"))

        self.ui_master_data.pushButton_save_general_age_release.clicked.connect(lambda:
            self.manage_methods(method_name = "add_general_age_release"))

        self.ui_master_data.pushButton_apply_change_general_age_release.clicked.connect(lambda:
            self.manage_methods(method_name = "edit_general_age_release"))

        self.ui_master_data.pushButton_delete_general_age_release.clicked.connect(lambda:
            self.manage_methods(method_name = "delete_general_age_release"))

        self.ui_master_data.pushButton_download_general_age_release.clicked.connect(lambda:
            self.change_page(index = 2,
                stack_widget = self.ui_master_data.stackedWidget_general_age_release,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_general_age_release.clicked.connect(lambda:
            self.change_page(index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_age_release,
                column_index = 1))

        self.ui_master_data.pushButton_update_general_age_release.clicked.connect(lambda:
            self.manage_methods(method_name = "update_general_age_release"))


        self.ui_master_data.pushButton_edit_general_age_release.clicked.connect(lambda:
            self.handle_edit(index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_general_age_release,
                lineEdit_object = self.ui_master_data.lineEdit_edit_general_age_release,
                set_text = self.ui_master_data.lineEdit_general_age_release.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_age_release.clicked.connect(lambda:
            self.change_page(index=0,
                stack_widget=self.ui_master_data.stackedWidget_general_age_release,
                clear_widget=True,
                current_obj_clear_selection=True,
                delete_selected_id = True,
                current_obj_clear=True))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_nationality.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_person_nationality",
                hide_column = True))

        self.ui_master_data.pushButton_save_person_nationality.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_person_nationality",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_person_nationality.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_person_nationality",
                hide_column = True))

        self.ui_master_data.pushButton_delete_person_nationality.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_person_nationality",
                hide_column = True))

        self.ui_master_data.pushButton_download_person_nationality.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_person_nationality.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                column_index = 1))

        self.ui_master_data.pushButton_update_person_nationality.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_person_nationality",
                hide_column = True))


        self.ui_master_data.pushButton_edit_person_nationality.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_person_nationality,
                lineEdit_object = self.ui_master_data.lineEdit_edit_person_nationality,
                set_text = self.ui_master_data.lineEdit_person_nationality.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_nationality.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_film_genre",
                hide_column = True))

        self.ui_master_data.pushButton_save_film_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_film_genre",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_film_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_film_genre",
                hide_column = True))

        self.ui_master_data.pushButton_delete_film_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_film_genre",
                hide_column = True))

        self.ui_master_data.pushButton_download_film_genre.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_film_genre,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_film_genre.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_film_genre,
                column_index = 1))

        self.ui_master_data.pushButton_update_film_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_film_genre",
                hide_column = True))


        self.ui_master_data.pushButton_edit_film_genre.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_film_genre,
                lineEdit_object = self.ui_master_data.lineEdit_edit_film_genre,
                set_text = self.ui_master_data.lineEdit_film_genre.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_genre.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_film_genre,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.pushButton_refresh_general_award.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_general_award",
                hide_column = True))

        self.ui_master_data.pushButton_save_general_award.clicked.connect(
            lambda: self.manage_methods(
                method_name = "add_general_award",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_general_award.clicked.connect(lambda:
            self.manage_methods(
            method_name = "edit_general_award",
                hide_column = True))

        self.ui_master_data.pushButton_delete_general_award.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_general_award",
                hide_column = True))

        self.ui_master_data.pushButton_download_general_award.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_general_award,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_general_award.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_award,
                column_index = 1))

        self.ui_master_data.pushButton_update_general_award.clicked.connect(
            lambda: self.manage_methods(
                method_name = "update_general_award",
                hide_column = True))

        self.ui_master_data.pushButton_edit_general_award.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_general_award,
                lineEdit_object = self.ui_master_data.lineEdit_edit_general_award,
                set_text = self.ui_master_data.lineEdit_general_award.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_award.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_general_award,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.pushButton_refresh_music_genre.clicked.connect(
            lambda: self.manage_methods(
                method_name = "fetch_all_music_genre",
                hide_column = True))

        self.ui_master_data.pushButton_save_music_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_music_genre",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_music_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_music_genre",
                hide_column = True))

        self.ui_master_data.pushButton_delete_music_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_music_genre",
                hide_column = True))

        self.ui_master_data.pushButton_download_music_genre.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_music_genre,
                column_index = 1))

        self.ui_master_data.pushButton_cancel_update_music_genre.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_music_genre,
                column_index = 1))

        self.ui_master_data.pushButton_update_music_genre.clicked.connect(lambda:
            self.manage_methods(
                method_name = "update_music_genre",
                hide_column = True))


        self.ui_master_data.pushButton_edit_music_genre.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_music_genre,
                lineEdit_object = self.ui_master_data.lineEdit_edit_music_genre,
                set_text = self.ui_master_data.lineEdit_music_genre.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_genre.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_music_genre,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.pushButton_refresh_person_gender.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_person_gender",
                hide_column = True))

        self.ui_master_data.pushButton_save_person_gender.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_person_gender",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_person_gender.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_person_gender",
                hide_column = True))

        self.ui_master_data.pushButton_delete_person_gender.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_person_gender",
                hide_column = True))

        # self.ui_master_data.pushButton_download_person_gender.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_gender.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "update_person_gender"))


        self.ui_master_data.pushButton_edit_person_gender.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
                lineEdit_object = self.ui_master_data.lineEdit_edit_person_gender,
                set_text = self.ui_master_data.lineEdit_person_gender.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_gender.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_gender,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_salutation.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_person_salutation",
                hide_column = True))

        self.ui_master_data.pushButton_save_person_salutation.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_person_salutation",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_person_salutation.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_person_salutation",
                hide_column = True))

        self.ui_master_data.pushButton_delete_person_salutation.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_person_salutation",
                hide_column = True))

        # self.ui_master_data.pushButton_download_person_salutation.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_salutation.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "update_person_salutation"))


        self.ui_master_data.pushButton_edit_person_salutation.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
                lineEdit_object = self.ui_master_data.lineEdit_edit_person_salutation,
                set_text = self.ui_master_data.lineEdit_person_salutation.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_salutation.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_salutation,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.pushButton_refresh_person_title.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_person_title",
                hide_column = True))

        self.ui_master_data.pushButton_save_person_title.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_person_title",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_person_title.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_person_title",
                hide_column = True))

        self.ui_master_data.pushButton_delete_person_title.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_person_title",
                hide_column = True))

        # self.ui_master_data.pushButton_download_person_title.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_title,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_title.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_title,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_title.clicked.connect(lambda: self.manage_methods(method_name = "update_person_title"))


        self.ui_master_data.pushButton_edit_person_title.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_person_title,
                lineEdit_object = self.ui_master_data.lineEdit_edit_person_title,
                set_text = self.ui_master_data.lineEdit_person_title.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_title.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_title,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_eye_color.clicked.connect(lambda:
            self.manage_methods(
                method_name = "fetch_all_person_eye_color",
                hide_column = True))

        self.ui_master_data.pushButton_save_person_eye_color.clicked.connect(lambda:
            self.manage_methods(
                method_name = "add_person_eye_color",
                hide_column = True))

        self.ui_master_data.pushButton_apply_change_person_eye_color.clicked.connect(lambda:
            self.manage_methods(
                method_name = "edit_person_eye_color",
                hide_column = True))

        self.ui_master_data.pushButton_delete_person_eye_color.clicked.connect(lambda:
            self.manage_methods(
                method_name = "delete_person_eye_color",
                hide_column = True))

        # self.ui_master_data.pushButton_download_person_eye_color.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_eye_color.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "update_person_eye_color"))


        self.ui_master_data.pushButton_edit_person_eye_color.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
                lineEdit_object = self.ui_master_data.lineEdit_edit_person_eye_color,
                set_text = self.ui_master_data.lineEdit_person_eye_color.text(),
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_eye_color.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_person_eye_color,
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_hair_color"))

        self.ui_master_data.pushButton_save_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "add_person_hair_color"))

        self.ui_master_data.pushButton_apply_change_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_hair_color"))

        self.ui_master_data.pushButton_delete_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_hair_color"))

        # self.ui_master_data.pushButton_download_person_hair_color.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_hair_color.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "update_person_hair_color"))


        self.ui_master_data.pushButton_edit_person_hair_color.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_hair_color,
                             set_text = self.ui_master_data.lineEdit_person_hair_color.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_hair_color.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_person_hair_color,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_religion"))

        self.ui_master_data.pushButton_save_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "add_person_religion"))

        self.ui_master_data.pushButton_apply_change_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_religion"))

        self.ui_master_data.pushButton_delete_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_religion"))

        # self.ui_master_data.pushButton_download_person_religion.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_religion.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "update_person_religion"))


        self.ui_master_data.pushButton_edit_person_religion.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_religion,
                             set_text = self.ui_master_data.lineEdit_person_religion.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_religion.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_person_religion,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.pushButton_refresh_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_relationship_status"))

        self.ui_master_data.pushButton_save_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "add_person_relationship_status"))

        self.ui_master_data.pushButton_apply_change_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_relationship_status"))

        self.ui_master_data.pushButton_delete_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_relationship_status"))

        # self.ui_master_data.pushButton_download_person_relationship_status.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_relationship_status.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "update_person_relationship_status"))


        self.ui_master_data.pushButton_edit_person_relationship_status.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship_status,
                             set_text = self.ui_master_data.lineEdit_person_relationship_status.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_relationship_status.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_person_relationship_status,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.pushButton_refresh_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_relationship"))

        self.ui_master_data.pushButton_save_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "add_person_relationship"))

        self.ui_master_data.pushButton_apply_change_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_relationship"))

        self.ui_master_data.pushButton_delete_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_relationship"))

        self.ui_master_data.pushButton_download_person_relationship.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_person_relationship.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "update_person_relationship"))


        self.ui_master_data.pushButton_edit_person_relationship.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship,
                             set_text = self.ui_master_data.lineEdit_person_relationship.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_relationship.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_person_relationship,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_media_type"))

        self.ui_master_data.pushButton_save_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "add_general_media_type"))

        self.ui_master_data.pushButton_apply_change_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_media_type"))

        self.ui_master_data.pushButton_delete_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_media_type"))

        self.ui_master_data.pushButton_download_general_media_type.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_media_type.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "update_general_media_type"))


        self.ui_master_data.pushButton_edit_general_media_type.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_general_media_type,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_media_type,
                             set_text = self.ui_master_data.lineEdit_general_media_type.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_media_type.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_general_media_type,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_packaging"))

        self.ui_master_data.pushButton_save_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "add_general_packaging"))

        self.ui_master_data.pushButton_apply_change_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_packaging"))

        self.ui_master_data.pushButton_delete_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_packaging"))

        self.ui_master_data.pushButton_download_general_packaging.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_packaging.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "update_general_packaging"))


        self.ui_master_data.pushButton_edit_general_packaging.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_general_packaging,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_packaging,
                             set_text = self.ui_master_data.lineEdit_general_packaging.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_packaging.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_general_packaging,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.pushButton_refresh_film_production_company.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_production_company"))

        self.ui_master_data.pushButton_save_film_production_company.clicked.connect(lambda: self.manage_methods(method_name = "add_film_production_company"))

        self.ui_master_data.pushButton_apply_change_film_production_company.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_production_company"))

        self.ui_master_data.pushButton_delete_film_production_company.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_production_company"))

        self.ui_master_data.pushButton_download_film_production_company.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_production_company,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_production_company.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_production_company,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_production_company.clicked.connect(lambda: self.manage_methods(method_name = "update_film_production_company"))


        self.ui_master_data.pushButton_edit_film_production_company.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_production_company,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_production_company,
                             set_text = self.ui_master_data.lineEdit_film_production_company.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_production_company.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_production_company,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_rentals.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_rentals"))

        self.ui_master_data.pushButton_save_film_rentals.clicked.connect(lambda: self.manage_methods(method_name = "add_film_rentals"))

        self.ui_master_data.pushButton_apply_change_film_rentals.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_rentals"))

        self.ui_master_data.pushButton_delete_film_rentals.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_rentals"))

        self.ui_master_data.pushButton_download_film_rentals.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_rentals,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_rentals.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_rentals,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_rentals.clicked.connect(lambda: self.manage_methods(method_name = "update_film_rentals"))


        self.ui_master_data.pushButton_edit_film_rentals.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_rentals,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_rentals,
                             set_text = self.ui_master_data.lineEdit_film_rentals.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_rentals.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_rentals,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_distribution.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_distribution"))

        self.ui_master_data.pushButton_save_film_distribution.clicked.connect(lambda: self.manage_methods(method_name = "add_film_distribution"))

        self.ui_master_data.pushButton_apply_change_film_distribution.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_distribution"))

        self.ui_master_data.pushButton_delete_film_distribution.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_distribution"))

        self.ui_master_data.pushButton_download_film_distribution.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_distribution,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_distribution.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_distribution,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_distribution.clicked.connect(lambda: self.manage_methods(method_name = "update_film_distribution"))


        self.ui_master_data.pushButton_edit_film_distribution.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_distribution,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_distribution,
                             set_text = self.ui_master_data.lineEdit_film_distribution.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_distribution.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_distribution,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_studio.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_studio"))

        self.ui_master_data.pushButton_save_film_studio.clicked.connect(lambda: self.manage_methods(method_name = "add_film_studio"))

        self.ui_master_data.pushButton_apply_change_film_studio.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_studio"))

        self.ui_master_data.pushButton_delete_film_studio.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_studio"))

        self.ui_master_data.pushButton_download_film_studio.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_studio,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_studio.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_studio,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_studio.clicked.connect(lambda: self.manage_methods(method_name = "update_film_studio"))


        self.ui_master_data.pushButton_edit_film_studio.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_studio,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_studio,
                             set_text = self.ui_master_data.lineEdit_film_studio.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_studio.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_studio,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_resolution.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_resolution"))

        self.ui_master_data.pushButton_save_film_resolution.clicked.connect(lambda: self.manage_methods(method_name = "add_film_resolution"))

        self.ui_master_data.pushButton_apply_change_film_resolution.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_resolution"))

        self.ui_master_data.pushButton_delete_film_resolution.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_resolution"))

        self.ui_master_data.pushButton_download_film_resolution.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_resolution,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_resolution.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_resolution,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_resolution.clicked.connect(lambda: self.manage_methods(method_name = "update_film_resolution"))


        self.ui_master_data.pushButton_edit_film_resolution.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_resolution,
                             list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_film_resolution,
                                                   self.ui_master_data.lineEdit_film_resolution.text()),
                                                   (self.ui_master_data.lineEdit_edit_film_presenting_format,
                                                   self.ui_master_data.lineEdit_film_presenting_format.text())],
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_resolution.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_resolution,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.pushButton_refresh_general_manufacturer.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_manufacturer"))

        self.ui_master_data.pushButton_save_general_manufacturer.clicked.connect(lambda: self.manage_methods(method_name = "add_general_manufacturer"))

        self.ui_master_data.pushButton_apply_change_general_manufacturer.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_manufacturer"))

        self.ui_master_data.pushButton_delete_general_manufacturer.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_manufacturer"))

        self.ui_master_data.pushButton_download_general_manufacturer.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_general_manufacturer,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_manufacturer.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_general_manufacturer,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_manufacturer.clicked.connect(lambda: self.manage_methods(method_name = "update_general_manufacturer"))


        self.ui_master_data.pushButton_edit_general_manufacturer.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_general_manufacturer,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_manufacturer,
                             set_text = self.ui_master_data.lineEdit_general_manufacturer.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_manufacturer.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_general_manufacturer,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.pushButton_refresh_film_regional_code.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_regional_code"))

        self.ui_master_data.pushButton_save_film_regional_code.clicked.connect(lambda: self.manage_methods(method_name = "add_film_regional_code"))

        self.ui_master_data.pushButton_apply_change_film_regional_code.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_regional_code"))

        self.ui_master_data.pushButton_delete_film_regional_code.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_regional_code"))

        self.ui_master_data.pushButton_download_film_regional_code.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_regional_code,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_regional_code.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_regional_code,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_regional_code.clicked.connect(lambda: self.manage_methods(method_name = "update_film_regional_code"))


        self.ui_master_data.pushButton_edit_film_regional_code.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_regional_code,
                             list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_film_regional_code,
                                                   self.ui_master_data.lineEdit_film_regional_code.text()),
                                                   (self.ui_master_data.lineEdit_edit_film_region,
                                                   self.ui_master_data.lineEdit_film_region.text())],
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_regional_code.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_regional_code,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_aspect_ratio.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_aspect_ratio"))

        self.ui_master_data.pushButton_save_film_aspect_ratio.clicked.connect(lambda: self.manage_methods(method_name = "add_film_aspect_ratio"))

        self.ui_master_data.pushButton_apply_change_film_aspect_ratio.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_aspect_ratio"))

        self.ui_master_data.pushButton_delete_film_aspect_ratio.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_aspect_ratio"))

        self.ui_master_data.pushButton_download_film_aspect_ratio.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_aspect_ratio,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_aspect_ratio.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_aspect_ratio,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_aspect_ratio.clicked.connect(lambda: self.manage_methods(method_name = "update_film_aspect_ratio"))


        self.ui_master_data.pushButton_edit_film_aspect_ratio.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_aspect_ratio,
                             list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_film_aspect_ratio,
                                                   self.ui_master_data.lineEdit_film_aspect_ratio.text()),
                                                   (self.ui_master_data.lineEdit_edit_film_aspect_ratio_format,
                                                   self.ui_master_data.lineEdit_film_aspect_ratio_format.text())],
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_aspect_ratio.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_aspect_ratio,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_dimension.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_dimension"))

        self.ui_master_data.pushButton_save_film_dimension.clicked.connect(lambda: self.manage_methods(method_name = "add_film_dimension"))

        self.ui_master_data.pushButton_apply_change_film_dimension.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_dimension"))

        self.ui_master_data.pushButton_delete_film_dimension.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_dimension"))

        self.ui_master_data.pushButton_download_film_dimension.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_dimension,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_dimension.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_dimension,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_dimension.clicked.connect(lambda: self.manage_methods(method_name = "update_film_dimension"))


        self.ui_master_data.pushButton_edit_film_dimension.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_dimension,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_dimension,
                             set_text = self.ui_master_data.lineEdit_film_dimension.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_dimension.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_dimension,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_edition.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_edition"))

        self.ui_master_data.pushButton_save_film_edition.clicked.connect(lambda: self.manage_methods(method_name = "add_film_edition"))

        self.ui_master_data.pushButton_apply_change_film_edition.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_edition"))

        self.ui_master_data.pushButton_delete_film_edition.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_edition"))

        self.ui_master_data.pushButton_download_film_edition.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_edition,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_edition.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_edition,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_edition.clicked.connect(lambda: self.manage_methods(method_name = "update_film_edition"))


        self.ui_master_data.pushButton_edit_film_edition.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_edition,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_edition,
                             set_text = self.ui_master_data.lineEdit_film_edition.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_edition.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_edition,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_version.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_version"))

        self.ui_master_data.pushButton_save_film_version.clicked.connect(lambda: self.manage_methods(method_name = "add_film_version"))

        self.ui_master_data.pushButton_apply_change_film_version.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_version"))

        self.ui_master_data.pushButton_delete_film_version.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_version"))

        self.ui_master_data.pushButton_download_film_version.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_version,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_version.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_version,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_version.clicked.connect(lambda: self.manage_methods(method_name = "update_film_version"))


        self.ui_master_data.pushButton_edit_film_version.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_version,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_version,
                             set_text = self.ui_master_data.lineEdit_film_version.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_version.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_version,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_videonorm.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_videonorm"))

        self.ui_master_data.pushButton_save_film_videonorm.clicked.connect(lambda: self.manage_methods(method_name = "add_film_videonorm"))

        self.ui_master_data.pushButton_apply_change_film_videonorm.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_videonorm"))

        self.ui_master_data.pushButton_delete_film_videonorm.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_videonorm"))

        self.ui_master_data.pushButton_download_film_videonorm.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_videonorm,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_videonorm.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_videonorm,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_videonorm.clicked.connect(lambda: self.manage_methods(method_name = "update_film_videonorm"))


        self.ui_master_data.pushButton_edit_film_videonorm.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_videonorm,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_videonorm,
                             set_text = self.ui_master_data.lineEdit_film_videonorm.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_videonorm.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_videonorm,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_audioformat.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_audioformat"))

        self.ui_master_data.pushButton_save_film_audioformat.clicked.connect(lambda: self.manage_methods(method_name = "add_film_audioformat"))

        self.ui_master_data.pushButton_apply_change_film_audioformat.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_audioformat"))

        self.ui_master_data.pushButton_delete_film_audioformat.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_audioformat"))

        self.ui_master_data.pushButton_download_film_audioformat.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_audioformat,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_audioformat.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_audioformat,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_audioformat.clicked.connect(lambda: self.manage_methods(method_name = "update_film_audioformat"))


        self.ui_master_data.pushButton_edit_film_audioformat.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_audioformat,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_audioformat,
                             set_text = self.ui_master_data.lineEdit_film_audioformat.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_audioformat.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_audioformat,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.pushButton_refresh_film_audio_channel.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_audio_channel"))

        self.ui_master_data.pushButton_save_film_audio_channel.clicked.connect(lambda: self.manage_methods(method_name = "add_film_audio_channel"))

        self.ui_master_data.pushButton_apply_change_film_audio_channel.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_audio_channel"))

        self.ui_master_data.pushButton_delete_film_audio_channel.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_audio_channel"))

        self.ui_master_data.pushButton_download_film_audio_channel.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_audio_channel,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_audio_channel.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_audio_channel,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_audio_channel.clicked.connect(lambda: self.manage_methods(method_name = "update_film_audio_channel"))


        self.ui_master_data.pushButton_edit_film_audio_channel.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_audio_channel,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_audio_channel,
                             set_text = self.ui_master_data.lineEdit_film_audio_channel.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_audio_channel.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_audio_channel,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.pushButton_refresh_film_colour_format.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_colour_format"))

        self.ui_master_data.pushButton_save_film_colour_format.clicked.connect(lambda: self.manage_methods(method_name = "add_film_colour_format"))

        self.ui_master_data.pushButton_apply_change_film_colour_format.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_colour_format"))

        self.ui_master_data.pushButton_delete_film_colour_format.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_colour_format"))

        self.ui_master_data.pushButton_download_film_colour_format.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_colour_format,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_colour_format.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_colour_format,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_colour_format.clicked.connect(lambda: self.manage_methods(method_name = "update_film_colour_format"))


        self.ui_master_data.pushButton_edit_film_colour_format.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_colour_format,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_colour_format,
                             set_text = self.ui_master_data.lineEdit_film_colour_format.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_colour_format.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_colour_format,
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_videoformat.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_videoformat"))

        self.ui_master_data.pushButton_save_film_videoformat.clicked.connect(lambda: self.manage_methods(method_name = "add_film_videoformat"))

        self.ui_master_data.pushButton_apply_change_film_videoformat.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_videoformat"))

        self.ui_master_data.pushButton_delete_film_videoformat.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_videoformat"))

        self.ui_master_data.pushButton_download_film_videoformat.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_videoformat,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_videoformat.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_videoformat,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_videoformat.clicked.connect(lambda: self.manage_methods(method_name = "update_film_videoformat"))


        self.ui_master_data.pushButton_edit_film_videoformat.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_videoformat,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_videoformat,
                             set_text = self.ui_master_data.lineEdit_film_videoformat.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_videoformat.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_videoformat,
                                         clear_widget=True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.pushButton_refresh_film_soundsystem.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_soundsystem"))

        self.ui_master_data.pushButton_save_film_soundsystem.clicked.connect(lambda: self.manage_methods(method_name = "add_film_soundsystem"))

        self.ui_master_data.pushButton_apply_change_film_soundsystem.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_soundsystem"))

        self.ui_master_data.pushButton_delete_film_soundsystem.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_soundsystem"))

        self.ui_master_data.pushButton_download_film_soundsystem.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_film_soundsystem,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_soundsystem.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_film_soundsystem,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_soundsystem.clicked.connect(lambda: self.manage_methods(method_name = "update_film_soundsystem"))


        self.ui_master_data.pushButton_edit_film_soundsystem.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_film_soundsystem,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_soundsystem,
                             set_text = self.ui_master_data.lineEdit_film_soundsystem.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_soundsystem.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_film_soundsystem,
                                         clear_widget=True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.pushButton_refresh_music_studio.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_music_studio"))

        self.ui_master_data.pushButton_save_music_studio.clicked.connect(lambda: self.manage_methods(method_name = "add_music_studio"))

        self.ui_master_data.pushButton_apply_change_music_studio.clicked.connect(lambda: self.manage_methods(method_name = "edit_music_studio"))

        self.ui_master_data.pushButton_delete_music_studio.clicked.connect(lambda: self.manage_methods(method_name = "delete_music_studio"))

        self.ui_master_data.pushButton_download_music_studio.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_music_studio,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_music_studio.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_music_studio,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_music_studio.clicked.connect(lambda: self.manage_methods(method_name = "update_music_studio"))


        self.ui_master_data.pushButton_edit_music_studio.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_music_studio,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_music_studio,
                             set_text = self.ui_master_data.lineEdit_music_studio.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_studio.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_music_studio,
                                         clear_widget=True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.pushButton_refresh_music_spars_code.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_music_spars_code"))

        self.ui_master_data.pushButton_save_music_spars_code.clicked.connect(lambda: self.manage_methods(method_name = "add_music_spars_code"))

        self.ui_master_data.pushButton_apply_change_music_spars_code.clicked.connect(lambda: self.manage_methods(method_name = "edit_music_spars_code"))

        self.ui_master_data.pushButton_delete_music_spars_code.clicked.connect(lambda: self.manage_methods(method_name = "delete_music_spars_code"))

        self.ui_master_data.pushButton_download_music_spars_code.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_music_spars_code,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_music_spars_code.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_music_spars_code,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_music_spars_code.clicked.connect(lambda: self.manage_methods(method_name = "update_music_spars_code"))


        self.ui_master_data.pushButton_edit_music_spars_code.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_music_spars_code,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_music_spars_code,
                             set_text = self.ui_master_data.lineEdit_music_spars_code.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_spars_code.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_music_spars_code,
                                         clear_widget=True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.pushButton_refresh_music_publisher.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_music_publisher"))

        self.ui_master_data.pushButton_save_music_publisher.clicked.connect(lambda: self.manage_methods(method_name = "add_music_publisher"))

        self.ui_master_data.pushButton_apply_change_music_publisher.clicked.connect(lambda: self.manage_methods(method_name = "edit_music_publisher"))

        self.ui_master_data.pushButton_delete_music_publisher.clicked.connect(lambda: self.manage_methods(method_name = "delete_music_publisher"))

        self.ui_master_data.pushButton_download_music_publisher.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget = self.ui_master_data.stackedWidget_music_publisher,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_music_publisher.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget = self.ui_master_data.stackedWidget_music_publisher,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_music_publisher.clicked.connect(lambda: self.manage_methods(method_name = "update_music_publisher"))


        self.ui_master_data.pushButton_edit_music_publisher.clicked.connect(lambda:
            self.handle_edit(index = 1,
                             stack_widget_object = self.ui_master_data.stackedWidget_music_publisher,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_music_publisher,
                             set_text = self.ui_master_data.lineEdit_music_publisher.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_publisher.clicked.connect(lambda:
            self.change_page(index=0,
                                         stack_widget=self.ui_master_data.stackedWidget_music_publisher,
                                         clear_widget=True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.pushButton_refresh_music_style.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_music_style"))

        self.ui_master_data.pushButton_save_music_style.clicked.connect(lambda:
          self.manage_methods(method_name = "add_music_style"))

        self.ui_master_data.pushButton_apply_change_music_style.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_music_style"))

        self.ui_master_data.pushButton_delete_music_style.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_music_style"))

        self.ui_master_data.pushButton_download_music_style.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_music_style,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_music_style.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_music_style,
            column_index = 1))

        self.ui_master_data.pushButton_update_music_style.clicked.connect(lambda:
          self.manage_methods(method_name = "update_music_style"))

        self.ui_master_data.pushButton_edit_music_style.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_music_style,
              lineEdit_object = self.ui_master_data.lineEdit_edit_music_style,
              set_text = self.ui_master_data.lineEdit_music_style.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_style.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_music_style,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.pushButton_refresh_music_label.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_music_label"))

        self.ui_master_data.pushButton_save_music_label.clicked.connect(lambda:
          self.manage_methods(method_name = "add_music_label"))

        self.ui_master_data.pushButton_apply_change_music_label.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_music_label"))

        self.ui_master_data.pushButton_delete_music_label.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_music_label"))

        self.ui_master_data.pushButton_download_music_label.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_music_label,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_music_label.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_music_label,
            column_index = 1))

        self.ui_master_data.pushButton_update_music_label.clicked.connect(lambda:
          self.manage_methods(method_name = "update_music_label"))

        self.ui_master_data.pushButton_edit_music_label.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_music_label,
              lineEdit_object = self.ui_master_data.lineEdit_edit_music_label,
              set_text = self.ui_master_data.lineEdit_music_label.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_label.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_music_label,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.pushButton_refresh_music_distribution.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_music_distribution"))

        self.ui_master_data.pushButton_save_music_distribution.clicked.connect(lambda:
          self.manage_methods(method_name = "add_music_distribution"))

        self.ui_master_data.pushButton_apply_change_music_distribution.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_music_distribution"))

        self.ui_master_data.pushButton_delete_music_distribution.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_music_distribution"))

        self.ui_master_data.pushButton_download_music_distribution.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_music_distribution,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_music_distribution.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_music_distribution,
            column_index = 1))

        self.ui_master_data.pushButton_update_music_distribution.clicked.connect(lambda:
          self.manage_methods(method_name = "update_music_distribution"))

        self.ui_master_data.pushButton_edit_music_distribution.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_music_distribution,
              lineEdit_object = self.ui_master_data.lineEdit_edit_music_distribution,
              set_text = self.ui_master_data.lineEdit_music_distribution.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_distribution.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_music_distribution,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.pushButton_refresh_book_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_book_genre"))

        self.ui_master_data.pushButton_save_book_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "add_book_genre"))

        self.ui_master_data.pushButton_apply_change_book_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_book_genre"))

        self.ui_master_data.pushButton_delete_book_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_book_genre"))

        self.ui_master_data.pushButton_download_book_genre.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_book_genre,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_book_genre.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_book_genre,
            column_index = 1))

        self.ui_master_data.pushButton_update_book_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "update_book_genre"))

        self.ui_master_data.pushButton_edit_book_genre.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_book_genre,
              lineEdit_object = self.ui_master_data.lineEdit_edit_book_genre,
              set_text = self.ui_master_data.lineEdit_book_genre.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_book_genre.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_book_genre,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_book_publisher.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_book_publisher"))

        self.ui_master_data.pushButton_save_book_publisher.clicked.connect(lambda:
          self.manage_methods(method_name = "add_book_publisher"))

        self.ui_master_data.pushButton_apply_change_book_publisher.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_book_publisher"))

        self.ui_master_data.pushButton_delete_book_publisher.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_book_publisher"))

        self.ui_master_data.pushButton_download_book_publisher.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_book_publisher,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_book_publisher.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_book_publisher,
            column_index = 1))

        self.ui_master_data.pushButton_update_book_publisher.clicked.connect(lambda:
          self.manage_methods(method_name = "update_book_publisher"))

        self.ui_master_data.pushButton_edit_book_publisher.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_book_publisher,
              lineEdit_object = self.ui_master_data.lineEdit_edit_book_publisher,
              set_text = self.ui_master_data.lineEdit_book_publisher.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_book_publisher.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_book_publisher,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.pushButton_refresh_book_binding.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_book_binding"))

        self.ui_master_data.pushButton_save_book_binding.clicked.connect(lambda:
          self.manage_methods(method_name = "add_book_binding"))

        self.ui_master_data.pushButton_apply_change_book_binding.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_book_binding"))

        self.ui_master_data.pushButton_delete_book_binding.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_book_binding"))

        self.ui_master_data.pushButton_download_book_binding.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_book_binding,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_book_binding.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_book_binding,
            column_index = 1))

        self.ui_master_data.pushButton_update_book_binding.clicked.connect(lambda:
          self.manage_methods(method_name = "update_book_binding"))

        self.ui_master_data.pushButton_edit_book_binding.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_book_binding,
              lineEdit_object = self.ui_master_data.lineEdit_edit_book_binding,
              set_text = self.ui_master_data.lineEdit_book_binding.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_book_binding.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_book_binding,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_alloy.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_coin_alloy",
                              hide_column = True))

        self.ui_master_data.pushButton_save_coin_alloy.clicked.connect(lambda:
          self.manage_methods(method_name = "add_coin_alloy"))

        self.ui_master_data.pushButton_apply_change_coin_alloy.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_coin_alloy"))

        self.ui_master_data.pushButton_delete_coin_alloy.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_coin_alloy"))

        self.ui_master_data.pushButton_download_coin_alloy.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_coin_alloy,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_coin_alloy.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_coin_alloy,
            column_index = 1))

        self.ui_master_data.pushButton_update_coin_alloy.clicked.connect(lambda:
          self.manage_methods(method_name = "update_coin_alloy"))

        self.ui_master_data.pushButton_edit_coin_alloy.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_coin_alloy,
              lineEdit_object = self.ui_master_data.lineEdit_edit_coin_alloy,
              set_text = self.ui_master_data.lineEdit_coin_alloy.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_alloy.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_coin_alloy,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_currency.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_coin_currency"))

        self.ui_master_data.pushButton_save_coin_currency.clicked.connect(lambda:
          self.manage_methods(method_name = "add_coin_currency"))

        self.ui_master_data.pushButton_apply_change_coin_currency.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_coin_currency"))

        self.ui_master_data.pushButton_delete_coin_currency.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_coin_currency"))

        self.ui_master_data.pushButton_download_coin_currency.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_coin_currency,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_coin_currency.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_coin_currency,
            column_index = 1))

        self.ui_master_data.pushButton_update_coin_currency.clicked.connect(lambda:
          self.manage_methods(method_name = "update_coin_currency"))

        self.ui_master_data.pushButton_edit_coin_currency.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_coin_currency,
              lineEdit_object = self.ui_master_data.lineEdit_edit_coin_currency,
              set_text = self.ui_master_data.lineEdit_coin_currency.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_currency.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_coin_currency,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_type.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_coin_type"))

        self.ui_master_data.pushButton_save_coin_type.clicked.connect(lambda:
          self.manage_methods(method_name = "add_coin_type"))

        self.ui_master_data.pushButton_apply_change_coin_type.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_coin_type"))

        self.ui_master_data.pushButton_delete_coin_type.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_coin_type"))

        self.ui_master_data.pushButton_download_coin_type.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_coin_type,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_coin_type.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_coin_type,
            column_index = 1))

        self.ui_master_data.pushButton_update_coin_type.clicked.connect(lambda:
          self.manage_methods(method_name = "update_coin_type"))

        self.ui_master_data.pushButton_edit_coin_type.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_coin_type,
              lineEdit_object = self.ui_master_data.lineEdit_edit_coin_type,
              set_text = self.ui_master_data.lineEdit_coin_type.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_type.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_coin_type,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_manufacturing_procedure.clicked.connect(lambda:
            self.manage_methods(method_name = "fetch_all_coin_manufacturing_procedure"))

        self.ui_master_data.pushButton_save_coin_manufacturing_procedure.clicked.connect(lambda:
            self.manage_methods(method_name = "add_coin_manufacturing_procedure"))

        self.ui_master_data.pushButton_apply_change_coin_manufacturing_procedure.clicked.connect(lambda:
            self.manage_methods(method_name = "edit_coin_manufacturing_procedure"))

        self.ui_master_data.pushButton_delete_coin_manufacturing_procedure.clicked.connect(lambda:
            self.manage_methods(method_name = "delete_coin_manufacturing_procedure"))

        self.ui_master_data.pushButton_download_coin_manufacturing_procedure.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                column_index = 1))
        self.ui_master_data.pushButton_cancel_update_coin_manufacturing_procedure.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                column_index = 1))

        self.ui_master_data.pushButton_update_coin_manufacturing_procedure.clicked.connect(lambda:
            self.manage_methods(method_name = "update_coin_manufacturing_procedure"))


        self.ui_master_data.pushButton_edit_coin_manufacturing_procedure.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure,
                                          self.ui_master_data.lineEdit_coin_manufacturing_procedure.text()),
                                          (self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure_shortcut,
                                          self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut.text())],
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_manufacturing_procedure.clicked.connect(lambda:
            self.change_page(
                index=0,
                stack_widget=self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                clear_widget=True,
                current_obj_clear_selection=True,
                delete_selected_id = True,
                current_obj_clear=True))


        ###################################################
        #################  Coin Degree of Preservation  ###
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_degree_preservation.clicked.connect(lambda:
            self.manage_methods(method_name = "fetch_all_coin_degree_preservation"))

        self.ui_master_data.pushButton_save_coin_degree_preservation.clicked.connect(lambda:
            self.manage_methods(method_name = "add_coin_degree_preservation"))

        self.ui_master_data.pushButton_apply_change_coin_degree_preservation.clicked.connect(lambda:
            self.manage_methods(method_name = "edit_coin_degree_preservation"))

        self.ui_master_data.pushButton_delete_coin_degree_preservation.clicked.connect(lambda:
            self.manage_methods(method_name = "delete_coin_degree_preservation"))

        self.ui_master_data.pushButton_download_coin_degree_preservation.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_coin_degree_preservation,
                column_index = 1))
        self.ui_master_data.pushButton_cancel_update_coin_degree_preservation.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_coin_degree_preservation,
                column_index = 1))

        self.ui_master_data.pushButton_update_coin_degree_preservation.clicked.connect(lambda:
            self.manage_methods(method_name = "update_coin_degree_preservation"))


        self.ui_master_data.pushButton_edit_coin_degree_preservation.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_coin_degree_preservation,
                list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_coin_degree_preservation,
                                          self.ui_master_data.lineEdit_coin_degree_preservation.text()),
                                          (self.ui_master_data.lineEdit_edit_coin_degree_preservation_shortcut,
                                          self.ui_master_data.lineEdit_coin_degree_preservation_shortcut.text())],
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_degree_preservation.clicked.connect(lambda:
            self.change_page(
                index=0,
                stack_widget=self.ui_master_data.stackedWidget_coin_degree_preservation,
                clear_widget=True,
                current_obj_clear_selection=True,
                delete_selected_id = True,
                current_obj_clear=True))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.ui_master_data.pushButton_refresh_coin_mint.clicked.connect(lambda:
            self.manage_methods(method_name = "fetch_all_coin_mint",
                                hide_column = True))

        self.ui_master_data.pushButton_save_coin_mint.clicked.connect(lambda:
            self.manage_methods(method_name = "add_coin_mint",
                                hide_column = True))

        self.ui_master_data.pushButton_apply_change_coin_mint.clicked.connect(lambda:
            self.manage_methods(method_name = "edit_coin_mint",
                                hide_column = True))

        self.ui_master_data.pushButton_delete_coin_mint.clicked.connect(lambda:
            self.manage_methods(method_name = "delete_coin_mint",
                                hide_column = True))

        self.ui_master_data.pushButton_download_coin_mint.clicked.connect(lambda:
            self.change_page(
                index = 2,
                stack_widget = self.ui_master_data.stackedWidget_coin_mint,
                column_index = 1))
        self.ui_master_data.pushButton_cancel_update_coin_mint.clicked.connect(lambda:
            self.change_page(
                index = 0,
                stack_widget = self.ui_master_data.stackedWidget_coin_mint,
                column_index = 1))

        self.ui_master_data.pushButton_update_coin_mint.clicked.connect(lambda:
            self.manage_methods(method_name = "update_coin_mint"))


        self.ui_master_data.pushButton_edit_coin_mint.clicked.connect(lambda:
            self.handle_edit(
                index = 1,
                stack_widget_object = self.ui_master_data.stackedWidget_coin_mint,
                list_tuple_widget_text = [(self.ui_master_data.lineEdit_edit_coin_mint,
                                          self.ui_master_data.lineEdit_coin_mint.text()),
                                          (self.ui_master_data.lineEdit_edit_coin_mint_mark,
                                          self.ui_master_data.lineEdit_coin_mint_mark.text())],
                set_focus = True,
                clear_widget = True,
                load_data = False))

        self.ui_master_data.pushButton_cancel_edit_coin_mint.clicked.connect(lambda:
            self.change_page(
                index=0,
                stack_widget=self.ui_master_data.stackedWidget_coin_mint,
                clear_widget=True,
                current_obj_clear_selection=True,
                delete_selected_id = True,
                current_obj_clear=True))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.pushButton_refresh_videogame_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "fetch_all_videogame_genre",
                                hide_column = True))

        self.ui_master_data.pushButton_save_videogame_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "add_videogame_genre",
                                hide_column = True))

        self.ui_master_data.pushButton_apply_change_videogame_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "edit_videogame_genre",
                                hide_column = True))

        self.ui_master_data.pushButton_delete_videogame_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "delete_videogame_genre",
                                hide_column = True))

        self.ui_master_data.pushButton_download_videogame_genre.clicked.connect(lambda:
          self.change_page(
            index = 2,
            stack_widget = self.ui_master_data.stackedWidget_videogame_genre,
            column_index = 1))

        self.ui_master_data.pushButton_cancel_update_videogame_genre.clicked.connect(lambda:
          self.change_page(index = 0,
            stack_widget = self.ui_master_data.stackedWidget_videogame_genre,
            column_index = 1))

        self.ui_master_data.pushButton_update_videogame_genre.clicked.connect(lambda:
          self.manage_methods(method_name = "update_videogame_genre",
                                hide_column = True))

        self.ui_master_data.pushButton_edit_videogame_genre.clicked.connect(lambda:
            self.handle_edit(
              index = 1,
              stack_widget_object = self.ui_master_data.stackedWidget_videogame_genre,
              lineEdit_object = self.ui_master_data.lineEdit_edit_videogame_genre,
              set_text = self.ui_master_data.lineEdit_videogame_genre.text(),
              set_focus = True,
              clear_widget = True,
              load_data = False))

        self.ui_master_data.pushButton_cancel_edit_videogame_genre.clicked.connect(lambda:
            self.change_page(
              index=0,
              stack_widget=self.ui_master_data.stackedWidget_videogame_genre,
              clear_widget=True,
              current_obj_clear_selection = True,
              delete_selected_id = True,
              current_obj_clear=True))

        return

    def open_context_menu(self,
                          position,
                          tree_view_obj,
                          pushbutton_object_edit,
                          pushbutton_object_delete):
        '''
           SUMMARY
           =======
           This method is used to create Custom context menu for QTreeView.

           EXTENTED DISCRIPTION
           ====================
           This function not only uploads files, but also access a HTTP Basic authenticated page.

           PARAMETERS
           ==========
           :position:                   The current positions of the mouse point is important.

           :tree_view_obj:              We need a QTreeView-widget

           :pushbutton_object_edit:     We expect a QPushButton for the change
                                        of records

           :pushbutton_object_delete:   We also need a QPushButton for the delete
                                        of records

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        indexes = tree_view_obj.selectedIndexes()

        if len(indexes) > 0:

            delete_action = QAction(QIcon(self._kwargs.get('image_path_collection').delete_default), self.tr("Delete"), self)
            delete_action.setObjectName("delete_action_master_data")
            delete_action.triggered.connect(pushbutton_object_delete.click)

            edit_action = QAction(QIcon(self._kwargs.get('image_path_collection').edit_default), self.tr("Edit"), self)
            edit_action.triggered.connect(pushbutton_object_edit.click)


            menu = QMenu()
            menu.setObjectName("context_menu_master_data")
            menu.addAction(edit_action)
            menu.addAction(delete_action)

            menu.exec_(tree_view_obj.viewport().mapToGlobal(position))

        return

    def init_textChanged_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set signal and slots of textChanged for QLineEdit()-objects.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize textChanged-signal for QLineEdit")

        ###################################################
        ################  General Region  #################
        ###################################################
        self.ui_master_data.lineEdit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_region,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_region,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        ################  General Country  ################
        ###################################################
        self.ui_master_data.lineEdit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_country,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_country,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        ################  General Language  ###############
        ###################################################
        self.ui_master_data.lineEdit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_language,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_language,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  General Age release  ###########
        ###################################################
        self.ui_master_data.lineEdit_general_age_release.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_age_release,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_general_age_release.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_age_release,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        ################  Person Nationality  ##############
        ###################################################
        self.ui_master_data.lineEdit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_nationality,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_nationality,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_film_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_genre,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_film_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_genre,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_award,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_award,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_music_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_music_genre,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_music_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_music_genre,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_gender,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_gender,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_salutation,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_salutation,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_title,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_title,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_eye_color,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_eye_color,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_hair_color,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_hair_color,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_religion,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_religion,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_relationship_status,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_relationship_status,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_relationship,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_relationship,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_general_media_type.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_media_type,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_general_media_type.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_media_type,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_general_packaging.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_packaging,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_general_packaging.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_packaging,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Film Production Company  #######
        ###################################################
        self.ui_master_data.lineEdit_film_production_company.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_production_company,
                                     max_length = 100))

        self.ui_master_data.lineEdit_edit_film_production_company.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_production_company,
                                     max_length = 100,
                                     check_selected_id = False))

        ###################################################
        #################  Film Rentals  ##################
        ###################################################
        self.ui_master_data.lineEdit_film_rentals.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_rentals,
                                     max_length = 200))

        self.ui_master_data.lineEdit_edit_film_rentals.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_rentals,
                                     max_length = 200,
                                     check_selected_id = False))

        ###################################################
        #################  Film Distribution  #############
        ###################################################
        self.ui_master_data.lineEdit_film_distribution.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_distribution,
                                     max_length = 200))

        self.ui_master_data.lineEdit_edit_film_distribution.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_distribution,
                                     max_length = 200,
                                     check_selected_id = False))

        ###################################################
        #################  Film Studio  ###################
        ###################################################
        self.ui_master_data.lineEdit_film_studio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_studio,
                                     max_length = 200))

        self.ui_master_data.lineEdit_edit_film_studio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_studio,
                                     max_length = 200,
                                     check_selected_id = False))

        ###################################################
        #################  Film Resolution  ###############
        ###################################################
        self.ui_master_data.lineEdit_film_resolution.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_resolution,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_resolution.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_resolution,
                                     max_length = 20,
                                     check_selected_id = False))

        self.ui_master_data.lineEdit_film_presenting_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_presenting_format,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_film_presenting_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_presenting_format,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  General Manufacturer  ##########
        ###################################################
        self.ui_master_data.lineEdit_general_manufacturer.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_manufacturer,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_general_manufacturer.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_manufacturer,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Film Regional Code   ###########
        ###################################################
        self.ui_master_data.lineEdit_film_regional_code.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_regional_code,
                                     max_length = 5))

        self.ui_master_data.lineEdit_edit_film_regional_code.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_regional_code,
                                     max_length = 5,
                                     check_selected_id = False))

        self.ui_master_data.lineEdit_film_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_region,
                                     max_length = 500,
                                     check_selected_id = False))

        self.ui_master_data.lineEdit_edit_film_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_region,
                                     max_length = 500,
                                     check_selected_id = False))

        ###################################################
        #################  Film Aspect Ratio   ############
        ###################################################
        self.ui_master_data.lineEdit_film_aspect_ratio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_aspect_ratio,
                                     max_length = 6))

        self.ui_master_data.lineEdit_edit_film_aspect_ratio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_aspect_ratio,
                                     max_length = 6,
                                     check_selected_id = False))

        self.ui_master_data.lineEdit_film_aspect_ratio_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_aspect_ratio_format,
                                     max_length = 60,
                                     check_selected_id = False))

        self.ui_master_data.lineEdit_edit_film_aspect_ratio_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_aspect_ratio_format,
                                     max_length = 60,
                                     check_selected_id = False))

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
        self.ui_master_data.lineEdit_film_dimension.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_dimension,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_dimension.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_dimension,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Edition    ################
        ###################################################
        self.ui_master_data.lineEdit_film_edition.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_edition,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_edition.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_edition,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Version    ################
        ###################################################
        self.ui_master_data.lineEdit_film_version.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_version,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_version.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_version,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
        self.ui_master_data.lineEdit_film_videonorm.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_videonorm,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_videonorm.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_videonorm,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
        self.ui_master_data.lineEdit_film_audioformat.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_audioformat,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_film_audioformat.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_audioformat,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Film Audio Channel    ##########
        ###################################################
        self.ui_master_data.lineEdit_film_audio_channel.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_audio_channel,
                                     max_length = 50))

        self.ui_master_data.lineEdit_edit_film_audio_channel.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_audio_channel,
                                     max_length = 50,
                                     check_selected_id = False))

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
        self.ui_master_data.lineEdit_film_colour_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_colour_format,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_colour_format.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_colour_format,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
        self.ui_master_data.lineEdit_film_videoformat.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_videoformat,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_videoformat.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_videoformat,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Film Soundsystem   #############
        ###################################################
        self.ui_master_data.lineEdit_film_soundsystem.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_soundsystem,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_film_soundsystem.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_soundsystem,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Music Studio    ################
        ###################################################
        self.ui_master_data.lineEdit_music_studio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_music_studio,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_music_studio.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_music_studio,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
        self.ui_master_data.lineEdit_music_spars_code.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_music_spars_code,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_music_spars_code.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_music_spars_code,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Music Publisher    #############
        ###################################################
        self.ui_master_data.lineEdit_music_publisher.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_music_publisher,
                                     max_length = 20))

        self.ui_master_data.lineEdit_edit_music_publisher.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_music_publisher,
                                     max_length = 20,
                                     check_selected_id = False))

        ###################################################
        #################  Music Style    #################
        ###################################################
        self.ui_master_data.lineEdit_music_style.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_music_style,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_music_style.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_music_style,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Music Label    #################
        ###################################################
        self.ui_master_data.lineEdit_music_label.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_music_label,
              max_length = 100))

        self.ui_master_data.lineEdit_edit_music_label.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_music_label,
              max_length = 100,
              check_selected_id = False))

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
        self.ui_master_data.lineEdit_music_distribution.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_music_distribution,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_music_distribution.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_music_distribution,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Book Genre    ##################
        ###################################################
        self.ui_master_data.lineEdit_book_genre.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_book_genre,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_book_genre.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_book_genre,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
        self.ui_master_data.lineEdit_book_publisher.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_book_publisher,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_book_publisher.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_book_publisher,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Book Binding    ################
        ###################################################
        self.ui_master_data.lineEdit_book_binding.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_book_binding,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_book_binding.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_book_binding,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
        self.ui_master_data.lineEdit_coin_alloy.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_coin_alloy,
              max_length = 20))

        self.ui_master_data.lineEdit_edit_coin_alloy.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_coin_alloy,
              max_length = 20,
              check_selected_id = False))

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
        self.ui_master_data.lineEdit_coin_currency.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_coin_currency,
              max_length = 100))

        self.ui_master_data.lineEdit_edit_coin_currency.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_coin_currency,
              max_length = 100,
              check_selected_id = False))

        ###################################################
        #################  Coin Type     ##################
        ###################################################
        self.ui_master_data.lineEdit_coin_type.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_coin_type,
              max_length = 200))

        self.ui_master_data.lineEdit_edit_coin_type.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_coin_type,
              max_length = 200,
              check_selected_id = False))

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
        self.ui_master_data.lineEdit_videogame_genre.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_videogame_genre,
              max_length = 150))

        self.ui_master_data.lineEdit_edit_videogame_genre.textChanged.connect(lambda:
            self.handle_text_changed(
              line_edit_obj = self.ui_master_data.lineEdit_edit_videogame_genre,
              max_length = 150,
              check_selected_id = False))

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
        self.ui_master_data.lineEdit_coin_manufacturing_procedure.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                max_length = 200))

        self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure,
                max_length = 200,
                check_selected_id = False))

        self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut,
                max_length = 50))

        self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure_shortcut.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure_shortcut,
                max_length = 50,
                check_selected_id = False))

        ###################################################
        #################  Coin Degree of Preservation  ###
        ###################################################
        self.ui_master_data.lineEdit_coin_degree_preservation.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_degree_preservation,
                max_length = 200))

        self.ui_master_data.lineEdit_edit_coin_degree_preservation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_coin_degree_preservation,
                max_length = 200,
                check_selected_id = False))

        self.ui_master_data.lineEdit_coin_degree_preservation_shortcut.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_degree_preservation_shortcut,
                max_length = 50))

        self.ui_master_data.lineEdit_edit_coin_degree_preservation_shortcut.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_edit_coin_degree_preservation_shortcut,
                max_length = 50,
                check_selected_id = False))

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
        self.ui_master_data.lineEdit_coin_mint.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_mint,
                max_length = 200))

        self.ui_master_data.lineEdit_edit_coin_mint.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_coin_mint,
                max_length = 200,
                check_selected_id = False))

        self.ui_master_data.lineEdit_coin_mint_mark.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_coin_mint_mark,
                max_length = 50))

        self.ui_master_data.lineEdit_edit_coin_mint_mark.textChanged.connect(lambda:
            self.handle_text_changed(
                line_edit_obj = self.ui_master_data.lineEdit_edit_coin_mint_mark,
                max_length = 50,
                check_selected_id = False))

        return

    def init_item_clicked_signal_tree_widget(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots of clicked for QTreeWidget()-object(s).

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize textChanged-signal for QTreeWidget")

        self.ui_master_data.treeView_navigation.selectionModel().selectionChanged.connect(lambda new_index:
            self.change_page(new_index = new_index,
                             stack_widget = self.ui_master_data.stackedWidget,
                             column_index = 1,
                             load_data = True))

        return

    def set_cursor_position(self,
                            widget,
                            pos):
        '''
           SUMMARY
           =======
           This method is used to set the cursor position.

           PARAMETERS
           ==========
           :widget (QLineEdit):     We expect a given QLineEdit()-Widget where we set the cursor position.

           :pos (int):              We need a int value to set the cursor position.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the cursor position in QLineEdit()")

        if isinstance(widget, QLineEdit):
            widget.setCursorPosition(pos)

        return


    def handle_text_changed(self,
                            line_edit_obj,
                            max_length,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False,
                            check_selected_id = True):
        '''
           SUMMARY
           =======
           This method is used to control the input in given QLineEdit().

           PARAMETERS
           ==========
           :line_edit_obj (QLineEdit):              Here, we want a QWidget()-object: QLineEdit.
                                                    Well, we need this object to get the current position
                                                    of the cursor.

           :max_length (int):                       This keyword-argument gets an integer for setting a maximum length
                                                    of a given text.


           :change_windows_title (bool):            In this keyword argument we except a special value: boolean value.
                                                    By default, the value is set to False, because we don't want the
                                                    window title to change.

           :allow_trailing_spaces (bool):           We except a boolean value. By default, its set to True.
                                                    True means that spaces between the each words are allowed.
                                                    It is very rare that we want to ban trailing space.

           :allow_only_number (bool):               We except a boolean value. When we get True we
                                                    want to know we shoukd allow only numbers. By default
                                                    the value is set to False, because we want the user to
                                                    enter all characters.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Control the input")

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text=unicode(line_edit_obj.text()),
                                                                      pos = line_edit_obj.cursorPosition(),
                                                                      allow_trailing_spaces=allow_trailing_spaces,
                                                                      allow_only_number=allow_only_number,
                                                                      max_length=max_length)



        self.set_text(widget = line_edit_obj, text = get_text)
        self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if get_text:
            '''
                No, the string isn't empty
            '''
            self.set_text(widget = line_edit_obj, text = get_text)
            self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

            # self.set_enabled_widget(signle_widget=push_button_obj_save,
            #                              boolean_value=True)

        else:
            '''
                Bad news, the string is emptyx.
            '''

            self.current_selected_id = None

        #     self.set_enabled_widget(signle_widget=push_button_obj_save,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                  boolean_value=False)


        # if check_selected_id:

        #     '''
        #         Its possible that the variable named (self.selected_be_edited_id) is still
        #         not empty. For instance the user clicked on a TreeView()-object. In
        #         this case special pushButton()-obejcts must be enabled oder disabled.
        #     '''
        #     if not self.selected_be_edited_id is None:


        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=True)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=True)

        #     else:

        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=False)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=False)

        return

    def clear_widget_object_content(self,
                                    list_widget_object = None,
                                    widget_object = None):
        '''
           SUMMARY
           =======
           Clear the content of given widget-objects, that supports clear()-method.

           PARAMETERS
           ==========
           :list_widget_object (list):      Here, we get a list of widget-objects.

           :widget_object (QLineEdit or
                           QTextEdit or
                           QPlainTextEdit): We get a single widget object.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Clear the content of given widgets")

        print ""
        print "MasterData_Window clear_widget_object_content is called successfully"
        print "MasterData_Window in clear_widget_object_content / list_widget_object: -> \n {}".format(list_widget_object)
        print "MasterData_Window in clear_widget_object_content / widget_object: -> \n {}".format(widget_object)

        if isinstance(list_widget_object, list):

            for widget in self.generator_list_widget_object(list_widget_object):
                print "widget.objectName(): {}".format(widget.objectName())
                widget.clear()

        if not widget_object is None:
            widget_object.clear()

        return

    def handle_edit(self,
                    index,
                     stack_widget_object,
                     set_text = None,
                     lineEdit_object = None,
                     list_tuple_widget_text = None,
                     new_index = None,
                     set_focus = False,
                     clear_widget = True,
                     load_data = True,
                     current_obj_clear_selection = False,
                     current_obj_clear = False):
        '''
           SUMMARY
           =======
           This method is just a intermediate function.

           EXTENTED DISCRIPTION
           ====================
           We need this metho for duck typing. Here I want to check if variable namend
           'self.selected_be_edited_id' and 'self.current_selected_id' are really an integer.

           PARAMETERS
           ==========
           :index (int):                            The given index changes the view of stacked widget

           :new_index (int):                        The new on of the given index changes the view of stacked widget

           :stack_widget_object (QStackedWidget):   We need a QStackedWidget object

           :tree_view_object (QTreeView):           We also need a QTreeView object

           :column_index (int):                     For QTreeView object we expect a index for column

           :set_text (QString):                     We get a text - context of QLineEdit object

           :lineEdit_object (QLineEdit):            We need a QLineEdit to set the text there

           :set_focus (bool):                       When we should set the focus in given QLineEdit object, we set to True, otherwise to False

           :clear_widget (bool):                    When we should clear the given QLineEdit object, we set to True, otherwise to False

           :load_data (bool):                       When we want the program to load records again, one the page of stacked widget
                                                    has been changed we set to True, otherwise to False

           :current_obj_clear_selection (bool):     Its True, we clear the QTreeView selection

           :current_obj_clear (bool)                Its True we just clear all widget objects in current pagp, except QTreeView.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("<use the intermediate function")

        try:

            #   When the user clicks QTreeView as usual the ID of the record is saved
            #   in self.selected_be_edited_id and self.selected_be_edited_id.
            #   But when the user doesn't do that and clicks the
            #   edit button directly, we will generate an error message from the TypeError execption,
            #   otjerwise we will scroll through the StackWidget.
            int(self.selected_be_edited_id)
            int(self.current_selected_id)

            self.change_page(index = index,
                             new_index = new_index,
                             stack_widget = stack_widget_object,
                             set_text = set_text,
                             lineEdit_object = lineEdit_object,
                             list_tuple_widget_text = list_tuple_widget_text,
                             set_focus = set_focus,
                             clear_widget = clear_widget,
                             load_data = load_data,
                             current_obj_clear_selection = current_obj_clear_selection,
                             current_obj_clear = current_obj_clear)

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or\
            not desired_trace.find("int() argument must be a string or a number, not") == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to edit an item. You must select an item to edit.")

                self.create_generally_crritcal_msg(err_title = title_text,
                                                   err_msg = conn_err,
                                                   icon = self._kwargs.get('image_path_collection').warning_48x48_default,
                                                   set_flag = True)

        return

    def handle_selection_changed(self,
                                 new_index,
                                 old_index,
                                 widget):
        '''
           SUMMARY
           =======
           TThis method is used to get selected item.

           PARAMETERS
           ==========
           :new_index (QItemSelection): #selectionChanged() sends new QItemSelection

           :old_index (QItemSelection): #selectionChanged() sends old QItemSelection

           :widget (QLineEdit / List):  We get a single QLineEdit or a listw of QLineEdit.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("<use the intermediate function")

        count_index = 1

        try: #if QItemSelection
            #   We need access to special date of selected item#.
            #   By using indexes() with the number in square brackets we can
            #   extract it. With data() we automatically convert a QVariant back to a
            #   Python object
            self.selected_be_edited_id = new_index.indexes()[0].data() #    We want the selected ID of the record

            self.current_selected_id = new_index.indexes()[0].data()

            if isinstance(widget, list):

                for single_widget in widget:

                    single_widget.setText(new_index.indexes()[count_index].data()) # We want fill the given widget with record.

                    count_index += 1

            else:

                widget.setText(new_index.indexes()[1].data()) # We want fill the given widget with record.

        except Exception as er: #if qModelIndex
            pass

        return

    def generator_list_widget_object(self,
                                     list_widget_object):
        '''
           SUMMARY
           =======
           This generator take a list of widgets and yield its back.

           PARAMETERS
           ==========
           :list_widget_object (lst): We need a list.
        '''
        logger.info("Yield each element back")

        for widget in list_widget_object:

            yield widget

    def handle_timer(self,
                     timer_widget = None,
                     text = None,
                     show_wait_message = False,
                     show_pushbutton = False):

        if show_wait_message:
            self.create_wait_window(text = text, show_pushbutton = show_pushbutton, parent = self)

        timer_widget.stop()
        timer_widget.start()

        return

    def filter_update(self,
                      **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        self.task_thread = QThread()
        self.task_thread.work = TaskFilterUpdate(parent = None,
                                                **kwargs)

        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        self.task_thread.work.create_generally_information_msg_signal.connect(
          lambda
          title,
          msg,
          detail_msg,
          yes_no_button,
          only_yes_button,
          line_edit_object_get_focus,
          icon,
          set_flag:
          self.create_generally_information_msg(
            title = title,
            msg = msg,
            detail_msg = detail_msg,
            yes_no_button = yes_no_button,
            only_yes_button = only_yes_button,
            line_edit_object_get_focus = line_edit_object_get_focus,
            icon = icon,
            set_flag = set_flag))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_thread)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def manage_methods(self,
                       method_name,
                       hide_column = False,
                       force_fetch_all = False):

        dict_methods = {
        ###################################################
        #################  General Region    ##############
        ###################################################
                        "fetch_all_general_region": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_region,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_REGION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_region,
                            widget_label = self.ui_master_data.label_general_region_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_region,
                            show_messagebox = True,
                            force_fetch_all = force_fetch_all,
                            category = 'general_region'),

                        "add_general_region": lambda: self.add_record(
                            general_region = unicode(self.ui_master_data.lineEdit_general_region.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_region,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_region,
                            label_object = self.ui_master_data.label_general_region_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_region,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_region'),

                        "edit_general_region": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_region = unicode(self.ui_master_data.lineEdit_edit_general_region.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_region,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_region,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_region,
                            stack_widget = self.ui_master_data.stackedWidget_general_region,
                            label_object = self.ui_master_data.label_general_region_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_region,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_region'),

                        "delete_general_region": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_region,
                            line_edit_obj = self.ui_master_data.lineEdit_general_region,
                            label_object = self.ui_master_data.label_general_region_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_region,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_region,
                            set_flush = True,
                            category = 'general_region'),

                        "update_general_region": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                         set_page = True,
                                                                         widget_stack_widget = self.ui_master_data.stackedWidget_general_region,
                                                                         page_index = 0,
                                                                         widget_line_edit = self.ui_master_data.lineEdit_general_region,
                                                                         widget_label = self.ui_master_data.label_general_region_counter,
                                                                         widget_tree_view = self.ui_master_data.tree_view_general_region,
                                                                         notify_on_widget = self.ui_master_data.textEdit_notify_general_region,
                                                                         certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_region,
                                                                         sort_column = 1,
                                                                         category = 'general_region'),

        ###################################################
        #################  General Country    #############
        ###################################################
                        "fetch_all_general_country": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_country,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_COUNTRY,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_country,
                            widget_label = self.ui_master_data.label_general_country_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_country,
                            show_messagebox = True,
                            force_fetch_all = force_fetch_all,
                            category = 'general_country'),

                        "add_general_country": lambda: self.add_record(
                            general_country = unicode(self.ui_master_data.lineEdit_general_country.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_country,
                            label_object = self.ui_master_data.label_general_country_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_country,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_country'),

                        "edit_general_country": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_country = unicode(self.ui_master_data.lineEdit_edit_general_country.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_country,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_production_country,
                            stack_widget = self.ui_master_data.stackedWidget_general_country,
                            label_object = self.ui_master_data.label_general_country_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_country,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_country'),

                        "delete_general_country": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_country,
                            line_edit_obj = self.ui_master_data.lineEdit_general_country,
                            label_object = self.ui_master_data.label_general_country_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_country,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_production_country,
                            set_flush = True,
                            category = 'general_country'),

        ###################################################
        #################  General Language    ############
        ###################################################
                        "fetch_all_general_language": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_language,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_LANGUAGE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_language,
                            widget_label = self.ui_master_data.label_general_language_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_language,
                            show_messagebox = True,
                            category = 'general_language'),

                        "add_general_language": lambda: self.add_record(
                            general_language = unicode(self.ui_master_data.lineEdit_general_language.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_language,
                            label_object = self.ui_master_data.label_general_language_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_language,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_language'),

                        "edit_general_language": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_language = unicode(self.ui_master_data.lineEdit_edit_general_language.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_language,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_language,
                            stack_widget = self.ui_master_data.stackedWidget_general_language,
                            label_object = self.ui_master_data.label_general_language_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_language,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_language'),

                        "delete_general_language": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_language,
                            line_edit_obj = self.ui_master_data.lineEdit_general_language,
                            label_object = self.ui_master_data.label_general_language_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_language,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_language,
                            set_flush = True,
                            category = 'general_language'),

                        "update_general_language": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                          set_page = True,
                                                                          widget_stack_widget = self.ui_master_data.stackedWidget_general_language,
                                                                          page_index = 0,
                                                                          widget_line_edit = self.ui_master_data.lineEdit_general_language,
                                                                          widget_label = self.ui_master_data.label_general_language_counter,
                                                                          widget_tree_view = self.ui_master_data.tree_view_general_language,
                                                                          notify_on_widget = self.ui_master_data.textEdit_notify_general_language,
                                                                          certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_language,
                                                                          sort_column = 1,
                                                                          category = 'general_language'),

        ###################################################
        #################  General Age release  ###########
        ###################################################
                        "fetch_all_general_age_release": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_age_release,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_AGE_RELEASE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_age_release,
                            widget_label = self.ui_master_data.label_general_age_release_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_age_release,
                            show_messagebox = True,
                            category = 'general_age_release'),

                        "add_general_age_release": lambda: self.add_record(
                            general_age_release = unicode(self.ui_master_data.lineEdit_general_age_release.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_age_release,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_age_release,
                            label_object = self.ui_master_data.label_general_age_release_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_age_release,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_age_release'),

                        "edit_general_age_release": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_age_release = unicode(self.ui_master_data.lineEdit_edit_general_age_release.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_age_release,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_age_release,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_age_release,
                            stack_widget = self.ui_master_data.stackedWidget_general_age_release,
                            label_object = self.ui_master_data.label_general_age_release_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_age_release,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_age_release'),

                        "delete_general_age_release": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_age_release,
                            line_edit_obj = self.ui_master_data.lineEdit_general_age_release,
                            label_object = self.ui_master_data.label_general_age_release_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_age_release,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_age_release,
                            set_flush = True,
                            category = 'general_age_release'),

                        "update_general_age_release": lambda: self.fetch_all(
                            work_mode = 'fetch_online_data',
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_general_age_release,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_general_age_release,
                            widget_label = self.ui_master_data.label_general_age_release_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_age_release,
                            notify_on_widget = self.ui_master_data.textEdit_notify_general_age_release,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_age_release,
                            sort_column = 1,
                            category = 'general_age_release'),

        ###################################################
        #################  Person Nationality    ##########
        ###################################################
                        "fetch_all_person_nationality": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_nationality,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_NATIONALITY,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_nationality,
                            widget_label = self.ui_master_data.label_person_nationality_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                            show_messagebox = True,
                            category = 'person_nationality'),

                        "add_person_nationality": lambda: self.add_record(
                            person_nationality = unicode(self.ui_master_data.lineEdit_person_nationality.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_nationality,
                            label_object = self.ui_master_data.label_person_nationality_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_nationality'),

                        "edit_person_nationality": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_nationality = unicode(self.ui_master_data.lineEdit_edit_person_nationality.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_nationality,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_nationality,
                            stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                            label_object = self.ui_master_data.label_person_nationality_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_nationality'),

                        "delete_person_nationality": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_nationality,
                            line_edit_obj = self.ui_master_data.lineEdit_person_nationality,
                            label_object = self.ui_master_data.label_person_nationality_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_nationality,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_nationality,
                            set_flush = True,
                            category = 'person_nationality'),

                        "update_person_nationality": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_person_nationality,
                                                                            widget_label = self.ui_master_data.label_person_nationality_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_person_nationality,
                                                                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_nationality,
                                                                            sort_column = 1,
                                                                            category = 'person_nationality'),

        ###################################################
        #################  Film Genre    ##################
        ###################################################
                        "fetch_all_film_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_genre,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_GENRE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_genre,
                            widget_label = self.ui_master_data.label_film_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_genre,
                            show_messagebox = True,
                            category = 'film_genre'),

                        "add_film_genre": lambda: self.add_record(
                            film_genre = unicode(self.ui_master_data.lineEdit_film_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_genre,
                            label_object = self.ui_master_data.label_film_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_genre'),

                        "edit_film_genre": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_genre = unicode(self.ui_master_data.lineEdit_edit_film_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_genre,
                            stack_widget = self.ui_master_data.stackedWidget_film_genre,
                            label_object = self.ui_master_data.label_film_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_genre,
                            show_messagebox = True,
                            set_flush = True,
                            set_page = True,
                            work_mode = "edit_record",
                            category = 'film_genre'),

                        "delete_film_genre": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_genre,
                            line_edit_obj = self.ui_master_data.lineEdit_film_genre,
                            label_object = self.ui_master_data.label_film_genre_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_genre,
                            set_flush = True,
                            category = 'film_genre'),

                        "update_film_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_film_genre,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_film_genre,
                            widget_label = self.ui_master_data.label_film_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_genre,
                            notify_on_widget = self.ui_master_data.textEdit_notify_film_genre,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_genre,
                            sort_column = 1,
                            category = 'film_genre'),

        ###################################################
        #################  General Award    ###############
        ###################################################
                        "fetch_all_general_award": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_award,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_AWARD,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_award,
                            widget_label = self.ui_master_data.label_general_award_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_award,
                            show_messagebox = True,
                            category = 'general_award'),

                        "add_general_award": lambda: self.add_record(
                            general_award = unicode(self.ui_master_data.lineEdit_general_award.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_award,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_award,
                            label_object = self.ui_master_data.label_general_award_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_award,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_award'),

                        "edit_general_award": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_award = unicode(self.ui_master_data.lineEdit_edit_general_award.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_award,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_award,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_award,
                            stack_widget = self.ui_master_data.stackedWidget_general_award,
                            label_object = self.ui_master_data.label_general_award_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_award,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_award'),

                        "delete_general_award": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_award,
                            line_edit_obj = self.ui_master_data.lineEdit_general_award,
                            label_object = self.ui_master_data.label_general_award_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_award,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_award,
                            set_flush = True,
                            category = 'general_award'),

                        # "update_general_award": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_general_award,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_general_award,
                        #                                                  widget_label = self.ui_master_data.label_general_award_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_general_award,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_general_award,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_award,
                        #                                                  sort_column = 1,
                        #                                                  category = 'general_award')

        ###################################################
        #################  Music Genre    #################
        ###################################################
                        "fetch_all_music_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_genre,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_GENRE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_genre,
                            widget_label = self.ui_master_data.label_music_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_genre,
                            show_messagebox = True,
                            category = 'music_genre'),

                        "add_music_genre": lambda: self.add_record(
                            music_genre = unicode(self.ui_master_data.lineEdit_music_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_genre,
                            label_object = self.ui_master_data.label_music_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_genre'),

                        "edit_music_genre": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_genre = unicode(self.ui_master_data.lineEdit_edit_music_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_genre,
                            stack_widget = self.ui_master_data.stackedWidget_music_genre,
                            label_object = self.ui_master_data.label_music_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_genre'),

                        "delete_music_genre": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_genre,
                            line_edit_obj = self.ui_master_data.lineEdit_music_genre,
                            label_object = self.ui_master_data.label_music_genre_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_genre,
                            set_flush = True,
                            category = 'music_genre'),

                        # "update_music_genre": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_music_genre,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_music_genre,
                        #                                                  widget_label = self.ui_master_data.label_music_genre_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_music_genre,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_music_genre,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_genre,
                        #                                                  sort_column = 1,
                        #                                                  category = 'music_genre')

        ###################################################
        #################  Person Gender    ###############
        ###################################################
                        "fetch_all_person_gender": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_gender,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_GENDER,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_gender,
                            widget_label = self.ui_master_data.label_person_gender_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_gender,
                            show_messagebox = True,
                            category = 'person_gender'),

                        "add_person_gender": lambda: self.add_record(
                            person_gender = unicode(self.ui_master_data.lineEdit_person_gender.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_gender,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_gender,
                            label_object = self.ui_master_data.label_person_gender_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_gender,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_gender'),

                        "edit_person_gender": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_gender = unicode(self.ui_master_data.lineEdit_edit_person_gender.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_gender,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_gender,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_gender,
                            stack_widget = self.ui_master_data.stackedWidget_person_gender,
                            label_object = self.ui_master_data.label_person_gender_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_gender,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_gender'),

                        "delete_person_gender": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_gender,
                            line_edit_obj = self.ui_master_data.lineEdit_person_gender,
                            label_object = self.ui_master_data.label_person_gender_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_gender,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_gender,
                            set_flush = True,
                            category = 'person_gender'),

                        # "update_person_gender": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_gender,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_gender,
                        #                                                  widget_label = self.ui_master_data.label_person_gender_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_gender,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_gender,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_gender,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_gender')


        ###################################################
        #################  Person Salutation    ###########
        ###################################################
                        "fetch_all_person_salutation": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_salutation,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_SALUTATION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_salutation,
                            widget_label = self.ui_master_data.label_person_salutation_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                            show_messagebox = True,
                            category = 'person_salutation'),

                        "add_person_salutation": lambda: self.add_record(
                            person_salutation = unicode(self.ui_master_data.lineEdit_person_salutation.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_salutation,
                            label_object = self.ui_master_data.label_person_salutation_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_salutation'),

                        "edit_person_salutation": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_salutation = unicode(self.ui_master_data.lineEdit_edit_person_salutation.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_salutation,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_salutation,
                            stack_widget = self.ui_master_data.stackedWidget_person_salutation,
                            label_object = self.ui_master_data.label_person_salutation_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_salutation'),

                        "delete_person_salutation": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_salutation,
                            line_edit_obj = self.ui_master_data.lineEdit_person_salutation,
                            label_object = self.ui_master_data.label_person_salutation_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_salutation,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_salutation,
                            set_flush = True,
                            category = 'person_salutation'),

                        # "update_person_salutation": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_salutation,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_salutation,
                        #                                                  widget_label = self.ui_master_data.label_person_salutation_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_salutation,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_salutation,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_salutation')

        ###################################################
        #################  Person Title    ################
        ###################################################
                        "fetch_all_person_title": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_title,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_TITLE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_title,
                            widget_label = self.ui_master_data.label_person_title_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_title,
                            show_messagebox = True,
                            category = 'person_title'),

                        "add_person_title": lambda: self.add_record(
                            person_title = unicode(self.ui_master_data.lineEdit_person_title.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_title,
                            label_object = self.ui_master_data.label_person_title_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_title,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_title'),

                        "edit_person_title": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_title = unicode(self.ui_master_data.lineEdit_edit_person_title.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_title,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_title,
                            stack_widget = self.ui_master_data.stackedWidget_person_title,
                            label_object = self.ui_master_data.label_person_title_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_title,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_title'),

                        "delete_person_title": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_title,
                            line_edit_obj = self.ui_master_data.lineEdit_person_title,
                            label_object = self.ui_master_data.label_person_title_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_title,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_title,
                            set_flush = True,
                            category = 'person_title'),

                        # "update_person_title": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_title,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_title,
                        #                                                  widget_label = self.ui_master_data.label_person_title_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_title,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_title,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_title,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_title')

        ###################################################
        #################  Person Eye Colour    ###########
        ###################################################
                        "fetch_all_person_eye_color": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_eye_color,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_EYE_COLOR,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_eye_color,
                            widget_label = self.ui_master_data.label_person_eye_color_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                            show_messagebox = True,
                            category = 'person_eye_color'),

                        "add_person_eye_color": lambda: self.add_record(
                            person_eye_color = unicode(self.ui_master_data.lineEdit_person_eye_color.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_eye_color,
                            label_object = self.ui_master_data.label_person_eye_color_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_eye_color'),

                        "edit_person_eye_color": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_eye_color = unicode(self.ui_master_data.lineEdit_edit_person_eye_color.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_eye_color,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_eye_color,
                            stack_widget = self.ui_master_data.stackedWidget_person_eye_color,
                            label_object = self.ui_master_data.label_person_eye_color_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_eye_color'),

                        "delete_person_eye_color": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_eye_color,
                            line_edit_obj = self.ui_master_data.lineEdit_person_eye_color,
                            label_object = self.ui_master_data.label_person_eye_color_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_eye_color,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_eye_color,
                            set_flush = True,
                            category = 'person_eye_color'),

                        # "update_person_eye_color": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_eye_color,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_eye_color,
                        #                                                  widget_label = self.ui_master_data.label_person_eye_color_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_eye_color,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_eye_color,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_eye_color')

        ###################################################
        #################  Person Hair Colour    ##########
        ###################################################
                        "fetch_all_person_hair_color": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_hair_color,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_HAIR_COLOR,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_hair_color,
                            widget_label = self.ui_master_data.label_person_hair_color_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                            show_messagebox = True,
                            category = 'person_hair_color'),

                        "add_person_hair_color": lambda: self.add_record(
                            person_hair_color = unicode(self.ui_master_data.lineEdit_person_hair_color.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_hair_color,
                            label_object = self.ui_master_data.label_person_hair_color_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_hair_color'),

                        "edit_person_hair_color": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_hair_color = unicode(self.ui_master_data.lineEdit_edit_person_hair_color.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_hair_color,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_hair_color,
                            stack_widget = self.ui_master_data.stackedWidget_person_hair_color,
                            label_object = self.ui_master_data.label_person_hair_color_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_hair_color'),

                        "delete_person_hair_color": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_hair_color,
                            line_edit_obj = self.ui_master_data.lineEdit_person_hair_color,
                            label_object = self.ui_master_data.label_person_hair_color_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_hair_color,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_hair_color,
                            set_flush = True,
                            category = 'person_hair_color'),

                        # "update_person_hair_color": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_hair_color,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_hair_color,
                        #                                                  widget_label = self.ui_master_data.label_person_hair_color_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_hair_color,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_hair_color,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_hair_color')

        ###################################################
        #################  Person Religion    #############
        ###################################################
                        "fetch_all_person_religion": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_religion,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_RELIGION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_religion,
                            widget_label = self.ui_master_data.label_person_religion_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_religion,
                            show_messagebox = True,
                            category = 'person_religion'),

                        "add_person_religion": lambda: self.add_record(
                            person_religion = unicode(self.ui_master_data.lineEdit_person_religion.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_religion,
                            label_object = self.ui_master_data.label_person_religion_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_religion,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_religion'),

                        "edit_person_religion": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_religion = unicode(self.ui_master_data.lineEdit_edit_person_religion.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_religion,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_religion,
                            stack_widget = self.ui_master_data.stackedWidget_person_religion,
                            label_object = self.ui_master_data.label_person_religion_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_religion,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_religion'),

                        "delete_person_religion": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_religion,
                            line_edit_obj = self.ui_master_data.lineEdit_person_religion,
                            label_object = self.ui_master_data.label_person_religion_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_religion,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_religion,
                            set_flush = True,
                            category = 'person_religion'),

                        # "update_person_religion": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_religion,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_religion,
                        #                                                  widget_label = self.ui_master_data.label_person_religion_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_religion,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_religion,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_religion,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_religion')

        ###################################################
        #################  Person Relationship Status    ##
        ###################################################
                        "fetch_all_person_relationship_status": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship_status,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP_STATUS,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status,
                            widget_label = self.ui_master_data.label_person_relationship_status_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                            show_messagebox = True,
                            category = 'person_relationship_status'),


                        "add_person_relationship_status": lambda: self.add_record(
                            person_relationship_status = unicode(self.ui_master_data.lineEdit_person_relationship_status.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status,
                            label_object = self.ui_master_data.label_person_relationship_status_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_relationship_status'),

                        "edit_person_relationship_status": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_relationship_status = unicode(self.ui_master_data.lineEdit_edit_person_relationship_status.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_relationship_status,
                            stack_widget = self.ui_master_data.stackedWidget_person_relationship_status,
                            label_object = self.ui_master_data.label_person_relationship_status_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_relationship_status'),

                        "delete_person_relationship_status": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_relationship_status,
                            line_edit_obj = self.ui_master_data.lineEdit_person_relationship_status,
                            label_object = self.ui_master_data.label_person_relationship_status_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_relationship_status,
                            set_flush = True,
                            category = 'person_relationship_status'),

                        # "update_person_relationship_status": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_relationship_status,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_relationship_status,
                        #                                                  widget_label = self.ui_master_data.label_person_relationship_status_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_relationship_status,
                        #                                                  certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship_status,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_relationship_status')

        ###################################################
        #################  Person Relationship    #########
        ###################################################
                        "fetch_all_person_relationship": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_person_relationship,
                            list_header = self. LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship,
                            widget_label = self.ui_master_data.label_person_relationship_counter,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                            show_messagebox = True,
                            category = 'person_relationship'),

                        "add_person_relationship": lambda: self.add_record(
                            person_relationship = unicode(self.ui_master_data.lineEdit_person_relationship.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship,
                            label_object = self.ui_master_data.label_person_relationship_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'person_relationship'),

                        "edit_person_relationship": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            person_relationship = unicode(self.ui_master_data.lineEdit_edit_person_relationship.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_relationship,
                            stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                            label_object = self.ui_master_data.label_person_relationship_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'person_relationship'),

                        "delete_person_relationship": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_person_relationship,
                            line_edit_obj = self.ui_master_data.lineEdit_person_relationship,
                            label_object = self.ui_master_data.label_person_relationship_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_person_relationship,
                            set_flush = True,
                            category = 'person_relationship'),

                        "update_person_relationship": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                             general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                             work_mode = 'fetch_online_data',
                                                                             bulk_the_list = True,
                                                                             set_page = True,
                                                                             widget_stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                                                                             page_index = 0,
                                                                             widget_line_edit = self.ui_master_data.lineEdit_person_relationship,
                                                                             widget_label = self.ui_master_data.label_person_relationship_counter,
                                                                             widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                                                                             notify_on_widget = self.ui_master_data.textEdit_notify_person_relationship,
                                                                             certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_person_relationship,
                                                                             sort_column = 1,
                                                                             category = 'person_relationship'),

        ###################################################
        #################  General Media Type    ##########
        ###################################################
                        "fetch_all_general_media_type": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_media_type,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_MEDIA_TYPE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_media_type,
                            widget_label = self.ui_master_data.label_general_media_type_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                            show_messagebox = True,
                            category = 'general_media_type'),

                        "add_general_media_type": lambda: self.add_record(
                            general_media_type = unicode(self.ui_master_data.lineEdit_general_media_type.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_media_type,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_media_type,
                            label_object = self.ui_master_data.label_general_media_type_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_media_type'),

                        "edit_general_media_type": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_media_type = unicode(self.ui_master_data.lineEdit_edit_general_media_type.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_media_type,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_media_type,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_media_type,
                            stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                            label_object = self.ui_master_data.label_general_media_type_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_media_type'),

                        "delete_general_media_type": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_media_type,
                            line_edit_obj = self.ui_master_data.lineEdit_general_media_type,
                            label_object = self.ui_master_data.label_general_media_type_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_media_type,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_media_type,
                            set_flush = True,
                            category = 'general_media_type'),

                        "update_general_media_type": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                            work_mode = 'fetch_online_data',
                                                                            bulk_the_list = True,
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_general_media_type,
                                                                            widget_label = self.ui_master_data.label_general_media_type_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_general_media_type,
                                                                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_media_type,
                                                                            sort_column = 1,
                                                                            category = 'general_media_type'),

        ###################################################
        #################  General Packaging    ###########
        ###################################################
                        "fetch_all_general_packaging": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_packaging,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_PACKAGING,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_packaging,
                            widget_label = self.ui_master_data.label_general_packaging_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                            show_messagebox = True,
                            category = 'general_packaging'),

                        "add_general_packaging": lambda: self.add_record(
                            general_packaging = unicode(self.ui_master_data.lineEdit_general_packaging.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_packaging,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_packaging,
                            label_object = self.ui_master_data.label_general_packaging_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_packaging'),

                        "edit_general_packaging": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_packaging = unicode(self.ui_master_data.lineEdit_edit_general_packaging.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_packaging,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_packaging,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_packaging,
                            stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                            label_object = self.ui_master_data.label_general_packaging_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_packaging'),

                        "delete_general_packaging": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_packaging,
                            line_edit_obj = self.ui_master_data.lineEdit_general_packaging,
                            label_object = self.ui_master_data.label_general_packaging_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_packaging,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_packaging,
                            set_flush = True,
                            category = 'general_packaging'),

                        "update_general_packaging": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                           general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                           work_mode = 'fetch_online_data',
                                                                           bulk_the_list = True,
                                                                           set_page = True,
                                                                           widget_stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                                                                           page_index = 0,
                                                                           widget_line_edit = self.ui_master_data.lineEdit_general_packaging,
                                                                           widget_label = self.ui_master_data.label_general_packaging_counter,
                                                                           widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                                                                           notify_on_widget = self.ui_master_data.textEdit_notify_general_packaging,
                                                                           certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_packaging,
                                                                           sort_column = 1,
                                                                           category = 'general_packaging'),
        ###################################################
        #################  Film Production Company    #####
        ###################################################
                        "fetch_all_film_production_company": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_production_company,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_PRODUCTION_COMPANY,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_production_company,
                            widget_label = self.ui_master_data.label_film_production_company_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_production_company,
                            show_messagebox = True,
                            category = 'film_production_company'),

                        "add_film_production_company": lambda: self.add_record(
                            film_production_company = unicode(self.ui_master_data.lineEdit_film_production_company.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_production_company,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_production_company,
                            label_object = self.ui_master_data.label_film_production_company_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_production_company,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_production_company'),

                        "edit_film_production_company": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_production_company = unicode(self.ui_master_data.lineEdit_edit_film_production_company.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_production_company,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_production_company,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_production_company,
                            stack_widget = self.ui_master_data.stackedWidget_film_production_company,
                            label_object = self.ui_master_data.label_film_production_company_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_production_company,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_production_company'),

                        "delete_film_production_company": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_production_company,
                            line_edit_obj = self.ui_master_data.lineEdit_film_production_company,
                            label_object = self.ui_master_data.label_film_production_company_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_production_company,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_production_company,
                            set_flush = True,
                            category = 'film_production_company'),

                        "update_film_production_company": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                                general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                                work_mode = 'fetch_online_data',
                                                                                bulk_the_list = True,
                                                                                set_page = True,
                                                                                widget_stack_widget = self.ui_master_data.stackedWidget_film_production_company,
                                                                                page_index = 0,
                                                                                widget_line_edit = self.ui_master_data.lineEdit_film_production_company,
                                                                                widget_label = self.ui_master_data.label_film_production_company_counter,
                                                                                widget_tree_view = self.ui_master_data.tree_view_film_production_company,
                                                                                notify_on_widget = self.ui_master_data.textEdit_notify_film_production_company,
                                                                                certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_production_company,
                                                                                sort_column = 1,
                                                                                category = 'film_production_company'),

        ###################################################
        #################  Film Rentals    ################
        ###################################################
                        "fetch_all_film_rentals": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_rentals,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_RENTALS,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_rentals,
                            widget_label = self.ui_master_data.label_film_rentals_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_rentals,
                            show_messagebox = True,
                            category = 'film_rentals'),

                        "add_film_rentals": lambda: self.add_record(
                            film_rentals = unicode(self.ui_master_data.lineEdit_film_rentals.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_rentals,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_rentals,
                            label_object = self.ui_master_data.label_film_rentals_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_rentals,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_rentals'),

                        "edit_film_rentals": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_rentals = unicode(self.ui_master_data.lineEdit_edit_film_rentals.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_rentals,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_rentals,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_rentals,
                            stack_widget = self.ui_master_data.stackedWidget_film_rentals,
                            label_object = self.ui_master_data.label_film_rentals_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_rentals,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_rentals'),

                        "delete_film_rentals": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_rentals,
                            line_edit_obj = self.ui_master_data.lineEdit_film_rentals,
                            label_object = self.ui_master_data.label_film_rentals_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_rentals,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_rentals,
                            set_flush = True,
                            category = 'film_rentals'),

                        "update_film_rentals": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                      general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                      work_mode = 'fetch_online_data',
                                                                      bulk_the_list = True,
                                                                      set_page = True,
                                                                      widget_stack_widget = self.ui_master_data.stackedWidget_film_rentals,
                                                                      page_index = 0,
                                                                      widget_line_edit = self.ui_master_data.lineEdit_film_rentals,
                                                                      widget_label = self.ui_master_data.label_film_rentals_counter,
                                                                      widget_tree_view = self.ui_master_data.tree_view_film_rentals,
                                                                      notify_on_widget = self.ui_master_data.textEdit_notify_film_rentals,
                                                                      certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_rentals,
                                                                      sort_column = 1,
                                                                      category = 'film_rentals'),

        ###################################################
        #################  Film Distribution    ###########
        ###################################################
                        "fetch_all_film_distribution": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_distribution,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_DISTRIBUTION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_distribution,
                            widget_label = self.ui_master_data.label_film_distribution_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_distribution,
                            show_messagebox = True,
                            category = 'film_distribution'),

                        "add_film_distribution": lambda: self.add_record(
                            film_distribution = unicode(self.ui_master_data.lineEdit_film_distribution.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_distribution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_distribution,
                            label_object = self.ui_master_data.label_film_distribution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_distribution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_distribution'),

                        "edit_film_distribution": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_distribution = unicode(self.ui_master_data.lineEdit_edit_film_distribution.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_distribution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_distribution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_distribution,
                            stack_widget = self.ui_master_data.stackedWidget_film_distribution,
                            label_object = self.ui_master_data.label_film_distribution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_distribution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_distribution'),

                        "delete_film_distribution": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_distribution,
                            line_edit_obj = self.ui_master_data.lineEdit_film_distribution,
                            label_object = self.ui_master_data.label_film_distribution_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_distribution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_distribution,
                            set_flush = True,
                            category = 'film_distribution'),

                        "update_film_distribution": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                          general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                           work_mode = 'fetch_online_data',
                                                                           bulk_the_list = True,
                                                                           set_page = True,
                                                                           widget_stack_widget = self.ui_master_data.stackedWidget_film_distribution,
                                                                           page_index = 0,
                                                                           widget_line_edit = self.ui_master_data.lineEdit_film_distribution,
                                                                           widget_label = self.ui_master_data.label_film_distribution_counter,
                                                                           widget_tree_view = self.ui_master_data.tree_view_film_distribution,
                                                                           notify_on_widget = self.ui_master_data.textEdit_notify_film_distribution,
                                                                           certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_distribution,
                                                                           sort_column = 1,
                                                                           category = 'film_distribution'),

        ###################################################
        #################  Film Studio    #################
        ###################################################
                        "fetch_all_film_studio": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_studio,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_STUDIO,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_studio,
                            widget_label = self.ui_master_data.label_film_studio_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_studio,
                            show_messagebox = True,
                            category = 'film_studio'),

                        "add_film_studio": lambda: self.add_record(
                            film_studio = unicode(self.ui_master_data.lineEdit_film_studio.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_studio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_studio,
                            label_object = self.ui_master_data.label_film_studio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_studio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_studio'),

                        "edit_film_studio": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_studio = unicode(self.ui_master_data.lineEdit_edit_film_studio.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_studio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_studio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_studio,
                            stack_widget = self.ui_master_data.stackedWidget_film_studio,
                            label_object = self.ui_master_data.label_film_studio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_studio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_studio'),

                        "delete_film_studio": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_studio,
                            line_edit_obj = self.ui_master_data.lineEdit_film_studio,
                            label_object = self.ui_master_data.label_film_studio_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_studio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_studio,
                            set_flush = True,
                            category = 'film_studio'),

                        "update_film_studio": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                     general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                     work_mode = 'fetch_online_data',
                                                                     bulk_the_list = True,
                                                                     set_page = True,
                                                                     widget_stack_widget = self.ui_master_data.stackedWidget_film_studio,
                                                                     page_index = 0,
                                                                     widget_line_edit = self.ui_master_data.lineEdit_film_studio,
                                                                     widget_label = self.ui_master_data.label_film_studio_counter,
                                                                     widget_tree_view = self.ui_master_data.tree_view_film_studio,
                                                                     notify_on_widget = self.ui_master_data.textEdit_notify_film_studio,
                                                                     certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_studio,
                                                                     sort_column = 1,
                                                                     category = 'film_studio'),

        ###################################################
        #################  Film Resolution    #############
        ###################################################
                        "fetch_all_film_resolution": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_resolution,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_RESOLUTION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_resolution,
                            widget_label = self.ui_master_data.label_film_resolution_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_resolution,
                            show_messagebox = True,
                            category = 'film_resolution'),

                        "add_film_resolution": lambda: self.add_record(
                            film_resolution = unicode(self.ui_master_data.lineEdit_film_resolution.text()).strip(),
                            film_presenting_format = unicode(self.ui_master_data.lineEdit_film_presenting_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_resolution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_resolution,
                            label_object = self.ui_master_data.label_film_resolution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_resolution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_resolution'),

                        "edit_film_resolution": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_resolution = unicode(self.ui_master_data.lineEdit_edit_film_resolution.text()).strip(),
                            film_presenting_format = unicode(self.ui_master_data.lineEdit_edit_film_presenting_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_resolution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_resolution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_resolution,
                            stack_widget = self.ui_master_data.stackedWidget_film_resolution,
                            label_object = self.ui_master_data.label_film_resolution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_resolution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_resolution'),

                        "delete_film_resolution": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_resolution,
                            line_edit_obj = self.ui_master_data.lineEdit_film_resolution,
                            label_object = self.ui_master_data.label_film_resolution_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_resolution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_resolution,
                            set_flush = True,
                            category = 'film_resolution'),

                        "update_film_resolution": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_film_resolution,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_film_resolution,
                            widget_label = self.ui_master_data.label_film_resolution_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_resolution,
                            notify_on_widget = self.ui_master_data.textEdit_notify_film_resolution,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_resolution,
                            sort_column = 1,
                            category = 'film_resolution'),

        ###################################################
        #################  General Manufacturer    ########
        ###################################################
                        "fetch_all_general_manufacturer": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_general_manufacturer,
                            list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_MANUFACTURER,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer,
                            widget_label = self.ui_master_data.label_general_manufacturer_counter,
                            widget_tree_view = self.ui_master_data.tree_view_general_manufacturer,
                            show_messagebox = True,
                            category = 'general_manufacturer'),

                        "add_general_manufacturer": lambda: self.add_record(
                            general_manufacturer = unicode(self.ui_master_data.lineEdit_general_manufacturer.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_manufacturer,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer,
                            label_object = self.ui_master_data.label_general_manufacturer_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_manufacturer,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'general_manufacturer'),

                        "edit_general_manufacturer": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            general_manufacturer = unicode(self.ui_master_data.lineEdit_edit_general_manufacturer.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_manufacturer,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_manufacturer,
                            stack_widget = self.ui_master_data.stackedWidget_general_manufacturer,
                            label_object = self.ui_master_data.label_general_manufacturer_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_general_manufacturer,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'general_manufacturer'),

                        "delete_general_manufacturer": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_general_manufacturer,
                            line_edit_obj = self.ui_master_data.lineEdit_general_manufacturer,
                            label_object = self.ui_master_data.label_general_manufacturer_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_general_manufacturer,
                            set_flush = True,
                            category = 'general_manufacturer'),

                        "update_general_manufacturer": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                              work_mode = 'fetch_online_data',
                                                                              bulk_the_list = True,
                                                                              set_page = True,
                                                                              widget_stack_widget = self.ui_master_data.stackedWidget_general_manufacturer,
                                                                              page_index = 0,
                                                                              widget_line_edit = self.ui_master_data.lineEdit_general_manufacturer,
                                                                              widget_label = self.ui_master_data.label_general_manufacturer_counter,
                                                                              widget_tree_view = self.ui_master_data.tree_view_general_manufacturer,
                                                                              notify_on_widget = self.ui_master_data.textEdit_notify_general_manufacturer,
                                                                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_general_manufacturer,
                                                                              sort_column = 1,
                                                                              category = 'general_manufacturer'),

        ###################################################
        #################  Film Regional Code    ##########
        ###################################################
                        "fetch_all_film_regional_code": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_regional_code,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_REGIONAL_CODE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_regional_code,
                            widget_label = self.ui_master_data.label_film_regional_code_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_regional_code,
                            show_messagebox = True,
                            category = 'film_regional_code'),

                        "add_film_regional_code": lambda: self.add_record(
                            film_regional_code = unicode(self.ui_master_data.lineEdit_film_regional_code.text()).strip(),
                            film_region = unicode(self.ui_master_data.lineEdit_film_region.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_regional_code,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_regional_code,
                            label_object = self.ui_master_data.label_film_regional_code_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_regional_code,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_regional_code'),

                        "edit_film_regional_code": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_regional_code = unicode(self.ui_master_data.lineEdit_edit_film_regional_code.text()).strip(),
                            film_region = unicode(self.ui_master_data.lineEdit_edit_film_region.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_regional_code,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_regional_code,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_regional_code,
                            stack_widget = self.ui_master_data.stackedWidget_film_regional_code,
                            label_object = self.ui_master_data.label_film_regional_code_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_regional_code,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_regional_code'),

                        "delete_film_regional_code": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_regional_code,
                            line_edit_obj = self.ui_master_data.lineEdit_film_regional_code,
                            label_object = self.ui_master_data.label_film_regional_code_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_regional_code,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_regional_code,
                            set_flush = True,
                            category = 'film_regional_code'),

                        "update_film_regional_code": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_film_regional_code,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_film_regional_code,
                            widget_label = self.ui_master_data.label_film_regional_code_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_regional_code,
                            notify_on_widget = self.ui_master_data.textEdit_notify_film_regional_code,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_regional_code,
                            sort_column = 1,
                            category = 'film_regional_code'),

        ###################################################
        #################  Film Aspect Ratio    ###########
        ###################################################
                        "fetch_all_film_aspect_ratio": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_aspect_ratio,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_ASPECT_RATIO,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio,
                            widget_label = self.ui_master_data.label_film_aspect_ratio_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_aspect_ratio,
                            show_messagebox = True,
                            category = 'film_aspect_ratio'),

                        "add_film_aspect_ratio": lambda: self.add_record(
                            film_aspect_ratio = unicode(self.ui_master_data.lineEdit_film_aspect_ratio.text()).strip(),
                            film_aspect_ratio_format = unicode(self.ui_master_data.lineEdit_film_aspect_ratio_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_aspect_ratio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio,
                            label_object = self.ui_master_data.label_film_aspect_ratio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_aspect_ratio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_aspect_ratio'),

                        "edit_film_aspect_ratio": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_aspect_ratio = unicode(self.ui_master_data.lineEdit_edit_film_aspect_ratio.text()).strip(),
                            film_aspect_ratio_format = unicode(self.ui_master_data.lineEdit_edit_film_aspect_ratio_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_aspect_ratio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_aspect_ratio,
                            stack_widget = self.ui_master_data.stackedWidget_film_aspect_ratio,
                            label_object = self.ui_master_data.label_film_aspect_ratio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_aspect_ratio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_aspect_ratio'),

                        "delete_film_aspect_ratio": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_aspect_ratio,
                            line_edit_obj = self.ui_master_data.lineEdit_film_aspect_ratio,
                            label_object = self.ui_master_data.label_film_aspect_ratio_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_aspect_ratio,
                            set_flush = True,
                            category = 'film_aspect_ratio'),

                        "update_film_aspect_ratio": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_film_aspect_ratio,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_film_aspect_ratio,
                            widget_label = self.ui_master_data.label_film_aspect_ratio_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_aspect_ratio,
                            notify_on_widget = self.ui_master_data.textEdit_notify_film_aspect_ratio,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_aspect_ratio,
                            sort_column = 1,
                            category = 'film_aspect_ratio'),

        ###################################################
        #################  Film Dimension    ##############
        ###################################################
                        "fetch_all_film_dimension": lambda: self.fetch_all(
                              scoped_session = self._scoped_session,
                              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                              hide_column = True,
                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                              resize_column = 1,
                              populate_timer_interval = 1,
                              bulk_the_list = False,
                              general_proxy_model = self._kwargs.get('general_proxy_model'),
                              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_dimension,
                              list_header = self. LIST_HORIZONTAL_HEADER_FILM_DIMENSION,
                              work_mode = 'fetch_all',
                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_dimension,
                              widget_label = self.ui_master_data.label_film_dimension_counter,
                              widget_tree_view = self.ui_master_data.tree_view_film_dimension,
                              show_messagebox = True,
                              category = 'film_dimension'),

                        "add_film_dimension": lambda: self.add_record(
                            film_dimension = unicode(self.ui_master_data.lineEdit_film_dimension.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_dimension,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_dimension,
                            label_object = self.ui_master_data.label_film_dimension_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_dimension,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_dimension'),

                        "edit_film_dimension": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_dimension = unicode(self.ui_master_data.lineEdit_edit_film_dimension.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_dimension,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_dimension,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_dimension,
                            stack_widget = self.ui_master_data.stackedWidget_film_dimension,
                            label_object = self.ui_master_data.label_film_dimension_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_dimension,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_dimension'),

                        "delete_film_dimension": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_dimension,
                            line_edit_obj = self.ui_master_data.lineEdit_film_dimension,
                            label_object = self.ui_master_data.label_film_dimension_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_dimension,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_dimension,
                            set_flush = True,
                            category = 'film_dimension'),

                        "update_film_dimension": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                        general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                        work_mode = 'fetch_online_data',
                                                                        bulk_the_list = True,
                                                                        set_page = True,
                                                                        widget_stack_widget = self.ui_master_data.stackedWidget_film_dimension,
                                                                        page_index = 0,
                                                                        widget_line_edit = self.ui_master_data.lineEdit_film_dimension,
                                                                        widget_label = self.ui_master_data.label_film_dimension_counter,
                                                                        widget_tree_view = self.ui_master_data.tree_view_film_dimension,
                                                                        notify_on_widget = self.ui_master_data.textEdit_notify_film_dimension,
                                                                        certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_dimension,
                                                                        sort_column = 1,
                                                                        category = 'film_dimension'),

        ###################################################
        #################  Film Edition    ################
        ###################################################
                        "fetch_all_film_edition": lambda: self.fetch_all(
                              scoped_session = self._scoped_session,
                              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                              hide_column = True,
                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                              resize_column = 1,
                              populate_timer_interval = 1,
                              bulk_the_list = False,
                              general_proxy_model = self._kwargs.get('general_proxy_model'),
                              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_edition,
                              list_header = self. LIST_HORIZONTAL_HEADER_FILM_EDITION,
                              work_mode = 'fetch_all',
                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_edition,
                              widget_label = self.ui_master_data.label_film_edition_counter,
                              widget_tree_view = self.ui_master_data.tree_view_film_edition,
                              show_messagebox = True,
                              category = 'film_edition'),

                        "add_film_edition": lambda: self.add_record(
                            film_edition = unicode(self.ui_master_data.lineEdit_film_edition.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_edition,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_edition,
                            label_object = self.ui_master_data.label_film_edition_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_edition,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_edition'),

                        "edit_film_edition": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_edition = unicode(self.ui_master_data.lineEdit_edit_film_edition.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_edition,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_edition,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_edition,
                            stack_widget = self.ui_master_data.stackedWidget_film_edition,
                            label_object = self.ui_master_data.label_film_edition_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_edition,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_edition'),

                        "delete_film_edition": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_edition,
                            line_edit_obj = self.ui_master_data.lineEdit_film_edition,
                            label_object = self.ui_master_data.label_film_edition_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_edition,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_edition,
                            set_flush = True,
                            category = 'film_edition'),

                        "update_film_edition": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                      general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                      work_mode = 'fetch_online_data',
                                                                      bulk_the_list = True,
                                                                      set_page = True,
                                                                      widget_stack_widget = self.ui_master_data.stackedWidget_film_edition,
                                                                      page_index = 0,
                                                                      widget_line_edit = self.ui_master_data.lineEdit_film_edition,
                                                                      widget_label = self.ui_master_data.label_film_edition_counter,
                                                                      widget_tree_view = self.ui_master_data.tree_view_film_edition,
                                                                      notify_on_widget = self.ui_master_data.textEdit_notify_film_edition,
                                                                      certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_edition,
                                                                      sort_column = 1,
                                                                      category = 'film_edition'),

        ###################################################
        #################  Film Version    ################
        ###################################################
                        "fetch_all_film_version": lambda: self.fetch_all(
                              scoped_session = self._scoped_session,
                              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                              hide_column = True,
                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                              resize_column = 1,
                              populate_timer_interval = 1,
                              bulk_the_list = False,
                              general_proxy_model = self._kwargs.get('general_proxy_model'),
                              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_version,
                              list_header = self. LIST_HORIZONTAL_HEADER_FILM_VERSION,
                              work_mode = 'fetch_all',
                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_version,
                              widget_label = self.ui_master_data.label_film_version_counter,
                              widget_tree_view = self.ui_master_data.tree_view_film_version,
                              show_messagebox = True,
                              category = 'film_version'),

                        "add_film_version": lambda: self.add_record(
                            film_version = unicode(self.ui_master_data.lineEdit_film_version.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_version,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_version,
                            label_object = self.ui_master_data.label_film_version_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_version,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_version'),

                        "edit_film_version": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_version = unicode(self.ui_master_data.lineEdit_edit_film_version.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_version,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_version,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_version,
                            stack_widget = self.ui_master_data.stackedWidget_film_version,
                            label_object = self.ui_master_data.label_film_version_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_version,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_version'),

                        "delete_film_version": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_version,
                            line_edit_obj = self.ui_master_data.lineEdit_film_version,
                            label_object = self.ui_master_data.label_film_version_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_version,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_version,
                            set_flush = True,
                            category = 'film_version'),

                        "update_film_version": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                      general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                      work_mode = 'fetch_online_data',
                                                                      bulk_the_list = True,
                                                                      set_page = True,
                                                                      widget_stack_widget = self.ui_master_data.stackedWidget_film_version,
                                                                      page_index = 0,
                                                                      widget_line_edit = self.ui_master_data.lineEdit_film_version,
                                                                      widget_label = self.ui_master_data.label_film_version_counter,
                                                                      widget_tree_view = self.ui_master_data.tree_view_film_version,
                                                                      notify_on_widget = self.ui_master_data.textEdit_notify_film_version,
                                                                      certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_version,
                                                                      sort_column = 1,
                                                                      category = 'film_version'),

        ###################################################
        #################  Film Videonorm    ##############
        ###################################################
                        "fetch_all_film_videonorm": lambda: self.fetch_all(
                              scoped_session = self._scoped_session,
                              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                              hide_column = True,
                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                              resize_column = 1,
                              populate_timer_interval = 1,
                              bulk_the_list = False,
                              general_proxy_model = self._kwargs.get('general_proxy_model'),
                              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videonorm,
                              list_header = self. LIST_HORIZONTAL_HEADER_FILM_VIDEONORM,
                              work_mode = 'fetch_all',
                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videonorm,
                              widget_label = self.ui_master_data.label_film_videonorm_counter,
                              widget_tree_view = self.ui_master_data.tree_view_film_videonorm,
                              show_messagebox = True,
                              category = 'film_videonorm'),

                        "add_film_videonorm": lambda: self.add_record(
                            film_videonorm = unicode(self.ui_master_data.lineEdit_film_videonorm.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_videonorm,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videonorm,
                            label_object = self.ui_master_data.label_film_videonorm_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_videonorm,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_videonorm'),

                        "edit_film_videonorm": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_videonorm = unicode(self.ui_master_data.lineEdit_edit_film_videonorm.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_videonorm,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videonorm,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_videonorm,
                            stack_widget = self.ui_master_data.stackedWidget_film_videonorm,
                            label_object = self.ui_master_data.label_film_videonorm_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_videonorm,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_videonorm'),

                        "delete_film_videonorm": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_videonorm,
                            line_edit_obj = self.ui_master_data.lineEdit_film_videonorm,
                            label_object = self.ui_master_data.label_film_videonorm_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videonorm,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_videonorm,
                            set_flush = True,
                            category = 'film_videonorm'),

                        "update_film_videonorm": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                        general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                        work_mode = 'fetch_online_data',
                                                                        bulk_the_list = True,
                                                                        set_page = True,
                                                                        widget_stack_widget = self.ui_master_data.stackedWidget_film_videonorm,
                                                                        page_index = 0,
                                                                        widget_line_edit = self.ui_master_data.lineEdit_film_videonorm,
                                                                        widget_label = self.ui_master_data.label_film_videonorm_counter,
                                                                        widget_tree_view = self.ui_master_data.tree_view_film_videonorm,
                                                                        notify_on_widget = self.ui_master_data.textEdit_notify_film_videonorm,
                                                                        certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videonorm,
                                                                        sort_column = 1,
                                                                        category = 'film_videonorm'),

        ###################################################
        #################  Film Audioformat    ############
        ###################################################
                        "fetch_all_film_audioformat": lambda: self.fetch_all(
                              scoped_session = self._scoped_session,
                              configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                              hide_column = True,
                              general_standard_item_model = self._kwargs.get('standard_item_model'),
                              resize_column = 1,
                              populate_timer_interval = 1,
                              bulk_the_list = False,
                              general_proxy_model = self._kwargs.get('general_proxy_model'),
                              certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audioformat,
                              list_header = self. LIST_HORIZONTAL_HEADER_FILM_AUDIOFORMAT,
                              work_mode = 'fetch_all',
                              certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audioformat,
                              widget_label = self.ui_master_data.label_film_audioformat_counter,
                              widget_tree_view = self.ui_master_data.tree_view_film_audioformat,
                              show_messagebox = True,
                              category = 'film_audioformat'),

                        "add_film_audioformat": lambda: self.add_record(
                            film_audioformat = unicode(self.ui_master_data.lineEdit_film_audioformat.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_audioformat,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audioformat,
                            label_object = self.ui_master_data.label_film_audioformat_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_audioformat,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_audioformat'),

                        "edit_film_audioformat": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_audioformat = unicode(self.ui_master_data.lineEdit_edit_film_audioformat.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_audioformat,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audioformat,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_audioformat,
                            stack_widget = self.ui_master_data.stackedWidget_film_audioformat,
                            label_object = self.ui_master_data.label_film_audioformat_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_audioformat,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_audioformat'),

                        "delete_film_audioformat": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_audioformat,
                            line_edit_obj = self.ui_master_data.lineEdit_film_audioformat,
                            label_object = self.ui_master_data.label_film_audioformat_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audioformat,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_audioformat,
                            set_flush = True,
                            category = 'film_audioformat'),

                        "update_film_audioformat": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                          general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                          work_mode = 'fetch_online_data',
                                                                          bulk_the_list = True,
                                                                          set_page = True,
                                                                          widget_stack_widget = self.ui_master_data.stackedWidget_film_audioformat,
                                                                          page_index = 0,
                                                                          widget_line_edit = self.ui_master_data.lineEdit_film_audioformat,
                                                                          widget_label = self.ui_master_data.label_film_audioformat_counter,
                                                                          widget_tree_view = self.ui_master_data.tree_view_film_audioformat,
                                                                          notify_on_widget = self.ui_master_data.textEdit_notify_film_audioformat,
                                                                          certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audioformat,
                                                                          sort_column = 1,
                                                                          category = 'film_audioformat'),

        ###################################################
        #################  Film Audio Channal    ##########
        ###################################################
                        "fetch_all_film_audio_channel": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_audio_channel,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_AUDIOCHANNEL,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel,
                            widget_label = self.ui_master_data.label_film_audio_channel_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_audio_channel,
                            show_messagebox = True,
                            category = 'film_audio_channel'),

                        "add_film_audio_channel": lambda: self.add_record(
                            film_audio_channel = unicode(self.ui_master_data.lineEdit_film_audio_channel.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_audio_channel,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel,
                            label_object = self.ui_master_data.label_film_audio_channel_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_audio_channel,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_audio_channel'),

                        "edit_film_audio_channel": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_audio_channel = unicode(self.ui_master_data.lineEdit_edit_film_audio_channel.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_audio_channel,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_audio_channel,
                            stack_widget = self.ui_master_data.stackedWidget_film_audio_channel,
                            label_object = self.ui_master_data.label_film_audio_channel_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_audio_channel,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_audio_channel'),

                        "delete_film_audio_channel": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_audio_channel,
                            line_edit_obj = self.ui_master_data.lineEdit_film_audio_channel,
                            label_object = self.ui_master_data.label_film_audio_channel_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_audio_channel,
                            set_flush = True,
                            category = 'film_audio_channel'),

                        "update_film_audio_channel": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                            work_mode = 'fetch_online_data',
                                                                            bulk_the_list = True,
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_film_audio_channel,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_film_audio_channel,
                                                                            widget_label = self.ui_master_data.label_film_audio_channel_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_film_audio_channel,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_film_audio_channel,
                                                                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_audio_channel,
                                                                            sort_column = 1,
                                                                            category = 'film_audio_channel'),

        ###################################################
        #################  Film Colour Format    ##########
        ###################################################
                        "fetch_all_film_colour_format": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_colour_format,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_COLOURFORMAT,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_colour_format,
                            widget_label = self.ui_master_data.label_film_colour_format_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_colour_format,
                            show_messagebox = True,
                            category = 'film_colour_format'),

                        "add_film_colour_format": lambda: self.add_record(
                            film_colour_format = unicode(self.ui_master_data.lineEdit_film_colour_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_colour_format,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_colour_format,
                            label_object = self.ui_master_data.label_film_colour_format_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_colour_format,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_colour_format'),

                        "edit_film_colour_format": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_colour_format = unicode(self.ui_master_data.lineEdit_edit_film_colour_format.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_colour_format,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_colour_format,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_colour_format,
                            stack_widget = self.ui_master_data.stackedWidget_film_colour_format,
                            label_object = self.ui_master_data.label_film_colour_format_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_colour_format,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_colour_format'),

                        "delete_film_colour_format": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_colour_format,
                            line_edit_obj = self.ui_master_data.lineEdit_film_colour_format,
                            label_object = self.ui_master_data.label_film_colour_format_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_colour_format,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_colour_format,
                            set_flush = True,
                            category = 'film_colour_format'),

                        "update_film_colour_format": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                            work_mode = 'fetch_online_data',
                                                                            bulk_the_list = True,
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_film_colour_format,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_film_colour_format,
                                                                            widget_label = self.ui_master_data.label_film_colour_format_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_film_colour_format,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_film_colour_format,
                                                                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_colour_format,
                                                                            sort_column = 1,
                                                                            category = 'film_colour_format'),

        ###################################################
        #################  Film Videoformat    ############
        ###################################################
                        "fetch_all_film_videoformat": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_videoformat,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_VIDEOFORMAT,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videoformat,
                            widget_label = self.ui_master_data.label_film_videoformat_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_videoformat,
                            show_messagebox = True,
                            category = 'film_videoformat'),

                        "add_film_videoformat": lambda: self.add_record(
                            film_videoformat = unicode(self.ui_master_data.lineEdit_film_videoformat.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_videoformat,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videoformat,
                            label_object = self.ui_master_data.label_film_videoformat_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_videoformat,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_videoformat'),

                        "edit_film_videoformat": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_videoformat = unicode(self.ui_master_data.lineEdit_edit_film_videoformat.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_videoformat,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videoformat,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_videoformat,
                            stack_widget = self.ui_master_data.stackedWidget_film_videoformat,
                            label_object = self.ui_master_data.label_film_videoformat_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_videoformat,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'film_videoformat'),

                        "delete_film_videoformat": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_videoformat,
                            line_edit_obj = self.ui_master_data.lineEdit_film_videoformat,
                            label_object = self.ui_master_data.label_film_videoformat_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videoformat,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_videoformat,
                            set_flush = True,
                            category = 'film_videoformat'),

                        "update_film_videoformat": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                          general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                          work_mode = 'fetch_online_data',
                                                                          bulk_the_list = True,
                                                                          set_page = True,
                                                                          widget_stack_widget = self.ui_master_data.stackedWidget_film_videoformat,
                                                                          page_index = 0,
                                                                          widget_line_edit = self.ui_master_data.lineEdit_film_videoformat,
                                                                          widget_label = self.ui_master_data.label_film_videoformat_counter,
                                                                          widget_tree_view = self.ui_master_data.tree_view_film_videoformat,
                                                                          notify_on_widget = self.ui_master_data.textEdit_notify_film_videoformat,
                                                                          certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_videoformat,
                                                                          sort_column = 1,
                                                                          category = 'film_videoformat'),

        ###################################################
        #################  Film Soundsystem    ############
        ###################################################
                        "fetch_all_film_soundsystem": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_film_soundsystem,
                            list_header = self. LIST_HORIZONTAL_HEADER_FILM_SOUNDSYSTEM,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem,
                            widget_label = self.ui_master_data.label_film_soundsystem_counter,
                            widget_tree_view = self.ui_master_data.tree_view_film_soundsystem,
                            show_messagebox = True,
                            category = 'film_soundsystem'),

                        "add_film_soundsystem": lambda: self.add_record(
                            film_soundsystem = unicode(self.ui_master_data.lineEdit_film_soundsystem.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_soundsystem,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem,
                            label_object = self.ui_master_data.label_film_soundsystem_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_soundsystem,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'film_soundsystem'),

                        "edit_film_soundsystem": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            film_soundsystem = unicode(self.ui_master_data.lineEdit_edit_film_soundsystem.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_film_soundsystem,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_soundsystem,
                            stack_widget = self.ui_master_data.stackedWidget_film_soundsystem,
                            label_object = self.ui_master_data.label_film_soundsystem_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_film_soundsystem,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            work_area = "master_data",
                            category = 'film_soundsystem'),

                        "delete_film_soundsystem": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_film_soundsystem,
                            line_edit_obj = self.ui_master_data.lineEdit_film_soundsystem,
                            label_object = self.ui_master_data.label_film_soundsystem_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_film_soundsystem,
                            set_flush = True,
                            category = 'film_soundsystem'),

                        "update_film_soundsystem": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                          general_standard_item_model = self._kwargs.get('standard_item_model'),
                                                                          work_mode = 'fetch_online_data',
                                                                          bulk_the_list = True,
                                                                          set_page = True,
                                                                          widget_stack_widget = self.ui_master_data.stackedWidget_film_soundsystem,
                                                                          page_index = 0,
                                                                          widget_line_edit = self.ui_master_data.lineEdit_film_soundsystem,
                                                                          widget_label = self.ui_master_data.label_film_soundsystem_counter,
                                                                          widget_tree_view = self.ui_master_data.tree_view_film_soundsystem,
                                                                          notify_on_widget = self.ui_master_data.textEdit_notify_film_soundsystem,
                                                                          certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_film_soundsystem,
                                                                          sort_column = 1,
                                                                          category = 'film_soundsystem'),

        ###################################################
        #################  Music Studio    ################
        ###################################################
                        "fetch_all_music_studio": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_studio,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_STUDIO,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_studio,
                            widget_label = self.ui_master_data.label_music_studio_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_studio,
                            show_messagebox = True,
                            category = 'music_studio'),

                        "add_music_studio": lambda: self.add_record(
                            music_studio = unicode(self.ui_master_data.lineEdit_music_studio.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_studio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_studio,
                            label_object = self.ui_master_data.label_music_studio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_studio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_studio'),

                        "edit_music_studio": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_studio = unicode(self.ui_master_data.lineEdit_edit_music_studio.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_studio,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_studio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_studio,
                            stack_widget = self.ui_master_data.stackedWidget_music_studio,
                            label_object = self.ui_master_data.label_music_studio_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_studio,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_studio'),

                        "delete_music_studio": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_studio,
                            line_edit_obj = self.ui_master_data.lineEdit_music_studio,
                            label_object = self.ui_master_data.label_music_studio_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_studio,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_studio,
                            set_flush = True,
                            category = 'music_studio'),

                        "update_music_studio": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_studio,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_studio,
                            widget_label = self.ui_master_data.label_music_studio_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_studio,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_studio,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_studio,
                            sort_column = 1,
                            category = 'music_studio'),

        ###################################################
        #################  Music SPARS Code    ############
        ###################################################
                        "fetch_all_music_spars_code": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_spars_code,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_SPARS_CODE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_spars_code,
                            widget_label = self.ui_master_data.label_music_spars_code_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_spars_code,
                            show_messagebox = True,
                            category = 'music_spars_code'),

                        "add_music_spars_code": lambda: self.add_record(
                            music_spars_code = unicode(self.ui_master_data.lineEdit_music_spars_code.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_spars_code,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_spars_code,
                            label_object = self.ui_master_data.label_music_spars_code_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_spars_code,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_spars_code'),

                        "edit_music_spars_code": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_spars_code = unicode(self.ui_master_data.lineEdit_edit_music_spars_code.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_spars_code,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_spars_code,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_spars_code,
                            stack_widget = self.ui_master_data.stackedWidget_music_spars_code,
                            label_object = self.ui_master_data.label_music_spars_code_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_spars_code,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_spars_code'),

                        "delete_music_spars_code": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_spars_code,
                            line_edit_obj = self.ui_master_data.lineEdit_music_spars_code,
                            label_object = self.ui_master_data.label_music_spars_code_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_spars_code,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_spars_code,
                            set_flush = True,
                            category = 'music_spars_code'),

                        "update_music_spars_code": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_spars_code,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_spars_code,
                            widget_label = self.ui_master_data.label_music_spars_code_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_spars_code,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_spars_code,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_spars_code,
                            sort_column = 1,
                            category = 'music_spars_code'),

        ###################################################
        #################  Music Publisher    #############
        ###################################################
                        "fetch_all_music_publisher": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_publisher,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_PUBLISHER,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_publisher,
                            widget_label = self.ui_master_data.label_music_publisher_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_publisher,
                            show_messagebox = True,
                            category = 'music_publisher'),

                        "add_music_publisher": lambda: self.add_record(
                            music_publisher = unicode(self.ui_master_data.lineEdit_music_publisher.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_publisher,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_publisher,
                            label_object = self.ui_master_data.label_music_publisher_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_publisher,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_publisher'),

                        "edit_music_publisher": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_publisher = unicode(self.ui_master_data.lineEdit_edit_music_publisher.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_publisher,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_publisher,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_publisher,
                            stack_widget = self.ui_master_data.stackedWidget_music_publisher,
                            label_object = self.ui_master_data.label_music_publisher_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_publisher,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_publisher'),

                        "delete_music_publisher": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_publisher,
                            line_edit_obj = self.ui_master_data.lineEdit_music_publisher,
                            label_object = self.ui_master_data.label_music_publisher_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_publisher,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_publisher,
                            set_flush = True,
                            category = 'music_publisher'),

                        "update_music_publisher": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_publisher,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_publisher,
                            widget_label = self.ui_master_data.label_music_publisher_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_publisher,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_publisher,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_publisher,
                            sort_column = 1,
                            category = 'music_publisher'),

        ###################################################
        #################  Music Style    #################
        ###################################################
                        "fetch_all_music_style": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_style,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_STYLE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_style,
                            widget_label = self.ui_master_data.label_music_style_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_style,
                            show_messagebox = True,
                            category = 'music_style'),

                        "add_music_style": lambda: self.add_record(
                            music_style = unicode(self.ui_master_data.lineEdit_music_style.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_style,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_style,
                            label_object = self.ui_master_data.label_music_style_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_style,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_style'),

                        "edit_music_style": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_style = unicode(self.ui_master_data.lineEdit_edit_music_style.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_style,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_style,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_style,
                            stack_widget = self.ui_master_data.stackedWidget_music_style,
                            label_object = self.ui_master_data.label_music_style_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_style,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_style'),

                        "delete_music_style": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_style,
                            line_edit_obj = self.ui_master_data.lineEdit_music_style,
                            label_object = self.ui_master_data.label_music_style_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_style,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_style,
                            set_flush = True,
                            category = 'music_style'),

                        "update_music_style": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_style,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_style,
                            widget_label = self.ui_master_data.label_music_style_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_style,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_style,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_style,
                            sort_column = 1,
                            category = 'music_style'),

        ###################################################
        #################  Music Label    #################
        ###################################################
                        "fetch_all_music_label": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_label,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_LABEL,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_label,
                            widget_label = self.ui_master_data.label_music_label_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_label,
                            show_messagebox = True,
                            category = 'music_label'),

                        "add_music_label": lambda: self.add_record(
                            music_label = unicode(self.ui_master_data.lineEdit_music_label.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_label,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_label,
                            label_object = self.ui_master_data.label_music_label_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_label,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_label'),

                        "edit_music_label": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_label = unicode(self.ui_master_data.lineEdit_edit_music_label.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_label,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_label,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_label,
                            stack_widget = self.ui_master_data.stackedWidget_music_label,
                            label_object = self.ui_master_data.label_music_label_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_label,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_label'),

                        "delete_music_label": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_label,
                            line_edit_obj = self.ui_master_data.lineEdit_music_label,
                            label_object = self.ui_master_data.label_music_label_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_label,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_label,
                            set_flush = True,
                            category = 'music_label'),

                        "update_music_label": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_label,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_label,
                            widget_label = self.ui_master_data.label_music_label_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_label,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_label,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_label,
                            sort_column = 1,
                            category = 'music_label'),

        ###################################################
        #################  Music Distribution    ##########
        ###################################################
                        "fetch_all_music_distribution": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_music_distribution,
                            list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_DISTRIBUTION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_distribution,
                            widget_label = self.ui_master_data.label_music_distribution_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_distribution,
                            show_messagebox = True,
                            category = 'music_distribution'),

                        "add_music_distribution": lambda: self.add_record(
                            music_distribution = unicode(self.ui_master_data.lineEdit_music_distribution.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_distribution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_distribution,
                            label_object = self.ui_master_data.label_music_distribution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_distribution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'music_distribution'),

                        "edit_music_distribution": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            music_distribution = unicode(self.ui_master_data.lineEdit_edit_music_distribution.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_music_distribution,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_distribution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_distribution,
                            stack_widget = self.ui_master_data.stackedWidget_music_distribution,
                            label_object = self.ui_master_data.label_music_distribution_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_music_distribution,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'music_distribution'),

                        "delete_music_distribution": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_music_distribution,
                            line_edit_obj = self.ui_master_data.lineEdit_music_distribution,
                            label_object = self.ui_master_data.label_music_distribution_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_distribution,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_music_distribution,
                            set_flush = True,
                            category = 'music_distribution'),

                        "update_music_distribution": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_music_distribution,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_music_distribution,
                            widget_label = self.ui_master_data.label_music_distribution_counter,
                            widget_tree_view = self.ui_master_data.tree_view_music_distribution,
                            notify_on_widget = self.ui_master_data.textEdit_notify_music_distribution,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_music_distribution,
                            sort_column = 1,
                            category = 'music_distribution'),

        ###################################################
        #################  Book Genre    ##################
        ###################################################
                        "fetch_all_book_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_genre,
                            list_header = self. LIST_HORIZONTAL_HEADER_BOOK_GENRE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_genre,
                            widget_label = self.ui_master_data.label_book_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_genre,
                            show_messagebox = True,
                            category = 'book_genre'),

                        "add_book_genre": lambda: self.add_record(
                            book_genre = unicode(self.ui_master_data.lineEdit_book_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_genre,
                            label_object = self.ui_master_data.label_book_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'book_genre'),

                        "edit_book_genre": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            book_genre = unicode(self.ui_master_data.lineEdit_edit_book_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_genre,
                            stack_widget = self.ui_master_data.stackedWidget_book_genre,
                            label_object = self.ui_master_data.label_book_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'book_genre'),

                        "delete_book_genre": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_book_genre,
                            line_edit_obj = self.ui_master_data.lineEdit_book_genre,
                            label_object = self.ui_master_data.label_book_genre_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_genre,
                            set_flush = True,
                            category = 'book_genre'),

                        "update_book_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_book_genre,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_book_genre,
                            widget_label = self.ui_master_data.label_book_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_genre,
                            notify_on_widget = self.ui_master_data.textEdit_notify_book_genre,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_genre,
                            sort_column = 1,
                            category = 'book_genre'),

        ###################################################
        #################  Book Publisher    ##############
        ###################################################
                        "fetch_all_book_publisher": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_publisher,
                            list_header = self. LIST_HORIZONTAL_HEADER_BOOK_PUBLISHER,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_publisher,
                            widget_label = self.ui_master_data.label_book_publisher_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_publisher,
                            show_messagebox = True,
                            category = 'book_publisher'),

                        "add_book_publisher": lambda: self.add_record(
                            book_publisher = unicode(self.ui_master_data.lineEdit_book_publisher.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_publisher,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_publisher,
                            label_object = self.ui_master_data.label_book_publisher_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_publisher,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'book_publisher'),

                        "edit_book_publisher": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            book_publisher = unicode(self.ui_master_data.lineEdit_edit_book_publisher.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_publisher,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_publisher,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_publisher,
                            stack_widget = self.ui_master_data.stackedWidget_book_publisher,
                            label_object = self.ui_master_data.label_book_publisher_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_publisher,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'book_publisher'),

                        "delete_book_publisher": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_book_publisher,
                            line_edit_obj = self.ui_master_data.lineEdit_book_publisher,
                            label_object = self.ui_master_data.label_book_publisher_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_publisher,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_publisher,
                            set_flush = True,
                            category = 'book_publisher'),

                        "update_book_publisher": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_book_publisher,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_book_publisher,
                            widget_label = self.ui_master_data.label_book_publisher_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_publisher,
                            notify_on_widget = self.ui_master_data.textEdit_notify_book_publisher,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_publisher,
                            sort_column = 1,
                            category = 'book_publisher'),

        ###################################################
        #################  Book Binding    ################
        ###################################################
                        "fetch_all_book_binding": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_book_binding,
                            list_header = self. LIST_HORIZONTAL_HEADER_BOOK_BINDING,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_binding,
                            widget_label = self.ui_master_data.label_book_binding_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_binding,
                            show_messagebox = True,
                            category = 'book_binding'),

                        "add_book_binding": lambda: self.add_record(
                            book_binding = unicode(self.ui_master_data.lineEdit_book_binding.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_binding,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_binding,
                            label_object = self.ui_master_data.label_book_binding_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_binding,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'book_binding'),

                        "edit_book_binding": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            book_binding = unicode(self.ui_master_data.lineEdit_edit_book_binding.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_book_binding,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_binding,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_binding,
                            stack_widget = self.ui_master_data.stackedWidget_book_binding,
                            label_object = self.ui_master_data.label_book_binding_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_book_binding,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'book_binding'),

                        "delete_book_binding": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_book_binding,
                            line_edit_obj = self.ui_master_data.lineEdit_book_binding,
                            label_object = self.ui_master_data.label_book_binding_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_binding,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_book_binding,
                            set_flush = True,
                            category = 'book_binding'),

                        "update_book_binding": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_book_binding,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_book_binding,
                            widget_label = self.ui_master_data.label_book_binding_counter,
                            widget_tree_view = self.ui_master_data.tree_view_book_binding,
                            notify_on_widget = self.ui_master_data.textEdit_notify_book_binding,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_book_binding,
                            sort_column = 1,
                            category = 'book_binding'),

        ###################################################
        #################  Coin Alloy     #################
        ###################################################
                        "fetch_all_coin_alloy": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_alloy,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_ALLOY,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_alloy,
                            widget_label = self.ui_master_data.label_coin_alloy_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_alloy,
                            show_messagebox = True,
                            category = 'coin_alloy'),

                        "add_coin_alloy": lambda: self.add_record(
                            coin_alloy = unicode(self.ui_master_data.lineEdit_coin_alloy.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_alloy,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_alloy,
                            label_object = self.ui_master_data.label_coin_alloy_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_alloy,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_alloy'),

                        "edit_coin_alloy": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_alloy = unicode(self.ui_master_data.lineEdit_edit_coin_alloy.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_alloy,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_alloy,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_alloy,
                            stack_widget = self.ui_master_data.stackedWidget_coin_alloy,
                            label_object = self.ui_master_data.label_coin_alloy_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_alloy,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_alloy'),

                        "delete_coin_alloy": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_alloy,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_alloy,
                            label_object = self.ui_master_data.label_coin_alloy_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_alloy,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_alloy,
                            set_flush = True,
                            category = 'coin_alloy'),

                        "update_coin_alloy": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_alloy,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_alloy,
                            widget_label = self.ui_master_data.label_coin_alloy_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_alloy,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_alloy,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_alloy,
                            sort_column = 1,
                            category = 'coin_alloy'),

        ###################################################
        #################  Coin Currency     ##############
        ###################################################
                        "fetch_all_coin_currency": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_currency,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_CURRENCY,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_currency,
                            widget_label = self.ui_master_data.label_coin_currency_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_currency,
                            show_messagebox = True,
                            category = 'coin_currency'),

                        "add_coin_currency": lambda: self.add_record(
                            coin_currency = unicode(self.ui_master_data.lineEdit_coin_currency.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_currency,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_currency,
                            label_object = self.ui_master_data.label_coin_currency_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_currency,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_currency'),

                        "edit_coin_currency": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_currency = unicode(self.ui_master_data.lineEdit_edit_coin_currency.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_currency,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_currency,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_currency,
                            stack_widget = self.ui_master_data.stackedWidget_coin_currency,
                            label_object = self.ui_master_data.label_coin_currency_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_currency,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_currency'),

                        "delete_coin_currency": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_currency,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_currency,
                            label_object = self.ui_master_data.label_coin_currency_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_currency,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_currency,
                            set_flush = True,
                            category = 'coin_currency'),

                        "update_coin_currency": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_currency,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_currency,
                            widget_label = self.ui_master_data.label_coin_currency_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_currency,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_currency,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_currency,
                            sort_column = 1,
                            category = 'coin_currency'),

        ###################################################
        #################  Coin Type     ##################
        ###################################################
                        "fetch_all_coin_type": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_type,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_TYPE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_type,
                            widget_label = self.ui_master_data.label_coin_type_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_type,
                            show_messagebox = True,
                            category = 'coin_type'),

                        "add_coin_type": lambda: self.add_record(
                            coin_type = unicode(self.ui_master_data.lineEdit_coin_type.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_type,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_type,
                            label_object = self.ui_master_data.label_coin_type_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_type,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_type'),

                        "edit_coin_type": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_type = unicode(self.ui_master_data.lineEdit_edit_coin_type.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_type,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_type,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_type,
                            stack_widget = self.ui_master_data.stackedWidget_coin_type,
                            label_object = self.ui_master_data.label_coin_type_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_type,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_type'),

                        "delete_coin_type": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_type,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_type,
                            label_object = self.ui_master_data.label_coin_type_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_type,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_type,
                            set_flush = True,
                            category = 'coin_type'),

                        "update_coin_type": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_type,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_type,
                            widget_label = self.ui_master_data.label_coin_type_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_type,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_type,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_type,
                            sort_column = 1,
                            category = 'coin_type'),

        ###################################################
        #################  Videogame Genre     ############
        ###################################################
                        "fetch_all_videogame_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_videogame_genre,
                            list_header = self. LIST_HORIZONTAL_HEADER_VIDEOGAME_GENRE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_videogame_genre,
                            widget_label = self.ui_master_data.label_videogame_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_videogame_genre,
                            category = 'videogame_genre'),

                        "add_videogame_genre": lambda: self.add_record(
                            videogame_genre = unicode(self.ui_master_data.lineEdit_videogame_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_videogame_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_videogame_genre,
                            label_object = self.ui_master_data.label_videogame_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_videogame_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'videogame_genre'),

                        "edit_videogame_genre": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            videogame_genre = unicode(self.ui_master_data.lineEdit_edit_videogame_genre.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_videogame_genre,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_videogame_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_videogame_genre,
                            stack_widget = self.ui_master_data.stackedWidget_videogame_genre,
                            label_object = self.ui_master_data.label_videogame_genre_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_videogame_genre,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'videogame_genre'),

                        "delete_videogame_genre": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_videogame_genre,
                            line_edit_obj = self.ui_master_data.lineEdit_videogame_genre,
                            label_object = self.ui_master_data.label_videogame_genre_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_videogame_genre,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_videogame_genre,
                            set_flush = True,
                            category = 'videogame_genre'),

                        "update_videogame_genre": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_videogame_genre,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_videogame_genre,
                            widget_label = self.ui_master_data.label_videogame_genre_counter,
                            widget_tree_view = self.ui_master_data.tree_view_videogame_genre,
                            notify_on_widget = self.ui_master_data.textEdit_notify_videogame_genre,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_videogame_genre,
                            sort_column = 1,
                            category = 'videogame_genre'),

        ###################################################
        #################  Coin Manufacturing Procedure  ##
        ###################################################
                        "fetch_all_coin_manufacturing_procedure": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_manufacturing_procedure,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_MANUFACTURING_PROCEDURE,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure,
                            widget_label = self.ui_master_data.label_coin_manufacturing_procedure_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_manufacturing_procedure,
                            show_messagebox = True,
                            category = 'coin_manufacturing_procedure'),

                        "add_coin_manufacturing_procedure": lambda: self.add_record(
                            coin_manufacturing_procedure = unicode(self.ui_master_data.lineEdit_coin_manufacturing_procedure.text()).strip(),
                            coin_manufacturing_procedure_shortcut = unicode(self.ui_master_data.lineEdit_coin_manufacturing_procedure_shortcut.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure,
                            label_object = self.ui_master_data.label_coin_manufacturing_procedure_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_manufacturing_procedure,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_manufacturing_procedure'),

                        "edit_coin_manufacturing_procedure": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_manufacturing_procedure = unicode(self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure.text()).strip(),
                            coin_manufacturing_procedure_shortcut = unicode(self.ui_master_data.lineEdit_edit_coin_manufacturing_procedure_shortcut.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_manufacturing_procedure,
                            stack_widget = self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                            label_object = self.ui_master_data.label_coin_manufacturing_procedure_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_manufacturing_procedure,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_manufacturing_procedure'),

                        "delete_coin_manufacturing_procedure": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_manufacturing_procedure,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                            label_object = self.ui_master_data.label_coin_manufacturing_procedure_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_manufacturing_procedure,
                            set_flush = True,
                            category = 'coin_manufacturing_procedure'),

                        "update_coin_manufacturing_procedure": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_manufacturing_procedure,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_manufacturing_procedure,
                            widget_label = self.ui_master_data.label_coin_manufacturing_procedure_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_manufacturing_procedure,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_manufacturing_procedure,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_manufacturing_procedure,
                            sort_column = 1,
                            category = 'coin_manufacturing_procedure'),

        ###################################################
        #################  Coin Degree Preservation  ######
        ###################################################
                        "fetch_all_coin_degree_preservation": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_degree_preservation,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_DEGREE_PRESERVATION,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation,
                            widget_label = self.ui_master_data.label_coin_degree_preservation_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_degree_preservation,
                            show_messagebox = True,
                            category = 'coin_degree_preservation'),

                        "add_coin_degree_preservation": lambda: self.add_record(
                            coin_degree_preservation = unicode(self.ui_master_data.lineEdit_coin_degree_preservation.text()).strip(),
                            coin_degree_preservation_shortcut = unicode(self.ui_master_data.lineEdit_coin_degree_preservation_shortcut.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_degree_preservation,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation,
                            label_object = self.ui_master_data.label_coin_degree_preservation_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_degree_preservation,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_degree_preservation'),

                        "edit_coin_degree_preservation": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_degree_preservation = unicode(self.ui_master_data.lineEdit_edit_coin_degree_preservation.text()).strip(),
                            coin_degree_preservation_shortcut = unicode(self.ui_master_data.lineEdit_edit_coin_degree_preservation_shortcut.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_degree_preservation,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_degree_preservation,
                            stack_widget = self.ui_master_data.stackedWidget_coin_degree_preservation,
                            label_object = self.ui_master_data.label_coin_degree_preservation_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_degree_preservation,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_degree_preservation'),

                        "delete_coin_degree_preservation": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_degree_preservation,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_degree_preservation,
                            label_object = self.ui_master_data.label_coin_degree_preservation_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_degree_preservation,
                            set_flush = True,
                            category = 'coin_degree_preservation'),

                        "update_coin_degree_preservation": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_degree_preservation,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_degree_preservation,
                            widget_label = self.ui_master_data.label_coin_degree_preservation_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_degree_preservation,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_degree_preservation,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_degree_preservation,
                            sort_column = 1,
                            category = 'coin_degree_preservation'),

        ###################################################
        #################  Coin Mint  #####################
        ###################################################
                        "fetch_all_coin_mint": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                            hide_column = True,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            resize_column = 1,
                            populate_timer_interval = 1,
                            bulk_the_list = False,
                            general_proxy_model = self._kwargs.get('general_proxy_model'),
                            certain_proxy_model = self._kwargs.get('general_proxy_model').proxy_model_film_general_coin_mint,
                            list_header = self. LIST_HORIZONTAL_HEADER_COIN_MINT,
                            work_mode = 'fetch_all',
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_mint,
                            widget_label = self.ui_master_data.label_coin_mint_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_mint,
                            show_messagebox = True,
                            category = 'coin_mint'),

                        "add_coin_mint": lambda: self.add_record(
                            coin_mint = unicode(self.ui_master_data.lineEdit_coin_mint.text()).strip(),
                            coin_mint_mark = unicode(self.ui_master_data.lineEdit_coin_mint_mark.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_mint,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_mint,
                            label_object = self.ui_master_data.label_coin_mint_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_mint,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "add_record",
                            work_area = "master_data",
                            category = 'coin_mint'),

                        "edit_coin_mint": lambda: self.add_record(
                            id = self.selected_be_edited_id,
                            coin_mint = unicode(self.ui_master_data.lineEdit_edit_coin_mint.text()).strip(),
                            coin_mint_mark = unicode(self.ui_master_data.lineEdit_edit_coin_mint_mark.text()).strip(),
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_coin_mint,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_mint,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_mint,
                            stack_widget = self.ui_master_data.stackedWidget_coin_mint,
                            label_object = self.ui_master_data.label_coin_mint_counter,
                            resize_column = 1,
                            widget_tree_view = self.ui_master_data.tree_view_coin_mint,
                            show_messagebox = True,
                            set_flush = True,
                            work_mode = "edit_record",
                            category = 'coin_mint'),

                        "delete_coin_mint": lambda: self.delete_record(
                            id = self.selected_be_edited_id,
                            tree_view_object = self.ui_master_data.tree_view_coin_mint,
                            line_edit_obj = self.ui_master_data.lineEdit_coin_mint,
                            label_object = self.ui_master_data.label_coin_mint_counter,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_mint,
                            temp_model = self._kwargs.get('standard_item_model').temporary_standard_item_model_film_general_coin_mint,
                            set_flush = True,
                            category = 'coin_mint'),

                        "update_coin_mint": lambda: self.fetch_all(
                            scoped_session = self._scoped_session,
                            general_standard_item_model = self._kwargs.get('standard_item_model'),
                            work_mode = 'fetch_online_data',
                            bulk_the_list = True,
                            set_page = True,
                            widget_stack_widget = self.ui_master_data.stackedWidget_coin_mint,
                            page_index = 0,
                            widget_line_edit = self.ui_master_data.lineEdit_coin_mint,
                            widget_label = self.ui_master_data.label_coin_mint_counter,
                            widget_tree_view = self.ui_master_data.tree_view_coin_mint,
                            notify_on_widget = self.ui_master_data.textEdit_notify_coin_mint,
                            certain_standard_item_model = self._kwargs.get('standard_item_model').standard_item_model_coin_mint,
                            sort_column = 1,
                            category = 'coin_mint'),

                        }

        try:
            function_first = dict_methods[method_name]
            function_first()

        except KeyError as key_err:
            pass#print "key_err", key_err

        return
        pass

        return


    def manage_pages(self,
                    page_id = None):
        '''
           SUMMARY
           =======
           This method manages all pages.

           PARAMETERS
           ==========
           :page_id (int): The given ID cahnges the page.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        dict_pages = {
                        1: lambda: self.manage_methods(method_name = "fetch_all_film_soundsystem"),
                        2: lambda: self.manage_methods(method_name = "fetch_all_film_genre"),
                        3: lambda: self.manage_methods(method_name = "fetch_all_film_regional_code"),
                        4: lambda: self.manage_methods(method_name = "fetch_all_film_aspect_ratio"),
                        5: lambda: self.manage_methods(method_name = "fetch_all_film_audioformat"),
                        6: lambda: self.manage_methods(method_name = "fetch_all_film_audio_channel"),
                        7: lambda: self.manage_methods(method_name = "fetch_all_film_colour_format"),
                        8: lambda: self.manage_methods(method_name = "fetch_all_film_videonorm"),
                        9: lambda: self.manage_methods(method_name = "fetch_all_film_resolution"),
                        10: lambda: self.manage_methods(method_name = "fetch_all_film_videoformat"),
                        11: lambda: self.manage_methods(method_name = "fetch_all_film_version"),
                        12: lambda: self.manage_methods(method_name = "fetch_all_film_dimension"),
                        13: lambda: self.manage_methods(method_name = "fetch_all_film_edition"),
                        14: lambda: self.manage_methods(method_name = "fetch_all_film_production_company"),
                        15: lambda: self.manage_methods(method_name = "fetch_all_film_distribution"),
                        16: lambda: self.manage_methods(method_name = "fetch_all_film_rentals"),
                        17: lambda: self.manage_methods(method_name = "fetch_all_film_studio"),
                        19: lambda: self.manage_methods(method_name = "fetch_all_music_genre"),
                        20: lambda: self.manage_methods(method_name = "fetch_all_music_style"),
                        21: lambda: self.manage_methods(method_name = "fetch_all_music_studio"),
                        22: lambda: self.manage_methods(method_name = "fetch_all_music_publisher"),
                        23: lambda: self.manage_methods(method_name = "fetch_all_music_label"),
                        24: lambda: self.manage_methods(method_name = "fetch_all_music_distribution"),
                        25: lambda: self.manage_methods(method_name = "fetch_all_music_spars_code"),
                        27: lambda: self.manage_methods(method_name = "fetch_all_book_genre"),
                        28: lambda: self.manage_methods(method_name = "fetch_all_book_publisher"),
                        29: lambda: self.manage_methods(method_name = "fetch_all_book_binding"),
                        33: lambda: self.manage_methods(method_name = "fetch_all_videogame_genre"),
                        35: lambda: self.manage_methods(method_name = "fetch_all_person_gender"),
                        36: lambda: self.manage_methods(method_name = "fetch_all_person_salutation"),
                        37: lambda: self.manage_methods(method_name = "fetch_all_person_title"),
                        38: lambda: self.manage_methods(method_name = "fetch_all_person_nationality"),
                        39: lambda: self.manage_methods(method_name = "fetch_all_person_eye_color"),
                        40: lambda: self.manage_methods(method_name = "fetch_all_person_hair_color"),
                        41: lambda: self.manage_methods(method_name = "fetch_all_person_religion"),
                        42: lambda: self.manage_methods(method_name = "fetch_all_person_relationship_status"),
                        43: lambda: self.manage_methods(method_name = "fetch_all_person_relationship"),
                        47: lambda: self.manage_methods(method_name = "fetch_all_coin_type"),
                        48: lambda: self.manage_methods(method_name = "fetch_all_coin_alloy"),
                        49: lambda: self.manage_methods(method_name = "fetch_all_coin_currency"),
                        50: lambda: self.manage_methods(method_name = 'fetch_all_coin_manufacturing_procedure'),
                        51: lambda: self.manage_methods(method_name = 'fetch_all_coin_degree_preservation'),
                        52: lambda: self.manage_methods(method_name = 'fetch_all_coin_mint'),
                        53: lambda: self.manage_methods(method_name = "fetch_all_general_age_release"),
                        54: lambda: self.manage_methods(method_name = "fetch_all_general_packaging"),
                        56: lambda: self.manage_methods(method_name = "fetch_all_general_media_type"),
                        57: lambda: self.manage_methods(method_name = "fetch_all_general_manufacturer"),
                        58: lambda: self.manage_methods(method_name = "fetch_all_general_language"),
                        59: lambda: self.manage_methods(method_name = "fetch_all_general_award"),
                        60: lambda: self.manage_methods(method_name = "fetch_all_general_country"),
                        61: lambda: self.manage_methods(method_name = "fetch_all_general_region")

                     }

        try:
            function_first = dict_pages[int(page_id)]
            function_first()

##            function_first, function_second = dict_stack_page[int(stack_page_id)]
##            function_first()
##            function_second()

        except KeyError as key_err:
            pass#print "key_err", key_err

        return

    def change_page(self,
                    **kwargs):
        '''
           SUMMARY
           =======
           This method is used to change the view of stacked widget using setCurrentIndex()
           and its fades between widgets on different pages - dependent on the index.

           EXTENTED DISCRIPTION
           ====================
           This method not only changes the view of stacked widget, but it also takes care of
           the other widgets (at the moment: QTreeView, QLineEdit, QPlainTextEdit and QTextEdit)
           that are on the old and current page.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window / change_page is called successfully"
        print "MasterData_Window in change_page / ***kwargs: -> \n {}".format(kwargs)

        logger.info("change the widget of QStackedWidget")

        #   First, before we start to change the view of stacked widget,
        #   we have to stop all runing threads.
        #self.quite_threads()

        #   We have to check if the given argument (new_index) isn't a
        #   None object (singleton object). Sometimes, its a NoneType.
        if not isinstance(kwargs.get('new_index', None), type(None)):

            #   Its necessary to to convert it with toPyObject(), because version 1
            #   of the QVariant API provides the QVariant.toPyObject() method to convert
            #   the QVariant back to a Python object of the correct type.
            #   Both versions will raise a Python exception if the conversion cannot be done.
            try:

                kwargs['index'] = kwargs['new_index'].indexes()[1].data().toPyObject()

            except AttributeError:

                kwargs['index'] = kwargs['new_index'].indexes()[1].data()

        #   We save the given index as a current page index.
        self._kwargs['page_number'] = int(kwargs['index'])

            #   When the user changes the page of QStackedWidget all Qt-bjects
            #   (QLineEdit, QTextEdit, QPlainTextEdit, QTreeView), which are
            #   children of current widget, are deleted or cleared. That means,
            #   when the user leaves the current page and goes to the other new page,
            #   all QLineEdit()-object, which are childrem from old pages, should be
            #   deleted or cleared.

            #   So we need to check, if the variables named (self._new_page) is not None.
        if not isinstance(self._new_page, type(None)):

                #   Yes, its not None. The variable isn't empty.
                #   Great. Next step, we save the current index in the protected variable named
                #   (self._new_page).
            self._new_page = int(kwargs['index'])

                #   After the new index has been saved, we create a list of all Qt-objects
                #   (QLineEdit, QTextEdit, QPlainTextEdit, QTreeView), which are children of
                #   the old stack page, that the user was left. In this case we
                #   need to use the old page number, which is saved in the variable named (self._old_stack_page).
                #   The created list we sent it to the method called self.clear_widget_object_content().

            if kwargs.get('clear_widget', True):
                #   Yes we want to clear all Qt-object
                self.clear_widget_object_content(
                    list_widget_object = self.ui_master_data.stackedWidget.widget(int(self._old_stack_page)).findChildren((
                      QLineEdit,
                      QTextEdit,
                      QPlainTextEdit)))

            #   After all Qt-objects (QLineEdit, QTextEdit, QPlainTextEdit, QTreeView) has beend deleted
            #   or cleared we set the old page with the new page. That means, we overwrite the old page
            #   number with the given new page number. We create a copy of them.
            self._old_stack_page = self._new_page

        else:
            #   The protected variable named 'self._new_page' is None.
            #   Maybe the user starts this window at first time. Now we have to set the new and old page at the
            #   same time.
            self._new_page = kwargs['index']
            self._old_stack_page = kwargs['index']

            if kwargs.get('clear_widget', True):
                #   Yes we want to clear all Qt objects
                self.clear_widget_object_content(
                    list_widget_object= self.ui_master_data.stackedWidget.widget(self._old_stack_page).findChildren((
                      QLineEdit,
                      QTextEdit,
                      QPlainTextEdit)))

        #   Is there a QLineEdit()? If None, there isn't.
        if not kwargs.get('lineEdit_object', None) is None:
            #   Yes, there is a QlineEdit(), let us set text on it.
            self.set_text(widget = kwargs['lineEdit_object'], text = kwargs['set_text'])

        #   Is there a fully list of QLineEdit()s? If None, there arent.
        elif not kwargs.get('list_tuple_widget_text', None) is None:

            for lineEdit_object, text in kwargs.get('list_tuple_widget_text'):
                self.set_text(widget = lineEdit_object, text = text)

        #   Does the user want QLineEdit() to get the focus?
        if kwargs.get('set_focus', False):
            #   Yes, he wants it. Set the focus in.
            self.set_focus(widget_object = kwargs['lineEdit_object'])

        #   Does the user want to load and populate records from database?
        if kwargs.get('load_data', False):
            #   Yes, lets load and populate the records.
            self.manage_pages(kwargs['index'])

        # The current page can change when inserting an index.
        if not kwargs.get('stack_widget') is None:
            kwargs.get('stack_widget').setCurrentIndex(int(kwargs['index']))

        #   In this case, we look, if the user wants to clear
        #   all QTreeView() objects in current page.
        if kwargs.get('current_obj_clear_selection', False):

            #   Yes, he wants it. Lets us find all QTreView() object, then clear it, by using a generator.
            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QTreeView))

            for widget in self.generator_list_widget_object(clear_selection_list):

                widget.clearSelection()

        #   Here we want take a look, if the user wants the programm to clear
        #   all QLineEdit(), QTextEdit, QPlainTextEdit.
        if kwargs.get('current_obj_clear', False) :

            # Yes, so let us find all of them and clear it, by using a generator.
            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QLineEdit, QTextEdit, QPlainTextEdit))

            for widget in self.generator_list_widget_object(clear_selection_list):

                widget.clear()

        #   Last but not least, we have to look if the user wants to remove ID,
        #   which are save in two variables.
        if kwargs.get('delete_selected_id', False):
            #   Yes, we don't need the IDs anymore. Let us clear the
            #   variables where the IDs are saved.
            self.remove_saved_record_id(record_id = self.current_selected_id)
            self.remove_saved_record_id(record_id = self.selected_be_edited_id)

        return

    def remove_saved_record_id(self,
                               record_id):
        '''
           SUMMARY
           =======
           This method deletes the variable named (self.current_selected_id),
           which we don't need it no longer.

           PARAMETERS
           ==========
           :record_id (int):    We get a variable, which schould be set on None.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the variable (current_selected_id) on None")

        current_id = None

        return

    def populate_navi_data(self,
                           model_obj,
                           data):
        '''
           SUMMARY
           =======
           We have to popuolate the navigation menu programmatically.

           PARAMETERS
           ==========
           :model_obj (QStandardItem):  we need a model where the data should be populated.

           :data (list):                This argument contains data to populate and display.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Populate the TreeView of Navigate")

        try:

            for parent_text, child_text in data:
                count_item = QStandardItem('{}'.format(self._navi_count))

                if not isinstance(parent_text, list):
                    item = QStandardItem(parent_text)
                    model_obj.appendRow([item, count_item])
                    self._navi_count += 1
                    #print "Item:", unicode(parent_text), "Count:", self._navi_count

                    if child_text:
                        self.populate_navi_data(item, child_text)

        except TypeError:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def standard_item_model(self):
        '''
           SUMMARY
           =======
           Create an empty model for the Navi-TreeView.

           EXTENTED DISCRIPTION
           ====================
           QTreeView needs a model to manage its data.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set QStandardItemModel on Navigate-Model")

        self._navigate_standard_model = QStandardItemModel()

        return

    def set_hide_column(self,
                        column,
                        bool_value,
                        list_tree_view_object = None,
                        tree_view_object = None):
        '''
           SUMMARY
           =======
           The given column will be hidden, otherwise it will be shown

           PARAMETERS
           ==========
           :list_tree_view_object (list):   This argument contains a list of QTreeView()-objects

           :tree_view_object (QTreeView):   It contains a single QTreeView()-object

           :column (int):                   We need a number to hide a special column.

           :bool_value (bool):              Boolean to hide, when it is True.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set column hidden on given Qt-object")

        if not tree_view_object is None:
            tree_view_object.setColumnHidden(column, bool_value)

        if isinstance(list_tree_view_object, list):
            for tree_view in list_tree_view_object:
                tree_view.setColumnHidden(column, bool_value)

        return

    def hide_widget(self,
                    widget,
                    boolen_value):
        '''
           SUMMARY
           =======
           The given widget will be hidden, otherwise it will be shown.

           EXTENTED DISCRIPTION
           ====================
           This method uses an exception of AttributeError. If setVisible() doesn't
           work, the exception attemps to call setHidden() on the given qt-widget.

           PARAMETERS
           ==========
           :widget (Qt-Object):     Expect a widget-object to hide.

           :bool_value (bool):      Boolean to hide, when it is True.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window hide_widget is called successfully"
        print "MasterData_Window in hide_widget / Widget.objectName(): -> \n {}".format(widget.objectName())
        print "MasterData_Window in hide_widget / boolen_value: -> \n {}".format(boolen_value)
        logger.info("Set visible or hidden on given Qt-object")

        try:

            widget.setVisible(boolen_value)

        except AttributeError:

            widget.setHidden(boolen_value)

        return


    def set_text(self,
                 widget = None,
                 text = None):
        '''
           SUMMARY
           =======
           Updates the input each time a key is pressed.

           PARAMETERS
           ==========
           :widget (Qt-Object): On which qt object should be shown the text?

           :text (unicode):     We need text to set it.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window set_text is called successfully"
        print "MasterData_Window in set_text / text: -< \n {}".format(text)

        logger.info("Set the text given Qt-object.")

        if isinstance(widget, QLineEdit):
            widget.setText(text)
        elif isinstance(widget, QProgressBar):
            widget.setText(text)
        elif isinstance(widget, QLabel):
            widget.setText(text)

        return

    def set_focus(self,
                  widget_object,
                  bool_value = True):

        '''
           SUMMARY
           =======
           This method is used to set the focus on given QLineEdit()-object.

           EXTENTED DISCRIPTION
           ====================
           This method is necessary, because the setFocus()-function is used
           in several places.

           PARAMETERS
           ==========
           :widget_object (Qt-object):  We need the QLineEdit()-object to set the focus in.

           :bool_value (bool):          By default, its on True, which menas, the given Qt-object
                                        will be visible.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the focus on given QLineEdit()-object.")

        if not widget_object is None:
            widget_object.setFocus(bool_value)

        return

    def set_maximum_value(self,
                          value = True):
        '''
           SUMMARY
           =======
           This method is used to set maximum value of QProgressBar().

           EXTENTED DISCRIPTION
           ====================
           And its also used to show a QProgressbar()-object in busy mode with text instead of a
           percenttage of steps, because minimum and maximum are both set to 0.

           PARAMETERS
           ==========
           :value (bool):       Get a vbalue to set the maximum value of progress bar, it
                                can be any int and it will be automatically converted to x/100% values
                                e.g. max_value = 3, current_value = 1, the progress bar will show 33%.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set maximun on busy_progress_bar")

        try:

            self.hide_widget(widget = self.ui_master_data.frame_progress_bar, boolen_value = True)
            self.busy_progress_bar.setMaximum(value)

        except AttributeError:

            pass

        return

    def set_model(self,
                 widget_tree_view,
                 certain_proxy_model):
        print ""
        print "MasterData_Window set_model is called"
        print "MasterData_Window set_model in / widget_tree_view.objectName(): \n ->  {}".format(widget_tree_view.objectName())
        print "MasterData_Window in set_model / certain_proxy_model.objectName(): \n ->  {}".format(certain_proxy_model.objectName())
        '''
           SUMMARY
           =======
           Sets the model for the view to present.

           PARAMETERS
           ==========
           :widget (Qt-object):     we except a widget for the view.

           :model (QStandardItemModel):      We required a model to set it to the widget.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set given model on given Qt-object")

        try:

          widget_tree_view.setModel(certain_proxy_model)

          self.init_selectionChanged_signal_treeview()
          self.close_wait_message_signal.emit()

        except AttributeError:

          pass

        return

    def update_progress_bar(self,
                            progress_bar_object,
                            process_value = 0,
                            final_count = 0):
        '''
           SUMMARY
           =======
           The given QProgressBar()-object will update its value, so it will suppose
           to progress while the items are populate.

           EXTENTED DISCRIPTION
           ====================
           This method is also used to set range to minimum = 0 and to maximum = final_count.

           PARAMETERS
           ==========
           :progress_bar_object (QProgressbar):     Let me know which QProgressbar should be updated.

           :process_value (int):                    Current number of processes.

           :final_count (int):                      Total number of rows in a table. This method is also
                                                    used to stop the busy indicator of given
                                                    QProgressBar()-object by using setValue()-method.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary,
                                                    but this statement gives me a better feeling, that says: 'return'
                                                    terminates a function definitely.
        '''
        logger.info("Update the QProgressbar")

        self._proccessed_progressbar = process_value
        try:

            progress_bar_object.setRange(0, final_count)

            progress_bar_object.setValue(process_value)

        except (TypeError, AttributeError):

            pass

        return


    def get_current_index(self,
                          widget):
        '''
           SUMMARY
           =======
           This method is used to hold the index position of the widget that is visible.

           PARAMETERS
           ==========
           :widget (Qt-object): We need the widget to determine the current index.

           RETURNS
           =======
           :return:             Its returns the current index of the given widget.
        '''
        logger.info("Find out current index")

        return widget.currentIndex()

    def append_text(self,
                    widget,
                    text = None,
                    clear_widget = False):
        '''
           SUMMARY
           =======
           This function is used to upload file.

           PARAMETERS
           ==========
           :protected_url (str):    Here we just the clear url to the protected area, not to the upload-page.

           :upload_url (str):       We ecxept a url to the formular, which initialize the upload.

           :user (unicode):         The user name of this protected area.

           :pwd str (unicode):      Password is import to login to protected area successfully.

           :file_path (unicode):    We need the path to the image file which we would to upload it.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Append text to given Qt-object")

        if not widget is None:

            if clear_widget:
                widget.clear()

            if not text is None:
                widget.append(text)

        return

    def set_count_text(self,
                         widget,
                         row_count):
        '''
           SUMMARY
           =======
           This method is used to set the text of counter and text.

           PARAMETERS
           ==========
           :widget (Qt-object):                             Usually, we get QLabel to show the counter and text.

           :standard_item_model (QStandardItemModel):       We need a model, where we get the count of saved items.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set counter and Text on given Qt-object")

        result = self.tr("Result(s):")
        counter = str(row_count)

        together = result + " " + counter

        widget.setText(together)

        return

    def convert_string_to_bool(self,
                               value):
        '''
           SUMMARY
           =======
           This method converts a given string in boolean value.

           EXTENTED DISCRIPTION
           ====================
           In the ini file, where all settings are saved, there are only strings
           (for example: 'True' or 'False'). All values aren't boolean value.
           so we have to convert all corresponding strings before.

           PARAMETERS
           ==========

           :value (str):    We get a bool as string.

           RETURNS
           =======
           :return:         In the try block its returns the converted value. But, when
                            the operation fails its returns False. In this case its
                            possible that the user or a other software has manipulated
                            the contents of the ini file.
        '''

        try:

            #   Let us try to invoke the function by passing an argument. In this case, we access
            #   to a dictionary at first, because the loaded settings are saved in the dictionary.
            #   We use the key (its the argument named value) to get the value, that is saved behind
            #   the key. With the obtained value we try to convert it in the bool-
            logger.info("Try to invoke a parsing_str_to_bool()-function by passing an {}-argument".format(value))

            return parsing_str_to_bool(value)

        except ValueError as VaErr:

            #   Ups, an error has occurred. Maybe the user or other program has changed
            #   the content of this ini file. We write this error in the log file and
            #   return False. That means, the current QCheckBox()-object isn't check. The
            #   user will see an uncheck QCheckBox()-object.

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            return False

    def clear_tree_view_selection(self,
                                  tree_view ):

        '''
           SUMMARY
           =======
           This method is used to deselect all selected items in the given QTreeView.

           PARAMETERS
           ==========
           :tree_view (QTreeView):    We need QTreeView to deselect all selected items on that.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Deselect all selected item")

        tree_view.selectionModel().clearSelection()

        return

    def display_counting(self,
                         widget,
                         count):
        '''
           SUMMARY
           =======
           Show the count and text on given qt-object (Usually its QLabel).

           PARAMETERS
           ==========
           :widget (qt object):     On which widget should be displayed the counter?

           :count (int):            In this case we ecpect int()-value(s) to display
                                    the counter on given widget.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        result = self.tr("Result(s):")
        counter = str(count)
        togehter = result + " " + counter
        widget.setText(togehter)

        return

    def sort_model(self,
                    work_mode,
                    line_edit_object_get_focus,
                    model,
                    column,
                    page_index = None,
                    stack_widget_object = None,
                    current_obj_clear_selection = False,
                    delete_selected_id = False,
                    current_obj_clear = False,
                    change_page = False,
                    order = Qt.AscendingOrder):
        '''
           SUMMARY
           =======
           This method sorts the given model.

           PARAMETERS
           ==========
           :work_mode (str):                        Say what working mode we are in.

           :line_edit_object_get_focus (QLineEdit): Which QLineEdit should get the focus in?

           :model (QStandardItemModel):             We expect a model which should be sorted.

           :column (int):                           For sorting a given model we need a column.

           :page_index (int):                       We get the index of the page we want to change to.

           :stack_widget_object (QStackedWidget):   It just a QStackedWidget()-object.

           :current_obj_clear_selection (bool):     We ecxept a url to the formular, which initialize the upload.

           :delete_selected_id (bool):              By default its False, All saved IDs shouldn't be deleted. Otherwise
                                                    remove all IDs.

           :current_obj_clear (bool):               By default its False. No widget on current page should be cleaned. Otherwise clear
                                                    all widgets on current page.

           :change_page (bool):                     By default, its False. No page on stacked widget should be changed. Otherwise
                                                    it changes it.

           :order:                                  By default its Qt.AscendingOrder. The items are sorted ascending. Its sorts the model
                                                    in the given order.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window sort_model is called successfully"
        print "MasterData_Window in sort_model / change_page: \n ->{}".format(change_page)
        print "MasterData_Window in sort_model / current_obj_clear: \n ->{}".format(current_obj_clear)
        print "MasterData_Window in sort_model / delete_selected_id: \n ->{}".format(delete_selected_id)
        print "MasterData_Window in sort_model / current_obj_clear_selection: \n ->{}".format(current_obj_clear_selection)
        print "MasterData_Window in sort_model / stack_widget_object.objectName(): \n ->{}".format(stack_widget_object.objectName())
        print "MasterData_Window in sort_model / page_index: \n ->{}".format(page_index)
        print "MasterData_Window in sort_model / column: \n ->{}".format(column)
        print "MasterData_Window in sort_model / model.objectName(): \n ->{}".format(model.objectName())
        print "MasterData_Window in sort_model / work_mode: \n ->{}".format(work_mode)
        print "MasterData_Window in sort_model / line_edit_object_get_focus.objectName(): \n ->{}".format(line_edit_object_get_focus.objectName())

        logger.info("Sort the given model")

        #model.sort(column, order)

        self.close_wait_message_signal.emit()

        if work_mode == "add_record":

            self.create_generally_information_msg(title = self.tr('Confirmation'),
                                                  msg = self.tr("The item has been saved into the database successfully."),
                                                  detail_msg = self.tr(''),
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = line_edit_object_get_focus,
                                                  only_yes_button = True,
                                                  icon = self._kwargs.get('image_path_collection').info_48x48_default,
                                                  set_flag = True)

        # if work_mode == "edit_record":

        #     self.create_generally_information_msg(title = self.tr('Confirmation'),
        #                                           msg = self.tr("The item has been changed into the database successfully."),
        #                                           detail_msg = self.tr(''),
        #                                           yes_no_button = False,
        #                                           line_edit_object_get_focus = line_edit_object_get_focus,
        #                                           only_yes_button = True,
        #                                           icon = self._kwargs.get('image_path_collection').info_48x48_default,
        #                                           set_flag = True)

        if change_page:
            self.change_page(index = page_index,
                                         stack_widget = stack_widget_object,
                                         clear_widget = True,
                                         current_obj_clear_selection = current_obj_clear_selection,
                                         delete_selected_id = delete_selected_id,
                                         current_obj_clear = current_obj_clear)

        return



    def update_proxy_model(self,
                           **kwargs):

        '''
           SUMMARY
           =======
           This method starts a thread to update current proxy model.

           PARAMETERS
           ==========
           :widget_tree_view (QTreeView):               Here we get the QTreeView.

           :starndard_item_model (QStandardItemModel):  We ecxept a model.

           RETURNS
           =======
           :return:                                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this
                                                        statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the proxy model")

        self.quite_threads()

        kwargs['init_selectionChanged_signal_treeview'] = self.init_selectionChanged_signal_treeview
        kwargs['close_wait_message'] = self.close_wait_message_signal

        #kwargs.get('general_proxy_model').reset_proxy_model()


        self.task_thread = QThread()
        self.task_thread.work = TaskProxyModel(**kwargs)

        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.Set_model_on_treeview_signal.connect(
          lambda
          widget_tree_view,
          certain_proxy_model:
          self.set_model(
            widget_tree_view = widget_tree_view,
            certain_proxy_model = certain_proxy_model))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_work)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def task_model(self,
                   change_page = False,
                   page_index = None,
                   stack_widget_object = None,
                   clear_widget = False,
                   current_obj_clear_selection = None,
                   delete_selected_id = None,
                   current_obj_clear = None,
                   line_edit_object_get_focus = None,
                   **kwargs):
        #print ""
        #print "MasterData_Window task_model is called successfully"
        #print "MasterData_Window in task_model / **kwargs: -> \n {}".format(kwargs)
        '''
           SUMMARY
           =======
           This method is used to start a thread that refreshs the model.

           PARAMETERS
           ==========
           :change_page (bool):                     By default, its False. No page on stacked widget should be changed. Otherwise
                                                    it changes it.

           :page_index (int):                       We get the index of the page we want to change to.

           :stack_widget_object (QStackedWidget):   It just a QStackedWidget()-object.

           :clear_widget (bool):                    By default its False, no widget should be cleaned. Otherwise clear all
                                                    widgets.

           :current_obj_clear_selection (bool):     We ecxept a url to the formular, which initialize the upload.

           :delete_selected_id (bool):              By default its False, All saved IDs shouldn't be deleted. Otherwise
                                                    remove all IDs.

           :current_obj_clear (bool):               By default its False. No widget on current page should be cleaned. Otherwise clear
                                                    all widgets on current page.

           :line_edit_object_get_focus (QLineEdit): Which QLineEdit should get the focus in?

           :column (int):                           Which column should be sorted?

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start thread for refreshing the model")
        #   We create QThread() as container.
        self.task_thread = QThread()
        #   Workaround - we bind the `work_object` - namend TaskModel() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskModel(**kwargs)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskModel() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskModel()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start_thread)

        #   The clean-up: when the worker instance emits finished() it will signal the thread
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished()
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasnâ€™t fully
        #   shut down yet when it is deleted, we connect the finished() of the thread
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def delete_widget_from_layout(self):
        '''
           SUMMARY
           =======
           This method deletes the ProgressBar()-object from the QLayout

           EXTENTED DISCRIPTION
           ====================
           It should also hides the widget, to make sure you don't get one
           funny paintEvent before deleteLater actually does it's thing

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start thread for refreshing the model")

        try:

            self.busy_progress_bar.hide()
            self.horizontal_Layout.removeWidget(self.busy_progress_bar)
            #   Qt should take care of the rest, like removing it from layout
            self.busy_progress_bar.deleteLater()
            del self.busy_progress_bar
            self.busy_progress_bar = None

        except AttributeError:
            pass

        return

    def manage_standart_item_model(self,
                                   category = None,
                                   columns = None):
        '''
           SUMMARY
           =======
           This method set a certain QStandardItemModel.

           PARAMETERS
           ==========
           :category (str):    There are several QStandardItemModel, which of one should be set up?

           :columns (int):     We need a coumnn for setting a QStandardItemModel:

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set up a certain QStandardItemModel")

        self._kwargs.get('standard_item_model').standard_item_model(category = category, rows = 0, columns = columns, parent = self)

        return

    def change_tree(self,
                    logicalIndex,
                    oldSize,
                    newSize):
        print "logicalIndex", logicalIndex
        print "oldSize", oldSize
        print "newSize", newSize

        return

    def resize_to_contents(self,
                           tree_view_object,
                           resize_column,
                           stretch_last_section = True):
        # print ""
        # print "MasterData_Window resize_to_contents is called"
        # print "MasterData_Window in resize_to_contents / tree_view_object.objectName(): -> \n {}".format(tree_view_object.objectName())
        # print "MasterData_Window in resize_to_contents / resize_column: -> \n {}".format(resize_column)
        'Thins method need to resize to contents as well as switching off stretch last section'
        """
        StandardWidgetHeaderView is the standard header view (containing
        column/row labels) inheriting from QHeaderView. The main
        difference between this class and the original one is that it
        allows resizing and stretching at the same time

        """
        #tree_view_object.header().setVisible(True)
        #tree_view_object.setSizeAdjustPolicy(QtWidget.QAbstractScrollArea.AdjustToContents)
        #print "1".format(tree_view_object.resizeColumnToContents(1))

        #headCol = QHeaderView(Qt.Horizontal)
        #headCol.sectionResized.connect(self.change_tree)
        #tree_view_object.setHorizontalHeader(headCol)

        tree_view_object.resizeColumnToContents(resize_column)
        #tree_view_object.header().setResizeMode(QHeaderView.ResizeToContents)
        width = tree_view_object.sizeHintForColumn(resize_column) + 35
        #tree_view_object.header().resizeSection(resize_column, width)

        tree_view_object.setColumnWidth(resize_column, width)

        width = tree_view_object.header().sectionResized.connect(self.change_tree)

        #.sectionSize(1)
        #print "width".format(type(width))

        #print "2".format(tree_view_object.resizeColumnToContents(2))

        #tree_view_object.header().setStretchLastSection(stretch_last_section)
        #tree_view_object.header().setResizeMode(1, QHeaderView.Stretch)
        #tree_view_object.header().setResizeMode(2, QHeaderView.ResizeToContents)
        #tree_view_object.header().setResizeMode(3, QHeaderView.Stretch)

        #header = tree_view_object.horizontalHeader()
        #header.setResizeMode(1, QHeaderView.ResizeToContents)
        #header.setResizeMode(2, QHeaderView.ResizeToContents)

        #tree_view_object.header().setStretchLastSection(stretch_last_section)
        #tree_view_object.header().setResizeMode(1, QHeaderView.Stretch)
        #tree_view_object.header().setResizeMode(2, QHeaderView.Stretch)
        #tree_view_object.header().setResizeMode(2, QHeaderView.ResizeToContents)

        return

    def set_horizontal_header_labels(self,
                                     **kwargs):

        if not kwargs.get('list_header', None) is None:
            #   When given argument is NOT None,
            #   we need to set the QStandItemModel
            kwargs['certain_standard_item_model'].setHorizontalHeaderLabels(kwargs['list_header'])

        return

    def fetch_all(self,
                  **kwargs):
        '''
           SUMMARY
           =======
           This method is used to start each thread to be fetching all data.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start a thread for fetching all records from database.")

        #   We need the current scoped session

        #   First, before we start a thread, we have to quite all running threads.
        self.interrupt_working_thread_signal.emit()

        self.quite_threads()

        #   Second, we try to remove the busy progressbar
        self.delete_widget_from_layout()

        kwargs['page_index'] = self.get_current_index(widget = self.ui_master_data.stackedWidget)

        #   Fith we have to delete all selected IDs they are saved in the attributes
        self.remove_saved_record_id(
          record_id = self.current_selected_id)

        self.remove_saved_record_id(
          record_id = self.selected_be_edited_id)

        if not kwargs.get('notify_on_widget', None) is None:
            #   When given argument is NOT None, we have to
            #   append text to given widget.
            self.append_text(widget = kwargs.get('notify_on_widget'),
                             clear_widget = True)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation(**kwargs)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.set_model_signal.connect(
          lambda
          widget_tree_view,
          certain_proxy_model:
          self.set_model(
            widget_tree_view = widget_tree_view,
            certain_proxy_model = certain_proxy_model))

        self.task_thread.work.set_enable_widget_signal.connect(
          lambda
          boolean:
          self.set_enable_widget_signal.emit(boolean))

        # self.task_thread.work.init_custom_progres_bar_signal.connect(
        #   self.init_custom_progres_bar)

        self.task_thread.work.set_horizontal_header_labels_signal.connect(
          lambda
          signal_kwargs:
          self.set_horizontal_header_labels(**signal_kwargs))

        self.task_thread.work.populate_proxy_model_signal.connect(
          lambda
          update_proxy_model_kwargs:
          self.update_proxy_model(
            **update_proxy_model_kwargs))

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        #if not resize_column is None:
        self.task_thread.work.resize_to_contents_signal.connect(
          lambda
          tree_view_object,
          resize_column:
          self.resize_to_contents(
            tree_view_object = tree_view_object,
            resize_column = resize_column))

        self.task_thread.work.set_text_label_signal.connect(
          lambda
          label_widget,
          row_count:
          self.set_text(
            widget = label_widget,
            text = row_count))

        #if not kwargs.get('widget_label', None) is None:
        self.task_thread.work.counting_label_signal.connect(
          lambda
          counting_label,
          count_value:
          self.display_counting(
            widget = counting_label,
            count = count_value))

        self.task_thread.work.change_page_signal.connect(
          lambda
          change_page_kwargs:
          self.change_page(**change_page_kwargs))

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        self.task_thread.work.count_progress_signal.connect(
          lambda
          value:
          self.count_progress_signal.emit(value))

        self.task_thread.work.update_progress_bar_signal.connect(
          lambda
          process_value,
          row_count:
          #self.waiting_window.update_progress_bar(process_value, row_count))
          self.update_progress_bar_signal.emit(process_value, row_count))

        self.task_thread.work.set_enable_widget_signal.connect(self.set_enable_widget_signal.emit)

        self.task_thread.work.manage_standart_item_model_signal.connect(lambda:
            self.manage_standart_item_model(category = kwargs['category'],
                                            columns = len(kwargs['list_header'])))

        self.task_thread.work.create_wait_window_signal.connect(lambda text:
                self.create_wait_window(text = text, parent = self))

        self.task_thread.work.hide_column_signal.connect(lambda:
          self.set_hide_column(
            tree_view_object = kwargs['widget_tree_view'],
            column = 0,
            bool_value = True))


        self.task_thread.work.count_model_items_signal.connect(lambda:
            self.set_count_text(widget = kwargs['widget_label'],
                                   standard_item_model = kwargs['certain_standard_item_model']))


        self.task_thread.work.text_notify_update_signal.connect(lambda text:
                                                           self.append_text(widget = kwargs.get('notify_on_widget'),
                                                                            clear_widget = False,
                                                                            text = text))

        self.task_thread.work.confirm_saving_successful_signal.connect(lambda msg_text, detail_msg:
            self.create_generally_information_msg(title = self.tr('Confirmation'),
                                                  msg = self.tr(msg_text),
                                                  detail_msg = detail_msg,
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = kwargs['widget_line_edit'],
                                                  only_yes_button = True,
                                                  icon = self._kwargs.get('image_path_collection').info_48x48_default,
                                                  set_flag = True))

        self.task_thread.work.error_message_signal.connect(lambda title_text, conn_err, detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                               err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self._kwargs.get('image_path_collection').warning_48x48_default,
                                               set_flag = True))

        self.interrupt_working_thread_signal.connect(self.task_thread.work.finish_task_thread)

        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start_work)

        #   We connect the finish-signal with deleteLater()-method of QThread for
        #   scheduling a delete of an QObject to free resources from within the
        #   object itself. That menas, when the thread has finished executing, the
        #   finished-signal is emiiited. Its importen, because we should terminate
        #   threads correctly, which must be done with existing cross-threading
        #   signal slot connections with deleteLater(), so that pending events
        #   do not trigger a hanging pointer. So we connect the finished()
        #   of the thread (not the worker!) to its own deleteLater() slot.
        #   This will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def add_record(self,
                   **kwargs):
        '''
           SUMMARY
           =======
           This method adds records.

           EXTENTED DISCRIPTION
           ====================
           This method not only adds records, but also edits it.

           PARAMETERS
           ==========
           :kwargs (dict):  This method expects several arguments, that are saved in the dictionary.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this
                            statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Add or Edit the record")
        print ""
        print "MasterData_Window add_record is called successfully"
        print "MasterData_Window in add_record / kwargs: -> \n {}".format(kwargs)


        #   Before we add a new record, we need the current scoped session

        #   Well we have the current scoped session, now we have to save it in
        #   given dictionary (**kwargs)
        kwargs['scoped_session'] = self._scoped_session

        #   First, we have to quite all running threads.
        self.quite_threads()

        #   For later, we need the current index of given QStackWidget to
        #   hold the index position of the widget that is visible
        stackwidget_current_idx = self.get_current_index(
          widget = self.ui_master_data.stackedWidget)
        print "MasterData_Window in add_record / stackwidget_current_idx: ->\n {}".format(stackwidget_current_idx)
        #   We create QThread() as container.
        kwargs['index'] = stackwidget_current_idx
        self.task_thread = QThread()

        #   Workaround - we bind the TaskDataManipulation() -
        #   to an attribute of the thread object, named self.task_thread.work,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation(**kwargs)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate sub class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   We implemented everything on a class derived from QObject.
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation(). That means, the
        #   TaskDataManipulation()-object has the affinity of the new thread.
        #   We have to do this, because we need an eventloop in our new thread.
        #   This is the only way to get TaskDataManipulation() working.
        #   In general: conceptually, QThread is not a thread itself,
        #   but a thread controller/container. It makes no sense for a thread
        #   controller/container to live in the thread it controls.
        #   We need a controller/container to stay alive.
        self.task_thread.work.moveToThread(self.task_thread)

        #   Now we need to connect the signals from created subclass, which
        #   is moved to the created thread, with methods in this main thread.
        self.task_thread.work.resize_to_contents_signal.connect(lambda
            tree_view_object,
            resize_column:
            self.resize_to_contents(
              tree_view_object = tree_view_object,
              resize_column = resize_column))

        self.task_thread.work.count_model_items_signal.connect(lambda
          widget,
          row_count:
            self.set_count_text(
              widget = widget,
              row_count = row_count))

        self.task_thread.work.confirm_saving_successful_signal.connect(lambda
          title,
          msg_text,
          detail_msg:
          self.create_generally_information_msg(
            title = title,
            msg = msg_text,
            detail_msg = detail_msg,
            yes_no_button = False,
            line_edit_object_get_focus = kwargs['line_edit_object_get_focus'],
            only_yes_button = True,
            icon = self._kwargs.get('image_path_collection').info_48x48_default,
            set_flag = True))

        self.task_thread.work.error_message_signal.connect(lambda
          title_text,
          conn_err,
          detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                             err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self._kwargs.get('image_path_collection').warning_48x48_default,
                                               set_flag = True))


        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        self.task_thread.work.create_wait_window_signal.connect(lambda
          waiting_text:
          self.create_wait_window(text = waiting_text, parent = self))

        self.task_thread.work.remove_current_selected_id_signal.connect(lambda:
          self.remove_saved_record_id(record_id = self.current_selected_id))
        self.task_thread.work.remove_selected_be_edited_id_signal.connect(lambda:
          self.remove_saved_record_id(record_id = self.selected_be_edited_id))

        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        if kwargs.get('work_mode') == "add_record":

            self.task_thread.work.sort_model_signal.connect(lambda:
                self.sort_model(work_mode = 'add_record',
                                line_edit_object_get_focus = kwargs.get('line_edit_object_get_focus'),
                                model = kwargs.get('certain_standard_item_model'),
                                column = 1))

            self.task_thread.started.connect(self.task_thread.work.add_record)

        elif kwargs.get('work_mode') == "edit_record":

            self.task_thread.work.change_page_signal.connect(lambda:
              self.change_page(
                index = 0,
                stack_widget = kwargs.get('stack_widget'),
                clear_widget = True,
                current_obj_clear_selection = True,
                delete_selected_id = True,
                current_obj_clear = True))

            self.task_thread.started.connect(self.task_thread.work.edit_record)

        #   We connect the finish-signal with deleteLater()-method of QThread for
        #   scheduling a delete of an QObject to free resources from within the
        #   object itself. That menas, when the thread has finished executing, the
        #   finished-signal is emiiited. Its importen, because we should terminate
        #   threads correctly, which must be done with existing cross-threading
        #   signal slot connections with deleteLater(), so that pending events
        #   do not trigger a hanging pointer. So we connect the finished()
        #   of the thread (not the worker!) to its own deleteLater() slot.
        #   This will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def create_wait_window(self,
                           text,
                           parent = None,
                           show_pushbutton = True):

        '''
           SUMMARY
           =======
           This method just creates the wait window.

           PARAMETERS
           ==========
           :text (str):    The given text should be shown on the wait window.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create the wait window")

        self.waiting_window = Waiting_Window(
            text_message = text,
            show_pushbutton = show_pushbutton,
            parent = parent)
        self.waiting_window.quite_thread_signal.connect(self.quite_threads)
        self.waiting_window.show()

        return

    def delete_record(self,
                      **kwargs):
        print ""
        print "MasterData_Window delete_record is called successfully"
        print "MasterData_Window in delete_record / **kwargs: -> {}".format(kwargs)
        '''
           SUMMARY
           =======
           Here in this method we want add a new genre of movies.

           RETURNS
           =======
           :return:                                               Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Delete the selected record")

        #   We can't delete a record without a current scoped sessin, so
        #   we need to get it

        kwargs['scoped_session'] = self._scoped_session
        print ""
        print "MasterData_Window in delete_record / later **kwargs: -> {}".format(kwargs)

        #   First, before we start a thread, we have to quite all running threads.
        self.quite_threads()

        stackwidget_current_idx = self.get_current_index(widget = self.ui_master_data.stackedWidget)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation(**kwargs)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing of QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.resize_to_contents_signal.connect(lambda
            tree_view_object,
            resize_column:
            self.resize_to_contents(
                tree_view_object = tree_view_object,
                resize_column = resize_column))

        self.task_thread.work.clear_widget_object_content_signal.connect(lambda:
            self.clear_widget_object_content(
                list_widget_object = self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((
                    QTextEdit, QLineEdit, QPlainTextEdit))))

        self.task_thread.work.count_model_items_signal.connect(lambda:
            self.set_count_text(widget = kwargs.get('label_object'),
                                   row_count = row_count))

        self.task_thread.work.error_message_signal.connect(lambda title_text, conn_err, detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                               err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self._kwargs.get('image_path_collection').warning_48x48_default,
                                               set_flag = True))

        self.task_thread.work.remove_current_selected_id_signal.connect(lambda:
          self.remove_saved_record_id(
            record_id = self.current_selected_id))
        self.task_thread.work.remove_selected_be_edited_id_signal.connect(lambda:
          self.remove_saved_record_id(
            record_id = self.selected_be_edited_id))

        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        converted_bool = self.convert_string_to_bool(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["AskBeforDelete"])

        try:

            int(kwargs.get('id'))

            if converted_bool:

                result_msg_box = self.create_generally_question_msg(title = self.tr('Conformation'),
                                                                    text = self.tr('Are you sure you want to permamently delete this item?'),
                                                                    detail_text = self.tr('NOTICE: By deleting, the selected record will be irrevocably removed! Continue?'),
                                                                    icon = self._kwargs.get('image_path_collection').question_48x48_default,
                                                                    set_flag = True)

            if result_msg_box:

                self.task_thread.work.delete_record()

            self.task_thread.finished.connect(self.task_thread.deleteLater)

            self.task_thread.start()

        except (ValueError, TypeError):

            desired_trace = format_exc(exc_info())

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or \
            not desired_trace.find("int() argument must be a string or a number") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                self.create_generally_crritcal_msg(err_title = title_text,
                                                   err_msg = conn_err,
                                                   icon = self._kwargs.get('image_path_collection').warning_48x48_default,
                                                   set_flag = True)

        self.clear_widget_object_content(
                list_widget_object = self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((
                  QTextEdit,
                  QLineEdit,
                  QPlainTextEdit)))

        self.clear_tree_view_selection(tree_view = kwargs.get('tree_view_object'))

        self.set_focus(widget_object = kwargs.get('line_edit_obj'))

        return

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._list_threads. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window quite_threads is called successfully"

        logger.info("Close all opened threads")

        for thread in self._list_threads:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in place.
        del self._list_threads[:]

        #   And then we want the programm to delete the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event (class):  By default the given event is accepted and the widget is closed. We
                            get this event when Qt receives a window close request for a top-level
                            widget from the window system. That means, first it sends the widget a QCloseEvent.
                            The widget is closed if it accepts the close event. The default implementation of
                            QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):

            #   The isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
            #   that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
            #   Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
            #   This is the case when the window is a subwindow in the mdi. We want to know if
            #   the actual window is an instance attribute inside in this class.

            self.set_flat_signal.emit('master_data', False)
            self.parent().close()
        else:

            #   No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
            #   That means, the window is not a subwindow.
            self.close()

        return

def output(text):
    print "Output:",  text

def show_ui():
    configuration_saver_dict = ConfigurationSaverDict()

    app = QApplication(argv)
    master_data_window = MasterData_Window(configuration_saver_dict = configuration_saver_dict,
                                           page_number = 0)
    master_data_window.showMaximized()
    exit(app.exec_())

def build_engine(dbms = None, db_driver = None, sqlite_path = None):
    engine = create_engine('{dbms}+{db_driver}:///{file_path}'.format(
        dbms = 'sqlite',
        db_driver = u'pysqlite',
        file_path = sqlite_path,
        encoding = 'utf8',
        echo = True))

    # We should bind engine to your model.
    ScopedSession.configure(bind=engine)

    return

def main():
    # This is only needed for Python v2 but is harmless for Python v3.
    import sip

    sqlite_path = None
    db_driver = None

    try:
        sip.setapi('QVariant', 2)
    except ValueError:
        sip.setapi('QVariant', 1)

    dbms = raw_input('Enter dbms (e.g. sqlite): ')
    if dbms == 'sqlite':
        sqlite_path = raw_input('Enter path to sqlite file: ')
        db_driver = 'pysqlite'

    build_engine(dbms = dbms, db_driver = db_driver, sqlite_path = sqlite_path)

    show_ui()

    return

if __name__ == "__main__":
    main()
