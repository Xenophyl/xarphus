#-*- coding:latin1 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import sys
import webbrowser


'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QIcon, QMessageBox
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QTextStream, QFile
'''
Private modules are imported.
'''
from xarphus.collection_paths_image import Collection_Path_Images
from xarphus.info import info_app
from xarphus.core.manage_ini_file import read_licence
from xarphus.core.qt_info import get_versio_pyqt, get_version_qt


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
class PushbuttonsCloseDeleteSave_Window(QWidget):
    def __init__(self, func_up_to_date, update_sublist, save_config, parent):
        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        UI_PATH = QFile(":/ui_file/pushbuttons_close_delete_save.ui")

        self.set_get_settings = save_config

        self.get_app_info = info_app()

        UI_PATH.open(QFile.ReadOnly)
        self.ui_pushbuttons_close_delete_save = loadUi(UI_PATH, self)
        UI_PATH.close()


        #self.color_black = "<font color='#000000'>"
        #self.ui_pushbuttons_close_delete_save.groupBox.setStyleSheet('border: none solid gray;')
#        self.ui_search.textEditExplain.setStyleSheet(('background-color: #ffffff;')) # white

        #self.ui_search.setWindowModality(Qt.ApplicationModal)
        self.init_ui()

        #self.get_language_about()
        #self.set_color()
        #self.set_language_about()
        #self.ui_pushbuttons_close_delete_save.show()
        #self.init_actions_button()
        #self.create_actions_label()
        #self.get_licence()
#--------------------------------------------------------------------------------------
    def init_ui(self):
        pass
        #self.ui_pushbuttons_close_delete_save.setWindowModality(Qt.ApplicationModal)
#        self.ui_pushbuttons_close_delete_save.setFixedSize(self.ui_pushbuttons_close_delete_save.width(), self.ui_pushbuttons_close_delete_save.height())

    def set_color(self):
        self.color_gray = "<font color='#808080'>"
        self.color_black = "<font color='#000000'>"
#--------------------------------------------------------------------------------------
    def get_language_about(self):
        from xarphus.languages.german import Language
        self.set_language = Language()
        # if self.set_get_settings.dict_set_settings["language"] == "German":
        #     from xarphus.languages.german import Language
        #     self.set_language = Language()
#--------------------------------------------------------------------------------------
    def set_language_about(self):
        m = get_version_qt()

        self.ui_pushbuttons_close_delete_save.setWindowTitle(self.set_language.dict_ui_about["about_title"])
        self.ui_pushbuttons_close_delete_save.label_product_name.setText(self.get_app_info.dict_info["product_name"] + " (" + self.get_app_info.dict_info["product_version"] + " - " + self.get_app_info.dict_info["product_built"] + ")")

        self.ui_pushbuttons_close_delete_save.pushButton_close.setText(self.set_language.dict_ui_about["pushButton_close"])
        #self.ui.textEditLicence.setText(self.set_licence.dict_gpl["licence"])
        self.ui_pushbuttons_close_delete_save.tabWidget_about.setTabText(0, self.set_language.dict_ui_about["tab_version"]) # First Tab
        self.ui_pushbuttons_close_delete_save.tabWidget_about.setTabText(1, self.set_language.dict_ui_about["tab_authors"]) # Second Tab
        self.ui_pushbuttons_close_delete_save.tabWidget_about.setTabText(2, self.set_language.dict_ui_about["tab_thanks"]) # Third Tab
        self.ui_pushbuttons_close_delete_save.tabWidget_about.setTabText(3, self.set_language.dict_ui_about["tab_philosophy"]) #  Fourth Tab
        self.ui_pushbuttons_close_delete_save.tabWidget_about.setTabText(4, self.set_language.dict_ui_about["tab_licence"]) # Fifth Tab
        self.ui_pushbuttons_close_delete_save.label_copyright.setText(self.color_gray + self.get_app_info.dict_info["product_copyright"] +
                                                self.get_app_info.dict_info["product_copyright_characters"] +
                                                " " +
                                                self.get_app_info.dict_info["product_year_from"] +
                                                " - " +
                                                self.get_app_info.dict_info["product_year_till"] +
                                                " " +
                                                self.get_app_info.dict_info["product_by"] +
                                                " " +
                                                self.get_app_info.dict_info["product_author"])
        self.ui_pushbuttons_close_delete_save.label_gui.setText(self.set_language.dict_ui_about["label_gui_inferface"] +
                                                "  " +
                                                #self.get_app_info.dict_info["product_GUI_Version"])

                                                m)

        self.ui_pushbuttons_close_delete_save.textEdit_philosophy.setText(self.color_black + self.set_language.dict_ui_about["textEdit_philosophy"])
        print "STATUS [OK]  (", FILE_NAME, "): The language was set successfully"
#--------------------------------------------------------------------------------------
    def open_url_homepage(self, link):
        # open in a new tab, if possible
        new = 2
        # open a public URL, in this case, the webbrowser docs
        webbrowser.open(link, new=new)
#--------------------------------------------------------------------------------------
    def create_actions_label(self):
        homepage_link = self.get_app_info.dict_info["product_site"]
        link_tag = self.color_gray + self.set_language.dict_ui_about["label_homepage"] + " <a href='%s'>" % homepage_link + self.set_language.dict_ui_about["label_homepage_url"] + "</a>"
        self.ui_pushbuttons_close_delete_save.label_homepage_url.setText(link_tag)
        self.ui_pushbuttons_close_delete_save.label_homepage_url.linkActivated.connect(self.open_url_homepage)
#--------------------------------------------------------------------------------------
    def init_actions_button(self):
        self.ui_pushbuttons_close_delete_save.pushButton_close.clicked.connect(self.close)

        print "STATUS [OK]  (", FILE_NAME, "): All actions for Pushbuttons have been set successfully"
#--------------------------------------------------------------------------------------
    def get_licence(self):
        licence_path = unicode(self.set_get_settings.dict_set_settings["PathToLicenceGPL2"])
        licence = read_licence(licence_path)
        self.ui_pushbuttons_close_delete_save.textEdit_licence.setText(licence)
