#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

''' The modules of required libraries are imported. '''
from os import sep, error, path
from sys import exc_info, _getframe, exit
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


from sqlalchemy.exc import SQLAlchemyError

try:
    #from configparser import ConfigParser
    from configparser import MissingSectionHeaderError, NoOptionError
except ImportError:
    from ConfigParser import MissingSectionHeaderError, NoOptionError  # ver. < 3.0

try:
    from xarphus.core import manage_prase
except ImportError:
    from core import manage_prase

''' The modules for Qt are imported. PyQt are a set of Python bindings for Qt. '''
from PyQt4.QtCore import Qt, QFile, QSettings, QByteArray, pyqtSignal
from PyQt4.QtGui import QMainWindow, QAction, QMenu, QSystemTrayIcon, QIcon, QMessageBox, \
    QPushButton, QToolButton, QMdiArea, QMdiSubWindow, \
    QLabel, QFrame, QColor
from PyQt4.uic import loadUi


''' Private modules are imported. '''
from xarphus.core.manage_prase import parsing_str_to_bool
from xarphus.core.manage_logging import configure_logging
from xarphus.core.standard_item_model import StandardItemModel
from xarphus.core.proxy_model import ProxyModel
from xarphus.core.managed_defined_session import *

from xarphus.frm_setting import Setting_Window
from xarphus.frm_about import About_Window
from xarphus.frm_db_login import DB_Login_Window
from xarphus.frm_download import Download_Window
from xarphus.frm_about_version import NewInVersion_Window
from xarphus.frm_register import Register_Window
from xarphus.frm_customer_login import CustomerLogin_Window
from xarphus.frm_feedback import Feedback_Window
from xarphus.frm_search import Search_Window
from xarphus.frm_movie import Movie_Window
from xarphus.frm_cover_viewer import CoverViewer_Window
from xarphus.frm_person_profile import PersonProfile_Window
from xarphus.frm_note import Note_Window
from xarphus.frm_store_on_medium import StoreOnMediumn_Window
from xarphus.frm_select_person import SelectPerson_Window
from xarphus.frm_collection import Collection_Window
from xarphus.frm_error import Error_Window
from xarphus.frm_coin_detail import CoinDetail_Window
from xarphus.frm_series_season_episode import Series_Season_Episode_Window
from xarphus.frm_select_details import Select_Details_Window
#from xarphus.frm_master_data import MasterData_Window
from xarphus.frm_master_data import MasterData_Window
from xarphus.frm_connection_profiles import ConnectionProfiles_Window
from xarphus.frm_filter import Filter_Window

from xarphus.frm_combine_movie_general_details_buttons import CombineMovieGeneralDetailsButtons_Window
from xarphus.frm_combine_comic_general_details_buttons import CombineComicGeneralDetailsButtons_Window
from xarphus.frm_combine_serie_general_details_buttons import CombineSerieGeneralDetailsButtons_Window
from xarphus.frm_combine_music_general_details_buttons import CombineMusikAlbumGeneralDetailsButtons_Window
from xarphus.frm_combine_coin_general_details_buttons import CombineCoinGeneralDetailsButtons_Window

from xarphus.frm_customer_online_profile import CustomerOnlineProfile_Window
from xarphus.frm_movie_detail import MovieDetail_Window

from xarphus.frm_overview_ratings import OverViewRatingsClassification_Window
from xarphus.frm_ratings import Ratings_Window
from xarphus.frm_rating_classification import RatingClassification_Window

from xarphus.core.managed_defined_session import *
from xarphus.core.managed_defined_session import DefinedSessionScope

from xarphus.core.manage_webrowser import open_web_browser

from xarphus.core.check_update import compare_parser

from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint

from xarphus.frm_test import MyCustomDialog


# # ----
from xarphus.core.manage_ini_file import create_write_ini_file, create_write_conf_file, read_conf_file
from xarphus.core import manage_folder
from xarphus.core import restart

BASE_PATH = path.dirname(path.abspath(__file__))

class MDI_Window(QMainWindow):

    send_current_scoped_session_master_data_signal = pyqtSignal(object)

    send_current_scoped_session_combine_movie_general_details_buttons_form_signal = pyqtSignal(object)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QMainWindow.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/mdi.ui")

        # Create instance variables
        self._kwargs = kwargs
        self.get_info_app = self._kwargs.get('info_app')
        self.default_ini_file = self._kwargs.get('default_ini_file')
        self.configuration_saver_dict = self._kwargs.get('configuration_saver_dict')
        self.image_path_collection = self._kwargs.get('image_path_collection')
        self.safe_config_parser = self._kwargs.get('safe_config_parser')
        self._app = self._kwargs.get('app')

        '''
            Create attributes.
        '''
        self._engine = None
        self.defined_session_scope = None
        self._logged_in = False
        self.flags = flag_dialog_titlehint_customizewondowhint()
        self._standard_item_model = StandardItemModel(configuration_saver_dict = self.configuration_saver_dict)
        self._general_proxy_model = ProxyModel()

        self.unload_me = None

        self.win_status = None
        self.messege = None
        self.server_host = None

        self.user_informed = None

        self._master_data_window_is_opend = False

        # Call methods
        self.load_ui_file(ui_path = UI_PATH)
        self.init_gui(kwargs = self._kwargs)

    def configure_logging_data(self, LOG_FILE_PATH):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           PARAMETERS
           ==========
           :LOG_FILE_PATH (str):  Expected an argument, that contains a
                                  compiled path to the directory where
                                  the .*ui-file is stored.

           RETURNS
           =======
           :return:               Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        try:
            configure_logging(LOG_FILE_PATH)
        except IOError:
            pass

        return

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_mdi = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def popen_it(self):

        file_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings.get('BasePath')
        print "file_path", file_path

        file_path_1 = path.join(file_path, 'installer', 'installer', 'frm_installer.py')

        restart.start_update(exec_file = file_path_1)

        return

    def init_gui(self,
                kwargs):
        '''
           SUMMARY
           =======
           In this method the gui is inistialized. Currently the title
           of the window is set and the flag of this window is flagged.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize the GUI")

        RedColor = int(kwargs.get('configuration_saver_dict').dict_set_general_settings.get('RedColorMDI'))
        GreenColor = int(kwargs.get('configuration_saver_dict').dict_set_general_settings.get('GreenColorMDI'))
        BlueColor = int(kwargs.get('configuration_saver_dict').dict_set_general_settings.get('BlueColorMDI'))
        self.mdiArea.setBackground(QColor(RedColor,GreenColor,BlueColor))

        self.restore_window_state(kwargs = self._kwargs)
        self.init_statusbar()
        self.set_icon_on_menu(image_path_collection = self._kwargs.get('image_path_collection'))
        self.set_Window_title(kwargs = kwargs)  # 1
        self.set_visible_sub_menu(kwargs = kwargs)
        self.set_triggered_menubar_action()  # 2
        self.init_toolbuttons(kwargs = kwargs)  # 3
        self.init_menu_for_toolbuttons(image_path_collection = self._kwargs.get('image_path_collection'))  # 4
        self.init_action_in_toolbar(kwargs = self._kwargs)  # 5
        self.set_popup_mode_and_set_menu_in_toolbuttons()  # 6
        self.add_toolbuttons_to_toolbar(kwargs = self._kwargs)  # 7
        self.set_triggered_action_menuebar_in_toolbar(defined_session_scope = self.defined_session_scope, kwargs = self._kwargs)  # 8
        self.create_actions_tray(kwargs = self._kwargs)
        self.set_up_mdi_area()

        #self.init_tool_bar()
        #self.init_status_bar()
        self.init_database_status(logged_in = self._logged_in, kwargs = self._kwargs)

        return

    def set_icon_on_menu(self,
                        image_path_collection):
        '''
           SUMMARY
           =======
           This method is used to set images on QMenu and QAction.

           PARAMETERS
           ==========
           :image_path_collection (str):  There is the path to the compiled image.

           RETURNS
           =======
           :return:                       Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                          me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Set Icons on QMenu and QAction')

        # QMenu
        self.ui_mdi.menu_film.setIcon(QIcon(image_path_collection.film_16x16))
        self.ui_mdi.menu_book.setIcon(QIcon(image_path_collection.book_16x16))
        self.ui_mdi.menu_music.setIcon(QIcon(image_path_collection.music_16x16))
        self.ui_mdi.menu_coin.setIcon(QIcon(image_path_collection.coin_16x16))
        self.ui_mdi.menu_contact.setIcon(QIcon(image_path_collection.contact_16x16))
        self.ui_mdi.menu_person.setIcon(QIcon(image_path_collection.person_16x16))
        self.ui_mdi.menu_note.setIcon(QIcon(image_path_collection.note_16x16))
        self.ui_mdi.menu_video_game.setIcon(QIcon(image_path_collection.video_game_16x16))
        self.ui_mdi.menu_quotation.setIcon(QIcon(image_path_collection.quotation_16x16))
        self.ui_mdi.menu_stamp.setIcon(QIcon(image_path_collection.stamp_16x16))
        self.ui_mdi.menu_wine.setIcon(QIcon(image_path_collection.wine_16x16))
        self.ui_mdi.menu_rent.setIcon(QIcon(image_path_collection.rent_16x16))

        # QAction
        self.ui_mdi.action_add_serie.setIcon(QIcon(image_path_collection.film_16x16))
        self.ui_mdi.action_add_movie.setIcon(QIcon(image_path_collection.film_16x16))
        self.ui_mdi.action_log_in_db.setIcon(QIcon(image_path_collection.connect_database_32x32))
        self.ui_mdi.action_disconnection_db.setIcon(QIcon(image_path_collection.disconnect_database_32x32))
        self.ui_mdi.action_connection_profiles.setIcon(QIcon(image_path_collection.database_profile_connection_16x16))
        self.ui_mdi.action_search.setIcon(QIcon(image_path_collection.search_16x16))
        self.ui_mdi.action_filter.setIcon(QIcon(image_path_collection.filter_32x32))
        self.ui_mdi.action_master_data.setIcon(QIcon(image_path_collection.master_data_32x32))
        self.ui_mdi.action_settings.setIcon(QIcon(image_path_collection.settings_32x32))
        self.ui_mdi.action_help.setIcon(QIcon(image_path_collection.help_16x16))
        self.ui_mdi.action_support.setIcon(QIcon(image_path_collection.support_16x16))
        self.ui_mdi.action_feedback.setIcon(QIcon(image_path_collection.feedback_16x16))
        self.ui_mdi.action_log_in_user_profile.setIcon(QIcon(image_path_collection.log_in_user_profile_online_16x16))
        self.ui_mdi.action_faq.setIcon(QIcon(image_path_collection.faq_16x16))
        self.ui_mdi.action_release_note.setIcon(QIcon(image_path_collection.release_note_16x16))
        self.ui_mdi.action_about.setIcon(QIcon(image_path_collection.about_16x16))
        self.ui_mdi.action_update.setIcon(QIcon(image_path_collection.update_16x16))
        self.ui_mdi.action_add_book.setIcon(QIcon(image_path_collection.book_16x16))
        self.ui_mdi.action_add_comic.setIcon(QIcon(image_path_collection.comic_book_16x16))
        self.ui_mdi.action_add_magazine.setIcon(QIcon(image_path_collection.magazine_16x16))
        self.ui_mdi.action_add_newspaper.setIcon(QIcon(image_path_collection.newspaper_16x16))
        self.ui_mdi.action_add_miscellany.setIcon(QIcon(image_path_collection.miscellany_16x16))
        self.ui_mdi.action_add_journal_article.setIcon(QIcon(image_path_collection.journal_article_16x16))
        self.ui_mdi.action_add_newspaper_article.setIcon(QIcon(image_path_collection.journal_article_16x16))
        self.ui_mdi.action_add_e_book.setIcon(QIcon(image_path_collection.ebook_16x16))
        self.ui_mdi.action_audio_book.setIcon(QIcon(image_path_collection.audio_book_16x16))
        self.ui_mdi.action_add_record.setIcon(QIcon(image_path_collection.record_16x16))
        self.ui_mdi.action_add_album.setIcon(QIcon(image_path_collection.album_16x16))
        self.ui_mdi.action_add_single.setIcon(QIcon(image_path_collection.single_16x16))
        self.ui_mdi.action_add_single_audio.setIcon(QIcon(image_path_collection.single_16x16))
        self.ui_mdi.action_add_coin.setIcon(QIcon(image_path_collection.coin_16x16))
        self.ui_mdi.action_add_contact.setIcon(QIcon(image_path_collection.contact_16x16))
        self.ui_mdi.action_add_person.setIcon(QIcon(image_path_collection.person_16x16))
        self.ui_mdi.action_add_note.setIcon(QIcon(image_path_collection.note_16x16))
        self.ui_mdi.action_add_video_game.setIcon(QIcon(image_path_collection.video_game_16x16))
        self.ui_mdi.action_add_quotation.setIcon(QIcon(image_path_collection.quotation_16x16))
        self.ui_mdi.action_add_stamp.setIcon(QIcon(image_path_collection.stamp_16x16))
        self.ui_mdi.action_add_wine.setIcon(QIcon(image_path_collection.wine_16x16))
        self.ui_mdi.action_add_rent.setIcon(QIcon(image_path_collection.rent_16x16))

        return

    def restore_window_state(self,
                             kwargs):
        '''
           SUMMARY
           =======
           This method is used to restore the state of this mainwindow's
           toolbars and dock-widgets.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Restore all states")

        conf_base_path = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('PathToSettingFolder')

        base_path = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('BasePath')
        profile_name = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('current_profile')

        file_path = path.join(base_path, 'profiles', unicode(profile_name), 'settings', 'window_state.conf')

        try:
            self.restoreState(QByteArray(read_conf_file(file_path = file_path, file_mode = 'rb')))
        except IOError:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def set_logged_in(self,
                       kwargs,
                       is_logged_in = False):
        '''
           SUMMARY
           =======
           This method is used to save the given session object in the attribute.

           PARAMETERS
           ==========
           :is_logged_in (bool):  We except a bool value. By default its False. Which means,
                                  we aren't loggged in with database.

           RETURNS
           =======
           :return:               Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the flag of LogIn-Status")

        self._logged_in = is_logged_in

        self.init_database_status(
          logged_in = is_logged_in,
          kwargs = kwargs)

        return

    def set_enabled_menu_item(self,
                              defined_session_scope):
        '''
           SUMMARY
           =======
           This method checks if the session is scoped.

           PARAMETERS
           ==========
           :defined_session_scope (class):  There is a class of current scoped session.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set enabled the menu item.")

        if defined_session_scope:

            self.ui_mdi.action_log_in_db.setEnabled(False)
            self.ui_mdi.action_disconnection_db.setEnabled(True)

        elif not defined_session_scope:

            self.ui_mdi.action_log_in_db.setEnabled(True)
            self.ui_mdi.action_disconnection_db.setEnabled(False)

        return

    def save_engine_scoped_session(self,
                                   engine,
                                   scoped_session,
                                   kwargs):
        '''
           SUMMARY
           =======
           This method is used to save the given
           session object in the attribute.

           PARAMETERS
           ==========
           :engine (class):                 Its a created engine, it is how SQLAlchemy communicates with database.

           :defined_session_scope (class):  There is a class of current scoped session.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Save the given session object.")

        if engine:

            self.defined_session_scope = DefinedSessionScope(
              scoped_session = scoped_session,
              engine = engine)

            #self._engine = engine
            self.set_enabled_menu_item(
              defined_session_scope = self.defined_session_scope)

            self.set_logged_in(
              kwargs = kwargs,
              is_logged_in = True)

        if self._master_data_window_is_opend:
            self.send_current_scoped_session_master_data_signal.emit(self.defined_session_scope)

        return

    def init_database_status(self,
                             logged_in,
                             kwargs):
        '''
           SUMMARY
           =======
           This method is used to set up the statusBar.

           PARAMETERS
           ==========
           :logged_in (bool): Its a created engine, it is how SQLAlchemy communicates with database.

           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Save the given session object.")

        database_type = kwargs.get('safe_config_parser').get('General_Configuration', 'database_type')

        db_status = None

        if logged_in:
            db_status = "Online"
        else:
            db_status = "Offline"

        self.label_middle_statusbar.setText("Database: {data_base_type} ({db_status}) ".format(data_base_type=database_type,
                                                                                                    db_status=db_status))

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :set_flag (bool):        For removing close button from QMessageBox.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         text,
                                         title,
                                         detail_text = None,
                                         icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (Qstring):        We except a text for the title bar of the message box.

           :text (Qstring) :        We need the text for the main message to be displayed.

           :detail_text (Qstring):  When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = text,
                                  title = title,
                                  detail_text = detail_text,
                                  icon = icon)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def db_logout(self,
                  defined_session_scope,
                  icon,
                  logged_in,
                  kwargs):
        '''
           SUMMARY
           =======
           This method logs out of the database.

           PARAMETERS
           ==========
           :defined_session_scope (class):  There is a class of current scoped session.

           :icon (str):                     For better understanding, we should show an icon on message box by using path to icon file.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.

        '''
        logger.info("Logout out of the database and remove the connection")

        if not defined_session_scope is None:

            defined_session_scope.disconnect()

            defined_session_scope = None

            self._logged_in = False

            remove_scope_session()

            self.set_enabled_menu_item(
              defined_session_scope = defined_session_scope)

            self.init_database_status(
              logged_in = logged_in,
              kwargs = kwargs)

            self._standard_item_model.set_standard_item_model()

            self.create_generally_information_msg(title = self.tr("Confirmation"),
                                                  text = self.tr("You have signed out of the database successfully."),
                                                  icon = icon)
            return

    # TODO disable or delete this function later
    def show_sub_window_list(self):
        '''
        NOTICE:
        =======
        This function is only for a test. I want to check, if all widgets will closed.
        '''
        print "saved window", self.save_window
        print "saved window, type", type(self.save_window)
        try:
            for obj in self.mdiArea.subWindowList():
                print "There is a sub window: ", obj
                if self.save_window == str(obj):
                    print "hurray"

        except: pass

    #--------------------------------------------------------------------------------------
    def set_Window_title(self,
                         kwargs):
        '''
           SUMMARY
           =======
           This method is used to the title on MDI Window.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the title on MDI Window")

        product_name = kwargs.get('info_app').dict_info.get('product_name')
        product_built = kwargs.get('info_app').dict_info.get('product_built')
        product_version = kwargs.get('info_app').dict_info.get('product_version')
        current_profile = kwargs.get("configuration_saver_dict").dict_set_general_settings.get('current_profile')

        self.ui_mdi.setWindowTitle(

            product_name + ' ' + '(' + product_built + '- ' + product_version + ')' + '  ' + '[ ' + self.tr('Profile') + ': '  + current_profile  + ' ]'

                )

        return

    def set_triggered_menubar_action(self):
        '''
           SUMMARY
           =======
           This method sets the triggered signal of action from menubar, which are a part of QToolBar.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Set triggered of menubar in QToolBar')

        self.ui_mdi.action_search.triggered.connect(
          lambda:
          self.create_search_form(kwargs = self._kwargs) )

        self.ui_mdi.action_filter.triggered.connect(self.create_filter_form)

        self.ui_mdi.action_rating.triggered.connect(
          lambda:
          self.create_overview_ratings(object_category = "movie"))


        self.ui_mdi.action_master_data.triggered.connect(self.create_master_data_form)

        self.ui_mdi.action_film_version.triggered.connect(self.create_master_data_form)

        self.ui_mdi.action_film_edition.triggered.connect(self.create_master_data_form)

        self.ui_mdi.action_add_contact.triggered.connect(self.create_person_selection)
        self.ui_mdi.action_add_contact.triggered.connect(self.create_profile_contact)
        self.ui_mdi.action_settings.triggered.connect(self.create_setting_form)
        self.ui_mdi.action_add_person.triggered.connect(lambda:
            self.create_person_profile(person_id = None))

        # ToDo: Later, delete it from here and from gui
        self.ui_mdi.action_person_addressbook.triggered.connect(self.create_profile_contact)
        self.ui_mdi.action_person_actor.triggered.connect(self.create_person_prominent)
        self.ui_mdi.action_person_prominent_contact.triggered.connect(self.create_person_prominent_contact)

        self.ui_mdi.menu_land.triggered.connect(self.create_person_prominent)
        self.ui_mdi.action_sys_tray.triggered.connect(self.create_tray_icon)

        self.ui_mdi.action_add_movie.triggered.connect(
            lambda: self.create_combine_movie_general_details_buttons(
                scoped_session = self.defined_session_scope,
                work_mode = 'add_record',
                show_messagebox = True,
                standard_item_model = self._standard_item_model,
                general_proxy_model = self._general_proxy_model,
                form_window_title = self.tr("Add movie")))

        self.ui_mdi.action_add_serie.triggered.connect(
            lambda: self.create_combine_serie_general_details_buttons(0, self.tr("Add serie")))

        self.ui_mdi.action_add_comic.triggered.connect(
            lambda: self.create_combine_comic_general_details_buttons(form_window_title = self.tr("Add comic book")))

        self.ui_mdi.action_add_album.triggered.connect(
            lambda: self.create_music_album(0, self.tr("Add album")))


        self.ui_mdi.action_customer_online_profile.triggered.connect(self.create_customer_online_profile_form)
        self.ui_mdi.action_restart.triggered.connect(self.ask_to_restart)
        self.ui_mdi.action_close_all_sub_windows.triggered.connect(self.close_all_subwindows)
        self.ui_mdi.action_register.triggered.connect(self.create_register_form)
        self.ui_mdi.action_close.triggered.connect(self.close)

        self.ui_mdi.action_about.triggered.connect(
          lambda: self.create_about_form(kwargs = self._kwargs))

        self.ui_mdi.action_log_in_user_profile.triggered.connect(
          lambda: self.create_customer_login_form(self._kwargs))
        self.ui_mdi.action_faq.triggered.connect(self.open_url_faq)
        self.ui_mdi.action_release_note.triggered.connect(self.create_history_form)
        self.ui_mdi.action_feedback.triggered.connect(self.create_feedback_form)

        self.ui_mdi.action_disconnection_db.triggered.connect(
          lambda:
          self.db_logout(
            defined_session_scope = self.defined_session_scope,
            icon = self.image_path_collection.check_circle_48x48_default))

        self.ui_mdi.action_update.triggered.connect(lambda: self.check_arequired_access_data(kwargs = self._kwargs))
        self.ui_mdi.action_add_note.triggered.connect(self.create_combine_note_buttons)

        self.ui_mdi.action_log_in_db.triggered.connect(
          lambda: self.create_db_login_form(kwargs = self._kwargs))
        self.ui_mdi.action_connection_profiles.triggered.connect(self.create_connection_profiles_form)

        return

    def open_url_faq(self):
        open_web_browser(self.get_info_app.dict_info["product_faq"])

    def activate_menu_item_customer_profile(self):
        self.ui_mdi.action_customer_online_profile.setEnabled(True)

    def set_flag(self, form_name, boolean_value):

        print "set_flag is called"

        if form_name == "master_data":
            self._master_data_window_is_opend = boolean_value
    '''
        #######################################################################################
        #######################################################################################
        ##############   FROM HERE ALL FORMS ARE ADMINISTERED THAT ARE CREATED.  ##############
        #######################################################################################
        #######################################################################################
    '''

    def create_music_album(self, form_condition, form_window_title):
        logger.info("Calling the function")

        subwindow_person_selection = QMdiSubWindow()

        person_selection_form = CombineMusikAlbumGeneralDetailsButtons_Window \
                                                        (int(form_condition),
                                                         self.up_to_date_it,
                                                         #self.show_sub_qwidgets_children,
                                                         self.create_cover_viewer,
                                                         self.create_movie_detail,
                                                         form_window_title,
                                                         self.configuration_saver_dict,
                                                         self.image_path_collection,
                                                         self.get_info_app,
                                                         self)
        # NOTICE:
        # =======
        # In general, Qt tends not to implicitly delete objects,
        # which is why we had to explicitly mark the widget for deletion.
        person_selection_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_person_selection.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_person_selection.setWidget(person_selection_form)
        self.mdiArea.addSubWindow(subwindow_person_selection)

        # '''
        # calculate the widht and height of ui_person_address_book
        # '''
        # ui_height = float(person_selection_form.height())
        # ui_width = float(person_selection_form.width())
        #
        # hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
        #                                                                     form_width=ui_width)
        #
        # subwindow_person_selection.resize(ui_width, ui_height)
        #
        # subwindow_person_selection.move(hpos, vpos)

        subwindow_person_selection.showMaximized()

        return

    def create_coin_detail_form(self):
        '''
        NOTICE:
        -------
        Currently, the class (CombineMovieGeneralDetailsButtons_Window) takes nine
        arguments in all.

        SEQUENCE:
        ---------
        01 argument:     There are two options (0 or 1). "Zero" means,
                         the user will add a new movie, and the print-button,
                         and the delete-buttons has been turned off (enabled=False).
                         And "One" means, the user wants to load an existing movie,
                         that is why in this case the both buttons (the print-button and
                         the delete-button) are activated.

        02 argument:     The function (up_to_date_it) is passed as an argument.
        03 argument:     The function (showQWidgetsChildren) is passed as an argument.
        04 argument:     The function (create_cover_viewer) is passed as an argument.
        05 argument:     The function (create_movie_detail) is passed as an argument.
        06 argument:     The obtained argument (form_window_title) is passed as an argument.
        07 argument:     The reference class (get_set_setting) is passed as an argument.
        08 argument:     The reference class (image_path_collection) is passed as an argument.
        09 argument:     The reference class (get_info_app) is passed as an argument.
        '''

        form_title_movie = ""
        pushbutton_status = 0
        detail_type = 0

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        self.subwindow_movie_detail = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:

            logger.info("Try to create an instance of the class of CoinDetail_Window()")

            movie_detail_form = CoinDetail_Window(int(pushbutton_status),
                                                        form_title_movie,
                                                        detail_type,
                                                        self.image_path_collection,
                                                        logger,
                                                        self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            movie_detail_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            self.subwindow_movie_detail.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            self.subwindow_movie_detail.setWidget(movie_detail_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(self.subwindow_movie_detail)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            self.subwindow_movie_detail.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(report_mode="critical",
                                   title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_combine_coin_general_details_buttons(self,
                                                    form_condition=0,
                                                    form_window_title=""):
        '''
        NOTICE:
        =======

        PARAMETERS:
        ============
        :form_condition     -   In which condition the form is to be displayed?
                                This function expects two different integer:
                                0 or 1.
                                For example:
                                0 --> When the user decides to add a new record then some
                                widgets are disabled or/and changed)

                                1 --> The user wants to view, delete, or modify an
                                existing record, the user gets some an enabled widgets or
                                gets some widgets, that are disable or are not present in default.

        :form_window_title   -  Which title should get the form?
                                (For example:
                                The user opens an existing record for modifying, deleting, or just viewing.
                                Then the user gets only the title, that is saved in database. But when the
                                user decides to add a new record, the title maybe looks like in this case:
                                'Add a coin' )

        :return             -   Its returns nothing, the statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_coin = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

            coin_form = CombineCoinGeneralDetailsButtons_Window \
                                                            (int(form_condition),
                                                             self.create_cover_viewer,
                                                             self.create_select_details_form,
                                                             self.create_coin_detail_form,
                                                             form_window_title,
                                                             self.image_path_collection,
                                                             self.configuration_saver_dict,
                                                             parent=self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            coin_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_coin.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_coin.setWidget(coin_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_coin)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            subwindow_coin.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_series_season_episode(self):

        subwindow_coin = QMdiSubWindow()

        series_season_episode = Series_Season_Episode_Window(self.configuration_saver_dict,
                                                 self.image_path_collection,
                                                 self.get_info_app,
                                                 logger,
                                                 self.show_sub_window_list,
                                                 self.up_to_date_it,
                                                 self)

        series_season_episode.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_coin.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_coin.setWidget(series_season_episode)
        self.mdiArea.addSubWindow(subwindow_coin)

        '''
        calculate the widht and height of ui_person_address_book
        '''
        ui_height = float(series_season_episode.height())
        ui_width = float(series_season_episode.width())

        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        subwindow_coin.resize(ui_width, ui_height)

        subwindow_coin.move(hpos, vpos)

        subwindow_coin.showMaximized()

        self.show_sub_window_list()

    def create_cover_viewer(self, img_path, cover_form_title):
        '''
        NOTICE:
        =======
        This method creates and shows the form that displays
        the given image .

        PARAMETERS:
        ===========
        :img_path           -       Display the picture with given path.


        :cover_form_title   -       Set the title of the window with given
                                    title.

        :return             -       Its returns nothing, the statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_cover_viewer = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CoverViewer_Window()")

            cover_viewer_form = CoverViewer_Window(img_path,
                                                   cover_form_title,
                                                   self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            cover_viewer_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_cover_viewer.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_cover_viewer.setWidget(cover_viewer_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_cover_viewer, Qt.WindowMinimizeButtonHint)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")


            '''
            calculate the widht and height of the window
            '''
            ui_height = float(cover_viewer_form.height())
            ui_width = float(cover_viewer_form.width())


            hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                                form_width=ui_width)

            subwindow_cover_viewer.resize(ui_width, ui_height)

            subwindow_cover_viewer.setGeometry(hpos, vpos, ui_widht, ui_height)

            logger.info("Show the new subwindow")

            subwindow_cover_viewer.show()

            logger.info("The new subwindow is shown successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_person_selection(self):

        subwindow_person_selection = QMdiSubWindow()

        person_selection_form = SelectPerson_Window(self.up_to_date_it, self.show_sub_window_list, self)

        person_selection_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_person_selection.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_person_selection.setWidget(person_selection_form)
        self.mdiArea.addSubWindow(subwindow_person_selection)

        '''
        calculate the widht and height of ui_person_address_book
        '''
        ui_height = float(person_selection_form.height())
        ui_width = float(person_selection_form.width())

        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        subwindow_person_selection.resize(ui_width, ui_height)

        subwindow_person_selection.move(hpos, vpos)

        subwindow_person_selection.show()

        self.show_sub_window_list()

    def create_filter_form(self):
        '''
        NOTICE:
        =======

        PARAMETERS:
        ===========
        :return     -   Its returns nothing, the statement 'return'
                        terminates a function. That makes sure that the
                        function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_filter = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

            filter_form = Filter_Window(configuration_saver_dict = self.configuration_saver_dict,
                                        image_path_collection = self.image_path_collection,
                                        standard_item_model = self._standard_item_model,
                                        general_proxy_model = self._general_proxy_model,
                                        parent = self)

            logger.info("Connecting all signales of Filter_Window()-class")

            filter_form.open_person_profile_signal.connect(lambda person_id:
                self.create_person_profile(person_id = person_id,
                                           form_condition = 0))


            logger.info("Using the method (setAttribute) of Filter_Window()-class")

            filter_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Filter_Window()-class is in use successfully")

            self.send_current_scoped_session_master_data_signal.emit(self.defined_session_scope)

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_filter.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_filter.setWidget(filter_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_filter)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            subwindow_filter.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_person_profile(self,
                              person_id = None,
                              form_condition = 0):
        print "create_person_profile is called", person_id
        '''
        NOTICE:
        =======

        PARAMETERS:
        ===========
        :return     -   Its returns nothing, the statement 'return'
                        terminates a function. That makes sure that the
                        function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_person_profile = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

            person_profile_form = PersonProfile_Window(form_condition = form_condition,
                                                       person_id = person_id,
                                                       func_up_to_date = self.up_to_date_it,
                                                       configuration_saver_dict = self.configuration_saver_dict,
                                                       image_path_collection = self.image_path_collection,
                                                       standard_item_model = self._standard_item_model,
                                                       proxy_model = self._general_proxy_model,
                                                       parent = self)

            logger.info("Connecting all signales of PersonProfile_Window()-class")
            person_profile_form.open_master_data_signal.connect(self.create_master_data_form)

            person_profile_form.open_cover_viewer_signal.connect(self.create_cover_viewer)


            logger.info("Using the method (setAttribute) of PersonProfile_Window()-class")

            person_profile_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of PersonProfile_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_person_profile.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_person_profile.setWidget(person_profile_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_person_profile)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            subwindow_person_profile.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_person_prominent(self):

        subwindow_person_prominent = QMdiSubWindow()

        self.person_prominent_form = PersonProfile_Window(2,
                                                          self.up_to_date_it,
                                                          self.show_sub_window_list,
                                                          self.create_cover_viewer,
                                                          self.image_path_collection,
                                                          self)

        self.person_prominent_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_person_prominent.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_person_prominent.setWidget(self.person_prominent_form)
        self.mdiArea.addSubWindow(subwindow_person_prominent, Qt.WindowMinimizeButtonHint)

        self.person_prominent_form.showMaximized()

        self.show_sub_window_list()

    def create_person_prominent_contact(self):

        subwindow_person_prominent = QMdiSubWindow()

        self.person_prominent_form = PersonProfile_Window(3,
                                                          self.up_to_date_it,
                                                          self.show_sub_window_list,
                                                          self.create_cover_viewer,
                                                          self.image_path_collection,
                                                          self)

        self.person_prominent_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_person_prominent.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_person_prominent.setWidget(self.person_prominent_form)
        self.mdiArea.addSubWindow(subwindow_person_prominent, Qt.WindowMinimizeButtonHint)

        self.person_prominent_form.showMaximized()

        self.show_sub_window_list()


    def create_combine_note_buttons(self):

        subwindow_combine_note_buttons = QMdiSubWindow()

        combine_note_buttons_form = Note_Window(self)

        combine_note_buttons_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_combine_note_buttons.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_combine_note_buttons.setWidget(combine_note_buttons_form)
        #self.mdiArea.addSubWindow(subwindow_combine_note_buttons, Qt.WindowMinimizeButtonHint)
        self.mdiArea.addSubWindow(subwindow_combine_note_buttons)

        '''
        calculate the widht and height of the window
        '''
        ui_height = float(combine_note_buttons_form.height())
        ui_width = float(combine_note_buttons_form.width())


        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        subwindow_combine_note_buttons.resize(ui_widht, ui_height)

        subwindow_combine_note_buttons.setGeometry(hpos, vpos, ui_widht, ui_height)

        subwindow_combine_note_buttons.showMaximized()

        self.show_sub_window_list()

    def create_combine_movie_general_details_buttons(self,
                                                     scoped_session,
                                                     standard_item_model,
                                                     general_proxy_model,
                                                     work_mode,
                                                     show_messagebox = False,
                                                     film_id = None,
                                                     form_window_title = None):
        '''
        NOTICE:
        =======
        This method creates and shows the form that displays
        the given image .

        PARAMETERS:
        ===========
        :film_id            -       Display the picture with given path.


        :form_window_title  -       Set the title of the window with given
                                    title.

        :return             -       Its returns nothing, the statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_combine_movie_general_details_buttons = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CombineMovieGeneralDetailsButtons_Window()")

            combine_movie_general_details_buttons_form = CombineMovieGeneralDetailsButtons_Window \
                                                         (scoped_session = scoped_session,
                                                          film_id = film_id,
                                                          create_cover_viewer = self.create_cover_viewer,
                                                          create_select_details_form = self.create_select_details_form,
                                                          form_title = form_window_title,
                                                          configuration_saver_dict =self.configuration_saver_dict,
                                                          image_path_collection = self.image_path_collection,
                                                          create_error_form = self.create_error_form,
                                                          standard_item_model = standard_item_model,
                                                          general_proxy_model = general_proxy_model,
                                                          work_mode = work_mode,
                                                          show_messagebox = show_messagebox,
                                                          parent = self,
                                                          app = self._app)#self.parent)

            logger.info("Connecting all signales of PersonProfile_Window()-class")

            combine_movie_general_details_buttons_form.open_master_data_signal.connect(self.create_master_data_form)

            combine_movie_general_details_buttons_form.get_current_scoped_session_signal.connect(lambda:
                self.send_current_scoped_session_combine_movie_general_details_buttons_form_signal.emit(self.defined_session_scope))

            logger.info("Using the method (setAttribute) of CombineMovieGeneralDetailsButtons_Window()-class")

            combine_movie_general_details_buttons_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of CombineMovieGeneralDetailsButtons_Window()-class is in use successfully")

            self.send_current_scoped_session_combine_movie_general_details_buttons_form_signal.emit(self.defined_session_scope)

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_combine_movie_general_details_buttons.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_combine_movie_general_details_buttons.setWidget(combine_movie_general_details_buttons_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_combine_movie_general_details_buttons)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow")

            subwindow_combine_movie_general_details_buttons.showMaximized()

            logger.info("The new subwindow is shown successfully")


        except Exception as ex:

            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_combine_comic_general_details_buttons(self,
                                                     comic_id=None,
                                                     form_condition=0,
                                                     form_window_title=None):
        '''
        NOTICE:
        =======
        This method creates and shows the form.

        PARAMETERS:
        ===========
        :form_condition:    -   We except two different integer: 0 or 1. In which condition the form is to be displayed?
                                0 --> When the user decides to add a new comic book then some
                                widgets are disabled or/and changed). By default, the condition is set on zero.

                                1 --> The user wants to view, delete, or modify an
                                existing record, the user gets some an enabled widgets or
                                gets some widgets, that are disable or are not present in default.

        :form_window_title: -   We except a text for setting window title. Which title should get the form?
                                For instance: The user opens an existing comic book for modifying, deleting, or just viewing.
                                Then the user gets only the title, that is saved in database. But when the
                                user decides to add a new comic book, the title maybe looks like in this case:
                                'Add a comic book' )

        :return             -   Its returns nothing, the statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_combine_comic_general_details_buttons = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:

          logger.info("Try to create an instance of the class of CoverViewer_Window()")

          combine_comic_general_details_buttons_form = CombineComicGeneralDetailsButtons_Window \
               (int(form_condition),
                self.create_cover_viewer,
                self.create_store_on_medium,
                form_window_title,
                self.image_path_collection,
                parent=self)

          logger.info("Using the method (setAttribute) of Search_Window()-class")

          combine_comic_general_details_buttons_form.setAttribute(Qt.WA_DeleteOnClose)

          logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

          logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

          subwindow_combine_comic_general_details_buttons.setAttribute(Qt.WA_DeleteOnClose)

          logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

          logger.info("Set an internal widget by calling setWidget()")

          subwindow_combine_comic_general_details_buttons.setWidget(combine_comic_general_details_buttons_form)

          logger.info("An internal widget is set by calling setWidget() successfully")

          logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

          self.mdiArea.addSubWindow(subwindow_combine_comic_general_details_buttons)

          logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

          logger.info("Show the new subwindow")

          subwindow_combine_comic_general_details_buttons.showMaximized()

          logger.info("The new subwindow is shown successfully")

        except Exception as ex:

          logger.info("An error has occurred..")

          desired_trace = format_exc(exc_info())

          logger.error(desired_trace)

          self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_collection_form(self):

        subwindow_profile_contact = QMdiSubWindow()

        self.profile_contact_form = Collection_Window(1, self.up_to_date_it, self.show_sub_window_list,
                                                         self.create_cover_viewer, self.image_path_collection, self)

        self.profile_contact_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_profile_contact.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_profile_contact.setWidget(self.profile_contact_form)
        #self.mdiArea.addSubWindow(subwindow_combine_profile_contact, Qt.WindowMinimizeButtonHint)
        self.mdiArea.addSubWindow(subwindow_profile_contact)
        mdi_width, mdi_height = self.get_size_mdi_area()

        # calculate the widht and height of the mdi widget
        # and resize the created window based on the calculate
        #show_width=(mdi_width/2.5)
        #show_height=(mdi_height/1.1)
        #subwindow_combine_profile_contact.resize(show_width, show_height)

        # calculate the widht and height of the window
        self.p_height = int(self.profile_contact_form.height())
        #print "hoch_pp_check " + str(self.p_height)
        self.p_widht = int(self.profile_contact_form.width())
        #print "breit_pp_check " + str(self.p_widht)


        # The horizontal position is calulated as screenwidth - windowwidth /2
        hpos = (mdi_width - self.p_widht) / 2.3

        # And vertical position the same, but with the height dimensions
        vpos = (mdi_height - self.p_height) / 3.6

        # And the move call repositions the window
        # and the QMdiSubWindow
        self.profile_contact_form.move(hpos, vpos)
        subwindow_profile_contact.move(hpos, vpos)

        #self.profile_contact_form.show()
        self.profile_contact_form.showMaximized()

        self.show_sub_window_list()

    def create_profile_contact(self):

        subwindow_profile_contact = QMdiSubWindow()

        self.profile_contact_form = PersonProfile_Window(1,
                                                         self.up_to_date_it,
                                                         self.show_sub_window_list,
                                                         self.create_cover_viewer,
                                                         self.image_path_collection,
                                                         self)

        self.profile_contact_form.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_profile_contact.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_profile_contact.setWidget(self.profile_contact_form)
        #self.mdiArea.addSubWindow(subwindow_combine_profile_contact, Qt.WindowMinimizeButtonHint)
        self.mdiArea.addSubWindow(subwindow_profile_contact)
        mdi_width, mdi_height = self.get_size_mdi_area()

        # calculate the widht and height of the mdi widget
        # and resize the created window based on the calculate
        #show_width=(mdi_width/2.5)
        #show_height=(mdi_height/1.1)
        #subwindow_combine_profile_contact.resize(show_width, show_height)

        # calculate the widht and height of the window
        self.p_height = int(self.profile_contact_form.height())
        #print "hoch_pp_check " + str(self.p_height)
        self.p_widht = int(self.profile_contact_form.width())
        #print "breit_pp_check " + str(self.p_widht)


        # The horizontal position is calulated as screenwidth - windowwidth /2
        hpos = (mdi_width - self.p_widht) / 2.3

        # And vertical position the same, but with the height dimensions
        vpos = (mdi_height - self.p_height) / 3.6

        # And the move call repositions the window
        # and the QMdiSubWindow
        self.profile_contact_form.move(hpos, vpos)
        subwindow_profile_contact.move(hpos, vpos)

        #self.profile_contact_form.show()
        self.profile_contact_form.showMaximized()

        self.show_sub_window_list()

    def create_movie_detail(self, form_title_movie, film_id, detail_type):
        '''
        NOTICE:
        =======
        This method creates and shows the form that displays
        the given image .

        PARAMETERS:
        ===========
        :form_title_movie           -       Display the picture with given path.


        :film_id                    -       Set the title of the window with given
                                            title.

        :detail_type                -       Set the title of the window with given
                                            title.

        :return                     -       Its returns nothing, the statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_movie_detail = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CoverViewer_Window()")

            movie_detail_form = MovieDetail_Window(film_id,
                                                   form_title_movie,
                                                   detail_type,
                                                   self.image_path_collection,
                                                   self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            movie_detail_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_movie_detail.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_movie_detail.setWidget(movie_detail_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            self.mdiArea.addSubWindow(subwindow_movie_detail, Qt.WindowMinimizeButtonHint)

            logger.info("Show the new subwindow")

            subwindow_movie_detail.showMaximized()

            logger.info("The new subwindow is shown successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_combine_serie_general_details_buttons(self,
                                                     form_condition,
                                                     form_window_title):
        '''
            NOTICE:
            =======

            PARAMETERS:
            ============
            :form_condition     -   In which condition the form is to be displayed?
                                    This function expects two different integer:
                                    0 or 1.
                                    For instance:
                                    0 --> When the user decides to add a new comic book then some
                                    widgets are disabled or/and changed)

                                    1 --> The user wants to view, delete, or modify an
                                    existing comic book, the user gets some an enabled widgets or
                                    gets some widgets, that are disable or are not present in default.

            :form_window_title  -   Which title should get the form?
                                    (For instance:
                                    The user opens an existing comic book for modifying, deleting, or just viewing.
                                    Then the user gets only the title, that is saved in database. But when the
                                    user decides to add a new comic book, the title maybe looks like in this case:
                                    'Add a comic book' )

            :return             -   Its returns nothing, the statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_combine_series_general_details_buttons = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of CoverViewer_Window()")

            combine_series_general_details_buttons_form = CombineSerieGeneralDetailsButtons_Window \
                                                                                                   (form_condition=int(form_condition),
                                                                                                    movie_detail=self.create_store_on_medium,
                                                                                                    configuration_saver_dict=self.configuration_saver_dict,
                                                                                                    image_path_collection=self.image_path_collection,
                                                                                                    create_series_season_episode=self.create_series_season_episode,
                                                                                                    create_cover_viewer=self.create_cover_viewer,
                                                                                                    form_title=form_window_title,
                                                                                                    serie_id=None,
                                                                                                    parent=self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            combine_series_general_details_buttons_form.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_combine_series_general_details_buttons.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_combine_series_general_details_buttons.setWidget(combine_series_general_details_buttons_form)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_combine_series_general_details_buttons)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow")

            subwindow_combine_series_general_details_buttons.showMaximized()

            logger.info("The new subwindow is shown successfully")

        except Exception as ex:

            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_history_form(self):
        '''
            NOTICE:
            =======
            Create the form to learn the version history.

            PARAMETERS
            ==========

            :return     -   Its returns nothing, the statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        try:
            logger.info("Try to create an instance of the class of CoverViewer_Window()")

            version_history_form = NewInVersion_Window(self.configuration_saver_dict.dict_set_general_settings["URLServerVersion"],
                                                    self.tr(" Versionsgeschichte"),
                                                    self.tr(" Die Entwicklung der Versionen im Ãberblick"),
                                                    self.tr("Einen Moment bitte, die Versionsgeschichte wirde geladen..."),
                                                    self.configuration_saver_dict, self)

            logger.info("Show the version_history_form")

            version_history_form.show()

            logger.info("The version_history_form is shown successfully")

        except Exception as ex:

            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_movie_form(self):

        self.ui_movie = Movie_Window(0, self.up_to_date_it, self.show_sub_window_list,
                                     self.create_cover_viewer, self.configuration_saver_dict, self)

        self.mdi_subwindow_movie = QMdiSubWindow()

        #            self.search_widget = Search_Window(self.up_to_date_it, self.showQWidgetsChildren, self)
        self.ui_movie.setAttribute(Qt.WA_DeleteOnClose)
        self.mdi_subwindow_movie.setAttribute(Qt.WA_DeleteOnClose)

        self.mdi_subwindow_movie.setWidget(self.ui_movie)
        self.mdiArea.addSubWindow(self.mdi_subwindow_movie)
        self.ui_movie.showMaximized()
        self.show_sub_window_list()

    #--------------------------------------------------------------------------------------
    def create_customer_online_profile_form(self):

        logger.info('Creating an instance of the class of Register_Window()')
        self.ui_customer_online_rpofile = CustomerOnlineProfile_Window(self.configuration_saver_dict, self.get_info_app, self)

        # Now when I try to show (show()-method) a window I get two windows
        # The reason is: I open and load the ui files from compiled
        # qt resorce file in __init__-module. There is a function that opens
        # the resource file, reads the ui file an closes and returns the ui file back

        self.ui_customer_online_rpofile.show()

    #--------------------------------------------------------------------------------------
    def create_about_form(self,
                          kwargs):
        '''
           SUMMARY
           =======
           Thos method creates a form for managing masta data.

           PARAMETERS
           ==========
           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        try:
            logger.info("Try to create an instance of the class of About_Window()")

            self.ui_about = About_Window(app_info = kwargs.get('info_app'),
                                         parent = self)


            logger.info("The instance of the class of About_Window() is created")
            logger.info("Try to show the instanced and created class of About_Window() is shown")

            self.ui_about.show()

            logger.info("The instanced and created class of About_Window() is shown")

        except Exception:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

            return

    def create_master_data_form(self,
                                stack_widget_page = 0,
                                load_data = False):
        '''
           SUMMARY
           =======
           This method creates a form for managing masta data.

           PARAMETERS
           ==========
           :stack_widget_page (int):    We need this int for changing the page. By default, its zero.
                                        its means, the first page is visible.

           :load_data (bool):           When we want the program to load records again, one the page of stacked widget
                                        has been changed we set to True, otherwise to False

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary,
                                        but this statement gives me a better feeling, that says: 'return'
                                        terminates a function definitely.
        '''
        #   Let us take a look, if this window is already open. Its True
        #   we just activated this subwindow. We don't need to open a new
        #   one subwindow.
        if self._master_data_window_is_opend:
            #   Its already open, so wie just activate the existing subwindow.

            self.master_data_form.change_page(index = stack_widget_page,
                                              stack_widget_object = self.master_data_form.stackedWidget,
                                              load_data = load_data)

            self.mdiArea.setActiveSubWindow(self.subwindow_master_data)

        else:
            #   No, there isn't a subwindow, so we have to create a new subwindow.

            logger.info("Creating an instance of the class of QMdiSubWindow()")

            self.subwindow_master_data = QMdiSubWindow()

            logger.info("The instance of the class of QMdiSubWindow() is created successfully")

            try:
                logger.info("Try to create an instance of the class of CombineCoinGeneralDetailsButtons_Window()")

                self.master_data_form = MasterData_Window(image_path_collection = self.image_path_collection,
                                                          configuration_saver_dict = self.configuration_saver_dict,
                                                          standard_item_model = self._standard_item_model,
                                                          general_proxy_model = self._general_proxy_model,
                                                          page_number = stack_widget_page,
                                                          load_data = load_data,
                                                          parent = self)

                logger.info("Using the method (setAttribute) of Search_Window()-class")

                self.master_data_form.set_flat_signal.connect(self.set_flag)

                #   Ensure this windows gets garbage-collected when closed.
                self.master_data_form.setAttribute(Qt.WA_DeleteOnClose)

                logger.info("The method (setAttribute) of Search_Window()-class is in use successfully")

                self.send_current_scoped_session_master_data_signal.emit(self.defined_session_scope)

                logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

                self.subwindow_master_data.setAttribute(Qt.WA_DeleteOnClose)

                logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

                logger.info("Set an internal widget by calling setWidget()")

                self.subwindow_master_data.setWidget(self.master_data_form)

                logger.info("An internal widget is set by calling setWidget() successfully")

                logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

                self.mdiArea.addSubWindow(self.subwindow_master_data, Qt.WindowMinimizeButtonHint)

                logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

                logger.info("Show the new subwindow maximized")

                self.subwindow_master_data.showMaximized()

                logger.info("The new subwindow is maximized successfully")

                self._master_data_window_is_opend = True

            except Exception as ex:
                logger.info("An error has occurred..")

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

                self.create_error_form(title_text=self.tr("Error"),
                                       detail_mode=desired_trace)

        return


    def create_overview_ratings(self, object_category=None):

        subwindow_overview_ratings = QMdiSubWindow()

        ui_overview_ratings = OverViewRatingsClassification_Window(
                                                self.up_to_date_it,
                                                self.show_sub_window_list,
                                                self.get_info_app,
                                                self.create_rating,
                                                self.create_classification,
                                                object_category,
                                                self
                                            )

        ui_overview_ratings.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_overview_ratings.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_overview_ratings.setWidget(ui_overview_ratings)
        self.mdiArea.addSubWindow(subwindow_overview_ratings, Qt.WindowMinimizeButtonHint)

        mdi_width, mdi_height = self.get_size_mdi_area()

        # calculate the widht and height of the mdi widget
        # and resize the created window based on the calculate
        #show_width=(mdi_width/2.5)
        #show_height=(mdi_height/1.1)
        #subwindow_person_selection.resize(show_width, show_height)

        # calculate the widht and height of the window
        self.p_height = int(ui_overview_ratings.height())
        #print "hoch_pp_check " + str(self.p_height)
        self.p_widht = int(ui_overview_ratings.width())
        #print "breit_pp_check " + str(self.p_widht)


        # The horizontal position is calulated as screenwidth - windowwidth /2
        hpos = (mdi_width - self.p_widht) / 2

        # And vertical position the same, but with the height dimensions
        vpos = (mdi_height - self.p_height) / 2

        # And the move call repositions the window
        # and the QMdiSubWindow
        #self.person_selection_form.move(hpos, vpos)
        #subwindow_person_selection.move(hpos, vpos)

        # The first two parameters are the x and y positions of the window.
        # The third is the width and the fourth is the height of the window.
        # In fact, it combines the resize() and move() methods in one method
        subwindow_overview_ratings.setGeometry(hpos, vpos, self.p_widht, self.p_height)
        #self.person_selection_form.setGeometry(hpos, vpos, self.p_widht, self.p_height)

        subwindow_overview_ratings.show()

        self.show_sub_window_list()
        #except Exception as ex:
        #    print ex

    def create_combine_ratings_classification(self, rating_type=""):

        subwindow_combine_ratings_classification = QMdiSubWindow()

        ui_combine_ratings_classification = CombineRatingsClassification_Window(
                                                self.up_to_date_it,
                                                self.show_sub_window_list,
                                                self.get_info_app,
                                                "Altersfreigaben hinzufÃ¼gen / bearbeiten",
                                                rating_type,
                                                self
                                            )

        ui_combine_ratings_classification.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_combine_ratings_classification.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_combine_ratings_classification.setWidget(ui_combine_ratings_classification)
        self.mdiArea.addSubWindow(subwindow_combine_ratings_classification, Qt.WindowMinimizeButtonHint)

        mdi_width, mdi_height = self.get_size_mdi_area()

        # calculate the widht and height of the mdi widget
        # and resize the created window based on the calculate
        #show_width=(mdi_width/2.5)
        #show_height=(mdi_height/1.1)
        #subwindow_person_selection.resize(show_width, show_height)

        # calculate the widht and height of the window
        self.p_height = int(ui_combine_ratings_classification.height())
        #print "hoch_pp_check " + str(self.p_height)
        self.p_widht = int(ui_combine_ratings_classification.width())
        #print "breit_pp_check " + str(self.p_widht)


        # The horizontal position is calulated as screenwidth - windowwidth /2
        hpos = (mdi_width - self.p_widht) / 2

        # And vertical position the same, but with the height dimensions
        vpos = (mdi_height - self.p_height) / 2

        # And the move call repositions the window
        # and the QMdiSubWindow
        #self.person_selection_form.move(hpos, vpos)
        #subwindow_person_selection.move(hpos, vpos)

        # The first two parameters are the x and y positions of the window.
        # The third is the width and the fourth is the height of the window.
        # In fact, it combines the resize() and move() methods in one method
        subwindow_combine_ratings_classification.setGeometry(hpos, vpos, self.p_widht, self.p_height)
        #self.person_selection_form.setGeometry(hpos, vpos, self.p_widht, self.p_height)

        subwindow_combine_ratings_classification.show()

        self.show_sub_window_list()
        #except Exception as ex:
        #    print ex

    def move_created_form_center_mdi(self, form_height=0, form_width=0):

        mdi_width, mdi_height = self.get_size_mdi_area()

        # calculate the widht and height of the window
        ui_height = float(form_height)
        ui_widht = float(form_width)

        # The horizontal position is calulated as screenwidth - windowwidth /2
        hpos = (mdi_width - ui_widht) / 2

        # And vertical position the same, but with the height dimensions
        vpos = (mdi_height - ui_height) / 2

        # The first two parameters are the x and y positions of the window.
        # The third is the width and the fourth is the height of the window.
        # In fact, it combines the resize() and move() methods in one method
        return hpos, vpos, ui_widht, ui_height

    def create_classification(self, show_format=None):

        subwindow_create_classification = QMdiSubWindow()

        ui_create_classification = RatingClassification_Window(
                                                self.up_to_date_it,
                                                self.show_sub_window_list,
                                                self.get_info_app,
                                                show_format,
                                                self
                                            )

        ui_create_classification.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_create_classification.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_create_classification.setWidget(ui_create_classification)
        self.mdiArea.addSubWindow(subwindow_create_classification, Qt.WindowMinimizeButtonHint)

        '''
        calculate the widht and height of the window
        '''
        ui_height = float(ui_create_classification.height())
        ui_width = float(ui_create_classification.width())


        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        subwindow_create_classification.setGeometry(hpos, vpos, ui_widht, ui_height)

        subwindow_create_classification.show()

        self.show_sub_window_list()
        #except Exception as ex:
        #    print ex


    def create_rating(self, object_category=None):

        subwindow_add_ratings = QMdiSubWindow()

        ui_ratings = Ratings_Window(
                                                self.up_to_date_it,
                                                self.show_sub_window_list,
                                                self.get_info_app,
                                                object_category,
                                                self
                                            )

        ui_ratings.setAttribute(Qt.WA_DeleteOnClose)
        subwindow_add_ratings.setAttribute(Qt.WA_DeleteOnClose)

        subwindow_add_ratings.setWidget(ui_ratings)
        self.mdiArea.addSubWindow(subwindow_add_ratings, Qt.WindowMinimizeButtonHint)

        '''
        calculate the widht and height of the window
        '''
        ui_height = float(ui_ratings.height())
        ui_width = float(ui_ratings.width())


        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        subwindow_add_ratings.setGeometry(hpos, vpos, ui_widht, ui_height)
        subwindow_add_ratings.show()

        self.show_sub_window_list()
        #except Exception as ex:
        #    print ex


    def create_store_on_medium(self, form_title_movie, pushbutton_status, detail_type):

        mdi_subwindow_add_person_address_book = QMdiSubWindow()

        ui_person_address_book = StoreOnMediumn_Window(self.up_to_date_it,
                                                       self.show_sub_window_list,
                                                       self.create_person_profile,
                                                       self.create_person_selection,
                                                       self.configuration_saver_dict,
                                                       self.create_movie_detail,
                                                       0,
                                                       form_title_movie,
                                                       self)

        ui_person_address_book.setAttribute(Qt.WA_DeleteOnClose)
        mdi_subwindow_add_person_address_book.setAttribute(Qt.WA_DeleteOnClose)

        mdi_subwindow_add_person_address_book.setWidget(ui_person_address_book)
        self.mdiArea.addSubWindow(mdi_subwindow_add_person_address_book, Qt.WindowMinimizeButtonHint)

        '''
        calculate the widht and height of ui_person_address_book
        '''
        ui_height = float(ui_person_address_book.height())
        ui_width = float(ui_person_address_book.width())


        hpos, vpos, ui_widht, ui_height = self.move_created_form_center_mdi(form_height=ui_height,
                                                                            form_width=ui_width)

        mdi_subwindow_add_person_address_book.move(hpos, vpos)

        mdi_subwindow_add_person_address_book.show()

        self.show_sub_window_list()

    def create_setting_form(self):
        '''
           SUMMARY
           =======
           This method creates and displays the window for settings.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        subwindow_setting = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of Setting_Window()")

            ui_setting = Setting_Window(configuration_saver_dict=self.configuration_saver_dict,
                                             close_application=self.unload_main,
                                             image_path_collection=self.image_path_collection,
                                             default_ini_file=self.default_ini_file,
                                             create_error_form=self.create_error_form,
                                             parent=self)

            logger.info("Using the method (setAttribute) of Setting_Window()-class")

            ui_setting.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of Setting_Window()-class is in use successfully")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            subwindow_setting.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("The method (setAttribute) of QMdiSubWindow()-class is in use successfully")

            logger.info("Set an internal widget by calling setWidget()")

            subwindow_setting.setWidget(ui_setting)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(subwindow_setting, Qt.WindowMinimizeButtonHint)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow")

            subwindow_setting.showMaximized()

            logger.info("The new subwindow is shown successfully")

        except Exception:

            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_feedback_form(self):
        '''
           SUMMARY
           =======
           This method creates and displays the window for sending feedback.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        try:
            logger.info("Try to create an instance of the class of Feedback_Window()")

            feedback_form = Feedback_Window(self.get_info_app, self)

            logger.info("Show the new window")

            feedback_form.show()

            logger.info("The new window is shown successfully")

        except Exception:

            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_customer_login_form(self, kwargs):
        '''
           SUMMARY
           =======
           This method is used to create a form for logging in as customer.

           PARAMETERS
           ==========
           :kwargs (dict):  The data structure dictionary is used to pass keyworded variable length
                            of arguments. We used it because we want to handle named arguments in
                            this method.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Try to create a register form")

        try:
            logger.info("Try to create an instance of the class of CustomerLogin_Window()")

            self.customer_login_form = CustomerLogin_Window(self, **kwargs)

            logger.info("The instance of the class of CustomerLogin_Window() is created")

            logger.info("Try to show the instanced and created class of CustomerLogin_Window()")

            self.customer_login_form.show()

            logger.info("The instanced and created class of CustomerLogin_Window() is shown")

        except Exception:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text = self.tr("Error"),
                                   detail_mode = desired_trace)

            return



    def check_arequired_access_data(self, kwargs):
        '''
           SUMMARY
           =======
           This method is used to check the required access data.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Check required access data")

        kwargs.get('configuration_saver_dict').ht_access['htaccess_username'] = 'admin'
        kwargs.get('configuration_saver_dict').ht_access['htpassword'] = 'danny82'

        base_path = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('BasePath')

        TEMP_UPDATE_FOLDER = path.join(base_path, 'update')
        print "check_arequired_access_data, TEMP_UPDATE_FOLDER:", TEMP_UPDATE_FOLDER

        kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToTempFolder"] = TEMP_UPDATE_FOLDER

        if kwargs.get('configuration_saver_dict').ht_access.get('htaccess_username') and \
           kwargs.get('configuration_saver_dict').ht_access.get('htpassword'):

           self.create_download_form(kwargs = kwargs)
        else:

            icon = kwargs.get('image_path_collection').warning_48x48
            title_text = self.tr('LogIN Required')
            error_message = self.tr('You are not logged in.\n\n' \
                                    'You cannot update your program, because you need log in or sign up for an account to access this service.\n\n' \
                                    'You do not have an account? Create one now. Go in the menu to the question mark and then on sign up.\n\n' \
                                    'If you think this message is wrong, please consult your administrators about getting the necessary permissions.')

            self.create_generally_crritcal_msg(
              err_title = title_text,
              err_msg = error_message,
              set_flag = True,
              detail_msg = None,
              icon = icon)

        return

    def create_download_form(self, kwargs):
        '''
           SUMMARY
           =======
           This method is used to create a download form.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Try to create a download form")

        try:
            logger.info("Creating an instance of the class of Download_Window()")

            download_folder = kwargs['configuration_saver_dict'].dict_set_general_settings['PathToTempFolder']
            print "create_download_form, download_folder:", download_folder

            self.download_form = Download_Window(self, **kwargs)

            logger.info("Try to show the instanced and created class of Download_Window() is shown")

            self.download_form.show()

            logger.info("The instanced and created class of Download_Window() is shown")

        except Exception:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text = self.tr("Error"),
                                   detail_mode = desired_trace)

            return

    def create_register_form(self):
        '''
           SUMMARY
           =======
           This method is used to create a register form.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Try to create a register form")

        try:
            logger.info("Creating an instance of the class of Register_Window()")

            self.register_form = Register_Window(configuration_saver_dict = self.configuration_saver_dict,
                                                 info_app = self.get_info_app,
                                                 image_path_collection = self.image_path_collection,
                                                 parent = self)

            logger.info("The instance of the class of Register_Window() is created")

            logger.info("Try to show the instanced and created class of Register_Window() is shown")

            self.register_form.show()

            logger.info("The instanced and created class of Register_Window() is shown")
        except Exception:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

            return

    def create_db_login_form(self,
                             kwargs):
        '''
           SUMMARY
           =======
           This method is used to create a form for logging in to database.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Open Log-In window to log into the database.")

        try:

            logger.info("Try to create an instance of the class of DB_Login_Window()")

            self.database_login_form = DB_Login_Window(
              safe_config_parser = kwargs.get('safe_config_parser'),
              configuration_saver_dict = kwargs.get('configuration_saver_dict'),
              image_path_collection = kwargs.get('image_path_collection'),
              dbms = kwargs.get('safe_config_parser').get('General_Configuration', 'database_type').lower(),
              parent = self)

            logger.info("The instance of the class of DB_Login_Window() is created successfully")

            self.database_login_form.save_engine_scoped_session_signal.connect(
              lambda
              engine,
              scoped_session:
              self.save_engine_scoped_session(
                engine = engine,
                scoped_session = scoped_session,
                kwargs = kwargs))

            self.database_login_form.show()

        except Exception:
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_search_form(self,
                          kwargs):
        '''
           SUMMARY
           =======
           This method is used to create a form for searching in the database.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Open search window.")

        mdi_subwindow_search = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of Search_Window()")

            self.search_widget = Search_Window(kwargs.get('image_path_collection'),
                                               kwargs.get('configuration_saver_dict'),
                                               self.create_error_form,
                                               self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            self.search_widget.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            mdi_subwindow_search.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Set an internal widget by calling setWidget()")

            mdi_subwindow_search.setWidget(self.search_widget)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(mdi_subwindow_search)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            self.search_widget.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            logger.info("Create an instance of the class of create_error_form()")

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_select_details_form(self, list_items):

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        mdi_subwindow_select_details = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of Search_Window()")

            self.select_details_widget = Select_Details_Window(logger,
                                                               list_items,
                                                               self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            self.select_details_widget.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            mdi_subwindow_select_details.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Set an internal widget by calling setWidget()")

            mdi_subwindow_select_details.setWidget(self.select_details_widget)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(mdi_subwindow_select_details)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            self.select_details_widget.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def create_connection_profiles_form(self):

        logger.info("Creating an instance of the class of QMdiSubWindow()")

        mdi_subwindow_connection_profiles = QMdiSubWindow()

        logger.info("The instance of the class of QMdiSubWindow() is created successfully")

        try:
            logger.info("Try to create an instance of the class of Search_Window()")

            self.connect_profiles_widget = ConnectionProfiles_Window(image_path_collection=self.image_path_collection, parent=self)

            logger.info("Using the method (setAttribute) of Search_Window()-class")

            self.connect_profiles_widget.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Using the method (setAttribute) of QMdiSubWindow()-class")

            mdi_subwindow_connection_profiles.setAttribute(Qt.WA_DeleteOnClose)

            logger.info("Set the Attribute)on Qt.WA_DeleteOnClose")

            logger.info("Set an internal widget by calling setWidget()")

            mdi_subwindow_connection_profiles.setWidget(self.connect_profiles_widget)

            logger.info("An internal widget is set by calling setWidget() successfully")

            logger.info("Adds widget as a new subwindow to the mdiArea by calling addSubWindow()")

            self.mdiArea.addSubWindow(mdi_subwindow_connection_profiles)

            logger.info("A widget is added as a new subwindow to the mdiArea by calling addSubWindow() successfully")

            logger.info("Show the new subwindow maximized")

            self.connect_profiles_widget.showMaximized()

            logger.info("The new subwindow is maximized successfully")

        except Exception as ex:
            logger.info("An error has occurred..")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def up_to_date_it(self): pass

    #--------------------------------------------------------------------------------------
    def create_error_form(self, title_text=None, detail_mode=None):

        logger.info("Creating an instance of the class of Register_Window()")

        error_form = Error_Window(title_text,
                                  detail_mode,
                                  self)

        logger.info("he instance of the class of Register_Window() is created")

        error_form.show()

        logger.info("Show instanced and created class of Register_Window()")

    #--------------------------------------------------------------------------------------


    # TODO: This window is used only as an example. Later, this example will be deleted.
    def create_check_form(self):
        pass

    # TODO: This function have to edit!
    def close_all_subwindows(self):
        a = len(self.mdiArea.subWindowList())
        self.mdiArea.closeAllSubWindows()
        print "How much?", a
        # while a > 0:
        #     a = a - 1
        #     print a
        # print "Done"
        # self.up_to_date_it()

        # print "STATUS [OK]  ", FILE_NAME, ": The function (close_all_subwindows) was called"

    #--------------------------------------------------------------------------------------
    def delete_all_lists(self):
        self.subwindow_list[:] = []

    #--------------------------------------------------------------------------------------
    def check_exists_sub_form(self, Title):
        for window in self.mdiArea.subWindowList():
            print window
            if Title == window.windowTitle():
                return True

    #--------------------------------------------------------------------------------------
    def create_second_form(self):
        from ..modules_ui.ui_pp_second import Second_Window

        self.subwindow_secondform = QMdiSubWindow()
        self.second_form = Second_Window(self.close_sub_form, self.subwindow_secondform, self)
        self.second_form.setAttribute(Qt.WA_DeleteOnClose)
        self.subwindow_secondform.setWidget(self.second_form)
        self.mdiArea.addSubWindow(self.subwindow_secondform)
        self.second_form.show()
        print "STATUS [MESSAGE]  ", FILE_NAME, ": second_form was created and is now displayed"
        self.show_sub_window_list()  # Check, if are some Sub_Widgets open

    #--------------------------------------------------------------------------------------

    def get_focus_on_subwindow(self, form_name):
        form_name.setFocus()

    # TODO: Edit this close-function
    def close_sub_form(self, form_name):
        #self.mdiArea.removeSubWindow(sub_name)
        self.mdiArea.removeSubWindow(form_name)
        print "STATUS [OK]  ", FILE_NAME, ": The function (close_sub_form) was called"
        self.show_sub_window_list()


    def create_actions_tray(self,
                           kwargs):
        '''
           SUMMARY
           =======
           In this method we create QAction for System tray applications (or menu bar applications).

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Create QAction for System tray applications')

        self.aboutAction = QAction(QIcon(self.image_path_collection.SystemTrayIcon_about),
                                   self.tr("About {}".format(kwargs.get('info_app').dict_info.get('product_name'))),
                                   self, triggered = lambda: self.create_about_form(kwargs = self._kwargs))

        self.maximizeAction = QAction(self.tr("Open {}".format(kwargs.get('info_app').dict_info.get('product_name'))),
                                      self, triggered=self.show_me)

        self.quitAction = QAction(QIcon(self.image_path_collection.SystemTrayIcon_exit),
                                  self.tr("Close {}".format(kwargs.get('info_app').dict_info.get('product_name'))),
                                  self, triggered=exit)

        return

    def set_up_mdi_area(self):
        '''
           SUMMARY
           =======
           This method sets the mdi area up.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Set the MDI Area up')

        self.mdiArea.setOption(QMdiArea.DontMaximizeSubWindowOnActivation, True)
        self.setCentralWidget(self.mdiArea)

        return

    def create_tray_icon(self):
        self.hide()
        self.trayIconMenu = QMenu(self)
        self.trayIcon = QSystemTrayIcon(QIcon(self.image_path_collection.SystemTrayIcon_logo))
        self.trayIcon.setToolTip(self.get_info_app.dict_info["product_name"])
        self.trayIconMenu.addAction(self.aboutAction)

        self.trayIconMenu.addAction(self.maximizeAction)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.quitAction)
        self.trayIcon.setContextMenu(self.trayIconMenu)
        self.trayIcon.show()

    #--------------------------------------------------------------------------------------
    def ask_to_restart(self):
        setIc_OK = QIcon(self.image_path_collection.check_icon)
        setIc_No = QIcon(self.image_path_collection.no_icon_unload_mdi_msgbox)
        setIc_Cancel = QIcon(self.image_path_collection.cancel_icon_unload_mdi_msgbox)


        mBox = custom_message_box(self, self.tr("Sie sind gerade dabei das Programm neuzustarten. \n"
                                                "\n "
                                                "MÃ¶chten Sie das Programm wirklich neustarten?"), "Hinweis",
                                  icon = QMessageBox.Question)
        mBox.addButton(
            QPushButton(setIc_OK, self.tr(" Ja")),
            mBox.YesRole)  # An "OK" button defined with the AcceptRole
        mBox.addButton(
            QPushButton(setIc_No, self.tr(" Nein")),
            mBox.NoRole)  # An "OK" button defined with the AcceptRole
        mBox.addButton(
            QPushButton(setIc_Cancel, self.tr(" Abbrechen")),
            mBox.RejectRole)  # An "OK" button defined with the AcceptRole
        #mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.
        ret = mBox.exec_()
        if ret == 0:
            self.mdiArea.closeAllSubWindows()
            restart.restart_program()
            exit()
        elif ret == 1:
            pass
        else:
            pass

    #--------------------------------------------------------------------------------------
    def show_me(self):
        self.trayIcon.setVisible(False)  # After showing die MainWindow make this TrayIcon in systray-area visible.
        self.showMaximized()

    def unload_main(self):
        self.unload_me = True
        self.close()

    def create_missing_folders(self, base_path):
        path = manage_folder.generator_top_sub_folder(base_path)

        for i in path:
            try:
                manage_folder.mkdir_top_sub_folder(i)
            except error, e:
                pass

    def save_setting_ini(self, set_get_settings):
        '''
        NOTICE:
        =======
            We save all settings in the ini file.

        PARAMETERS:
        ===========
            :set_get_settings   -       It excepts a dictionary.
                                        In module, named config, there is
                                        class, named ConfigurationSaver, you will
                                        find a dictionary.
            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        try:
            logger.info("Trying ti write all setting in a ini file")

            create_write_ini_file(set_get_settings, self.default_ini_file)

            logger.info("All setting is written in a ini file successfully")

        except (Exception, IOError, ValueError):

            logger.info("An error has occurred")

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return


    def get_size_mdi_area(self):
        width_mdiArea = int(self.mdiArea.width())
        height_mdiArea = int(self.mdiArea.height())
        return width_mdiArea, height_mdiArea

    def resizeEvent(self, event):
        pass

    def get_bool_value(self, key):
        try:
            return parsing_str_to_bool(self.configuration_saver_dict.dict_set_general_settings[key])
        except ValueError:
            return False

    def set_visible_sub_menu(self,
                        kwargs):
        '''
           SUMMARY
           =======
           This method sets visible on all sub menu(s). Its a part for managing menu items.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Restore all states")

        try:

            self.ui_mdi.menu_film.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_movie'))
            self.ui_mdi.menu_book.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_book'))
            self.ui_mdi.menu_music.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_music'))
            self.ui_mdi.menu_coin.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_coin'))
            self.ui_mdi.menu_contact.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_contact'))
            self.ui_mdi.menu_person.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_person'))
            self.ui_mdi.menu_note.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_note'))
            self.ui_mdi.menu_video_game.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_video_game'))
            self.ui_mdi.menu_quotation.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_quotation'))
            self.ui_mdi.menu_stamp.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_stamp'))
            self.ui_mdi.menu_wine.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_wine'))
            self.ui_mdi.menu_rent.menuAction().setVisible(kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_rent'))

        except (NoOptionError, MissingSectionHeaderError):
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('No option error')

            err_msg = self.tr("We are sorry, but an error in your configuration has occurred. \
                  \nAdditional details have been looged. The program is started anyway. \
                  \n\nIf the issue continues, please visit our technical support.")

            icon = kwargs.get('image_path_collection').warning_48x48_default

            if not desired_trace.find('No option') == -1:
                self.create_generally_crritcal_msg(
                    err_title = title_text,
                    err_msg = err_msg,
                    set_flag = True,
                    detail_msg = None,
                    icon = icon)
        return

    def init_toolbuttons(self,
                        kwargs):
        '''
           SUMMARY
           =======
           This method sets all toolbuttons.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize all toolbuttons")

        self.toolbutton_mowie = QToolButton()
        self.toolbutton_mowie.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.actionPic_movie = self.toolbutton_mowie.setIcon(QIcon(kwargs.get('image_path_collection').film_32x32))
        self.actionText_movie = self.toolbutton_mowie.setText(self.tr("Film"))

        self.toolbutton_book = QToolButton()
        self.toolbutton_book.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_book.setIcon(QIcon(kwargs.get('image_path_collection').book_32x32))
        self.toolbutton_book.setText(self.tr("Book"))

        self.toolbutton_music = QToolButton()
        self.toolbutton_music.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_music.setIcon(QIcon(kwargs.get('image_path_collection').music_32x32))
        self.toolbutton_music.setText(self.tr("Music"))

        self.toolbutton_person = QToolButton()
        self.toolbutton_person.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_person.setIcon(QIcon(kwargs.get('image_path_collection').person_32x32))
        self.toolbutton_person.setText(self.tr("Person"))

        self.toolbutton_contact = QToolButton()
        self.toolbutton_contact.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_contact.setIcon(QIcon(kwargs.get('image_path_collection').contact_32x32))
        self.toolbutton_contact.setText(self.tr("Contact"))

        self.toolbutton_coin = QToolButton()
        self.toolbutton_coin.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_coin.setIcon(QIcon(kwargs.get('image_path_collection').coin_32x32))
        self.toolbutton_coin.setText(self.tr("Coin"))

        self.toolbutton_game = QToolButton()
        self.toolbutton_game.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_game.setIcon(QIcon(kwargs.get('image_path_collection').video_game_32x32))
        self.toolbutton_game.setText(self.tr("Video game"))

        self.toolbutton_stamp = QToolButton()
        self.toolbutton_stamp.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_stamp.setIcon(QIcon(kwargs.get('image_path_collection').stamp_32x32))
        self.toolbutton_stamp.setText(self.tr("Stamp"))

        self.toolbutton_note = QToolButton()
        self.toolbutton_note.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_note.setIcon(QIcon(kwargs.get('image_path_collection').note_32x32))
        self.toolbutton_note.setText(self.tr("Note"))

        self.toolbutton_quotation = QToolButton()
        self.toolbutton_quotation.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_quotation.setIcon(QIcon(kwargs.get('image_path_collection').quotation_32x32))
        self.toolbutton_quotation.setText(self.tr("Quotation"))

        self.toolbutton_wine = QToolButton()
        self.toolbutton_wine.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_wine.setIcon(QIcon(kwargs.get('image_path_collection').wine_32x32))
        self.toolbutton_wine.setText(self.tr("Wine"))

        self.toolbutton_rent = QToolButton()
        self.toolbutton_rent.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.toolbutton_rent.setIcon(QIcon(kwargs.get('image_path_collection').rent_32x32))
        self.toolbutton_rent.setText(self.tr("Rent"))

        return

    def init_menu_for_toolbuttons(self,
                                 image_path_collection):
        '''
           SUMMARY
           =======
           This method Inistializes all menu for toolbuttons.

           PARAMETERS
           ==========
           :image_path_collection (str):  There is the path to the compiled image.

           RETURNS
           =======
           :return:                       Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                          me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize all toolbuttons")

        # Movie
        self.toolmenu_movie = QMenu()

        self.action_movie_add = self.toolmenu_movie.addAction(
            QIcon(image_path_collection.film_16x16),
            self.tr("Add movie"))

        self.action_serie_add = self.toolmenu_movie.addAction(
            QIcon(self.image_path_collection.film_16x16),
            self.tr("Add series"))

        self.toolmenu_movie.addSeparator()

        self.action_add_movie_edition = self.toolmenu_movie.addMenu(
            QIcon(),
            self.tr("Edition"))

        self.action_add_movie_edition.addAction(
            QIcon(),
            self.tr("Movie"))

        self.action_add_movie_edition.addAction(
            QIcon(),
            self.tr("Serie"))

        self.action_add_movie_collection = self.toolmenu_movie.addMenu(
            QIcon(),
            self.tr("Collection"))

        self.action_add_movie_collection.addAction(
            QIcon(),
            self.tr("Movie"))

        self.action_add_movie_collection.addAction(
            QIcon(),
            self.tr("Serie"))

        # Book
        self.toolmenu_book = QMenu()

        self.action_book_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.book_16x16),
            self.tr("Add book"))

        self.action_comic_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.comic_book_16x16),
            self.tr("Add comic-book"))

        self.action_magazin_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.magazine_16x16),
            self.tr("Add magazine"))

        self.action_news_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.newspaper_16x16),
            self.tr("Add newspaper"))

        self.action_miscellany_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.miscellany_16x16),
            self.tr("Add edited volume"))

        self.action_article_in_a_periodical_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.journal_article_16x16),
            self.tr("Add article in a periodical"))

        self.news_article_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.journal_article_16x16),
            self.tr("Add newspaper article"))

        self.e_book_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.ebook_16x16),
            self.tr("Add eBook"))

        self.audio_book_add = self.toolmenu_book.addAction(
            QIcon(self.image_path_collection.audio_book_16x16),
            self.tr("Add audio book"))

        # Music
        self.toolmenu_music = QMenu()

        self.action_single_audio_add = self.toolmenu_music.addAction(
            QIcon(self.image_path_collection.getPath_signleaudio_icon),
            self.tr("Add single song"))

        self.action_album_add = self.toolmenu_music.addAction(
            QIcon(self.image_path_collection.album_16x16),
            self.tr("Add album"))

        self.action_single_add = self.toolmenu_music.addAction(
            QIcon(self.image_path_collection.single_16x16),
            self.tr("Add single"))

        self.action_record_add = self.toolmenu_music.addAction(
            QIcon(self.image_path_collection.record_16x16),
            self.tr("Add record"))

        # Person
        self.toolmenu_person = QMenu()

        self.action_person_add = self.toolmenu_person.addAction(
            QIcon(self.image_path_collection.person_16x16),
            self.tr("Add person"))

        # Contact
        self.toolmenu_contact = QMenu()

        self.action_contact_add = self.toolmenu_contact.addAction(
            QIcon(self.image_path_collection.contact_16x16),
            self.tr("Add contact"))

        # Note
        self.toolmenu_note = QMenu()

        self.action_note_add = self.toolmenu_note.addAction(
            QIcon(self.image_path_collection.note_16x16),
            self.tr("Add note"))

        # Coin
        self.toolmenu_coin = QMenu()

        self.action_coin_add = self.toolmenu_coin.addAction(
            QIcon(self.image_path_collection.coin_16x16),
            self.tr("Add coin"))

        # Game
        self.toolmenu_game = QMenu()

        self.action_game_add = self.toolmenu_game.addAction(
            QIcon(self.image_path_collection.video_game_16x16),
            self.tr("Add video game"))

        # Stamp
        self.toolmenu_stamp = QMenu()

        self.action_stamp_add = self.toolmenu_stamp.addAction(
            QIcon(self.image_path_collection.stamp_16x16),
            self.tr("Add stamp"))

        # Quotation
        self.toolmenu_quotation = QMenu()

        self.action_quotation_add = self.toolmenu_quotation.addAction(
            QIcon(self.image_path_collection.quotation_16x16),
            self.tr("Add quotation"))

        # Wine
        self.toolmenu_wine = QMenu()

        self.action_wine_add = self.toolmenu_wine.addAction(
            QIcon(self.image_path_collection.wine_16x16),
            self.tr("Add wine"))

        # Rent
        self.toolmenu_rent = QMenu()

        self.action_rent_add = self.toolmenu_rent.addAction(
            QIcon(self.image_path_collection.toolbar_button_rent_icon),
            self.tr("Add rent"))

        return

    def init_action_in_toolbar(self,
                              kwargs):
        '''
           SUMMARY
           =======
           This method Inistializes all menu for toolbuttons.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize all actions for toolbuttons")

        self.search_action = QAction(self)
        self.search_action.setIconText(self.tr("Search"))
        self.search_action.setStatusTip(self.tr("Search"))
        self.search_action.setIcon(QIcon(kwargs.get('image_path_collection').search_32x32))
        self.search_action.setShortcut('Ctrl+Q')

        self.filter_action = QAction(self)
        self.filter_action.setIconText(self.tr("Filter"))
        self.filter_action.setStatusTip(self.tr("Filter"))
        self.filter_action.setIcon(QIcon(kwargs.get('image_path_collection').filter_32x32))
        self.filter_action.setShortcut('Ctrl+Q')

        self.update_action = QAction(self)
        self.update_action.setIconText(self.tr("Update"))
        self.update_action.setStatusTip(self.tr("Update"))
        self.update_action.setIcon(QIcon(kwargs.get('image_path_collection').update_32x32))
        self.update_action.setShortcut('Ctrl+Q')

        self.action_exit = QAction(self)
        self.action_exit.setIconText(self.tr("{} Exit".format(kwargs.get('info_app').dict_info.get('exe_name'))))
        self.action_exit.setStatusTip(self.tr("{} Exit".format(kwargs.get('info_app').dict_info.get('exe_name'))))
        self.action_exit.setIcon(QIcon(kwargs.get('image_path_collection').exit_32x32))
        self.action_exit.setShortcut('Ctrl+Q')

        return

    def set_popup_mode_and_set_menu_in_toolbuttons(self):
        '''
           SUMMARY
           =======
           This method Inistializes all popup mode for toolbuttons.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize all actions for toolbuttons")

        self.toolbutton_mowie.setMenu(self.toolmenu_movie)
        self.toolbutton_mowie.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_book.setMenu(self.toolmenu_book)
        self.toolbutton_book.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_music.setMenu(self.toolmenu_music)
        self.toolbutton_music.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_person.setMenu(self.toolmenu_person)
        self.toolbutton_person.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_contact.setMenu(self.toolmenu_contact)
        self.toolbutton_contact.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_coin.setMenu(self.toolmenu_coin)
        self.toolbutton_coin.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_note.setMenu(self.toolmenu_note)
        self.toolbutton_note.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_game.setMenu(self.toolmenu_game)
        self.toolbutton_game.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_quotation.setMenu(self.toolmenu_quotation)
        self.toolbutton_quotation.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_stamp.setMenu(self.toolmenu_stamp)
        self.toolbutton_stamp.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_wine.setMenu(self.toolmenu_wine)
        self.toolbutton_wine.setPopupMode(QToolButton.InstantPopup)

        self.toolbutton_rent.setMenu(self.toolmenu_rent)
        self.toolbutton_rent.setPopupMode(QToolButton.InstantPopup)

        return

    def store_window_state(self,
                           kwargs):

        '''
           SUMMARY
           =======
           This method is used to store the state of this mainwindow's in a file.
           toolbars and dock-widgets.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Restore all states")

        conf_base_path = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('PathToSettingFolder')

        base_path = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('BasePath')
        profile_name = kwargs.get('configuration_saver_dict').dict_set_general_settings.get('current_profile')

        file_path = path.join(base_path, 'profiles', unicode(profile_name), 'settings', 'window_state.conf')


            # The data() method of a QByteArray converts it to a
            # python bytes object - which means it's guaranteed to
            # work with all versions of python/pyqt.

        create_write_conf_file(file_path = file_path,
                                content = self.saveState().data(),
                                file_mode = 'wb')

        return

    def add_toolbuttons_to_toolbar(self,
                                  kwargs):
        '''
           SUMMARY
           =======
           This method is used to add  all QToolButtons to the QToolBar.
           toolbars and dock-widgets.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Add all QToolButtons to the  QToolBar")

        # We create a list temporary.
        counter_list = list()

        self.ui_mdi.toolBar_MDI.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_movie'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_mowie)  # Movie Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_movie'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_book'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_book)  # Book Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_book'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_music'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_music)  # Music Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_music'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_coin'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_coin)  # Coin Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_coin'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_person'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_person)  # Person Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_person'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_contact'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_contact)  # Contact Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_contact'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_note'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_note)  # Note Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_note'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_video_game'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_game)  # Game Button
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_video_game'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_quotation'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_quotation)  # Quotation
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_quotation'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_stamp'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_stamp)  # Stamp
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_stamp'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_wine'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_wine)  # Wine
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_wine'):
            counter_list.append(1)

        if kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_rent'):
            self.ui_mdi.toolBar_MDI.addWidget(self.toolbutton_rent)  # Rent
        elif not kwargs.get('safe_config_parser').getboolean('Category_Manager', 'visible_rent'):
            counter_list.append(1)

        categories_number = 12

        if len(counter_list) == int(categories_number):
            #   When all QToolButton are invisible, we do nothing here.
            #   We just pass it. That means, we don't add a Seperator() to
            #   the ToolBar.
            #   Because the Seperator() is used for separating the QToolButton
            #   of categories-manager and the other permanent QToolButtons,
            #   which is always visible. In this case we compare the currently number
            #   of categories, that are saved in the variable named (categories_number)
            #   which the lengthof the list named (counter_list).
            pass
        else:
            #   At least one QToolBar is visible and we add the Separator()
            #   to the ToolBar.
            self.ui_mdi.toolBar_MDI.addSeparator()  # Seperator-Line

        #   Now we dont need the list anymore,
        #   that is why we empty the list.
        del counter_list[:]

        #   That are permanent QToolButtons, which is always visible. That why
        #   we add them to the ToolBar.
        self.ui_mdi.toolBar_MDI.addAction(self.search_action)  # Search Button
        self.ui_mdi.toolBar_MDI.addAction(self.filter_action)  # Filter Button
        self.ui_mdi.toolBar_MDI.addSeparator()  # Seperator-Line
        self.ui_mdi.toolBar_MDI.addAction(self.update_action)  # Update Button
        self.ui_mdi.toolBar_MDI.addSeparator()  # Seperator-Line
        self.ui_mdi.toolBar_MDI.addAction(self.action_exit)  # Exit Button

        return

    def look_window(self):
        print "Look!"


    def set_triggered_action_menuebar_in_toolbar(self,
                                                 **kwargs):
        '''
           SUMMARY
           =======
           In this method we set the signal of triggered.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the triggered slot.")

        self.action_movie_add.triggered.connect(
            lambda: self.create_combine_movie_general_details_buttons(
                scoped_session = kwargs.get('defined_session_scope'),
                work_mode = 'add_record',
                show_messagebox = True,
                standard_item_model = self._standard_item_model,
                general_proxy_model = self._general_proxy_model,
                form_window_title = self.tr("Add movie")))

        self.action_serie_add.triggered.connect(
          lambda:
          self.create_combine_serie_general_details_buttons(0, self.tr("Add series")))

        self.action_comic_add.triggered.connect(
          lambda:
          self.create_combine_comic_general_details_buttons(form_window_title=self.tr("Add comic-book")))

        self.action_album_add.triggered.connect(
          lambda:
          self.create_music_album(0, self.tr("Add album")))

        self.action_coin_add.triggered.connect(
          lambda:
          self.create_combine_coin_general_details_buttons(
            form_condition = 0,
            form_window_title = self.tr("Add coin")))

        self.search_action.triggered.connect(
          lambda:
          self.store_window_state(kwargs = kwargs))

        self.filter_action.triggered.connect(self.create_filter_form)

        self.actionShow_sub_window_list.triggered.connect(self.show_sub_window_list)

        self.actionCustomSetting.triggered.connect(self.look_window)

        self.action_person_add.triggered.connect(
          lambda:
          self.create_person_profile(person_id = None))

        self.action_contact_add.triggered.connect(self.create_person_selection)
        self.action_collection.triggered.connect(self.create_collection_form)

        self.action_note_add.triggered.connect(self.create_combine_note_buttons)

        self.action_note_add.triggered.connect(self.popen_it)

        # WARNING: Don't delete this comment!!!!
        #self.actionBook.triggered.connect()
        #self.actionComic.triggered.connect()
        #self.actionMagazin.triggered.connect()

        self.update_action.triggered.connect(lambda: self.check_arequired_access_data(kwargs = self._kwargs))
        self.action_exit.triggered.connect(self.close)

        return

    def init_statusbar(self):
        self.label_left_statusbar = QLabel()
        self.label_left_statusbar.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        self.label_middle_statusbar = QLabel()
        self.label_middle_statusbar.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        self.label_right_statusbar = QLabel()
        self.label_right_statusbar.setFrameStyle(QFrame.Panel | QFrame.Sunken)

        self.ui_mdi.statusBar.addPermanentWidget(self.label_left_statusbar, 1)
        self.ui_mdi.statusBar.addPermanentWidget(self.label_middle_statusbar, 1)
        self.ui_mdi.statusBar.addPermanentWidget(self.label_right_statusbar, 1)

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event (class):  By default the given event is accepted and the widget is closed. We
                            get this event when Qt receives a window close request for a top-level
                            widget from the window system. That means, first it sends the widget a QCloseEvent.
                            The widget is closed if it accepts the close event. The default implementation of
                            QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        #   Save all the current state of this mainwindow's toolbars and dockwidgets.
        self.store_window_state(kwargs = self._kwargs)

        #   When the user finishs the program, all settings are saved.
        self.save_setting_ini(self.configuration_saver_dict)

        #   In general, before the program will be closed, its has to check the configuration. For example: the
        #   configuration named (AskBeforClose) sets on False its hasn't shows an messagebox. But if this
        #   configuration is on True its has to show the messagebox and ask the user if he really wants
        #   to exit the programm
        if self.unload_me == True:
            exit()

        if self.unload_me == None:
            if self.configuration_saver_dict.dict_set_general_settings["AskBeforClose"] == "False":

                #   Don't ask the user. Only call the function namend (create_missing_folders) in
                #   module named (manage_folder) and give the function the argument of base path
                #   that is saved in the module named (config)
                self.create_missing_folders(self.configuration_saver_dict.dict_set_general_settings["BasePath"])

                #   Call the function namend (create_write_ini_file) of the module namend (manage_ini_file) and
                #   write all settings in the config-file again. It doesn't matter if the config-file already exists.
                #   The reason of this procedure is, if the configuration file during the duration of the program is
                #   deleted. That means, this procdure create a new setting folder and/or a config-file -
                #   if necessary.
                #   NOTICE: The argument 'No' means that the config-file shouldn't be read. Just write all
                #   saved settings in the config-file.
                create_write_ini_file(self.configuration_saver_dict, self.default_ini_file)
                exit()
            else:
                #   Ask the user, if he really wants to exit the program.
                setIc_OK = QIcon(self.image_path_collection.check_icon)
                setIc_No = QIcon(self.image_path_collection.no_icon_unload_mdi_msgbox)
                setIc_Cancel = QIcon(self.image_path_collection.cancel_icon_unload_mdi_msgbox)

                mBox = custom_message_box(self,
                                          text = self.tr("Are you sure you want to quit that program? \n"),
                                          windows_title = self.tr('Question'),
                                          title_icon = self.image_path_collection.question_32x32_default)

                mBox.addButton(
                    QPushButton(setIc_OK, self.tr("Yes")),
                    mBox.YesRole)  # An "OK" button defined with the AcceptRole
                mBox.addButton(
                    QPushButton(setIc_No, self.tr("No")),
                    mBox.NoRole)  # An "OK" button defined with the AcceptRole
                mBox.addButton(
                    QPushButton(setIc_Cancel, self.tr("Cancel")),
                    mBox.RejectRole)  # An "OK" button defined with the AcceptRole
                #mBox.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.
                ret = mBox.exec_()
                print "my ret", ret
            if ret == 0:
                #manage_folder.mkdir_top_sub_folder(self.get_set_setting.dict_set_general_settings["BasePath"])
                #manage_ini_file.create_write_ini_file(self.get_set_setting, self.default_ini_file)

                #if self._engine:
                if self.defined_session_scope:
                    #self._engine = None
                    self.defined_session_scope = None

                event.accept()
                self.mdiArea.closeAllSubWindows()
                exit()
            elif ret == 1:
                event.ignore()
            elif ret == 2:
                event.ignore()

        return
