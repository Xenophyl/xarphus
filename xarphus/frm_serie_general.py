#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path

import sys
from inspect import currentframe

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QTreeWidget, QFileDialog, QPixmap

try:
    from xarphus.core.manage_calculation import calculate_cover_poster
    from xarphus.core.manage_folder import is_folder_exists
    from xarphus.core.manage_path import extract_directory
except ImportError:
    pass

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class SerieGeneral_Window(QWidget):
    def __init__(self,
                configuration_saver_dict,
                parent=None):

        QWidget.__init__(self, parent)


        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/serie_general.ui")

        self.configuration_saver_dict = configuration_saver_dict

        self.is_focused = None
        self.film_poster_path_old = None

        self.load_ui_file(UI_PATH)
        self.init_clicked_signal_push_buttons()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__


    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_serie_general = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        self.set_visible_treeWidget_header(instance_name_tabWidget_object=self.ui_serie_general.tabWidget_general_information,
                                  widghet_n=0,
                                  bool_value=True)

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object(s)")

        self.ui_serie_general.pushButton_add_serie_poster.clicked.connect(self.open_file_dialog)

        return

    def set_visible_treeWidget_header(self, instance_name_tabWidget_object="",
                                           widghet_n = None,
                                           bool_value=None):
        '''
            NOTICE:
            =======
            In this method we want to get all QTreeWidget that are children of parentWidget.
            

            PARAMETERS:
            ===========
            :instance_name_tree_Widget_object   -   The given argumnent contains instance of a QTabWidget.
                                                    In this case, QTabWidget is a parentWidget.

            :widghet_n                          -   Here, we ecpect an integer. The integer is an index. 
                                                    We need the index, because the QTabWidget has a method to 
                                                    access any tab by its index, sensibly called widget(index).
                                                    Therefore, if you want to access the n-th widget,
                                                    you can get it by calling self.tabWidget.widget(n).

            :bool_value                         -   In this keyword argument we want a special value: boolean value.


            :return                             -   Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''

        logger.info("For loop starts")

        for tree_widget_object in instance_name_tabWidget_object.widget(int(widghet_n)).findChildren(QTreeWidget):
            tree_widget_object.setHeaderHidden(bool_value)

        return

    def set_focus(self):
        '''
            NOTICE:
            =======
            This method is used to set the focus. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the focus")

        self.ui_serie_general.lineEdit_title_translated.setFocus()

        return

    def open_file_dialog(self):
        '''
            NOTICE:
            =======
            In this method we select a file with a QFileDialog - its saves
            in the variables named (self.file_path_front_cover or self.file_path_back_cover).
            Its enables the user to navigate through the file system and select a file to open 

            PARAMETERS:
            ===========
            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        logger.info("Open the file dialog")
        

        '''
            We save the caption of the title in the variable named (qfile_dialog_title),
            because we need it often.
        '''
        file_dialog_title = self.tr("Select")

        home_path = path.expanduser(self.configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"])

        file_extensions_filter = "*.png *.jpg *.bmp"


        '''

            In this condition, we want a variable (in this case a string) named (self.file_path_front_cover). 
            Then we ask Qt to open a QFileDialog by using the function named (getOpenFileName), that returns us the 
            name of the file that is selected. 

            well, we have to pass a couple arguments to it:
            First, we give it the parent widget (in this case, our main window - self), 
            then we give it a title, that is saved in the variable above named (qfile_dialog_title) 
            for the dialog, then we give it a place to open up to (in this case, the directory is saved in in a dictionary, 
            if the path is not saved in the dictionary its opens the user's home directory).
            non-native dialog
        '''

        
        '''
            Before we pop up a file dialog we have to check if the folder is already exists.
            In the variable named (home_path) the path to the folder is saved.
        '''
        if is_folder_exists(home_path):
            '''
                Yes the folder already exists. We don't have to do anything.
            '''
            pass
        else:
            '''
                There is a problem. The folder doesn't exists. We have to overwirte 
                the variable named (home_path). In this case we save a simple
                path seperator. So the file dialoge will pop up witha home path.
            '''
            home_path = path.sep

        self.film_poster_path = unicode(QFileDialog.getOpenFileName(self, 
                                                                             file_dialog_title,
                                                                             home_path,
                                                                             "Images ({file_extensions})".format(file_extensions=file_extensions_filter)))

        '''
            we check if the user selected a file. If the user did it,
            the path is returned, otherwise if not
            (ie, the user cancels the operation) None is returned.
        '''
        if self.film_poster_path:
            '''
                Yes the path is returned. We set enabled on True. 
                User can click on the QPushButtons
            '''

            self.film_poster_path_old = self.film_poster_path
            self.ui_serie_general.pushButton_zoom_serie_poster.setEnabled(True)
            self.ui_serie_general.pushButton_del_serie_poster.setEnabled(True)

            self.configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"] = extract_directory(self.film_poster_path)
            home_path = self.configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"]

            '''
                We invoke a methode and pass them two arguments.
            '''
            self.show_image_on_label(self.film_poster_path)

        else:
            '''
                No. The user cancels the operation. All QPushButtons are disabled.
            '''
            if self.film_poster_path_old:
                self.ui_serie_general.pushButton_zoom_serie_poster.setEnabled(True)
                self.ui_serie_general.pushButton_del_serie_poster.setEnabled(True)
            else:
                self.ui_serie_general.pushButton_zoom_serie_poster.setEnabled(False)
                self.ui_serie_general.pushButton_del_serie_poster.setEnabled(False)

        return

    def show_image_on_label(self, 
                            img_path):

        print "img_path", img_path
        '''
            NOTICE:
            =======
            In this method we load and stack_set_current_index the image on QLabel()-object. 

            PARAMETERS:
            ===========
            :img_path       -       The given argument contains a path to the
                                    image file that we want to load and stack_set_current_index.

            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        pixmap = QPixmap(img_path)

        self.ui_serie_general.label_poster.clear()
        self.ui_serie_general.label_poster.setPixmap(pixmap)

        return

    def resizeEvent(self,
                    resizeEvent):
        '''
            NOTICE:
            =======
            This method is reimplemented and modified to receive widget resize events which are passed in the event parameter. 
            When the widget is resized it already has its new geometry.

            Here its looks if the variable named (self.is_focused) is NOT True. By default this variable has the value None.
            If there is None do nothing.

            Next step its calls the function named (calculate_cover_poster), that returns a result, and
            sets the width (w) and the height (h) of the widget everytime: setFixedSize(width, height) 

            PARAMETERS:
            ===========
            :event      -       This method gets a resize events.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the fixed size of the widget")

        if not self.is_focused:
            self.set_focus()

        result_groupBox_poster = calculate_cover_poster(int(self.ui_serie_general.width()), 6)

        self.ui_serie_general.groupBox_poster.setFixedSize(result_groupBox_poster, self.ui_serie_general.tabWidget_general_information.height())

        return

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    #   Import core codes
    from collection_paths_image import ImagePathnCollection

    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists
    from core.manage_path import extract_directory
   
    #   Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    print  """NOTICE: \n======= \nYou can also leave the inputs blank - by pressing just enter.
              """

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    image_pathn_collection = ImagePathnCollection()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()


    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)



    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    LOG_FOLDER_PATH = path.join(BASE_PATH, LOG_FILE_PATH)

    print ""
    print "The log file is saved in", LOG_FOLDER_PATH

    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))
    
    app = QApplication(sys.argv)

    translator = QTranslator()
    translator_path = ":/translators/german_sie.qm"
    translator.load(translator_path)
    app.installTranslator(translator)

    window = SerieGeneral_Window(dict_custom_config)


    #window.resize(600, 400)
    window.showMaximized()
    sys.exit(app.exec_())