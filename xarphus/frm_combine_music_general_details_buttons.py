#-*- coding:latin1 -*-

FILE_NAME = __name__

import os
import inspect

from PyQt4.QtCore import Qt, QRegExp

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QIcon, QFileDialog, \
    QPixmap, QValidator, QRegExpValidator, QTabWidget, QGridLayout, QTextEdit

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window

from xarphus.frm_music_album_general_details import MusicAlbumGeneralDetails_Window
from xarphus.frm_music_general import MusicAlbumGeneral_Window

from xarphus.core.manage_calculation import calculate_cover_poster



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineMusikAlbumGeneralDetailsButtons_Window(QWidget):
    def __init__(self,
                 view_mode,
                 update_sublist,
                 cover_viewer,
                 movie_detail,
                 form_title,
                 save_config,
                 image_path_collection,
                 app_info,
                 parent):
        '''

        :param view_mode:               You get a number as a integer. The numbers
                                        are 0 and 1. The number 0 says, when a user
                                        decides to add a new comic data. Otherwise, 1
                                        says,the user wants to take a look a existing comic data
                                        and maybe he wants to edit the comic data.

        :param update_sublist:          In the module named (mdi.py) there is a function
                                        named (show_sub_qwidgets_children).
                                        This function is only temporarily for a test.
                                        Later, the function is deleted. This function shows
                                        me, how much sub-windows are open in MDI-Area currently.

        :param cover_viewer:            You get an access to the function un the moduke
                                        named (mdi.py). This function starts the cover-window.

        :param movie_detail:            Text

        :param form_title:              This new created window get a title.

        :param save_config:             You get an access to the dictionary that is saved in
                                        module named (config.py). There, all settings are saved.

        :param image_path_collection:   In the module named (collection_paths_image.py). There
                                        are all paths of images or icons. Notice: All the paths
                                        of icons or images are used repeatedly. That's the reason
                                        why the paths were outsourced.

        :param app_info:                You get an access to a dictionary that is saved in
                                        module named (info.py). There, all information about
                                        this program are saved.

        :param parent:                  --

        :return:                        None
        '''

        QWidget.__init__(self, parent)

        self.up_to_date_sublist = update_sublist
        self.create_cover_viewer = cover_viewer
        self.view_mode = view_mode
        self.movie_detail = movie_detail
        self.form_title = form_title

        self.set_get_settings = save_config
        self.image_path_collection = image_path_collection
        self.info_app = app_info

        logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")
        logger.info("--"*10)
        logger.info("The instance of the class of MovieGeneral_Window() is creating")

        self.music_album_general = MusicAlbumGeneral_Window(self.up_to_date_sublist, self.up_to_date_sublist, self.info_app, self)


        logger.info("--"*10)
        logger.info("The instance of the class of MovieGeneralDetails_Window() is creating")

        self.music_album_general_details = MusicAlbumGeneralDetails_Window(self.up_to_date_sublist, self.up_to_date_sublist, self)

        logger.info("--"*10)
        logger.info("The instance of the class of PushbuttonsClosePrintDeleteSave_Window() is creating")
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.up_to_date_sublist, self)

        '''
        This is a new window that combines two existing windows.
        That why we need a vertical layout.
        '''
        vertical_layout = QVBoxLayout()
        vertical_layout.addWidget(self.music_album_general)
        vertical_layout.addWidget(self.music_album_general_details)
        vertical_layout.addWidget(self.pushbutton_close_print_delete_save)


        textEdit_view = QTextEdit(self)
        textEdit_view.setReadOnly(True)

        gridLayout_view = QGridLayout(self)
        gridLayout_view.addWidget(textEdit_view, 0, 0)
        #self.setLayout(gridLayout_view)

        tab_widget = QTabWidget(self)
        tab1 = QWidget(self)
        tab2 = QWidget(self)

        tab1.setLayout(vertical_layout)
        tab2.setLayout(gridLayout_view)

        tab_widget.addTab(tab1, self.tr("Bearbeitung"))
        tab_widget.addTab(tab2, self.tr("Gesamt�bersicht"))

        gridLayout_1 = QGridLayout(self)
        gridLayout_1.addWidget(tab_widget, 0, 0)


        self.film_title_name = ""


        # activate all necessary functions
##        self.init_ui_comic()
##        self.chosen_function()
##        self.init_pushbuttons()
##        self.set_hide_header()
##        self.set_focus()


    def get_class_name(self):
        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + __name__ + ")")
        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
        logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        return self.__class__.__name__

##    def manage_push_buttons(self, bool_value_pushButton_delete=None,
##                                    bool_value_pushButton_print=None):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.pushbutton_close_print_delete_save.pushButton_delete.setEnabled(bool_value_pushButton_delete)
##        self.pushbutton_close_print_delete_save.pushButton_print.setEnabled(bool_value_pushButton_print)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def init_ui_add_comic(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.setWindowTitle(self.form_title)
##
##        self.manage_push_buttons(bool_value_pushButton_delete=False,
##                                 bool_value_pushButton_print=False)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def show_comic(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.setWindowTitle(self.tr("Film..."))
##        self.manage_push_buttons(bool_value_pushButton_delete=True,
##                                 bool_value_pushButton_print=True)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def set_focus(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general.lineEdit_title_translated.setFocus()
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def init_ui_comic(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.setWindowIcon(QIcon(self.image_path_collection.movie_icon))
##
##        #self.comic_general.on_start()
##
##        self.pushbutton_close_print_delete_save.pushButton_save.setEnabled(False)
##        self.comic_general.lineEdit_title_original.textChanged.connect(lambda: self.lineEdit_title_translated_change(
##                                                                                                self.comic_general.lineEdit_title_original.text(),
##                                                                                                self.comic_general.lineEdit_title_original, 1))
##
##        self.comic_general.lineEdit_title_translated.textChanged.connect(lambda: self.lineEdit_title_translated_change(
##                                                                                                self.comic_general.lineEdit_title_translated.text(),
##                                                                                                self.comic_general.lineEdit_title_translated, 0))
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def show_comic_detail(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        '''
##        Let us take a look, what the user want. This variable (view_mode) is obtained
##        by the CombineMovieGeneralDetailsButtons_Window()-class. In this variable you only find
##        0 or 1. Zero means, the user want to add a new movie data and 1 means,
##        the user wants to look at the existing film data.
##        '''
##        if self.view_mode == 0:
##
##            form_title = self.tr("Eine Fassung vom Film ") + " (" + unicode(self.film_title_name) + ") " + self.tr(("hinzuf�gen"))
##
##            '''
##            NOTICE:
##            -------
##            "movie_detail" is an obtained argument in this CombineMovieGeneralDetailsButtons_Window()-class.
##            This argument contains a function object that takes three arguments.
##
##            SEQUENCE:
##            ---------
##            01 argument:    The variable (choice_function) is passed as an argument.
##                            This variable is obtained by the CombineMovieGeneralDetailsButtons_Window()-class.
##                            In the variable you only find 0 or 1. Zero means, the user want to add a new
##                            movie data and 1 means, the user wants to look at the existing film data.
##
##            02 argument:    The user wants to look at the existing film data, so the window gets another
##                            title. The title is saved in the above variable (form_title).
##
##            03 argument:    The integer says about the type of details. Here, the zero says that the user
##                            wants add a new film version. If there were an integer 1 that would means,
##                            the user wants add a new film edition.
##
##            Once the function has been called the form (movie_detail) is generated by frm_mdi.
##            '''
##            self.movie_detail(self.view_mode, form_title, 0)
##
##        if self.view_mode == 1:
##            '''
##            The user wants to look at the existing film data. So the winsow gets another title.
##            '''
##            form_title = self.tr("Die Fassung vom Film ") + " (" + unicode(self.film_title_name) + ") " + self.tr(("anzeigen"))
##
##            self.movie_detail(self.view_mode, form_title, 1)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def init_pushbuttons(self):
##        pass
##
##    def update_tltie_form(self, title_name):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.setWindowTitle(self.form_title + " [ " + title_name + " ] ")
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def setting_pushbutton_save(self, status):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.pushbutton_close_print_delete_save.pushButton_save.setEnabled(status)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def setting_pushbutton_add_translated_title(self, status):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general.pushButton_add_alternate_translated_title.setEnabled(status)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def setting_pushbutton_add_original_title(self, status):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general.pushButton_add_original_title.setEnabled(status)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def setting_pushbutton_add_film_version(self, status):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general_details.pushButton_add_film_version.setEnabled(status)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def setting_pushbutton_add_film_edition(self, status):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general_details.pushButton_add_film_edition.setEnabled(status)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def set_hide_header(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        list_tree_header = [self.comic_general.treeWidget_identify_numbers,
##                       self.comic_general.treeWidget_genre,
##                       self.comic_general.treeWidget_product_country]
##
##        for tree_widget_header in list_tree_header:
##            tree_widget_header.setHeaderHidden(True)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def check_is_line_edit_translated_original_title_empty(self, line_edit_obj, obj_num):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        '''
##        :param line_edit_obj: Here you get just the pure object of line_edit.
##
##        :param obj_num: Here you get a number that indicates which object is to be monitored.
##        '''
##        if not line_edit_obj.text().isEmpty():
##            '''
##            First, its checks if the textfield is not empty.
##            '''
##            if unicode(line_edit_obj.text()).isspace():
##                '''
##                In this step, the line_edit_obj is NOT empty. Second, its checks if
##                there are only whitespaces in the textfield. In the if-block,
##                there will check if there a whitespaces.
##
##                Notice: Call the function named (collect_objects)
##                and give the argument (setting_button) for setting the button
##                and give the second argument as bool (False), that means the button
##                ist NOT enabled.
##                '''
##                if obj_num == 0:
##                    self.collect_objects(key_name='setting_save_button',
##                                         bool_value=False)
##                    self.collect_objects(key_name='setting_add_alternate_translated_title_button',
##                                         bool_value=False)
##                    self.collect_objects(key_name='setting_pushbutton_add_film_version',
##                                         bool_value=False)
##                    self.collect_objects(key_name='setting_pushbutton_add_film_edition',
##                                         bool_value=False)
##                if obj_num == 1:
##                    self.collect_objects(key_name='setting_pushbutton_add_alternate_original_title',
##                                         bool_value=False)
##                '''
##                Moves the cursor back steps characters.
##                If mark is true each character moved over
##                is added to the selection; if mark is false the
##                selection is cleared. In this case, if the line_edit_obj
##                is empty or there are only whitespaces then don't move
##                the cursor forward with the setCursorPosition()-method.
##                And keep with the clear()-method the line_edit_obj always
##                empty till the user enter some characters.
##                '''
##                #line_edit_obj.cursorBackward(False)
##                line_edit_obj.setCursorPosition(0)
##                line_edit_obj.clear()
##
##            else:
##                if obj_num == 0:
##                    self.collect_objects(key_name='update_form_title',
##                                         changed_content=unicode(line_edit_obj.text()),
##                                         bool_value=True)
##                    self.collect_objects(key_name='setting_save_button',
##                                         changed_content=unicode(line_edit_obj.text()),
##                                         bool_value=True)
##                    self.collect_objects(key_name='setting_add_alternate_translated_title_button',
##                                         bool_value=True)
##                    self.collect_objects(key_name='setting_pushbutton_add_film_version',
##                                         bool_value=True)
##                    self.collect_objects(key_name='setting_pushbutton_add_film_edition',
##                                         bool_value=True)
##
##                    self.film_title_name = unicode(line_edit_obj.text())
##                if obj_num == 1:
##                    self.collect_objects(key_name='setting_pushbutton_add_alternate_original_title',
##                                         bool_value=True)
##        else:
##            '''
##            Textfield is really empty
##            '''
##            if obj_num == 0:
##                self.collect_objects(key_name='update_form_title',
##                                     changed_content=unicode(line_edit_obj.text()),
##                                     bool_value=False)
##                self.collect_objects(key_name='setting_save_button',
##                                     changed_content=unicode(line_edit_obj.text()),
##                                     bool_value=False)
##                self.collect_objects(key_name='setting_add_alternate_translated_title_button',
##                                     bool_value=False)
##                self.collect_objects(key_name='setting_pushbutton_add_film_version',
##                                     bool_value=False)
##                self.collect_objects(key_name='setting_pushbutton_add_film_edition',
##                                     bool_value=False)
##            if obj_num:
##                self.collect_objects(key_name='setting_pushbutton_add_alternate_original_title',
##                                     bool_value=False)
##
##            line_edit_obj.clear()
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def lineEdit_title_original_change(self, content_line_edit, line_edit_obj):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        #self.check_is_line_edit_original_title_empty(unicode(content_line_edit), line_edit_obj)
##        self.check_is_line_edit_translated_original_title_empty(line_edit_obj, None)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def lineEdit_title_translated_change(self, content_line_edit, line_edit_obj, obj_num):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.check_is_line_edit_translated_original_title_empty(line_edit_obj, obj_num)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def create_function_list(self, choice_number):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        list_functions = [self.init_ui_add_comic,
##                          self.show_comic
##                            ]
##
##        list_functions[choice_number]()
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def lineEdit_validator(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.comic_general.lineEdit_title_translated.setValidator(
##            QRegExpValidator(QRegExp('^[A-Za-z0-9-_,;:.()/!���]*$[\\\]'),
##                             self.comic_general.lineEdit_title_translated))
##        self.comic_general.lineEdit_original_title.setValidator(
##            QRegExpValidator(QRegExp("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"),
##                             self.comic_general.lineEdit_original_title))
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def collect_objects(self, key_name=None, changed_content=None, bool_value=None):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        parsing_dict = {
##        "update_form_title": self.update_tltie_form,
##        "setting_save_button": self.setting_pushbutton_save,
##        "setting_add_alternate_translated_title_button": self.setting_pushbutton_add_translated_title,
##        "setting_pushbutton_add_alternate_original_title": self.setting_pushbutton_add_original_title,
##        "setting_pushbutton_add_film_version": self.setting_pushbutton_add_film_version,
##        "setting_pushbutton_add_film_edition": self.setting_pushbutton_add_film_edition,
##        }
##        try:
##            parsing_dict[key_name](bool_value)
##        except:
##            parsing_dict[key_name](changed_content)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def function_chosen(self, current_index, status_set):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        function_list = [self.comboBox_criteria_zero,
##                      self.comboBox_criteria_movie,
##                      self.comboBox_criteria_book,
##                      self.comboBox_criteria_music,
##                      self.comboBox_criteria_coin,
##                      self.comboBox_criteria_contact,
##                      self.comboBox_criteria_note,
##                      self.comboBox_criteria_video_game,
##                      self.comboBox_criteria_quotation,
##                      self.comboBox_criteria_stamp,
##                      self.comboBox_criteria_lending]
##
##        #print "Index", current_combo_index
##        function_list[current_index](status_set)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def check_inputs(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        # -------------------------------------------------------------------------------
##        sender = self.sender()
##        validator = sender.validator()
##        state = validator.validate(sender.text(), 0)[0]
##        # -------------------------------------------------------------------------------
##        if state == QValidator.Acceptable:
##            self.ui_register.pushButton_regist.setEnabled(
##                self.lineEdit_nickname.hasAcceptableInput()
##                and self.lineEdit_password.hasAcceptableInput()
##                and self.lineEdit_password_confirm.hasAcceptableInput()
##                and self.lineEdit_mail_address.hasAcceptableInput()
##                and self.lineEdit_mail_address_confirm.hasAcceptableInput())
##            #-------------------------------------------------------------------------------
##            color = '#c4df9b'  # green
##        #-------------------------------------------------------------------------------
##        elif state == QValidator.Intermediate:
##
##            color = '#fff79a'  # yellow
##            self.ui_register.pushButton_regist.setEnabled(False)
##
##        else:
##            color = '#f6989d'  # red
##        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def chosen_function(self):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        self.create_function_list(self.view_mode)
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def do_something(self):
##        pass
##    #--------------------------------------------------------------------------------------
##    def open_file_dialog(self, cover_site=None):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        qfile_dialog_title = self.tr("Cover ausw�hlen")
##
##        if cover_site=="front":
##            self.file_path_front_cover = unicode(QFileDialog.getOpenFileName(self, qfile_dialog_title,
##                                                                                 os.path.expanduser(self.set_get_settings.dict_set_settings["LastPathFolderDialog"]),
##                                                                                  "*.png *.jpg *.bmp"))
##
##            if unicode(self.file_path_front_cover) == '':
##                self.comic_general.pushButton_zoom_front.setEnabled(False)
##                self.comic_general.pushButton_del_front_cover.setEnabled(False)
##            else:
##                self.comic_general.pushButton_zoom_front.setEnabled(True)
##                self.comic_general.pushButton_del_front_cover.setEnabled(True)
##                #self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_front_cover)
##                self.show_cover(self.file_path_front_cover, 'front')
##
##        if cover_site=="back":
##            self.file_path_back_cover = unicode(QFileDialog.getOpenFileName(self, qfile_dialog_title,
##                                                                                 os.path.expanduser(self.set_get_settings.dict_set_settings["LastPathFolderDialog"]),
##                                                                                  "*.png *.jpg *.bmp"))
##
##            if unicode(self.file_path_back_cover) == '':
##                self.comic_general.pushButton_zoom_back.setEnabled(False)
##                self.comic_general.pushButton_del_back_cover.setEnabled(False)
##            else:
##                self.comic_general.pushButton_zoom_back.setEnabled(True)
##                self.comic_general.pushButton_del_back_cover.setEnabled(True)
##                #self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_front_cover)
##                self.show_cover(self.file_path_back_cover, 'back')
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def show_cover(self, img_path, cover_site=None):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        if cover_site=="front":
##            self.comic_general.label_cover_front.setPixmap(QPixmap(""))
##            self.comic_general.label_cover_front.setPixmap(QPixmap(img_path))
##        if cover_site=="back":
##            self.comic_general.label_cover_back.setPixmap(QPixmap(""))
##            self.comic_general.label_cover_back.setPixmap(QPixmap(img_path))
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return
##
##    def del_cover(self, cover_site=None):
##        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
##            3] + ") - (" + __name__ + ")")
##        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        if cover_site=="front":
##            self.comic_general.label_cover_front.setPixmap(QPixmap(""))
##            self.file_path_front_cover = ""
##            self.comic_general.pushButton_zoom_front.setEnabled(False)
##            self.comic_general.pushButton_del_front_cover.setEnabled(False)
##            self.comic_general.label_cover_front.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))
##        if cover_site=="back":
##            self.comic_general.label_cover_back.setPixmap(QPixmap(""))
##            self.file_path_front_cover = ""
##            self.comic_general.pushButton_zoom_back.setEnabled(False)
##            self.comic_general.pushButton_del_back_cover.setEnabled(False)
##            self.comic_general.label_cover_back.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))
##
##        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
##        logger.info(
##            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")
##
##        return

    def resizeEvent(self,resizeEvent):
        result_tabWidget_cover = calculate_cover_poster(int(self.music_album_general.width()), 6)

        # resize(width, height)
        self.music_album_general.tabWidget_cover.setFixedSize(result_tabWidget_cover, self.music_album_general.tabWidget.height())


    def closeEvent(self, event):
        pass

    def close_form(self):
        logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + __name__ + ")")
        logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()

        logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
        logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        return

