#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_add_person.py"

'''
The modules of required libraries are imported.
'''
import os
import sys

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QMdiSubWindow
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile
'''
Private modules are imported.
'''
from xarphus.core.qt_info import get_versio_pyqt, get_version_qt


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
class StoreOnMediumn_Window(QWidget):

    def __init__(self, func_up_to_date,
                 update_sublist,
                 add_person,
                 select_person,
                 save_config,
                 create_movie_detail,
                 show_type,
                 form_title_movie,
                 parent):

        QWidget.__init__(self, parent)

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.add_person = add_person
        self.select_person = select_person
        self.create_movie_detail = create_movie_detail
        self.show_type = show_type
        self.form_title_movie = form_title_movie

        self.set_get_settings = save_config

        UI_PATH = QFile(":/ui_file/store_movie_version_edition_on_medium.ui")

        UI_PATH.open(QFile.ReadOnly)
        self.ui_store_on_medium = loadUi(UI_PATH, self)
        UI_PATH.close()


#        self.color_black = "<font color='#000000'>"
#        self.ui_select_person.groupBox.setStyleSheet('border: none solid gray;')
#        self.ui_search.textEditExplain.setStyleSheet(('background-color: #ffffff;')) # white


        #self.ui_search.setWindowModality(Qt.ApplicationModal)
        self.set_gui()
        self.action_pushButtons()

#--------------------------------------------------------------------------------------
    def set_gui(self):
        pass
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint)
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowMaximizeButtonHint)
        #self.ui_select_person.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowTitleHint | Qt.WindowMinMaxButtonsHint)

        #self.ui_select_person.setWindowFlags(self.ui_select_person.windowFlags() | Qt.CustomizeWindowHint)
        #self.ui_select_person.setWindowFlags(self.ui_select_person.windowFlags() & ~Qt.WindowMaximizeButtonHint)

    def action_pushButtons(self):
        pass
        #self.ui_store_on_medium.pushButton_add_medium.clicked.connect(lambda: self.create_movie_detail(self.form_title_movie, 0, 0))
        #self.ui_store_on_medium.pushButton_close.clicked.connect(self.close_parent)

    def close_parent(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
