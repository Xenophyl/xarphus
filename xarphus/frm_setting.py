#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    frm_setting
    ===========
    This module handels the view-class(). Here the setting-window is loaded.

"""

FILE_NAME = __name__


import os
import sys
import inspect
import traceback
from os.path import expanduser

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QFile, QSize, QTranslator
from PyQt4.QtGui import QWidget, QButtonGroup, QFileDialog, QStyleFactory, QMessageBox, \
    QPushButton, QIcon, QPixmap, QMdiSubWindow, QListWidgetItem, QApplication, QCheckBox, QGroupBox, \
    QToolTip, QCursor

from PyQt4.uic import loadUi

'''
    IMPORTANT:
    ===========
    This file was created once as a module and once as a main program. 
    First of all, In the Try-block the file is treated as a module. But
    if you are about to use this file as a main program this is done
    in the Except-block.
''' 
try: 

    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.manage_prase import parsing_str_to_bool

    from xarphus.core.manage_scheme_color import Scheme_Color_Manager
    from xarphus.core import restart
    from xarphus.core import manage_prase
    from xarphus.core.manage_ini_file import get_configuration, create_write_ini_file

    from xarphus.core.manage_calculation import calculate_cover_poster
    from xarphus.core.manage_generator import list_object_generator

    from xarphus.core.manage_path import is_specifically_suffix

except ImportError:

    from core.manage_path import is_specifically_suffix
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_prase import parsing_str_to_bool
    from info import info_app
    from collection_paths_image import ImagePathnCollection
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging

    from manage_generator import list_object_generator

    from core import restart
    from core.manage_ini_file import get_configuration, create_write_ini_file
   
    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'setting.ui')
#UI_PATH = QFile(":/ui_file/setting.ui")
#self.path_mdi_form = os.path.abspath(".")
#self.getPath_setting = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_setting.ui')
class Setting_Window(QWidget):
    """ 
        A Setting_Window()-object loads the setting-window. It is uses
        the qt-framework by using pyqt-bindings. In this object the user
        can change and save settings for this program. First, the settings
        are stored in an INI file, then into a dictionary.
    """
    def __init__(self,
                 configuration_saver_dict=None,
                 close_application=None,
                 image_path_collection=None,
                 default_ini_file=None,
                 create_error_form=None,
                 parent=None):
        QWidget.__init__(self, parent)

        '''
            NOTICE:
            =======
            All instance variables that contain class variables
        '''
        self.configuration_saver_dict = configuration_saver_dict
        self.close_application = close_application
        self.default_ini_file = default_ini_file
        self.create_error_form = create_error_form

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.image_path_collection = image_path_collection

        UI_PATH = QFile(":/ui_file/setting.ui")


        self.load_ui_file(UI_PATH)
        self.init_ui()
        self.init_list_widget_menu()
        self.init_textChanged_signal_line_edits()

        self.load_and_show_settings()
        self.init_stateChanged_signal_check_boxes()
        self.init_toggled_signal_radio_buttons()
        self.init_clicked_signal_push_buttons()
        self.init_activated_signal_combo_boxes()
        self.init_currentRowChanged_signal_list_widget()

        self.Check_all_check_boxes_activated()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__


    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_setting = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def menu_generator(self, menu_list):
        '''
            NOTICE:
            =======
            We don't need a new list. 
            This method expects a list of tuples.
        '''
        logger.info("Yield each item of a menu_list")

        for icon, text in menu_list:

            yield icon, text
           

    def init_list_widget_menu(self):
        '''
            NOTICE:
            =======
            QListQidget is inistialized. This method contains
            a list of tuples. Next we need to write a for-loop, that
            loop through a generator (menu_generator).
        '''

        items_list = [(':/img_48x48/img_48x48/Home.png', self.tr('General')), 
                        (':/img_48x48/img_48x48/Category.png', self.tr('Categories')),
                        (":/img_48x48/img_48x48/Folder.png", self.tr("Paths")),
                        (":/img_48x48/img_48x48/Database.png", self.tr("Database")),
                        (":/img_48x48/img_48x48/Updates.png", self.tr("Update"))]

        logger.info("The for loop runs that generator and create a menue")
        
        for icon, text in self.menu_generator(items_list):
            '''
                NOTICE:
                ======
                By looping through a generator it will create a 
                QListWidgetItem every time and than add 
                items to a list widget using QListWidgetItem.

                In this case its constructs a list widget 
                item of the specified type with the given icon and text.
                We don't need to specify the parent, because 
                all items will add into a list widget with the addItem()-method
                of the QListWidget()-class. 

                Each icon of an item is displayed with a size of 48x48 currently.

                Last but not least: Each item is set the size hint with the QSize()-class, because
                we don't want the item delegate will compute the size hint based on the item data.
                We want that all items are display in the center of the QListWidget. the First
                number stands for horizontal indication. That means from left to right. 
                Smaller the number, it moves the items to the left - vice versa 
                larger the number, the items moves to the right of the QListWidget. 
                Well the second number is a vertical indication. That means, 
                it vertically stretches the icon and the text apart. Smaller the
                number less is stretched. Gaps between icon and text are reduced. 
                Bigger the number, more is strechted. Graps between icon and text 
                gets bigger.

                PARAMETERS:
                ===========
                :return    -    Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
            '''
            item = QListWidgetItem(QIcon(icon), self.tr(text))

            self.ui_setting.listWidget_menu.addItem(item) 
            self.ui_setting.listWidget_menu.setIconSize(QSize(48, 48))

            item.setSizeHint(QSize(146, 75))

        return

    def init_textChanged_signal_line_edits(self):
        '''
            NOTE:
            ======
            This method initializes the LineEdit signals: textChanged().
            It makes an action when the QLineEdit()-object text is changed.
            When the user types something in the QLineEdit()-object -
            (now, the text is changed by a user) -
            the textChanged-signal of QLineEdit()-object emitters.
            In this case, the signal connects with a set_text()-method by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QLineEdit()-object with textChanged-signal")
       
        self.ui_setting.lineEdit_server_version_address.textChanged.connect(lambda:
                                                                            self.set_text(dict_key="URLServerVersion",
                                                                            line_text_obj=self.ui_setting.lineEdit_server_version_address))

        self.ui_setting.lineEdit_temp_folder.textChanged.connect(lambda:
                                                                 self.set_text(dict_key="PathToTempFolder",
                                                                 line_text_obj=self.ui_setting.lineEdit_temp_folder))

        self.ui_setting.lineEdit_url_update.textChanged.connect(lambda:
                                                                 self.set_text(dict_key="URLServerUpdate",
                                                                 line_text_obj=self.ui_setting.lineEdit_url_update))

        self.ui_setting.lineEdit_timeout_update.textChanged.connect(lambda:
                                                                 self.set_text(dict_key="TimeOutUpdate",
                                                                 line_text_obj=self.ui_setting.lineEdit_timeout_update))

        return    

    def init_ui(self):

        '''
            NOTE:
            ======
            This method initializes the user interface of setting-window. 
            Currently it sets an icon on the window - left up in the corner.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Set the icon on the window")

        #flags = flag_dialog_titlehint_customizewondowhint()

        #self.ui_setting.setWindowFlags(flags)

        self.ui_setting.setWindowIcon(QIcon(":/img_16x16/img_16x16/Settings.png"))

        self.ui_setting.comboBox_style.addItems((QStyleFactory.keys()))

        return


    def open_folder_dialog(self):
        '''
            NOTE:
            ======
            We want to know in which folder the update file should be stored temporarily.

            This method implements a QFileDialog()-class that a getExistingDirectory()-method.
            This dialog is used, because the user should select a directory. 
            Ask the user for the name of an existing directory, starting at dir.
            Well, that the user doesn't have to search the path again, 
            the last path is stored in a dictionary and in a setting file permanently.
            If the user starts this program at first time, the dir is an empty string.
            In this case, the current directory will be used respectively. That means,
            if dir is null, getExistingDirectory() starts wherever the previous file dialog left off.
            Next step, ShowDirsOnly is set, that ensure a native file dialog. Here only
            show directories in the file dialog.

            If the user selected a folder it returns the name of the directory.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Open the file dialog")

        folder = QFileDialog.getExistingDirectory(None, self.tr("Select folder"),
                                                  #os.path.expanduser("~"),
                                                  os.path.expanduser(self.configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"]),
                                                  #("C:\meins"),
        QFileDialog.ShowDirsOnly)

        if unicode(folder) == '':
            '''
                NOTICE:
                =======
                No, the user doesn't select a folder.
                By default the QLineEdit()-object is emptied when the user just closes the QFileDialog() - 
                without selecting a folder. The QLineEdit()-object is empty. To prevent this: there is a 
                dictionary where the last path of current temp folder is stored. Well, when the local 
                variable named (folder) is empty, that tells us, the user just closed the folder-dialog.
                In this case the LineEdit()-object is filled with setText()-method. 
                QLineEdit()-object holds a path, which is stored in a dictionary. The text is converted 
                with unicode().

            '''
            self.ui_setting.lineEdit_temp_folder.setText(self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"])
        else:
            '''
                NOTICE:
                =======
                Yes, the user does select a folder. 
                - First its updates the QLineEdit()-object with setText()-method. 
                We don'T need to clear the contant of the QLineEdit()-object before, 
                because this dropped automatically. The user will see the new text.
                - Second, the new path, which the user has selected, is stored in the dictionary. 
            '''
            self.ui_setting.lineEdit_temp_folder.setText(unicode(folder))
            self.configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"] = unicode(folder)

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        self.ui_setting.pushButton_Ok.clicked.connect(lambda: self.save_setting_ini(self.configuration_saver_dict))
        self.ui_setting.toolButton_open_update_temp_folder.clicked.connect(self.open_folder_dialog)
        self.ui_setting.pushButton_Cancel.clicked.connect(self.close_form)

        self.ui_setting.pushButton_ask_before_delte_data_item.clicked.connect(lambda: 
            self.show_tool_tip_text(self.tr('When disabled the selected data item is automatically deleted in database without confirmation prompt.')))

        return

    def init_currentRowChanged_signal_list_widget(self):
        logger.info("Initialize the QListWidget()-object with currentRowChanged-signal")

        self.ui_setting.listWidget_menu.currentRowChanged.connect(self.select_stacked_widget)

    def init_stateChanged_signal_check_boxes(self):
        '''
            NOTE:
            ======
            This method initializes the checkBox signals: stateChanged().
            It makes an action when the QCheckBox()-object is stateChanged.
            When the user checks or unchecks the QCheckBox()-object
            the stateChanged-signal of QCheckBox()-object emitters.
            The state of the QCheckBox()-object is changed.

            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the QCheckBox()-object with stateChanged-signal")

        self.ui_setting.checkBoxAskBeforClose.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="AskBeforClose",
                                        chk_obj=self.ui_setting.checkBoxAskBeforClose,
                                        choosen_chkbox_key="checkBox_ask_befor_close"))

        self.ui_setting.checkBox_ask_before_delte_data_item.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="AskBeforDelete",
                                         chk_obj=self.ui_setting.checkBox_ask_before_delte_data_item,
                                         choosen_chkbox_key="checkBox_ask_befor_delete"))

        self.ui_setting.checkBox_show_tool_bar.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleToolBar",
                                        chk_obj=self.ui_setting.checkBox_show_tool_bar,
                                        choosen_chkbox_key="checkBox_visible_tool_bar"))

        self.ui_setting.checkBox_show_status_bar.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleStatusBar",
                                        chk_obj=self.ui_setting.checkBox_show_status_bar,
                                        choosen_chkbox_key="checkBox_visible_status_bar"))

        self.ui_setting.checkBox_visible_film.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleMovie",
                                        chk_obj=self.ui_setting.checkBox_visible_film,
                                        choosen_chkbox_key="checkBox_visible_movie"))

        self.ui_setting.checkBox_visible_coin.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleCoin",
                                        chk_obj=self.ui_setting.checkBox_visible_coin,
                                        choosen_chkbox_key="checkBox_visible_coin"))

        self.ui_setting.checkBox_visible_quotation.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleQuotation",
                                        chk_obj=self.ui_setting.checkBox_visible_quotation,
                                        choosen_chkbox_key="checkBox_visible_quotation"))

        self.ui_setting.checkBox_visible_stamp.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleStamp",
                                        chk_obj=self.ui_setting.checkBox_visible_stamp,
                                        choosen_chkbox_key="checkBox_visible_stamp"))

        self.ui_setting.checkBox_visible_video_game.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleVideoGame",
                                        chk_obj=self.ui_setting.checkBox_visible_video_game,
                                        choosen_chkbox_key="checkBox_visible_video_game"))

        self.ui_setting.checkBox_visible_wine.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleWine",
                                        chk_obj=self.ui_setting.checkBox_visible_wine,
                                        choosen_chkbox_key="checkBox_visible_wine"))

        self.ui_setting.checkBox_visible_rent.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleRent",
                                        chk_obj=self.ui_setting.checkBox_visible_rent,
                                        choosen_chkbox_key="checkBox_visible_rent"))

        self.ui_setting.checkBox_visible_person.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisiblePerson",
                                        chk_obj=self.ui_setting.checkBox_visible_person,
                                        choosen_chkbox_key="checkBox_visible_person"))

        self.ui_setting.checkBox_visible_contact.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleContact",
                                        chk_obj=self.ui_setting.checkBox_visible_contact,
                                        choosen_chkbox_key="checkBox_visible_contact"))

        self.ui_setting.checkBox_visible_music.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleMusic",
                                        chk_obj= self.ui_setting.checkBox_visible_music,
                                        choosen_chkbox_key="checkBox_visible_music"))

        self.ui_setting.checkBox_visible_book.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleBook",
                                        chk_obj=self.ui_setting.checkBox_visible_book,
                                        choosen_chkbox_key="checkBox_visible_book"))

        self.ui_setting.checkBox_visible_note.stateChanged.connect(
            lambda: self.set_checkboxes(dict_config_key="VisibleNote",
                                        chk_obj=self.ui_setting.checkBox_visible_note,
                                        choosen_chkbox_key="checkBox_visible_note"))

        self.ui_setting.checkBox_select_all_categories.stateChanged.connect(
            lambda: self.check_all_categories_check_boxes(chk_obj=self.ui_setting.checkBox_select_all_categories))

        return

    def init_activated_signal_combo_boxes(self):
        '''
            NOTE:
            ======
            The user chooses an item in the QComboBox()-object.

            This method initializes the ComboBox signals: activated().
            It makes an action when the QComboBox()-object is activated.
            This signal is sent when the user chooses an item in the QCheckBox()-object. 
            The item's text is passed. This signal is sent even when the choice 
            is not changed. The activated-signal of QCheckBox()-object emitters.

            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the QComboBox()-object with activated-signal")

        self.ui_setting.comboBox_style.activated[str].connect(lambda:
                                                              self.combo_gui_style_changed(self.ui_setting.comboBox_style))
        
        self.ui_setting.comboBox_scheme_color.activated.connect(lambda:
                                                                self.combo_scheme_color_changed(self.ui_setting.comboBox_scheme_color))
        
        self.ui_setting.comboBox_language.currentIndexChanged.connect(lambda:
                                                                      self.combo_language_changed(self.ui_setting.comboBox_language))


        return

    def init_toggled_signal_radio_buttons(self):
        '''
            NOTE:
            ======
            The user chooses and click QRadioButton()-object.

            This method initializes the RadioButton signals: toggled().
            It makes an action when the QRadioButton()-object is toggled.
            This signal is sent when the user checks a QRadioButton()-object. 
            This signal is only emitted if the button changes state. 
            The signal has a bool that gives the new state of the button. 

            In this case, the signal connects with a method - by passing some arguments.

            This signal is only emitted if the button changes state

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the QRadioButton()-object with toggled-signal")

        self.ui_setting.radioButton_mysql.toggled.connect(lambda:
                                                          self.set_radioButton(dict_database_key="Database",
                                                                               db_system="MySQL",
                                                                               radio_obj=self.ui_setting.radioButton_mysql))
                    
        self.ui_setting.radioButton_postgre.toggled.connect(lambda:
                                                          self.set_radioButton(dict_database_key="Database",
                                                                               db_system="PostgreSQL",
                                                                               radio_obj=self.ui_setting.radioButton_postgre))
        
        self.ui_setting.radioButton_mssql.toggled.connect(lambda:
                                                          self.set_radioButton(dict_database_key="Database",
                                                                               db_system="Microsoft SQL",
                                                                               radio_obj=self.ui_setting.radioButton_mssql))
        
        self.ui_setting.radioButton_sqlite.toggled.connect(lambda:
                                                          self.set_radioButton(dict_database_key="Database",
                                                                               db_system="SQLite",
                                                                               radio_obj=self.ui_setting.radioButton_sqlite))
        
        self.ui_setting.radioButton_ms_access.toggled.connect(lambda:
                                                          self.set_radioButton(dict_database_key="Database",
                                                                               db_system="Microsoft Access",
                                                                               radio_obj=self.ui_setting.radioButton_ms_access))

        return

    def find_Children_object(self,
                            widget_obj=None,
                            qt_obj=None):
        '''
            NOTICE:
            =======
            Note: that findChild and findChildren are methods of QObject - 
            so if your form does not have them, it cannot be a QWidget 
            (because all widgets inherit QObject). this method returns 
            the childs of this object that can be cast.

            PARAMETERS:
            ===========
            :widget_obj -   We need a the specified parentWidget to find a list of child QWidgets 
            :qt_obj     -   This parameter tells us the given name of the widget,  that are children of parentWidget.
            :return     -   The function returns a list that contains all
                            children of this object. If there are no such objects
                            its returns an empty list.
        '''
        logger.info("Return a list of all children of this object")

        return widget_obj.findChildren(qt_obj)

    def Check_all_check_boxes_activated(self):

        logger.info("Check all QCheckBox()-objects in category-area are checked")
        '''
            NOTICE:
            =======
            Situation: On the "categories-page" of stackedWidget the user can
            pick up the appropriate individual category which he wants to include 
            in his management. But there is a an option the user can
            choose a QCheckBox()-object that allows to change the 
            state of all existing QCheckBox()-objects on this "categories-page" at once.
            Well, this method checks whether all QCheckBox()-objects are checked.

            Let us take a look, which QCheckBox()-object is checked or not.
            In This chase it uses the isChecked()-method. 
            Caution: Do not use isChecked without round brackets, because
            that isn't a boolean, it's a method - which, in a boolean context, 
            will always evaluate to True. But we you want the state of the checkbox, 
            that we just invoke the method with round bracktes.


            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        '''
        We use findChildren()-method of QObject()-class to grab certain components of the form instance.
        In this case we want all QCheckBox()-objects, they are collected in a groupBox. the 
        findChildren()-method returns a list. This list is saved in the variable named (check_box_result)
        '''
        check_box_result = self.find_Children_object(widget_obj=self.ui_setting.groupBox_categories,
                                                qt_obj=QCheckBox)

        '''
        We want to know the number of elements in the list, that is saved 
        in the variable named (check_box_result) above. So we us the 
        len()-method that returns the number of elements in the list and
        then we save the number in the variable named (len_old_list).
        '''
        len_old_list = len(check_box_result)

        '''
        Create a new empty list.
        '''
        list_check_boxes = list()

        '''
        Loop over the list that contains all QCheckBox()-objects
        '''
        for check_box_object in list_object_generator(check_box_result):
            '''
            Each individual check_box_object is checked if it is checked.
            Here we invoke the isChecked()-method.
            '''
            if check_box_object.isChecked():
                '''
                Yes, the check_box_object is checked. Next we
                will append each individual check_box_object 
                to the list named (list_check_boxes).
                '''
                list_check_boxes.append(check_box_object)

        '''
        Well the loop is over. We want to know the number of elements in the list
        named (list_check_boxes), that is saved in the variable names (len_new_list).
        '''
        len_new_list = len(list_check_boxes)

        '''
        Last step in this if-statement we compare the two numbers.
        '''
        if len_new_list == len_old_list:
            '''
            Both numbers are equal. We change the state of the
            QCheckBox()-object named (checkBox_select_all_categories).
            We invoke the setChecked()-method and set it on True. 
            '''
            self.ui_setting.checkBox_select_all_categories.setChecked(True)

        return

    def check_all_categories_check_boxes(self, chk_obj=None):
        '''
            NOTICE:
            =======
            Text Here

            PARAMETERS:
            ===========
            :param chk_obj -    This function excepts an QCheckBox()-object.
                                In this case we need this object for checking 
                                the changed state.

            :return    -        Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Check the chk_obj ({}) is isChecked".format(chk_obj.objectName()))

        '''
        We call the function named (find_Children_object)
        and pass two keyword arguments.
        The called function returns a list that is saved in the local
        variable named (chk_obj_result).
        '''
        chk_obj_result = self.find_Children_object(widget_obj=self.ui_setting.groupBox_categories,
                                                qt_obj=QCheckBox)

        '''
        Let us start to take a look if the obtained QCheckBox()-object is checked.
        '''
        if chk_obj.isChecked():
            '''
                Yes, the isChecked()-method returns a boolean value True,
                that means the object is checked.

                Next step - since the condition is fulfilled - 
                we iterate through the generator named (list_object_generator) 
                with passed arguments.Here the argument (chk_obj_result) is a list.
                Here we change the state of checkbox button to True
            '''
            for check_box in list_object_generator(chk_obj_result):
                check_box.setChecked(True)

        else:
            '''
                But, if  the isChecked()- method returns a boolean value False,
                that means the obtained QCheckBox()-object is not checked. 
                We also iterate through the generator named (list_object_generator) 
                with passed arguments.Here the argument (chk_obj_result) is a list.
                We want change the state of the QCheckBox()-object to False
            '''
            for check_box in list_object_generator(chk_obj_result):
                check_box.setChecked(False)

        return


    def set_checkboxes(self,
                       dict_config_key=None,
                       chk_obj=None,
                       choosen_chkbox_key=None):
        '''
        :param dict_config_key:     In the module named (config) there is a dictionary-container.
                                    Here you get the key to access to this container. This
                                    dictionary saves all setting of this program. You have to
                                    modify the value for saving the setting in a ini-file.

        :param choosen_chkbox_key:  In the function namend (list_check_box) you will
                                    find all checkboxes in diffferent container: dictionaries.
                                    In this case you need the parameter (choosen_chkbox)
                                    to pick up particular checkbox from
                                    dictionary. This parameter contains a key. With them
                                    you can access to the value, that contains a checkbox-widget.

        :param select_mode:         Currently you can choose: (all_manager_categories) and
                                    (single). When you choose (all_manager_categories),
                                    that means, you want to check or uncheck all the
                                    other special checkbox with one special checkbox.
                                    But if  you choose (single), you want to check or uncheck
                                    each individual checkbox.
                                    
        :return:                    None
        '''
        logger.info("Save the setting of {}()-object in the dictionary".format(chk_obj))

        if chk_obj.isChecked():
            self.configuration_saver_dict.dict_set_general_settings[dict_config_key] = "True"
        else:
            self.configuration_saver_dict.dict_set_general_settings[dict_config_key] = "False"

        return

    def set_radioButton(self, dict_database_key=None,
                        db_system=None,
                        radio_obj=None):

        '''
            NOTICE:
            =======
            Once you have done this, you can simply check the value of the parameter. 
            If it is false it means that the button was checked and now has just been unchecked. 
            If the radio button has just been unchecked, do not do anything, just go away.
        '''
        if radio_obj.isChecked():
            self.configuration_saver_dict.dict_set_general_settings[dict_database_key] = db_system
        else:
            self.configuration_saver_dict.dict_set_general_settings[dict_database_key] = "None"
            #print self.configuration_saver_dict.dict_set_general_settings["Database"]

    def radio_boxes_database(self, mode_str):
        #print "DATABASE", mode_str
        try:
            parsing_dict = {
                    "mysql": self.ui_setting.radioButton_mysql,
                    "microsoft sql": self.ui_setting.radioButton_mssql,
                    "microsoft access": self.ui_setting.radioButton_ms_access,
                    "postgresql": self.ui_setting.radioButton_postgre,
                    "sqlite": self.ui_setting.radioButton_sqlite
                        }
            #print "DATABASE After", mode_str.lower()
            ret = parsing_dict[mode_str.lower()].setChecked(True)
        except:
            return False
#-----------------------------------------------------------------------------------------------------------------------
    def convert_string_to_bool(self, value):
        '''
            NOTICE:
            ======
            This method converts a given string in boolean value.
            We need this method for all QCheckBox()-objects. In the ini file, where all 
            settings are saved, there are only strings (for example: 'True' or 'False'). 
            All values aren't boolean value. When we want to change the state of 
            QCheckBox()-object, we have to convert all corresponding strings before.

            PARAMETERS:
            ===========
            :return     -   In the try block its returns the converted value. But, when 
                            the operation fails its returns False. In this case its
                            possible that the user or a other software has manipulated 
                            the contents of the ini file. 
        '''

        try:
            '''
                Let us try to invoke the function by passing an argument. In this case, we access
                to a dictionary at first, because the loaded settings are saved in the dictionary.
                We use the key (its the argument named value) to get the value, that is saved behind
                the key. With the obtained value we try to convert it in the bool-
            '''
            logger.info("Try to invoke a parsing_str_to_bool()-function by passing an {}-argument".format(value))

            return parsing_str_to_bool(self.configuration_saver_dict.dict_set_general_settings[value])
        except ValueError as VaErr:
            '''
                Ups, an error has occurred. Maybe the user or other program has changed 
                the content of this ini file. We write this error in the log file and
                return False. That means, the current QCheckBox()-object isn't check. The
                user will see an uncheck QCheckBox()-object.
            '''
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            return False

    def select_stacked_widget(self, index):
        '''
            NOTICE:
            ======
            This functions changes widget of QStackedWidget and its fades 
            between widgets on different pages - dependent on the index. 

            PARAMETERS:
            ===========
            :index -        This function excepts an index of an QListWidget()-object.
                            This method is connected with an QListWidget()-object over 
                            currentRowChanged()-signal. This object emits the index of 
                            the chosen element if the current item hase been changed and 
                            the row is different to the row of the previous current item.
                            This method gets the current index of changed item.

            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        
        try:

            logger.info("Try to set the current indext of stackedWidget()-object to {} ".format(index))

            self.ui_setting.stackedWidget.setCurrentIndex(int(index))

        except ValueError as VaErr:
            '''
                Ups, an error has occurred. Maybe the user or other program has changed 
                the content of this ini file. We write this error in the log file and
                return False. That means, the current QCheckBox()-object isn't check. The
                user will see an uncheck QCheckBox()-object.
            '''
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

        return

    def load_and_show_settings(self):
        '''
            NOTICE:
            =======
            Once the user loads this setting window, we want to show him all his saved settings.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Load an show all setting through Qwidgets()-objects")

        '''
                In the setText()-method we access to a dictionary at first, 
                because the loaded settings are saved in the dictionary.
                We use the key to get the value, that is saved behind
                the key. With the obtained value we fill it in the QLineEdit()-object.
        '''
        logger.info("Setting the text of lineEdit_url_update()-object  - (" + __name__ + ")")
        
        self.ui_setting.lineEdit_url_update.setText(self.configuration_saver_dict.dict_set_general_settings["URLServerUpdate"])

        logger.info("The text of lineEdit_url_update()-object is set successfully - (" + __name__ + ")")
        
        logger.info("Setting the text of lineEdit_temp_folder()-object  - (" + __name__ + ")")

        self.ui_setting.lineEdit_temp_folder.setText(self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"])

        logger.info("The text of lineEdit_temp_folder()-object is set successfully - (" + __name__ + ")")

        logger.info("Setting the text of lineEdit_server_version_address()-object  - (" + __name__ + ")")

        self.ui_setting.lineEdit_server_version_address.setText(self.configuration_saver_dict.dict_set_general_settings["URLServerVersion"])

        logger.info("The text of lineEdit_server_version_address()-object is set successfully - (" + __name__ + ")")

        logger.info("Setting the text of lineEdit_timeout_update()-object  - (" + __name__ + ")")

        self.ui_setting.lineEdit_timeout_update.setText(self.configuration_saver_dict.dict_set_general_settings["TimeOutUpdate"])

        logger.info("The text of lineEdit_timeout_update()-object is set successfully - (" + __name__ + ")")

        '''
                In the setChecked()-method we invoke the 
                convert_string_to_bool()-method at first by passing an string-argument.
                We except a boolean value from the convert_string_to_bool()-method 
                that is called. With the obtained boolean value we change the state
                of the QCheckBox()-object.
        '''
        self.ui_setting.checkBoxAskBeforClose.setChecked(self.convert_string_to_bool("AskBeforClose"))
        self.ui_setting.checkBox_ask_before_delte_data_item.setChecked(self.convert_string_to_bool("AskBeforDelete"))
        self.ui_setting.checkBox_show_tool_bar.setChecked(self.convert_string_to_bool("VisibleToolBar"))
        self.ui_setting.checkBox_show_status_bar.setChecked(self.convert_string_to_bool("VisibleStatusBar"))
        self.ui_setting.checkBox_visible_film.setChecked(self.convert_string_to_bool("VisibleMovie"))
        self.ui_setting.checkBox_visible_contact.setChecked(self.convert_string_to_bool("VisibleContact"))
        self.ui_setting.checkBox_visible_quotation.setChecked(self.convert_string_to_bool("VisibleQuotation"))
        self.ui_setting.checkBox_visible_person.setChecked(self.convert_string_to_bool("VisiblePerson"))
        self.ui_setting.checkBox_visible_wine.setChecked(self.convert_string_to_bool("VisibleWine"))
        self.ui_setting.checkBox_visible_video_game.setChecked(self.convert_string_to_bool("VisibleVideoGame"))
        self.ui_setting.checkBox_visible_rent.setChecked(self.convert_string_to_bool("VisibleRent"))
        self.ui_setting.checkBox_visible_stamp.setChecked(self.convert_string_to_bool("VisibleStamp"))
        self.ui_setting.checkBox_visible_music.setChecked(self.convert_string_to_bool("VisibleMusic"))
        self.ui_setting.checkBox_visible_book.setChecked(self.convert_string_to_bool("VisibleBook"))
        self.ui_setting.checkBox_visible_coin.setChecked(self.convert_string_to_bool("VisibleCoin"))
        self.ui_setting.checkBox_visible_note.setChecked(self.convert_string_to_bool("VisibleNote"))

        '''
                In the setCurrentIndex()-signal we access to a dictionary at first, 
                because the loaded settings are saved in the dictionary.
                We use the key to get the value, that is saved behind
                the key. With the obtained value we set the current index of the
                QComboBox()-object. We use the build-in init()-function of
                Python to convert the value (it should be a number) in 
                a integer. When the operation fails we use zero. That means,
                the user is shown the first element of the QComboBox()-object.
        '''
        try:
            self.ui_setting.comboBox_language.setCurrentIndex(int(self.configuration_saver_dict.dict_set_general_settings["language_id"]))
        except ValueError:
            self.ui_setting.comboBox_language.setCurrentIndex(int(0))

        try:
            self.ui_setting.comboBox_style.setCurrentIndex(int(self.configuration_saver_dict.dict_set_general_settings["GUIStiyleID"]))
        except ValueError:
            self.ui_setting.comboBox_style.setCurrentIndex(int(0))

        try:
            self.ui_setting.comboBox_scheme_color.setCurrentIndex(int(self.configuration_saver_dict.dict_set_general_settings["SchemeColorID"]))
        except ValueError:
            self.ui_setting.comboBox_scheme_color.setCurrentIndex(int(0))

        #   Mark all QRadio-Widgets
        self.radio_boxes_database(self.configuration_saver_dict.dict_set_general_settings["Database"])

        return

    def create_info_messagebox(self, messagebox_title, message):
        '''
            NOTICE:
            =======
            When the user clicks on the apply button for saving all settings,
            he will get a message box that says him the settings were saved successfully
            and it will ask the user if he wants to restart the program.

            PARAMETERS:
            ===========
            :messagebox_title   -       The title of the message box.
            :message            -       The message of the message box.
            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Creating a info message box")

        setIc_OK = QIcon(self.image_path_collection.ok_icon_unload_mdi_msgbox)
        setIc_No = QIcon(self.image_path_collection.no_icon_unload_mdi_msgbox)
        setIc_Cancel = QIcon(self.image_path_collection.cancel_icon_unload_mdi_msgbox)
        msgBox_unload_mdi = QMessageBox()
        msgBox_unload_mdi.setWindowTitle(messagebox_title)
        msgBox_unload_mdi.setWindowIcon(QIcon(self.image_path_collection.question_icon_title__unload_mdi_msgbox))
        msgBox_unload_mdi.setIconPixmap(QPixmap(self.image_path_collection.question_icon_unload_mdi_msgbox))
        msgBox_unload_mdi.setText(message)
        msgBox_unload_mdi.addButton(
            QPushButton(setIc_OK, self.tr("Ja")),
            QMessageBox.YesRole)
        msgBox_unload_mdi.addButton(
            QPushButton(setIc_No, self.tr("Nein")),
            QMessageBox.NoRole)
        msgBox_unload_mdi.addButton(
            QPushButton(setIc_Cancel, self.tr("Abbrechen")),
            QMessageBox.RejectRole)
        ret = msgBox_unload_mdi.exec_()
        if ret == 0:
            restart.restart_program(self)
            self.close_application()
        elif ret == 1:
            pass
        else:
            pass

        return

    def read_ini_file(self, configuration_saver_dict):
        '''
        NOTICE:
        =======
            We read in the ini file again after.

        PARAMETERS:
        ===========
            :configuration_saver_dict   -       It excepts a dictionary.
                                        In module, named config, there is 
                                        class, named ConfigurationSaver, you will 
                                        find a dictionary.
            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        try:
            logger.info("Trying to save all settings in the dictionary")
            
            get_configuration(self.configuration_saver_dict.dict_set_general_settings["PathToINISetting"], configuration_saver_dict, self.default_ini_file)

            logger.info("All settings are saved in the dictionary successfully")

            logger.info("Trying to create a message box")

            messagebox_title = self.tr("Message")
            message = self.tr("The setting changes were saved successfully \n"
                              "\n "
                              "Notice: "
                              "Some changes only take effect after a restart. \n"
                              "\n"
                              "Do you want to restart the program? You also have the option to restart the program later.")
            self.create_info_messagebox(messagebox_title, message)

            logger.info("The message box is created successfully")

        except (Exception, IOError, ValueError):
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def save_setting_ini(self, configuration_saver_dict):
        '''
        NOTICE:
        =======
            We save all settings in the ini file.

        PARAMETERS:
        ===========
            :configuration_saver_dict   -       It excepts a dictionary.
                                        In module, named config, there is 
                                        class, named ConfigurationSaver, you will 
                                        find a dictionary.
            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        
        try:
            logger.info("Trying ti write all setting in a ini file")

            create_write_ini_file(configuration_saver_dict, self.default_ini_file)

            logger.info("All setting is written in a ini file successfully")

            logger.info("Trying to read all setting from a ini file")

            self.read_ini_file(configuration_saver_dict)

            logger.info("All setting are read from a ini file successfully")

        except (Exception, IOError, ValueError):

            logger.info("An error has occurred")

            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            self.create_error_form(title_text=self.tr("Error"),
                                   detail_mode=desired_trace)

        return

    def set_text(self, dict_key=None, line_text_obj=None):
        '''
        NOTICE:
        =======
            This method excepts a key of a dictionary and a QLineEdit()-object.
            In the QLineEdit()-object there a some information that we want to
            save it in the dictionary. That why wie need a key.

        PARAMETERS:
        ===========
            :dict_key           -       Its excepts a key of a dictionary. 
                                        We need the key of the dictionary to get access to the value.
            :line_text_obj      -       In this parameter you will get the QLineEdit()-object.
            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        logger.info("Fill the value of the dictionary")

        self.configuration_saver_dict.dict_set_general_settings[dict_key] = unicode(line_text_obj.text())

        return

    def combo_language_changed(self, combo_obj):
        '''
        NOTICE:
        =======
            This method stores a dictionary. Here we 
            construct a dictionary with integer keys and unbound methods as values.
            We use the lambda function to pass extra arguments to the methods user wants to execute.
            The user wants to change the language. He clicks in the combo box
            and select his language. The current index of the combo box is changed.
            Once the index has been changed, this method is called.

        PARAMETERS:
        ===========
            :combo_obj          -       QComboBox()-object is saved in this parmater.
                                        We need this object to find out the current index.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("The language is changed")
        dict_ids = {
                    0: lambda: self.change_language("language", "german_sie",
                                                    "language_id", int(combo_obj.currentIndex())),
                    1: lambda: self.change_language("language", "german_du",
                                                    "language_id", int(combo_obj.currentIndex())),
                    2: lambda: self.change_language("language", "english_us",
                                                    "language_id", int(combo_obj.currentIndex())),
                    3: lambda: self.change_language("language", "english_uk",
                                                    "language_id", int(combo_obj.currentIndex())),
                    }

        '''
            During execution, we access to the dictionary. In this case  
            we use the integer keys (the index of the current item in the combobox)  
            to select which method (self.change_language()) to execute.
        '''
        dict_ids[int(combo_obj.currentIndex())]()

        return

    def combo_gui_style_changed(self, combo_obj):
        '''
        NOTICE:
        =======
            This method stores a dictionary. Here we 
            construct a dictionary with integer keys and unbound methods as values.
            We use the lambda function to pass extra arguments to the methods user wants to execute.
            The user wants to change the combo box for gui style. He clicks in the combo box
            and select his favorite gui style. The current index of the combo box is changed.
            Once the index has been changed, this method is called.

        PARAMETERS:
        ===========
            :combo_obj          -       QComboBox()-object is saved in this parmater.
                                        We need this object to find out the current index.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("The GUI style is changed")
        dict_ids = {
                    0: lambda: self.change_gui_style("GUIStyle", "automatically",
                                                    "GUIStiyleID", unicode(combo_obj.currentIndex()))
                    }

        if combo_obj.currentIndex() == 0:
            '''
                User selects the first item in the combo box. In Python language, the 
                first item beginns with zero. Well when the user selectss the first
                item that means, the gui style isn't to be changed. 
                The GUI style adapts to the operating system.
            '''
            dict_ids[int(combo_obj.currentIndex())]()
        else:
            '''
                The user would like a particular gui style. In this case we access to the
                dictionary to the two keys (GUIStyle, GUIStiyleID). In the first key (GUIStyle)
                we save the text of the current item of the combo box by using currentText()-signal.
                In the second key (GUIStiyleID) we save the current index of the combo box, by using
                the currentIndex()-signal.The resulting index is converted to a string.
            '''
            self.configuration_saver_dict.dict_set_general_settings["GUIStyle"] = unicode(combo_obj.currentText())
            self.configuration_saver_dict.dict_set_general_settings["GUIStiyleID"] = str(combo_obj.currentIndex())

        return

    def combo_scheme_color_changed(self, combo_obj):
        '''
        NOTICE:
        =======
            This method stores a dictionary. Here we 
            construct a dictionary with integer keys and unbound methods as values.
            We use the lambda function to pass extra arguments to the methods user wants to execute.
            The user wants to change the combo box for scheme color. He clicks in the combo box
            and select his favorite scheme color. The current index of the combo box is changed.
            Once the index has been changed, this method is called.

        PARAMETERS:
        ===========
            :combo_obj          -       QComboBox()-object is saved in this parmater.
                                        We need this object to find out the current index.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("The SchemeColor is changed")

        dict_ids = {
                    0: lambda: self.change_scheme_color("SchemeColor", "default",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    1: lambda: self.change_scheme_color("SchemeColor", "classic",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    2: lambda: self.change_scheme_color("SchemeColor", "dark_modern",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    3: lambda: self.change_scheme_color("SchemeColor", "dark_old",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    4: lambda: self.change_scheme_color("SchemeColor", "metal",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    5: lambda: self.change_scheme_color("SchemeColor", "dracula",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    6: lambda: self.change_scheme_color("SchemeColor", "ida_skin",
                                                    "SchemeColorID", int(combo_obj.currentIndex())),
                    
                    }
        '''
            During execution, we access to the dictionary. In this case  
            we use the integer keys (the index of the current item in the combobox)  
            to select which method (self.change_language()) to execute.
        '''
        dict_ids[int(combo_obj.currentIndex())]()

        return

    def change_language(self,setting_key_language, language_value,
                        setting_key_language_id, combo_id):
        '''
        NOTICE:
        =======
            This method access to a dictionary. 
            The user wants to change the language. 

        PARAMETERS:
        ===========
            :setting_key_language          -        In this parameter its a key of the dictionary.
                                                    We need it to access to the value.

            :language_value                 -       Its excepts a value for dictionary. Here we get the catogory of language.

            :setting_key_language_id        -       its a key of the dictionary.

            :combo_id                       -       The current index is saved in this parameter

            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.

        '''
        logger.info("Fill the value of the dictionary")

        self.configuration_saver_dict.dict_set_general_settings[setting_key_language] = language_value
        self.configuration_saver_dict.dict_set_general_settings[setting_key_language_id] = str(combo_id)

        return

    def change_gui_style(self,
                            setting_key_gui_style_name,
                            gui_style_value,
                            setting_key_gui_style_id,
                            combo_id):
        '''
        NOTICE:
        =======
            This method access to a dictionary. Here we 
            The user wants to change the gui style. 

        PARAMETERS:
        ===========
            :setting_key_gui_style_name         -       In this parameter its a key of the dictionary.
                                                        We need it to access to the value.

            :gui_style_value                    -       Its excepts a value for dictionary. Here we get the catogory of language.

            :setting_key_gui_style_id           -       its a key of the dictionary.

            :combo_id                           -       The current index is saved in this parameter

            :return                             -       Nothing is returned. The statement 'return'
                                                        terminates a function. That makes sure that the
                                                        function is definitely finished.
        '''
        
        logger.info("Fill the value of the dictionary")

        self.configuration_saver_dict.dict_set_general_settings[setting_key_gui_style_name] = gui_style_value
        self.configuration_saver_dict.dict_set_general_settings[setting_key_gui_style_id] = str(combo_id)

        return

    def change_scheme_color(self,
                            setting_key_css_name,
                            css_file_value,
                            setting_key_scheme_color_id,
                            combo_id):
        '''
        NOTICE:
        =======
            This method access to a dictionary. Here we 
            The user wants to change the scheme color. 

        PARAMETERS:
        ===========
            :setting_key_css_name           -       In this parameter its a key of the dictionary.
                                                    We need it to access to the value.

            :css_file_value                 -       Its excepts a value for dictionary. Here we get the name of css file.

            :setting_key_scheme_color_id    -       its a key of the dictionary.

            :combo_id                       -       The current index is saved in this parameter

            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        
        logger.info("Fill the value of the dictionary")

        self.configuration_saver_dict.dict_set_general_settings[setting_key_css_name] = css_file_value
        self.configuration_saver_dict.dict_set_general_settings[setting_key_scheme_color_id] = str(combo_id)

        return

    def show_tool_tip_text(self, text):
        '''
        NOTICE:
        =======
            This method is used to show the tip ballroon and to follow the cursor. 

        PARAMETERS:
        ===========
            :text       -       We need a given text to put it in the tool tip.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        
        logger.info("Fill the value of the dictionary")

        QToolTip.showText(QCursor.pos(), unicode(text))

        return

    def resizeEvent(self,resizeEvent):
        pass

    def closeEvent(self, event):
        pass

    def close_form(self):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_setting.close()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()
    program_info = info_app()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()

    if not USER_INI_FILE_PATH:
        print "You entered nothing!"
    else:
        dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

        get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


        LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

        result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

        LOG_FILE_FOLDER_PATH_NEW = None

        if not result_suffix == True:
            LOG_FILE_FOLDER_PATH = result_suffix

        else:
            pass

        LOG_FILE_PATH = os.path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
        configure_logging(LOG_FILE_PATH)

        image_path_collection = ImagePathnCollection()
        default_ini_file = DefaultINIFile()
        close_application = ""
        app = QApplication(sys.argv)

        translator = QTranslator()
        path = ":/translators/german_sie.qm"
        translator.load(path)
        app.installTranslator(translator)

        empty_widget = None

        window = Setting_Window(dict_custom_config,
                               empty_widget,
                               image_path_collection,
                               default_ini_file,
                               empty_widget,
                               None)

        #window.resize(600, 400)
        window.showMaximized()
        sys.exit(app.exec_())