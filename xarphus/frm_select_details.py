#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
from time import sleep
import inspect

from inspect import currentframe

from PyQt4.QtCore import QFile, Qt, QString, QThread, pyqtSignal
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QTreeWidgetItem

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class Select_Details_Window(QWidget):
    def __init__(self,
                 custom_logger,
                 list_items,
                 parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/select_details.ui")

        self.list_items = list_items
        self.custom_logger = custom_logger

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created successfully - (" + __name__ + ")")

        self.load_ui_file(UI_PATH)
        self.init_PushButton()
        self.set_tree_widget_header_()
        self.init_tree_widget()
        self.on_start_thread()
#        self.fill_tree_widget()


    def load_ui_file(self, UI_PATH):
        '''
        :return:    Its returns nothing, the statement 'return'
                    terminates a function. That makes sure that the
                    function is definitely finished.
        '''

        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        try:
            self.custom_logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")
            UI_PATH.open(QFile.ReadOnly)
            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is opened - (" + __name__ + ")")
            self.custom_logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + ") - (" + __name__ + ")")

            self.ui_select_details = loadUi(UI_PATH, self)

            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")

        finally:
            self.custom_logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded - (" + __name__ + ")")

            UI_PATH.close()

            self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed - (" + __name__ + ")")


        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def get_class_name(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + __name__ + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called - (" + __name__ + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + __name__ + ")")

        return self.__class__.__name__

    def init_PushButton(self):
        #self.myThread.start()
        #self.ui_select_details.pushButton_ok.clicked.connect(None)
        self.ui_select_details.pushButton_close.clicked.connect(self.close_form)

    def init_tree_widget(self):
        pass
        #self.ui_select_details.treeWidget_details.itemChanged.connect(self.handleItemChanged)

    def set_tree_widget_header_(self):
        self.ui_select_details.treeWidget_details.setHeaderHidden(True)

    def on_start_thread(self):
        self.ui_select_details.treeWidget_details.clear()
        self.ui_select_details.label_counter.clear()
        self.work_thread = WorkThread(self.list_items)
        self.work_thread.notify_progress.connect(self.fill_tree_widget)
        self.work_thread.fire_label.connect(self.on_label)

        self.work_thread.start()

    def on_label(self, i):
        self.label.setText("Anzahl: {}".format(i))

    def getTreePath(self, item):
        # We create a empty list
        path = []
        # Do while there is not none, when there is none
        # then stopp immediately
        while item is not None:
            # In while corpus, we a every item,
            # that is converted in string, and now we
            # only want first text
            path.append(str(item.text(0)))
            item = item.parent()

            # The join()-method converts
            # list in string
        return ''.join(path)

    def handleItemChanged(self, item, column):
        path = self.getTreePath(item)
        if item.checkState(0) == Qt.Checked:
            print('%s: Checked' % path)
        else:
            print('%s: UnChecked' % path)

    def fill_tree_widget(self, item):

        parent = QTreeWidgetItem(self.ui_select_details.treeWidget_details)
        parent.setText(0, item)
        parent.setCheckState(0, Qt.Unchecked)
        parent.setFlags(parent.flags() | Qt.ItemIsTristate | Qt.ItemIsUserCheckable)

    def close_form(self):
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
        else:
            self.close()

        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")

class WorkThread(QThread):
    notify_progress = pyqtSignal(object)
    fire_label = pyqtSignal(str)
    def __init__(self, list_items, parent=None):
        QThread.__init__(self, parent)

        self.list_items = list_items

        self.total = len(list_items)
        self.count = 0

        self.percentage = (self.total/100*0.65)

        self.a = self.percentage

        self.stopp = 1

    def create_items(self):
         for elem in self.list_items:
             yield elem

    def run(self):
        for i in self.create_items():
            self.notify_progress.emit(unicode(i))
            self.count += 1
            self.fire_label.emit(str(self.count))

            if self.count == self.percentage:
                self.percentage += self.a
                sleep(0.5)

            if self.stopp == 0:
                self.stopp = 1
                break

    def stop(self):
        self.stopp = 0

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    '''
    Private modules are imported.
    '''
    from core.manange_logging import set_custom_logger, create_log_file
    from info import info_app

    from core.manage_time import get_time_now

    ## Import compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    program_info = info_app()
    custom_logger = set_custom_logger(program_info)

    FILE_NAME = "frm_select_details.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    create_log_file(LOG_FILE_PATH, program_info)

    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Select_Details_Window(custom_logger,
                          None)
    window.show()
    sys.exit(app.exec_())