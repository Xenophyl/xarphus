#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import webbrowser
import inspect

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QDialog, QMdiSubWindow
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile
from PyQt4 import QtCore

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################
'''
Private modules are imported.
'''
from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
#self.path_mdi_form = os.path.abspath(".")
#self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

class Series_Season_Episode_Window(QDialog):
    def __init__(self,
                 save_config,
                 info_app,
                 get_qt_version,
                 set_custom_logger,
                 func,
                 up_to_date_it,
                 parent):
        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/serie_season_episode.ui")

        self.info_app = info_app

        self.custom_logger = set_custom_logger

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created successfully - (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END CLASS (" + Series_Season_Episode_Window.__name__ + ") - (" + FILE_NAME + ")")

        self.set_get_settings = save_config
        self.get_app_info = info_app
        self.get_qt_version = get_qt_version
        self.up_to_date_it = up_to_date_it
        self.func = func

        self.custom_logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        UI_PATH.open(QFile.ReadOnly)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened successfully - (" +  FILE_NAME +")")
        self.custom_logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        self.ui_series_season_episode = loadUi(UI_PATH, self)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded successfully - (" +  FILE_NAME +")")

        UI_PATH.close()

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed successfully - (" +  FILE_NAME +")")


        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_series_season_episode.setWindowFlags(flags)

        #self.init_gui()
        self.init_actions_button()
        self.create_actions_label()


    # def init_gui(self):
    #     self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
    #         3] + ") - (" + FILE_NAME + ")")
    #     self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")
    #
    #     self.ui_about.setWindowModality(Qt.ApplicationModal)
    #
    #     '''
    #     :return: None
    #     '''
    #     self.ui_about.label_product_name.setText(self.get_app_info.dict_info["product_name"] + " (" + self.get_app_info.dict_info["product_version"] + " - " + self.get_app_info.dict_info["product_built"] + ")")
    #
    #     self.ui_about.label_copyright.setText(self.get_app_info.dict_info["product_copyright"] +
    #                                             self.get_app_info.dict_info["product_copyright_characters"] +
    #                                             " " +
    #                                             self.get_app_info.dict_info["product_year_from"] +
    #                                             " - " +
    #                                             self.get_app_info.dict_info["product_year_till"] +
    #                                             " " +
    #                                             self.get_app_info.dict_info["product_by"] +
    #                                             " " +
    #                                             self.get_app_info.dict_info["product_author"])
    #
    #     self.ui_about.label_gui.setText(self.tr("User interface (QT): ") + str(self.get_qt_version()))
    #
    #     self.ui_about.textEdit_philosophy.setText(self.get_app_info.dict_info["product_name"] +
    #                                             self.tr(" verfolgt einen Grundsatz: Ordnung ist das halbe Leben (zitiert von Heinrich Boell). " +
    #                                             "Dieses Programm erhebt den Anspruch FAST alles zu verwalten, was man "
    #                                             "verwalten kann. "
    #                                             "In dieser Version lassen sich folgende Medien verwalten: ") +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["movie"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["books"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["musics"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["coins"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["contacts"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["notes"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["videogames"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["quotations"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["stamps"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["idioms"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info["persons"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info ["articles"] +
    #                                             "<br>" +
    #                                             "- " + self.get_app_info.dict_manager_info ["miscellany"] +
    #                                             ""
    #                                             "<br><br> " +
    #                                             self.tr("Darueber hinaus versteht sich dieses Programm in erster Linie als reine Verwaltungssoftware. "
    #                                             "Sie bekommen die komplette Kontrolle ueber die Verwaltung. "
    #                                             "Das bedeutet, dass Sie die Daten manuell verwalten. "
    #                                             "Jedoch ist das Pogramm lernfaehig. "
    #                                             "Stammdaten werden ein einziges Mal in die Datenbank gepflegt. Je umfangreicher "
    #                                             "die Stammdaten, desto schneller lassen sich die (neuen) Medien und Daten verwalten. "
    #                                             "Durch die strikte manuelle Pflege behalten Sie den ueberblick."))
    #
    #     self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
    #     self.custom_logger.info(
    #         "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")
    #
    #     return

    def get_class_name(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return self.__class__.__name__

    def open_url_homepage(self, link):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        # open in a new tab, if possible
        new = 2
        # open a public URL, in this case, the webbrowser docs
        webbrowser.open(link, new=new)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def create_actions_label(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        #homepage_link = self.get_app_info.dict_info["product_site"]
        #link_tag = "Internetseite:" + " <a href='%s'>" % homepage_link + "www.meine_domain.de" + "</a>"
#        self.ui_about.label_homepage_url.setText(link_tag)
#        self.ui_about.label_homepage_url.linkActivated.connect(self.open_url_homepage)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return

    def init_actions_button(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.ui_series_season_episode.pushButton_close.clicked.connect(self.close_form)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully -  (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        return


    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_it()
            self.func()

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    '''
    Private modules are imported.
    '''
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manange_logging import set_custom_logger, create_log_file
    from core.config import ConfigurationSaver
    from info import info_app
    from core.qt_info import get_version_qt
    from core.manage_time import get_time_now

    ## Import compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    dict_custom_config = ConfigurationSaver()
    program_info = info_app()
    set_custom_logger = set_custom_logger(program_info)

    FILE_NAME = "frm_about.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    create_log_file(LOG_FILE_PATH, program_info)

    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Series_Season_Episode_Window(dict_custom_config,
                                            program_info,
                                            get_version_qt,
                                            set_custom_logger,
                                            None)
    window.showMaximized()
    sys.exit(app.exec_())
