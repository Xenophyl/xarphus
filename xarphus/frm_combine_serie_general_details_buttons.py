#-*- coding:latin1 -*-

FILE_NAME = __name__

from os import path
import sys

from PyQt4.QtCore import Qt, QRegExp

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QHBoxLayout, QIcon, QFileDialog, \
    QPixmap, QValidator, QRegExpValidator, QTabWidget, QGridLayout, QTextEdit, QToolButton, QSpacerItem, \
    QSizePolicy, QLabel, QPushButton, QFrame, QTreeWidget

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

try:
    '''
        Load forms.
    '''
    from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
    from xarphus.frm_serie_general import SerieGeneral_Window
    from xarphus.frm_serie_general_details import SerieGeneralDetails_Window
    from xarphus.frm_serie_general_cover import MovieGeneralCover_Window

    '''
        Load core
    '''
    from xarphus.core.manage_calculation import calculate_cover_poster
    from xarphus.core.manage_folder import is_folder_exists

except ImportError:

    '''
        Load forms
    '''
    from frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
    from frm_serie_general import SerieGeneral_Window
    from frm_serie_general_details import SerieGeneralDetails_Window

    '''
        Load core
    '''
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class CombineSerieGeneralDetailsButtons_Window(QWidget):
    def __init__(self,
                 form_condition=None,
                 movie_detail=None,
                 configuration_saver_dict=None,
                 image_path_collection=None,
                 create_series_season_episode=None,
                 create_cover_viewer=None,
                 form_title=None,
                 serie_id=None,
                 parent=None):

        QWidget.__init__(self, parent)

        self.create_cover_viewer = create_cover_viewer
        self.form_condition = form_condition
        self.movie_detail = movie_detail
        self.form_title = form_title
        self.serie_id = serie_id

        self.configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = image_path_collection

        self.is_focused = None

        self.create_series_season_episode = create_series_season_episode

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))


        self.film_title_name = ""

        # There are all paths of images or icons.
        # Notice: All the paths of icons or images are used repeatedly.
        # That's the reason why the paths were outsourced.

        # activate all necessary functions
        self.init_ui_series()
        self.init_clicked_signal_push_buttons()
        self.init_textChanged_signal_line_edit()
        self.init_series_detail_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def create_combined_form(self):
        '''
            NOTICE:
            =======
            A new window is created from several subclasses.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Try to create an instance of SerieGeneral_Window()")

        self.serie_general = SerieGeneral_Window(self.configuration_saver_dict,
                                                self)

        logger.info("Try to create an instance of SerieGeneralDetails_Window()")

        self.serie_general_details = SerieGeneralDetails_Window(self)

        logger.info("Try to create an instance of PushbuttonsClosePrintDeleteSave_Window()")

        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self)

        logger.info("Try to create an instance of QVBoxLayout()")

        v_layout = QVBoxLayout()

        logger.info("The instance of the class of  QVBoxLayout() is created successfully")

        logger.info("Try to add all the given widgets to QVBoxLayout()")

        v_layout.addWidget(self.serie_general)
        v_layout.addWidget(self.serie_general_details)
        v_layout.addWidget(self.pushbutton_close_print_delete_save)

        logger.info("All given widgets are added to to QVBoxLayout() successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        textEdit_view = QTextEdit(self)

        logger.info("The instance of the class of  QTextEdit() is created successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        logger.info("Try to set the instance of QTextEdit() in read-only mode (True)")

        textEdit_view.setReadOnly(True)

        logger.info("The instance of QTextEdit is set in read-only mode (True) successfully")

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_view = QGridLayout(self)

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_view.addWidget(textEdit_view, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        logger.info("Try to create an instance of QTabWidget()")

        tab_widget = QTabWidget(self)

        logger.info("The instance of QTabWidget() successfully")

        logger.info("Try to create an instances of QWidget()")

        tab1 = QWidget(self)
        tab2 = QWidget(self)

        logger.info("The instances of QWidget() successfully")

        logger.info("Try to set the layout manager for given widget to QVBoxLayout()")

        tab1.setLayout(v_layout)

        logger.info("The given widget is set to QVBoxLayout() successfully")

        logger.info("Try to set the layout manager for given widget to QGridLayout()")

        tab2.setLayout(gridLayout_view)

        logger.info("The given widget is set to QGridLayout() successfully")

        logger.info("Try to add tabs with the given page and label to QTabWidget()")

        tab_widget.addTab(tab1, self.tr("Bearbeitung"))
        tab_widget.addTab(tab2, self.tr("Gesamtübersicht"))

        logger.info("All given tabs with the given page and label are added to QTabWidget() successfully")

        logger.info("Try to create an instance of QGridLayout()")

        self.remove_tab(tab_widget)

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_1 = QGridLayout(self)

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_1.addWidget(tab_widget, 0, 0)

        logger.info("All given widgets are added to to QGridLayout() successfully")

        return

    def set_visible_tabWidget_header(self, instance_name_tabWidget_object="",
                                           widghet_n = None,
                                           bool_value=None):
        '''
            NOTICE:
            =======
            In this method we want to get all QTreeWidget that are children of parentWidget.
            

            PARAMETERS:
            ===========
            :instance_name_tree_Widget_object   -   The given argumnent contains instance of a QTabWidget.
                                                    In this case, QTabWidget is a parentWidget.

            :widghet_n                          -   Here, we ecpect an integer. The integer is an index. 
                                                    We need the index, because the QTabWidget has a method to 
                                                    access any tab by its index, sensibly called widget(index).
                                                    Therefore, if you want to access the n-th widget,
                                                    you can get it by calling self.tabWidget.widget(n).

            :bool_value                         -   In this keyword argument we want a special value: boolean value.


            :return                             -   Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''

        logger.info("For loop starts")

        for tree_widget_object in instance_name_tabWidget_object.widget(int(widghet_n)).findChildren(QTreeWidget):
            tree_widget_object.setHeaderHidden(bool_value)

        return

    def set_font_color_label(self):
        '''
            NOTICE:
            =======
            This method is used to set color of text of a QLabel()-object.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Set color of text of QLabel")

        labels = self.pushbutton_close_print_delete_save.findChildren(QLabel)
        
        for label in labels:
            label.setStyleSheet("QLabel {color : red; }")

        return

    def hide_widget(self):
        '''
            NOTICE:
            =======
            This method is used to hide a widget.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Hide the QFrame()object")

        frames = self.pushbutton_close_print_delete_save.findChildren(QFrame)

        for frame in frames:
            frame.close()

        return

    def remove_tab(self, tab_obj):
        '''
            NOTICE:
            =======
            This method removes the tab at position index from this stack of widgets. 
            The page widget itself is not deleted..

            PARAMETERS:
            ===========
            :tab_obj    -   The given argumnent says which tabs at the position 
                            should be removed.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Revmove the tab with index 1")

        '''
                No existing movie was selected. The user wants to add a new
                movie to database. That is the reason why the user doesn't need the
                the tab, that shows the overview like a print report.
        '''
        tab_obj.removeTab(1)

        return

    def check_and_set_the_Window_title(self):
        '''
            NOTICE:
            =======
            This method removes the tab at position index from this stack of widgets. 
            The page widget itself is not deleted..

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Check and set the title of the window")

        # '''
        #     First we check the current index of the stack widget.
        #     Zero means, the user wants to see the page where he 
        #     can input general information of the movie, like: year, title, translated title,
        #     genre etc.
        # '''
        # if self.stack_current_index == 0:

        #     '''
        #         Well, we take a look if there is an existing movie data or
        #         the user is about to add a new movoe data to database. None says,
        #         there isn't an existing movie.
        #     '''            
        #     if self.film_id is None:

        #         '''
        #             We know there isn't an existing movie. So we set the title of the window.
        #         '''
        #         self.setWindowTitle(self.tr(u"Add movie {}".format(self.film_title_name)))

        #         '''
        #             Here we check if the variable named (self.film_id) contains a integer or int-like attributes. 
        #             In this case we also want to check for long.
        #         '''
        #     elif isinstance(self.film_id, (long, int)):

        #         '''
        #             Exactly, the variable above contains a integer. That means, there is an existing
        #             movie data. So we just take the film title from database that ist saved in the variable
        #             named (self.film_title_name)
        #         '''
        #         self.setWindowTitle(self.film_title_name)

        #     '''
        #         Maybe the user wants to add a edition or version of a movie. All this
        #         information are on the second page. In Paython-words: Second means number 1.
        #     '''
        # elif self.stack_current_index == 1:

        #     '''
        #         We don't know if the user wants to add a edition or version of the movie.
        #         Lets take a look. Number 1 says, the user wants to add version of the movie.
        #     '''

        #     '''
        #         Well, we take a look if there is an existing movie data or
        #         the user is about to add a new movoe data to database. None says,
        #         there isn't an existing movie.
        #     '''            
        #     if self.film_id is None:

        #         self.setWindowTitle(self.tr(u"Add a new version of the movie: {movie_title}".format(movie_title=self.film_title_name)))

        #     elif isinstance(self.film_id, (long, int)):

        #         self.setWindowTitle(self.tr(u"Display the version of the movie: {movie_title}".format(movie_title=self.film_title_name)))

        # elif self.stack_current_index == 2:

        #     '''
        #         We don't know if the user wants to add a edition or version of the movie.
        #         Lets take a look. Number 1 says, the user wants to add version of the movie.
        #     '''

        #     '''
        #         Well, we take a look if there is an existing movie data or
        #         the user is about to add a new movoe data to database. None says,
        #         there isn't an existing movie.
        #     '''            
        #     if self.film_id is None:

        #         self.setWindowTitle(self.tr(u"Add a new edition of the movie: {movie_title}".format(movie_title=self.film_title_name)))

        #     elif isinstance(self.film_id, (long, int)):

        #         self.setWindowTitle(self.tr(u"Display the edition of the movie: {movie_title}".format(movie_title=self.film_title_name)))

        
        return


    def init_series_detail_ui(self):
        '''
            NOTICE:
            =======
            Well, the user is about to add a new version or edition of a movie.
            So, this method is used to take a look if the user wants to add a new edition 
            or a version of the already existing or newly added movie.

            PARAMETERS:
            ===========
            :stack_page_id              -   With the given integer we want to set the current index
                                            of a stack widget.

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        '''
            Let us take a look, what the user want. This variable (film_id) is obtained
            by the CombineMovieGeneralDetailsButtons_Window()-class. In this variable you get
            the id of a film or the value None. None means, there isn't a film to show. 
            If there is None the user is about to add a new movie andf then
            he maybe wants to add a new edition or version of the already 
            newly added movie. 
        '''

        self.check_and_set_the_Window_title()

        if not self.serie_id is None:
            '''
                User opens the form with a existing film.
            '''

            self.set_font_color_label()

        return

    def init_ui_series(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.create_combined_form()
        self.set_window_icon()
        

        if self.serie_id == None:

            self.hide_widget()
            self.set_window_title(self.form_title)
            self.set_enabled_push_button(except_push_button=[self.pushbutton_close_print_delete_save.pushButton_close])

        else:

            self.set_font_color_label()
            self.set_window_title(title_text = self.tr("Film..."))

        return

        #elf.pushbutton_close_print_delete_save.pushButton_save.setEnabled(False)

        return

    def set_enabled_push_button(self, except_push_button=None):
        '''
            NOTICE:
            =======
            This method sets all QPushButton of the specified parentWidget. 

            PARAMETERS:
            ===========
            :except_push_button     -   This keyword argument gets a psuhbutton-widget
                                        that is not to be deactivated. As default there is 
                                        the keyword None.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set enabled for all push buttons")

        for push_button in self.pushbutton_close_print_delete_save.findChildren(QPushButton):
            push_button.setEnabled(False)

        if not except_push_button == None:

            for button_push in except_push_button:
                button_push.setEnabled(True)

        return

    def set_window_title(self,
                        title_text=None):
        '''
            NOTICE:
            =======
            This method changes the title of the window.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the title for the window.")

        self.setWindowTitle(title_text)

        return

    def set_window_icon(self):
        '''
            NOTICE:
            =======
            This method puts the icon on the taskbar next to the windowtitle.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the icon for the window")

        self.setWindowIcon(QIcon(self.image_path_collection.movie_icon))

        return


    def init_textChanged_signal_line_edit(self):
        '''
            NOTICE:
            =======
            This method is used to set signal and slots for QLineEdit()-objects. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the signal and slots for QLineEdit()-object(s)")

        self.serie_general.lineEdit_title_original.textChanged.connect(lambda: self.lineEdit_title_translated_change(
                                                                                                self.serie_general.lineEdit_title_original.text(),
                                                                                                self.serie_general.lineEdit_title_original, 1))

        self.serie_general.lineEdit_title_translated.textChanged.connect(lambda: self.lineEdit_title_translated_change(
                                                                                                self.serie_general.lineEdit_title_translated.text(),
                                                                                                self.serie_general.lineEdit_title_translated, 0))

        return

    def show_movie_detail(self):
        '''
        Let us take a look, what the user want. This variable (choice_function) is obtained
        by the CombineMovieGeneralDetailsButtons_Window()-class. In this variable you only find
        0 or 1. Zero means, the user want to add a new movie data and 1 means,
        the user wants to look at the existing film data.
        '''
        if self.form_condition == 0:

            form_title = self.tr("Eine Fassung vom Film ") + " (" + unicode(self.film_title_name) + ") " + self.tr(("hinzufügen"))

            '''
            NOTICE:
            -------
            "movie_detail" is an obtained argument in this CombineMovieGeneralDetailsButtons_Window()-class.
            This argument contains a function object that takes three arguments.

            SEQUENCE:
            ---------
            01 argument:    The variable (choice_function) is passed as an argument.
                            This variable is obtained by the CombineMovieGeneralDetailsButtons_Window()-class.
                            In the variable you only find 0 or 1. Zero means, the user want to add a new
                            movie data and 1 means, the user wants to look at the existing film data.

            02 argument:    The user wants to look at the existing film data, so the window gets another
                            title. The title is saved in the above variable (form_title).

            03 argument:    The integer says about the type of details. Here, the zero says that the user
                            wants add a new film version. If there were an integer 1 that would means,
                            the user wants add a new film edition.

            Once the function has been called the form (movie_detail) is generated by frm_mdi.
            '''
            self.movie_detail(self.form_condition, form_title, 0)

        if self.form_condition == 1:
            '''
            The user wants to look at the existing film data. So the winsow gets another title.
            '''
            form_title = self.tr("Die Fassung vom Film ") + " (" + unicode(self.film_title_name) + ") " + self.tr(("anzeigen"))

            self.movie_detail(self.form_condition, form_title, 1)

            return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object(s)")

        self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close)

        return


    def show_cover(self, img_path, cover_site=None):
        if cover_site=="front":
            self.serie_general.label_cover_front.setPixmap(QPixmap(""))
            self.serie_general.label_cover_front.setPixmap(QPixmap(img_path))
        if cover_site=="back":
            self.serie_general.label_cover_back.setPixmap(QPixmap(""))
            self.serie_general.label_cover_back.setPixmap(QPixmap(img_path))
    #--------------------------------------------------------------------------------------
    def del_cover(self, cover_site=None):
        if cover_site=="front":
            self.serie_general.label_cover_front.setPixmap(QPixmap(""))
            self.file_path_front_cover = ""
            self.serie_general.pushButton_zoom_front.setEnabled(False)
            self.serie_general.pushButton_del_front_cover.setEnabled(False)
            self.serie_general.label_cover_front.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))
        if cover_site=="back":
            self.serie_general.label_cover_back.setPixmap(QPixmap(""))
            self.file_path_front_cover = ""
            self.serie_general.pushButton_zoom_back.setEnabled(False)
            self.serie_general.pushButton_del_back_cover.setEnabled(False)
            self.serie_general.label_cover_back.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))

    def resizeEvent(self,resizeEvent):
        pass

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")
        
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return
