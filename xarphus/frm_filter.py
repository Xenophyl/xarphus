#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path
from functools import partial
from sys import argv, exit, exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

try: #  Import all modules from...
    #   ... core
    #from xarphus.core.manage_data_manipulation_master_data import select_all
    from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel
    from xarphus.subclass_master_data_load_data_item import TaskDataManipulation
    from xarphus.core.managed_pyqt4_threads import TaskProxyModel
    from xarphus.core.manage_qt import ProgressBarText
    from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from xarphus.collection_paths_image import ImagePathnCollection
    from xarphus.core.managed_image_delegate import Image_Delegate
    from xarphus.core.build_closure import chain_funcs
    from xarphus.core.managed_drag_drop import ManagedDragDrop
    from xarphus.frm_waiting import Waiting_Window
except ImportError: #   When the module is started as a program
    #   Import all compiled files
    from gui import ui_rc
    #   Import all modules from core
    #from core.manage_data_manipulation_master_data import select_all
    from core.manage_qt import ProgressBarText
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from core.custom_sort_filter_model import MySortFilterProxyModel
    from core.managed_image_delegate import Image_Delegate
    #from customsortfiltermodel import MySortFilterProxyModel
    from subclass_master_data_load_data_item import TaskDataManipulation
    from managed_pyqt4_threads import TaskProxyModel
    from core.config import ConfigurationSaverDict
    from core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from collection_paths_image import ImagePathnCollection
    from core.build_closure import chain_funcs
    from core.managed_drag_drop import ManagedDragDrop
    from frm_waiting import Waiting_Window
    from images import images_rc
    from localization_files import translate_files_rc

from PyQt4.QtCore import QFile, Qt, QTranslator, QThread, pyqtSlot, QSize, pyqtSignal, QTimer, \
                         QRegExp, QString
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QApplication, QStandardItem, QStandardItemModel, \
                        QIcon, QPixmap, QLabel, QImage, QTreeView, QAction, QMenu, QMessageBox, \
                        QPushButton

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")
class Filter_Window(QWidget):

    interrupt_working_thread_signal = pyqtSignal()
    open_person_profile_signal = pyqtSignal(object)

    def __init__(self,
                 data_id = None,
                 configuration_saver_dict = None,
                 image_path_collection = None,
                 standard_item_model = None,
                 general_proxy_model = None,
                 parent = None):

        QWidget.__init__(self, parent = None)

        '''
        ATTRIBUTES
        ==========
        :parent      -  The class supports passing a parent arguments
                        to its base class. Its more felxible. By default,
                        the parent is set to None.

        '''

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))


        '''
            Store constance
        '''
        self.LIST_HORIZONTAL_HEADER_GENERAL_PERSON = [self.tr("ID"), self.tr("Photo"), self.tr("First Name"), self.tr("Last Name")]

        ''' Connect the pyqtSignal(s) '''
                #   Define pyqtSignal()
        parent.send_current_scoped_session_master_data_signal.connect(self.save_scoped_session)

        ''' Create all attributes '''
        self._configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = image_path_collection
        self._data_id = data_id

        UI_PATH = QFile(":/ui_file/filter.ui")

        self._proxy_model = None
        self.set_proxy_model()

        self._standard_item_model = standard_item_model
        self._general_proxy_model = general_proxy_model

        self._list_threads = []
        self._filter_category = None
        self._selected_be_edited_id = None
        self._current_selected_id = None
        self.flags = None
        self.messagebox_called = False

        self._scoped_session = None

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_filter = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):
        '''
            NOTE:
            ======
            This method initializes the user interface of master_data-window.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        self.flags = flag_dialog_titlehint_customizewondowhint()


        self.set_hide_widget(widget = self.ui_filter.frame_result, bool_value = False)
        #self.init_timer()
        self.init_clicked_signal_push_buttons()
        self.init_tree_view()
        self.init_selectionChanged_signal_treeview()
        self.init_fill_combo_box()
        self.init_combo_box()
        self.init_timer()
        self.init_returnPressed_signal_line_edit()

        #self.set_header_hidden(children_widget = QTreeView)

        return

    def save_scoped_session(self,
                            scoped_session):
        print ""
        print "Filter_Window save_scoped_session is called successfully"
        print "Filter_Window in save_scoped_session / scoped_session: -> \n {}".format(scoped_session)
        self._scoped_session = scoped_session

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method is used to set the signal and slots for QpuschButton()-object(s).
            That menas, it is initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")
        self.ui_filter.pushButton_close.clicked.connect(self.close)

        self.ui_filter.pushButton_filter.clicked.connect(
            lambda:
            self.fetch_all(
                scoped_session = self._scoped_session,
                configuration_saver_dict = self._configuration_saver_dict,
                hide_column = True,
                general_standard_item_model = self._standard_item_model,
                resize_column = 1,
                populate_timer_interval = 10,
                bulk_the_list = False,
                general_proxy_model = self._general_proxy_model,
                certain_proxy_model = self._general_proxy_model.proxy_model_filter_person_profile,
                list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_PERSON,
                work_mode = 'fetch_all',
                certain_standard_item_model = self._standard_item_model.standard_item_model_person_profile,
                widget_label = self.ui_filter.label_result,
                widget_tree_view = self.ui_filter.treeView_person,
                show_messagebox = True,
                show_image = True,
                category = 'person_profile')) #person_profile / general_country

        self.ui_filter.pushButton_11.clicked.connect(
            lambda:
            self.set_header_data(
                model = self._standard_item_model.standard_item_model_person_profile,
                list_header_data = self.LIST_HORIZONTAL_HEADER_GENERAL_PERSON))
        return

    def init_fill_combo_box(self):

        list_item_comboBox_category = [
                                       self.tr('Select'),
                                       self.tr('--- Film ---'),
                                       self.tr('Film'),
                                       self.tr('Serie'),
                                       self.tr('Stamp'),
                                       self.tr('Wine'),
                                       self.tr('Lending'),
                                       self.tr('Book'),
                                       self.tr('Coin'),
                                       self.tr('Note'),
                                       self.tr('Videp Game'),
                                       self.tr('Music'),
                                       self.tr('Person'),
                                            ]
        for item_comboBox_category in list_item_comboBox_category:

            self.ui_filter.comboBox_category.addItem(item_comboBox_category)

        list_item_comboBox_category_movie = [
                                             self.tr('Select'),
                                             self.tr('Movie'),
                                             self.tr('Serie'),
                                            ]

        for item_comboBox_category_movie in list_item_comboBox_category_movie:

            self.ui_filter.comboBox_category_movie.addItem(item_comboBox_category_movie)

        return

    def init_combo_box(self):
        '''
           SUMMARY
           =======
           Here we initialize all QComboBox()-object(s).

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('initialize all QComboBox()-object(s)')

        self.ui_filter.comboBox_category.currentIndexChanged.connect(self.combo_selection_change)

        self.ui_filter.comboBox_category.model().item(1).setEnabled(False)

        return

    def init_returnPressed_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize returnPressed-Signal for QLineEdit()")

        ###################################################
        #################  Person Profile  ################
        ###################################################
        self.ui_filter.lineEdit_search.returnPressed.connect(lambda:
          self.handle_timer(timer_widget = self.search_delay_timer_film_person_profile,
                            show_wait_message = True,
                            text = self.tr("Searching for records. This should only take a moment.")))

    def handle_timer(self,
                     timer_widget,
                     text,
                     show_wait_message = False):
        '''
           SUMMARY
           =======
           TThis method is used to handle the given QTimer.

           EXTENTED DISCRIPTION
           ====================
           This function not only handles given QTimer, but also
           creates another waiting-window.

           PARAMETERS
           ==========
           :timer_widget (class):       selectionChanged() sends new QItemSelection.

           :text (class):               We get a QString, because the given text will be translated.

           :show_wait_message (bool):   Let us know should we show the wait-window?

           RETURNS
           =======
           :return:                     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Handle the certain QTimer")

        if show_wait_message:
            self.create_wait_window(text = text)

        timer_widget.stop()
        timer_widget.start()

        return

    def create_wait_window(self,
                           text):
        '''
           SUMMARY
           =======
           This method just creates the wait window.

           PARAMETERS
           ==========
           :text (str):    The given text should be shown on the wait window.

           RETURNS
           =======
           :return:         Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create the wait window")

        self.waiting_window = Waiting_Window(text_message = text)
        self.waiting_window.show()

        return

    def init_timer(self):
        '''
           SUMMARY
           =======
           This method initializes all QTimer(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize all QTimer(s)")

        ###################################################
        #################  General Country  ###############
        ###################################################

        self.update_proxy_model_timer = QTimer()
        self.update_proxy_model_timer.setSingleShot(True)
        self.update_proxy_model_timer.setInterval(1)
        self.update_proxy_model_timer.timeout.connect(lambda:
        self.update_proxy_model(
          widget_tree_view = self.ui_filter.treeView_person,
          certain_standard_item_model = self._standard_item_model.standard_item_model_person_profile,
          general_proxy_model = self._general_proxy_model,
          certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
          category = "person_profile"))
        #self.update_proxy_model_timer.start()

        self.search_delay_timer_film_person_profile = QTimer()
        self.search_delay_timer_film_person_profile.setSingleShot(True)
        self.search_delay_timer_film_person_profile.setInterval(1000)
        self.search_delay_timer_film_person_profile.timeout.connect(lambda:
          self.filter_update(line_edit_widget = self.ui_filter.lineEdit_search,
                              certain_proxy_model = self._general_proxy_model.proxy_model_filter_person_profile,
                              column = 2))


        return

    def init_tree_view(self):
        '''
           SUMMARY
           =======
           Here we initialize the QTreeView()-object for all categories.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QTreeView for all categories")

        ###################################################
        #################  Person Profile  ################
        ###################################################
        self.set_header_data(model = self._standard_item_model.standard_item_model_person_profile,
                             list_header_data = self.LIST_HORIZONTAL_HEADER_GENERAL_PERSON)

        person_thumbnail_width = int(self._configuration_saver_dict.dict_set_general_settings['thumbnail_aspect_ratio_width'])

        person_thumbnail_height = int(self._configuration_saver_dict.dict_set_general_settings['thumbnail_aspect_ratio_height'])

        person_delegate = Image_Delegate(img_width = person_thumbnail_width,
                                         img_height = (person_thumbnail_height+4))





        self.ui_filter.treeView_person.sortByColumn(1, Qt.AscendingOrder)


        self.ui_filter.treeView_person.setHeaderHidden(False)
        self.ui_filter.treeView_person.setIconSize(QSize(person_thumbnail_width, person_thumbnail_height)) # w, h
        self.ui_filter.treeView_person.setModel(self._standard_item_model.standard_item_model_person_profile)
        self.ui_filter.treeView_person.setItemDelegate(person_delegate)

        self.ui_filter.treeView_person.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_filter.treeView_person.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_filter.treeView_person,
            pushbutton_object_edit = None,
            pushbutton_object_delete = None:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        #self.ui_master_data.tree_view_general_region.setModel(self._proxy_model)
        #self.ui_filter.treeView_filter.setSortingEnabled(True)
        #self.ui_filter.treeView_filter.sortByColumn(1, Qt.AscendingOrder)

        self.init_doubleClicked_signal_treeview()

        return

    def init_doubleClicked_signal_treeview(self):
        self.ui_filter.treeView_person.doubleClicked.connect(self.test_double_clicked)

    def test_double_clicked(self, i):
        print "test_double_clicked", self._current_selected_id

        if self._current_selected_id:
            self.open_person_profile_signal.emit(self._current_selected_id)

    def set_focus(self,
                  widget_object,
                  bool_value = True):

        '''
           SUMMARY
           =======
           This method is used to set the focus on given QLineEdit()-object.

           EXTENTED DISCRIPTION
           ====================
           This method is necessary, because the setFocus()-function is used
           in several places.

           PARAMETERS
           ==========
           :widget_object (Qt-object):  We need the QLineEdit()-object to set the focus in.

           :bool_value (bool):          By default, its on True, which menas, the given Qt-object
                                        will be visible.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the focus on given QLineEdit()-object.")

        if not widget_object is None:
            widget_object.setFocus(bool_value)

        return

    def init_selectionChanged_signal_treeview(self):
        '''
            NOTE:
            ======
            This method initializes the user interface of master_data-window.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        '''
            #######################################################
            ###################  Category Person  #################
            #######################################################
        '''

        self.ui_filter.treeView_person.selectionModel().selectionChanged.connect(
            lambda new_index, old_index:
            self.handle_selection_changed(new_index=new_index, old_index=old_index))

    def handle_selection_changed(self, new_index=None, old_index=None, edit_line_obj=None):
        '''
            NOTE:
            ======
            This method gets two qItemSelection, the new selection and the old one.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        #tuples_list_push_push_button = [(self.ui_master_data.pushButton_delete_general_country, True)]

        try: #if qItemSelection
            self._selected_be_edited_id = new_index.indexes()[0].data()
            self._current_selected_id = new_index.indexes()[0].data()

            print "selected_be_edited_id", self._selected_be_edited_id
            print "current_selected_id", self._current_selected_id

            #edit_line_obj.setText(new_index.indexes()[1].data())

            #if self.selected_be_edited_id:


                #self.set_enabled_widget(two_pair_tuples_in_list_widget_boolean=tuples_list_push_push_button)

        except Exception as er: #if qModelIndex
            pass

        return

    def open_context_menu(self, position,
                                tree_view_obj=None,
                                pushbutton_object_edit=None,
                                pushbutton_object_delete=None):

        indexes = tree_view_obj.selectedIndexes()

        if len(indexes) > 0:

            delete_action = QAction(QIcon(self.image_path_collection.delete_default), self.tr("Delete"), self)
            #delete_action.triggered.connect(pushbutton_object_delete.click)

            edit_action = QAction(QIcon(self.image_path_collection.edit_default), self.tr("View / Edit"), self)
            #edit_action.triggered.connect(pushbutton_object_edit.click)


            menu = QMenu()
            menu.addAction(edit_action)
            menu.addAction(delete_action)

            menu.exec_(tree_view_obj.viewport().mapToGlobal(position))

        return

    def find_children(self, widget = None):

        for widget_obj in self.findChildren(widget):

            yield widget_obj

    def set_header_hidden(self,
                          widget = None,
                          children_widget = None,
                          bool = False):

        if not children_widget is None:

            for widget_obj in self.find_children(widget = children_widget):

                widget_obj.setHeaderHidden(bool)



    def set_filter_category(self, category = None):
        '''
            NOTICE:
            =======
            Create an empty model for the TreeViews' data

            This method constructs a new item model that initially
            has rows and columns, and that has the given parent.

            QTreeView needs a model to manage its data.

            PARAMETERS:
            ===========
            :category   -   Given category, in which part you want
                            to filtering.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        return

    def close_wait_message(self):
        '''
           SUMMARY
           =======
           This method just closes the created wait window.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("closed the created wait window")

        try:

            self.waiting_window.close()

        except AttributeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def display_stack_widget(self, stack_widget = None, index = 0):

        # use a stacked layout to view only one of two combo box at a time

        stack_widget.setCurrentIndex(index)

        return

    def set_hide_widget(self, widget = None, bool_value = None):

        widget.setVisible(bool_value)

        return


    def combo_selection_change(self, index):
        print "index", index

        dict_store_functions = {

                            0: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            1: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            2: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            3: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            4: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            5: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            6: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            7: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            8: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            9: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            10: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index)),
                            11: chain_funcs(partial(self.set_filter_category, category = 'general_person'),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_category,
                                        index = index),
                                partial(self.display_stack_widget, stack_widget = self.ui_filter.stackedWidget_tree_view,
                                        index = index)),
                                }

        dict_store_functions[index]()

        return

    def filter_update(self,
                      line_edit_widget = None,
                      certain_proxy_model = None,
                      column = None):
        '''
            NOTICE:
            =======
            This method updates the filter of the given model.

            PARAMETERS:
            ===========
            :string_text     -   we except a text für updating the filter.

            :certain_proxy_model    -   We need the model where we should update the filter.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info('Update the filter of the given model')
        print ""
        print "Filter_Window in filter_update / line_edit_widget.objectName(): --> \n {}".format(line_edit_widget.objectName())
        print "Filter_Window in filter_update / line_edit_widget.text(): --> \n {}", line_edit_widget.text()
        print "Filter_Window in filter_update / certain_proxy_model.objectName(): --> \n {}".format(certain_proxy_model.objectName())
        print "Filter_Window in filter_update / certain_proxy_model.rowCount() before search: --> \n {}".format(certain_proxy_model.rowCount())

        print "Filter_Window in filter_update / self._standard_item_model.temporary_standard_item_model_film_general_film_production_company.rowCount() before search: --> \n {}".format(self._standard_item_model.temporary_standard_item_model_film_general_film_production_company.rowCount())

        certain_proxy_model.setFilterMaximumColumns(column = column)

        if line_edit_widget is None:
            line_edit_widget = QLineEdit()

        syntax = QRegExp.RegExp
        caseSensitivity = Qt.CaseInsensitive
        regExp = QRegExp(unicode(line_edit_widget.text()), caseSensitivity, syntax)
        certain_proxy_model.setFilterRegExp(regExp)

        print "Filter_Window / filter_update / certain_proxy_model.rowCount() after search: --> \n".format(certain_proxy_model.rowCount())
        print ""

        # No results match for your search.
        if certain_proxy_model.rowCount() == 0:

            syntax = QRegExp.RegExp
            caseSensitivity = Qt.CaseInsensitive
            regExp = QRegExp(QString(""), caseSensitivity, syntax)
            certain_proxy_model.setFilterRegExp(regExp)

            self.close_wait_message()

            self.create_generally_information_msg(title = self.tr("No matching results"),
                                                 msg = "No results were found for this search",
                                                 detail_msg = "",
                                                 yes_no_button = False,
                                                 only_yes_button = True,
                                                 line_edit_object_get_focus = line_edit_widget,
                                                 icon = self.image_path_collection.info_32x32_default,
                                                 set_flag = True)

        else:

            self.close_wait_message()

        return

    def set_hide_column(self,
                        column,
                        bool_value,
                        list_tree_view_widget = None,
                        tree_view_widget = None):
        '''
           SUMMARY
           =======
           The given column will be hidden, otherwise it will be shown

           PARAMETERS
           ==========
           :list_tree_view_widget (list):   This argument contains a list of QTreeView()-objects

           :tree_view_widget (class):       It contains a single QTreeView()-object

           :column (int):                   We need a number to hide a special column.

           :bool_value (bool):              Boolean to hide, when it is True.

           RETURNS
           =======
           :return:                         Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set column hidden on given Qt-object")

        if not tree_view_widget is None:
            tree_view_widget.setColumnHidden(column, bool_value)

        if isinstance(list_tree_view_widget, list):
            for tree_view in list_tree_view_widget:
                tree_view.setColumnHidden(column, bool_value)

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (class):    We except a text (Qstring) for the title bar of the message box.

           :err_msg (class) :     We need the text (Qstring) for the main message to be displayed.

           :detail_msg (class):   When we get a detailed text (Qstring), the hide/show details" button will be added.

           :icon (str):           For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         title,
                                         msg ,
                                         detail_msg ,
                                         icon,
                                         line_edit_object_get_focus,
                                         set_flag = False,
                                         yes_no_button = False,
                                         only_yes_button = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (str):                        This methods excepts a text for the title bar of the message box.

           :msg (str) :                         We also need the text for the main message to be displayed.

           :detail_msg (str):                   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):                         A icon path to display that on message box.

           :line_edit_object_get_focus (class): By closing the message box the given QLineEdit() get focus in.

           :yes_no_button (bool):               By default its False. We don't show yes and no button, otherwise we show both.

           :only_yes_button (bool):             By defaul, its False, we don't show onlly yes button, otherwise we show it.

        '''
        logger.info("Create a message box for information")

        if not self.messagebox_called:

            if yes_no_button:

                yes_no_msg_box = custom_message_box(self, text = msg,
                                                          title = title,
                                                          icon = icon,
                                                          set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_no_msg_box.setInformativeText(detail_msg)

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Yes")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("No")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.

                # 1 means No
                if result_button == 1:
                    self.close()
                # 0 means Yes
                elif result_button == 0:
                    self._image_path = None
                    self.pixmap = None
                    self.ui_person_profile.label_photo.clear()
                    self.set_all_widget_on_default()
                    self.quite_threads()

            if only_yes_button:

                yes_msg_box = custom_message_box(self,
                                                  text = msg,
                                                  title = title,
                                                  icon = icon,
                                                  set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_msg_box.setInformativeText(detail_msg)

                yes_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Ok")),
                    #QPushButton(self.tr(" No")),
                    yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
                yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

                self.set_focus(widget_object = line_edit_object_get_focus, bool_value = True)

    def display_counting(self,
                         widget = None,
                         count = None):
        '''
            NOTICE:
            =======
            The given QProgressBar()-object will update its value, so it will suppose
            to progress while the items are populate.

            PARAMETERS:
            ===========
            :widget -   On which widget should be displayed the counter?

            :count  -   In this case we ecpect int()-value(s) to display
                        the counter on given widget.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        widget.setText("Result(s): {}".format(count))

        return

    def generator_header_data(self, list_header):

        for header_data in list_header:
            yield header_data


    def set_header_data(self, model = None, list_header_data = None):
        '''
            NOTICE:
            =======
            This method sets the data for the given role and section
            in the header with the specified orientation to the value supplied.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        print "\n"
        print "Filter_Window set_header_data is called successfully"
        print "filter_update in set_header_data / model.objectName(): --> \n {}".format(model.objectName())
        print "Filter_Window in set_header_data / list_header_data: --> \n {}".format(list_header_data)
        count_column = 0

        for header_data in self.generator_header_data(list_header_data):
            model.setHeaderData(count_column, Qt.Horizontal, header_data)

            count_column +=1

        return

    def update_progress_bar(self,
                            process_value = 0,
                            progress_bar_object = None,
                            final_count = 0):
        '''
            NOTICE:
            =======
            The given QProgressBar()-object will update its value, so it will suppose
            to progress while the items are populate.

            DETAILS:
            ========
            This method is also used to set range to minimum = 0 and to maximum = final_count.

            PARAMETERS:
            ===========
            :process_value  -   Current number of processes.

            :final_count    -   Total number of rows in a table. This method is also
                                used to stop the busy indicator of given
                                QProgressBar()-object by using setValue()-method.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        self._proccessed_progressbar = process_value

        progress_bar_object.setRange(0, final_count)

        progress_bar_object.setValue(process_value)

        #print "count {}".format(process_value)
        if process_value == 0 and final_count == 0:
            self.set_text(widget = progress_bar_object, text = 'Buuh')

        return

    def set_maximum_value(self,
                          value = None):
        '''
            NOTICE:
            =======
            This method is used to set maximum value of QProgressBar() and its also
            used to show a QProgressbar()-object in busy mode with text instead of a
            percenttage of steps, because minimum and maximum are both set to 0.

            PARAMETERS:
            ===========
            :value  -   Get a vbalue to set the maximum value of progress bar, it
                        can be any int and it will be automatically converted to x/100% values
                        e.g. max_value = 3, current_value = 1, the progress bar will show 33%.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        self.set_hide_widget(widget = self.ui_filter.frame_result, bool_value = True)
        self.ui_filter.progressBar_result.setMaximum(value)

        return

    def set_source_model(self, model=None):
        '''
            NOTICE:
            =======
            Set main model as source for the proxy model.

            PARAMETERS:
            ===========
            :model      -   We except a model for the proxy model.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        self._proxy_model.setSourceModel(model)

        return

    def set_proxy_model(self):
        '''
            NOTICE:
            =======
            Create proxy model for filtering and main model for data.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        self._proxy_model = MySortFilterProxyModel(self)
        self._proxy_model.setDynamicSortFilter(True)

        return

    def set_proxy_later(self,
                        tree_view = None,
                        person_profile = None):

        dict_ex = {'person_profile': lambda: self.set_source_model(model = self._standard_item_model_general_person)}

        dict_ex[person_profile]()
        tree_view.setModel(self._proxy_model)

    def Set_model_on_treeview(self,
                              **kwargs):
        '''
           SUMMARY
           =======
           Sets the model for the view to present.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Sets the model for the view to present.")

        try:


          kwargs.get('widget_tree_view').setModel(kwargs.get('certain_proxy_model'))

          self.init_selectionChanged_signal_treeview()
          self.close_wait_message()

          self.set_hide_column(
            tree_view_widget = kwargs.get('widget_tree_view'),
            column = 0,
            bool_value = True)

        except AttributeError:

          pass

        return

    def update_proxy_model(self,
                           **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the certain proxy model")

        #self.quite_threads()

        self.task_thread = QThread()
        self.task_thread.work = TaskProxyModel(parent = None,
                                              **kwargs)

        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        # self.task_thread.work.notify_item.connect(lambda standard_item:
        #     general_proxy_model.set_source_model(model = standard_item))

        # self.task_thread.work.notify_progress.connect(lambda certain_proxy_model, category:
        #     general_proxy_model.refresh_proxy_model(widget_tree_view = widget_tree_view,
        #                                             certain_proxy_model = certain_proxy_model,
        #                                             category = category,
        #                                             call_back_init_selectionChanged_signal_treeview = self.init_selectionChanged_signal_treeview,
        #                                             call_back_close_wait_message = self.close_wait_message))

        self.task_thread.work.Set_model_on_treeview_signal.connect(
          lambda kwargs:
          self.Set_model_on_treeview(**kwargs))


        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_work)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def test_func(self, text):
        print text

    def populate_standard_item_model(self,
                                    certain_standard_item_model,
                                    append_row,
                                    img_size,
                                    IMAGE_PATH,
                                    row_total):

        #All graphical Qt operations should happen in the main thread. Other threads are not really allowed to call Qt graphical operations (including probably pixmaps).

        if row_total == 4:

            append_row.remove(append_row[1])

            item = QStandardItem()

            item.setIcon(QIcon(QPixmap(IMAGE_PATH).scaled(img_size)))

            append_row.insert(1, item)

            certain_standard_item_model.appendRow(append_row)

        return

    def fetch_all(self,
                  **kwargs):
        '''
            NOTICE:
            =======
            This method is used to start each thread to be fetching all data.

            DETAILS:
            ========
            First we create a new QThread()-object, second we have to create another
            instance of a class that inherits from QObject. Third, we move the created
            instance of QObject in the first created QThread()-object. Fourth all signals
            are connected to methods and the thread-process can begin.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        #   First, before we start a thread, we have to quite all running threads.
        self.quite_threads()

        self.set_header_data(
            model = kwargs.get('certain_standard_item_model'),
            list_header_data = kwargs.get('list_header'))

        self.task_thread = QThread(self)

        self.task_thread.work = TaskDataManipulation(**kwargs)

        # self.task_thread.work.set_header_data_signal.connect(lambda:
        #     self.set_header_data(model = standard_item_model,
        #                          list_header_data = list_header))

        ''' # need to store worker and thread '''
        self._list_threads.append((self.task_thread))

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.populate_proxy_model_signal.connect(
            lambda
            kwargs:
            self.update_proxy_model(**kwargs))

        # self.task_thread.work.set_proxy_later_signal.connect(lambda:
        #     self.set_proxy_later(tree_view = tree_view,
        #                          category = category))

        # self.task_thread.work.set_hide_widget_signal.connect(
        #     lambda
        #     bool_value:
        #     self.set_hide_widget(
        #         widget = self.ui_filter.frame_result,
        #         bool_value = bool_value))

        # self.task_thread.work.count_row_signal.connect(
        #     lambda
        #     count_value:
        #     self.display_counting(
        #         widget = self.ui_filter.label_result,
        #         count = count_value))

        # self.task_thread.work.count_progress_signal.connect(
        #     lambda
        #     value:
        #     self.set_maximum_value(
        #         value = value))

        # self.task_thread.work.update_progress_bar_signal.connect(
        #     lambda
        #     process_value,
        #     row_count:
        #     self.update_progress_bar(
        #         process_value = process_value,
        #         progress_bar_object = self.ui_filter.progressBar_result,
        #         final_count = row_count))

        #task_thread.work.update_progress_bar_online_signal.connect(lambda process_value, row_count:
        #                                                   self.update_progress_bar(process_value = process_value,
        #                                                                            progress_bar_object = self.ui_master_data.progressBar_online_master_data_general_region,
        #                                                                            final_count = row_count))

        #task_thread.work.count_progress_signal.connect(lambda value:
        #                                               self.set_maximum_value(value = value))

        # self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)

        self.task_thread.work.hide_column_signal.connect(
            lambda
            widget_tree_view:
            self.set_hide_column(
                tree_view_widget = widget_tree_view,
                column = 0,
                bool_value = True))

        self.task_thread.work.populate_standard_item_model_in_gui_signal.connect(
            lambda
            append_row,
            row_total,
            img_size,
            IMAGE_PATH:
            self.populate_standard_item_model(
                certain_standard_item_model = kwargs.get('certain_standard_item_model'),
                append_row = append_row,
                row_total = row_total,
                img_size = img_size,
                IMAGE_PATH = IMAGE_PATH))

        self.task_thread.work.confirm_saving_successful_signal.connect(lambda msg_text, detail_msg:
            self.create_generally_information_msg(title = self.tr('Confirmation'),
                                                  msg = self.tr(msg_text),
                                                  detail_msg = detail_msg,
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = widget_line_edit,
                                                  only_yes_button = True,
                                                  icon = self.image_path_collection.info_48x48_default,
                                                  set_flag = True))

        self.task_thread.work.error_message_signal.connect(lambda title_text, conn_err, detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                               err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self.image_path_collection.warning_48x48_default,
                                               set_flag = True))



        #task_thread.work.text_notify_update_signal.connect(lambda text:
        #                                                   self.append_text(widget = notify_on_widget,
        #                                                                    clear_widget = False,
        #                                                                    text = text))

        #task_thread.work.error_message_signal.connect(lambda title, error_msg:
        #                                              self.create_critical_message_box(title = title,
        #                                                                               text = error_msg,
        #                                                                               icon = self.image_path_collection.warning_48x48_default))

        # self.interrupt_working_thread_signal.connect(self.task_thread.work.abort_task_thread)

        # self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)

        # self.task_thread.work.populate_item_signal.connect(lambda tuple_items:
        #     self.populate_model(tuple_items = tuple_items,
        #                         model = self._standard_item_model_general_person))


        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start_work)

        #   We connect the finish-signal with deleteLater()-method of QThread for
        #   scheduling a delete of an QObject to free resources from within the
        #   object itself. That menas, when the thread has finished executing, the
        #   finished-signal is emiiited. Its importen, because we should terminate
        #   threads correctly, which must be done with existing cross-threading
        #   signal slot connections with deleteLater(), so that pending events
        #   do not trigger a hanging pointer. So we connect the finished()
        #   of the thread (not the worker!) to its own deleteLater() slot.
        #   This will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def create_generally_crritcal_msg(self, err_title, err_msg, detail_msg):
        mBox = custom_message_box(self, err_msg, err_title, QMessageBox.Critical)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_msg)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._list_threads. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close all opened threads")

        for thread in self._list_threads:
            try:
                #print "ID Thread", id(thread), "running?", thread.isRunning()
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in -place
        del self._list_threads[:]

        #   And then we want the programm will deletes the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def closeEvent(self,
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            #self.set_flat_signal.emit('master_data', False)
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return

if __name__ == "__main__":

    USER_INI_FILE_PATH = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    dict_custom_config = ConfigurationSaverDict()

    convert_current_time = get_time_now()

    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')
    configure_logging(LOG_FILE_PATH)

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Note_Window()

    window.showMaximized()
    exit(app.exec_())
