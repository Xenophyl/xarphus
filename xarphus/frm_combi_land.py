#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys
from inspect import currentframe

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow

from xarphus.core.manange_logging import set_custom_logger


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class ComboLand_Window(QWidget):
    def __init__(self, func_up_to_date, update_sublist, info_app, parent):

        QWidget.__init__(self, parent)

        self.info_app = info_app

        self.custom_logger = set_custom_logger(self.info_app)

        self.custom_logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")

        UI_PATH = QFile(":/ui_file/combo_land.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        self.custom_logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.open(QFile.ReadOnly)

        self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + "opened")
        self.custom_logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + "")

        self.ui_combo_land = loadUi(UI_PATH, self)

        self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded")
        self.custom_logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.close()

        self.custom_logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed")

        self.init_ui()
        self.init_pushButton()

    def init_ui(self):
        pass

    def init_pushButton(self):
        pass
        #self.ui_add_ratings.pushButton_close.clicked.connect(self.close_form)

    def get_class_name(self):
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")
        return self.__class__.__name__

    def closeEvent(self, event):
        pass

    def close_form(self):
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        self.custom_logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")