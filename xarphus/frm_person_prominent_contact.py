#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = "frm_movie_.py"

import os
import sys
from os.path import expanduser

from PyQt4.QtCore import Qt, QFile
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QFileDialog, QPixmap, QIcon, QWidget, QLabel

from xarphus.core import get_news_in_version



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class PersonProminentContact_Window(QWidget):
    def __init__(self,
                 func_up_to_date,
                 update_sublist,
                 parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/person_prominent_contact.ui")

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist


        UI_PATH.open(QFile.ReadOnly)
        self.ui_person_profile = loadUi(UI_PATH, self)
        UI_PATH.close()



    def closeEvent(self, event):
        pass

    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()

