FILE_NAME = "downloader.py"

try:
    import os
    import sys
    import requests
    from requests.auth import HTTPBasicAuth
except ImportError as ImpErr:
    raise

try:
    from PyQt4.QtCore import QThread, pyqtSignal, Qt, QSemaphore
    from PyQt4.QtGui import QVBoxLayout, QPushButton, QDialog, QProgressBar, QApplication, QMessageBox, QMdiSubWindow

except ImportError:
    raise

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'DVDProInstall.zip')
DOWNLOAD_URL = 'http://xarphus.bplaced.net/xarphus_update/DVDProInstall.zip'
USERNAME = 'admin'
PASSWORD = 'danny82'

class Download_Thread(QThread):
    finished_thread = pyqtSignal()
    error_http = pyqtSignal(str, str, str)
    finished_download = pyqtSignal()
    notify_progress = pyqtSignal(int)
    set_max_value = pyqtSignal(int)

    on_progress_ready_signal = pyqtSignal(object)

    def __init__(self, localfile, url, username, password, parent=None):
        QThread.__init__(self, parent)

        self.url = url #specify the url of the file which is to be downloaded
        self.username = username
        self.password = password
        self.localfile = localfile  # specify save location where the file is to be saved

        self._run_semaphore = QSemaphore(1)

    def run(self):

        chunk_size = 0
        try:
            session_requests = None
            session_requests = requests.session()

            auth = HTTPBasicAuth(self.username, self.password)

            response = session_requests.get(url = self.url, auth = auth, stream = True)
            http_status_code = response.status_code

            print "http_status_code", http_status_code

            if not http_status_code == 200:
                print http_status_code

                self.error_http.emit(
                    'Error',
                    'The file could not be downloaded',
                    str(http_status_code))

            else:
                content_txt_file = DOWNLOAD_URL

                #file_size = int(requests.head(content_txt_file).headers.get('content-length', [0]))

                resGet = session_requests.get(content_txt_file, auth = auth, stream=True)
                file_size = int(resGet.headers['Content-length']) # output 121291539

                #print "%s Byte" %file_size
                print "file size", int(file_size)
                self.on_progress_ready_signal.emit(file_size)
                self.set_max_value.emit(file_size)

                result = file_size / (1024*2)

                chunk_size = int(result)

                downloaded_bytes = 0
                update_file = session_requests.get(url = DOWNLOAD_URL, auth= auth, stream = True)
                with open(self.localfile, 'wb') as fd:


                    for chunk in update_file.iter_content(chunk_size):
                        fd.write(chunk)
                        downloaded_bytes = fd.tell()
                        #print (float(downloaded_bytes)/file_size*100)
                        download_percentage = float(downloaded_bytes)/file_size*100

                        self.notify_progress.emit(download_percentage)
                        #self.on_progress_ready_signal.emit(float(downloaded_bytes)/file_size*100)

                        if self._run_semaphore.available() == 0:
                            self._run_semaphore.release(1)
                            break

                    print "Finish"
                    # self.finished_download.emit()
                    # self.finished_thread.emit()

        except (requests.exceptions.URLRequired,
                requests.exceptions.ConnectionError,
                requests.exceptions.HTTPError,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ReadTimeout), g:
            self.error_http.emit()

    def stop(self):
        print "stop"
        self._run_semaphore.acquire(1)

class MyCustomDialog(QDialog):

    def __init__(self):
        super(MyCustomDialog, self).__init__()
        layout = QVBoxLayout(self)

        # Create a progress bar and a button and add them to the main layout
        self.progressBarUpdate = QProgressBar(self)
        self.progressBarUpdate.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.progressBarUpdate)

        pushButtonUpdate = QPushButton("Start", self)
        layout.addWidget(pushButtonUpdate)
        pushButtonCancel = QPushButton("Cancel", self)
        layout.addWidget(pushButtonCancel)

        localfile = os.path.abspath(os.path.join('temppor'))

        self.progress_initialized = False

        pushButtonUpdate.clicked.connect(lambda: self.check_folder_exists(localfile = localfile))

        self.download_task = Download_Thread(
            localfile = TEMP_PATH,
            url= DOWNLOAD_URL,
            username = USERNAME,
            password = PASSWORD
            )

        self.download_task.notify_progress.connect(self.on_progress)
        self.download_task.finished_thread.connect(self.on_finished)
        self.download_task.set_max_value.connect(self.set_progress_bar)
        self.download_task.on_progress_ready_signal.connect(self.on_progress_ready)


        self.download_task.error_http.connect(self.on_HTTPError)
        self.download_task.finished_download.connect(self.on_finish_download)

        pushButtonCancel.clicked.connect(self.on_finished)

    def on_start(self):
        self.progressBarUpdate.setRange(0, 0)
        self.download_task.start()

    def on_finish_download(self):
        msg_box = QMessageBox()

        QMessageBox.question(msg_box, ' Message ',
                                           "The file has been fully downloaded.", msg_box.Ok)

    def on_HTTPError(self,
                     msg_title,
                     msg_text,
                     detail_text):
        reply = QMessageBox.question(self, msg_title,
                                           msg_text + "\n\n\n More details: {detail_text}".format(detail_text = detail_text), QMessageBox.Ok)

        self.download_task.stop()

        return

    def set_progress_bar(self, max_value):

        self.progressBarUpdate.setMaximum(100)

        #self.progressBarUpdate.setRange(0, max_value/5)
        #self.progressBarUpdate.setRange(0, 100)

        #self.progressBarUpdate.setValue(max_value)

        return

    def on_progress(self, i):
        #self.progressBarUpdate.setRange(0, 100)
        #print i
        self.progressBarUpdate.setValue(int(i))

    def on_progress_ready(self, data):
        # The first signal sets the maximum, the other signals increase a progress
        if self.progress_initialized:
            print "data", data
            #self.progressBarUpdate.setValue(self.progressBarUpdate.value() + int(data))
            self.progressBarUpdate.setValue(int(data))
        else:
            #self.progressBarUpdate.setMaximum(int(data))
            self.progressBarUpdate.setRange(0, int(data))

            self.progress_initialized = True

        return

    def check_folder_exists(self,
                            localfile):

        localfile = os.path.abspath(os.path.join('temppor'))

        if not os.path.exists(localfile):
            os.makedirs(localfile)
            self.on_start()
        else:
            self.on_start()

        return

    def on_finished(self):
        self.progressBarUpdate.setValue(0)
        self.close()

    def closeEvent(self, event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event (class):  By default the given event is accepted and the widget is closed. We
                            get this event when Qt receives a window close request for a top-level
                            widget from the window system. That means, first it sends the widget a QCloseEvent.
                            The widget is closed if it accepts the close event. The default implementation of
                            QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()
            exit()

        return

def downloader_main():
    #   Create app
    app = QApplication(sys.argv)

    #   Create an instance of our window
    window = MyCustomDialog()
    window.resize(600, 400)
    window.show()

    #    start the app
    sys.exit(app.exec_())

# main method to call our app
if __name__ == "__main__":
    downloader_main()
