FILE_NAME = "downloader2.py"

import os
import requests
import sys
from requests.auth import HTTPBasicAuth

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QVBoxLayout, QPushButton, QWidget, QProgressBar, QMessageBox

from requests.auth import HTTPDigestAuth

import urllib2

class MyCustomDialog(QWidget):
    def __init__(self, parent):
        QWidget.__init__(self, parent)

        super(MyCustomDialog, self).__init__()
        layout = QVBoxLayout(self)

        #self.url = get_access_data_and_link('Sophus','danny5658')
        #print "CALLING DOWNLOAD", self.url

        self.url = 'http://xarphus.de/protect_folder/'

        # Create a progress bar and a button and add them to the main layout
        self.progressBarUpdate = QProgressBar(self)
        self.progressBarUpdate.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.progressBarUpdate)

        pushButtonUpdate = QPushButton("Start", self)
        layout.addWidget(pushButtonUpdate)
        pushButtonCancel = QPushButton("Cancel", self)
        layout.addWidget(pushButtonCancel)

        pushButtonUpdate.clicked.connect(self.check_folder_exists)

        # Set data for download and saving in path
        self.location = os.path.abspath(os.path.join('temp', 'example-app-0.3.win32.zip'))
        #self.url = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

        self.setWindowModality(Qt.ApplicationModal)

        pushButtonCancel.clicked.connect(self.on_finished)

    def on_start(self):
        self.progressBarUpdate.setRange(0, 0)
        self.download_task.start()

    def on_finish_download(self):
        msg_box = QMessageBox()

        QMessageBox.question(msg_box, ' Message ',
                                           "The file has been fully downloaded.", msg_box.Ok)

    def on_HTTPError(self):
        reply = QMessageBox.question(self, ' Error ',
                                           "The file could not be downloaded. Will they do it again?", QMessageBox.Yes |
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.on_start()
        else:
            print "Close button pressed"
            #event.ignore()

    def on_progress(self, i):
        self.progressBarUpdate.setRange(0, 100)
        self.progressBarUpdate.setValue(i)

    def check_folder_exists(self):
        location = os.path.abspath(os.path.join('temp'))
        if not os.path.exists(location):
            os.makedirs(location)
            print "Folder was created"
            self.on_start()
        else:
            print "Folder already exists"
            self.on_start()

    def on_finished(self):
        self.progressBarUpdate.setValue(0)
        self.close()

    def closeEvent(self, event):
        pass

