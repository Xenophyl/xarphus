#-*- coding:latin1 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QIcon, QMdiSubWindow, QHeaderView
from PyQt4.uic import loadUi
from PyQt4.QtCore import QFile, pyqtSignal
'''
Private modules are imported.
'''

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")

class MovieDetail_Window(QWidget):

    init_movie_detail_signal = pyqtSignal(int)
    set_window_title_combined_form_signal = pyqtSignal()

    def __init__(self, 
                 film_id,
                 form_title_movie,
                 type_detail,
                 img_paths,
                 parent):
        QWidget.__init__(self, parent)

        self.film_id = film_id
        self.form_title_movie = form_title_movie
        self.type_detail = type_detail

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        UI_PATH = QFile(":/ui_file/movie_details.ui")

        self.img_paths = img_paths

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_movie_detail = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def stack_set_current_index(self, 
                                     stack_widget_obj=None, 
                                     current_index=None):
        '''
            NOTICE:
            =======
            This method is used to change the stacked widget with the index of the page.


            PARAMETERS:
            ===========
            :stack_widget   -   We need the QStackedWidget()-object to work with it.

            :current_index  -   That keyxword carries the desired index of the page 
                                we want to change to.
            
            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the current index of stack widget")

        print "stack_set_current_index ID", current_index

        self.stack_current_index = current_index

        stack_widget_obj.setCurrentIndex(current_index)       

        return

    def set_window_title(self, title=None):
        print "set_window_title is called"
        print "title", title
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowTitle(title)

        return

    def set_window_icon(self, image=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.ui_movie_detail.setWindowIcon(QIcon(image))

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        self.set_window_icon(QIcon(self.img_paths.movie_icon))

        # This makes sure that columnwidths and heights are set to match their content.
        self.ui_movie_detail.treeWidget_audio_format.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_detail.treeWidget_audio_format.header().setStretchLastSection(True)

        self.ui_movie_detail.treeWidget_language.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_detail.treeWidget_language.header().setStretchLastSection(True)

        self.ui_movie_detail.treeWidget_aspect_ratio.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_detail.treeWidget_aspect_ratio.header().setStretchLastSection(True)

        self.init_clicked_signal_push_buttons()

    def change_window_title(self, detail_type):
        detail_type_dict = {0: "Blubb"}

        self.set_window_title(detail_type_dict[detail_type])

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object(s)")

        self.ui_movie_detail.pushButton_cancel.clicked.connect(self.unload_me)

        self.ui_movie_detail.pushButton_manage_medium.clicked.connect(
            lambda: self.stack_set_current_index(stack_widget_obj=self.ui_movie_detail.stackedWidget_medium,
                                                 current_index=1))

        self.ui_movie_detail.pushButton_back_medium_overview.clicked.connect(
            lambda: self.stack_set_current_index(stack_widget_obj=self.ui_movie_detail.stackedWidget_medium,
                                                 current_index=0))

        return

    def unload_me(self):
        self.init_movie_detail_signal.emit(0)
        self.set_window_title_combined_form_signal.emit()

    def lineEdit_change(self):
        self.ui_movie_detail.lineEdit_title_translated.textChanged.connect(self.title_form_change)

        return

    def closeEvent(self, event):

        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):

            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_movie_detail.close()
            print "closeEvent is calling"

        return

if __name__ == "__main__":

    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    from os import path

    from collection_paths_image import ImagePathnCollection
    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging  
    from frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
    from frm_movie_general import MovieGeneral_Window
    from frm_movie_general_details import MovieGeneralDetails_Window
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists
    from frm_movie_general_cover import MovieGeneralCover_Window
   
    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    image_pathn_collection = ImagePathnCollection()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()

    if not USER_INI_FILE_PATH:
        print "You entered nothing!"
    else:
        dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

        get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


        LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

        result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

        if not result_suffix == True:
            LOG_FILE_FOLDER_PATH = result_suffix

        else:
            pass

        LOG_FILE_PATH = os.path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
        configure_logging(LOG_FILE_PATH)

        QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))
        
        app = QApplication(sys.argv)

        translator = QTranslator()
        path = ":/translators/german_sie.qm"
        translator.load(path)
        app.installTranslator(translator)

        create_cover_viewer = None
        create_movie_detail = None
        create_select_details_form = None
        create_error_form = None
        parent=None     

        window = MovieDetail_Window(None,
                                    'Example_Movie',
                                    2,
                                    image_pathn_collection,
                                    parent)


        #window.resize(600, 400)
        window.showMaximized()
        sys.exit(app.exec_())

