#-*- coding:utf-8 -*-

FILE_NAME = __name__

from os import path
import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QFile
from PyQt4.QtGui import QPixmap, QSizePolicy, QWidget, QMdiSubWindow
from PyQt4.uic import loadUi

class CoverViewer_Window(QWidget):

    def __init__(self,
                 image_path,
                 form_title,
                 parent):

        QWidget.__init__(self, parent=None)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.image_path = image_path
        self.form_title = form_title


        #self.path_ui_MovieCoverViewer = os.path.abspath(os.path.join('files', "qt_ui", 'pp_MovieCoverViewer.ui'))
        UI_PATH = QFile(":/ui_file/cover_viewer.ui")


        self.load_ui_file(UI_PATH)
        self.init_clicked_signal_push_buttons()
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)

        self.init_cover_viewer()
        self.ui_cover_viewer.imageLabel.setPixmap(QPixmap(self.image_path))
        self.set_background_colors_label()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_cover_viewer = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_cover_viewer(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the window")
        
        self.ui_cover_viewer.setWindowTitle(self.form_title)
        self.ui_cover_viewer.labelExplain.setAutoFillBackground(True)

        return

    def set_background_colors_label(self):
        '''
            NOTICE:
            =======
            This method starts by setting white as the background color of the QLabel in an application. . 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set white as the background color the QLabel")

        self.ui_cover_viewer.labelExplain.setStyleSheet("background-color: white;")

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object")

        self.ui_cover_viewer.pushButton_close.clicked.connect(self.close)

        return

    def closeEvent(self, event):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    from collection_paths_image import ImagePathnCollection
    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging  

    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    image_pathn_collection = ImagePathnCollection()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()

    if not USER_INI_FILE_PATH:
        print "You entered nothing!"
    else:
        dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

        get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


        LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

        result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

        if not result_suffix == True:
            LOG_FILE_FOLDER_PATH = result_suffix

        else:
            pass

        LOG_FILE_PATH = os.path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
        configure_logging(LOG_FILE_PATH)

        QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

        app = QApplication(sys.argv)

        translator = QTranslator()
        path = ":/translators/german_sie.qm"
        translator.load(path)
        app.installTranslator(translator)

        form_title = raw_input('Enter form title: ')    

        image_path = raw_input('Enter path to image: ')    

        window = CoverViewer_Window(image_path,
                                    form_title,
                                    None)


        #window.resize(600, 400)
        window.showMaximized()
        sys.exit(app.exec_())
