#-*- coding:latin1 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QMdiSubWindow
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")

class PushbuttonSaveClose_Window(QWidget):
    def __init__(self, func_up_to_date, update_sublist, parent):
        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist

        UI_PATH = QFile(":/ui_file/pushbutton_save_close.ui")

        UI_PATH.open(QFile.ReadOnly)
        self.ui_pushbutton_close = loadUi(UI_PATH, self)
        UI_PATH.close()

    def closeEvent(self, event):
        pass

    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()