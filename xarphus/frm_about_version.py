#!/usr/bin/env python
#-*- coding:latin1 -*-

from os import path
from sys import argv, exit
import requests

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import Qt, QFile, QThread, pyqtSignal, QObject
from PyQt4.uic import loadUi
from PyQt4.QtGui import QDialog, QMdiSubWindow

try:
    from xarphus.core.get_news_in_version import get_version_news
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
except ImportError:
    from core.get_news_in_version import get_version_news
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class NewInVersion_Window(QDialog):
    def __init__(self,
                 url_version,
                 title_form,
                 label_content,
                 waiting_text,
                 configuration_saver_dict,
                 parent=None):

        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/new_in_version.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.configuration_saver_dict = configuration_saver_dict

        BASE_PATH = self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"]
        self.TIME_OUT = self.configuration_saver_dict.dict_set_general_settings["TimeOutGetVersionHistory"]
        TEMP_PATH = path.join(BASE_PATH, 'example-app-0.3.win32.zip')

        self.Form_Title = title_form
        self.Label_Content = label_content
        self.URL_Version = url_version
        self.waiting_text = waiting_text

        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_new_in_version = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def start_task(self):
        self.change_plainTextEdit_version_news("")

        task_thread = QThread(self)
        task_thread.work = CheckVersionHistory_Thread(self.URL_Version, self.TIME_OUT)
        task_thread.work.moveToThread(task_thread)

        task_thread.work.text_request.connect(self.change_plainTextEdit_version_news)

        task_thread.started.connect(task_thread.work.load_version_info)

        task_thread.finished.connect(task_thread.deleteLater)

        task_thread.start()

    def activate_push_button(self):

        self.ui_new_in_version.pushButton_close.setEnabled(True)

        return

    def change_plainTextEdit_version_news(self, given_text):

        self.ui_new_in_version.plainTextEdit_version_news.setPlainText(given_text)

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        self.ui_new_in_version.pushButton_close.clicked.connect(self.close)

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently the title
            of the window is set and the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Inistialize the GUI")

        self.ui_new_in_version.setWindowModality(Qt.ApplicationModal)
        # title
        self.ui_new_in_version.setWindowTitle(self.Form_Title)
        # PlainText
        self.change_plainTextEdit_version_news(self.waiting_text)
        # label
        self.ui_new_in_version.label_explain.setText(self.Label_Content)

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_new_in_version.setWindowFlags(flags)

        self.ui_new_in_version.pushButton_close.setEnabled(True)

        self.init_clicked_signal_push_buttons()

        self.start_task()

        return

    def closeEvent(self,
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        self.configuration_saver_dict.dict_set_general_settings["TempImagePathFilmPoster"]=""

        return

    def keyPressEvent(self, event):
        '''
        Did the user press the Escape key?
        Note: QtCore.Qt.Key_Escape is a value that equates to
        what the operating system passes to python from the
        keyboard when the escape key is pressed.
        '''
        pass

        return

class CheckVersionHistory_Thread(QObject):
    text_request = pyqtSignal(str)

    def __init__(self,
                 given_url,
                 given_timeout,
                 parent=None):
        QObject.__init__(self, parent)

        self.given_url = given_url
        self.given_timeout = int(given_timeout)

    def load_version_info(self):

        try:
            response = get_version_news(self.given_url, self.given_timeout)

            self.text_request.emit(response.text)

        except (requests.exceptions.ConnectionError,
                   requests.exceptions.HTTPError,
                   requests.exceptions.Timeout,
                   requests.exceptions.ConnectTimeout):

            pass

            self.text_request.emit(self.tr("ERROR: The text could not be loaded. ") +
                                          "\n" +
                                          "\n" + self.tr("Possible causes:") +
                                          "\n" + self.tr("1. Your Internet connection was interrupted") +
                                          "\n" + self.tr("2. The server is currently not available") +
                                          "\n" +
                                          "\n" + self.tr("Please check your Internet connection or try again later."))

        return

    def stop(self):
        pass


if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    '''
    Private modules are imported.
    '''
    from core import get_news_in_version
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint

    from core.manange_logging import create_log_file
    from core.config import ConfigurationSaver
    from info import info_app
    from core.manage_databases import DatabaseConnectionSQLite
    from core.manage_time import get_time_now

    ## Import compiled files
    from gui import ui_rc
    from images import images_rc
    from localization_files import translate_files_rc

    FILE_NAME = "frm_about_version.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')

    dict_custom_config = ConfigurationSaver()
    app_info = info_app()

    create_log_file(LOG_FILE_PATH, app_info)

    url_version = dict_custom_config.dict_set_general_settings["URLServerVersionNews"]

    title_form = "What's new?"
    label_content = "Lets take a look"
    waiting_text = "Wait"

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = NewInVersion_Window(url_version,
                                 title_form,
                                 label_content,
                                 waiting_text,
                                 dict_custom_config,
                                 app_info,
                                 None)
    window.show()
    exit(app.exec_())
