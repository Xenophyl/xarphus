#!/usr/bin/python
#-*- coding:utf-8 -*-


#   Import all Built-in Functions
from os import path
import sys
import inspect
import traceback
from itertools import chain
from datetime import date


import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



from PyQt4.QtCore import Qt, QRegExp, QString, QEvent, pyqtSignal, QThread, pyqtSlot, QModelIndex

from PyQt4.QtGui import QMdiSubWindow, QWidget, QVBoxLayout, QIcon, QFileDialog, \
    QPixmap, QValidator, QRegExpValidator, QTabWidget, QGridLayout, QTextEdit, QToolButton, QSpacerItem, \
    QTreeWidget, QMenu, QLabel, QFrame, QHBoxLayout, QSizePolicy, QPushButton, QStackedWidget, QFormLayout, QLineEdit

try:
    #   Import forms
    from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
    from xarphus.frm_movie_general import MovieGeneral_Window
    from xarphus.frm_movie_general_details import MovieGeneralDetails_Window
    from xarphus.frm_movie_detail import MovieDetail_Window
    from xarphus.frm_movie_general_cover import MovieGeneralCover_Window
    from xarphus.frm_waiting import Waiting_Window
    from xarphus.subclass_master_data_load_data_item import TaskDataManipulation


    #   Import core codes
    from xarphus.core.manage_calculation import calculate_cover_poster
    from xarphus.core.manage_folder import is_folder_exists
    from xarphus.core.manage_input_controller import InputController
    from xarphus.core.manage_path import extract_directory
    from xarphus.core.manage_qt import custom_message_box

except ImportError:

    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    #   Import forms
    from collection_paths_image import ImagePathnCollection
    from frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
    from frm_movie_general import MovieGeneral_Window
    from frm_movie_general_details import MovieGeneralDetails_Window
    from frm_movie_detail import MovieDetail_Window
    from frm_movie_general_cover import MovieGeneralCover_Window
    from frm_waiting import Waiting_Window
    from subclass_master_data_load_data_item import LoadDataItem, TaskDataManipulation, TaskModel

    #   Import core codes
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists
    from core.manage_input_controller import InputController
    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from core.manage_qt import custom_message_box
    #   Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class MovieDataCache(object):
    def __init__(self):
        pass

class CombineMovieGeneralDetailsButtons_Window(QWidget):

    manage_save_push_button = pyqtSignal(object)
    set_text_cursor_line_edit_movie_general_signal = pyqtSignal(object)
    hide_status_signal = pyqtSignal()
    set_font_color_label_signal = pyqtSignal()
    set_enabled_push_button_signal = pyqtSignal(str, bool)
    set_all_widget_on_default_signal = pyqtSignal()
    set_translated_line_edit_focus_in_signal = pyqtSignal()
    set_all_combobox_default_signal = pyqtSignal()
    get_all_current_value_combobox_signal = pyqtSignal()

    get_current_scoped_session_signal = pyqtSignal()
    send_current_scoped_session_combine_movie_general_details_buttons_form_signal = pyqtSignal(object)
    start_thread_movie_general_signal = pyqtSignal()
    open_master_data_signal = pyqtSignal(int)
    close_wait_message_signal = pyqtSignal()

    quite_threads_signal = pyqtSignal()

    '''
        NOTICE:
        =======
        This class loads and combines interfaces. Here loads three interfaces in total.

        DETAILS:
        ========

        Movie General           -   Here we want to save and display some data in general, for example
                                    the translated, originale and alternative title of the movie or the
                                    year of the production etc.

        Movie General Details   -   We also want to save and display data in general, but we split the
                                    generel-part in two separate - from top to bottom. Here we save the
                                    actors, comments, editions or versions of the movie etc.

        Movie Details           -   On this interface you can save detailed information about the movie,
                                    which is different from other version of edition of the same film. After all,
                                    there are several editions and versions of the same movie.

        Pushbuttons             -   This combined interface needs some buttons to push.
                                    Here we use buttons for Deleting, Printing, Saving the
                                    data and last but not least for Closing the interface.

    '''
    def __init__(self,
                 scoped_session,
                 standard_item_model,
                 general_proxy_model,
                 work_mode,
                 app,
                 show_messagebox = False,
                 film_id = None,
                 create_cover_viewer = None,
                 create_select_details_form = None,
                 form_title = None,
                 configuration_saver_dict = None,
                 image_path_collection = None,
                 create_error_form = None,
                 parent = None):

        '''
            PARAMETERS
            ==========
            :film_id                        -   We except two values. When the iflm exists, we
                                                need the ID to display the data. But, when the film
                                                doesn't exist, we except None as value.

            :work_mode:

            :create_cover_viewer            -   In this keyword argument we except a function, that
                                                creates an instance and displays the instantiated
                                                when its called.


            :create_select_details_form     -   TODO: I am about to delete this one. I think
                                                don't need it anymore.

            :form_title                     -   When this combined window is called we need a
                                                window title. For instance: "Add movie" oder "Edit movie XYZ" etc.

            :configuration_saver_dict       -   We get a created instance of a class there is
                                                a dictionary with information about configuration.
                                                This created class is important to get access to the
                                                configurations.

            :image_path_collection          -   In this keyword there is a created instance of a class.
                                                In this class there is a collection of paths to some
                                                icons.

            :create_error_form              -   Here we need a function that creates an instance
                                                to display the form. In this case we want the function
                                                to display a error window when an error has occurred.

            :parent                         -   This class does support passing a parent argument, because
                                                we know that we will need the parent argument - its more flexible.
        '''

        QWidget.__init__(self, parent)

        '''
            Create all instance variables
        '''
        self._create_cover_viewer = create_cover_viewer
        self._create_error_form = create_error_form
        self._film_id = film_id
        self._form_title = form_title
        self._create_select_details_form = create_select_details_form
        self._configuration_saver_dict = configuration_saver_dict
        self._image_path_collection = image_path_collection
        self._work_mode = work_mode
        self._show_messagebox = show_messagebox
        self._app = app

        print "CombineMovieGeneralDetailsButtons_Window, self._app", self._app

        self._scoped_session = scoped_session

        self._standard_item_model = standard_item_model
        self._general_proxy_model = general_proxy_model

        parent.send_current_scoped_session_combine_movie_general_details_buttons_form_signal.connect(self.save_scoped_session)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        '''
            Create an instance of imported class.
        '''
        self.input_controller = InputController()

        '''
            Create all attributes
        '''
        self._film_title = ''.join(''.split())
        self._film_title_edition_version = None
        self._film_poster_path_old = None

        self._general_film_title_translated = None
        self._general_film_title_original = None
        self._general_film_release_year = None
        self._general_film_sound_system = None
        self._general_film_colour_format = None
        self._image_path = None
        self._file_extension = None
        self._genre_evaluation  = None
        self._total_evaluation = None
        self._allocation_film_general_general_country_adding_list = None
        self._allocation_film_general_general_country_deleting_list = None
        self._allocation_film_general_film_genre_adding_list = None
        self._allocation_film_general_film_genre_deleting_list = None

        self._stack_current_index=0

        self._protect_filter_widget = None

        self._count_space_pressed = 0
        self._current_pos = None

        self._list_threads = []

        '''
        There are all paths of images or icons.
        NOTICE:
        =======
        All the paths of icons or images are used repeatedly.
        That's the reason why the paths were outsourced.
        '''
        self.create_combined_form()
        self.init_ui()
        self.send_pyqt_signales()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def create_combined_form(self):
        '''
            NOTICE:
            =======
            A new window is created from several subclasses.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Try to create an instance of MovieGeneral_Window()")

        self.movie_general = MovieGeneral_Window(configuration_saver_dict = self._configuration_saver_dict,
                                                 image_path_collection = self._image_path_collection,
                                                 standard_item_model = self._standard_item_model,
                                                 general_proxy_model = self._general_proxy_model,
                                                 app = self._app,
                                                 parent = self)

        logger.info("Connecting all signales of PersonProfile_Window()-class")

        self.movie_general.get_current_scoped_session_signal.connect(lambda:
            self.get_current_scoped_session_signal.emit())

        self.movie_general.open_master_data_signal.connect(lambda number:
            self.open_master_data_signal.emit(number))

        logger.info("Try to create an instance of MovieGeneralDetails_Window()")

        self.movie_general_details = MovieGeneralDetails_Window(self._image_path_collection, self)

        logger.info("Try to create an instance of PushbuttonsClosePrintDeleteSave_Window()")

        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(parent = self)

        logger.info("Try to create an instance of QVBoxLayout()")

        v_layout = QVBoxLayout()

        logger.info("The instance of the class of  QVBoxLayout() is created successfully")

        logger.info("Try to add all the given widgets to QVBoxLayout()")

        v_layout.addWidget(self.movie_general)
        v_layout.addWidget(self.movie_general_details)
        v_layout.addWidget(self.pushbutton_close_print_delete_save)

        logger.info("All given widgets are added to to QVBoxLayout() successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        textEdit_view = QTextEdit(self)

        logger.info("The instance of the class of  QTextEdit() is created successfully")

        logger.info("Try to add all the given widgets to QTextEdit()")

        logger.info("Try to set the instance of QTextEdit() in read-only mode (True)")

        textEdit_view.setReadOnly(True)

        logger.info("The instance of QTextEdit is set in read-only mode (True) successfully")

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_view = QGridLayout()

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_view.addWidget(textEdit_view, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        logger.info("Try to create an instance of QTabWidget()")

        tab_widget = QTabWidget(self)

        logger.info("The instance of QTabWidget() successfully")

        logger.info("Try to create an instances of QWidget()")

        tab_movie_editing = QWidget(self)
        tab_movie_total_view = QWidget(self)

        logger.info("The instances of QWidget() successfully")

        logger.info("Try to set the layout manager for given widget to QVBoxLayout()")

        tab_movie_editing.setLayout(v_layout)

        logger.info("The given widget is set to QVBoxLayout() successfully")

        logger.info("Try to set the layout manager for given widget to QGridLayout()")

        tab_movie_total_view.setLayout(gridLayout_view)

        logger.info("The given widget is set to QGridLayout() successfully")

        logger.info("Try to add tabs with the given page and label to QTabWidget()")

        tab_widget.addTab(tab_movie_editing, self.tr("Editing"))

        tab_widget.addTab(tab_movie_total_view, self.tr("Total view"))

        logger.info("All given tabs with the given page and label are added to QTabWidget() successfully")

        self.remove_tab(tab_widget)

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_tab_widget = QGridLayout()

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_tab_widget.addWidget(tab_widget, 0, 0)

        logger.info("All given widgets are added to QVBoxLayout() successfully")

        logger.info("Try to create an instance of MovieDetail_Window()")

        self.movie_detail_form = MovieDetail_Window(self._film_id,
                                                   self._form_title,
                                                   0,
                                                   self._image_path_collection,
                                                   self)

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_movie_detail_form = QGridLayout()

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_movie_detail_form.addWidget(self.movie_detail_form)

        logger.info("All given widgets are added to QGridLayout() successfully")

        logger.info("Try to create an instances of QWidget()")

        stack_moviel_general = QWidget(self)
        stack_moviel_details = QWidget(self)

        logger.info("The instances of QWidget() successfully")

        logger.info("Try to create an instances of QStackedWidget()")

        self.stack_widget_movie = QStackedWidget()

        logger.info("The instances of QWidget() is created successfully")

        logger.info("Appends the given widgets to the QStackedWidget() with addWidget()-method")

        self.stack_widget_movie.addWidget(stack_moviel_general)
        self.stack_widget_movie.addWidget(stack_moviel_details)

        logger.info("The given widgets are added to the QStackedWidget successfully")

        logger.info("Sets the layout manager for this widgets to layouts")

        stack_moviel_general.setLayout(gridLayout_tab_widget)
        stack_moviel_details.setLayout(gridLayout_movie_detail_form)

        logger.info("The layout manager for this widgets are set")

        logger.info("Try to create an instance of QGridLayout()")

        gridLayout_Stack = QGridLayout()

        logger.info("The instance of the class of  QGridLayout() is created successfully")

        logger.info("Try to add all the given widgets to QGridLayout()")

        gridLayout_Stack.addWidget(self.stack_widget_movie)

        logger.info("All given widgets are added to QGridLayout() successfully")

        logger.info("Sets the layout manager for this widgets to layouts")

        self.setLayout(gridLayout_Stack)

        logger.info("The layout manager for this widgets are set")

        return

    def send_pyqt_signales(self):
        '''
           SUMMARY
           =======
           This method is used to send all defined and the necessary signalas of
           pyqtSignal() to the corresponding objects.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Send signals of pyqtSignal()')

        #   Send signales of pyqtSignal() who belong to MovieGeneral_Window()
        self.get_all_current_value_combobox_signal.emit()

        return

    def stack_set_current_index(self, current_index):
        '''
            NOTICE:
            =======
            This method is used to change the stacked widget with the index of the page.


            PARAMETERS:
            ===========
            :stack_widget   -   We need the QStackedWidget()-object to work with it.

            :current_index  -   That keyxword carries the desired index of the page
                                we want to change to.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the current index of stack widget")

        self._stack_current_index = current_index

        self.stack_widget_movie.setCurrentIndex(current_index)

        self.check_and_set_the_Window_title()

        return

    def check_and_set_the_Window_title(self):
        '''
            NOTICE:
            =======
            This method removes the tab at position index from this stack of widgets.
            The page widget itself is not deleted..

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Check and set the title of the window")

        '''
            First we check the current index of the stack widget.
            Zero means, the user wants to see the page where he
            can input general information of the movie, like: year, title, translated title,
            genre etc.
        '''
        if self._stack_current_index == 0:

            '''
                Well, we take a look if there is an existing movie data or
                the user is about to add a new movoe data to database. None says,
                there isn't an existing movie.
            '''
            if self._film_id is None:

                '''
                    We know there isn't an existing movie. So we set the title of the window.
                '''
                self.set_window_title(self.tr(u"Add movie: {}".format(self._film_title)))


                '''
                    Here we check if the variable named (self._film_id) contains a integer or int-like attributes.
                    In this case we also want to check for long.
                '''
            elif isinstance(self._film_id, (long, int)):

                '''
                    Exactly, the variable above contains a integer. That means, there is an existing
                    movie data. So we just take the film title from database that ist saved in the variable
                    named (self._film_title)
                '''
                self.set_window_title(self._film_title)

            '''
                Maybe the user wants to add a edition or version of a movie. All this
                information are on the second page. In Python-words: Second means number 1.
            '''
        elif self._stack_current_index == 1:

            '''
                We don't know if the user wants to add a edition or version of the movie.
                Lets take a look. Number 1 says, the user wants to add version of the movie.
            '''

            '''
                Well, we take a look if there is an existing movie data or
                the user is about to add a new movie data to database. None says,
                there isn't an existing movie.
            '''
            if self._film_id is None:

                self.set_window_title(self.tr(u"Add a new version of the movie: {movie_title}".format(movie_title=self._film_title)))

            elif isinstance(self._film_id, (long, int)):

                self.set_window_title(self.tr(u"Display the version of the movie: {movie_title}".format(movie_title=self._film_title)))

        elif self._stack_current_index == 2:

            '''
                We don't know if the user wants to add a edition or version of the movie.
                Lets take a look. Number 1 says, the user wants to add version of the movie.
            '''

            '''
                Well, we take a look if there is an existing movie data or
                the user is about to add a new movoe data to database. None says,
                there isn't an existing movie.
            '''
            if self._film_id is None:

                self.set_window_title(self.tr(u"Add a new edition of the movie: {movie_title}".format(movie_title=self._film_title)))

            elif isinstance(self._film_id, (long, int)):

                self.set_window_title(self.tr(u"Display the edition of the movie: {movie_title}".format(movie_title=self._film_title)))


        return

    def remove_tab(self, tab_obj):
        '''
            NOTICE:
            =======
            This method removes the tab at position index from this stack of widgets.
            The page widget itself is not deleted..

            PARAMETERS:
            ===========
            :tab_obj    -   The given argumnent says which tabs at the position
                            should be removed.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Revmove the tab with index 1")

        if self._film_id == None:
            '''
                No existing movie was selected. The user wants to add a new
                movie to database. That is the reason why the user doesn't need the
                the tab, that shows the overview like a print report.
            '''
            tab_obj.removeTab(1)

        return

    def save_scoped_session(self, scoped_session):

        print "save_scoped_session in CombineMovieGeneralDetailsButtons_Window is called"
        print "scoped_session", scoped_session

        self._scoped_session = scoped_session

        self.send_current_scoped_session_combine_movie_general_details_buttons_form_signal.emit(scoped_session)

        return

    def set_window_title(self,
                         title):
        '''
            NOTICE:
            =======
            This method is used to set title of the window.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        created_text = self.tr("Add movie:") + " " + title

        self.setWindowTitle(created_text)

        return

    def set_window_icon(self, image=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowIcon(QIcon(image))

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            This method is used to initialize the combined window.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Initialize the user interface.")

        #self.stack_set_current_index(0)

        self.set_window_icon((QIcon(self._image_path_collection.movie_icon)))

        self.init_emit_pyqt_signal()

        self.init_connect_pyqt_signal()

        '''
            When initializing, let us see if there (in the variable named self._film_id)
            is an ID.
        '''
        if self._film_id is None:
            '''
                No there isn't an ID. The means the user wants to a new movie meta data.
            '''
            self.hide_status_signal.emit()
            self.set_window_title(self._form_title)
            self.set_enabled_push_button_signal.emit("close", True)

        else:

            self.set_font_color_label_signal.emit()
            self.set_window_title(self.tr("Film..."))

        return

    def init_connect_pyqt_signal(self):
        '''
            NOTICE:
            =======
            This method is used to connect the pyqtSignal()-objects -
            from other threads to methods of this class.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Connecting the pyqtSignal()-objects")

        self.movie_general_details.init_movie_detail_signal.connect(self.init_movie_detail_ui)

        self.pushbutton_close_print_delete_save.close_signal.connect(self.close)

        self.pushbutton_close_print_delete_save.save_signal.connect(lambda:
            self.add_record(label_object = None,
                            general_standard_item_model = self._standard_item_model,
                            work_mode = self._work_mode,
                            category = "film_general",
                            work_area = "film_general",
                            set_flush = False,
                            image_path_collection = self._image_path_collection,
                            image_path = self._image_path,
                            stack_widget = None,
                            scoped_session = self._scoped_session,
                            configuration_saver_dict = self._configuration_saver_dict,
                            show_messagebox = self._show_messagebox,
                            film_general_translated_title = self._general_film_title_translated,
                            film_general_original_title = self._general_film_title_original,
                            film_general_relase_year = self._general_film_release_year,
                            film_general_genre_evalution = self._genre_evaluation,
                            film_general_total_evaluation = self._total_evaluation,
                            film_general_soundsystem = self._general_film_sound_system,
                            film_general_colour_format = self._general_film_colour_format,
                            film_soundsystem = self._general_film_sound_system,
                            film_colour_format = self._general_film_colour_format,
                            film_general_release_year = self._general_film_release_year,
                            film_file_extension_poster = unicode(self._file_extension),
                            temp_add_list_allocation_film_general_general_country = self._allocation_film_general_general_country_adding_list,
                            temp_del_list_allocation_film_general_general_country = self._allocation_film_general_general_country_deleting_list,
                            temp_add_list_allocation_film_general_film_genre = self._allocation_film_general_film_genre_adding_list,
                            temp_del_list_allocation_film_general_film_genre = self._allocation_film_general_film_genre_deleting_list))

        self.movie_general.open_view_form_signal.connect(self.visible_cover_viewer)

        self.movie_general.set_window_title_signal.connect(self.set_window_title)

        self.movie_general.handle_text_changed_signal.connect(self._save_movie_data)

        self.movie_general.save_temp_adding_list_signal.connect(lambda category, send_list:
            self.save_temp_adding_list(existing_id = 0, #self._film_id,
                                       category = category,
                                       send_list = send_list))

        self.movie_general.save_temp_deleting_list_signal.connect(lambda category, send_list:
            self.save_temp_deleting_list(existing_id = self._film_id,
                                         category = category,
                                         send_list = send_list))

        return

    def init_emit_pyqt_signal(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for pyqtSignal()-object(s).
            Connect the trigger signal to a slot.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for pyqtSignal()-object(s)")

        self.manage_save_push_button.connect(self.pushbutton_close_print_delete_save.manage_save_push_button)

        #self.set_text_cursor_line_edit_movie_general_signal.connect(self.movie_general.intermediate_set_the_text_cursor)

        self.hide_status_signal.connect(self.pushbutton_close_print_delete_save.hide_status_widget)

        self.set_font_color_label_signal.connect(self.pushbutton_close_print_delete_save.set_font_color_label)

        return

    def init_movie_detail_ui(self, stack_page_id):
        '''
            NOTICE:
            =======
            Well, the user is about to add a new version or edition of a movie.
            So, this method is used to take a look if the user wants to add a new edition
            or a version of the already existing or newly added movie.

            PARAMETERS:
            ===========
            :stack_page_id              -   With the given integer we want to set the current index
                                            of a stack widget.

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        '''
            Let us take a look, what the user want. This variable (self._film_id) is obtained
            by the CombineMovieGeneralDetailsButtons_Window()-class. In this variable you get
            n ID of a movie or you get a value None. None means, there isn't a movie to show.
            If there is None the user is about to add a new movie and then
            he maybe wants to add a new edition or version of the already
            newly added movie.
        '''

        self.check_and_set_the_Window_title()

        if self._film_id is None:

                self.hide_status_signal.emit()

        if not self._film_id is None:

            self.set_font_color_label_signal.emit()

        self.stack_set_current_index(current_index=int(stack_page_id))

        return

    def visible_cover_viewer(self, image_url=None):
        '''
            NOTICE:
            =======
            This method is used to show the form that shows the current poster
            of the movie.

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Create and show the Zoom-Window")

        self._create_cover_viewer(image_url,
                                 self.tr(u"Film-Poster: {}".format(self._film_title)))

        return

    def save_temp_adding_list(self,
                              existing_id,
                              category,
                              send_list):

        print ""
        print "save_temp_adding_list in CombineMovieGeneralDetailsButtons_Windowis called"
        print "save_temp_adding_list in CombineMovieGeneralDetailsButtons_Windowis / existing_id: {}".format(existing_id)
        print "save_temp_adding_list in CombineMovieGeneralDetailsButtons_Windowis / category: {}".format(category)
        print "save_temp_adding_list in CombineMovieGeneralDetailsButtons_Windowis / send_list: {}".format(send_list)
        print ""

        if category == "allocation_film_general_general_country":

            self._allocation_film_general_general_country_adding_list = send_list

        if category == "allocation_filmgeneral_filmgenre":

            self._allocation_film_general_film_genre_adding_list = send_list

        return

    def save_temp_deleting_list(self,
                                existing_id,
                                category,
                                send_list):

        if not existing_id is None:

            if category == "allocation_film_general_general_country":

                self._allocation_film_general_general_country_deleting_list = send_list

            if category == "allocation_filmgeneral_filmgenre":

                self._allocation_film_general_film_genre_deleting_list = send_list

        return


    def _save_movie_data(self,
                        text,
                        category):

        #print "CombineMovieGeneralDetailsButtons_Window in _save_movie_data / text: {}, category: {}".format(unicode(text), category)

        if category == 'general_film_title_translated':
            self._general_film_title_translated = unicode(text)

        elif category == 'general_film_title_original':
            self._general_film_title_original = unicode(text)

        elif category == 'general_film_release_year':
            self._general_film_release_year = unicode(text)

        elif category == 'general_film_genre_evaluation':
            self._genre_evaluation = int(text)

        elif category == 'general_film_total_evaluation':
            self._total_evaluation = int(text)#= None if not unicode(text) else unicode(text)

        elif category == 'film_poster_path':
            self._image_path = unicode(text)#= None if not unicode(text) else unicode(text)

        elif category == 'film_poster_extension':
            self._file_extension = unicode(text)

        elif category == 'general_film_sound_system':
            self._general_film_sound_system = unicode(text)

        elif category == 'general_film_colour_format':
            self._general_film_colour_format = unicode(text)

        if self._film_id is None:
            if self._general_film_title_translated:
                self.set_enabled_push_button_signal.emit('add_movie', True)
            else:
                self.set_enabled_push_button_signal.emit('add_movie', False)

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         title,
                                         msg ,
                                         detail_msg ,
                                         icon,
                                         line_edit_object_get_focus,
                                         title_icon = None,
                                         yes_no_button = False,
                                         only_yes_button = False,
                                         set_flag = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (str):                            We except a text for the title bar of the message box.

           :msg (str) :                             We need the text for the main message to be displayed.

           :detail_msg (str):                       When we get a detailed text, the hide/show details" button will be added.

           :icon (str):                             We need a icon path to display that on message box.

           :line_edit_object_get_focus (QLineEdit): By closing the message box the given QLineEdit() get focus in.

           :yes_no_button (bool):                   By default its False. We don't show yes and no button, otherwise we show both.

           :only_yes_button (bool):                 By defaul, its False, we don't show onlly yes button, otherwise we show it.

        '''
        logger.info("Create a message box for information")

        if yes_no_button:

            yes_no_msg_box = custom_message_box(self,
                                                text = msg,
                                                title = title,
                                                detail_text = detail_msg,
                                                icon = icon,
                                                title_icon = title_icon,
                                                set_flag = set_flag)

            #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
            #yes_no_msg_box.setDetailedText(detail_msg)
            #yes_no_msg_box.setInformativeText(detail_msg)

            yes_no_msg_box.addButton(
                #QPushButton(setIc_OK, self.tr(" Ok")),
                QPushButton(self.tr("Yes")),
                #QPushButton(self.tr(" No")),
                yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

            yes_no_msg_box.addButton(
                #QPushButton(setIc_OK, self.tr(" Ok")),
                QPushButton(self.tr("No")),
                #QPushButton(self.tr(" No")),
                yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

            result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
            # execution and allows a developer to wait for user
            # input before continuing.

            # 1 means No
            if result_button == 1:
                self.close()
            # 0 means Yes
            elif result_button == 0:
                #self._image_path = None
                #self.pixmap = None
                #self.ui_person_profile.label_photo.clear()
                self.set_all_widget_on_default_signal.emit()
                self.set_translated_line_edit_focus_in_signal.emit()
                self.set_all_combobox_default_signal.emit()
                #self.quite_threads()

        if only_yes_button:

            yes_msg_box = custom_message_box(self,
                                              text = msg,
                                              title = title,
                                              icon = icon,
                                              title_icon = title_icon,
                                              set_flag = set_flag)
            #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
            #yes_no_msg_box.setDetailedText(detail_msg)
            #yes_msg_box.setInformativeText(detail_msg)

            yes_msg_box.addButton(
                #QPushButton(setIc_OK, self.tr(" Ok")),
                QPushButton(self.tr("Ok")),
                #QPushButton(self.tr(" No")),
                yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
            yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

            #self.set_focus(widget_object = line_edit_object_get_focus)

    def create_wait_window(self,
                           text):
        '''
           SUMMARY
           =======
           This method just creates the wait window.

           PARAMETERS
           ==========
           :text (str):    The given text should be shown on the wait window.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create the wait window")

        self.waiting_window = Waiting_Window(text_message = text, show_progress_bar = False, parent = self)
        self.waiting_window.show()

        return

    def add_record(self,
                   **kwargs):
        '''
           SUMMARY
           =======
           This method adds records.

           EXTENTED DISCRIPTION
           ====================
           This method not only adds records, but also edits it.

           PARAMETERS
           ==========
           :kwargs (dict):                  This method expects several arguments, that are saved in the dictionary.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary, but this
                                            statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Add or Edit the record")
        print ""
        print "CombineMovieGeneralDetailsButtons_Window add_record is called successfully"
        print "CombineMovieGeneralDetailsButtons_Window in add_record / kwargs: -> \n {}".format(kwargs)

        #   Fist we have to get current scoped session
        self.get_current_scoped_session_signal.emit()

        dict_store_edit_records = {'film_general_original_title': lambda:
            self.task_thread.work.add_record(id = self.selected_be_edited_id,
                                              film_genre = unicode(kwargs.get('line_edit_obj_film_genre')),
                                              temp_add_list_allocation_film_general_general_country = kwargs.get('temp_add_list_allocation_film_general_general_country'),
                                              temp_del_list_allocation_film_general_general_country = kwargs.get('temp_add_list_allocation_film_general_general_country'),
                                              temp_add_list_allocation_film_general_film_genre = kwargs.get('temp_add_list_allocation_film_general_film_genre'),
                                              temp_del_list_allocation_film_general_film_genre = kwargs.get('temp_del_list_allocation_film_general_film_genre'),
                                              set_flush = False,
                                              category = category),


                                }

        #   First, before we start a thread, we have to quite all running threads.
        ##self.quite_threads()

        ##stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.

        self.task_thread.work = TaskDataManipulation(**kwargs)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        #   Now we need to connect the signals from created subclass, which
        #   is moved to the created thrad, with methods in this main thread.
        self.task_thread.work.error_message_signal.connect(lambda title_text, conn_err, detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                               err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self._image_path_collection.warning_48x48_default))

        self.task_thread.work.create_wait_window_signal.connect(lambda text:
                self.create_wait_window(text = text))

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message_signal.emit)

        if kwargs.get("work_mode") == "add_record":

            self.task_thread.work.confirm_saving_successful_signal.connect(lambda title, msg, detail_msg:
                self.create_generally_information_msg(title = title,
                                                      msg = msg,
                                                      detail_msg = detail_msg,
                                                      yes_no_button = True,
                                                      line_edit_object_get_focus = kwargs.get('line_edit_object_get_focus'),
                                                      only_yes_button = False,
                                                      icon =  kwargs.get('image_path_collection').info_48x48_default,
                                                      set_flag = True))

            self.task_thread.started.connect(self.task_thread.work.add_record)

        elif kwargs.get("work_mode") == "edit_record":

            # self.task_thread.work.sort_model_signal.connect(lambda:
            #     self.sort_model(work_mode = 'edit_record',
            #                     change_page = True,
            #                     page_index = 0,
            #                     stack_widget_object = stack_widget,
            #                     current_obj_clear_selection = True,
            #                     delete_selected_id = True,
            #                     current_obj_clear = True,
            #                     line_edit_object_get_focus = kwargs.get('line_edit_object_get_focus'),
            #                     model = model,
            #                     column = 1))

            self.task_thread.started.connect(self.task_thread.work.edit_record)

        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return


    def resizeEvent(self,
                    resizeEvent):
        '''
            NOTICE:
            =======
            This method is reimplemented and modified to receive widget resize events which are passed in the event parameter.
            When the widget is resized it already has its new geometry.

            Here its looks if the variable named (self.is_focused) is NOT True. By default this variable has the value None.
            If there is None do nothing.

            Next step its calls the function named (calculate_cover_poster), that returns a result, and
            sets the width (w) and the height (h) of the widget everytime: setFixedSize(width, height)

            PARAMETERS:
            ===========
            :event      -       This method gets a resize events.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the fixed size of the widget")

        pass

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._list_threads. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close all opened threads")

        for thread in self._list_threads:
            try:
                #print "ID Thread", id(thread), "running?", thread.isRunning()
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in -place
        del self._list_threads[:]

        #   And then we want the programm will deletes the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event :     By default the given event is accepted and the widget is closed. We
                        get this event when Qt receives a window close request for a top-level
                        widget from the window system. That means, first it sends the widget a QCloseEvent.
                        The widget is closed if it accepts the close event. The default implementation of
                        QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        self.quite_threads()

        self.quite_threads_signal.emit()

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return


if __name__ == "__main__":


    print  """NOTICE: \n======= \nYou can also leave the inputs blank - by pressing just enter.
              """

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    image_pathn_collection = ImagePathnCollection()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()


    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)



    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    LOG_FOLDER_PATH = path.join(BASE_PATH, LOG_FILE_PATH)

    print ""
    print "The log file is saved in", LOG_FOLDER_PATH

    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    create_cover_viewer = None
    create_movie_detail = None
    create_select_details_form = None
    create_error_form = None
    parent=None

    window = CombineMovieGeneralDetailsButtons_Window(None,
                                                        create_cover_viewer,
                                                        create_select_details_form,
                                                        'Add film',
                                                        dict_custom_config,
                                                        image_pathn_collection,
                                                        create_error_form,
                                                        parent)


    #window.resize(600, 400)
    window.showMaximized()
    sys.exit(app.exec_())
