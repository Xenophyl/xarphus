#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path
from sys import exit, argv, exc_info
from traceback import format_exc

from sqlalchemy.exc import SQLAlchemyError, InternalError, UnboundExecutionError, OperationalError

from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from PyQt4.QtCore import Qt, QFile, QRegExp, QObject, pyqtSignal, QThread, QTimer
from PyQt4.QtGui import QDialog, QIcon, QMessageBox, QRegExpValidator, \
        QValidator, QPushButton, QLineEdit, QMovie, QMdiSubWindow, QDesktopWidget, QFileDialog
from PyQt4.uic import loadUi

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################
try:
    from xarphus.core.managed_engine import ManagedEngine
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    #from xarphus.core.managed_defined_session import *
    from xarphus.core.manage_db_connection import SessionScope
    #from xarphus.collection_paths_image import ImagePathnCollection

    from xarphus.frm_error import Error_Window

except ImportError:

    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    ## Import compiled files
    from gui import ui_rc
    from images import images_rc
    from localization_files import translate_files_rc

    from managed_engine import ManagedEngine
    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists
    from core.manage_path import extract_directory
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    #from core.managed_defined_session import *
    from core.manage_db_connection import SessionScope

    from collection_paths_image import ImagePathnCollection
    from frm_error import Error_Window

BASE_PATH = path.dirname(path.abspath(__file__))
#UI_PATH = path.join(BASE_PATH, 'gui', 'db_login.ui')
#UI_PATH = QFile(":/ui_file/db_login.ui")
#self.path_mdi_form = path.abspath(".")
#self.getPath_setting = path.join(path.abspath("."), 'files', "qt_ui", 'pp_setting.ui')

class ConnectionTask(QObject):

    save_engine_scoped_session_signal = pyqtSignal(object, object)
    set_stack_widget_signal = pyqtSignal(int)
    start_progress_bar_signal = pyqtSignal(str)
    log_in_successfully_signal = pyqtSignal()
    log_in_failed_signal = pyqtSignal(str, str, str, bool)

    def __init__(self, dbms = None,
                       dbuser = None,
                       dbuser_pwd = None,
                       db_server_host = None,
                       dbport = None,
                       db_name = None,
                       echo_verbose = False,
                       admin_database = None,
                       configuration_saver_dict = None,
                       image_path_collection = None,
                       parent = None):


        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format('ConnectionTask'))

        self.dbms = dbms
        self.dbuser = dbuser
        self.dbuser_pwd = dbuser_pwd
        self.db_server_host = db_server_host
        self.dbport = dbport
        self.db_name = db_name
        self.echo_verbose = echo_verbose
        self.admin_database = admin_database
        self._configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = image_path_collection


        self._ScopedSession = None
        self._engine = None

    def init_object(self):

        self.start_progress_bar_signal.emit('waiting')
        self.set_stack_widget_signal.emit(1)

        self.timer = QTimer()

        # assoziiert increment() mit TIMEOUT Ereignis
        self.timer.setSingleShot(True)
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.log_in_db)
        self.timer.start()

    def log_in_db(self):

        # self.start_progress_bar_signal.emit('waiting')
        # self.set_stack_widget_signal.emit(1)

        try:

            with ManagedEngine(dbms = self.dbms,
                               dbuser = unicode(self.dbuser),
                               dbuser_pwd = unicode(self.dbuser_pwd),
                               db_server_host = unicode(self.db_server_host),
                               dbport = unicode(self.dbport),
                               db_name = unicode(self.db_name),
                               echo_verbose = self.echo_verbose,
                               configuration_saver_dict = self._configuration_saver_dict,
                               admin_database = self.admin_database) as engine:

                session_factory = sessionmaker()
                self._ScopedSession = scoped_session(session_factory)
                self._engine = engine

                # We should bind engine to your model.
                self._ScopedSession.configure(bind = self._engine)

                self.save_engine_scoped_session_signal.emit(self._engine, self._ScopedSession)


            self.log_in_successfully_signal.emit()

        except InternalError:

            desired_trace = format_exc(exc_info())

            self.start_progress_bar_signal.emit('log_in')
            self.set_stack_widget_signal.emit(0)

            title_text = None

            if not unicode(desired_trace).find("Unknown database") == -1:

                title_text = "Unknown database name!"

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "It doesn't support the requested database name ({database_name_db}). \n\n"
                 "That means, the database name doesn't exist. \n\n"
                 "Please contact system support".format(database_name_db=self.db_name))

            self.log_in_failed_signal.emit(self.tr(title_text), conn_err, '', True)

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        # except OperationalError as OpErr:

        #     self.start_progress_bar_signal.emit('log_in')
        #     self.set_stack_widget_signal.emit(0)

        #     title_text = None

        except SQLAlchemyError:

            desired_trace = format_exc(exc_info())

            self.start_progress_bar_signal.emit('log_in')
            self.set_stack_widget_signal.emit(0)

            title_text = "ERROR..."

            conn_err = desired_trace

            if not unicode(desired_trace).find("Can't connect to") == -1:

                title_text = "Can't connect"

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "Unable to connect for user ({user_db}) to server host ({host_server}) and database name "
                 "({database_name_db}) with port ({port_db}). \n\n"
                 "Be sure that the address is correct and that you have the necessary privileges.".format(user_db=self.dbuser,
                                                        host_server=self.db_server_host,
                                                        port_db=self.dbport,
                                                        database_name_db=self.db_name))
             # "Please... \n "
             # "... check that mysql is running on server: {host_server}. \n"
             # "... check that the name of the database ({database_name_db}) is correct. \n "
             # "... check that mysql is running on port: {port_db}. \n "
             # "... check that the address ({host_server}) is correct. \n "
             # "... check that the user ({user_db}) has the necessary privilees. \n "
             # "... check that your network connection online. \n "
             # "... make sure you have opened the TCP port ({port_db}), when a personal "
             # "firewall is running on your machine.\n "
             # "... make sure you are using the correct username and password.\n \n "
             # "Otherwise, please contact the server administrator.".format(user_db=self.dbuser,
             #                                        host_server=self.db_server_host,
             #                                        port_db=self.dbport,
             #                                        database_name_db=self.db_name))

            if not unicode(desired_trace).find("Access denied for") == -1:

                title_text = "Access denied!"

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "Unable to connect for user ({user_db}) to server host ({host_server}) because access was denied. \n\n"
                 "Double-check your username and password and ensure that access from your current location is "
                 "permitted".format(user_db=self.dbuser, host_server=self.db_name))

            if not unicode(desired_trace).find("Unknown database") == -1:

                title_text = "Unknown database name!"

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "It doesn't support the requested database name ({database_name_db}). \n\n"
                 "Make sure that the path to the database file is correct.".format(user_db=self.dbuser,
                                                                                   database_name_db=self.db_server_host))

            if not unicode(desired_trace).find('unable to open database file') == -1:

                title_text = "Unable to open database file!"

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "The path is invalid \n\n"
                 "Make sure that the path to the database file is correct.".format(user_db=self.dbuser,
                                    database_name_db=self.db_server_host))

            self.log_in_failed_signal.emit(title_text, conn_err, "", True)

            logger.error(desired_trace)

        except UnboundExecutionError as sa_unb_err:

            self.start_progress_bar_signal.emit('log_in')
            self.set_stack_widget_signal.emit(0)

            self.log_in_failed_signal.emit("Can't connect to database server - internal error", "", str(sa_unb_err), True)

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        except Exception as ex:

            self.start_progress_bar_signal.emit('log_in')
            self.set_stack_widget_signal.emit(0)

            self.log_in_failed_signal.emit(self.tr("Error"), "", str(ex), True)

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return



class DB_Login_Window(QDialog):

    save_engine_scoped_session_signal = pyqtSignal(object, object)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/db_login.ui")

        # Create instance variables.
        self._kwargs = kwargs

        self._configuration_saver_dict = self._kwargs.get('configuration_saver_dict')
        self.image_path_collection = self._kwargs.get('image_path_collection')
        self.dbms = self._kwargs.get('dbms')

        # Create attributes.
        self._engine = None
        self._scoped_session = None

        self._path_sqlite_database = None

        self.window_height = None
        self.window_width = None

        self.flags = None

        # Call methods
        self.load_ui_file(ui_path = UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_login = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently the title
            of the window is set and the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Inistialize the GUI")

        self.setWindowModality(Qt.ApplicationModal)

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_login.setWindowFlags(flags)
        self.flags = flags

        self.ui_login.setWindowModality(Qt.ApplicationModal)

        # This code-line shows how to add image to QLabel.
        # Inserting image inside QLabel text
        # and adding image tag with required values.
        # This example also shows how to add http link to QLabel.
        # QLabel in this example has a link to http://www.google.com.
        # If user clicks it, the link will be opened in browser window.
        self.ui_login.label_test.setText("<html><img src=':/img_16x16/img_16x16/About.png'></html> This is a <a href=\"http://www.google.com\">link</a> to Google.")
        self.ui_login.label_test.setOpenExternalLinks(True)

        # QGroupBox
        if self._configuration_saver_dict.dict_set_general_settings["Database"].lower() == "mysql":
            self.ui_login.groupBox_access.setTitle(self.tr("Access data to the server: {}".format(
                self._configuration_saver_dict.dict_set_general_settings["Database"])))
            self.init_textChanged_signal_line_edit()
            self.set_setValidator_method_line_edit()
            self.ui_login.pushButton_connect.setEnabled(False)

        elif self.dbms == "sqlite":
            self.ui_login.groupBox_access.setTitle(self.tr("Connect to: {dbms}".format(dbms = self.dbms)))
            self.ui_login.stackedWidget_dbms.setCurrentIndex(1)
            self.ui_login.lineEdit_sqlite_path.setText(self._configuration_saver_dict.dict_set_general_settings["PathToSQLite"])

            current_profile = self._configuration_saver_dict.dict_set_general_settings.get('current_profile')
            base_dir = self._configuration_saver_dict.dict_set_general_settings.get('BasePath')
            sqlite_file_name = self._configuration_saver_dict.dict_set_general_settings.get('SQLiteFileName') + ".sqlite"

            complete_path_to_sql_file = path.join(base_dir, "profiles",  unicode(current_profile), sqlite_file_name)

            self.ui_login.lineEdit_sqlite_path.setText(complete_path_to_sql_file)

            self.ui_login.pushButton_connect.setEnabled(True)
            self.ui_login.pushButton_clear.hide()


        self.ui_login.setFixedSize(self.ui_login.width(), self.ui_login.height())

        #self.ui_login.show()
        self.init_clicked_signal_push_buttons()
        self.get_geometry()

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :set_flag (bool):        For removing close button from QMessageBox.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
          text = err_msg,
          title = err_title,
          detail_text = detail_msg,
          icon = icon,
          set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         text,
                                         title,
                                         icon = None,
                                         set_flag = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :title (Qstring):        We except a text for the title bar of the message box.

           :text (Qstring) :        We need the text for the main message to be displayed.

           :set_flag (bool):        For removing close button from QMessageBox.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for general information")

        mBox = custom_message_box(self, text = text, title = title, icon = icon, set_flag = set_flag)
        setIc_OK = QIcon(self.image_path_collection.check_icon)
        mBox.addButton(
            QPushButton(setIc_OK, self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        self.ui_login.close()

    def init_textChanged_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set signal and slots for QLineEdit()-objects.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the signal and slots for QLineEdit()-object(s)")

        self.ui_login.lineEdit_sevrer_host.textChanged.connect(self.check_inputs)
        # This QRegExp allows IP-address and letters (ex. 127.0.0.1 or localhost)
        self.ui_login.lineEdit_sevrer_host.setValidator(QRegExpValidator(QRegExp("[aA-zZ0-9\{1,3\}\.0-9\{1,3\}\.0-9\{1,3\}\.0-9\{1,3\}]+"), self.label_server_host))

        self.ui_login.lineEdit_username.textChanged.connect(self.check_inputs)
        # This QRegExp allows username with umlaut
        self.ui_login.lineEdit_username.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.label_user))

        self.ui_login.lineEdit_password.textChanged.connect(self.check_inputs)
        self.ui_login.lineEdit_password.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.lineEdit_password))

        self.ui_login.lineEdit_database_name.textChanged.connect(self.check_inputs)
        self.ui_login.lineEdit_database_name.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.label_dataabase_name))

        self.ui_login.lineEdit_port.textChanged.connect(self.check_inputs)
        self.ui_login.lineEdit_port.setValidator(QRegExpValidator(QRegExp("[0-9]+"), self.label_port))

        return

    def set_setValidator_method_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set setValidator-methods for QLineEdit()-objects.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the setValidator-methods for QLineEdit()-object(s)")

        # This QRegExp allows IP-address and letters (ex. 127.0.0.1 or localhost)
        self.ui_login.lineEdit_sevrer_host.setValidator(QRegExpValidator(QRegExp("[aA-zZ0-9\{1,3\}\.0-9\{1,3\}\.0-9\{1,3\}\.0-9\{1,3\}]+"), self.label_server_host))

        # This QRegExp allows username with umlaut
        self.ui_login.lineEdit_username.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.label_user))

        self.ui_login.lineEdit_password.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.lineEdit_password))

        self.ui_login.lineEdit_database_name.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.label_dataabase_name))

        self.ui_login.lineEdit_port.setValidator(QRegExpValidator(QRegExp("[0-9]+"), self.label_port))

        return

    def open_file_dialog(self,
                         kwargs):
        '''
           SUMMARY
           =======
           In this method we select a file with a QFileDialog - its saves
           in the variables named (self.file_path_front_cover or self.file_path_back_cover).
           Its enables the user to navigate through the file system and select a file to open

           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.
           RETURNS
           =======

           :return:           Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Open the file dialog")


        # We save the caption of the title in the variable named (qfile_dialog_title),
        # because we need it often.

        file_dialog_title = self.tr("Select database")

        file_extension = 'SQLite (*.sqlite)'

        home_path = None

        if path.expanduser(kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFileDialog"]):
            home_path =  path.expanduser(kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFileDialog"])
        else:
            home_path = path.expanduser("~")

        # #     In this condition, we want a variable (in this case a string) named (self.file_path_front_cover).
        # #     Then we ask Qt to open a QFileDialog by using the function named (getOpenFileName), that returns us the
        # #     name of the file that is selected.

        # #     well, we have to pass a couple arguments to it:
        # #     First, we give it the parent widget (in this case, our main window - self),
        # #     then we give it a title, that is saved in the variable above named (qfile_dialog_title)
        # #     for the dialog, then we give it a place to open up to (in this case, the directory is saved in in a dictionary,
        # #     if the path is not saved in the dictionary its opens the user's home directory).
        # #     non-native dialog
        # # '''

        self._path_sqlite_database = unicode(QFileDialog.getOpenFileName(self,
                                                                  file_dialog_title,
                                                                  home_path,
                                                                  file_extension))

        # we check if the user selected a file. If the user did it,
        # the path is returned, otherwise if not
        # (ie, the user cancels the operation) None is returned.
        if self._path_sqlite_database:
            # Yes the path is returned. We set enabled on True.
            self._path_named_profile_folder_old = self._path_sqlite_database

            self.ui_login.lineEdit_sqlite_path.setText(self._path_named_profile_folder_old)

            self._kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFileDialog"] = self._path_sqlite_database


        else:
            #No. The user cancels the operation.
            # So let us take a look, if the old path is still saved.
            if self._path_named_profile_folder_old:
                # Yes the old path ist still saved in there.
                self.ui_login.lineEdit_sqlite_path.setText(self._path_named_profile_folder_old)
            else:
                # No there isn't a old path. Nothing is saved there.
                self.ui_login.lineEdit_sqlite_path.setText("")

        return

    def check_inputs(self):
        #-------------------------------------------------------------------------------
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        color = ''
        #-------------------------------------------------------------------------------
        if state == QValidator.Acceptable:
            self.ui_login.pushButton_connect.setEnabled(
                self.ui_login.lineEdit_sevrer_host.hasAcceptableInput()
                and self.ui_login.lineEdit_username.hasAcceptableInput()
                and self.ui_login.lineEdit_password.hasAcceptableInput()
                and self.ui_login.lineEdit_database_name.hasAcceptableInput()
                and self.ui_login.lineEdit_port.hasAcceptableInput())
         #-------------------------------------------------------------------------------
            color = '#c4df9b' # green
            self.ui_login.lineEdit_password.setEnabled(True)
            self.ui_login.lineEdit_database_name.setEnabled(True)
            self.ui_login.lineEdit_username.setEnabled(True)
            self.ui_login.lineEdit_port.setEnabled(True)
            self.ui_login.lineEdit_sevrer_host.setEnabled(True)
        #-------------------------------------------------------------------------------
        elif state == QValidator.Intermediate:

            color = '#fff79a' # yellow
#            color = css.set_css_test
            self.ui_login.pushButton_connect.setEnabled(False)

        else:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def get_geometry(self):
        '''
            NOTICE:
            =======
            In this method we need to find out the height and width
            of the created GUI.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Get the width and height of the GUI")

        self.window_height = self.ui_login.width()
        self.window_width = self.ui_login.height()

        return

    def reset_qt_obj(self, qt_obj):
        '''
            NOTICE:
            =======
            This method is used to returns all children of this object.

            PARAMETERS:
            ===========
            :qt_obj     -   We except a given name of a qt object to return
                            all children.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Return all children")

        for child in self.findChildren(qt_obj):
            child.clear()

        return

    def stack_set_current_index(self, current_index):
        '''
            NOTICE:
            =======
            This method is used to change the stacked widget with the index of the page.


            PARAMETERS:
            ===========
            :stack_widget   -   We need the QStackedWidget()-object to work with it.

            :current_index  -   That keyxword carries the desired index of the page
                                we want to change to.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the current index of stack widget")

        self._stack_current_index = current_index

        self.stackedWidget_login_in.setCurrentIndex(current_index)

        return

    def save_engine_scoped_session(self, engine, scoped_session):
        '''
            NOTICE:
            =======
            This method is used to save the given session object.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Save the given session object")

        self._engine = engine
        self._scoped_session = scoped_session

        return

    def center_window_on_screen (self):
        '''center_window_on_screen() Centers the window on the screen.

            center_window_on_screen() works by taking the desktop’s
            current resolution and dividing it by 2 to get half of the
            viewport. Next, it takes half of the window size (including the border)
            and subtracts that from the viewport half.
            This provides an origin for the upper-left corner of the window
            to be located such that the window itself is fully “centered.

            Window borders can range from fairly minor (4 pixels around each
            side adding a total of 8 pixels horizontally and vertically) to
            imposing (16 pixels or more). Unfortunately, QWidget.geometry()
            and friends only return a QSize object describing the geometry of
            the contained window rather than the containing window. It seems
            insignificant, sure, but if you want pixel-perfect alignment,
            you have to use something else: QWidget.frameSize(), because
            frameSize() will return the size of the entire window, border included
        '''
        resolution = QDesktopWidget().screenGeometry()
        self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
                  (resolution.height() / 2) - (self.frameSize().height() / 2))

    def create_login_connection_thread(self,
                                       kwargs,
                                       server_host = None,
                                       username = None,
                                       password = None,
                                       database_name = None,
                                       port = None):
        '''
           SUMMARY
           =======
           In this method the gui is inistialized. Currently the title
           of the window is set and the flag of this window is flagged.

           PARAMETERS
           ==========
           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                              of arguments. We used it because we want to handle named arguments in
                              this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start LogIn to Datatbase")

        # server_host = unicode(self.ui_login.lineEdit_sevrer_host.text())
        # username = unicode(self.ui_login.lineEdit_username.text())
        # password = unicode(self.ui_login.lineEdit_password.text())
        # database_name = unicode(self.ui_login.lineEdit_database_name.text())
        # port = unicode(self.ui_login.lineEdit_port.text())

        dbms_driver = None
        dbm_system = None#

        try:
            task_thread = QThread(self)
            task_thread.work = ConnectionTask(
              dbms = kwargs.get('safe_config_parser').get('General_Configuration', 'database_type').lower(),
              dbuser = unicode(username),
              dbuser_pwd = unicode(password),
              db_server_host = unicode(server_host),
              dbport = unicode(port),
              db_name = unicode(database_name),
              echo_verbose = False,
              admin_database = self.ui_login.checkBox_administrate_database.isChecked(),
              configuration_saver_dict = kwargs.get('configuration_saver_dict'),
              image_path_collection = kwargs.get('image_path_collection'),
              parent = None)

            task_thread.work.moveToThread(task_thread)

            task_thread.work.start_progress_bar_signal.connect(self.resize_the_form)
            task_thread.work.set_stack_widget_signal.connect(self.stack_set_current_index)

            task_thread.work.log_in_successfully_signal.connect(
              lambda:
              self.create_generally_information_msg(
                title = self.tr('Information'),
                text = self.tr("Connection was successful. You are connected."),
                icon = self.image_path_collection.check_circle_48x48_default,
                set_flag = True))

            task_thread.work.log_in_failed_signal.connect(
              lambda
              err_title,
              err_msg,
              detail_msg,
              set_flag:
              self.create_generally_crritcal_msg(
                err_title = err_title,
                err_msg = err_msg,
                detail_msg = detail_msg,
                icon = self.image_path_collection.check_circle_48x48_default,
                set_flag = set_flag) )

            task_thread.work.save_engine_scoped_session_signal.connect(self.save_engine_scoped_session)

            task_thread.started.connect(task_thread.work.init_object)
            task_thread.finished.connect(task_thread.deleteLater)

            task_thread.start()

        except TypeError as err:

            print str(err)

        except AttributeError as aterr:
            print str(aterr)

        return

    def resize_the_form(self, view_modus):
        '''
            NOTE:
            ======
            This method sets the minimum, and maximum values.

            PARAMETERS:
            ===========
            :view_modus     -   This argument tell us, what to show.
                                When we get:
                                - 'waiting' we have to show a certain
                                  stackedWidget-page on which the QProgressBar
                                  is to be viewed, with the message that the user
                                  should wait.

                                - 'log_in' - says, that there are problems, and
                                  the user isn't connected to the database successfully.
                                  That means, the user gets error messages, so we have
                                  to switch the stackedWidget-page back to the Log-In interface.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.

        '''
        logger.info("Set the values of the QProgressBar()-object")

        if view_modus == "waiting":
            '''
                            (width, height)
            '''
            if  self.dbms == "sqlite":
                self.ui_login.stackedWidget_dbms.setCurrentIndex(0)

            self.setFixedSize(300, 150)

            self.on_start_progressbar()

        elif view_modus == "log_in":
            '''
                            (width, height)
            '''
            if  self.dbms == "sqlite":
                self.ui_login.stackedWidget_dbms.setCurrentIndex(1)

            self.setFixedSize(self.window_width, self.window_height)

        self.center_window_on_screen()

    def on_start_progressbar(self):
        '''
            NOTE:
            ======
            This method sets the minimum, and maximum values
            with a single function call.

            We set minimum and maximum to 0, so
            the progress bar will show a busy indicator instead of a
            percentage of steps. We do not know how long the registration takes.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Set the values of the QProgressBar()-object")

        self.ui_login.progressBar_wait.setRange(0, 0)

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        self.ui_login.toolButton_edit_saved_connect_3.clicked.connect(
          lambda:
          self.resize_the_form('waiting'))

        self.ui_login.pushButton_connect.clicked.connect(
          lambda: self.create_login_connection_thread(
            server_host = unicode(self.ui_login.lineEdit_sevrer_host.text()),
            kwargs = self._kwargs))

        self.ui_login.pushButton_clear.clicked.connect(
          lambda:
          self.reset_qt_obj(QLineEdit))

        self.ui_login.pushButton_browse_sqlite.clicked.connect(
          lambda:
          self.open_file_dialog(kwargs = self._kwargs))

        self.ui_login.pushButton_cancel.clicked.connect(self.close)

        return

    def keyPressEvent(self, event):
        '''
        Did the user press the Escape key?
        Note: QtCore.Qt.Key_Escape is a value that equates to
        what the operating system passes to python from the
        keyboard when the escape key is pressed.
        '''
        pass

    def closeEvent(self, event):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:

            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_login.close()

        self.save_engine_scoped_session_signal.emit(self._engine, self._scoped_session)

        return

if __name__ == "__main__":

    print  """NOTICE: \n======= \nYou can also leave the inputs blank - by pressing just enter.
              """

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    configuration_saver_dict = ConfigurationSaverDict()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()


    configuration_saver_dict.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    get_configuration(configuration_saver_dict.dict_set_general_settings["PathToINISetting"], configuration_saver_dict, default_ini_file)


    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)



    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    LOG_FOLDER_PATH = path.join(BASE_PATH, LOG_FILE_PATH)

    print ""
    print "The log file is saved in", LOG_FOLDER_PATH

    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

    image_pathn_collection = ImagePathnCollection()

    app = QApplication(argv)

    translator = QTranslator()
    translator_path = ":/translators/german_sie.qm"
    translator.load(translator_path)
    app.installTranslator(translator)

    window = DB_Login_Window(configuration_saver_dict=configuration_saver_dict,
                            image_pathn_collection=image_pathn_collection)


    #window.resize(600, 400)
    window.showMaximized()
    exit(app.exec_())


