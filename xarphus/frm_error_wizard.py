#!/usr/bin/env python
# -*- coding: utf-8 -*-

import inspect
import shutil

from sys import argv, exit, exc_info
from traceback import format_exc

from os import path

from os.path import expanduser

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtGui import QApplication, QWizard, QWizardPage, QPixmap, QLabel, \
     QRadioButton, QVBoxLayout, QLineEdit, QGridLayout, \
     QRegExpValidator, QCheckBox, \
     QMessageBox, QFileDialog, QTextCursor, QTextCharFormat, \
     QFont, QStandardItemModel, QStandardItem

from PyQt4.uic import loadUi

from PyQt4.QtCore import pyqtSlot, QRegExp, Qt, QFile, QObject, QThread, pyqtSignal, QString

try:
    from xarphus.core.config import ConfigurationSaverDict
    from info import info_app
    from xarphus.core.manage_time import get_time_now
    from xarphus.core.manage_ini_file import get_configuration, create_write_ini_file
    from xarphus.core.manage_ini_file import DefaultINIFile
    from xarphus.core.manage_ini_file import write_in_configuration
    from xarphus.core.manage_file import copy_file_to, copy_folder_to
    from xarphus.core.restart import restart_program
    from xarphus.core.manage_input_controller import InputController
    from xarphus.core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, create_folder, is_folder_exists

except ImportError:

    from core.config import ConfigurationSaverDict
    from info import info_app
    from core.manage_time import get_time_now
    from core.manage_ini_file import get_configuration, create_write_ini_file
    from core.manage_ini_file import DefaultINIFile
    from core.manage_ini_file import write_in_configuration
    from core.manage_file import copy_file_to, copy_folder_to
    from core.restart import restart_program
    from core.manage_input_controller import InputController
    from core.manage_folder import generator_top_sub_folder, mkdir_top_sub_folder, create_folder, is_folder_exists

#from xarphus.gui import ui_rc

CURRENT_MAIN_WIZARD = None
SUB_TITLE = ''

BASE_PATH = None

class MainMissingProfileWizard(QWizard):

    NUM_PAGES = 9

    (PageIntro,
     PageProfile,
     PageINISetting,
     PageSelectLanguage,
     PageSelectDatabaseSystem,
     PageDesktopDatabaseSystem,
     PageServerDatabaseSystem,
     PageSummary,
     PageGettingFinied) = range(NUM_PAGES)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizard.__init__(self, parent)

        global CURRENT_MAIN_WIZARD
        global BASE_PATH
        global SUB_TITLE

        CURRENT_MAIN_WIZARD = self

        self._kwargs = kwargs

        print "MainMissingProfileWizard", self._kwargs

        # The text format of this sub title is HTML.
        SUB_TITLE = "<font size='5'><b>Failed to find profile</b></font>"

        self.error_text = "Your profile was removed or the path for the profile folder is invalid. " \
                          "\n\nThere are two possible reasons why you get this message. First, its seems you start this " \
                          "program for the first time on this computer. Second, your profile path was moved or deleted. " \
                          "In order to use this program, you need to first set it up property." \
                          "\n\nThis wizard help you adjust the most important settings. That wiil guide you along the configuration " \
                          "process quickly, step by step before its first use. "\
                          "\n\nClick Next to start the configuration."


        BASE_PATH = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"]

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        logger.info("Creating an instance of the class of ProfileWizard()")

        self.setPage(self.PageProfile, ProfileWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                     parent = self))

        logger.info("Creating an instance of the class of IntroPageWizard()")

        self.setPage(self.PageIntro, IntroPageWizard(error_text = self.error_text, parent = self))

        logger.info("Creating an instance of the class of INISettingWizard()")

        self.setPage(self.PageINISetting, INISettingWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                           parent = self))

        logger.info("Creating an instance of the class of LanguageWizard()")

        self.setPage(self.PageSelectLanguage, LanguageWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                             parent = self))

        logger.info("Creating an instance of the class of DatabaseSystemWizard()")

        self.setPage(self.PageSelectDatabaseSystem, DatabaseSystemWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                                         parent = self))

        logger.info("Creating an instance of the class of DesktopDatabaseSystemWizard()")

        self.setPage(self.PageDesktopDatabaseSystem, DesktopDatabaseSystemWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                                 parent = self))

        logger.info("Creating an instance of the class of ServerDatabaseSystemWizard()")

        self.setPage(self.PageServerDatabaseSystem, ServerDatabaseSystemWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                               parent = self))

        self.action_wizard_button()

        logger.info("Creating an instance of the class of SummaryWizard()")

        self.setPage(self.PageSummary, SummaryWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                     configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                     parent = self))

        logger.info("Creating an instance of the class of GettingFinishedWizard()")

        self.setPage(self.PageGettingFinied, GettingFinishedWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                   default_ini_file = self._kwargs.get('default_ini_file'),
                                                                   configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                                   path_ini_file = self._kwargs.get('path_ini_file'),
                                                                   parent = self))

        logger.info("Set start id of wizard page")

        self.setStartId(self.PageIntro)

        logger.info("Set start id of wizard page was set")

        # images won't show in Windows 7 if style not set
        self.setWizardStyle(self.ModernStyle)
        #self.setOption(self.HaveHelpButton, True)
        self.setPixmap(QWizard.LogoPixmap, QPixmap(":/images/logo.png"))

        # set up help messages
        self._lastHelpMsg = ''
        #self._helpMsgs = self._createHelpMsgs()
        #self.helpRequested.connect(self._showHelp)

        self.setWindowTitle(self.tr("Error"))

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    @pyqtSlot()
    def _showHelp(self):
        # get the help message for the current page
        msg = self._helpMsgs[self.currentId()]

        # if same as last message, display alternate message
        if msg == self._lastHelpMsg:
            msg = self._helpMsgs[self.NUM_PAGES + 1]

        QMessageBox.information(self,
                                self.tr("License Wizard Help"),
                                msg)
        self._lastHelpMsg = msg

    def action_wizard_button(self):
        self.button(QWizard.CancelButton).clicked.connect(self.close)

    def accept(self):
        '''
            Notice
            ------
            The super() method returns the parent object of the MainMissingProfileWizard-class.
            Finish and close window
        '''
        super(MainMissingProfileWizard, self).accept()

        restart_program(self)
        self.close()

    def closeEvent(self, event):
        exit()

class MainMissingINIFileWizard(QWizard):

    NUM_PAGES = 8

    (PageIntro,
     PageINISetting,
     PageSelectLanguage,
     PageSelectDatabaseSystem,
     PageDesktopDatabaseSystem,
     PageServerDatabaseSystem,
     PageSummary,
     PageGettingFinied) = range(NUM_PAGES)

    # len(tuple)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizard.__init__(self, parent)

        global CURRENT_MAIN_WIZARD
        global BASE_PATH
        global SUB_TITLE

        self._kwargs = kwargs

        print "MainMissingINIFileWizard", self._kwargs

        CURRENT_MAIN_WIZARD = self

        # The text format of this sub title is HTML.
        SUB_TITLE = "<font size='5'><b>Failed to find ini file</b></font>"

        self.error_text = "Your ini file was removed or the path for the ini file is invalid. " \
                          "\n\nThe possible reason why you get this message. Your ini file path was moved or deleted. " \
                          "In order to use this program, you need to first set it up property." \
                          "\n\nThis wizard help you adjust the most important settings. That wiil guide you along the configuration " \
                          "process quickly, step by step before its first use. "\
                          "\n\nClick Next to start the configuration."

        BASE_PATH = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"]


        profile_name = path.basename(path.normpath(self._kwargs.get("profile_name")))

        self._kwargs.get('safe_config_parser').set('General_Configuration', 'current_profile', unicode(profile_name))



        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        logger.info("Creating an instance of the class of IntroPageWizard()")

        self.setPage(self.PageIntro, IntroPageWizard(error_text = self.error_text, parent = self))

        logger.info("Creating an instance of the class of INISettingWizard()")

        self.setPage(self.PageINISetting, INISettingWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                           parent = self))

        logger.info("Creating an instance of the class of LanguageWizard()")

        self.setPage(self.PageSelectLanguage, LanguageWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                             parent = self))

        logger.info("Creating an instance of the class of DatabaseSystemWizard()")

        self.setPage(self.PageSelectDatabaseSystem, DatabaseSystemWizard(configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                                         parent = self))

        logger.info("Creating an instance of the class of DesktopDatabaseSystemWizard()")

        self.setPage(self.PageDesktopDatabaseSystem, DesktopDatabaseSystemWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                                 parent = self))

        logger.info("Creating an instance of the class of ServerDatabaseSystemWizard()")

        self.setPage(self.PageServerDatabaseSystem, ServerDatabaseSystemWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                               parent = self))

        self.action_wizard_button()

        logger.info("Creating an instance of the class of SummaryWizard()")

        self.setPage(self.PageSummary, SummaryWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                     configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                     parent = self))

        logger.info("Creating an instance of the class of GettingFinishedWizard()")

        self.setPage(self.PageGettingFinied, GettingFinishedWizard(safe_config_parser = self._kwargs.get('safe_config_parser'),
                                                                   default_ini_file = self._kwargs.get('default_ini_file'),
                                                                   configuration_saver_dict = self._kwargs.get('configuration_saver_dict'),
                                                                   path_ini_file = self._kwargs.get('path_ini_file'),
                                                                   parent = self))

        logger.info("Set start id of wizard page")

        self.setStartId(self.PageIntro)

        logger.info("Set start id of wizard page was set")

        # images won't show in Windows 7 if style not set
        self.setWizardStyle(self.ModernStyle)
        #self.setOption(self.HaveHelpButton, True)
        self.setPixmap(QWizard.LogoPixmap, QPixmap(":/images/logo.png"))

        # set up help messages
        self._lastHelpMsg = ''
        #self._helpMsgs = self._createHelpMsgs()
        #self.helpRequested.connect(self._showHelp)

        self.setWindowTitle(self.tr("Error"))

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    @pyqtSlot()
    def _showHelp(self):
        # get the help message for the current page
        msg = self._helpMsgs[self.currentId()]

        # if same as last message, display alternate message
        if msg == self._lastHelpMsg:
            msg = self._helpMsgs[self.NUM_PAGES + 1]

        QMessageBox.information(self,
                                self.tr("License Wizard Help"),
                                msg)
        self._lastHelpMsg = msg

    def action_wizard_button(self):
        self.button(QWizard.CancelButton).clicked.connect(self.close)

    def accept(self):
        '''
            Notice
            ------
            The super() method returns the parent object of the MainMissingINIFileWizard-class.
            Finish and close window
        '''
        super(MainMissingINIFileWizard, self).accept()

        restart_program(self)
        self.close()

    def closeEvent(self, event):
        exit()

class IntroPageWizard(QWizardPage):
    '''
           SUMMARY
           =======
           This is the first user interface, since the user gets a view before the actual wizard is started.
    '''
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self._kwargs = kwargs

        UI_PATH = QFile(":/ui_file/IntroWizard.ui")

        self.load_ui_file(UI_PATH)

        self.set_sub_title()

        self.init_qlable(text = self._kwargs.get('error_text'))

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load the .ui file inside an instance.
        self.ui_intro_page = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_qlable(self, text):
        '''
           SUMMARY
           =======
           In this method, we set the text on given QLabel.

           PARAMETERS
           ==========
           :text (str): We get a text to display it.

           RETURNS
           =======

           :return:     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the text on QLabel")

        self.ui_intro_page.label_intro_erro_text.setText(text)

        return

    def set_sub_title(self):
        '''
           SUMMARY
           =======
           This method holds the sub title of the page.
           Here the subtitle is shown by the QWizard, between the title and the actual page

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the sub title")

        self.setSubTitle(SUB_TITLE)

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

        return

class ProfileWizard(QWizardPage):
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        self._kwargs = kwargs

        self.input_controller = InputController()

        self._path_named_profile_folder_old = None

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/CreateProfile.ui")

        self.load_ui_file(UI_PATH)

        self.setSubTitle("<font size='5'><b>Managing your profile</b></font>")

        self.init_ui()

        self.register_fields()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_profile = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.ui_profile.lineEdit.hide()

        self.init_textChanged_signal_line_edit()

        self.ui_profile.radioButton_use_exiting_profile_folder.toggled.connect(
          self.set_default_exists_profile_area)

        self.ui_profile.radioButton_use_exiting_profile_folder.toggled.connect(lambda:
          self.set_enable_widget(
            radio_button = self.ui_profile.radioButton_use_exiting_profile_folder,
            widget = self.ui_profile.toolButton_open_folder))

        ###########################################################
        ###########################################################
        ###########################################################
        self.ui_profile.radioButton_create_new_profile_folder.toggled.connect(lambda:
          self.set_enable_widget(
            radio_button = self.ui_profile.radioButton_create_new_profile_folder,
            widget = self.ui_profile.lineEdit_profile_name))

        self.ui_profile.radioButton_create_new_profile_folder.toggled.connect(
          self.set_default_new_profile_area)

        self.ui_profile.toolButton_open_folder.clicked.connect(self.open_directory_dialog)

        return

    def register_fields(self):

        '''
           SUMMARY
           =======
           Field Validation - Fields are only validated if they are registered as mandatory -
           (denoted by an asterisk after the name).


           In this case, the field must be filled before the user can advance to the next page.

           That means the next button on this WizardPage will disable until the registered
           QLineEdit is filled and validated

           DETAILS:
           ========
           If we want to validate the fields - by registering it as mandatory -its means all fields have to filled up.
           In this case, we only want to fill up one of this fields - not all. So we need to install a third field,
           that is registered as mandory. That means, the third field is filled up by changing text of the others fields.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Register the fields")

        self.registerField('lineEdit*', self.ui_profile.lineEdit)

        return

    def set_default_new_profile_area(self):
        '''
           SUMMARY
           =======
           In this method we select a file with a QFileDialog - its saves
           in the variables named (self.file_path_front_cover or self.file_path_back_cover).
           Its enables the user to navigate through the file system and select a file to open

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set up default on new profile")

        self.ui_profile.lineEdit_profile_folder_path.clear()

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToCopyProfileFolder_Temp'] = None

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToProfileFolder'] = None

        self.ui_profile.lineEdit.clear

        self.ui_profile.lineEdit_profile_name.setFocus()

        return

    def set_default_exists_profile_area(self):
        '''
           SUMMARY
           =======
           In this method we select a file with a QFileDialog - its saves
           in the variables named (self.file_path_front_cover or self.file_path_back_cover).
           Its enables the user to navigate through the file system and select a file to open

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set up default on new profile")

        self.ui_profile.lineEdit_profile_name.clear()

        self.ui_profile.lineEdit.clear

        return

    def open_directory_dialog(self):
        '''
           SUMMARY
           =======
           In this method we select a folder with a QFileDialog - its saves
           in the variables named (self._path_named_profile_folder).
           Its enables the user to navigate through the folder system and select a folder to open

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Open the directory dialog")


        # We save the caption of the title in the variable named (qfile_dialog_title),
        # because we need it often.

        file_dialog_title = self.tr("Select Directory")

        home_path = None

        if path.expanduser(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFolderDialog"]):
            home_path =  path.expanduser(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFolderDialog"])
        else:
            home_path = expanduser("~")

        #     In this condition, we want a variable (in this case a string) named (self._path_named_profile_folder).
        #     Then we ask Qt to open a QFileDialog by using the function named (getExistingDirectory), that returns us the
        #     name of the folder that is selected.

        #     well, we have to pass a couple arguments to it:
        #     First, we give it the parent widget (in this case, our main window - self),
        #     then we give it a title, that is saved in the variable above named (qfile_dialog_title)
        #     for the dialog, then we give it a place to open up to (in this case, the directory is saved in in a dictionary,
        #     if the path is not saved in the dictionary its opens the user's home directory).
        #     non-native dialog
        # '''

        # Before we pop up a file dialog we have to check if the folder is already exists.
        # In the variable named (home_path) the path to the folder is saved.
        if is_folder_exists(home_path):
            # Yes the folder already exists. We don't have to do anything.
            pass
        else:
            # There is a problem. The folder doesn't exists. We have to overwirte
            # the variable named (home_path). In this case we save a simple
            # path seperator. So the file dialoge will pop up with a home path.
            home_path = expanduser("~")

        self._path_named_profile_folder = unicode(QFileDialog.getExistingDirectory(self,
                                                                  file_dialog_title,
                                                                  home_path,
                                                                  QFileDialog.DontUseNativeDialog))

        # we check if the user selected a folder. If the user did it,
        # the path is returned, otherwise if not
        # (ie, the user cancels the operation) None is returned.
        if self._path_named_profile_folder:
            # Yes the path is returned. We set enabled on True.
            self._path_named_profile_folder_old = self._path_named_profile_folder

            self.ui_profile.lineEdit_profile_folder_path.setText(self._path_named_profile_folder_old)
            self.ui_profile.lineEdit.setText(self._path_named_profile_folder_old)

            self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToCopyProfileFolder_Temp'] = self._path_named_profile_folder_old

            self._kwargs.get('configuration_saver_dict').dict_set_general_settings["LastPathFolderDialog"] = path.dirname(self._path_named_profile_folder)

            self._kwargs.get('configuration_saver_dict').dict_set_general_settings['BaseSelectedProfileFolder'] = path.basename(self._path_named_profile_folder)

        else:
            #No. The user cancels the operation.
            # So let us take a look, if the old path is still saved.
            if self._path_named_profile_folder_old:
                # Yes the old path ist still saved in there.
                self.ui_profile.lineEdit_profile_folder_path.setText(self._path_named_profile_folder_old)
                self.ui_profile.lineEdit.setText(self._path_named_profile_folder_old)
            else:
                # No there isn't a old path. Nothing is saved there.
                self.ui_profile.lineEdit_profile_folder_path.setText("")
                self.ui_profile.lineEdit.setText("")

        return

    def save_profile_name(self,
                          profile_name,
                          section_name = 'General_Configuration',
                          vale_name = 'current_profile'):
        '''
           SUMMARY
           =======
           When the user chooses to create a new profile folder, its necessary to
           activiate the QToolButton again.

           PARAMETERS
           ==========
           :radio_button (class): Here, we want a QWidget()-object: QRadioButton.
                                  Well, we need this object to manage the other given widgets

           :tool_button (class):  Here, we want a QWidget()-object: QToolButton.
                                  Well, we need this object to manage the other given widgets

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the given profile name")

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"] = profile_name

        return

    def set_enable_widget(self,
                            radio_button,
                            widget):
        '''
           SUMMARY
           =======
           When the user chooses to create a new profile folder, its necessary to
           activiate the QToolButton again.

           PARAMETERS
           ==========
           :radio_button (class): Here, we want a QWidget()-object: QRadioButton.
                                  Well, we need this object to manage the other given widgets

           :tool_button (class):  Here, we want a QWidget()-object: QToolButton.
                                  Well, we need this object to manage the other given widgets

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("set enabled the QToolbutton")

        if radio_button.isChecked():
          widget.setEnabled(True)
        else:
          widget.setEnabled(False)

        return

    def init_textChanged_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set signal and slots of textChanged for QLineEdit()-objects.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize textChanged-signal for QLineEdit")

        self.ui_profile.lineEdit_profile_name.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_profile.lineEdit_profile_name,
                                     max_length = 20))

        self.ui_profile.lineEdit_profile_folder_path.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_profile.lineEdit_profile_folder_path,
                                     max_length = len(self._path_named_profile_folder)))

        self.ui_profile.lineEdit_profile_name.textChanged.connect(lambda:
          self.ui_profile.lineEdit.setText(self.ui_profile.lineEdit_profile_name.text() ))


    def handle_text_changed(self,
                            line_edit_obj,
                            max_length,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False,
                            check_selected_id = True):
        '''
           SUMMARY
           =======
           This method is used to control the input in given QLineEdit().

           PARAMETERS
           ==========
           :line_edit_obj (QLineEdit):              Here, we want a QWidget()-object: QLineEdit.
                                                    Well, we need this object to get the current position
                                                    of the cursor.

           :max_length (int):                       This keyword-argument gets an integer for setting a maximum length
                                                    of a given text.


           :change_windows_title (bool):            In this keyword argument we except a special value: boolean value.
                                                    By default, the value is set to False, because we don't want the
                                                    window title to change.

           :allow_trailing_spaces (bool):           We except a boolean value. By default, its set to True.
                                                    True means that spaces between the each words are allowed.
                                                    It is very rare that we want to ban trailing space.

           :allow_only_number (bool):               We except a boolean value. When we get True we
                                                    want to know we shoukd allow only numbers. By default
                                                    the value is set to False, because we want the user to
                                                    enter all characters.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Control the input")

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text=unicode(line_edit_obj.text()),
                                                                      pos = line_edit_obj.cursorPosition(),
                                                                      allow_trailing_spaces=allow_trailing_spaces,
                                                                      allow_only_number=allow_only_number,
                                                                      max_length=max_length)



        self.set_text(widget = line_edit_obj, text = get_text)
        self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if get_text:
            '''
                No, the string isn't empty
            '''
            self.set_text(widget = line_edit_obj, text = get_text)
            self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        else:
            '''
                Bad news, the string is emptyx.
            '''

            self.current_selected_id = None

        return

    def set_text(self,
                 widget = None,
                 text = None):
        '''
           SUMMARY
           =======
           Updates the input each time a key is pressed.

           PARAMETERS
           ==========
           :widget (Qt-Object): On which qt object should be shown the text?

           :text (unicode):     We need text to set it.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the text given Qt-object.")

        if isinstance(widget, QLineEdit):
            widget.setText(text)

            self.save_profile_name(profile_name = text)

        return

    def set_cursor_position(self,
                            widget,
                            pos):
        '''
           SUMMARY
           =======
           This method is used to set the cursor position.

           PARAMETERS
           ==========
           :widget (QLineEdit):     We expect a given QLineEdit()-Widget where we set the cursor position.

           :pos (int):              We need a int value to set the cursor position.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the cursor position in QLineEdit()")

        if isinstance(widget, QLineEdit):
            widget.setCursorPosition(pos)

        return

    def initializePage(self):
        '''
           SUMMARY
           =======
           In this reimplemented function, we need to manage fields on this page.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Check the fields")

        saved_path = unicode(self.ui_profile.lineEdit_profile_folder_path.text())
        saved_profile_name = unicode(self.ui_profile.lineEdit_profile_name.text())

        if saved_path:
            self.ui_profile.lineEdit.setText(saved_path)
        elif saved_profile_name:
            self.ui_profile.lineEdit.setText(saved_profile_name)

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

        return

    def nextId(self):

        if self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToCopyProfileFolder_Temp']:
            return CURRENT_MAIN_WIZARD.PageSummary
        else:
            return CURRENT_MAIN_WIZARD.PageINISetting

        return

class LanguageWizard(QWizardPage):

    def __init__(
        self,
        parent = None,
        **kwargs):

        QWizardPage.__init__(self, parent)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Your language</b></font>")

        self.setWindowTitle(self.tr("Select language"))

        UI_PATH = QFile(":/ui_file/SelectLanguage.ui")

        self.load_ui_file(UI_PATH)

        self.init_ui()
        self.set_actions_comboxes()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(
        self,
        ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_select_language = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.registerField("comboBox_language*", self.ui_select_language.comboBox_language)

        return

    def set_actions_comboxes(self):

        self.ui_select_language.comboBox_language.currentIndexChanged.connect(
          lambda:
          self.combo_language_changed(
            combo_id = self.ui_select_language.comboBox_language.currentIndex()))

        return

    def change_language(
        self,
        combo_id,
        language_value,
        kwargs):
        '''
           SUMMARY
           =======
           This method updates the given safe config parser for language.

           PARAMETERS
           ==========
           :combo_id (int):               We get the ID of choosen language.

           :language_value (str):         We get the choosen language.

           :kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the safe config parser object in language")

        if not language_value == 'None':

            kwargs.get('configuration_saver_dict').language = language_value      #[setting_key_language] = language_value
            kwargs.get('configuration_saver_dict').language_id = str(combo_id)     #[setting_key_language_id] = str(combo_id)

        return

    def combo_language_changed(
        self,
        combo_id = 0):
        '''
           SUMMARY
           =======
           In this method, a dictionary is stored with the associated methods,
           which are selected from the dictionary and called up accordingly.

           PARAMETERS
           ==========
           :combo_id (int): We get the ID of choosen language.

           RETURNS
           =======

           :return:           Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the safe config parser object in language")


        try:
            dict_ids = {
            0: lambda: self.change_language(
                kwargs = self._kwargs,
                language_value = "None",
                combo_id = int(combo_id)),
            1: lambda: self.change_language(
                kwargs = self._kwargs,
                language_value = "german_sie",
                combo_id = int(combo_id)),
            2: lambda: self.change_language(
                kwargs = self._kwargs,
                language_value = "german_du",
                combo_id = int(combo_id)),
            3: lambda: self.change_language(
                kwargs = self._kwargs,
                language_value = "english_uk",
                combo_id = int(combo_id)),
            4: lambda: self.change_language(
                kwargs = self._kwargs,
                language_value = "english_us",
                combo_id = int(combo_id)),
                        }

            dict_ids[int(combo_id)]()

        except KeyError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

        return

class INISettingWizard(QWizardPage):
    '''
           SUMMARY
           =======
           This class shows the user interface in which the user can choose whether
           to select an existing INI file or to create a new INI file.
    '''

    def __init__(self,
                 configuration_saver_dict,
                 parent = None):
        QWizardPage.__init__(self, parent)

        self._configuration_saver_dict = configuration_saver_dict

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Managing configuration file</b></font>")

        UI_PATH = QFile(":/ui_file/INIFileOptions.ui")

        self.load_ui_file(UI_PATH)

        self.init_ui()
        self.register_fields()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_options = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        #self.ui_options.radioButton_use_exitingINI.toggled.connect(self.enabled_line_edit)

        self.ui_options.radioButton_use_exitingINI.toggled.connect(lambda: self.ui_options.lineEdit_ini_file_path.setEnabled(True))
        self.ui_options.radioButton_use_exitingINI.toggled.connect(lambda: self.ui_options.toolButton_open_file.setEnabled(True))
        self.ui_options.radioButton_use_exitingINI.toggled.connect(lambda: self.ui_options.lineEdit_ini_file_path.clear() )


        self.ui_options.radioButton_CreateNewINI.toggled.connect(lambda: self.ui_options.lineEdit_ini_file_path.setEnabled(False))
        self.ui_options.radioButton_CreateNewINI.toggled.connect(lambda: self.ui_options.toolButton_open_file.setEnabled(False))
        self.ui_options.radioButton_CreateNewINI.toggled.connect(lambda: self.ui_options.lineEdit_ini_file_path.clear())

        self.ui_options.radioButton_CreateNewINI.toggled.connect(self.check_radio_button)
        self.ui_options.toolButton_open_file.clicked.connect(self.open_directory_dialog)

        return

    def register_fields(self):

        '''
           SUMMARY
           =======
           Field Validation - Fields are only validated if they are registered as mandatory.
           In this case, the field must be filled before the user can advance to the next page.

           That means the next button on this WizardPage will disable until the QLineEdit is filled and validated

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Register the fields")

        self.registerField('lineEdit_ini_file_path*', self.ui_options.lineEdit_ini_file_path)

        return

    def check_radio_button(self):

        '''
           SUMMARY
           =======
           When the user chooses to create a new ini_file, its necessary to
           activiate the next_button again. Thats why the function activates the
           nex_button on this WizardPage.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Enable the wizard next button")

        # The next_button can be activated, the QLineEdit has to be filled with a space.
        if self.ui_options.radioButton_CreateNewINI.isChecked():
            self.ui_options.lineEdit_ini_file_path.setText(" ")
            self._configuration_saver_dict.dict_set_general_settings["PathToCopyINISetting_Temp"] = None

        return


    def check_copy_ini_file(self):

        if self._configuration_saver_dict.dict_set_general_settings["PathToCopyINISetting_Temp"]:
            return True
        else:
            return False

        return

    def nextId(self):

        result = self.check_copy_ini_file()

        if result:
            return CURRENT_MAIN_WIZARD.PageSummary
        else:
            return CURRENT_MAIN_WIZARD.PageSelectLanguage

        return


    def open_directory_dialog(self):

        filename = QFileDialog.getOpenFileName(None, 'Open File', '/')
        self.ui_options.lineEdit_ini_file_path.setText(unicode(filename))
        self._configuration_saver_dict.dict_set_general_settings["PathToCopyINISetting_Temp"] = unicode(filename)

        return

    def enabled_line_edit(self):

        if self.ui_options.radioButton_use_exitingINI.isChecked():
            self.ui_options.lineEdit_ini_file_path.setEnabled(True)
            self.ui_options.toolButton_open_file.setEnabled(True)
            self.ui_options.lineEdit_ini_file_path.clear()
        else:
            self.ui_options.lineEdit_ini_file_path.setEnabled(False)
            self.ui_options.toolButton_open_file.setEnabled(False)

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

            self.wizard().setButtonText(QWizard.NextButton, self.tr("Next >"))

        return

class DatabaseSystemWizard(QWizardPage):
    def __init__(self,
                 configuration_saver_dict,
                 parent = None):
        QWizardPage.__init__(self, parent)

        self._configuration_saver_dict = configuration_saver_dict

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Select databasesystem</b></font>")

        UI_PATH = QFile(":/ui_file/SelectDatabaseSystem.ui")

        self.load_ui_file(UI_PATH)

        self.wizard_page = None

        self.init_ui()
        self.valide_fields()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_select_database_system = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.ui_select_database_system.radioButton_desktop_database_system.toggled.connect(self.enabled_wizard_next_button)
        self.ui_select_database_system.radioButton_server_database_system.toggled.connect(self.enabled_wizard_next_button)
        self.ui_select_database_system.lineEdit.setFrame(False)

        self.ui_select_database_system.checkBox_skip.stateChanged.connect(self.skip_database_system)

        return

    def valide_fields(self):

        '''
            Field Validation
            ----------------
            Fields are only validated if they are registered as mandatory
            (denoted by an asterisk after the name) even if they have
            been assigned a QRegExValidator.

            That means the next button on this WizardPage will disable until the QLineEdit is filled and validated

            i.e.
                registerField("ui_select_database_system*", emailEdit) - validates
                registerField("ui_select_database_system) - no validation
        '''
        self.registerField('database_system_lineEdit', self.ui_select_database_system.lineEdit)

        return

    def skip_database_system(self, int):

        if self.ui_select_database_system.checkBox_skip.isChecked():

            self.ui_select_database_system.radioButton_desktop_database_system.setAutoExclusive(False)
            self.ui_select_database_system.radioButton_server_database_system.setAutoExclusive(False)

            self.ui_select_database_system.radioButton_desktop_database_system.setChecked(False)
            self.ui_select_database_system.radioButton_server_database_system.setChecked(False)

            self.ui_select_database_system.radioButton_desktop_database_system.setEnabled(False)
            self.ui_select_database_system.radioButton_server_database_system.setEnabled(False)

            self.enabled_wizard_next_button()


        else:

            self.ui_select_database_system.radioButton_desktop_database_system.setEnabled(True)
            self.ui_select_database_system.radioButton_server_database_system.setEnabled(True)

        self.ui_select_database_system.radioButton_desktop_database_system.setAutoExclusive(True)
        self.ui_select_database_system.radioButton_server_database_system.setAutoExclusive(True)

        return


    def enabled_wizard_next_button(self):

        '''
            Notice
            ------
            When the user chooses to create a new ini_file, its necessary to
            activiate the next_button again. Thats why the function activates the
            nex_button on this WizardPage.

            The next_button can be activated, the QLineEdit has to be filled with a space.
        '''
        self.ui_select_database_system.lineEdit.setText(" ")

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

            self.wizard().setButtonText(QWizard.NextButton, self.tr("Next >"))

        return

    def nextId(self):

        if self.ui_select_database_system.radioButton_desktop_database_system.isChecked():
            self._configuration_saver_dict.dict_set_general_settings["DatabaseSystem_Temp"] = "Desktop"
            return CURRENT_MAIN_WIZARD.PageDesktopDatabaseSystem
        elif self.ui_select_database_system.radioButton_server_database_system.isChecked():
            self._configuration_saver_dict.dict_set_general_settings["DatabaseSystem_Temp"] = "Server"
            return CURRENT_MAIN_WIZARD.PageServerDatabaseSystem
        else:
            return CURRENT_MAIN_WIZARD.PageSummary
        return

class DesktopDatabaseSystemWizard(QWizardPage):
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Select database for desktop</b></font>")

        UI_PATH = QFile(":/ui_file/SelectDesktopDatabaseSystem.ui")

        self.load_ui_file(UI_PATH)

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_desktop_database_system = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def nextId(self):
        '''
            NOTES:
            =====
            The user has selected a desktop database, and
            when he clicks on NEXT-Button he gets a summary of settings.
            That is why we save the selection in a dictionary.


            Return: The return value isPageSummary.
        '''
        section_name = 'General_Configuration'
        value_name = 'database_type'

        if self.ui_desktop_database_system.radioButton_sqlite.isChecked():
            self._kwargs.get('safe_config_parser').set(section_name, value_name, 'SQLite')             #dict_set_general_settings["Database"] = "SQLite"
        elif self.ui_desktop_database_system.radioButton_ms_access.isChecked():
            self._kwargs.get('safe_config_parser').set(section_name, value_name, 'Microsoft Access')            #dict_set_general_settings["Database"] = "Microsoft Access"

        return CURRENT_MAIN_WIZARD.PageSummary

class ServerDatabaseSystemWizard(QWizardPage):
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Select database for server</b></font>")

        ui_path = QFile(":/ui_file/SelectServerDatabaseSystem.ui")

        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_master_data = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))


    def get_class_name(self):
            return self.__class__.__name__

    def nextId(self):
        '''
            NOTES:
            =====
            The user has selected a server database, and
            when he clicks on NEXT-Button he gets a summary of settings .


            Return: The return value isPageSummary.
        '''
        section_name = 'General_Configuration'
        value_name = 'database_type'
        if self.ui_server_database_system.radioButton_mysql.isChecked():
            self._kwargs.get('safe_config_parser').set(section_name, value_name, 'MySQL')    #dict_set_general_settings["Database"] = "MySQL"
        elif self.ui_server_database_system.radioButton_postgre.isChecked():
            self._kwargs.get('safe_config_parser').set(section_name, value_name, 'PostgreSQL')     #dict_set_general_settings["Database"] = "PostgreSQL"
        elif self.ui_server_database_system.radioButton_mssql.isChecked():
            self._kwargs.get('safe_config_parser').set(section_name, value_name, 'Microsoft SQL')    #dict_set_general_settings["Database"] = "Microsoft SQL"
        return CURRENT_MAIN_WIZARD.PageSummary

class SummaryWizard(QWizardPage):
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Complete configuration</b></font>")

        self.setPixmap(QWizard.WatermarkPixmap, QPixmap(":/images/watermark.png"))

        UI_PATH = QFile(":/ui_file/SummaryWizard.ui")

        self.load_ui_file(UI_PATH)

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_summary = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def check_copy_ini_file(self):

        if self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyINISetting_Temp"]:
            return True
        else:
            return False

        return

    def check_copy_profile_folder(self):

        if self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyProfileFolder_Temp"]:
            return True
        else:
            return False

        return

    def get_summary(self,
                    kwargs,
                    copy_from_path,
                    copy_to_path):
        '''
           SUMMARY
           =======
           This method is used to summarize the settings here in the form of a conclusion.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Summarize all the settings")

        section_name = 'General_Configuration'

        self.ui_summary.textEdit_summary.clear()
        cursor = self.ui_summary.textEdit_summary.textCursor()

        result_copy_file = self.check_copy_ini_file()
        result_copy_profile_folder = self.check_copy_profile_folder()

        if result_copy_file:
            cursor.insertHtml("You just want to copy an existing ini file")
            cursor.insertBlock()
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Copy the file <u>from</u> path: </b></html>")
            cursor.insertBlock()
            cursor.insertHtml(copy_from_path)
            cursor.insertBlock()
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Copy the file <u>to</u> path: </b></html>")
            cursor.insertBlock()
            cursor.insertHtml(copy_to_path)

        elif result_copy_profile_folder:
            cursor.insertHtml("You just want to copy an existing named profile")
            cursor.insertBlock()
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Copy the named profile <u>from</u> path: </b></html>")
            cursor.insertBlock()
            cursor.insertHtml(copy_from_path)
            cursor.insertBlock()
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Copy the named profile <u>to</u> path: </b></html>")
            cursor.insertBlock()
            cursor.insertHtml(copy_to_path)
        else:
            cursor.insertHtml("<html><b>Profile name: </b></html>"), cursor.insertHtml(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"])
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Language: </b></html>"), cursor.insertHtml(kwargs.get('safe_config_parser').get(section_name, "language"))
            cursor.insertBlock()
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Databasesystem: </b></html>"), cursor.insertHtml(self._kwargs.get('configuration_saver_dict').dict_set_general_settings["DatabaseSystem_Temp"])
            cursor.insertBlock()
            cursor.insertHtml("<html><b>Databasetype: </b></html>"), cursor.insertHtml(kwargs.get('safe_config_parser').get(section_name, "database_type"))

        return

    def nextId(self):
        '''
            NOTES:
            =====
            The user has selected a server database, and
            when he clicks on NEXT-Button he gets a summary of settings .


            Return: The return value isPageSummary.
        '''
        return CURRENT_MAIN_WIZARD.PageGettingFinied

    def validateCurrentPag(self):
        pass

    def initializePage(self):
        '''
            NOTES:
            =====
            This virtual function was re-implemented, because it is called to prepare
            the page just before it is shown a result of the user clicking Finish.
        '''
        section_name = 'General_Configuration'
        value_name = 'current_profile'

        copy_from_path = None
        copy_to_path = None

        profile_name = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"]

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToINISetting"] = path.join(BASE_PATH, 'profiles', unicode(profile_name), 'settings', 'config.ini')

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToProfileFolder"] = path.join(BASE_PATH, 'profiles')

        if self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyINISetting_Temp"] and \
        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToINISetting"]:

            copy_from_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyINISetting_Temp"]
            copy_to_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToINISetting"]

        elif self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyProfileFolder_Temp"] and \
        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToProfileFolder"]:

            print "lass uns mal sehen, es ist Profile"

            copy_from_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyProfileFolder_Temp"]
            copy_to_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToProfileFolder"]

        self.get_summary(
          kwargs = self._kwargs,
          copy_from_path = copy_from_path,
          copy_to_path = copy_to_path)

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        button_order_list = [QWizard.Stretch,
                             QWizard.CancelButton,
                             QWizard.BackButton,
                             QWizard.NextButton]

        if visible:
            self.wizard().setButtonLayout(button_order_list)

        return

class GettingFinishedWizard(QWizardPage):
    def __init__(self,
                 parent = None,
                 **kwargs):
        QWizardPage.__init__(self, parent)

        #self.setFinalPage(True)
        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.setSubTitle("<font size='5'><b>Getting finished</b></font>")

        self.setPixmap(QWizard.WatermarkPixmap, QPixmap(":/images/watermark.png"))

        UI_PATH = QFile(":/ui_file/getting_finiehd_wizard.ui")

        self.load_ui_file(UI_PATH)

        self.init_list_view()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):

        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_getting_finiegd = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_list_view(self):
        '''
           SUMMARY
           =======
           This method initializes the QListView. In this case we use the model to iterate over the items later.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initializes the QListView")

        self.model = QStandardItemModel()
        self.ui_getting_finiegd.listView_details_process.setModel(self.model)

        return

    def add_items_model(self, text):
        '''
           SUMMARY
           =======
           This method adds the given items to the model.

           PARAMETERS
           ==========
           :text (unicode):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''

        item = QStandardItem(text)
        self.model.appendRow(item)

        return

    def show_wizard_finish_button(self):
        '''
           SUMMARY
           =======
           This method set the special button of QWizard to the Button Layout. In this case we only
           add the finish button.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Add finish button")

        self.wizard().setButtonLayout([QWizard.Stretch, QWizard.FinishButton])

        return

    def initializePage(self):
        '''
           SUMMARY
           =======
           We have to reimplemented this method, so we are able to work with create thread.
           add the finish button.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Start thread')

        task_thread = QThread(self)
        task_thread.work = WizardWorker(work_mode = 'complete',
                                       safe_config_parser = self._kwargs.get('safe_config_parser'),
                                       default_ini_file = self._kwargs.get('default_ini_file'),
                                       path_ini_file = self._kwargs.get('path_ini_file'),
                                       configuration_saver_dict = self._kwargs.get('configuration_saver_dict'))

        task_thread.work.moveToThread(task_thread)

        task_thread.work.show_process_text_signal.connect(lambda text:
          self.add_items_model(text = text))

        task_thread.work.show_wizard_finish_button_signal.connect(self.show_wizard_finish_button)

        task_thread.work.quit_thread_signal.connect(task_thread.quit)
        task_thread.work.wait_thread_signal.connect(task_thread.wait)

        task_thread.started.connect(task_thread.work.start_wizard)

        task_thread.finished.connect(task_thread.deleteLater)

        task_thread.start()

        return

    def setVisible(self, visible):
        '''
           SUMMARY
           =======
           This method is reimplemented.
           When some Buttons are visible on this wizard page, then you can do anything with them.
           In this case it changes the order of the buttons or just remove all of them.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Hide a widget explicitly")

        QWizardPage.setVisible(self, visible)

        if visible:
            # We have to add an empty list to remove all buttons - we don't need to show all of them.
            self.wizard().setButtonLayout([])

        return

class WizardWorker(QObject):

    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()
    show_process_text_signal = pyqtSignal(str)
    show_wizard_finish_button_signal = pyqtSignal()

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)

        self._kwargs = kwargs

        self._path_ini_file = self._kwargs.get('path_ini_file')
        self._safe_config_parser = self._kwargs.get('safe_config_parser')
        self._default_ini_file = self._kwargs.get('default_ini_file')
        self._work_mode = self._kwargs.get('work_mode')

        profile_name = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"]

        self._complete_path_file = path.join(BASE_PATH, 'profiles', unicode(profile_name), 'settings', 'config.ini')

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToINISetting"] = path.join(BASE_PATH, 'profiles', unicode(profile_name), 'settings', 'config.ini')

        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToProfileFolder"] = path.join(BASE_PATH, 'profiles')

    def start_wizard(self):

        self.create_profile()

    def write_settings_ini_file(self, set_get_settings, default_ini_file):
        '''
           SUMMARY
           =======
           This method writes all given settings in the ini file.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Write alle given settings in the ini file")

        save_setting = create_write_ini_file(set_get_settings, default_ini_file)
        if save_setting:
            pass
        else:
            pass

        return

    def copy_file(self, input_file, folder_path):
        '''
           SUMMARY
           =======
           This method copies the existing ini file to the setting folder of current profile name.

           PARAMETERS
           ==========
           :input_file (str): #selectionChanged() sends new QItemSelection

           :folder_path (str): #selectionChanged() sends old QItemSelection

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Copy the existing ini file")

        try:
            copy_file_to(input_file, folder_path)

        except shutil.Error:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        except IOError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def create_ini_file(self):
        '''
           SUMMARY
           =======
           This method creates a ini file.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create a ini file")

        if self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToCopyINISetting_Temp'] and \
            self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToINISetting']:

            try:
                copy_file_to(
                  source_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyINISetting_Temp"],
                  destination_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToINISetting"]
                  )

            except shutil.Error:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            except IOError:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            return


        elif self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToCopyProfileFolder_Temp'] and \
            self._kwargs.get('configuration_saver_dict').dict_set_general_settings['PathToProfileFolder']:

            try:

                self._kwargs.get('configuration_saver_dict').dict_set_general_settings['BaseSelectedProfileFolder']
                copy_folder_to(
                    source_path = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToCopyProfileFolder_Temp"],
                    destination_path = path.join(
                        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["PathToProfileFolder"],
                        self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BaseSelectedProfileFolder"]
                        ))

            except shutil.Error:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            except IOError:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            return

        else:

            write_in_configuration(config_parser = self._safe_config_parser, file_name = self._complete_path_file)

        # It will quit **as soon as thread event loop unblocks**
        self.quit_thread_signal.emit()
        # We need to wait for it to *actually* quit
        self.wait_thread_signal.emit()

        return

    def create_profile(self):
        '''
           SUMMARY
           =======
           This method creates a folder with current profile name.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("create a profile")

        profile_name = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["current_profile"]

        BASE_PATH = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["BasePath"]

        folder_list = [path.join(BASE_PATH, 'profiles'),
                       path.join(BASE_PATH, 'profiles', unicode(profile_name))]

        if self._work_mode == "complete":

            for folder in folder_list:

                try:

                    create_folder(path = folder)

                    self.show_process_text_signal.emit(self.tr('The folder ') +  unicode(folder) + self.tr(' is created successfully'))

                except OSError:

                    desired_trace = format_exc(exc_info())
                    logger.error(desired_trace)

                except UnicodeEncodeError:

                    desired_trace = format_exc(exc_info())
                    logger.error(desired_trace)

                except Exception:

                    desired_trace = format_exc(exc_info())
                    logger.error(desired_trace)

                except:
                    desired_trace = format_exc(exc_info())
                    logger.error(desired_trace)

            #   After the loop is stopped and the top profile folder with a sub named profile
            #   folder is created we have to create the other missing sub in folders of current
            #   profile folder.
            self.create_missing_folders_in_profile(folder_path = path.join(BASE_PATH, 'profiles') ,profile_name = unicode(profile_name) )

            self.create_ini_file()

        return

    def create_missing_folders_in_profile(self, folder_path, profile_name):
        '''
           SUMMARY
           =======
           This method creates a all sub folders in the folder of current profile name.

           RETURNS
           =======
           :return:   Nothing is returned. Well I know a simple 'return'
                      isn't necessary, but this statement gives me a better
                      feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("create all sub foldeer")

        path = generator_top_sub_folder(folder_path =  folder_path, profile_name = profile_name)

        for folder in path:

            try:

                mkdir_top_sub_folder(folder)

                self.show_process_text_signal.emit(self.tr('The folder') + unicode(folder) + self.tr('is created successfully'))

            except OSError:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            except WindowsError:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            except Exception:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

            except:

                desired_trace = format_exc(exc_info())
                logger.error(desired_trace)

        self.show_wizard_finish_button_signal.emit()
        self.show_process_text_signal.emit('')
        self.show_process_text_signal.emit(self.tr('Creation has complted successfally'))

        return

def main():
    from core.config import ConfigurationSaver
    from core.manange_logging import create_log_file, set_custom_logger
    from info import info_app
    from core.manage_time import get_time_now
    from core import manage_folder
    from core.manage_ini_file import get_configuration, create_write_ini_file
    from core.manage_ini_file import DefaultINIFile
    from core.manage_file import copy_file_to

    from gui import ui_rc

    convert_current_time = get_time_now()

    BASE_PATH = path.dirname(path.abspath(FILE_NAME))

    manage_folder.mkdir_top_sub_folder(BASE_PATH)

    LOG_FILE_PATH = path.join(BASE_PATH, 'log', convert_current_time + '.log')

    BASE_PATH = path.dirname(path.abspath(__file__))
    CONFIG_PATH = path.join(BASE_PATH, 'settings', 'setting.ini')
    CONFIG_FOLDER_PATH = path.join(BASE_PATH, 'settings')

    about_app = info_app()

    custom_logger = set_custom_logger(about_app)

    configuration_saver_dict = ConfigurationSaver()

    configuration_saver_dict.dict_set_general_settings["PathToINISetting"] = CONFIG_PATH
    configuration_saver_dict.dict_set_general_settings["PathToSettingFolder"] = CONFIG_PATH

    create_log_file(LOG_FILE_PATH, about_app)

    default_ini_file = DefaultINIFile()

    #   Set the buttons in a sequence and safe it in a list
    btnList = [QWizard.Stretch, QWizard.CancelButton, QWizard.BackButton,
                       QWizard.NextButton, QWizard.FinishButton]

    app = QApplication(argv)
    wiz = MainMissingProfileWizard(btnList,
                        configuration_saver_dict,
                        about_app,
                        custom_logger,
                        default_ini_file)
    wiz.show()

    exit(app.exec_())

if __name__ == '__main__':
    main()
