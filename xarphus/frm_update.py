#!/usr/bin/env python
# -*- coding: utf-8 -*-

FILE_NAME = __name__

import os
import sys
import inspect
from inspect import currentframe
import requests

from PyQt4.QtCore import QThread, pyqtSignal, Qt, QSemaphore, QFile
from PyQt4.uic import loadUi
from PyQt4.QtGui import QDialog, QMessageBox, QIcon, QPushButton, QPixmap

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################
from xarphus.core import check_update
from xarphus.core.manage_folder import check_files_folder, create_folder
from xarphus.core import manage_zip_file
from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
from xarphus.core.manage_qt import custom_message_box
from xarphus.collection_paths_image import ImagePathnCollection

from xarphus.frm_about_version import NewInVersion_Window
from xarphus.frm_password_required import PasswordRequired_Window

from xarphus.core.downloading_update import Download_Thread

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class CheckUpdate_Thread(QThread):

    start_progress = pyqtSignal()
    stop_progress = pyqtSignal()
    check_result = pyqtSignal(str)
    generally_crritcal_msg = pyqtSignal(str, str)

    def __init__(self, VERSION_URL,
                 about_app,
                 change_pushButtons,
                 get_version,
                 compare_parser,
                 custom_logger,
                 parent=None):
        QThread.__init__(self, parent)

        self.VERSION_URL = VERSION_URL
        self.about_app = about_app
        self.change_push_button = change_pushButtons
        self.compare_parser = compare_parser
        self.get_version = get_version
        self.custom_logger = custom_logger

        self.custom_logger.info("The instance of the class of " + self.get_class_name() + "() is created successfully - (" +  FILE_NAME +")")


    def get_class_name(self):
        return self.__class__.__name__

    def run(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
            3] + ") - (" + FILE_NAME + ")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")

        self.start_progress.emit()

        try:

            update_version = self.get_version(self.VERSION_URL)

            check_update_result = self.compare_parser(unicode(self.about_app.dict_info["product_version"]),
                                                      update_version)
            
            if check_update_result:
                self.change_push_button(True, False, False, True)
                self.check_result.emit(self.tr("New update is available.."))
            else:
                self.change_push_button(False, False, False, True)
                self.check_result.emit(self.tr("No new update are available. \n"
                                                            "This program is up to date."))
                    
        except requests.exceptions.Timeout as e:
            # Maybe set up for a retry, or continue in a retry loop
            self.generally_crritcal_msg.emit(self.tr("Error - Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.TooManyRedirects as e:
            # Tell the user their URL was bad and try a different one
            self.generally_crritcal_msg.emit(self.tr("Error - Too many redirects"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.HTTPError as e:
            self.generally_crritcal_msg.emit(self.tr("Error - HTTP"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ConnectionError as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Connection"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.URLRequired as e:
            self.generally_crritcal_msg.emit(self.tr("Error - URL required"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ConnectTimeout as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Connect Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ReadTimeout as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Read Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.RequestException as e:
            self.generally_crritcal_msg.emit(self.tr(""), self.tr("An error has occurred: ") + unicode(e))
        finally:
            self.stop_progress.emit()

        self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully - (" + FILE_NAME + ")")
        self.custom_logger.info(
            "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")
        return

class Update_Window(QDialog):
    def __init__(self,
                 save_config,
                 info_app,
                 image_path_collection,
                 get_version,
                 compare_parser,
                 set_custom_logger,
                 parent=None):

        QDialog.__init__(self, parent)

        self.custom_logger = set_custom_logger

        self.custom_logger.info("The instance of the class of " + self.get_class_name() + "() is created - (" +  FILE_NAME +")")

        UI_PATH = QFile(":/ui_file/update.ui")

        self.about_app = info_app
        self.image_path_collection = image_path_collection
        self.get_version = get_version
        self.compare_parser = compare_parser


        self.set_get_settings = save_config
        self.custom_logger.info("Creating an instance of the class of " + self.set_get_settings.__class__.__name__ +"() - (" +  FILE_NAME +")")

        BASE_PATH = self.set_get_settings.dict_set_general_settings["PathToTempFolder"]


        DOWNLOAD_URL = self.set_get_settings.dict_set_general_settings["URLServerUpdate"]
        VERSION_URL = self.set_get_settings.dict_set_general_settings["URLServerVersion"]
        #print "VERSION_URL", VERSION_URL
        TEMP_PATH = os.path.join(BASE_PATH, 'example-app-0.3.win32.zip')
        TIME_OUT = int(self.set_get_settings.dict_set_general_settings["TimeOutUpdate"])

        self.custom_logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        UI_PATH.open(QFile.ReadOnly)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened successfully - (" +  FILE_NAME +")")
        self.custom_logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        self.ui_update = loadUi(UI_PATH, self)

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded successfully - (" +  FILE_NAME +")")
        self.custom_logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        UI_PATH.close()

        self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed successfully - (" +  FILE_NAME +")")


        self.custom_logger.info("Show the class Update_Window() and the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- (END)")

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.ui_update.setWindowFlags(self.flags)

        self.init_gui()
        self.init_action_pushbutton()

        '''
        The variable named aborted_download is a flag.
        '''
        self.aborted_download = None
        self.abort_checking = None

        self.custom_logger.info("Instantiate the class of  Download_Thread() - (" +  FILE_NAME +")")

        self.download_task = Download_Thread(TEMP_PATH, DOWNLOAD_URL, TIME_OUT)

        self.custom_logger.info("Connect with pyqtSignal()-method named (notify_progress) - (" +  FILE_NAME +")")

        self.download_task.notify_progress.connect(self.on_progress)

        self.custom_logger.info("The pyqtSignal()-method named (notify_progress) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (finished_thread) - (" +  FILE_NAME +")")

        self.download_task.finished_thread.connect(self.on_finished)

        self.custom_logger.info("The pyqtSignal()-method named (finished_thread) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (error_http) - (" +  FILE_NAME +")")

        self.download_task.error_http.connect(self.on_connection_error)

        self.custom_logger.info("The pyqtSignal()-method named (error_http) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (finished_download) - (" +  FILE_NAME +")")

        self.download_task.finished_download.connect(self.on_finish_download)

        self.custom_logger.info("The pyqtSignal()-method named (finished_download) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (abort_downloading) - (" +  FILE_NAME +")")

        self.download_task.abort_downloading.connect(self.abort_downloading)

        self.custom_logger.info("The pyqtSignal()-method named (abort_downloading) is associated successfully - (" +  FILE_NAME +")")



        self.custom_logger.info("Instantiate the class of  CheckUpdate_Thread() - (" +  FILE_NAME +")")
        self.check_update_task = CheckUpdate_Thread(VERSION_URL,
                                                    self.about_app,
                                                    self.change_pushButtons,
                                                    self.get_version,
                                                    self.compare_parser,
                                                    self.custom_logger)


        self.custom_logger.info("Connect with pyqtSignal()-method named (start_progress) - (" +  FILE_NAME +")")

        self.check_update_task.start_progress.connect(self.start_pulsing_progress_bar)

        self.custom_logger.info("The pyqtSignal()-method named (start_progress) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (stop_progress) - (" +  FILE_NAME +")")

        self.check_update_task.stop_progress.connect(self.stop_pulsing_progress_bar)

        self.custom_logger.info("The pyqtSignal()-method named (stop_progress) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (check_result) - (" +  FILE_NAME +")")

        self.check_update_task.check_result.connect(self.change_label_update)

        self.custom_logger.info("The pyqtSignal()-method named (check_result) is associated successfully - (" +  FILE_NAME +")")

        self.custom_logger.info("Connect with pyqtSignal()-method named (generally_crritcal_msg) - (" +  FILE_NAME +")")

        self.check_update_task.generally_crritcal_msg.connect(self.create_generally_crritcal_msg)

        self.custom_logger.info("The pyqtSignal()-method named (generally_crritcal_msg) is associated successfully - (" +  FILE_NAME +")")


        self.check_update_task.start()
        self.abort_checking = True

    def get_class_name(self):
            return self.__class__.__name__


    def change_label_update(self, label_text):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        self.ui_update.label_update.setText(self.tr(label_text))

        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return

    def change_pushButtons(self, bool_value, bool_value_1, bool_value_2, bool_value_3):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        self.ui_update.pushButton_update.setEnabled(bool_value)
        self.ui_update.pushButton_cancel.setEnabled(bool_value_1)
        self.ui_update.pushButton_what_is_new.setEnabled(bool_value_2)
        self.ui_update.pushButton_close.setEnabled(bool_value_3)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return

    def start_pulsing_progress_bar(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        self.ui_update.progressBar_update.setRange(0, 0)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return

    def stop_pulsing_progress_bar(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        self.ui_update.progressBar_update.setRange(0, 1)
        self.abort_checking = None

        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return

    def create_generally_crritcal_msg(self, err_title, err_msg):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +") ")

        self.ui_update.label_update.setText(self.tr("Ein Fehler ist aufgetreten."))

        mBox = custom_message_box(self, err_msg, err_title, QMessageBox.Critical)
        setIc_OK = QIcon(self.image_path_collection.check_icon)
        mBox.addButton(
            QPushButton(setIc_OK, self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        self.change_pushButtons(False, False, False, True)

        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return

    def check_temp_folder_content(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +") ")
        '''
        Before the download starts the function (check_files_folder) looks if there are
        some files and folders in the update-temp-folder. The folders shouldn't have any content.
        The update-temp-folder should be empty. If the update-temp-folder is empty, then the download
        starts.
        '''
        check_result = check_files_folder(self.set_get_settings.dict_set_general_settings["PathToTempFolder"])

        self.custom_logger.info("The function ("+ check_files_folder.__name__ +") is called - (" +  FILE_NAME +") ")

        #if check_result == True:

        #    self.custom_logger.info("Result: There aren't any folders and files - (" +  __name__ +") ")
        #    self.custom_logger.info("Calling the function (on_start) - (" +  __name__ +")")

        #    self.on_start()

        #    self.custom_logger.info("The function (" + self.on_start.__name__ +") is called - (" +  __name__ +") ")
        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is called - (" +  FILE_NAME +")")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        return check_result

    def init_action_pushbutton(self):
        self.ui_update.pushButton_cancel.clicked.connect(self.on_finished)
        self.ui_update.pushButton_what_is_new.clicked.connect(self.create_new_version_form)
        self.ui_update.pushButton_update.clicked.connect(self.check_folder_exists)
        self.ui_update.pushButton_close.clicked.connect(self.abort_downloading_close)

    def init_gui(self):
        self.ui_update.setWindowModality(Qt.ApplicationModal)
        #self.ui_update.setWindowTitle(self.tr("Aktualisierung"))
        #self.ui_update.label_update.setText(self.tr("Es wird nach einer Aktualisierung gesucht..."))

        #self.ui_update.pushButton_close.setText(self.tr("Abbrechen"))

        #self.ui_update.pushButton_what_is_new.setText(self.tr("Was ist neu?"))

        #self.ui_update.pushButton_cancel.setText(self.tr("Ladevorgang abbrechen"))

        #self.ui_update.pushButton_update.setText(self.tr("Aktualisieren"))
        
        self.ui_update.pushButton_update.setEnabled(False)
        self.ui_update.pushButton_close.setEnabled(False)
        self.ui_update.pushButton_cancel.setEnabled(False)
        self.ui_update.pushButton_what_is_new.setEnabled(False)
        self.ui_update.progressBar_update.setAlignment(Qt.AlignCenter)


    def on_start(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        self.custom_logger.info('Download-Server: ' + self.set_get_settings.dict_set_general_settings["URLServerUpdate"])
        self.aborted_download = True
        self.ui_update.pushButton_cancel.setEnabled(True)
        self.ui_update.progressBar_update.setRange(0, 0)
        self.custom_logger.info("Call Download-Thread (Download_Thread)")
        self.download_task.start()
        self.custom_logger.info("The Download-Thread (Download_Thread) is called ")
        self.ui_update.pushButton_update.setEnabled(False)
        self.custom_logger.info("The function (" + inspect.stack()[0][3] +") is finished - (" +  FILE_NAME +") ")
        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
    #--------------------------------------------------------------------------------------
    def requred_zip_password(self):
        self.password_required_form = PasswordRequired_Window(self.call_it_again)
        self.password_required_form.show()
    #--------------------------------------------------------------------------------------
    def need_zip_file_pwd_message_box(self):
        '''
        When download is finish the user get a messagebox. The user knows the download
        is complete.
        '''
        msg_box = QMessageBox()


        QMessageBox.warning(msg_box, self.tr('Message'),
                                           self.tr("The update file is password protected. "
                                                   "Please enter the password in following window."), msg_box.Ok)
        self.requred_zip_password()
    #--------------------------------------------------------------------------------------
    def info_messagebox(self):
        '''
        When download is finish the user get a messagebox. The user knows the download
        is complete.
        '''
        msg_box = QMessageBox()

        QMessageBox.question(msg_box, self.tr(' Message '),
                                           self.tr("The update file has been downloaded successfully."), msg_box.Ok)
    #--------------------------------------------------------------------------------------
    def warning_messagebox(self):
        '''
        When download is finish the user get a messagebox. The user knows the download
        is complete.
        '''
        msg_box = QMessageBox()

        QMessageBox.warning(msg_box, self.tr(' Message '),
                                           self.tr("The update file is corrupted. \n"
                                                    "This can have several reasons: \n"
                                                    "(1): The update file is defective. \n"
                                                    "(2): There are incorrect files in the update file."
                                                    "(3): The update file has not been downloaded fully \n"
                                                    "\n"
                                                    "To fix the error: \n"
                                                    "Stellen Sie sicher, dass Sie Ã¼ber eine stabile "
                                                    "Make sure that you have a stable internet connection and download the update file again."), msg_box.Ok)

    def create_connection_error_message_box(self):
        setIc_OK = QIcon(self.reload_ico)
        setIc_No = QIcon(self.cancel_ico)
        msgBox_unload_mdi = QMessageBox()
        msgBox_unload_mdi.setWindowTitle(self.tr("Warning"))
        msgBox_unload_mdi.setWindowIcon(QIcon(self.warning_ico))
        msgBox_unload_mdi.setIconPixmap(QPixmap(self.warning_ico))
        msgBox_unload_mdi.setText(self.tr("An error occurred while downloading the update file. \n"
                                           "\n"
                                           "Possible reasons: \n"
                                           "(1): Your Internet connect is currently very unstable. In this case, make sure "
                                           "you have a stable internet connection "
                                           "and download the update file (in a later stage) again.\n"
                                           "(2): The update server has just failed. In this case, "
                                           "you should try downloading the update file again later. \n"
                                           "(3): Connection timed out with the update server. "
                                           "The update server reacts / responds very much delayed. \n"
                                           "(4): It is quite possible that a firewall or a virus scanner "
                                           "on their system blocked to download the update file. Check your firewall or  virus scanner. \n"
                                           "\n"
                                           "Would you like to try again?"))
        msgBox_unload_mdi.addButton(
            QPushButton(setIc_OK, self.tr("Try again")),
            QMessageBox.YesRole)
        msgBox_unload_mdi.addButton(
            QPushButton(setIc_No, self.tr("Cancel")),
            QMessageBox.NoRole)

        ret = msgBox_unload_mdi.exec_()
        return ret

    def create_list_function(self, choice_number):
        functions_list = [self.info_messagebox,
                          self.warning_messagebox,
                          self.need_zip_file_pwd_message_box,
                          self.create_connection_error_message_box]
        ret = functions_list[choice_number]()
        return ret

    def call_it_again(self):
        result_zip_file, for_zip_pwd = self.is_zip_corrupt()
        #self.for_zip_pwd = for_zip_pwd

    def is_zip_corrupt(self, personal_pwd=''):
        path_to_zip_temp_folder = self.set_get_settings.dict_set_general_settings["PathToTempFolder"]
        zip_file_passwd = self.set_get_settings.dict_set_general_settings["zip_pwd"]
        result_zip = manage_zip_file.check_number_files(path_to_zip_temp_folder, zip_file_passwd)

        #return result_zip[0], result_zip[1]
        return result_zip

    def on_finish_download(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        '''
        Now the download is finish, and we should check if the zip file is
        coruppt. That ist why we call the function named (is_zip_corrupt) and
        wait for returning the value
        '''
        result_zip_file, for_zip_pwd = self.is_zip_corrupt()
        self.for_zip_pwd = for_zip_pwd
        self.aborted_download = None
        self.close()
        self.create_list_function(result_zip_file)

        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

    def on_connection_error(self):
        '''
        Once a connection error appears, the download is aborted.
        The function named (check_temp_folder_content) deletes
        the previously downloaded file.
        '''
        self.check_temp_folder_content()
        '''
        The function named (create_list_function) get the number 3 as an argument,
        because the messagebox is in fourth place in the created list. The messagebox,
        that has been called, returns a value: 0 for yes and 1 for no. The value is
        saved in the variable named (ret).
        '''
        ret = self.create_list_function(3)
        '''
        The variable is checked.
        '''
        if ret == 0:
            '''
            The user clicked the pushbutton yes.
            '''
            self.check_folder_exists()
        elif ret == 1:
            '''
            The user clicked the pushbutton no.
            '''
            self.progressBar_update.setValue(0)
            self.aborted_download = None
            self.ui_update.pushButton_cancel.setEnabled(False)
            self.ui_update.pushButton_update.setEnabled(True)

    def create_new_version_form(self):
        new_version_form = NewInVersion_Window(self.set_get_settings.dict_set_general_settings["URLServerVersionNews"],
                                               self.tr("Renewal"),
                                               self.tr("What's new?"), self)
        #new_version_form.show()

    def on_progress(self, i):
        self.ui_update.progressBar_update.setRange(0, 100)
        self.ui_update.progressBar_update.setValue(i)

    def check_folder_exists(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        result_folder = create_folder(self.set_get_settings.dict_set_general_settings["PathToTempFolder"])

        if result_folder == True:
            '''
            After the the folder is created the download starts
            '''
            self.custom_logger.info("Calling the function (" + self.on_start.__name__ + ")")

            self.on_start()

            self.custom_logger.info("The function (" + self.on_start.__name__ + ") is called")
        else:
            '''
            The folder existis. Now before the download starts the function has
            to check the contents of the update-temp-folder.
            '''
            result_contents = self.check_temp_folder_content()
            if result_contents == True:
                self.on_start()
            self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

    def on_finished(self):
        self.progressBar_update.setValue(0)
        self.download_task.stop()
        self.progressBar_update.setValue(0)
        self.aborted_download = None
        #self.close()

    def clear_temp_update_folder(self, temp_path):
        me = check_files_folder(temp_path)

    def show_abort_msgbox(self):
        '''
        When download is finish the user get a messagebox. The user knows the download
        is complete.
        '''
        self.aborted_download = True
        msg_box = QMessageBox()

        QMessageBox.warning(msg_box, self.tr('Message'),
                                           self.tr("Downloading was canceled."), msg_box.Ok)

    def abort_downloading_close(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

        #self.show_abort_msgbox()
        if self.aborted_download == None:
            self.close()
        else:
            self.download_task.stop()
            self.aborted_download = None
            self.close()

        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

    def abort_downloading(self):
        self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")
        '''
        The user interrupts the download.
        '''
        self.show_abort_msgbox()

        self.ui_update.progressBar_update.setValue(0)

        self.clear_temp_update_folder(self.set_get_settings.dict_set_general_settings["PathToTempFolder"])

        self.ui_update.pushButton_cancel.setEnabled(False)
        self.ui_update.pushButton_update.setEnabled(True)
        self.aborted_download = None

        self.custom_logger.info("-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] +") - (" +  FILE_NAME +")")

    def closeEvent(self, event):
        self.custom_logger.info('Calling (closeEvent) in frm_update')
        self.custom_logger.info('Stop to download the update file')
        if self.abort_checking == None:
            self.close()
        if self.aborted_download == None:
            self.close()
        else:
            self.download_task.stop()
            self.aborted_download = True
            self.close()
        self.custom_logger.info('The download is stopped')

    def keyPressEvent(self, event):
        '''
        Did the user press the Escape key?
        Note: QtCore.Qt.Key_Escape is a value that equates to
        what the operating system passes to python from the
        keyboard when the escape key is pressed.
        '''
        pass

if __name__ == "__main__":
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator
    
    from core.check_update import get_version, compare_parser
    from core.manage_folder import check_files_folder, create_folder
    from core.manange_logging import set_custom_logger, create_log_file
    from core import manage_zip_file
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.config import ConfigurationSaver
    from info import info_app
    from core.manage_qt import custom_message_box
    from collection_paths_image import ImagePathnCollection
    
    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc
    

    #from frm_about_version import NewInVersion_Window
    #from frm_password_required import PasswordRequired_Window

    from core.downloading_update import Download_Thread
    from core.manage_time import get_time_now

    FILE_NAME = "frm_about_version.py"

    convert_current_time = get_time_now()

    LOG_FILE_PATH = os.path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')

    dict_custom_config = ConfigurationSaver()
    program_info = info_app()
    set_custom_logger = set_custom_logger(program_info)
    image_path_collection = ImagePathnCollection()

    create_log_file(LOG_FILE_PATH, program_info)

    user_ini_file = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    dict_custom_config.dict_set_general_settings["PathToINISetting"] = user_ini_file

    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Update_Window(dict_custom_config,
                           program_info,
                           image_path_collection,
                           get_version,
                           compare_parser,
                           set_custom_logger,
                           None)

    window.resize(600, 400)
    window.show()
    sys.exit(app.exec_())
