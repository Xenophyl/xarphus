#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

import os

class ImagePathnCollection(object):
      def __init__(self,
                   theme = None):

            self.theme = theme

            # Image Size 12x12
            self.delete_12x12 = ':/img_12x12_{theme}/{theme}/img_12x12/Button_LineEdit_Delete.png'.format(theme = str(self.theme))

            # Image Size 16x16
            self.cancel_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Cancel.png'.format(theme = str(self.theme))
            self.delete_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Delete.png'.format(theme = str(self.theme))
            self.critical_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Error.png'.format(theme = str(self.theme))


            self.double_right_aarrow_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Double_Arrow_Right.png'.format(theme = str(self.theme))
            self.person_16x16 =':/img_16x16_{theme}/{theme}/img_16x16/Person.png'.format(theme = str(self.theme))
            self.database_profile_connection_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Connection.png'.format(theme = str(self.theme))
            self.search_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/search.png'.format(theme = str(self.theme))
            self.help_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Help.png'.format(theme = str(self.theme))
            self.support_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Support.png'.format(theme = str(self.theme))
            self.feedback_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Feedback.png'.format(theme = str(self.theme))
            self.log_in_user_profile_online_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/password.png'.format(theme = str(self.theme))
            self.faq_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Question .png'.format(theme = str(self.theme))
            self.release_note_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/ReleaseNote.png'.format(theme = str(self.theme))
            self.about_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/About.png'.format(theme = str(self.theme))
            self.update_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Update.png'.format(theme = str(self.theme))
            self.film_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Film.png'.format(theme = str(self.theme))
            self.book_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Book.png'.format(theme = str(self.theme))
            self.magazine_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Magazine.png'.format(theme = str(self.theme))
            self.newspaper_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Newspaper.png'.format(theme = str(self.theme))
            self.journal_article_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/JournalArticle.png'.format(theme = str(self.theme))
            self.newspaper_article_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/NewspaperArticle.png'.format(theme = str(self.theme))
            self.ebook_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Ebook.png'.format(theme = str(self.theme))
            self.audio_book_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/AudioBook.png'.format(theme = str(self.theme))
            self.record_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Record.png'.format(theme = str(self.theme))
            self.album_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Album.png'.format(theme = str(self.theme))
            self.single_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Single_Music.png'.format(theme = str(self.theme))
            self.coin_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Coin.png'.format(theme = str(self.theme))
            self.contact_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Contact.png'.format(theme = str(self.theme))
            self.note_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Note.png'.format(theme = str(self.theme))
            self.video_game_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/VideoGame.png'.format(theme = str(self.theme))
            self.music_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Music.png'.format(theme = str(self.theme))
            self.comic_book_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/ComicBook.png'.format(theme = str(self.theme))
            self.miscellany_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/MiscellanyBook.png'.format(theme = str(self.theme))
            self.quotation_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Quotation.png'.format(theme = str(self.theme))
            self.stamp_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Stamp.png'.format(theme = str(self.theme))
            self.wine_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Wine.png'.format(theme = str(self.theme))
            self.rent_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Rent.png'.format(theme = str(self.theme))

            self.masta_data_icon_16x16_default = ':/img_16x16_{theme}/{theme}/img_16x16/MindMap.png'
            self.connection_profile_icon_data_16x16_default = ':/img_16x16_{theme}/{theme}/img_16x16/Connection.png'.format(theme = str(self.theme))


            self.delete_16x16 = ':/img_16x16_{theme}/{theme}/img_16x16/Delete.png'.format(theme = str(self.theme))

            # Image Size 24x24
            self.cancel_24x24 = ':/img_24x24_{theme}/{theme}/img_24x24/Cancel.png'.format(theme = str(self.theme))

            # Image Size 32x32
            self.add_Movie_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/addMovie.png'.format(theme = str(self.theme))
            self.search_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Search.png'.format(theme = str(self.theme))
            self.filter_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Filter.png'.format(theme = str(self.theme))
            self.connect_database_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Connect.png'.format(theme = str(self.theme))
            self.disconnect_database_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Disconnect.png'.format(theme = str(self.theme))
            self.filter_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Filter.png'.format(theme = str(self.theme))
            self.master_data_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/MindMap.png'.format(theme = str(self.theme))
            self.settings_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Settings.png'.format(theme = str(self.theme))
            self.quotation_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Quotation.png'.format(theme = str(self.theme))
            self.film_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Film.png'.format(theme = str(self.theme))
            self.book_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Book.png'.format(theme = str(self.theme))
            self.music_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Music.png'.format(theme = str(self.theme))
            self.person_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Person.png'.format(theme = str(self.theme))
            self.contact_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Contact.png'.format(theme = str(self.theme))
            self.coin_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Coin.png'.format(theme = str(self.theme))
            self.video_game_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/VideoGame.png'.format(theme = str(self.theme))
            self.stamp_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Stamp.png'.format(theme = str(self.theme))
            self.note_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Note.png'.format(theme = str(self.theme))
            self.quotation_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Quotation.png'.format(theme = str(self.theme))
            self.wine_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Wine.png'.format(theme = str(self.theme))
            self.rent_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Rent.png'.format(theme = str(self.theme))
            self.update_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Update.png'.format(theme = str(self.theme))
            self.exit_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/Exit.png'.format(theme = str(self.theme))
            self.info_32x32 = ':/img_32x32_{theme}/{theme}/img_32x32/info.png'.format(theme = str(self.theme))

            # Image Size 64x64
            self.profile_image_64x64 = ':/img_64x64_{theme}/{theme}/img_64x64/Person.png'.format(theme = str(self.theme))

            self.delete_default = ':/img_24x24_{theme}/{theme}/img_24x24/Delete.png'
            self.edit_default = ':/img_24x24_{theme}/{theme}/img_24x24/Edit.png'
            self.check_icon_default = ':/img_24x24/img_24x24/Ok.png'
            self.check_circle_default = ':/img_24x24_{theme}/{theme}/img_24x24/check_mark_white_on_black_circular_background.png'
            self.no_default = ':/img_24x24/img_24x24/No.png'
            self.refresh_default = ':/img_24x24_{theme}/{theme}/img_24x24/Refresh.png'
            self.add_default = ':/img_24x24_{theme}/{theme}/img_24x24/Add.png'

            # Image Size 32x32
            self.question_32x32_default = ':/img_48x48_{theme}/{theme}/img_32x32/Question.png'
            self.info_32x32_default = ':/img_32x32_{theme}/{theme}/img_32x32/info.png'
            self.check_circle_32x32_default = ':/img_32x32_{theme}/{theme}/img_32x32/check_mark_white_on_black_circular_background.png'
            self.warning_32x32_default = ':/img_32x32_{theme}/{theme}/img_32x32/Warning.png'

            # Image Size 48x48
            self.warning_48x48 = ':/img_48x48_{theme}/{theme}/img_48x48/Warning.png'.format(theme = str(self.theme))
            #self.warning_48x48 = ':/img_48x48{theme}/{theme}/img_48x48/Warning.png'.format(theme = str(self.theme))


            self.check_circle_48x48_default = ':/img_48x48_{theme}/{theme}/img_48x48/check-mark-white-on-black-circular-background.png'
            self.info_48x48_default = ':/img_48x48_{theme}/{theme}/img_48x48/info.png'

            self.question_48x48_default = ':/img_48x48_{theme}/{theme}/img_48x48/Question.png'.format(theme = str(self.theme))
            self.warning_48x48_default = ':/img_48x48_{theme}/{theme}/img_48x48/Warning.png'

            # Image Size 150x200
            self.standard_photo_150x200_default =':/img_150x200_{theme}/{theme}/img_150x200/default_photo.png'

            # Image Size 150x220
            self.default_film_poster = ':/img_156x220_{theme}/{theme}/img_156x220/default.png'


            '''
                  ==============================
                              OTHER CSS
                  ==============================
            '''







            #--------------------------------------------------------------------------------------
            self.warning_icon_48x48 = ':/img_48x48/img_48x48/Warning.png'
            self.reload_ico = ':/img_90x90/img_90x90/reload.png'
            self.cancel_ico = ':/img_24x24/img_24x24/Cancel.png'
            self.movie_icon = ':/img_24x24/img_24x24/Movie.png'
            self.critical_icon = ':/img_16x16/img_16x16/Error.png'
            self.check_icon = ':/img_24x24/img_24x24/Ok.png'
            self.error_icon = ':/img_36x36/img_36x36/Error.png'
            self.coin_icon = ':/img_32x32/img_32x32/Coin.png'
            self.details_view_icon_230x211 = ':/img_230x211/img_230x211/Details_View.png'
            self.details_view_pic_icon_259x218 = ':/img_259x218/img_259x218/Details_View_Pic.png'
            self.setting_icon_16x16 = ':/img_16x16/img_16x16/Settings.png'
            #--------------------------------------------------------------------------------------
            # For specific forms in frm_person_profile
                # For adress book
            #self.person_address_book = ':/img_24x24/img_24x24/user_profile.png'
            self.person_address_book_icon = ':/img_32x32/img_32x32/Contact.png'
            self.person_prominent_icon = ':/img_24x24/img_24x24/user_profile.png'
            #--------------------------------------------------------------------------------------
            # For closeEvent in ui_pp_mdi.py - QMessageBox
            self.question_icon_title__unload_mdi_msgbox = ':/img_16x16/img_16x16/Question.png'
            self.question_icon_unload_mdi_msgbox = ':/img_32x32/img_32x32/Question.png'
            self.cancel_icon_unload_mdi_msgbox = ':/img_24x24/img_24x24/Cancel.png'
            self.ok_icon_unload_mdi_msgbox = ':/img_24x24/img_24x24/Ok.png'
            self.no_icon_unload_mdi_msgbox = ':/img_24x24/img_24x24/No.png'
            #--------------------------------------------------------------------------------------
            # For Textboxes are empty in frm_feedback.py - QMessageBox
            self.get_Path_error_title_icon = os.path.join(os.path.abspath('.'), 'files', 'images', 'img_16x16', 'Error.png')
            self.get_Path_msgbox_error_icon = os.path.join(os.path.abspath('.'), 'files', 'images', 'img_32x32', 'Critical.png')
            self.getPath_Ok_iconl = os.path.join(os.path.abspath('.'), 'files', 'images', 'img_24x24', 'Ok.png')
            #--------------------------------------------------------------------------------------
            # Images and icons in ui_pp_mdi.py
                # SystemTrayIcon
            self.SystemTrayIcon_logo = ':/img_16x16/img_16x16/Logo.png'
            self.SystemTrayIcon_about = ':/img_16x16/img_16x16/About.png'
            self.SystemTrayIcon_exit = ':/img_16x16/img_16x16/Exit.png'
                # Toolbar-Button

            self.toolbar_button_music_icon = ':/img_32x32_default/default/img_32x32/Music.png'
            self.toolbar_button_person_icon = ':/img_32x32_default/default/img_32x32/Person.png'
            self.toolbar_button_contact_icon = ':/img_32x32_default/default/img_32x32/Contact.png'
            self.toolbar_button_coin_icon = ':/img_32x32_default/default/img_32x32/Coin.png'
            self.toolbar_button_note_icon = ':/img_32x32_default/default/img_32x32/Note.png'
            self.toolbar_button_game_icon = ':/img_32x32_default/default/img_32x32/Game.png'
            self.toolbar_button_stamp_icon = ':/img_32x32_default/default/img_32x32/Stamp.png'
            self.toolbar_button_wine_icon = ':/img_32x32_default/default/img_32x32/Wine_Bottle.png'

            self.toolbar_button_rent_icon = ':/img_32x32_default/default/img_32x32/Rent.png'

                # For MenuItem and MenuItem in Toolbar-Button
            self.getPath_add_Movie_icon = ':/img_32x32_default/default/img_32x32/addMovie.png'
            self.getPath_add_Serie_icon = ':/img_32x32_default/default/img_32x32/addSerie.png'
            self.getPath_addBook_icon = ':/img_32x32_default/default/img_32x32/addBook.png'
            self.getPath_addComic_icon = ':/img_32x32_default/default/img_32x32/addComic.png'
            self.getPath_addMagazin_icon = ':/img_32x32_default/default/img_32x32/addMagazin.png'
            self.getPath_addNews_icon = ':/img_32x32_default/default/img_32x32/addNews.png'
            self.getPath_album_single_icon = ':/img_32x32_default/default/img_32x32/Music_CD.png'
            self.getPath_record_icon = ':/img_32x32_default/default/img_32x32/Record.png'
            self.getPath_signleaudio_icon = ':/img_32x32_default/default/img_32x32/Audio_File.png'
            self.getPath_addNote_icon = ':/img_32x32_default/default/img_32x32/addNote.png'
            self.getPath_Quotation_icon = ':/img_32x32_default/default/img_32x32/Quotation.png'
                # Default Cover
            self.default_cover = ':/img_156x220/img_156x220/default.jpg'
            #--------------------------------------------------------------------------------------
            # Images and icons in ui_pp_language.py
                # For combobox
            self.us_flag_icon = ':/img_24x24/img_24x24/US_Flag.png'
            self.uk_flag_icon = ':/img_24x24/img_24x24/UK_Flag.png'
            self.ger_flag_icon = ':/img_24x24/img_24x24/Ger_Flag.png'

      def set_theme(self,
                    theme):
        '''
           SUMMARY
           =======
           This method is considered to be a general method for starting the checks.

           PARAMETERS
           ==========
           :theme (str):      NWe need the name of the theme.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Set the theme')

        self.theme = theme

        return
