#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#   Change the version of QVariant with sip before importing PyQt
import sip
sip.setapi('QVariant',2)

from sys import exit, argv, exc_info
from os import path
from traceback import format_exc

try: #  Import all modules from...
    #   ... core
    from xarphus.core.managed_defined_session import *
    #from xarphus.core.manage_data_manipulation_master_data import select_all

    from xarphus.q_line_edit_button_right import ButtonLineEdit

    from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel
    from xarphus.core.manage_input_controller import InputController
    from xarphus.subclass_master_data_load_data_item import LoadDataItem, TaskDataManipulation, TaskModel
    from xarphus.core.manage_qt import ProgressBar
    from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    #from xarphus.collection_paths_image import ImagePathnCollection
    from xarphus.core.manage_prase import parsing_str_to_bool
    from xarphus.frm_waiting import Waiting_Window
except ImportError: #   When the module is started as a program
    #   Import all compiled files
    from gui import ui_rc
    #   Import all modules from core
    from core.managed_defined_session import *
    #from core.manage_data_manipulation_master_data import select_all
    from core.manage_qt import ProgressBarText
    from core.custom_sort_filter_model import MySortFilterProxyModel
    from core.manage_input_controller import InputController
    from core.manage_prase import parsing_str_to_bool
    #from customsortfiltermodel import MySortFilterProxyModel
    from subclass_master_data_load_data_item import TaskDataManipulation, TaskModel
    from core.config import ConfigurationSaverDict
    from core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from collection_paths_image import ImagePathnCollection
    from frm_waiting import Waiting_Window
    from images import images_rc

from sqlalchemy import create_engine

from PyQt4.QtCore import QFile, Qt, QTimer, QObject, pyqtSignal, \
                        QThread, QRegExp, QTime, QDateTime, QDate, QSize, \
                        pyqtSlot, QVariant, QString

from PyQt4.uic import loadUi

from PyQt4.QtGui import QWidget, QMdiSubWindow, QIcon, QMessageBox, QPushButton, QTreeWidgetItem, \
                        QLineEdit, QStandardItemModel, QAbstractItemView, QAction, QMenu, \
                        QTextEdit, QPlainTextEdit, QTreeView, QStandardItem, QPixmap, QItemDelegate, \
                        QProgressBar

class TaskProxyModel(QObject):
    '''
        NOTICE:
        =======
        Updating the proxyModel. Set the given sourceModel to be processed by the proxy model.

        DETAILS:
        ========
        The main thread freezes when it is setting the model of proxy model,
        so we have to create a new thread for this task to provide the
        responsiveness of the window.
     '''
    notify_progress = pyqtSignal(object)
    notify_item = pyqtSignal(object)
    finish_progress = pyqtSignal()
    fire_label = pyqtSignal(int)
    hide_progress_bar_signal = pyqtSignal()
    replace_proxy_model_signal = pyqtSignal()
    quit_thread_signal = pyqtSignal()
   
    def __init__(self,
                 starndard_item_model = None,
                 proxy_model = None,
                 parent = None):
        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self._starndard_item_model = starndard_item_model
        self._proxy_model = proxy_model

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def init_object(self):

        self._proxy_model.setSourceModel(self._starndard_item_model)

        self.notify_progress.emit(self._proxy_model)

        self.replace_proxy_model_signal.emit()
        
        self.hide_progress_bar_signal.emit()

        self.quit_thread_signal.emit()

    def stop_task_thread(self):
        '''
            NOTICE:
            =======
            This method is used to stop the still running generator by just stopping the timer.
            No Exception is raised. Its a simple stopper.

            The user decided to break or interrupt this loop - for instance when
            the user just closes the window while the generator is still running.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        self.quit_thread_signal.emit()
               
class MasterData_Window(QWidget):
    '''Administration of the user interface for master data.'''

    interrupt_working_thread_signal = pyqtSignal()
    set_flat_signal = pyqtSignal(str, bool)

    def __init__(self, 
                 page_number = 0,
                 image_path_collection = None,
                 dict_custom_config = None,
                 create_error_form = None,
                 session_scope = None,
                 stack_widget_page = None, 
                 configuration_saver_dict = None,
                 standard_item_model = None,
                 parent = None):
        '''
        ATTRIBUTES
        ==========
        :page_number -  Expects an INT value, to change the pages. By default
                        the page is set to Zero.

        :parent      -  The class supports passing a parent arguments
                        to its base class. Its more felxible. By default,
                        the parent is set to None.
                        
        '''

        QWidget.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        '''
            Store constance
        '''
        self.LIST_HORIZONTAL_HEADER_GENERAL_REGION = [self.tr("ID"), self.tr("Region")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_AWARD = [self.tr("ID"), self.tr("Award")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_COUNTRY = [self.tr("ID"), self.tr("Country")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_LANGUAGE = [self.tr("ID"), self.tr("Language")]
        self.LIST_HORIZONTAL_HEADER_PERSON_GENDER = [self.tr("ID"), self.tr("Gender")]
        self.LIST_HORIZONTAL_HEADER_PERSON_NATIONALITY = [self.tr("ID"), self.tr("Nationality")]
        self.LIST_HORIZONTAL_HEADER_PERSON_SALUTATION = [self.tr("ID"), self.tr("Salutation")]
        self.LIST_HORIZONTAL_HEADER_PERSON_TITLE = [self.tr("ID"), self.tr("Title")]
        self.LIST_HORIZONTAL_HEADER_PERSON_HAIR_COLOR = [self.tr("ID"), self.tr("Hair Color")]
        self.LIST_HORIZONTAL_HEADER_PERSON_EYE_COLOR = [self.tr("ID"), self.tr("Eye Color")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELIGION = [self.tr("ID"), self.tr("Religion")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP_STATUS = [self.tr("ID"), self.tr("Relationship Status")]
        self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP = [self.tr("ID"), self.tr("Relationship")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_MEDIA_TYPE = [self.tr("ID"), self.tr("Media Type")]
        self.LIST_HORIZONTAL_HEADER_GENERAL_PACKAGING = [self.tr("ID"), self.tr("Packaging")]
        self.LIST_HORIZONTAL_HEADER_FILM_GENRE = [self.tr("ID"), self.tr("Genre")]
        self.LIST_HORIZONTAL_HEADER_MUSIC_GENRE = [self.tr("ID"), self.tr("Genre")]

        self.NAVI_DATA = [

            (self.tr("Film / Serie"), [
                (self.tr("Genre"), []),
                (self.tr("Regional code"), []),
                (self.tr("Aspect ratio"), []),
                (self.tr("Audio format"), []),
                (self.tr("Color format"), []),
                (self.tr("TV standard"), []),
                (self.tr("Resolution"), []),
                (self.tr("Video format"), []),
                (self.tr("Type of version"), []),
                (self.tr("Dimension"), []),
                (self.tr("Version / Edition"), []),
                (self.tr("Manufacturer"), []),
                (self.tr("Film production company"), []),
                (self.tr("Distribution"), []),
                (self.tr("Movie rentals"), []),
                (self.tr("Label"), []),
                ]),
            (self.tr("Music"), [
                (self.tr("Genre"), []),
                (self.tr("Studio"), []),
                (self.tr("Label"), []),
                ]),

            (self.tr("Book"), [
                (self.tr("Genre"), []),
                (self.tr("Publisher"), []),
                (self.tr("Binding"), []),
                (self.tr("Font size"), []),
                (self.tr("Color format"), []),
                ]),

            (self.tr("Video game"), [
                (self.tr("Genre"), []),
                ]),

            (self.tr("Person"), [
                (self.tr("Gender"), []),
                (self.tr("Salutation"), []),
                (self.tr("Title"), []),
                (self.tr("Nationality"), []),
                (self.tr("Eye color"), []),
                (self.tr("Hair color"), []),
                (self.tr("Religion"), []),
                (self.tr("Relationship status"), []),
                (self.tr("Relationship"), []),
                ]),

            (self.tr("Stamp"), [
                ("Blah", []),
                ]),

            (self.tr("Coin"), [
                (self.tr("Type of coin"), []),
                (self.tr("Alloy"), []),
                (self.tr("Currency"), []),
                ]),

            (self.tr("Age release"), [
                ([], []),
                ]),

            (self.tr("Packaging"), [
                ([], []),
                ]),

            (self.tr("Place of storage"), [
                ([], []),
                ]),

            (self.tr("Type of media"), [
                ([], []),
                ]),

            (self.tr("Language"), [
                ([], []),
                ]),

            (self.tr("Award"), [
                ([], []),
                ]),

            (self.tr("Country"), [
                ([], []),
                ]),

            (self.tr("Region / Place"), [
                ([], []),
                ]),
            ]

        '''
            Create an instance of imported class.
        '''
        self.input_controller = InputController()

        '''
            Create all attributes
        '''
        self.messagebox_called = False
        self._proccessed_progressbar = 0
        self._configuration_saver_dict = configuration_saver_dict
        self.image_path_collection = image_path_collection
        self.stack_widget_page = stack_widget_page
        self.flags = None
        self.current_selected_id = None
        self.selected_be_edited_id = None
        self.activated_thread = None
        self._current_page = page_number
        self._new_page = None
        self._old_page = None

        self._new_stack_page = None
        self._old_stack_page = None

        self._navi_count = 0

        self._list_threads = []


        self._proxy_model = None
        self._navigate_proxy_model = None
        self.set_sort_filter_proxy_model()

        self._standard_item_model = standard_item_model

        self._standard_item_model_person_relationship = None

        self._navigate_standard_model = None

        self.standard_item_model()

        UI_PATH = QFile(":/ui_file/master_data.ui")

        self.load_ui_file(UI_PATH)

        self.init_ui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path = None):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :ui_path -  Expected an argument, that contains a
                        compiled path to the directory where
                        the .*ui-file is stored.

            :return   - Nothing is returned. The statement 'return'
                        terminates a function. That makes sure that the
                        function is definitely finished.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        # loadUi load sthe .ui file inside an instance.
        self.ui_master_data = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
            NOTE:
            =====
            This method initializes the user interface of master_data-window. 
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the user interface")

        self.init_line_edit()

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.init_timer()
        
        self.change_page(index = self._current_page,
                         stack_widget_object = self.ui_master_data.stackedWidget,
                         column_index = 1)
        
        self.hide_widget(widget = self.ui_master_data.frame_progress_bar, boolen_value = False)
        self.init_tree_view()
        self.init_navi_treeview()
        self.init_clicked_signal_push_buttons()
        self.init_textChanged_signal_line_edit()
        self.init_item_clicked_signal_tree_widget()
        self.init_selectionChanged_signal_treeview()
        self.init_returnPressed_signal_line_edit()
        self.init_text_editor()

        return

    def init_line_edit(self):
        '''
            NOTE:
            =====
            This method initializes the custom QLineEdit.

            DETAILS:
            ========
            In this custom QLineEdit there is a QPushButton on right site for clear the content.
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the custom QLineEdit with QPushButton on right site")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.lineEdit_search_general_region = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_region.setObjectName("lineEdit_search_general_region")
        self.ui_master_data.lineEdit_search_general_region.setPlaceholderText("Search by region / place")
        self.ui_master_data.lineEdit_search_general_region.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_80.addWidget(self.ui_master_data.lineEdit_search_general_region)

        ###################################################
        #################  General Countra  ###############
        ###################################################
        self.ui_master_data.lineEdit_search_general_country = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_country.setObjectName("lineEdit_search_general_country")
        self.ui_master_data.lineEdit_search_general_country.setPlaceholderText("Search by country")
        self.ui_master_data.lineEdit_search_general_country.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_21.addWidget(self.ui_master_data.lineEdit_search_general_country)

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.lineEdit_search_general_language = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_language.setObjectName("lineEdit_search_general_language")
        self.ui_master_data.lineEdit_search_general_language.setPlaceholderText("Search by language")
        self.ui_master_data.lineEdit_search_general_language.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_35.addWidget(self.ui_master_data.lineEdit_search_general_language)

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.lineEdit_search_person_nationality = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_nationality.setObjectName("lineEdit_search_person_nationality")
        self.ui_master_data.lineEdit_search_person_nationality.setPlaceholderText("Search by nationality")
        self.ui_master_data.lineEdit_search_person_nationality.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_45.addWidget(self.ui_master_data.lineEdit_search_person_nationality)

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_search_film_genre = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_film_genre.setObjectName("lineEdit_search_film_genre")
        self.ui_master_data.lineEdit_search_film_genre.setPlaceholderText("Search by film-genre")
        self.ui_master_data.lineEdit_search_film_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_3.addWidget(self.ui_master_data.lineEdit_search_film_genre)

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_search_general_award = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_award.setObjectName("lineEdit_search_general_award")
        self.ui_master_data.lineEdit_search_general_award.setPlaceholderText("Search by award")
        self.ui_master_data.lineEdit_search_general_award.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_85.addWidget(self.ui_master_data.lineEdit_search_general_award)

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_search_music_genre = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_music_genre.setObjectName("lineEdit_search_music_genre")
        self.ui_master_data.lineEdit_search_music_genre.setPlaceholderText("Search by genre")
        self.ui_master_data.lineEdit_search_music_genre.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_102.addWidget(self.ui_master_data.lineEdit_search_music_genre)

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_search_person_gender = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_gender.setObjectName("lineEdit_search_person_gender")
        self.ui_master_data.lineEdit_search_person_gender.setPlaceholderText("Search by gender")
        self.ui_master_data.lineEdit_search_person_gender.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_40.addWidget(self.ui_master_data.lineEdit_search_person_gender)

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_search_person_salutation = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_salutation.setObjectName("lineEdit_search_person_salutation")
        self.ui_master_data.lineEdit_search_person_salutation.setPlaceholderText("Search by salutation")
        self.ui_master_data.lineEdit_search_person_salutation.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_50.addWidget(self.ui_master_data.lineEdit_search_person_salutation)

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_search_person_title = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_title.setObjectName("lineEdit_search_person_title")
        self.ui_master_data.lineEdit_search_person_title.setPlaceholderText("Search by title")
        self.ui_master_data.lineEdit_search_person_title.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_55.addWidget(self.ui_master_data.lineEdit_search_person_title)

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_search_person_eye_color = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_eye_color.setObjectName("lineEdit_search_person_eye_color")
        self.ui_master_data.lineEdit_search_person_eye_color.setPlaceholderText("Search by eye color")
        self.ui_master_data.lineEdit_search_person_eye_color.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_65.addWidget(self.ui_master_data.lineEdit_search_person_eye_color)

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_search_person_hair_color = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_hair_color.setObjectName("lineEdit_search_person_hair_color")
        self.ui_master_data.lineEdit_search_person_hair_color.setPlaceholderText("Search by hair color")
        self.ui_master_data.lineEdit_search_person_hair_color.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_60.addWidget(self.ui_master_data.lineEdit_search_person_hair_color)

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_search_person_religion = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_religion.setObjectName("lineEdit_search_person_religion")
        self.ui_master_data.lineEdit_search_person_religion.setPlaceholderText("Search by religion")
        self.ui_master_data.lineEdit_search_person_religion.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_70.addWidget(self.ui_master_data.lineEdit_search_person_religion)

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_search_person_relationship_status = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_relationship_status.setObjectName("lineEdit_search_person_relationship_status")
        self.ui_master_data.lineEdit_search_person_relationship_status.setPlaceholderText("Search by relationship status")
        self.ui_master_data.lineEdit_search_person_relationship_status.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_75.addWidget(self.ui_master_data.lineEdit_search_person_relationship_status)

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_search_person_relationship = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_person_relationship.setObjectName("lineEdit_search_person_relationship")
        self.ui_master_data.lineEdit_search_person_relationship.setPlaceholderText("Search by relationship")
        self.ui_master_data.lineEdit_search_person_relationship.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_92.addWidget(self.ui_master_data.lineEdit_search_person_relationship)

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_search_general_media_type = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_media_type.setObjectName("lineEdit_search_general_media_type")
        self.ui_master_data.lineEdit_search_general_media_type.setPlaceholderText("Search by media typ")
        self.ui_master_data.lineEdit_search_general_media_type.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_108.addWidget(self.ui_master_data.lineEdit_search_general_media_type)

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_search_general_packaging = ButtonLineEdit(image_path_collection = self.image_path_collection)
        self.ui_master_data.lineEdit_search_general_packaging.setObjectName("lineEdit_search_general_packaging")
        self.ui_master_data.lineEdit_search_general_packaging.setPlaceholderText("Search by packaging")
        self.ui_master_data.lineEdit_search_general_packaging.set_default_widget_signal.connect(lambda:
            self.filter_update(proxy_model = self._proxy_model))
        self.ui_master_data.horizontalLayout_113.addWidget(self.ui_master_data.lineEdit_search_general_packaging)

        return

    def init_timer(self):
        '''
            NOTE:
            =====
            This method initializes the user interface of master_data-window. 
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize the user interface")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.search_delay_timer_general_region = QTimer()
        self.search_delay_timer_general_region.setSingleShot(True)
        self.search_delay_timer_general_region.setInterval(1000)
        self.search_delay_timer_general_region.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_region,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.search_delay_timer_general_country = QTimer()
        self.search_delay_timer_general_country.setSingleShot(True)
        self.search_delay_timer_general_country.setInterval(1000)
        self.search_delay_timer_general_country.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_country,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.search_delay_timer_general_language = QTimer()
        self.search_delay_timer_general_language.setSingleShot(True)
        self.search_delay_timer_general_language.setInterval(1000)
        self.search_delay_timer_general_language.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_language,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.search_delay_timer_person_nationality = QTimer()
        self.search_delay_timer_person_nationality.setSingleShot(True)
        self.search_delay_timer_person_nationality.setInterval(1000)
        self.search_delay_timer_person_nationality.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_nationality,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.search_delay_timer_film_genre = QTimer()
        self.search_delay_timer_film_genre.setSingleShot(True)
        self.search_delay_timer_film_genre.setInterval(1000)
        self.search_delay_timer_film_genre.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_film_genre,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.search_delay_timer_general_award = QTimer()
        self.search_delay_timer_general_award.setSingleShot(True)
        self.search_delay_timer_general_award.setInterval(1000)
        self.search_delay_timer_general_award.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_award,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.search_delay_timer_music_genre = QTimer()
        self.search_delay_timer_music_genre.setSingleShot(True)
        self.search_delay_timer_music_genre.setInterval(1000)
        self.search_delay_timer_music_genre.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_music_genre,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.search_delay_timer_person_gender = QTimer()
        self.search_delay_timer_person_gender.setSingleShot(True)
        self.search_delay_timer_person_gender.setInterval(1000)
        self.search_delay_timer_person_gender.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_gender,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.search_delay_timer_person_salutation = QTimer()
        self.search_delay_timer_person_salutation.setSingleShot(True)
        self.search_delay_timer_person_salutation.setInterval(1000)
        self.search_delay_timer_person_salutation.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_salutation,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.search_delay_timer_person_title = QTimer()
        self.search_delay_timer_person_title.setSingleShot(True)
        self.search_delay_timer_person_title.setInterval(1000)
        self.search_delay_timer_person_title.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_title,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.search_delay_timer_person_eye_color = QTimer()
        self.search_delay_timer_person_eye_color.setSingleShot(True)
        self.search_delay_timer_person_eye_color.setInterval(1000)
        self.search_delay_timer_person_eye_color.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_eye_color,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.search_delay_timer_person_hair_color = QTimer()
        self.search_delay_timer_person_hair_color.setSingleShot(True)
        self.search_delay_timer_person_hair_color.setInterval(1000)
        self.search_delay_timer_person_hair_color.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_hair_color,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.search_delay_timer_person_religion = QTimer()
        self.search_delay_timer_person_religion.setSingleShot(True)
        self.search_delay_timer_person_religion.setInterval(1000)
        self.search_delay_timer_person_religion.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_religion,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.search_delay_timer_person_relationship_status = QTimer()
        self.search_delay_timer_person_relationship_status.setSingleShot(True)
        self.search_delay_timer_person_relationship_status.setInterval(1000)
        self.search_delay_timer_person_relationship_status.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship_status,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.search_delay_timer_person_relationship = QTimer()
        self.search_delay_timer_person_relationship.setSingleShot(True)
        self.search_delay_timer_person_relationship.setInterval(1000)
        self.search_delay_timer_person_relationship.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_person_relationship,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.search_delay_timer_general_media_type = QTimer()
        self.search_delay_timer_general_media_type.setSingleShot(True)
        self.search_delay_timer_general_media_type.setInterval(1000)
        self.search_delay_timer_general_media_type.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_media_type,
                                                                   proxy_model = self._proxy_model))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.search_delay_timer_general_packaging = QTimer()
        self.search_delay_timer_general_packaging.setSingleShot(True)
        self.search_delay_timer_general_packaging.setInterval(1000)
        self.search_delay_timer_general_packaging.timeout.connect(lambda:
                                                self.filter_update(line_edit_widget = self.ui_master_data.lineEdit_search_general_packaging,
                                                                   proxy_model = self._proxy_model))

        return

    def create_generally_crritcal_msg(self, 
                                      err_title = None, 
                                      err_msg = None, 
                                      detail_msg = None):
        '''
            NOTE:
            =====
            This method creates a MessageBox for critical. 
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self, err_msg, err_title, QMessageBox.Critical)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_msg)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_question_msg(self, 
                                      text = None, 
                                      title = None, 
                                      icon = None, 
                                      detail_text = None):
        '''
            NOTE:
            =====
            This method creates a MessageBox for question. 
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Create a message box for question")

        mBox = custom_message_box(self, text = text, title = title, icon = icon)
        mBox.setObjectName("generally_question_msg")
        setIc_OK = QIcon(self.image_path_collection.check_icon_default)
        setIc_No = QIcon(self.image_path_collection.no_default)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_text)

        mBox.addButton(QPushButton(setIc_OK, self.tr("Yes")),
                    mBox.YesRole)  # An "OK" button defined with the AcceptRole
        
        mBox.addButton(
                    QPushButton(setIc_No, self.tr("No")),
                    mBox.NoRole)  # An "OK" button defined with the AcceptRole

        clicked_result = mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        if clicked_result == 0:

            return True

        elif clicked_result == 1:

            return False

    def create_generally_information_msg(self, 
                                         title = None, 
                                         msg = None, 
                                         detail_msg = None,
                                         yes_no_button = False,
                                         only_yes_button = False,
                                         line_edit_object_get_focus = None,
                                         icon = None):
        if not self.messagebox_called:

            if yes_no_button:

                yes_no_msg_box = custom_message_box(self, text = msg, 
                                                          title = title, 
                                                          icon = icon)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_no_msg_box.setInformativeText(detail_msg)

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Yes")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("No")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.

                # 1 means No
                if result_button == 1:
                    self.close()
                # 0 means Yes
                elif result_button == 0:
                    self._image_path = None
                    self.pixmap = None
                    self.ui_person_profile.label_photo.clear()
                    self.set_all_widget_on_default()
                    self.quite_threads()

            if only_yes_button:

                yes_msg_box = custom_message_box(self, 
                                                  text = msg, 
                                                  title = title, 
                                                  icon = icon)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_msg_box.setInformativeText(detail_msg)

                yes_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Ok")),
                    #QPushButton(self.tr(" No")),
                    yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
                yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

                self.set_focus(widget_object = line_edit_object_get_focus)

    def init_text_editor(self):
        '''
            NOTE:
            =====
            This method sets style sheet on QTextEdit. 
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Set style sheet on QTextEdit")

        appStyle="""
                QTextEdit
                {
                    color: black;
                }
                """
        self.ui_master_data.textEdit_notify_general_region.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_country.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_language.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_person_nationality.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_film_genre.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_award.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_music_genre.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_packaging.setStyleSheet(appStyle)
        self.ui_master_data.textEdit_notify_general_media_type.setStyleSheet(appStyle)

        return

    def init_returnPressed_signal_line_edit(self):
        '''
            NOTE:
            ======
            This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).  

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize returnPressed-Signal for QLineEdit()")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.lineEdit_general_region.returnPressed.connect(self.ui_master_data.pushButton_save_general_region.click)
        self.ui_master_data.lineEdit_edit_general_region.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_region.click)
        self.ui_master_data.lineEdit_search_general_region.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_region,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.lineEdit_general_country.returnPressed.connect(self.ui_master_data.pushButton_save_general_country.click)
        self.ui_master_data.lineEdit_edit_general_country.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_country.click)
        self.ui_master_data.lineEdit_search_general_country.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_country,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment.")))         

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.lineEdit_general_language.returnPressed.connect(self.ui_master_data.pushButton_save_general_language.click)
        self.ui_master_data.lineEdit_edit_general_language.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_language.click)
        self.ui_master_data.lineEdit_search_general_language.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_language,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.lineEdit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_save_person_nationality.click)
        self.ui_master_data.lineEdit_edit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_nationality.click)
        self.ui_master_data.lineEdit_search_person_nationality.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_nationality,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_save_film_genre.click)
        self.ui_master_data.lineEdit_edit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_genre.click)
        self.ui_master_data.lineEdit_search_film_genre.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_film_genre,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_general_award.returnPressed.connect(self.ui_master_data.pushButton_save_general_award.click)
        self.ui_master_data.lineEdit_edit_general_award.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_award.click)
        self.ui_master_data.lineEdit_search_general_award.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_award,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_music_genre.returnPressed.connect(self.ui_master_data.pushButton_save_music_genre.click)
        self.ui_master_data.lineEdit_edit_music_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_music_genre.click)
        self.ui_master_data.lineEdit_search_music_genre.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_music_genre,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_save_person_gender.click)
        self.ui_master_data.lineEdit_edit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_gender.click)
        self.ui_master_data.lineEdit_search_person_gender.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_gender,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_save_person_salutation.click)
        self.ui_master_data.lineEdit_edit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_salutation.click)
        self.ui_master_data.lineEdit_search_person_salutation.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_salutation,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_person_title.returnPressed.connect(self.ui_master_data.pushButton_save_person_title.click)
        self.ui_master_data.lineEdit_edit_person_title.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_title.click)
        self.ui_master_data.lineEdit_search_person_title.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_title,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_eye_color.click)
        self.ui_master_data.lineEdit_edit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_eye_color.click)
        self.ui_master_data.lineEdit_search_person_eye_color.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_eye_color,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_hair_color.click)
        self.ui_master_data.lineEdit_edit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_hair_color.click)
        self.ui_master_data.lineEdit_search_person_hair_color.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_hair_color,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_save_person_religion.click)
        self.ui_master_data.lineEdit_edit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_religion.click)
        self.ui_master_data.lineEdit_search_person_religion.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_religion,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship_status.click)
        self.ui_master_data.lineEdit_edit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship_status.click)
        self.ui_master_data.lineEdit_search_person_relationship_status.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_relationship_status,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship.click)
        self.ui_master_data.lineEdit_edit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship.click)
        self.ui_master_data.lineEdit_search_person_relationship.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_person_relationship,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_general_media_type.returnPressed.connect(self.ui_master_data.pushButton_save_general_media_type.click)
        self.ui_master_data.lineEdit_edit_general_media_type.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_media_type.click)
        self.ui_master_data.lineEdit_search_general_media_type.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_media_type,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_general_packaging.returnPressed.connect(self.ui_master_data.pushButton_save_general_packaging.click)
        self.ui_master_data.lineEdit_edit_general_packaging.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_packaging.click)
        self.ui_master_data.lineEdit_search_general_packaging.returnPressed.connect(lambda:
                                                                               self.handle_timer(timer_widget = self.search_delay_timer_general_packaging,
                                                                                                 show_wait_message = True,
                                                                                                 text = self.tr("Searching for records. This should only take a moment."))) 

        return
         
    def init_custom_progres_bar(self):
        '''
            NOTE:
            =====
            This method implements an overridden QProgressBar().

            DETAILS:
            ========
            The overridden ProgressBars Range is set to minimum = 0
            and to maximum = 0 for pulsing. The overwridden ProgressBar
            contains also the setText()-method, so the text is also used.
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        print "init_custom_progres_bar is called"
        logger.info("Create a overwritten QProgressbar")

        self.hide_widget(widget = self.ui_master_data.frame_progress_bar, boolen_value = True)

        self.busy_progress_bar = ProgressBarText(self)
        self.busy_progress_bar.setObjectName("busy_progress_bar")
        self.busy_progress_bar.setRange(0, 0)

        logger.info(self.busy_progress_bar.objectName())
        
        self.set_text(widget = self.busy_progress_bar, text = 'Waiting for...')

        #   add the ProgressBarText()-object to the QLayout
        self.horizontal_Layout = self.ui_master_data.horizontalLayout_progress_bar
        self.horizontal_Layout.addWidget(self.busy_progress_bar)

        return

    def init_tree_view(self):
        '''
            NOTICE:
            =======
            Here we initialize the QTreeView()-object for all categories.
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Initialize QTreeView for all categories")

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.tree_view_general_country.setModel(self._standard_item_model.standard_item_model_general_country)
        self.ui_master_data.tree_view_general_country.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_country.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_country.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_country,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_country,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_country:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.tree_view_general_region.setModel(self._standard_item_model.standard_item_model_general_region)
        self.ui_master_data.tree_view_general_region.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_region.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_region.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_region,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_region,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_region:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.tree_view_general_language.setModel(self._standard_item_model.standard_item_model_general_language)
        self.ui_master_data.tree_view_general_language.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_language.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_language.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_language,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_language,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_language:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Nationality  ##############
        ###################################################
        self.ui_master_data.tree_view_person_nationality.setModel(self._standard_item_model.standard_item_model_person_nationality)
        self.ui_master_data.tree_view_person_nationality.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_nationality.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_nationality.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_nationality,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_nationality,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_nationality:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.tree_view_film_genre.setModel(self._standard_item_model.standard_item_model_film_genre)
        self.ui_master_data.tree_view_film_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_film_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_film_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_film_genre:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.tree_view_general_award.setModel(self._standard_item_model.standard_item_model_general_award)
        self.ui_master_data.tree_view_general_award.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_award.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_award.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_award,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_award,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_award:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.tree_view_music_genre.setModel(self._standard_item_model.standard_item_model_music_genre)
        self.ui_master_data.tree_view_music_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_music_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_music_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_music_genre,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_music_genre,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_music_genre:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.tree_view_person_gender.setModel(self._standard_item_model.standard_item_model_person_gender)
        self.ui_master_data.tree_view_person_gender.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_gender.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_gender.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_gender,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_gender,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_gender:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.tree_view_person_salutation.setModel(self._standard_item_model.standard_item_model_person_salutation)
        self.ui_master_data.tree_view_person_salutation.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_salutation.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_salutation.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_salutation,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_salutation,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_salutation:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.tree_view_person_title.setModel(self._standard_item_model.standard_item_model_person_title)
        self.ui_master_data.tree_view_person_title.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_title.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_title.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_title,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_title,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_title:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.tree_view_person_eye_color.setModel(self._standard_item_model.standard_item_model_person_eye_color)
        self.ui_master_data.tree_view_person_eye_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_eye_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_eye_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_eye_color,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_eye_color,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_eye_color:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.tree_view_person_hair_color.setModel(self._standard_item_model.standard_item_model_person_hair_color)
        self.ui_master_data.tree_view_person_hair_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_hair_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_hair_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_hair_color,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_hair_color,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_hair_color:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.tree_view_person_religion.setModel(self._standard_item_model.standard_item_model_person_religion)
        self.ui_master_data.tree_view_person_religion.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_religion.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_religion.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_religion,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_religion,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_religion:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.tree_view_person_relationship_status.setModel(self._standard_item_model.standard_item_model_person_relationship_status)
        self.ui_master_data.tree_view_person_relationship_status.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship_status.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship_status.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_relationship_status,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_relationship_status,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_relationship_status:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.tree_view_person_relationship.setModel(self._standard_item_model.standard_item_model_person_relationship)
        self.ui_master_data.tree_view_person_relationship.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_person_relationship,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_person_relationship,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_person_relationship:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.tree_view_general_media_type.setModel(self._standard_item_model.standard_item_model_general_media_type)
        self.ui_master_data.tree_view_general_media_type.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_media_type.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_media_type.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_media_type,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_media_type,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_media_type:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.tree_view_general_packaging.setModel(self._standard_item_model.standard_item_model_general_packaging)
        self.ui_master_data.tree_view_general_packaging.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_packaging.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_packaging.customContextMenuRequested.connect(lambda position,
            tree_view_obj = self.ui_master_data.tree_view_general_packaging,
            pushbutton_object_edit = self.ui_master_data.pushButton_edit_general_packaging,
            pushbutton_object_delete = self.ui_master_data.pushButton_delete_general_packaging:
        self.open_context_menu(position = position, 
                               tree_view_obj = tree_view_obj, 
                               pushbutton_object_edit = pushbutton_object_edit, 
                               pushbutton_object_delete = pushbutton_object_delete))


        return
        
    def init_navi_treeview(self):
        '''
            NOTICE:
            =======
            Here we initialize a special QTreeView()-object for the navigation menu.
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Initialize QTreeView for all navigation menu")

        self.ui_master_data.treeView_navigation.setModel(self._navigate_standard_model)
        self.ui_master_data.treeView_navigation.sortByColumn(1, Qt.AscendingOrder)
        
        #self.populate_navi_data(model_obj = self._navigate_standard_model, data = self.NAVI_DATA)

        self.set_hide_column(tree_view_object = self.ui_master_data.treeView_navigation,
                             column = 1,
                             bool_value = True)

        self.ui_master_data.treeView_navigation.expandAll()

        return

    def init_selectionChanged_signal_treeview(self):
        '''
            NOTICE:
            =======
            We need to initialize the selectionChanged-signals for QTreeView .
            
            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Initialize selectionChanged-signal for QTreeView")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.tree_view_general_region.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_region:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.tree_view_general_country.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_country:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.tree_view_general_language.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_language:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.tree_view_person_nationality.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_nationality:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.tree_view_film_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_film_genre:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.tree_view_general_award.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_award:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.tree_view_music_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_music_genre:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.tree_view_person_gender.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_gender:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.tree_view_person_salutation.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_salutation:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.tree_view_person_title.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_title:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.tree_view_person_eye_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_eye_color:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.tree_view_person_hair_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_hair_color:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.tree_view_person_religion.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_religion:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.tree_view_person_relationship_status.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_relationship_status:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.tree_view_person_relationship.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_person_relationship:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.tree_view_general_media_type.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_media_type:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.tree_view_general_packaging.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj = self.ui_master_data.lineEdit_general_packaging:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, widget = edit_line_obj))

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_master_data.pushButton_refresh_general_region.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_region"))

        self.ui_master_data.pushButton_save_general_region.clicked.connect(lambda: self.manage_methods(method_name = "add_general_region"))

        self.ui_master_data.pushButton_apply_change_general_region.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_region"))

        self.ui_master_data.pushButton_delete_general_region.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_region"))

        
        self.ui_master_data.pushButton_download_general_region.clicked.connect(lambda:
                                                                               self.change_page(index = 2,
                                                                                                stack_widget_object = self.ui_master_data.stackedWidget_general_region,
                                                                                                column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_region.clicked.connect(lambda:
                                                                                    self.change_page(index = 0,
                                                                                                     stack_widget_object = self.ui_master_data.stackedWidget_general_region,
                                                                                                     column_index = 1))

        self.ui_master_data.pushButton_update_general_region.clicked.connect(lambda: self.manage_methods(method_name = "update_general_region"))

        self.ui_master_data.pushButton_edit_general_region.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_region,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_region,
                             set_text = self.ui_master_data.lineEdit_general_region.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_region.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_region, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_country.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_country"))

        self.ui_master_data.pushButton_save_general_country.clicked.connect(lambda: self.manage_methods(method_name = "add_general_country"))

        self.ui_master_data.pushButton_apply_change_general_country.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_country"))

        self.ui_master_data.pushButton_delete_general_country.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_country"))
        
        self.ui_master_data.pushButton_download_general_country.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_general_country,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_country.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_general_country,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_country.clicked.connect(lambda: self.manage_methods(method_name = "update_general_country"))


        self.ui_master_data.pushButton_edit_general_country.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_country,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_country,
                             set_text = self.ui_master_data.lineEdit_general_country.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_country.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_country, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))
        
        self.ui_master_data.pushButton_master_data_close.clicked.connect(self.close)

        ###################################################
        #################  General Language  ##############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_language.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_language"))

        self.ui_master_data.pushButton_save_general_language.clicked.connect(lambda: self.manage_methods(method_name = "add_general_language"))

        self.ui_master_data.pushButton_apply_change_general_language.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_language"))

        self.ui_master_data.pushButton_delete_general_language.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_language"))
        
        self.ui_master_data.pushButton_download_general_language.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_general_language,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_language.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_general_language,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_language.clicked.connect(lambda: self.manage_methods(method_name = "update_general_language"))


        self.ui_master_data.pushButton_edit_general_language.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_language,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_language,
                             set_text = self.ui_master_data.lineEdit_general_language.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        #self.ui_master_data.pushButton_delete_general_region.clicked.connect(self.handle_delete)

        self.ui_master_data.pushButton_cancel_edit_general_language.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_language, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Nationality  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_nationality.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_nationality"))

        self.ui_master_data.pushButton_save_person_nationality.clicked.connect(lambda: self.manage_methods(method_name = "add_person_nationality"))

        self.ui_master_data.pushButton_apply_change_person_nationality.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_nationality"))

        self.ui_master_data.pushButton_delete_person_nationality.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_nationality"))
        
        self.ui_master_data.pushButton_download_person_nationality.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_person_nationality,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_person_nationality.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_person_nationality,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_person_nationality.clicked.connect(lambda: self.manage_methods(method_name = "update_person_nationality"))


        self.ui_master_data.pushButton_edit_person_nationality.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_nationality,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_nationality,
                             set_text = self.ui_master_data.lineEdit_person_nationality.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_nationality.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_nationality, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.pushButton_refresh_film_genre.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_film_genre"))

        self.ui_master_data.pushButton_save_film_genre.clicked.connect(lambda: self.manage_methods(method_name = "add_film_genre"))

        self.ui_master_data.pushButton_apply_change_film_genre.clicked.connect(lambda: self.manage_methods(method_name = "edit_film_genre"))

        self.ui_master_data.pushButton_delete_film_genre.clicked.connect(lambda: self.manage_methods(method_name = "delete_film_genre"))
        
        self.ui_master_data.pushButton_download_film_genre.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_film_genre,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_film_genre.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_film_genre,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_film_genre.clicked.connect(lambda: self.manage_methods(method_name = "update_film_genre"))


        self.ui_master_data.pushButton_edit_film_genre.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_film_genre,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_film_genre,
                             set_text = self.ui_master_data.lineEdit_film_genre.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_film_genre.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_film_genre, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.pushButton_refresh_general_award.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_award"))

        self.ui_master_data.pushButton_save_general_award.clicked.connect(lambda: self.manage_methods(method_name = "add_general_award"))

        self.ui_master_data.pushButton_apply_change_general_award.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_award"))

        self.ui_master_data.pushButton_delete_general_award.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_award"))
        
        self.ui_master_data.pushButton_download_general_award.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_general_award,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_award.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_general_award,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_award.clicked.connect(lambda: self.manage_methods(method_name = "update_general_award"))


        self.ui_master_data.pushButton_edit_general_award.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_award,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_award,
                             set_text = self.ui_master_data.lineEdit_general_award.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_award.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_award, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.pushButton_refresh_music_genre.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_music_genre"))

        self.ui_master_data.pushButton_save_music_genre.clicked.connect(lambda: self.manage_methods(method_name = "add_music_genre"))

        self.ui_master_data.pushButton_apply_change_music_genre.clicked.connect(lambda: self.manage_methods(method_name = "edit_music_genre"))

        self.ui_master_data.pushButton_delete_music_genre.clicked.connect(lambda: self.manage_methods(method_name = "delete_music_genre"))
        
        self.ui_master_data.pushButton_download_music_genre.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_music_genre,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_music_genre.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_music_genre,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_music_genre.clicked.connect(lambda: self.manage_methods(method_name = "update_music_genre"))


        self.ui_master_data.pushButton_edit_music_genre.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_music_genre,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_music_genre,
                             set_text = self.ui_master_data.lineEdit_music_genre.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_music_genre.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_music_genre, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.pushButton_refresh_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_gender"))

        self.ui_master_data.pushButton_save_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "add_person_gender"))

        self.ui_master_data.pushButton_apply_change_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_gender"))

        self.ui_master_data.pushButton_delete_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_gender"))
        
        # self.ui_master_data.pushButton_download_person_gender.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_gender.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_gender.clicked.connect(lambda: self.manage_methods(method_name = "update_person_gender"))


        self.ui_master_data.pushButton_edit_person_gender.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_gender,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_gender,
                             set_text = self.ui_master_data.lineEdit_person_gender.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_gender.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_gender, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_salutation"))

        self.ui_master_data.pushButton_save_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "add_person_salutation"))

        self.ui_master_data.pushButton_apply_change_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_salutation"))

        self.ui_master_data.pushButton_delete_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_salutation"))
        
        # self.ui_master_data.pushButton_download_person_salutation.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_salutation.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_salutation.clicked.connect(lambda: self.manage_methods(method_name = "update_person_salutation"))


        self.ui_master_data.pushButton_edit_person_salutation.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_salutation,
                             set_text = self.ui_master_data.lineEdit_person_salutation.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_salutation.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_salutation, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.pushButton_refresh_person_title.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_title"))

        self.ui_master_data.pushButton_save_person_title.clicked.connect(lambda: self.manage_methods(method_name = "add_person_title"))

        self.ui_master_data.pushButton_apply_change_person_title.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_title"))

        self.ui_master_data.pushButton_delete_person_title.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_title"))
        
        # self.ui_master_data.pushButton_download_person_title.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_title,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_title.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_title,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_title.clicked.connect(lambda: self.manage_methods(method_name = "update_person_title"))


        self.ui_master_data.pushButton_edit_person_title.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_title,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_title,
                             set_text = self.ui_master_data.lineEdit_person_title.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_title.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_title, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_eye_color"))

        self.ui_master_data.pushButton_save_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "add_person_eye_color"))

        self.ui_master_data.pushButton_apply_change_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_eye_color"))

        self.ui_master_data.pushButton_delete_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_eye_color"))
        
        # self.ui_master_data.pushButton_download_person_eye_color.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_eye_color.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_eye_color.clicked.connect(lambda: self.manage_methods(method_name = "update_person_eye_color"))


        self.ui_master_data.pushButton_edit_person_eye_color.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_eye_color,
                             set_text = self.ui_master_data.lineEdit_person_eye_color.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_eye_color.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_eye_color, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_hair_color"))

        self.ui_master_data.pushButton_save_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "add_person_hair_color"))

        self.ui_master_data.pushButton_apply_change_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_hair_color"))

        self.ui_master_data.pushButton_delete_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_hair_color"))
        
        # self.ui_master_data.pushButton_download_person_hair_color.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_hair_color.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_hair_color.clicked.connect(lambda: self.manage_methods(method_name = "update_person_hair_color"))


        self.ui_master_data.pushButton_edit_person_hair_color.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_hair_color,
                             set_text = self.ui_master_data.lineEdit_person_hair_color.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_hair_color.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_hair_color, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.pushButton_refresh_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_religion"))

        self.ui_master_data.pushButton_save_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "add_person_religion"))

        self.ui_master_data.pushButton_apply_change_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_religion"))

        self.ui_master_data.pushButton_delete_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_religion"))
        
        # self.ui_master_data.pushButton_download_person_religion.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_religion.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_religion.clicked.connect(lambda: self.manage_methods(method_name = "update_person_religion"))


        self.ui_master_data.pushButton_edit_person_religion.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_religion,
                             set_text = self.ui_master_data.lineEdit_person_religion.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_religion.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_religion, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.pushButton_refresh_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_relationship_status"))

        self.ui_master_data.pushButton_save_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "add_person_relationship_status"))

        self.ui_master_data.pushButton_apply_change_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_relationship_status"))

        self.ui_master_data.pushButton_delete_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_relationship_status"))
        
        # self.ui_master_data.pushButton_download_person_relationship_status.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_relationship_status.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_relationship_status.clicked.connect(lambda: self.manage_methods(method_name = "update_person_relationship_status"))


        self.ui_master_data.pushButton_edit_person_relationship_status.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship_status,
                             set_text = self.ui_master_data.lineEdit_person_relationship_status.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_relationship_status.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_relationship_status, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.pushButton_refresh_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_person_relationship"))

        self.ui_master_data.pushButton_save_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "add_person_relationship"))

        self.ui_master_data.pushButton_apply_change_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "edit_person_relationship"))

        self.ui_master_data.pushButton_delete_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "delete_person_relationship"))
        
        # self.ui_master_data.pushButton_download_person_relationship.clicked.connect(lambda:
        #                                                                       self.change_page(index = 2,
        #                                                                                        stack_widget_object = self.ui_master_data.stackedWidget_person_relationship,
        #                                                                                        column_index = 1))
        # self.ui_master_data.pushButton_cancel_update_person_relationship.clicked.connect(lambda:
        #                                                                            self.change_page(index = 0,
        #                                                                                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship,
        #                                                                                             column_index = 1))

        # self.ui_master_data.pushButton_update_person_relationship.clicked.connect(lambda: self.manage_methods(method_name = "update_person_relationship"))


        self.ui_master_data.pushButton_edit_person_relationship.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship,
                             set_text = self.ui_master_data.lineEdit_person_relationship.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_person_relationship.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_relationship, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_media_type"))

        self.ui_master_data.pushButton_save_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "add_general_media_type"))

        self.ui_master_data.pushButton_apply_change_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_media_type"))

        self.ui_master_data.pushButton_delete_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_media_type"))
        
        self.ui_master_data.pushButton_download_general_media_type.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_general_media_type,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_media_type.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_general_media_type,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_media_type.clicked.connect(lambda: self.manage_methods(method_name = "update_general_media_type"))


        self.ui_master_data.pushButton_edit_general_media_type.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_media_type,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_media_type,
                             set_text = self.ui_master_data.lineEdit_general_media_type.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_media_type.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_media_type, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.pushButton_refresh_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "fetch_all_general_packaging"))

        self.ui_master_data.pushButton_save_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "add_general_packaging"))

        self.ui_master_data.pushButton_apply_change_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "edit_general_packaging"))

        self.ui_master_data.pushButton_delete_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "delete_general_packaging"))
        
        self.ui_master_data.pushButton_download_general_packaging.clicked.connect(lambda:
                                                                              self.change_page(index = 2,
                                                                                               stack_widget_object = self.ui_master_data.stackedWidget_general_packaging,
                                                                                               column_index = 1))
        self.ui_master_data.pushButton_cancel_update_general_packaging.clicked.connect(lambda:
                                                                                   self.change_page(index = 0,
                                                                                                    stack_widget_object = self.ui_master_data.stackedWidget_general_packaging,
                                                                                                    column_index = 1))

        self.ui_master_data.pushButton_update_general_packaging.clicked.connect(lambda: self.manage_methods(method_name = "update_general_packaging"))


        self.ui_master_data.pushButton_edit_general_packaging.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_packaging,
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_packaging,
                             set_text = self.ui_master_data.lineEdit_general_packaging.text(),
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_cancel_edit_general_packaging.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_packaging, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        return

    def open_context_menu(self, 
                          position = None,  
                          tree_view_obj = None,
                          pushbutton_object_edit = None,
                          pushbutton_object_delete = None):
        '''
            NOTICE:
            =======
            This method is used to create Custom context menu for QTreeView.

            PARAMETERS:
            ===========
            :position                   -   The current positions of the mouse point is important.

            :tree_view_obj              -   We need a QTreeView-widget

            :pushbutton_object_edit     -   We expect a QPushButton for the change 
                                            of records

            :pushbutton_object_delete   -   We also need a QPushButton for the delete
                                            of records

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        indexes = tree_view_obj.selectedIndexes()

        if len(indexes) > 0:

            delete_action = QAction(QIcon(self.image_path_collection.delete_default), self.tr("Delete"), self)
            delete_action.setObjectName("delete_action_master_data")        
            delete_action.triggered.connect(pushbutton_object_delete.click)

            edit_action = QAction(QIcon(self.image_path_collection.edit_default), self.tr("Edit"), self)        
            edit_action.triggered.connect(pushbutton_object_edit.click)
        
        
            menu = QMenu()
            menu.setObjectName("context_menu_master_data")
            menu.addAction(edit_action)
            menu.addAction(delete_action)
            
            menu.exec_(tree_view_obj.viewport().mapToGlobal(position))

        return

    def init_textChanged_signal_line_edit(self):
        '''
            NOTICE:
            =======
            This method is used to set signal and slots of textChanged for QLineEdit()-objects. 
            
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        ###################################################
        ################  General Region  #################
        ###################################################
        self.ui_master_data.lineEdit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_region,))

        self.ui_master_data.lineEdit_edit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_region,
                                     check_selected_id = False))

        ###################################################
        ################  General Country  ################
        ###################################################
        self.ui_master_data.lineEdit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_country,))

        self.ui_master_data.lineEdit_edit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_country,
                                     check_selected_id = False))

        ###################################################
        ################  General Language  ################
        ###################################################
        self.ui_master_data.lineEdit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_language,))

        self.ui_master_data.lineEdit_edit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_language,
                                     check_selected_id = False))

        ###################################################
        ################  Person Nationality  ##############
        ###################################################
        self.ui_master_data.lineEdit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_nationality,))

        self.ui_master_data.lineEdit_edit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_nationality,
                                     check_selected_id = False))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_master_data.lineEdit_film_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_film_genre,))

        self.ui_master_data.lineEdit_edit_film_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_film_genre,
                                     check_selected_id = False))

        ###################################################
        #################  General Award  #################
        ###################################################
        self.ui_master_data.lineEdit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_award,))

        self.ui_master_data.lineEdit_edit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_award,
                                     check_selected_id = False))

        ###################################################
        #################  Music Genre  ###################
        ###################################################
        self.ui_master_data.lineEdit_music_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_music_genre,))

        self.ui_master_data.lineEdit_edit_music_genre.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_music_genre,
                                     check_selected_id = False))

        ###################################################
        #################  Person Gender  #################
        ###################################################
        self.ui_master_data.lineEdit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_gender,))

        self.ui_master_data.lineEdit_edit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_gender,
                                     check_selected_id = False))

        ###################################################
        #################  Person Salutation  #############
        ###################################################
        self.ui_master_data.lineEdit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_salutation,))

        self.ui_master_data.lineEdit_edit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_salutation,
                                     check_selected_id = False))

        ###################################################
        #################  Person Title  ##################
        ###################################################
        self.ui_master_data.lineEdit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_title,))

        self.ui_master_data.lineEdit_edit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_title,
                                     check_selected_id = False))

        ###################################################
        #################  Person Eye Colour  #############
        ###################################################
        self.ui_master_data.lineEdit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_eye_color,))

        self.ui_master_data.lineEdit_edit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_eye_color,
                                     check_selected_id = False))

        ###################################################
        #################  Person Hair Colour  ############
        ###################################################
        self.ui_master_data.lineEdit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_hair_color,))

        self.ui_master_data.lineEdit_edit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_hair_color,
                                     check_selected_id = False))

        ###################################################
        #################  Person Religion  ###############
        ###################################################
        self.ui_master_data.lineEdit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_religion,))

        self.ui_master_data.lineEdit_edit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_religion,
                                     check_selected_id = False))

        ###################################################
        #################  Person Relationship Status  ####
        ###################################################
        self.ui_master_data.lineEdit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_relationship_status,))

        self.ui_master_data.lineEdit_edit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_relationship_status,
                                     check_selected_id = False))

        ###################################################
        #################  Person Relationship  ###########
        ###################################################
        self.ui_master_data.lineEdit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_person_relationship,))

        self.ui_master_data.lineEdit_edit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_person_relationship,
                                     check_selected_id = False))

        ###################################################
        #################  General Media Type  ############
        ###################################################
        self.ui_master_data.lineEdit_general_media_type.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_media_type,))

        self.ui_master_data.lineEdit_edit_general_media_type.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_media_type,
                                     check_selected_id = False))

        ###################################################
        #################  General Packaging  #############
        ###################################################
        self.ui_master_data.lineEdit_general_packaging.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_general_packaging,))

        self.ui_master_data.lineEdit_edit_general_packaging.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_master_data.lineEdit_edit_general_packaging,
                                     check_selected_id = False))

        return

    def init_item_clicked_signal_tree_widget(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots of clicked for QTreeWidget()-object(s). 
            
            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        self.ui_master_data.treeView_navigation.selectionModel().selectionChanged.connect(lambda new_index: 
            self.change_page(new_index = new_index, tree_view_object = self.ui_master_data.treeView_navigation, 
                stack_widget_object = self.ui_master_data.stackedWidget, column_index = 1))

        return

    def set_cursor_position(self, 
                            widget = None, 
                            pos = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :widget  -
            
            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(widget, QLineEdit):
            widget.setCursorPosition(pos)

        return


    def handle_text_changed(self,
                            line_edit_obj = None,
                            push_button_obj_save = None,
                            push_button_obj_delete = None,
                            push_button_obj_edit = None,
                            push_button_obj_refresh = None,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False, 
                            max_length = None,
                            check_selected_id = True):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj              -   Here, we want a QWidget()-object: QLineEdit.
                                            Well, we need this object to get the current position
                                            of the cursor.

            :push_button_obj_save       -   specific pushButton()-object for saving data.

            :push_button_obj_delete     -   specific pushButton()-object for deleting data.

            :push_button_obj_edit       -   specific pushButton()-object for editing data.

            :push_button_obj_refresh    -   specific pushButton()-object for refreshing data.


            :change_windows_title       -   In this keyword argument we except a special value: boolean value.
                                            By default, the value is set to False, because we don't want the
                                            window title to change.

            :allow_trailing_spaces      -   We except a boolean value. By default, its set to True.
                                            True means that spaces between the each words are allowed.
                                            It is very rare that we want to ban trailing space.

            :allow_only_number          -   We except a boolean value. When we get True we
                                            want to know we shoukd allow only numbers. By default
                                            the value is set to False, because we want the user to
                                            enter all characters.    

            :max_length                 -   This keyword-argument gets an integer for  setting a maximum length 
                                            of a given text.

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text=unicode(line_edit_obj.text()),
                                                                      widget_object=line_edit_obj, 
                                                                      allow_trailing_spaces=allow_trailing_spaces,
                                                                      allow_only_number=allow_only_number,
                                                                      max_length=max_length)


        self.set_text(widget = line_edit_obj, text = get_text)
        self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if get_text:
            '''
                No, the string isn't empty
            '''
            self.set_text(widget = line_edit_obj, text = get_text)
            self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

            # self.set_enabled_widget(signle_widget=push_button_obj_save,
            #                              boolean_value=True)

        else:
            '''
                Bad news, the string is emptyx.
            '''

            self.current_selected_id=None

        #     self.set_enabled_widget(signle_widget=push_button_obj_save,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                  boolean_value=False)


        # if check_selected_id:

        #     '''
        #         Its possible that the variable named (self.selected_be_edited_id) is still
        #         not empty. For instance the user clicked on a TreeView()-object. In 
        #         this case special pushButton()-obejcts must be enabled oder disabled.
        #     '''
        #     if not self.selected_be_edited_id is None:


        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=True)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=True)

        #     else:

        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=False)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=False)

        return

    def clear_widget_object_content(self, 
                                    list_widget_object = None, 
                                    widget_object = None):
        '''
            NOTICE:
            =======
            Clear the content of given widget-objects, that supports clear()-method.

            PARAMETERS:
            ===========
            :list_widget_object     -   Here, we get a list of widget-objects.

            :widget_object          -   We get a single widget object.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Clear the content of given widgets")

        if isinstance(list_widget_object, list):

            for widget in self.generator_widget_object(list_widget_object):
                widget.clear()

        if not widget_object is None:
            widget_object.clear()

        return

    def handle_edit(self, 
                    index = None, 
                     new_index = None,
                     stack_widget_object = None, 
                     tree_view_object = None,
                     column_index = None,
                     set_text = None, 
                     lineEdit_object = None,
                     set_focus = None,
                     clear_widget = True,
                     load_data = True,
                     current_obj_clear_selection = False,
                     current_obj_clear = False):
        '''
            NOTICE:
            =======
            This method is just a intermediate function. 

            PARAMETERS:
            ===========
            :index                          -   The given index changes the view of stacked widget

            :new_index                      -   The new on of the given index changes the view of stacked widget

            :stack_widget_object            -   We need a QStackedWidget object

            :tree_view_object               -   We also need a QTreeView object

            :column_index                   -   For QTreeView object we expect a index for column 

            :set_text                       -   We get a text - context of QLineEdit object

            :lineEdit_object                -   We need a QLineEdit to set the text there

            :set_focus                      -   When we should set the focus in given QLineEdit object, we set to True, otherwise to False

            :clear_widget                   -   When we should clear the given QLineEdit object, we set to True, otherwise to False

            :load_data                      -   When we want the program to load records again, one the page of stacked widget
                                                has been changed we set to True, otherwise to False 

            :current_obj_clear_selection    -   Its True, we clear the QTreeView selection

            :current_obj_clear              -   Its True we just clear all widget objects in current pagp, except QTreeView.

            :return                         -   Nothing is returned. The statement 'return'
                                                terminates a function. That makes sure that the
                                                function is definitely finished.
        '''
        logger.info("<use the intermediate function")

        try:

            #   When the user clicks QTreeView as usual the ID of the record is saved 
            #   in self.selected_be_edited_id and self.selected_be_edited_id. 
            #   But when the user doesn't do that and clicks the 
            #   edit button directly, we will generate an error message from the TypeError execption,
            #   otjerwise we will scroll through the StackWidget.
            int(self.selected_be_edited_id)
            int(self.current_selected_id)

            self.stack_set_current_index(index = index, 
                                        new_index = new_index,
                                        stack_widget_object = stack_widget_object, 
                                        tree_view_object = tree_view_object,
                                        column_index = column_index,
                                        set_text = set_text, 
                                        lineEdit_object = lineEdit_object,
                                        set_focus = set_focus,
                                        clear_widget = clear_widget,
                                        load_data = load_data,
                                        current_obj_clear_selection = current_obj_clear_selection,
                                        current_obj_clear = current_obj_clear)

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or\
            not desired_trace.find("int() argument must be a string or a number, not") == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to edit an item. You must select an item to edit.")

                self.create_generally_crritcal_msg(err_title = title_text, 
                                                   err_msg = conn_err, 
                                                   detail_msg = "")

        return

    def handle_delete(self):
        '''
            NOTICE:
            =======
            This method is just a intermediate function. 

            PARAMETERS:
            ===========
            :return                         -   Nothing is returned. The statement 'return'
                                                terminates a function. That makes sure that the
                                                function is definitely finished.
        '''
        logger.info("<use the intermediate function")

        try:

            #   When the user clicks QTreeView as usual the ID of the record is saved 
            #   in self.selected_be_edited_id and self.selected_be_edited_id. 
            #   But when the user doesn't do that and clicks the 
            #   edit button directly, we will generate an error message from the TypeError execption,
            #   otjerwise we will scroll through the StackWidget.
            int(self.selected_be_edited_id)
            int(self.current_selected_id)

            print "Deleting item..."

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or\
            not desired_trace.find("int() argument must be a string or a number, not") == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item. You must select an item to edit.")

                self.create_generally_crritcal_msg(err_title = title_text, 
                                                   err_msg = conn_err, 
                                                   detail_msg = "")

        return

    def handle_selection_changed(self, 
                                 new_index = None, 
                                 old_index = None, 
                                 widget = None):
        '''
            NOTICE:
            =======
            This method is used to get selected item. 

            PARAMETERS:
            ===========
            :new_index      -   #selectionChanged() sends new QItemSelection

            :old_index      -   #selectionChanged() sends old QItemSelection

            :widget         -   We get a single widget object.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        ''' 
        logger.info("<use the intermediate function")

        try: #if qItemSelection
            #   We need access to special date of selected item#.
            #   By using indexes() with the number in square brackets we can
            #   extract it. With data() we automatically convert a QVariant back to a 
            #   Python object
            self.selected_be_edited_id = new_index.indexes()[0].data() #    We want the selected ID of the record

            self.current_selected_id = new_index.indexes()[0].data() 
            
            widget.setText(new_index.indexes()[1].data()) # We want fill the given widget with record.

        except Exception as er: #if qModelIndex
            pass

        return

    def generator_widget_object(self, list_widget_object):

        for widget in list_widget_object:
            yield widget

    def stack_set_current_index(self, 
                                index=None, 
                                new_index=None,
                                stack_widget_object=None, 
                                tree_view_object=None,
                                column_index=None,
                                set_text=None, 
                                lineEdit_object=None,
                                set_focus=None,
                                clear_widget=True,
                                load_data=True,
                                current_obj_clear_selection=False,
                                delete_selected_id = False,
                                current_obj_clear=False):

        '''
            NOTICE:
            =======
            This method is used ti change the view of stacked widget. 

            PARAMETERS:
            ===========
            :index                          -   The given index changes the view of stacked widget

            :new_index                      -   The new on of the given index changes the view of stacked widget

            :stack_widget_object            -   We need a QStackedWidget object

            :tree_view_object               -   We also need a QTreeView object

            :column_index                   -   For QTreeView object we expect a index for column 

            :set_text                       -   We get a text - context of QLineEdit object

            :lineEdit_object                -   We need a QLineEdit to set the text there

            :set_focus                      -   When we should set the focus in given QLineEdit object, we set to True, otherwise to False

            :clear_widget                   -   When we should clear the given QLineEdit object, we set to True, otherwise to False

            :load_data                      -   When we want the program to load records again, one the page of stacked widget
                                                has been changed we set to True, otherwise to False 

            :current_obj_clear_selection    -   Its True, we clear the QTreeView selection

            :delete_selected_id             -   Its True we delete the saved IDs 

            :current_obj_clear              -   Its True we just clear all widget objects in current pagp, except QTreeView.

            :return                         -   Nothing is returned. The statement 'return'
                                                terminates a function. That makes sure that the
                                                function is definitely finished.
        '''
        logger.info("change the widget of QStackedWidget")

        self.quite_threads()

        if not isinstance(new_index, bool):

            if not new_index is None:

                index = new_index.indexes()[1].data()#.toPyObject()

        self.stack_widget_page = int(index)

        '''
            When the user changes the page of QStackedWidget all QLineEdit()-objects,
            which are children of current widget, are deleted or cleared. That means,
            when the user leaves the current page and goes to the other new page, 
            all QLineEdit()-object, which are childrem from old pages, should be 
            deleted or cleared.

            First, we check, if the variables named (self._new_stack_page) is not None.
        '''
        if not self._new_stack_page is None:
            '''
                Yes, its not None. The variable isn't empty.
                Great. Ntext step, we save the current index in the variable named 
                (self._new_stack_page).
            '''

            self._new_stack_page = index

            '''
                After the new index has been saved, first we create a list of all QLineEdit()-objects,
                which are children of the old stack page, that the user was left. In this case we
                need to use the old page number, which is saved in the variable named (self._old_stack_page).
                The created list we sent it to the method called self.clear_widget_object_content().
            '''
            if clear_widget:

                self.clear_widget_object_content(
                    list_widget_object = self.ui_master_data.stackedWidget.widget(int(self._old_stack_page)).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

                # TODO: Think about what to do with selected ID

                #self.selected_be_edited_id = None

            '''
                After all QLineEdit()-objects has beend deleted or cleared we set the old page with the new page. 
                That means, we overwrite the old page number with the given new page number. We create a copy of them.
            '''
            self._old_stack_page = self._new_stack_page

        else:

            '''
                Maybe the user starts this window at first time. Now we have to set the new and old page at the
                same time.
            '''
            self._new_stack_page = index
            self._old_stack_page = index

            if clear_widget:

                self.clear_widget_object_content(
                    list_widget_object=self.ui_master_data.stackedWidget.widget(self._old_stack_page).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

        if not lineEdit_object is None:
            self.set_text(widget = lineEdit_object, text = set_text)

        if set_focus:
            self.set_focus(widget_object = lineEdit_object)

        if load_data:
            self.manage_master_data_page(index)


        stack_widget_object.setCurrentIndex(int(index))

        if current_obj_clear_selection:

            # self.clear_widget_object_content(
            #     list_widget_object=

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QTreeView))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clearSelection()

        if current_obj_clear:

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QLineEdit, QTextEdit, QPlainTextEdit))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clear()

        if delete_selected_id:
            self.remove_current_selected_id()
            self.remove_selected_be_edited_id()


        return

    def manage_master_data_page(self, stack_page_id):

        self.remove_current_selected_id()
        self.remove_selected_be_edited_id()

        print "manage_master_data_page, stack_page_id", stack_page_id

        dict_stack_page={

                        01: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_film_genre),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_FILM_GENRE,
                                                     model = self._standard_item_model.standard_item_model_film_genre,
                                                     widget_label = self.ui_master_data.label_film_genre_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_film_genre,
                                                     category = 'film_genre')),

                        17: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_music_genre),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_MUSIC_GENRE,
                                                     model = self._standard_item_model.standard_item_model_music_genre,
                                                     widget_label = self.ui_master_data.label_music_genre_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_music_genre,
                                                     category = 'music_genre')),

                        29: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_gender),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_GENDER,
                                                     model = self._standard_item_model.standard_item_model_person_gender,
                                                     widget_label = self.ui_master_data.label_person_gender_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_gender,
                                                     category = 'person_gender')),

                        30: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_salutation),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_SALUTATION,
                                                     model = self._standard_item_model.standard_item_model_person_salutation,
                                                     widget_label = self.ui_master_data.label_person_salutation_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                                                     category = 'person_salutation')),

                        31: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_title),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_TITLE,
                                                     model = self._standard_item_model.standard_item_model_person_title,
                                                     widget_label = self.ui_master_data.label_person_title_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_title,
                                                     category = 'person_title')),

                        32: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_nationality),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_NATIONALITY,
                                                     model = self._standard_item_model.standard_item_model_person_nationality,
                                                     widget_label = self.ui_master_data.label_person_nationality_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                                                     category = 'person_nationality')),

                        33: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_eye_color),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_EYE_COLOR,
                                                     model = self._standard_item_model.standard_item_model_person_eye_color,
                                                     widget_label = self.ui_master_data.label_person_eye_color_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                                                     category = 'person_eye_color')),

                        34: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_hair_color),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_HAIR_COLOR,
                                                     model = self._standard_item_model.standard_item_model_person_hair_color,
                                                     widget_label = self.ui_master_data.label_person_hair_color_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                                                     category = 'person_hair_color')),

                        35: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_religion),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_RELIGION,
                                                     model = self._standard_item_model.standard_item_model_person_religion,
                                                     widget_label = self.ui_master_data.label_person_religion_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_religion,
                                                     category = 'person_religion')),

                        36: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_relationship_status),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP_STATUS,
                                                     model = self._standard_item_model.standard_item_model_person_relationship_status,
                                                     widget_label = self.ui_master_data.label_person_relationship_status_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                                                     category = 'person_relationship_status')),

                        37: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_relationship),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP,
                                                     model = self._standard_item_model.standard_item_model_person_relationship,
                                                     widget_label = self.ui_master_data.label_person_relationship_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                                                     category = 'person_relationship')),

                        45: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_packaging),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_PACKAGING,
                                                     model = self._standard_item_model.standard_item_model_general_packaging,
                                                     widget_label = self.ui_master_data.label_general_packaging_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                                                     category = 'general_packaging')),

                        47: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_media_type),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_MEDIA_TYPE,
                                                     model = self._standard_item_model.standard_item_model_general_media_type,
                                                     widget_label = self.ui_master_data.label_general_media_type_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                                                     category = 'general_media_type')),

                        48: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_language),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_LANGUAGE,
                                                     model = self._standard_item_model.standard_item_model_general_language,
                                                     widget_label = self.ui_master_data.label_general_language_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_language,
                                                     category = 'general_language')),

                        49: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_award),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_AWARD,
                                                     model = self._standard_item_model.standard_item_model_general_award,
                                                     widget_label = self.ui_master_data.label_general_award_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_award,
                                                     category = 'general_award')),

                        50: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_country),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header =self.LIST_HORIZONTAL_HEADER_GENERAL_REGION,
                                                     model = self._standard_item_model.standard_item_model_general_country,
                                                     widget_label = self.ui_master_data.label_general_country_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_country,
                                                     category = 'general_country')),

                        51: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_region),
                              lambda: self.fetch_all(page_index = stack_page_id,
                                                     list_header =self.LIST_HORIZONTAL_HEADER_GENERAL_REGION,
                                                     model = self._standard_item_model.standard_item_model_general_region,
                                                     widget_label = self.ui_master_data.label_general_region_counter,
                                                     widget_tree_view = self.ui_master_data.tree_view_general_region,
                                                     category = 'general_region'))}


        try:
            function_first, function_second = dict_stack_page[int(stack_page_id)]
            function_first()
            function_second()

        except KeyError as key_err:
            print "key_err", key_err

        return

    def handle_timer(self, 
                     timer_widget = None,
                     text = None,
                     show_wait_message = False):

        if show_wait_message:
            self.create_wait_window(text = text)

        timer_widget.stop()
        timer_widget.start()

        return

    def filter_update(self,
                      line_edit_widget = None,
                      proxy_model = None):

        '''
            NOTICE:
            =======
            This method updates the filter of the given model.

            PARAMETERS:
            ===========
            :string_text    -   we except a text für updating the filter.

            :proxy_model    -   We need the model where we should update the filter.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        ''' 

        #self.create_wait_window()

        if line_edit_widget is None:
            line_edit_widget = QLineEdit()

        syntax = QRegExp.RegExp
        caseSensitivity = Qt.CaseInsensitive
        regExp = QRegExp(line_edit_widget.text(), caseSensitivity, syntax)
        proxy_model.setFilterRegExp(regExp)

        if proxy_model.rowCount() == 0:

            syntax = QRegExp.RegExp
            caseSensitivity = Qt.CaseInsensitive
            regExp = QRegExp(QString(""), caseSensitivity, syntax)
            proxy_model.setFilterRegExp(regExp)

            self.close_wait_message()

            self.create_generally_information_msg(title = self.tr("No matching results"),
                                                  msg = "No results were found for this search",
                                                  detail_msg = "",
                                                  yes_no_button = False,
                                                  only_yes_button = True,
                                                  line_edit_object_get_focus = line_edit_widget,
                                                  icon = self.image_path_collection.info_32x32_default)

        else:

            self.close_wait_message()

            # No results match for your search.
            #

        return

    def manage_methods(self, 
                       method_name = None):

        print "manage_methods is vcalled, method_name", method_name

        dict_methods = {
                        "fetch_all_general_region": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_REGION,
                                                                           work_mode = 'fetch_all',
                                                                           model = self._standard_item_model.standard_item_model_general_region,
                                                                           widget_label = self.ui_master_data.label_general_region_counter,
                                                                           widget_tree_view = self.ui_master_data.tree_view_general_region,
                                                                           category = 'general_region'),

                        "add_general_region": lambda: self.add_record(line_edit_obj_general_region = self.ui_master_data.lineEdit_general_region,
                                                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_general_region,
                                                                      model = self._standard_item_model.standard_item_model_general_region,
                                                                      label_object = self.ui_master_data.label_general_region_counter,
                                                                      work_mode = "add_record",
                                                                      category = 'general_region'),

                        "edit_general_region": lambda: self.add_record(line_edit_obj_general_region = self.ui_master_data.lineEdit_edit_general_region,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_general_region,
                                                                       model = self._standard_item_model.standard_item_model_general_region,
                                                                       stack_widget = self.ui_master_data.stackedWidget_general_region,
                                                                       label_object = self.ui_master_data.label_general_region_counter,
                                                                       work_mode = "edit_record",
                                                                       category = 'general_region'),

                        "delete_general_region": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                            tree_view_object = self.ui_master_data.tree_view_general_region,
                                                                            line_edit_obj = self.ui_master_data.lineEdit_general_region,
                                                                            label_object = self.ui_master_data.label_general_region_counter,
                                                                            model = self._standard_item_model.standard_item_model_general_region,
                                                                            category = 'general_region'),

                        "update_general_region": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                        bulk_the_list = True,
                                                                        set_page = True,
                                                                        widget_stack_widget = self.ui_master_data.stackedWidget_general_region,
                                                                        page_index = 0,
                                                                        widget_line_edit = self.ui_master_data.lineEdit_general_region,
                                                                        widget_label = self.ui_master_data.label_general_region_counter,
                                                                        widget_tree_view = self.ui_master_data.tree_view_general_region,
                                                                        notify_on_widget = self.ui_master_data.textEdit_notify_general_region,
                                                                        model = self._standard_item_model.standard_item_model_general_region,
                                                                        sort_column = 1,
                                                                        category = 'general_region'),

                        "fetch_all_general_country": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_COUNTRY,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_general_country,
                                                                            widget_label = self.ui_master_data.label_general_country_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_general_country,
                                                                            category = 'general_country'),
                        
                        "add_general_country": lambda: self.add_record(line_edit_obj_general_country = self.ui_master_data.lineEdit_general_country,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                                                                       model = self._standard_item_model.standard_item_model_general_country,
                                                                       label_object = self.ui_master_data.label_general_country_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'general_country'),

                        "edit_general_country": lambda: self.add_record(line_edit_obj_general_country = self.ui_master_data.lineEdit_edit_general_country,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                                                                        model = self._standard_item_model.standard_item_model_general_country,
                                                                        stack_widget = self.ui_master_data.stackedWidget_general_country,
                                                                        label_object = self.ui_master_data.label_general_country_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'general_country'),

                        "delete_general_country": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_general_country,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_general_country,
                                                                             label_object = self.ui_master_data.label_general_country_counter,
                                                                             model = self._standard_item_model.standard_item_model_general_country,
                                                                             category = 'general_country'),

                        "update_general_country": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                         bulk_the_list = True,
                                                                         set_page = True,
                                                                         widget_stack_widget = self.ui_master_data.stackedWidget_general_country,
                                                                         page_index = 0,
                                                                         widget_line_edit = self.ui_master_data.lineEdit_general_country,
                                                                         widget_label = self.ui_master_data.label_general_country_counter,
                                                                         widget_tree_view = self.ui_master_data.tree_view_general_country,
                                                                         notify_on_widget = self.ui_master_data.textEdit_notify_general_country,
                                                                         model = self._standard_item_model.standard_item_model_general_country,
                                                                         sort_column = 1,
                                                                         category = 'general_country'),

                        "fetch_all_general_language": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_LANGUAGE,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_general_language,
                                                                            widget_label = self.ui_master_data.label_general_language_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_general_language,
                                                                            category = 'general_language'),

                        "add_general_language": lambda: self.add_record(line_edit_obj_general_language = self.ui_master_data.lineEdit_general_language,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                                                                       model = self._standard_item_model.standard_item_model_general_language,
                                                                       label_object = self.ui_master_data.label_general_language_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'general_language'),

                        "edit_general_language": lambda: self.add_record(line_edit_obj_general_language = self.ui_master_data.lineEdit_edit_general_language,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                                                                        model = self._standard_item_model.standard_item_model_general_language,
                                                                        stack_widget = self.ui_master_data.stackedWidget_general_language,
                                                                        label_object = self.ui_master_data.label_general_language_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'general_language'),

                        "delete_general_language": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_general_language,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_general_language,
                                                                             label_object = self.ui_master_data.label_general_language_counter,
                                                                             model = self._standard_item_model.standard_item_model_general_language,
                                                                             category = 'general_language'),

                        "update_general_language": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                         bulk_the_list = True,
                                                                         set_page = True,
                                                                         widget_stack_widget = self.ui_master_data.stackedWidget_general_language,
                                                                         page_index = 0,
                                                                         widget_line_edit = self.ui_master_data.lineEdit_general_language,
                                                                         widget_label = self.ui_master_data.label_general_language_counter,
                                                                         widget_tree_view = self.ui_master_data.tree_view_general_language,
                                                                         notify_on_widget = self.ui_master_data.textEdit_notify_general_language,
                                                                         model = self._standard_item_model.standard_item_model_general_language,
                                                                         sort_column = 1,
                                                                         category = 'general_language'),

                        "fetch_all_person_nationality": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_NATIONALITY,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_nationality,
                                                                            widget_label = self.ui_master_data.label_person_nationality_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                                                                            category = 'person_nationality'),

                        "add_person_nationality": lambda: self.add_record(line_edit_obj_person_nationality = self.ui_master_data.lineEdit_person_nationality,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                                                                       model = self._standard_item_model.standard_item_model_person_nationality,
                                                                       label_object = self.ui_master_data.label_person_nationality_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_nationality'),

                        "edit_person_nationality": lambda: self.add_record(line_edit_obj_person_nationality = self.ui_master_data.lineEdit_edit_person_nationality,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                                                                        model = self._standard_item_model.standard_item_model_person_nationality,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                                                                        label_object = self.ui_master_data.label_person_nationality_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_nationality'),

                        "delete_person_nationality": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_nationality,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_nationality,
                                                                             label_object = self.ui_master_data.label_person_nationality_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_nationality,
                                                                             category = 'person_nationality'),

                        "update_person_nationality": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                            bulk_the_list = True,
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_person_nationality,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_person_nationality,
                                                                            widget_label = self.ui_master_data.label_person_nationality_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_nationality,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_person_nationality,
                                                                            model = self._standard_item_model.standard_item_model_person_nationality,
                                                                            sort_column = 1,
                                                                            category = 'person_nationality'),

                        "fetch_all_film_genre": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_FILM_GENRE,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_film_genre,
                                                                            widget_label = self.ui_master_data.label_film_genre_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_film_genre,
                                                                            category = 'film_genre'),

                        "add_film_genre": lambda: self.add_record(line_edit_obj_film_genre = self.ui_master_data.lineEdit_film_genre,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_film_genre,
                                                                       model = self._standard_item_model.standard_item_model_film_genre,
                                                                       label_object = self.ui_master_data.label_film_genre_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'film_genre'),

                        "edit_film_genre": lambda: self.add_record(line_edit_obj_film_genre = self.ui_master_data.lineEdit_edit_film_genre,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_film_genre,
                                                                        model = self._standard_item_model.standard_item_model_film_genre,
                                                                        stack_widget = self.ui_master_data.stackedWidget_film_genre,
                                                                        label_object = self.ui_master_data.label_film_genre_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'film_genre'),

                        "delete_film_genre": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_film_genre,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_film_genre,
                                                                             label_object = self.ui_master_data.label_film_genre_counter,
                                                                             model = self._standard_item_model.standard_item_model_film_genre,
                                                                             category = 'film_genre'),

                        "update_film_genre": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                    bulk_the_list = True,
                                                                    set_page = True,
                                                                    widget_stack_widget = self.ui_master_data.stackedWidget_film_genre,
                                                                    page_index = 0,
                                                                    widget_line_edit = self.ui_master_data.lineEdit_film_genre,
                                                                    widget_label = self.ui_master_data.label_film_genre_counter,
                                                                    widget_tree_view = self.ui_master_data.tree_view_film_genre,
                                                                    notify_on_widget = self.ui_master_data.textEdit_notify_film_genre,
                                                                    model = self._standard_item_model.standard_item_model_film_genre,
                                                                    sort_column = 1,
                                                                    category = 'film_genre'),

                        "fetch_all_general_award": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_GENERAL_AWARD,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_general_award,
                                                                            widget_label = self.ui_master_data.label_general_award_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_general_award,
                                                                            category = 'general_award'),

                        "add_general_award": lambda: self.add_record(line_edit_obj_general_award = self.ui_master_data.lineEdit_general_award,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_general_award,
                                                                       model = self._standard_item_model.standard_item_model_general_award,
                                                                       label_object = self.ui_master_data.label_general_award_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'general_award'),

                        "edit_general_award": lambda: self.add_record(line_edit_obj_general_award = self.ui_master_data.lineEdit_edit_general_award,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_general_award,
                                                                        model = self._standard_item_model.standard_item_model_general_award,
                                                                        stack_widget = self.ui_master_data.stackedWidget_general_award,
                                                                        label_object = self.ui_master_data.label_general_award_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'general_award'),

                        "delete_general_award": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_general_award,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_general_award,
                                                                             label_object = self.ui_master_data.label_general_award_counter,
                                                                             model = self._standard_item_model.standard_item_model_general_award,
                                                                             category = 'general_award'),

                        "update_general_award": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                       bulk_the_list = True,
                                                                       set_page = True,
                                                                       widget_stack_widget = self.ui_master_data.stackedWidget_general_award,
                                                                       page_index = 0,
                                                                       widget_line_edit = self.ui_master_data.lineEdit_general_award,
                                                                       widget_label = self.ui_master_data.label_general_award_counter,
                                                                       widget_tree_view = self.ui_master_data.tree_view_general_award,
                                                                       notify_on_widget = self.ui_master_data.textEdit_notify_general_award,
                                                                       model = self._standard_item_model.standard_item_model_general_award,
                                                                       sort_column = 1,
                                                                       category = 'general_award'),

                        "fetch_all_music_genre": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_MUSIC_GENRE,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_music_genre,
                                                                            widget_label = self.ui_master_data.label_music_genre_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_music_genre,
                                                                            category = 'music_genre'),

                        "add_music_genre": lambda: self.add_record(line_edit_obj_music_genre = self.ui_master_data.lineEdit_music_genre,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_music_genre,
                                                                       model = self._standard_item_model.standard_item_model_music_genre,
                                                                       label_object = self.ui_master_data.label_music_genre_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'music_genre'),

                        "edit_music_genre": lambda: self.add_record(line_edit_obj_music_genre = self.ui_master_data.lineEdit_edit_music_genre,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_music_genre,
                                                                        model = self._standard_item_model.standard_item_model_music_genre,
                                                                        stack_widget = self.ui_master_data.stackedWidget_music_genre,
                                                                        label_object = self.ui_master_data.label_music_genre_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'music_genre'),

                        "delete_music_genre": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_music_genre,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_music_genre,
                                                                             label_object = self.ui_master_data.label_music_genre_counter,
                                                                             model = self._standard_item_model.standard_item_model_music_genre,
                                                                             category = 'music_genre'),

                        "update_music_genre": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                     bulk_the_list = True,
                                                                     set_page = True,
                                                                     widget_stack_widget = self.ui_master_data.stackedWidget_music_genre,
                                                                     page_index = 0,
                                                                     widget_line_edit = self.ui_master_data.lineEdit_music_genre,
                                                                     widget_label = self.ui_master_data.label_music_genre_counter,
                                                                     widget_tree_view = self.ui_master_data.tree_view_music_genre,
                                                                     notify_on_widget = self.ui_master_data.textEdit_notify_music_genre,
                                                                     model = self._standard_item_model.standard_item_model_music_genre,
                                                                     sort_column = 1,
                                                                     category = 'music_genre'),

                        "fetch_all_person_gender": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_GENDER,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_gender,
                                                                            widget_label = self.ui_master_data.label_person_gender_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_gender,
                                                                            category = 'person_gender'),

                        "add_person_gender": lambda: self.add_record(line_edit_obj_person_gender = self.ui_master_data.lineEdit_person_gender,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_gender,
                                                                       model = self._standard_item_model.standard_item_model_person_gender,
                                                                       label_object = self.ui_master_data.label_person_gender_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_gender'),

                        "edit_person_gender": lambda: self.add_record(line_edit_obj_person_gender = self.ui_master_data.lineEdit_edit_person_gender,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_gender,
                                                                        model = self._standard_item_model.standard_item_model_person_gender,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_gender,
                                                                        label_object = self.ui_master_data.label_person_gender_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_gender'),

                        "delete_person_gender": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_gender,
                                                                             label_object = self.ui_master_data.label_person_gender_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_gender,
                                                                             category = 'person_gender'),

                        # "update_person_gender": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  bulk_the_list = True,    
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_gender,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_gender,
                        #                                                  widget_label = self.ui_master_data.label_person_gender_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_gender,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_gender,
                        #                                                  model = self._standard_item_model.standard_item_model_person_gender,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_gender')

                        "fetch_all_person_salutation": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_SALUTATION,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_salutation,
                                                                            widget_label = self.ui_master_data.label_person_salutation_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                                                                            category = 'person_salutation'),

                        "add_person_salutation": lambda: self.add_record(line_edit_obj_person_salutation = self.ui_master_data.lineEdit_person_salutation,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                                                                       model = self._standard_item_model.standard_item_model_person_salutation,
                                                                       label_object = self.ui_master_data.label_person_salutation_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_salutation'),

                        "edit_person_salutation": lambda: self.add_record(line_edit_obj_person_salutation = self.ui_master_data.lineEdit_edit_person_salutation,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                                                                        model = self._standard_item_model.standard_item_model_person_salutation,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_salutation,
                                                                        label_object = self.ui_master_data.label_person_salutation_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_salutation'),

                        "delete_person_salutation": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_salutation,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_salutation,
                                                                             label_object = self.ui_master_data.label_person_salutation_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_salutation,
                                                                             category = 'person_salutation'),

                        # "update_person_salutation": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_salutation,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_salutation,
                        #                                                  widget_label = self.ui_master_data.label_person_salutation_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_salutation,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_salutation,
                        #                                                  model = self._standard_item_model.standard_item_model_person_salutation,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_salutation')

                        "fetch_all_person_title": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_TITLE,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_title,
                                                                            widget_label = self.ui_master_data.label_person_title_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_title,
                                                                            category = 'person_title'),

                        "add_person_title": lambda: self.add_record(line_edit_obj_person_title = self.ui_master_data.lineEdit_person_title,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                                                                       model = self._standard_item_model.standard_item_model_person_title,
                                                                       label_object = self.ui_master_data.label_person_title_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_title'),

                        "edit_person_title": lambda: self.add_record(line_edit_obj_person_title = self.ui_master_data.lineEdit_edit_person_title,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                                                                        model = self._standard_item_model.standard_item_model_person_title,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_title,
                                                                        label_object = self.ui_master_data.label_person_title_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_title'),

                        "delete_person_title": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_title,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_title,
                                                                             label_object = self.ui_master_data.label_person_title_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_title,
                                                                             category = 'person_title'),

                        # "update_person_title": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_title,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_title,
                        #                                                  widget_label = self.ui_master_data.label_person_title_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_title,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_title,
                        #                                                  model = self._standard_item_model.standard_item_model_person_title,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_salutation')

                        "fetch_all_person_eye_color": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_EYE_COLOR,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_eye_color,
                                                                            widget_label = self.ui_master_data.label_person_eye_color_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                                                                            category = 'person_eye_color'),

                        "add_person_eye_color": lambda: self.add_record(line_edit_obj_person_eye_color = self.ui_master_data.lineEdit_person_eye_color,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                                                                       model = self._standard_item_model.standard_item_model_person_eye_color,
                                                                       label_object = self.ui_master_data.label_person_eye_color_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_eye_color'),

                        "edit_person_eye_color": lambda: self.add_record(line_edit_obj_person_eye_color = self.ui_master_data.lineEdit_edit_person_eye_color,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                                                                        model = self._standard_item_model.standard_item_model_person_eye_color,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_eye_color,
                                                                        label_object = self.ui_master_data.label_person_eye_color_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_eye_color'),

                        "delete_person_eye_color": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_eye_color,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_eye_color,
                                                                             label_object = self.ui_master_data.label_person_eye_color_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_eye_color,
                                                                             category = 'person_eye_color'),

                        # "update_person_eye_color": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_eye_color,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_eye_color,
                        #                                                  widget_label = self.ui_master_data.label_person_eye_color_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_eye_color,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_eye_color,
                        #                                                  model = self._standard_item_model.standard_item_model_person_eye_color,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_eye_color')
    
                        "fetch_all_person_hair_color": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_HAIR_COLOR,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_hair_color,
                                                                            widget_label = self.ui_master_data.label_person_hair_color_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                                                                            category = 'person_hair_color'),

                        "add_person_hair_color": lambda: self.add_record(line_edit_obj_person_hair_color = self.ui_master_data.lineEdit_person_hair_color,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                                                                       model = self._standard_item_model.standard_item_model_person_hair_color,
                                                                       label_object = self.ui_master_data.label_person_hair_color_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_hair_color'),

                        "edit_person_hair_color": lambda: self.add_record(line_edit_obj_person_hair_color = self.ui_master_data.lineEdit_edit_person_hair_color,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                                                                        model = self._standard_item_model.standard_item_model_person_hair_color,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_hair_color,
                                                                        label_object = self.ui_master_data.label_person_hair_color_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_hair_color'),

                        "delete_person_hair_color": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_hair_color,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_hair_color,
                                                                             label_object = self.ui_master_data.label_person_hair_color_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_hair_color,
                                                                             category = 'person_hair_color'),

                        # "update_person_hair_color": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_hair_color,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_hair_color,
                        #                                                  widget_label = self.ui_master_data.label_person_hair_color_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_hair_color,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_hair_color,
                        #                                                  model = self._standard_item_model.standard_item_model_person_hair_color,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_hair_color')

                        "fetch_all_person_religion": lambda: self.fetch_all(list_header = self. LIST_HORIZONTAL_HEADER_PERSON_RELIGION,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_religion,
                                                                            widget_label = self.ui_master_data.label_person_religion_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_religion,
                                                                            category = 'person_religion'),

                        "add_person_religion": lambda: self.add_record(line_edit_obj_person_religion = self.ui_master_data.lineEdit_person_religion,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                                                                       model = self._standard_item_model.standard_item_model_person_religion,
                                                                       label_object = self.ui_master_data.label_person_religion_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_religion'),

                        "edit_person_religion": lambda: self.add_record(line_edit_obj_person_religion = self.ui_master_data.lineEdit_edit_person_religion,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                                                                        model = self._standard_item_model.standard_item_model_person_religion,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_religion,
                                                                        label_object = self.ui_master_data.label_person_religion_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_religion'),

                        "delete_person_religion": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_religion,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_religion,
                                                                             label_object = self.ui_master_data.label_person_religion_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_religion,
                                                                             category = 'person_religion'),

                        # "update_person_religion": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_religion,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_religion,
                        #                                                  widget_label = self.ui_master_data.label_person_religion_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_religion,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_religion,
                        #                                                  model = self._standard_item_model.standard_item_model_person_religion,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_religion')

                        "fetch_all_person_relationship_status": lambda: self.fetch_all(list_header = self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP_STATUS,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_relationship_status,
                                                                            widget_label = self.ui_master_data.label_person_relationship_status_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                                                                            category = 'person_relationship_status'),

                        "add_person_relationship_status": lambda: self.add_record(line_edit_obj_person_relationship_status = self.ui_master_data.lineEdit_person_relationship_status,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                                                                       model = self._standard_item_model.standard_item_model_person_relationship_status,
                                                                       label_object = self.ui_master_data.label_person_relationship_status_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_relationship_status'),

                        "edit_person_relationship_status": lambda: self.add_record(line_edit_obj_person_relationship_status = self.ui_master_data.lineEdit_edit_person_relationship_status,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                                                                        model = self._standard_item_model.standard_item_model_person_relationship_status,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_relationship_status,
                                                                        label_object = self.ui_master_data.label_person_relationship_status_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_relationship_status'),

                        "delete_person_relationship_status": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_relationship_status,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_relationship_status,
                                                                             label_object = self.ui_master_data.label_person_relationship_status_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_relationship_status,
                                                                             category = 'person_relationship_status'),

                        # "update_person_relationship_status": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_relationship_status,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_relationship_status,
                        #                                                  widget_label = self.ui_master_data.label_person_relationship_status_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_relationship_status,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_relationship_status,
                        #                                                  model = self._standard_item_model.standard_item_model_person_relationship_status,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_relationship_status')

                        "fetch_all_person_relationship": lambda: self.fetch_all(list_header = self.LIST_HORIZONTAL_HEADER_PERSON_RELATIONSHIP,
                                                                            work_mode = 'fetch_all',
                                                                            model = self._standard_item_model.standard_item_model_person_relationship,
                                                                            widget_label = self.ui_master_data.label_person_relationship_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                                                                            category = 'person_relationship'),

                        "add_person_relationship": lambda: self.add_record(line_edit_obj_person_relationship = self.ui_master_data.lineEdit_person_relationship,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                                                                       model = self._standard_item_model.standard_item_model_person_relationship,
                                                                       label_object = self.ui_master_data.label_person_relationship_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'person_relationship'),

                        "edit_person_relationship": lambda: self.add_record(line_edit_obj_person_relationship = self.ui_master_data.lineEdit_edit_person_relationship,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                                                                        model = self._standard_item_model.standard_item_model_person_relationship,
                                                                        stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                                                                        label_object = self.ui_master_data.label_person_relationship_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'person_relationship'),

                        "delete_person_relationship": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_person_relationship,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_person_relationship,
                                                                             label_object = self.ui_master_data.label_person_relationship_counter,
                                                                             model = self._standard_item_model.standard_item_model_person_relationship,
                                                                             category = 'person_relationship'),

                        # "update_person_relationship": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                        #                                                  set_page = True,
                        #                                                  widget_stack_widget = self.ui_master_data.stackedWidget_person_relationship,
                        #                                                  page_index = 0,
                        #                                                  widget_line_edit = self.ui_master_data.lineEdit_person_relationship,
                        #                                                  widget_label = self.ui_master_data.label_person_relationship_counter,
                        #                                                  widget_tree_view = self.ui_master_data.tree_view_person_relationship,
                        #                                                  notify_on_widget = self.ui_master_data.textEdit_notify_person_relationship,
                        #                                                  model = self._standard_item_model.standard_item_model_person_relationship,
                        #                                                  sort_column = 1,
                        #                                                  category = 'person_relationship')

                        "fetch_all_general_media_type": lambda: self.fetch_all(list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_MEDIA_TYPE,
                                                                               work_mode = 'fetch_all',
                                                                               model = self._standard_item_model.standard_item_model_general_media_type,
                                                                               widget_label = self.ui_master_data.label_general_media_type_counter,
                                                                               widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                                                                               category = 'general_media_type'),

                        "add_general_media_type": lambda: self.add_record(line_edit_obj_general_media_type = self.ui_master_data.lineEdit_general_media_type,
                                                                         line_edit_object_get_focus = self.ui_master_data.lineEdit_general_media_type,
                                                                         model = self._standard_item_model.standard_item_model_general_media_type,
                                                                         label_object = self.ui_master_data.label_general_media_type_counter,
                                                                         work_mode = "add_record",
                                                                         category = 'general_media_type'),

                        "edit_general_media_type": lambda: self.add_record(line_edit_obj_general_media_type = self.ui_master_data.lineEdit_edit_general_media_type,
                                                                          line_edit_object_get_focus = self.ui_master_data.lineEdit_general_media_type,
                                                                          model = self._standard_item_model.standard_item_model_general_media_type,
                                                                          stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                                                                          label_object = self.ui_master_data.label_general_media_type_counter,
                                                                          work_mode = "edit_record",
                                                                          category = 'general_media_type'),

                        "delete_general_media_type": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_general_media_type,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_general_media_type,
                                                                             label_object = self.ui_master_data.label_general_media_type_counter,
                                                                             model = self._standard_item_model.standard_item_model_general_media_type,
                                                                             category = 'general_media_type'),

                        "update_general_media_type": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                            bulk_the_list = True,    
                                                                            set_page = True,
                                                                            widget_stack_widget = self.ui_master_data.stackedWidget_general_media_type,
                                                                            page_index = 0,
                                                                            widget_line_edit = self.ui_master_data.lineEdit_general_media_type,
                                                                            widget_label = self.ui_master_data.label_general_media_type_counter,
                                                                            widget_tree_view = self.ui_master_data.tree_view_general_media_type,
                                                                            notify_on_widget = self.ui_master_data.textEdit_notify_general_media_type,
                                                                            model = self._standard_item_model.standard_item_model_general_media_type,
                                                                            sort_column = 1,
                                                                            category = 'general_media_type'),

                        "fetch_all_general_packaging": lambda: self.fetch_all(list_header = self.LIST_HORIZONTAL_HEADER_GENERAL_PACKAGING,
                                                                              work_mode = 'fetch_all',
                                                                              model = self._standard_item_model.standard_item_model_general_packaging,
                                                                              widget_label = self.ui_master_data.label_general_packaging_counter,
                                                                              widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                                                                              category = 'general_packaging'),

                        "add_general_packaging": lambda: self.add_record(line_edit_obj_general_packaging = self.ui_master_data.lineEdit_general_packaging,
                                                                       line_edit_object_get_focus = self.ui_master_data.lineEdit_general_packaging,
                                                                       model = self._standard_item_model.standard_item_model_general_packaging,
                                                                       label_object = self.ui_master_data.label_general_packaging_counter,
                                                                       work_mode = "add_record",
                                                                       category = 'general_packaging'),

                        "edit_general_packaging": lambda: self.add_record(line_edit_obj_general_packaging = self.ui_master_data.lineEdit_edit_general_packaging,
                                                                        line_edit_object_get_focus = self.ui_master_data.lineEdit_general_packaging,
                                                                        model = self._standard_item_model.standard_item_model_general_packaging,
                                                                        stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                                                                        label_object = self.ui_master_data.label_general_packaging_counter,
                                                                        work_mode = "edit_record",
                                                                        category = 'general_packaging'),

                        "delete_general_packaging": lambda: self.delete_record(id = self.selected_be_edited_id,
                                                                             tree_view_object = self.ui_master_data.tree_view_general_packaging,
                                                                             line_edit_obj = self.ui_master_data.lineEdit_general_packaging,
                                                                             label_object = self.ui_master_data.label_general_packaging_counter,
                                                                             model = self._standard_item_model.standard_item_model_general_packaging,
                                                                             category = 'general_packaging'),

                        "update_general_packaging": lambda: self.fetch_all(work_mode = 'fetch_online_data',
                                                                           bulk_the_list = True,
                                                                           set_page = True,
                                                                           widget_stack_widget = self.ui_master_data.stackedWidget_general_packaging,
                                                                           page_index = 0,
                                                                           widget_line_edit = self.ui_master_data.lineEdit_general_packaging,
                                                                           widget_label = self.ui_master_data.label_general_packaging_counter,
                                                                           widget_tree_view = self.ui_master_data.tree_view_general_packaging,
                                                                           notify_on_widget = self.ui_master_data.textEdit_notify_general_packaging,
                                                                           model = self._standard_item_model.standard_item_model_general_packaging,
                                                                           sort_column = 1,
                                                                           category = 'general_packaging')

                        }

        try:
            function_first = dict_methods[method_name]
            function_first()

        except KeyError as key_err:
            pass#print "key_err", key_err

        return
        pass

        return
        

    def manage_pages(self,
                    page_id = None):
        '''
            NOTICE:
            =======
            This method manages all pages.

            PARAMETERS:
            ===========
            :page_id    -   The given ID cahnges the page.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        dict_pages = {
                        01: lambda: self.manage_methods(method_name = "fetch_all_film_genre"),
                        17: lambda: self.manage_methods(method_name = "fetch_all_music_genre"),
                        29: lambda: self.manage_methods(method_name = "fetch_all_person_gender"),
                        30: lambda: self.manage_methods(method_name = "fetch_all_person_salutation"),
                        31: lambda: self.manage_methods(method_name = "fetch_all_person_title"),
                        32: lambda: self.manage_methods(method_name = "fetch_all_person_nationality"),
                        33: lambda: self.manage_methods(method_name = "fetch_all_person_eye_color"),
                        34: lambda: self.manage_methods(method_name = "fetch_all_person_hair_color"),
                        35: lambda: self.manage_methods(method_name = "fetch_all_person_religion"),
                        36: lambda: self.manage_methods(method_name = "fetch_all_person_relationship_status"),
                        37: lambda: self.manage_methods(method_name = "fetch_all_person_relationship"),
                        45: lambda: self.manage_methods(method_name = "fetch_all_general_packaging"),
                        47: lambda: self.manage_methods(method_name = "fetch_all_general_media_type"),
                        48: lambda: self.manage_methods(method_name = "fetch_all_general_language"),
                        49: lambda: self.manage_methods(method_name = "fetch_all_general_award"),
                        50: lambda: self.manage_methods(method_name = "fetch_all_general_country"),
                        51: lambda: self.manage_methods(method_name = "fetch_all_general_region")

                     }
        
        try:
            function_first = dict_pages[int(page_id)]
            function_first()
            
##            function_first, function_second = dict_stack_page[int(stack_page_id)]
##            function_first()
##            function_second()

        except KeyError as key_err:
            pass#print "key_err", key_err

        return
        pass

    def change_page(self, 
                    index = None, 
                    new_index = None,
                    stack_widget_object = None,
                    tree_widget_object = None, 
                    tree_view_object = None,
                    column_index = None,
                    set_text = None, 
                    lineEdit_object = None,
                    set_focus = None,
                    clear_widget = True,
                    load_data = True,
                    current_obj_clear_selection = False,
                    delete_selected_id = False,
                    current_obj_clear = False):
        '''
            NOTICE:
            ======
            This method changes the index of the given widget (e.g. QStackedWidget)
            using setCurrentIndex() and its fades between widgets on different pages
            - dependent on the index.

            PARAMETERS:
            ===========
            :index                          -       This function excepts an index of given object.
                                                    In this argument is the current index of changed item.

            :new_index  -

            : stack_widget_object           -

            :tree_widget_object             -

            :tree_view_object               -

            :column_index                   -

            :set_text                       -

            :lineEdit_object                -

            :set_focus                      -

            :clear_widget                   -

            :load_data                      -

            :current_obj_clear_selection    -

            :delete_selected_id             -

            :current_obj_clear              -

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        #self.quite_threads()
        
        #   We have to check if the given argument (new_index) isn't a
        #   None object (singleton object).
        if not isinstance(new_index, type(None)):

            #   Its necessary to to convert it with toPyObject(), because version 1
            #   of the QVariant API provides the QVariant.toPyObject() method to convert
            #   the QVariant back to a Python object of the correct type.
            #   Both versions will raise a Python exception if the conversion cannot be done.
            try:

                index = new_index.indexes()[1].data().toPyObject()

            except AttributeError:

                index = new_index.indexes()[1].data()#.toPyObject()

        self._current_page = int(index)

        '''
            When the user changes the page of QStackedWidget all QLineEdit()-objects,
            which are children of current widget, are deleted or cleared. That means,
            when the user leaves the current page and goes to the other new page, 
            all QLineEdit()-object, which are childrem from old pages, should be 
            deleted or cleared.

            First, we check, if the variables named (self._new_page) is not None.
        '''
        if not isinstance(self._new_page, type(None)):
            '''
                Yes, its not None. The variable isn't empty.
                Great. Ntext step, we save the current index in the variable named 
                (self._new_page).
            '''

            self._new_page = index

            '''
                After the new index has been saved, first we create a list of all QLineEdit()-objects,
                which are children of the old stack page, that the user was left. In this case we
                need to use the old page number, which is saved in the variable named (self._old_stack_page).
                The created list we sent it to the method called self.clear_widget_object_content().
            '''
            if clear_widget:
                pass

                #self.clear_widget_object_content(
                #    list_widget_object=self.ui_master_data.stackedWidget.widget(int(self._old_stack_page)).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

                # TODO: Think about what to do with selected ID

                #self.selected_be_edited_id = None

            '''
                After all QLineEdit()-objects has beend deleted or cleared we set the old page with the new page. 
                That means, we overwrite the old page number with the given new page number. We create a copy of them.
            '''
            self._old_stack_page = self._new_page

        else:

            '''
                Maybe the user starts this window at first time. Now we have to set the new and old page at the
                same time.
            '''
            self._new_page = index
            self._old_stack_page = index

            if clear_widget:
                pass

                #self.clear_widget_object_content(
                #    list_widget_object=self.ui_master_data.stackedWidget.widget(self._old_stack_page).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

        if not lineEdit_object is None:
            pass
            #self.set_text(line_edit_obj=lineEdit_object, text=set_text)

        if set_focus:
            pass
            #self.set_focus(widget_object=lineEdit_object)

        if load_data:
            self.manage_pages(index)


        stack_widget_object.setCurrentIndex(int(index))

        if current_obj_clear_selection:

            # self.clear_widget_object_content(
            #     list_widget_object=

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QTreeView))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clearSelection()

        if current_obj_clear:

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QLineEdit, QTextEdit, QPlainTextEdit))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clear()

        if delete_selected_id:
            self.remove_current_selected_id()
            self.remove_selected_be_edited_id()

        return

    def remove_current_selected_id(self):
        '''
            NOTICE:
            ======
            This method deletes the variable named (self.current_selected_id),
            which we don't need it no longer.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        logger.info("Set the variable (current_selected_id) on None")

        self.current_selected_id = None

        return

    def remove_selected_be_edited_id(self):
        '''
            NOTICE:
            ======
            This method deletes the variable named (self.selected_be_edited_id),
            which we don't need it no longer.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        logger.info("Set the variable (selected_be_edited_id) on None")

        self.selected_be_edited_id = None

        return

    def populate_navi_data(self,
                           model_obj = None,
                           data = None):
        print "populate_navi_data is called"
        '''
            NOTICE:
            =======
            We have to popuolate the navigation menu.

            PARAMETERS:
            ===========
            :model_obj      -   we need a model where the data should be populated.

            :data           -   This argument contains data to populate and display.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        ''' 
        try:
 
            for parent_text, child_text in data:
                count_item = QStandardItem('{}'.format(self._navi_count))
                item = QStandardItem(parent_text)
                model_obj.appendRow([item, count_item]) 
                self._navi_count += 1
                
                if child_text:
                    self.populate_navi_data(item, child_text)
                    
        except TypeError:
            pass

        return
                    
    def set_source_model(self, model = None, mode = None, where = None):
        '''
            NOTICE:
            =======
            Set main model as source for the proxy model
            to be processed by the proxy model.

            PARAMETERS:
            ===========
            :model  -   We except a model for the proxy model.
            
            :mode   -   Tell us which model should be set? Currently
                        you have two options, "database" or "navigation"

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''        
        if mode == "database":
            
            if not self._proxy_model is None:
                ''' When model already exists only clear the model for adding new items '''
                self._proxy_model.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._proxy_model.setSourceModel(model)
            #self._proxy_model = model

        if mode == "navigation":
            self._navigate_standard_model.setSourceModel(model)

        return

    def replace_proxy_model(self, 
                            proxy_model = None,
                            widget_tree_view = None):
        '''
            NOTICE:
            =======
            This method is used to replace any proxy model.

            PARAMETERS:
            ===========
            :proxy_model        -   we except a widget for the view.

            :widget_tree_view   -   We required a model to set it to the widget. 

            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        del self._proxy_model
        self._proxy_model = None
        self._proxy_model = proxy_model

        self.set_model(widget = widget_tree_view, model = proxy_model)
        self.init_selectionChanged_signal_treeview()

        try:
            self.close_wait_message()
        except AttributeError:
            pass

        return

    def set_model(self,
                 widget = None,
                 model = None):
        '''
            NOTICE:
            =======
            Sets the model for the view to present.

            PARAMETERS:
            ===========
            :widget     -   we except a widget for the view.

            :model      -   We required a model to set it to the widget. 

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        widget.setModel(model)

        return


    def set_sort_filter_proxy_model(self):
        '''
            NOTICE:
            =======
            Initialize the overwritten proxy model for filtering
            and main model for data.

            DETAILS:
            ========
            The setDynamicSortFilter()-method is set to True on proxy model.
            This property holds whether the proxy model is dynamically sorted
            and filtered whenever the contents of the source model change.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        self._proxy_model = MySortFilterProxyModel(self)
        self._proxy_model.setDynamicSortFilter(True)

        return

    def standard_item_model(self):
        '''
            NOTICE:
            =======
            Create an empty model for the TreeViews' data

            This method constructs a new item model that initially 
            has rows and columns, and that has the given parent.

            QTreeView needs a model to manage its data.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        self._navigate_standard_model = QStandardItemModel()

    def set_hide_column(self, 
                        list_tree_view_object = None,
                        tree_view_object = None, 
                        column = None, 
                        bool_value = None):
        '''
            NOTICE:
            =======
            The given column will be hidden, otherwise it will be shown

            PARAMETERS:
            ===========
            :list_tree_view_object  -   This argument contains a list of QTreeView()-objects

            :tree_view_object       -   It contains a single QTreeView()-object

            :column                 -   We need a number to hide a special column.

            :bool_value             -   Boolean to hide, when it is True.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        if not tree_view_object is None:
            tree_view_object.setColumnHidden(column, bool_value)

        if isinstance(list_tree_view_object, list):
            for tree_view in list_tree_view_object:
                tree_view.setColumnHidden(column, bool_value)


        return

    def hide_widget(self,
                    widget = None,
                    boolen_value = None):
        '''
            NOTICE:
            =======
            The given column will be hidden, otherwise it will be shown

            PARAMETERS:
            ===========
            :widget         -   Expect a widget-object to hide.

            :bool_value     -   Boolean to hide, when it is True.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        try:
            
            widget.setVisible(boolen_value)
            
        except AttributeError:

            widget.setHidden(boolen_value)
            

    def set_text(self, 
                 widget = None, 
                 text = None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -

            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(widget, QLineEdit):
            widget.setText(text)
        elif isinstance(widget, QProgressBar):
            widget.setText(text)

        return

    def set_focus(self, 
                  widget_object = None):

        '''
            NOTICE:
            =======
            This method is used to set the focus on given QLineEdit()-object.

            This method is necessary, because the setFocus()-function is used 
            in several places.

            PARAMETERS:
            ===========
            :line_edit_obj  -   We need the QLineEdit()-object to set the focus.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''

        logger.info("Set the focus on given QLineEdit()-object.")

        if not widget_object is None:
            widget_object.setFocus(True)

        return
        
    def set_maximum_value(self,
                          value = None):
        '''
            NOTICE:
            =======
            This method is used to set maximum value of QProgressBar() and its also
            used to show a QProgressbar()-object in busy mode with text instead of a
            percenttage of steps, because minimum and maximum are both set to 0.
            
            PARAMETERS:
            ===========
            :value  -   Get a vbalue to set the maximum value of progress bar, it
                        can be any int and it will be automatically converted to x/100% values
                        e.g. max_value = 3, current_value = 1, the progress bar will show 33%.

            :return -   othing is returned. The statement 'return' terminates 
                        a function. That makes sure that the function is definitely 
                        finished.
        '''
        self.hide_widget(widget = self.ui_master_data.frame_progress_bar, boolen_value = True)
        self.busy_progress_bar.setMaximum(value)

        return

    def update_progress_bar(self,
                            process_value = 0,
                            progress_bar_object = None,
                            final_count = 0):
        '''
            NOTICE:
            =======
            The given QProgressBar()-object will update its value, so it will suppose
            to progress while the items are populate.

            DETAILS:
            ========
            This method is also used to set range to minimum = 0 and to maximum = final_count.

            PARAMETERS:
            ===========
            :process_value  -   Current number of processes.

            :final_count    -   Total number of rows in a table. This method is also
                                used to stop the busy indicator of given
                                QProgressBar()-object by using setValue()-method.

            :return         -   othing is returned. The statement 'return' terminates 
                                a function. That makes sure that the function is definitely 
                                finished.
        '''
        self._proccessed_progressbar = process_value

        progress_bar_object.setRange(0, final_count)

        progress_bar_object.setValue(process_value)

        return

    def populate_model(self,
                       model = None,
                       tuple_items = None):
        '''
            NOTICE:
            =======
            The given QProgressBar()-object will update its value, so it will suppose
            to progress while the items are populate.

            DETAILS:
            ========
            First this this method just unpacks the given tuple and second it creates
            a new QStandardItem-class for each unpacked data. Third a new list is created
            containing the newly created QStandardItem-class(es). Fourth the created list
            is added to the given model. 

            PARAMETERS:
            ===========
            :model          -   We need a model where the data should be loaded.

            :tuple_items    -   The given data are in a tuple.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        count_items = len(tuple_items)

        if count_items == 2:

            item_first, item_second = tuple_items

            #img_size = QSize(24, 24)
            #setIcon(QIcon(QPixmap(UI_PATH).scaled(img_size, Qt.KeepAspectRatio)))

            #item2.setIcon(QIcon(QPixmap('rsz_criminal-activities.jpg').scaled(img_size, Qt.KeepAspectRatio)))

            #self._standard_item_model.insertRow(0)

        #two_columns_item = [QStandardItem(str(item_first)), QStandardItem(item_second)]
        two_columns_item = [QStandardItem(str(item_first)), QStandardItem(item_second)]
        #item00 = [QtGui.QStandardItem('Title00'), QtGui.QStandardItem('Summary00'), QtGui.QStandardItem('Summary0')]

        model.appendRow(two_columns_item)

            #self._standard_item_model.setItem (0, 0, str(item1))
            #self._standard_item_model.setItem (0, 1, item2)
            #self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            #self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            # self._standard_item_model.setData(self._standard_item_model.index(0, 1), QIcon("boldText.png"))


        if count_items == 3:

            item_first, item_second, item_third = tuple_items

            self._standard_item_model.insertRow(0)
            self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            self._standard_item_model.setData(self._standard_item_model.index(0, 2), str(item_third))

        return

    def get_current_index(self, widget=None):
        '''
            NOTICE:
            =======
            This method is used to hold the index position of the widget that is visible.

            PARAMETERS:
            ===========
            :widget     -       We need the widget to determine the current index.

            :return     -       Its returns the current index of the given widget.
        '''
        return widget.currentIndex()

    def append_text(self,
                    widget = None,
                    clear_widget = False,
                    text = None):
        if not widget is None:
            
            if clear_widget:
                widget.clear()

            if not text is None:
                widget.append(text)

        return

    def count_model_items(self,
                         widget = None,
                         standard_item_model = None):

        result = self.tr("Result(s):")
        counter = str(standard_item_model.rowCount())

        together = result + " " + counter 

        widget.setText(together)

    def create_critical_message_box(self,
                                    title = None,
                                    text = None,
                                    detail_text = None,
                                    icon = None):
        
        mBox = mBox = custom_message_box(self, text = text, title = title, icon = icon)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        if not detail_text is None:
            mBox.setInformativeText(detail_text)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def convert_string_to_bool(self, value):
        '''
            NOTICE:
            ======
            This method converts a given string in boolean value.
            We need this method for all QCheckBox()-objects. In the ini file, where all 
            settings are saved, there are only strings (for example: 'True' or 'False'). 
            All values aren't boolean value. When we want to change the state of 
            QCheckBox()-object, we have to convert all corresponding strings before.

            PARAMETERS:
            ===========
            :return     -   In the try block its returns the converted value. But, when 
                            the operation fails its returns False. In this case its
                            possible that the user or a other software has manipulated 
                            the contents of the ini file. 
        '''

        try:
            '''
                Let us try to invoke the function by passing an argument. In this case, we access
                to a dictionary at first, because the loaded settings are saved in the dictionary.
                We use the key (its the argument named value) to get the value, that is saved behind
                the key. With the obtained value we try to convert it in the bool-
            '''
            logger.info("Try to invoke a parsing_str_to_bool()-function by passing an {}-argument".format(value))

            return parsing_str_to_bool(value)
        except ValueError as VaErr:
            '''
                Ups, an error has occurred. Maybe the user or other program has changed 
                the content of this ini file. We write this error in the log file and
                return False. That means, the current QCheckBox()-object isn't check. The
                user will see an uncheck QCheckBox()-object.
            '''
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            return False

    def clear_tree_view_selection(self, 
                                  tree_view = None):

        tree_view.selectionModel().clearSelection()

        return

    def display_counting(self,
                         widget = None,
                         count = None):
        '''
            NOTICE:
            =======
            The given QProgressBar()-object will update its value, so it will suppose
            to progress while the items are populate.

            PARAMETERS:
            ===========
            :widget -   On which widget should be displayed the counter?
            
            :count  -   In this case we ecpect int()-value(s) to display
                        the counter on given widget.

            RETURNS:
            ========
            Nothing is returned. The statement 'return'
            terminates a function. That makes sure that the
            function is definitely finished.
        '''
        result = self.tr("Result(s):")
        counter = str(count)
        togehter = result + " " + counter
        widget.setText(togehter)


        return

    def sort_model(self, 
                    work_mode = None,
                    change_page = False,
                    page_index = None,
                    stack_widget_object = None,
                    current_obj_clear_selection = False,
                    delete_selected_id = False,
                    current_obj_clear = False,
                    line_edit_object_get_focus = None,
                    model = None,
                    column = None,
                    order = Qt.AscendingOrder):

        model.sort(column, order)

        self.close_wait_message()

        if work_mode == "add_record":

            self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                                  msg = self.tr("The item has been saved into the database successfully."),
                                                  detail_msg = self.tr(''),
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = line_edit_object_get_focus,
                                                  only_yes_button = True,
                                                  icon = self.image_path_collection.info_48x48_default)

        if work_mode == "edit_record":

            self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                                  msg = self.tr("The item has been changed into the database successfully."),
                                                  detail_msg = self.tr(''),
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = line_edit_object_get_focus,
                                                  only_yes_button = True,
                                                  icon = self.image_path_collection.info_48x48_default)

        if change_page:
            self.stack_set_current_index(index = page_index, 
                                         stack_widget_object = stack_widget_object, 
                                         clear_widget = True,
                                         current_obj_clear_selection = current_obj_clear_selection,
                                         delete_selected_id = delete_selected_id,
                                         current_obj_clear = current_obj_clear)


        return

    def update_proxy_model(self,
                           widget_tree_view = None,
                           starndard_item_model = None):

        self.quite_threads()
        
        self.task_thread = QThread()
        self.task_thread.work = TaskProxyModel(starndard_item_model = starndard_item_model,
                                               proxy_model = self._proxy_model)
        
        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.notify_item.connect(lambda standard_item: 
            self.set_source_model(model = standard_item, 
                                  mode = "database",
                                  where = 'load_proxy_model'))

        #self.task_thread.work.set_proxy_later_signal.connect(lambda: self.set_proxy_later(widget_tree_view = widget_tree_view))
        self.task_thread.work.notify_progress.connect(lambda proxy_model:
            self.replace_proxy_model(widget_tree_view = widget_tree_view, proxy_model = proxy_model))

        self.task_thread.work.hide_progress_bar_signal.connect(lambda:
                                                          self.hide_widget(widget = self.ui_master_data.frame_progress_bar,
                                                                                boolen_value = False))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)

        self.task_thread.started.connect(self.task_thread.work.init_object)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

    def refresh_model(self, 
                      change_page = False,
                      page_index = None,
                      stack_widget_object = None,
                      clear_widget = None,
                      current_obj_clear_selection = None,
                      delete_selected_id = None,
                      current_obj_clear = None,
                      line_edit_object_get_focus = None,
                      model = None,
                      category = None):

        print "refresh_model, category", category

        if category == "general_region":
            self._standard_item_model.standard_item_model_general_region = model

        if category == "general_country":
            self._standard_item_model.standard_item_model_general_country = model

        if category == "person_nationality":
            self._standard_item_model.standard_item_model_person_nationality = model

        if category == "film_genre":
            self._standard_item_model.standard_item_model_film_genre = model

        if category == "general_award":
            self._standard_item_model.standard_item_model_general_award = model

        if category == "music_genre":
            self._standard_item_model.standard_item_model_music_genre = model

        if category == "person_gender":
            self._standard_item_model.standard_item_model_person_gender = model

        if category == "person_salutation":
            self._standard_item_model.standard_item_model_person_salutation = model

        if category == "person_title":
            self._standard_item_model.standard_item_model_person_title = model

        if category == "person_eye_color":
            self._standard_item_model.standard_item_model_person_eye_color = model

        if category == "person_hair_color":
            self._standard_item_model.standard_item_model_person_hair_color = model

        if category == "person_religion":
            self._standard_item_model.standard_item_model_person_religion = model

        if category == "person_relationship_status":
            self._standard_item_model.standard_item_model_person_relationship_status = model

        if category == "person_relationship":
            self._standard_item_model.standard_item_model_person_relationship = model

        if category == "general_media_type":
            self._standard_item_model.standard_item_model_general_media_type = model

        if category == "general_packaging":
            self._standard_item_model.standard_item_model_general_packaging = model

        self.close_wait_message()

        #if change_page:
        self.stack_set_current_index(index = page_index, 
                                     stack_widget_object = stack_widget_object, 
                                     clear_widget = True,
                                     current_obj_clear_selection = True,
                                     delete_selected_id = True,
                                     current_obj_clear = True)

        self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                              msg = self.tr("The item has been saved into the database successfully."),
                                              detail_msg = self.tr(''),
                                              yes_no_button = False,
                                              line_edit_object_get_focus = line_edit_object_get_focus,
                                              only_yes_button = True,
                                              icon = self.image_path_collection.info_48x48_default)

        return

    def task_model(self,
                   model = None,
                   column = None,
                   line_edit_object_get_focus = None,
                   change_page = False,
                   page_index = None,
                   stack_widget_object = None,
                   clear_widget = None,
                   current_obj_clear_selection = None,
                   delete_selected_id = None,
                   current_obj_clear = None,
                   category = None):

        #   We create QThread() as container.
        self.task_thread = QThread()
        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() - 
        #   to an attribute of the thread object, named self.task_thread, 
        #   since it belongs semantically as well. It is then cleared after 
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskModel(model = model,
                                          column = column)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject(). 
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)


        self.task_thread.work.send_model_signal.connect(lambda model:
            self.refresh_model(change_page = change_page,
                               page_index = page_index,
                               stack_widget_object = stack_widget_object,
                               clear_widget = None,
                               current_obj_clear_selection = None,
                               delete_selected_id = None,
                               current_obj_clear = None,
                               line_edit_object_get_focus = line_edit_object_get_focus,
                               model = model, 
                               category = category))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)

        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start)

        #   The clean-up: when the worker instance emits finished() it will signal the thread 
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished() 
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasn’t fully 
        #   shut down yet when it is deleted, we connect the finished() of the thread 
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def delete_widget_from_layout(self,
                                  widget = None,
                                  layout = None):

        #   delete the ProgressBar()-object from the QLayout

        #   should also hide the widget, to make sure you don't get one 
        #   funny paintEvent before deleteLater actually does it's thing
        print "delete_widget_from_layout is called"
        self.busy_progress_bar.hide()
        self.horizontal_Layout.removeWidget(self.busy_progress_bar)
        #   Qt should take care of the rest, like removing it from layout 
        self.busy_progress_bar.deleteLater()
        del self.busy_progress_bar
        self.busy_progress_bar = None

    def set_standard_item_model_on_tree_view(self, 
                                             tree_view_widget = None,
                                             model = None):

        tree_view_widget.setModel(model)

        return

    def manage_standart_item_model(self, 
                                   category = None,
                                   columns = None,
                                   parent = None):

        self._standard_item_model.standard_item_model(category = category, rows = 0, columns = columns, parent = parent)


    def fetch_all(self,
                  list_header = None,
                  work_mode = None,
                  bulk_the_list = None,
                  notify_on_widget = None,
                  widget_label = None,
                  widget_tree_view = None,
                  widget_line_edit = None,
                  widget_stack_widget = None,
                  set_page = False,
                  page_index = None,
                  model = None,
                  sort_column = 0,
                  category = None):
        '''
            NOTICE:
            =======
            This method is used to start each thread to be fetching all data.

            DETAILS:
            ========
            First we create a new QThread()-object, second we have to create another
            instance of a class that inherits from QObject. Third, we move the created
            instance of QObject in the first created QThread()-object. Fourth all signals
            are connected to methods and the thread-process can begin.

            PARAMETERS:
            ===========
            :list_header            -   We expect a list to name the heading of the tree column 

            :work_mode              -   This argument lets us know what we should do. 
            
            :notify_on_widget       -   In this argument we know on which widget the notification 
                                        should be issued - for example, during an update process.
            
            :category               -   We have to know which category this is.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        self._proxy_model = None
        #   First, before we start a thread, we have to quite all running threads.
        self.quite_threads()

        #   Second we have to set a proxy model for filtering        
        self.set_sort_filter_proxy_model()
        
        if work_mode == "fetch_all": #   not equal
            #   Third we needd a item model with columns and rows for QTreeView
            self.init_custom_progres_bar()

        #   Fourth we have to delete all selected IDs they are saved in the attributes
        self.remove_current_selected_id()
        self.remove_selected_be_edited_id()

        if not notify_on_widget is None:
            #   When given argument is NOT None, we have to
            #   append text to given widget.
            self.append_text(widget = notify_on_widget,
                             clear_widget = True)
                                                                   
        if not list_header is None:
            pass
            #   When given argument is NOT None,
            #   we need to set the QStandItemModel
            #self._standard_item_model.standard_item_model(category = category, rows = 0, columns = len(list_header), parent = self)
            #self.manage_standart_item_model(category = category, columns = len(list_header), parent = self)
            #print "Let us call self.set_standard_item_model_on_tree_view()"
            #print "ROWCOUNT", model.rowCount()
            #if model.rowCount() < 1:
            #    self.set_standard_item_model_on_tree_view(tree_view_widget = widget_tree_view, model = model)
            #    print "self.set_standard_item_model_on_tree_view() is called"

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() - 
        #   to an attribute of the thread object, named self.task_thread, 
        #   since it belongs semantically as well. It is then cleared after 
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation(category = category,
                                                     work_mode = work_mode,
                                                     bulk_the_list = bulk_the_list,
                                                     configuration_saver_dict = self._configuration_saver_dict,
                                                     model_item = model,
                                                     standart_item_model = self._standard_item_model,
                                                     timer_interval = 1)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject(). 
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.populate_proxy_model_signal.connect(lambda:
                                                             self.update_proxy_model(widget_tree_view = widget_tree_view, 
                                                                                     starndard_item_model = model))

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message)

        if not widget_label is None:
            self.task_thread.work.fire_label.connect(lambda count_value:
                self.display_counting(widget = widget_label, 
                                      count = count_value))

        if set_page:
            self.task_thread.work.change_page_signal.connect(lambda:
                self.stack_set_current_index(index = page_index, 
                                             stack_widget_object = widget_stack_widget, 
                                             clear_widget = True,
                                             current_obj_clear_selection = True,
                                             delete_selected_id = True,
                                             current_obj_clear = True))


        self.task_thread.work.count_progress_signal.connect(lambda value:
                                                   self.set_maximum_value(value = value))

        if work_mode == "fetch_all":

            self.task_thread.work.delete_widget_signal.connect(self.delete_widget_from_layout)

            self.task_thread.work.update_progress_bar_signal.connect(lambda process_value, row_count:
                                                           self.update_progress_bar(process_value = process_value,
                                                                                    progress_bar_object = self.busy_progress_bar,
                                                                                    final_count = row_count))
            self.task_thread.work.manage_standart_item_model_signal.connect(lambda:
                                                            self.manage_standart_item_model(category = category,
                                                                                            columns = len(list_header)))

        if work_mode != "fetch_all":

            self.task_thread.work.sort_model_signal.connect(lambda:
                self.task_model(model = model,
                                column = sort_column,
                                line_edit_object_get_focus = widget_line_edit,
                                change_page = True,
                                stack_widget_object = widget_stack_widget,
                                page_index = page_index,
                                clear_widget = True,
                                current_obj_clear_selection = True,
                                delete_selected_id = True,
                                current_obj_clear = True,
                                category = category))

        self.task_thread.work.hide_column_signal.connect(lambda:
                                                    self.set_hide_column(tree_view_object = widget_tree_view,
                                                                         column = 0,
                                                                         bool_value = True))

        self.task_thread.work.count_model_items_signal.connect(lambda:
            self.count_model_items(widget = widget_label,
                                   standard_item_model = model))

        self.task_thread.work.create_wait_window_signal.connect(lambda text: 
            self.create_wait_window(text = text))

        self.task_thread.work.text_notify_update_signal.connect(lambda text:
                                                           self.append_text(widget = notify_on_widget,
                                                                            clear_widget = False,
                                                                            text = text))

        self.task_thread.work.confirm_saving_successful_signal.connect(lambda msg_text, detail_msg:
            self.create_generally_information_msg(title = self.tr('Confirmation'), 
                                                  msg = self.tr(msg_text),
                                                  detail_msg = detail_msg,
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = widget_line_edit,
                                                  only_yes_button = True,
                                                  icon = self.image_path_collection.info_48x48_default))

        self.task_thread.work.error_message_signal.connect(lambda title, error_msg, detail_text:
                                                      self.create_critical_message_box(title = title,
                                                                                       text = error_msg,
                                                                                       detail_text = detail_text,
                                                                                       icon = self.image_path_collection.warning_48x48_default))

        self.interrupt_working_thread_signal.connect(self.task_thread.work.abort_task_thread)

        #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        #   on this way we can always call thread.start() again later. Also the intention is
        #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)

        self.task_thread.work.notify_item.connect(lambda tuple_items:
                                             self.populate_model(tuple_items = tuple_items, model = model))


        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start)

        #   The clean-up: when the worker instance emits finished() it will signal the thread 
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished() 
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasn’t fully 
        #   shut down yet when it is deleted, we connect the finished() of the thread 
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def add_record(self, 
                   line_edit_obj_film_genre = None,
                   line_edit_obj_music_genre = None,
                   line_edit_obj_general_country = None,
                   line_edit_obj_general_language = None,
                   line_edit_obj_general_region = None,
                   line_edit_obj_general_award = None,
                   line_edit_obj_general_media_type = None,
                   line_edit_obj_general_packaging = None,
                   line_edit_obj_person_gender = None,
                   line_edit_obj_person_nationality = None,
                   line_edit_obj_person_salutation = None,
                   line_edit_obj_person_title = None,
                   line_edit_obj_person_hair_color = None,
                   line_edit_obj_person_eye_color = None,
                   line_edit_obj_person_religion = None,
                   line_edit_obj_person_relationship_status = None,
                   line_edit_obj_person_relationship = None,
                   line_edit_object_get_focus = None,
                   data_query = None,
                   stack_widget = None,
                   label_object = None,
                   model = None,
                   work_mode = None,
                   category = None):
        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :line_edit_obj      -       We except a QLineEdit()-object for getting
                                        the content, that we want to save in the database.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        dict_store_add_records = {'film_genre':                 lambda: self.task_thread.work.add_record(film_genre = unicode(line_edit_obj_film_genre.text()),
                                                                        set_flush = True,  category = category, work_area = "master_data"),
                                  'music_genre':                 lambda: self.task_thread.work.add_record(music_genre = unicode(line_edit_obj_music_genre.text()),
                                                                        set_flush = True,  category = category, work_area = "master_data"),
                                  'general_country':            lambda: self.task_thread.work.add_record(general_country = unicode(line_edit_obj_general_country.text()),
                                                                        set_flush = True, category=category, work_area = "master_data"),
                                  'general_language':           lambda: self.task_thread.work.add_record(general_language = unicode(line_edit_obj_general_language.text()),
                                                                        set_flush = True, category=category, work_area = "master_data"),
                                  'general_region':             lambda: self.task_thread.work.add_record(general_region = unicode(line_edit_obj_general_region.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'general_award':              lambda: self.task_thread.work.add_record(general_award = unicode(line_edit_obj_general_award.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_gender':              lambda: self.task_thread.work.add_record(person_gender = unicode(line_edit_obj_person_gender.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_nationality':         lambda: self.task_thread.work.add_record(person_nationality = unicode(line_edit_obj_person_nationality.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_salutation':          lambda: self.task_thread.work.add_record(person_salutation =unicode(line_edit_obj_person_salutation.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_title':               lambda: self.task_thread.work.add_record(person_title = unicode(line_edit_obj_person_title.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_hair_color':          lambda: self.task_thread.work.add_record(person_hair_color = unicode(line_edit_obj_person_hair_color.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_eye_color':           lambda: self.task_thread.work.add_record(person_eye_color = unicode(line_edit_obj_person_eye_color.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_religion':            lambda: self.task_thread.work.add_record(person_religion = unicode(line_edit_obj_person_religion.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_relationship_status': lambda: self.task_thread.work.add_record(person_relationship_status = unicode(line_edit_obj_person_relationship_status.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'person_relationship':        lambda: self.task_thread.work.add_record(person_relationship = unicode(line_edit_obj_person_relationship.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'general_media_type':         lambda: self.task_thread.work.add_record(general_media_type = unicode(line_edit_obj_general_media_type.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                  'general_packaging':          lambda: self.task_thread.work.add_record(general_packaging = unicode(line_edit_obj_general_packaging.text()),
                                                                        set_flush = True, category = category, work_area = "master_data"),
                                }

        dict_store_edit_records = {'film_genre':                 lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        film_genre = unicode(line_edit_obj_film_genre.text()), set_flush = True,
                                                                        category = category),
                                  'music_genre':                 lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        music_genre = unicode(line_edit_obj_music_genre.text()), set_flush = True,
                                                                        category = category),
                                  'general_country':            lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_country = unicode(line_edit_obj_general_country.text()), set_flush = True,
                                                                        category = category),
                                  'general_language':           lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_language = unicode(line_edit_obj_general_language.text()), set_flush = True,
                                                                        category = category),
                                  'general_region':             lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_region = unicode(line_edit_obj_general_region.text()), set_flush = True, 
                                                                        category = category),
                                  'general_award':              lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_award = unicode(line_edit_obj_general_award.text()), set_flush = True,
                                                                        category = category),
                                  'person_gender':              lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_gender = unicode(line_edit_obj_person_gender.text()), set_flush = True,
                                                                        category = category),
                                  'person_nationality':         lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_nationality = unicode(line_edit_obj_person_nationality.text()), set_flush = True,
                                                                        category = category),
                                  'person_salutation':          lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_salutation = unicode(line_edit_obj_person_salutation.text()), set_flush = True,
                                                                        category=category),
                                  'person_title':               lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_title = unicode(line_edit_obj_person_title.text()), set_flush = True,
                                                                        category = category),
                                  'person_hair_color':          lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_hair_color = unicode(line_edit_obj_person_hair_color.text()), set_flush = True,
                                                                        category = category),
                                  'person_eye_color':           lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_eye_color=unicode(line_edit_obj_person_eye_color.text()), set_flush = True,
                                                                        category = category),
                                  'person_religion':            lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_religion = unicode(line_edit_obj_person_religion.text()), set_flush = True,
                                                                        category = category),
                                  'person_relationship_status': lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_relationship_status = unicode(line_edit_obj_person_relationship_status.text()), set_flush = True,
                                                                        category = category),
                                  'person_relationship':        lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        person_relationship = unicode(line_edit_obj_person_relationship.text()), set_flush = True,
                                                                        category = category),
                                  'general_media_type':         lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_media_type = unicode(line_edit_obj_general_media_type.text()), set_flush = True,
                                                                        category = category),
                                  'general_packaging':          lambda: self.task_thread.work.edit_record(id = self.selected_be_edited_id,
                                                                        general_packaging = unicode(line_edit_obj_general_packaging.text()), set_flush = True,
                                                                        category = category),
                                }

        #   First, before we start a thread, we have to quite all running threads.
        self.quite_threads()

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() - 
        #   to an attribute of the thread object, named self.task_thread, 
        #   since it belongs semantically as well. It is then cleared after 
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation()

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject(). 
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.clear_widget_object_content_signal.connect(lambda: 
            self.clear_widget_object_content(
                list_widget_object = self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit))))

        self.task_thread.work.notify_item.connect(lambda tuple_items:
            self.populate_model(tuple_items = tuple_items, 
                                 model = model))

        self.task_thread.work.count_model_items_signal.connect(lambda:
            self.count_model_items(widget = label_object,
                                   standard_item_model = model))

        self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message)

        self.task_thread.work.create_wait_window_signal.connect(lambda: self.create_wait_window(text = self.tr("Please wait while manipulating data")))

        self.task_thread.work.remove_current_selected_id_signal.connect(self.remove_current_selected_id)
        self.task_thread.work.remove_selected_be_edited_id_signal.connect(self.remove_selected_be_edited_id)

        if work_mode == "add_record":

            self.task_thread.work.sort_model_signal.connect(lambda:
                self.sort_model(work_mode = 'add_record',
                                line_edit_object_get_focus = line_edit_object_get_focus,
                                model = model,
                                column = 1))

            self.task_thread.started.connect(dict_store_add_records[category])

        elif work_mode == "edit_record":

            self.task_thread.work.sort_model_signal.connect(lambda:
                self.sort_model(work_mode = 'edit_record',
                                change_page = True,
                                page_index = 0,
                                stack_widget_object = stack_widget,
                                current_obj_clear_selection = True,
                                delete_selected_id = True,
                                current_obj_clear = True,
                                line_edit_object_get_focus = line_edit_object_get_focus,
                                model = model,
                                column = 1))

            self.task_thread.started.connect(dict_store_edit_records[category])

            self.task_thread.work.remove_item_model_signal.connect(lambda item_id:
                self.remove_row(standard_item_model = model,
                                 item_id = item_id))

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def remove_row(self, 
                     standard_item_model = None,
                     item_id = None,
                     flags = Qt.MatchExactly,
                     column = 0):

        #   We will first need to find the items with the matching text 
        #   by iterating through the returned list, and then remove them from the model:

        for item in standard_item_model.findItems(str(item_id), flags, column):
            standard_item_model.removeRow(item.row())

        #standard_item_model.removeRow(self._row)

        return

    def create_wait_window(self, text = None):

        self.waiting_window = Waiting_Window(text_message = text)
        self.waiting_window.show()

        return

    def close_wait_message(self):

        self.waiting_window.close()

    def delete_record(self, 
                      id = None, 
                      tree_view_object = None,
                      line_edit_obj = None,
                      label_object = None,
                      model = None,
                      category = None):
        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :id         -   We need an index as an integer. 
                            With this ID, we want to uniquely identify 
                            and delete a record.

            :model      -   We except a QLineEdit()-object for getting
                            the content, that we want to save in the database.

            :category   -

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        #   First, before we start a thread, we have to quite all running threads.
        self.quite_threads()

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() - 
        #   to an attribute of the thread object, named self.task_thread, 
        #   since it belongs semantically as well. It is then cleared after 
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation()

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject(). 
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.clear_widget_object_content_signal.connect(lambda: 
            self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit))))

        self.task_thread.work.remove_item_model_signal.connect(lambda item_id:
            self.remove_row(standard_item_model = model,
                            item_id = item_id))

        self.task_thread.work.count_model_items_signal.connect(lambda:
            self.count_model_items(widget = label_object,
                                   standard_item_model = model))

        self.task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)
        self.task_thread.work.remove_current_selected_id_signal.connect(self.remove_current_selected_id)
        self.task_thread.work.remove_selected_be_edited_id_signal.connect(self.remove_selected_be_edited_id)
        
        converted_bool = self.convert_string_to_bool(self._configuration_saver_dict.dict_set_general_settings["AskBeforDelete"])

        try:

            int(id)

            if converted_bool:

                result_msg_box = self.create_generally_question_msg(title = self.tr('Conformation'),
                                                                    text = self.tr('Are you sure you want to permamently delete this item?'),
                                                                    detail_text = self.tr('NOTICE: By deleting, the selected record will be irrevocably removed! Continue?'),
                                                                    icon = self.image_path_collection.question_48x48_default)

            if result_msg_box:

                self.task_thread.work.delete_record(id = int(id),
                                                    set_flush = True,
                                                    category = category)

            self.task_thread.finished.connect(self.task_thread.deleteLater)

            self.task_thread.start()

        except (ValueError, TypeError):

            desired_trace = format_exc(exc_info())

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or \
            not desired_trace.find("int() argument must be a string or a number") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                self.create_generally_crritcal_msg(title_text, conn_err, "")

        self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit)))

        self.clear_tree_view_selection(tree_view = tree_view_object)

        self.set_focus(widget_object = line_edit_obj)

        return

    @pyqtSlot()
    def quite_threads(self):
        '''
            NOTICE:
            =======
            In this method we close all (open) threads that have been saved in the list named
            self._list_threads.

            DETAILS:
            ========
            Explicitly mark a python method as a Qt slot and specify a C++ signature for it 
            (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        self.interrupt_working_thread_signal.emit()

        for thread in self._list_threads:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in -place
        del self._list_threads[:]

        #   And then we want the programm will deletes the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This is a overwirtten and reimplemented method. 

            PARAMETERS:
            ===========
            :event     -    By default the given event is accepted and the widget is closed. We 
                            get this event when Qt receives a window close request for a top-level 
                            widget from the window system. That means, first it sends the widget a QCloseEvent. 
                            The widget is closed if it accepts the close event. The default implementation of 
                            QWidget.closeEvent() accepts the close event.

            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Close the current form.")

        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi. We want to know if 
                the actual window is an instance attribute inside in this class.
            '''
            self.set_flat_signal.emit('master_data', False)
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return

def output(text):
    print "Output:",  text
        
def show_ui():
    configuration_saver_dict = ConfigurationSaverDict()

    app = QApplication(argv)
    master_data_window = MasterData_Window(configuration_saver_dict = configuration_saver_dict,
                                           page_number = 0)
    master_data_window.showMaximized()
    exit(app.exec_())
    
def build_engine(dbms = None, db_driver = None, sqlite_path = None):
    engine = create_engine('{dbms}+{db_driver}:///{file_path}'.format(
        dbms = 'sqlite',
        db_driver = u'pysqlite',
        file_path = sqlite_path,
        encoding = 'utf8',
        echo = True))

    # We should bind engine to your model.
    ScopedSession.configure(bind=engine)

    return
    
def main():
    # This is only needed for Python v2 but is harmless for Python v3.
    import sip

    sqlite_path = None
    db_driver = None

    try:
        sip.setapi('QVariant', 2)
    except ValueError:
        sip.setapi('QVariant', 1)
    
    dbms = raw_input('Enter dbms (e.g. sqlite): ')
    if dbms == 'sqlite':
        sqlite_path = raw_input('Enter path to sqlite file: ')
        db_driver = 'pysqlite'
                           
    build_engine(dbms = dbms, db_driver = db_driver, sqlite_path = sqlite_path)

    show_ui()

    return
    
if __name__ == "__main__":
    main()
