#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from os import path
from sys import argv, exit

import requests

from PyQt4.QtCore import QThread, pyqtSignal, Qt, QSemaphore, QFile, pyqtSlot
from PyQt4.uic import loadUi
from PyQt4.QtGui import QDialog, QMessageBox, QIcon, QPushButton, QPixmap, QMdiSubWindow

##################################################################################
##                                                                              ##
##  IMPORTANT: When you want to execute this script as standalone program       ##
##  you have to comment out the below imports.                                  ##
##                                                                              ##
##################################################################################

try:

    from xarphus.core.check_update import get_response, compare_parser
    from xarphus.core.manage_folder import check_files_folder, delete_folder, create_folder
    from xarphus.core import manage_zip_file
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.manage_qt import custom_message_box
    from xarphus.collection_paths_image import ImagePathnCollection

    from xarphus.frm_about_version import NewInVersion_Window
    from xarphus.frm_password_required import PasswordRequired_Window

    from xarphus.core.downloading_update import Download_Thread
except ImportError:
    from core.check_update import get_response, compare_parser
    from info import info_app
    from core.config import ConfigurationSaverDict
    from core import check_update
    from core.manage_folder import check_files_folder, delete_folder, create_folder
    from core import manage_zip_file
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manage_qt import custom_message_box
    from collection_paths_image import ImagePathnCollection

    from frm_about_version import NewInVersion_Window
    from frm_password_required import PasswordRequired_Window

    from core.downloading_update import Download_Thread

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class CheckUpdate_Thread(QThread):

    check_result = pyqtSignal(str)
    generally_crritcal_msg = pyqtSignal(str, str)

    start_pulsing_progress_bar_signal = pyqtSignal()
    set_engabled_signal = pyqtSignal(bool)
    stop_pulsing_progress_bar_signal = pyqtSignal()
    update_text_on_label_signal = pyqtSignal(str)
    save_checksum_general_update_signal = pyqtSignal(str)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()

    def __init__(self,
                 parent,
                 **kwargs):

        QThread.__init__(self, parent = None)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))


    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def start_update_check(self):
        '''
           SUMMARY
           =======
           This method is used to start check up for new update.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the check for new update")

        self.start_pulsing_progress_bar_signal.emit()

        self.update_text_on_label_signal.emit(self.tr("Ckecking for update..."))

        try:

            online_update_version = get_response(
                url = self._kwargs['configuration_saver_dict'].dict_set_general_settings['URLServerVersion'])
            print "online_update_version", online_update_version

            check_update_result = compare_parser(
                current_version = unicode(self._kwargs['configuration_saver_dict'].dict_program_info['product_version']),
                new_version = online_update_version)

            if check_update_result:
                self.set_engabled_signal.emit(True)
                self.update_text_on_label_signal.emit(self.tr("New update is available.."))
                url_general_update_checksum = self._kwargs['configuration_saver_dict'].dict_set_general_settings['URLGeneralUpdateCheckSum']

            else:
                self.set_engabled_signal.emit(True)#False
                self.stop_pulsing_progress_bar_signal.emit()
                self.update_text_on_label_signal.emit(self.tr("No new update are available. \n"
                                                            "This program is up to date."))


                url_general_update_checksum = self._kwargs['configuration_saver_dict'].dict_set_general_settings['URLGeneralUpdateCheckSum']
                time_out = int(self._kwargs['configuration_saver_dict'].dict_set_general_settings['TimeOutUpdate'])
                http_auth_user_name = self._kwargs['configuration_saver_dict'].ht_access['htaccess_username']
                http_auth_pwd = self._kwargs['configuration_saver_dict'].ht_access['htpassword']
                result_general_update_checksum = get_response(
                  url = url_general_update_checksum,
                  http_auth_user_name = http_auth_user_name,
                  http_auth_pwd = http_auth_pwd,
                  time_out = time_out, protected_area = True)

                if result_general_update_checksum:
                  self.save_checksum_general_update_signal.emit(result_general_update_checksum)
                else:
                  pass#print "EERRRRRRRORRRRRR"

        except requests.exceptions.Timeout as e:
            # Maybe set up for a retry, or continue in a retry loop
            self.generally_crritcal_msg.emit(self.tr("Error - Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.TooManyRedirects as e:
            # Tell the user their URL was bad and try a different one
            self.generally_crritcal_msg.emit(self.tr("Error - Too many redirects"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.HTTPError as e:
            self.generally_crritcal_msg.emit(self.tr("Error - HTTP"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ConnectionError as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Connection"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.URLRequired as e:
            self.generally_crritcal_msg.emit(self.tr("Error - URL required"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ConnectTimeout as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Connect Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.ReadTimeout as e:
            self.generally_crritcal_msg.emit(self.tr("Error - Read Timeout"), self.tr("An error has occurred: ") + unicode(e))
        except requests.exceptions.RequestException as e:
            self.generally_crritcal_msg.emit(self.tr(""), self.tr("An error has occurred: ") + unicode(e))
        finally:
            self.quit_thread_signal.emit()
            self.wait_thread_signal.emit()
            self.stop_pulsing_progress_bar_signal.emit()

        return

    def stop(self):
        pass


class Download_Window(QDialog):
    def __init__(self,
                 parent,
                 **kwargs):

        QDialog.__init__(self, parent = None)

        self._kwargs = kwargs

        UI_PATH = QFile(":/ui_file/download.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))


        self._thread_list = []
        self._worker_list = []

        #TEMP_UPDATE_FOLDER = self._kwargs['configuration_saver_dict'].dict_set_general_settings["PathToTempFolder"]
        #DOWNLOAD_URL = self._kwargs['configuration_saver_dict'].dict_set_general_settings["URLServerUpdate"]
        #VERSION_URL = self._kwargs['configuration_saver_dict'].dict_set_general_settings["URLServerVersion"]

        self._saved_online_checksum_general_update = None

        self.progress_initialized = False

        self._current_running_worker = None

    #     # TEMP_PATH = path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')

    #     # #TEMP_PATH = path.join(BASE_PATH, 'example-app-0.3.win32.zip')
    #     # TIME_OUT = int(self.set_get_settings.dict_set_general_settings["TimeOutUpdate"])

        self.load_ui_file(UI_PATH)
        self.init_ui()
        self.init_clicked_signal_push_buttons()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_download = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           In this method the gui is inistialized. Currently the title
           of the window is set and the flag of this window is flagged.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Inistialize the GUI")

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.ui_download.setWindowFlags(self.flags)

        self.ui_download.progressBar_update.setAlignment(Qt.AlignCenter)

        self.start_check_new_update(self._kwargs)

        self.ui_download.setWindowModality(Qt.ApplicationModal)
        self.ui_download.setWindowTitle(self.tr("Herunterladen"))
        self.ui_download.setWindowIcon(QIcon(
          self._kwargs['image_path_collection'].update_16x16))

        self.ui_download.pushButton_close.setText(self.tr("Abbrechen"))
        self.ui_download.pushButton_close.setIcon(QIcon(
          self._kwargs['image_path_collection'].cancel_16x16))

        self.ui_download.pushButton_cancel.setText(self.tr("Ladevorgang abbrechen"))
        self.ui_download.pushButton_cancel.setIcon(QIcon(
          self._kwargs['image_path_collection'].delete_16x16))

        self.ui_download.pushButton_download.setText(self.tr("Herunterladen"))
        self.ui_download.pushButton_download.setIcon(QIcon(
          self._kwargs['image_path_collection'].update_16x16))

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method initializes the PushButton signals: clicked().
           It makes an action when the QPushButton()-object is clicked.
           When the user clicks on the QPushButton()-object, the clicked-signal of QPushButton()-object emitters.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        #self.ui_update.pushButton_cancel.clicked.connect(self.stop_thread)

        self.ui_download.pushButton_download.clicked.connect(
          lambda: self.start_download_task(**self._kwargs))

        self.ui_download.pushButton_close.clicked.connect(self.close)

        return

    def save_online_checksum(self, checksum):

      self._saved_online_checksum_general_update = checksum

      print "self._saved_online_checksum_general_update: {}".format(self._saved_online_checksum_general_update)

      self._kwargs['online_checksum'] = self._saved_online_checksum_general_update

      return

    def start_check_new_update(self,
                               kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to sign in.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the Thread")

        #   We create QThread() as container.
        task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskModel() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        task_thread.work = CheckUpdate_Thread(parent = None, **kwargs)

        #   The current created QThread object is saved to a list.
        self._thread_list.append(task_thread)
        self._worker_list.append(task_thread.work)

        #   TaskModel() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskModel()
        task_thread.work.moveToThread(task_thread)

        #   This customer signals are emitted, when somthing goes in thread.
        task_thread.work.update_text_on_label_signal.connect(
            lambda
            text:
            self.update_text_on_label(
                label_object = self.ui_download.label_update,
                text = text))

        task_thread.work.save_checksum_general_update_signal.connect(
          lambda
          checksum:
          self.save_online_checksum(checksum = checksum))

        task_thread.work.start_pulsing_progress_bar_signal.connect(
            lambda:
            self.update_on_progress_bar(
                progressbar_object = self.ui_download.progressBar_update))

        task_thread.work.stop_pulsing_progress_bar_signal.connect(
            lambda:
            self.update_on_progress_bar(
                progressbar_object = self.ui_download.progressBar_update,
                maximum = 1))

        task_thread.work.set_engabled_signal.connect(
            lambda bool_value:
            self.set_enabled(
                qt_object = self.ui_download.pushButton_download,
                bool_value = bool_value))

        task_thread.work.quit_thread_signal.connect(task_thread.quit)
        task_thread.work.wait_thread_signal.connect(task_thread.wait)

        #   This signal is emitted when the thread starts executing.
        task_thread.started.connect(task_thread.work.start_update_check)

        #   The clean-up: when the worker instance emits finished() it will signal the thread
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished()
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasnâ€™t fully
        #   shut down yet when it is deleted, we connect the finished() of the thread
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        task_thread.finished.connect(task_thread.deleteLater)

        #   Finally let us start the QThread
        task_thread.start()

        return

    def update_text_on_label(self,
                             label_object,
                             text):
        '''
           SUMMARY
           =======
           This method changes the text in a given QLabel.

           PARAMETERS
           ==========
           :label_object (class):   Given QLabel-Object which have to update.

           :text (str):             Given text for updating the QLabel.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                    statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the Thread")

        label_object.setText(text)

        return

    def update_on_progress_bar(self,
                               progressbar_object,
                               minimum = 0,
                               maximum = 0):
        '''
           SUMMARY
           =======
           This method is used to make or stopp a pulsing QProgressBar.

           PARAMETERS
           ==========
           :progressbar_object (class):    Given QProgressBar.

           :minimum (int):                  By default the range is zero

           :maximum (int):                  By default the range is zero

           RETURNS
           =======
           :return:                         Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                            statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start pulsing QProgressBar")

        progressbar_object.setRange(minimum, maximum)

        return

    def set_enabled(self,
                     qt_object,
                     bool_value):
        '''
           SUMMARY
           =======
           This method is used to enable or disable the given Qt-Object .

           PARAMETERS
           ==========
           :qt_object (class):  Given object of Qt.

           :minimum (bool):     Value of boolean

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start enabling/disabling the Qt object")

        qt_object.setEnabled(bool_value)

        return


    def start_download_task(self, **kwargs):
        '''
           SUMMARY
           =======
           This method is used to start a thread that refreshs the model.

           PARAMETERS
           ==========

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start thread for refreshing the model")

        # before we start to download the update, we have to remove the temp folder for update

        print "update_folder", self._kwargs['configuration_saver_dict'].dict_set_general_settings['PathToTempFolder']


        delete_folder(
          folder_path = self._kwargs['configuration_saver_dict'].dict_set_general_settings['PathToTempFolder'])

        #   We create QThread() as container.
        task_thread = QThread()
        #   Workaround - we bind the `work_object` - namend TaskModel() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        task_thread.work = Download_Thread(parent = None, **kwargs)

        #   The current created QThread object is saved to a list.
        self._thread_list.append(task_thread)
        self._worker_list.append(task_thread.work)

        self._current_running_worker = task_thread.work

        #   TaskModel() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskModel()
        task_thread.work.moveToThread(task_thread)

        task_thread.work.create_folder_signal.connect(
          lambda: create_folder(folder_path = self._kwargs['configuration_saver_dict'].dict_set_general_settings['PathToTempFolder']))

        task_thread.work.set_engabled_signal.connect(
            lambda bool_value:
            self.set_enabled(
                qt_object = self.ui_download.pushButton_cancel,
                bool_value = bool_value))

        task_thread.work.error_http_signal.connect(
            lambda
            msg_title,
            msg_text,
            msg_detail_text,
            msg_title_icon,
            msg_icon:
            self.create_simple_ok_message_box(
              msg_title = msg_title,
              msg_text = msg_text,
              msg_detail_text = msg_detail_text,
              msg_title_icon = msg_title_icon,
              msg_icon = msg_icon))

        task_thread.work.download_complete_message_signal.connect(
            lambda
            msg_title,
            msg_text,
            msg_detail_text,
            msg_title_icon,
            msg_icon:
            self.create_simple_ok_message_box(
              msg_title = msg_title,
              msg_text = msg_text,
              msg_detail_text = msg_detail_text,
              msg_title_icon = msg_title_icon,
              msg_icon = msg_icon))

        task_thread.work.download_error_message_signal.connect(
            lambda
            msg_title,
            msg_text,
            msg_detail_text,
            msg_title_icon,
            msg_icon:
            self.create_simple_ok_message_box(
              msg_title = msg_title,
              msg_text = msg_text,
              msg_detail_text = msg_detail_text,
              msg_title_icon = msg_title_icon,
              msg_icon = msg_icon))

        task_thread.work.on_progress_ready_signal.connect(
          lambda file_size:
          self.on_progress_ready(
            progress_bar = self.ui_download.progressBar_update,
            data = file_size))

        task_thread.work.set_max_value_signal.connect(
          lambda:
          self.set_progress_bar(
            progress_bar = self.ui_download.progressBar_update))

        task_thread.work.change_progress_signal.connect(
          lambda value:
          self.change_progress(
            progress_bar = self.ui_download.progressBar_update,
            value = value))

        task_thread.work.change_pushbutton_signal.connect(
          lambda boolean:
          self.change_pushbutton(
            push_button = self.ui_download.pushButton_cancel,
            boolean = boolean))

        task_thread.work.close_window_signal.connect(self.close)

        task_thread.work.quit_thread_signal.connect(task_thread.quit)
        task_thread.work.wait_thread_signal.connect(task_thread.wait)

        #   This signal is emitted when the thread starts executing.
        task_thread.started.connect(task_thread.work.start_thread)

        #   The clean-up: when the worker instance emits finished() it will signal the thread
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished()
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasnâ€™t fully
        #   shut down yet when it is deleted, we connect the finished() of the thread
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        task_thread.finished.connect(task_thread.deleteLater)

        #   Finally let us start the QThread
        task_thread.start()

        #self._worker_list.append(task_thread.work)

        return

    def stop_thread(self):

        self._current_running_worker.stop()


    def create_simple_ok_message_box(self,
                                      msg_title = None,
                                      msg_text = None,
                                      msg_detail_text = None,
                                      msg_title_icon = None,
                                      msg_icon = None):

        mBox = mBox = custom_message_box(self, text = msg_text, title_icon = msg_title_icon, windows_title = msg_title, icon = msg_icon)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        if not msg_detail_text is None:
            mBox.setInformativeText(msg_detail_text)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def on_progress_ready(self,
                          progress_bar,
                          data):

        # The first signal sets the maximum, the other signals increase a progress
        if self.progress_initialized:
            #self.progressBarUpdate.setValue(self.progressBarUpdate.value() + int(data))
            progress_bar.setValue(int(data))
        else:
            #self.progressBarUpdate.setMaximum(int(data))
            progress_bar.setRange(0, int(data))

            self.progress_initialized = True

        return

    def set_progress_bar(self,
                         progress_bar):

        progress_bar.setMaximum(100)

        return

    def change_progress(self,
                        progress_bar,
                        value):

        progress_bar.setValue(int(value))

        return

    def change_pushbutton(self,
                          push_button,
                          boolean):

        push_button.setEnabled(boolean)

        return

    # def requred_zip_password(self):
    #     self.password_required_form = PasswordRequired_Window(self.call_it_again)
    #     self.password_required_form.show()
    # #--------------------------------------------------------------------------------------
    # def need_zip_file_pwd_message_box(self):
    #     '''
    #     When download is finish the user get a messagebox. The user knows the download
    #     is complete.
    #     '''
    #     msg_box = QMessageBox()


    #     QMessageBox.warning(msg_box, self.tr('Message'),
    #                                        self.tr("The update file is password protected. "
    #                                                "Please enter the password in following window."), msg_box.Ok)
    #     self.requred_zip_password()
    # #--------------------------------------------------------------------------------------
    # def info_messagebox(self):
    #     '''
    #     When download is finish the user get a messagebox. The user knows the download
    #     is complete.
    #     '''
    #     msg_box = QMessageBox()

    #     QMessageBox.question(msg_box, self.tr(' Message '),
    #                                        self.tr("The update file has been downloaded successfully."), msg_box.Ok)
    # #--------------------------------------------------------------------------------------
    # def warning_messagebox(self):
    #     '''
    #     When download is finish the user get a messagebox. The user knows the download
    #     is complete.
    #     '''
    #     msg_box = QMessageBox()

    #     QMessageBox.warning(msg_box, self.tr(' Message '),
    #                                        self.tr("The update file is corrupted. \n"
    #                                                 "This can have several reasons: \n"
    #                                                 "(1): The update file is defective. \n"
    #                                                 "(2): There are incorrect files in the update file."
    #                                                 "(3): The update file has not been downloaded fully \n"
    #                                                 "\n"
    #                                                 "To fix the error: \n"
    #                                                 "Stellen Sie sicher, dass Sie Ã¼ber eine stabile "
    #                                                 "Make sure that you have a stable internet connection and download the update file again."), msg_box.Ok)

    # def create_connection_error_message_box(self):
    #     setIc_OK = QIcon(self.reload_ico)
    #     setIc_No = QIcon(self.cancel_ico)
    #     msgBox_unload_mdi = QMessageBox()
    #     msgBox_unload_mdi.setWindowTitle(self.tr("Warning"))
    #     msgBox_unload_mdi.setWindowIcon(QIcon(self.warning_ico))
    #     msgBox_unload_mdi.setIconPixmap(QPixmap(self.warning_ico))
    #     msgBox_unload_mdi.setText(self.tr("An error occurred while downloading the update file. \n"
    #                                        "\n"
    #                                        "Possible reasons: \n"
    #                                        "(1): Your Internet connect is currently very unstable. In this case, make sure "
    #                                        "you have a stable internet connection "
    #                                        "and download the update file (in a later stage) again.\n"
    #                                        "(2): The update server has just failed. In this case, "
    #                                        "you should try downloading the update file again later. \n"
    #                                        "(3): Connection timed out with the update server. "
    #                                        "The update server reacts / responds very much delayed. \n"
    #                                        "(4): It is quite possible that a firewall or a virus scanner "
    #                                        "on their system blocked to download the update file. Check your firewall or  virus scanner. \n"
    #                                        "\n"
    #                                        "Would you like to try again?"))
    #     msgBox_unload_mdi.addButton(
    #         QPushButton(setIc_OK, self.tr("Try again")),
    #         QMessageBox.YesRole)
    #     msgBox_unload_mdi.addButton(
    #         QPushButton(setIc_No, self.tr("Cancel")),
    #         QMessageBox.NoRole)

    #     ret = msgBox_unload_mdi.exec_()
    #     return ret

    # def create_list_function(self, choice_number):
    #     functions_list = [self.info_messagebox,
    #                       self.warning_messagebox,
    #                       self.need_zip_file_pwd_message_box,
    #                       self.create_connection_error_message_box]
    #     ret = functions_list[choice_number]()
    #     return ret

    # def call_it_again(self):
    #     result_zip_file, for_zip_pwd = self.is_zip_corrupt()
    #     #self.for_zip_pwd = for_zip_pwd

    # def is_zip_corrupt(self, personal_pwd=''):
    #     path_to_zip_temp_folder = self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"]
    #     zip_file_passwd = self.configuration_saver_dict.dict_set_general_settings["zip_pwd"]
    #     result_zip = manage_zip_file.check_number_files(path_to_zip_temp_folder, zip_file_passwd)

    #     #return result_zip[0], result_zip[1]
    #     return result_zip

    # def on_finish_download(self):

    #     '''
    #     Now the download is finish, and we should check if the zip file is
    #     coruppt. That ist why we call the function named (is_zip_corrupt) and
    #     wait for returning the value
    #     '''
    #     result_zip_file, for_zip_pwd = self.is_zip_corrupt()
    #     self.for_zip_pwd = for_zip_pwd
    #     self.aborted_download = None
    #     self.close()
    #     self.create_list_function(result_zip_file)



    # def on_connection_error(self):
    #     '''
    #     Once a connection error appears, the download is aborted.
    #     The function named (check_temp_folder_content) deletes
    #     the previously downloaded file.
    #     '''
    #     self.check_temp_folder_content()
    #     '''
    #     The function named (create_list_function) get the number 3 as an argument,
    #     because the messagebox is in fourth place in the created list. The messagebox,
    #     that has been called, returns a value: 0 for yes and 1 for no. The value is
    #     saved in the variable named (ret).
    #     '''
    #     ret = self.create_list_function(3)
    #     '''
    #     The variable is checked.
    #     '''
    #     if ret == 0:
    #         '''
    #         The user clicked the pushbutton yes.
    #         '''
    #         self.check_folder_exists()
    #     elif ret == 1:
    #         '''
    #         The user clicked the pushbutton no.
    #         '''
    #         self.progressBar_update.setValue(0)
    #         self.aborted_download = None
    #         self.ui_update.pushButton_cancel.setEnabled(False)
    #         self.ui_update.pushButton_update.setEnabled(True)


    # def check_folder_exists(self):


    #     result_folder = create_folder(self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"])
    #     print "result_folder", self.configuration_saver_dict.dict_set_general_settings["PathToTempFolder"]

    #     if result_folder == True:
    #         '''
    #         After the the folder is created the download starts
    #         '''
    #         logger.info("Calling the function (" + self.on_start.__name__ + ")")

    #         self.start_update_t()

    #         logger.info("The function (" + self.on_start.__name__ + ") is called")
    #     else:
    #         '''
    #         The folder existis. Now before the download starts the function has
    #         to check the contents of the update-temp-folder.
    #         '''
    #         result_contents = self.check_temp_folder_content()
    #         if result_contents == True:
    #             self.start_update_t()


    # def on_finished(self):
    #     self.progressBar_update.setValue(0)
    #     self.download_task.stop()
    #     self.progressBar_update.setValue(0)
    #     self.aborted_download = None
    #     #self.close()

    # def clear_temp_update_folder(self, temp_path):
    #     me = check_files_folder(temp_path)

    # def show_abort_msgbox(self):
    #     '''
    #     When download is finish the user get a messagebox. The user knows the download
    #     is complete.
    #     '''
    #     self.aborted_download = True
    #     msg_box = QMessageBox()

    #     QMessageBox.warning(msg_box, self.tr('Message'),
    #                                        self.tr("Downloading was canceled."), msg_box.Ok)

    # def abort_downloading_close(self):


    #     #self.show_abort_msgbox()
    #     if self.aborted_download == None:
    #         self.close()
    #     else:
    #         self.download_task.stop()
    #         self.aborted_download = None
    #         self.close()






    @pyqtSlot()
    def quite_workers(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._worker_list. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close all opened threads")

        for worker in self._worker_list:
            try:
                # It will quit **as soon as thread event loop unblocks**
                worker.stop()
                del worker
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in place.
        del self._worker_list[:]

        #   And then we want the programm to delete the contents itself from memory,
        #   that means this will entirely delete the list
        del self._worker_list

        #   But we need the list again, so we reinitialize it.
        self._worker_list = []

        return

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._thread_list. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close all opened threads")

        for thread in self._thread_list:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in place.
        del self._thread_list[:]

        #   And then we want the programm to delete the contents itself from memory,
        #   that means this will entirely delete the list
        del self._thread_list

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._thread_list = []

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This method closes the form.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current form.")

        # Well we have to stop the thread at first.
        self.quite_workers()

        #   Before we close the form, we have to stopp all running threads.
        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):
            #   The isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
            #   that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
            #   Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
            #   This is the case when the window is a subwindow in the mdi.
            self.parent().close()
        else:
            #   No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
            #   That means, the window is not a subwindow.
            self.close()

        return

if __name__ == "__main__":
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator

    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    from core.downloading_update import Download_Thread
    from core.manage_time import get_time_now

    convert_current_time = get_time_now()

    LOG_FILE_PATH = path.join(BASE_PATH, 'log', convert_current_time + '.log')
    TEMP_UPDATE_FOLDER = path.join(BASE_PATH, 'update')

    configuration_saver_dict = ConfigurationSaverDict()

    image_path_collection = ImagePathnCollection(theme = 'default')

    user_ini_file = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"

    configuration_saver_dict.dict_set_general_settings["PathToINISetting"] = user_ini_file

    configuration_saver_dict.dict_set_general_settings["PathToTempFolder"] = TEMP_UPDATE_FOLDER

    configuration_saver_dict.ht_access['htaccess_username'] = 'admin'
    configuration_saver_dict.ht_access['htpassword'] = 'danny82'



    program_info = info_app()

    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = Download_Window(
        configuration_saver_dict = configuration_saver_dict,
        image_path_collection= image_path_collection,
        parent = None)

    # window = Update_Window(configuration_saver_dict,
    #                        program_info,
    #                        image_path_collection,
    #                        get_response,
    #                        compare_parser,
    #                        None)

    #window.resize(600, 400)
    window.show()
    exit(app.exec_())
