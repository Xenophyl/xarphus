#-*- coding:latin1 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import sys

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QWidget, QIcon, QMdiSubWindow, QHeaderView
from PyQt4.uic import loadUi
from PyQt4.QtCore import QFile
'''
Private modules are imported.
'''

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")

class MovieVersionDetail_Window(QWidget):
    def __init__(self, choice_function,
                 func_up_to_date,
                 update_sublist,
                 form_title_movie,
                 type_detail,
                 save_config,
                 info_app,
                 img_paths,
                 parent):
        QWidget.__init__(self, parent)

        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.choice_function = choice_function
        self.form_title_movie = form_title_movie
        self.type_detail = type_detail

        #self.path_mdi_form = os.path.abspath(".")
        #self.getPath_about = os.path.join(os.path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

        UI_PATH = QFile(":/ui_file/movie_version_details.ui")

        self.set_get_settings = save_config

        self.img_paths = img_paths

        self.get_app_info = info_app

        try:
            #self.ui_error = ui_file.ui_load(self, "about")
            UI_PATH.open(QFile.ReadOnly)
            self.ui_movie_version_detail = loadUi(UI_PATH, self)
            UI_PATH.close()
            print "STATUS [OK]  (", FILE_NAME, "): GUI is loaded from PyQt4"
        except Exception as ex:
            print "STATUS [FAILED_2]  (", FILE_NAME, "):", ex
        except:
            print "STATUS [FAILED_3]  (", FILE_NAME, "): Unexpected error:", sys.exc_info()[0]
            raise

        #self.color_black = "<font color='#000000'>"
        #self.ui_pushbuttons_close_delete_save.groupBox.setStyleSheet('border: none solid gray;')
#        self.ui_search.textEditExplain.setStyleSheet(('background-color: #ffffff;')) # white

        #self.ui_search.setWindowModality(Qt.ApplicationModal)
        self.init_ui_movie()
        self.chosen_function()
        self.type_of_detail()
        self.init_action_pushbuttons()
        #self.get_language_about()
        #self.set_color()
        #self.set_language_about()
        #self.ui_pushbuttons_close_delete_save.show()
        #self.init_actions_button()
        #self.create_actions_label()
        #self.get_licence()
#--------------------------------------------------------------------------------------
    def init_ui_add_new_movie_detail(self):
        self.ui_movie_version_detail.setWindowTitle(self.form_title_movie)
        self.ui_movie_version_detail.pushButton_delete.setEnabled(False)
        self.ui_movie_version_detail.pushButton_save.setEnabled(False)
    #--------------------------------------------------------------------------------------
    def ini_ui_show_movie(self):
        self.ui_movie_version_detail.setWindowTitle(self.form_title_movie)
    #--------------------------------------------------------------------------------------
    def init_ui_movie(self):
        self.ui_movie_version_detail.setWindowIcon(QIcon(self.img_paths.movie_icon))
#        weidh = (self.movie_detail.width() + self.pushbutton_close_delete_save.width())
#        heigh = (self.movie_detail.height() + self.pushbutton_close_delete_save.height())
#        print "weidh", weidh

        #self.ui_movie_detail.setFixedSize(50, 50)

        #self.ui_movie_detail.treeWidget_audio_format.resizeColumnsToContents()

        # This makes sure that columnwidths and heights are set to match their content.
        self.ui_movie_version_detail.treeWidget_audio_format.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_version_detail.treeWidget_audio_format.header().setStretchLastSection(True)

        self.ui_movie_version_detail.treeWidget_language.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_version_detail.treeWidget_language.header().setStretchLastSection(True)

        self.ui_movie_version_detail.treeWidget_aspect_ratio.header().setResizeMode(QHeaderView.ResizeToContents)
        self.ui_movie_version_detail.treeWidget_aspect_ratio.header().setStretchLastSection(True)

        #self.ui_movie_detail.treeWidget_language.resizeColumnsToContents()
        #self.ui_movie_detail.treeWidget_aspect_ratio.resizeColumnToContents()
        #self.ui_movie_detail.setFixedSize(self.ui_movie_detail.width(), self.ui_movie_detail.height())
#        self.pushbutton_close_delete_save.setFixedSize(self.movie_detail.width(), self.pushbutton_close_delete_save.height())
        #self.setFixedSize(self.movie_detail.width(), heigh)
        #self.pushbutton_close_delete_save.setFixedSize(self.pushbutton_close_delete_save.width(), self.pushbutton_close_delete_save.height())
        #self.setFixedSize(self.movie_detail.width(), int(heigh))

#        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
#        sizePolicy.setHeightForWidth(True)
#        self.setSizePolicy(sizePolicy)
    #--------------------------------------------------------------------------------------
    def init_action_pushbuttons(self):
        self.ui_movie_version_detail.pushButton_close.clicked.connect(self.close_form)
        #self.ui_movie_detail.pushButton_add_film_version.clicked.connect(self.show_movie_Detail)
    #--------------------------------------------------------------------------------------
    def lineEdit_change(self):
        self.ui_movie_version_detail.lineEdit_title_translated.textChanged.connect(self.title_form_change)
    #--------------------------------------------------------------------------------------
    def init_label_edition(self):
        self.ui_movie_version_detail.label_type_detail.setText(self.tr("Veröffentlichung / Edition"))

    def init_label_version(self):
        self.ui_movie_version_detail.label_type_detail.setText(self.tr("Fassung / Version"))

    def create_type_list(self, type_number):
        print "type_number", type_number
        list_functions = [
            self.init_label_version,
            self.init_label_edition
                            ]

        list_functions[type_number]()
    #--------------------------------------------------------------------------------------
    def type_of_detail(self):
        self.create_type_list(self.type_detail)
    #--------------------------------------------------------------------------------------
    def create_function_list(self, choice_number):
        list_functions = [self.init_ui_add_new_movie_detail,
                          self.ini_ui_show_movie
                            ]

        list_functions[choice_number]()
    #--------------------------------------------------------------------------------------
    def chosen_function(self):
        self.create_function_list(self.choice_function)
    #--------------------------------------------------------------------------------------
    def do_something(self):
        pass
    #--------------------------------------------------------------------------------------
    def closeEvent(self, event):
        pass
    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
        print "STATUS [OK]  (", FILE_NAME, "): The function (close_form) was called"
    #--------------------------------------------------------------------------------------
