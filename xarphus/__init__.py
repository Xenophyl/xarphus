#!/usr/bin/env python
# -*- coding: utf-8 -*-

##################################################
# import compile-file
from xarphus.licence import licence_rc
from xarphus.localization_files import translate_files_rc
from xarphus.css_template import qt_css_style_rc
from xarphus.images import images_rc
from xarphus.gui import ui_rc
###################################################