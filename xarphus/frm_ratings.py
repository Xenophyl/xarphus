#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import os
import sys
from inspect import currentframe

from PyQt4.QtCore import QFile, Qt
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow

BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class Ratings_Window(QWidget):
    def __init__(self, func_up_to_date,
                 update_subwindow_list,
                 info_app,
                 object_category,
                 parent):

        QWidget.__init__(self, parent)

        self.info_app = info_app

        logger.info(
            "The instance of the class of " + self.get_class_name() + "() is created. - (Module names: " + __name__ + ")")

        UI_PATH = QFile(":/ui_file/rating.ui")

        self.func = func_up_to_date
        self.update_subwindow_list = update_subwindow_list
        self.object_category = object_category

        #print "OBJEKT KATEGORIE - AddRatings_Window", self.object_category

        logger.info("Opening the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + "opened")
        logger.info("Loading the *.ui-files (" + str(UI_PATH.fileName()) + "")

        self.ui_rating = loadUi(UI_PATH, self)

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is loaded")
        logger.info(
                "Closing the *.ui-files (" + str(UI_PATH.fileName()) + "")

        UI_PATH.close()

        logger.info("The *.ui-files (" + str(UI_PATH.fileName()) + ") is closed")

        self.init_ui()
        self.init_pushButton()

    def init_ui(self):
        ui_height, ui_width = self.calculate_form_size(self.ui_rating,
                                                                    form_height=1.08,
                                                                    form_width=1)
        self.resize(ui_width, ui_height)


    def calculate_form_size(self, form_obj=None, form_height=0, form_width=0):
        '''
        :param form_obj: Gets the object of loafUi
        :param form_height: Gets the value of height
        :param form_width: Gets the value of width
        :return: Returns back the value of height and width
        '''
        ui_height = (form_obj.height() * float(form_height))
        ui_width = (form_obj.width() * float(form_width))
        return ui_height, ui_width

    def init_pushButton(self):
        self.ui_rating.pushButton_close.clicked.connect(self.close_form)

    def get_class_name(self):
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")
        return self.__class__.__name__

    def closeEvent(self, event):
        pass

    def close_form(self):
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is calling")
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.update_subwindow_list()
            self.func()
        logger.info("The function (" + str(sys._getframe().f_code.co_name) + ") is called")