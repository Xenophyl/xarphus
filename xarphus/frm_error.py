#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

'''
The modules of required libraries are imported.
'''
import os
import sys

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QDialog, QPixmap, QIcon
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile

'''
Private modules are imported.
'''
try:
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.qt_info import get_version_qt
except ImportError:
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.qt_info import get_version_qt


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")

class Error_Window(QDialog):
    def __init__(self,
                 text_title,
                 main_text,
                 parent):
        QDialog.__init__(self, parent)

        self.main_text = main_text
        self.text_title = text_title
        #self.image_path_collection = image_path_collection
        #self.icon_type = icon_type

        UI_PATH = QFile(":/ui_file/send_bug_report.ui")


        UI_PATH.open(QFile.ReadOnly)
        self.ui_error = loadUi(UI_PATH, self)
        UI_PATH.close()


        self.ui_error.setWindowModality(Qt.ApplicationModal)
        flags = flag_dialog_titlehint_customizewondowhint()

        self.ui_error.setWindowFlags(flags)

        self.init_gui()
        #self.init_icons()
        self.init_push_buttons()

#        self.set_language()
#        self.ui_error.show()
        #self.get_error_textt()
 #       self.set_actions_button()




#--------------------------------------------------------------------------------------
    def init_gui(self):
        self.ui_error.setWindowTitle(self.text_title)
        self.ui_error.plainTextEdit_bug_report.setPlainText(self.main_text)
#--------------------------------------------------------------------------------------
    def init_icons(self):

        dict_icons = {
                    "critical": [self.ui_error.label_icon.setPixmap(QPixmap(self.image_path_collection.error_icon)),
                                self.ui_error.setWindowIcon(QIcon(self.image_path_collection.error_icon))],

                        }

        dict_icons[self.icon_type]

#--------------------------------------------------------------------------------------
    def init_push_buttons(self):
        self.ui_error.pushButton_close.clicked.connect(self.close)

    def get_error_textt(self):
        for i in self.error_text:
            self.ui_error.plainTextEdit_bug_report.setPlainText(i)
#--------------------------------------------------------------------------------------
    def set_language(self):
        m = get_version_qt()

        self.ui_error.setWindowTitle(self.set_language.dict_ui_about["about_title"])

        self.ui_error.pushButton_close.setText(self.set_language.dict_ui_about["pushButton_close"])
