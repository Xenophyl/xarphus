#!/usr/bin/env python
#-*- coding:utf-8 -*-

import os
import sys
import hashlib
from os import path
import requests
from requests.auth import HTTPBasicAuth

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QRegExp, Qt, QString, QFile, pyqtSignal, QThread, pyqtSlot
from PyQt4.QtGui import QDialog, QIcon, QRegExpValidator, QValidator, QLineEdit, QMdiSubWindow, QMessageBox, QPushButton
from PyQt4.uic import loadUi

try:
    from xarphus.frm_register import Register_Window
    from xarphus.languages.german import Language
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    from xarphus.gui import ui_rc
except ImportError:
    from frm_register import Register_Window
    from languages.german import Language
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    from gui import ui_rc
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_path import is_specifically_suffix
    from core.manage_logging import configure_logging


BASE_PATH = os.path.dirname(os.path.abspath(__file__))
#UI_PATH = os.path.join(BASE_PATH, 'gui', 'costumer_login.ui')
#UI_PATH = QFile(":/ui_file/costumer_login.ui")

class SupportLogin_Worker(QThread):

    error_http_signal = pyqtSignal(str, str, str)
    general_info_http_signal = pyqtSignal(str, str, str)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()
    quite_all_threads_signal = pyqtSignal()

    def __init__(self, username, password, kwargs, parent = None):
        QThread.__init__(self, parent)

        self._username = username
        self._password = password
        self._kwargs = kwargs

    def start_sign_in(self):
        '''
           SUMMARY
           =======
           This method starts a thread to sign in.

           PARAMETERS
           ==========
           :text (unicode):     The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           :category (str):     The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start to update the given dictionary")

        try:
            session_requests = None
            session_requests = requests.session()

            url_server_update = self._kwargs.get('configuration_saver_dict').dict_set_general_settings["URLServerUpdate"]

            auth = HTTPBasicAuth(self._username, self._password)

            response = session_requests.get(url = url_server_update, auth = auth, stream = True)
            http_status_code = response.status_code

            print "http_status_code", http_status_code

            if not http_status_code == 200:
                print http_status_code

                self.error_http_signal.emit(
                    'Error',
                    'The username or password you entered is incorrect',
                    str(http_status_code))

            else:

                access_data_list = [(self._username, 'username', self._kwargs),(self._password, 'password', self._kwargs)]

                for data_access, category, kwargs in access_data_list:
                    self.update_dict(
                        text = unicode(data_access),
                        category = category,
                        kwargs = kwargs)

                self.general_info_http_signal.emit(
                    'Successful',
                    'Your login is successful',
                    str(http_status_code))

        except (requests.exceptions.URLRequired,
                requests.exceptions.ConnectionError,
                requests.exceptions.HTTPError,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectTimeout,
                requests.exceptions.ReadTimeout), g:
            self.error_http_signal.emit()

        finally:
            self.quit_thread_signal.emit()
            self.wait_thread_signal.emit()
            self.quite_all_threads_signal.emit()

        return

    def update_dict(self, text, category, kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to sign in.

           PARAMETERS
           ==========
           :text (unicode):     The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           :category (str):     The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start to update the given dictionary")

        if category == 'password':
            kwargs.get('configuration_saver_dict').ht_access['htpassword'] = unicode(text)
        elif category == 'username':
            kwargs.get('configuration_saver_dict').ht_access['htaccess_username'] = unicode(text)

        return


class CustomerLogin_Window(QDialog):
    '''
       SUMMARY
       =======
       Its shows a form where the user can log in on service areas.
    '''
    def __init__(self,
                 parent = None,
                 **kwargs):
        '''
            PARAMETERS
            ==========
            :parent:            This class does support passing a parent argument, because
                                we know that we will need the parent argument - its more flexible.
                                By default its none.

           :kwargs (dict):      The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.
        '''

        QDialog.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/customer_login.ui")

        self.load_ui_file(ui_path = UI_PATH)

        self.set_language = Language()

        self._list_threads = []

        self._kwargs = kwargs

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_customer_login.setWindowFlags(flags)

        self.set_ui_language()
        self.init_ui()
        self.create_action_button()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self,
                     ui_path):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(ui_path.fileName()))

        logger.info("Load the *.ui-files ({})".format(ui_path.fileName()))

        self.ui_customer_login = loadUi(ui_path, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(ui_path.fileName()))
        logger.info("Close the *.ui-files ({})".format(ui_path.fileName()))

        ui_path.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(ui_path.fileName()))

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of profile list.

           RETURNS
           =======

           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.ui_customer_login.setWindowModality(Qt.ApplicationModal)
        self.ui_customer_login.setFixedSize(self.ui_customer_login.width(), self.ui_customer_login.height())

        return

    def check_threads_alive(self):
        threads = [t for t in self._list_threads if t.isRunning()]
        print "check_threads_alive, threads", threads

    def sign_up_user(self, username, password, kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to sign in.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the Thread")

        self._my_thread_list = []


        task_thread = QThread(self)

        task_thread.work = SupportLogin_Worker(username, password, kwargs)

        task_thread.work.moveToThread(task_thread)

        self._list_threads.append(task_thread)

        task_thread.work.error_http_signal.connect(self.create_generally_crritcal_msg)
        task_thread.work.general_info_http_signal.connect(self.create_generally_information_msg)

        task_thread.work.quit_thread_signal.connect(task_thread.quit)
        task_thread.work.wait_thread_signal.connect(task_thread.wait)

        task_thread.work.quite_all_threads_signal.connect(self.quite_threads)



        task_thread.started.connect(task_thread.work.start_sign_in)
        task_thread.finished.connect(task_thread.deleteLater)
        task_thread.start()

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (Qstring):    We except a text for the title bar of the message box.

           :err_msg (Qstring) :     We need the text for the main message to be displayed.

           :set_flag (bool):        For removing close button from QMessageBox.

           :detail_msg (Qstring):   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         title,
                                         text,
                                         detail_text = None,
                                         icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (Qstring):        We except a text for the title bar of the message box.

           :text (Qstring) :        We need the text for the main message to be displayed.

           :detail_text (Qstring):  When we get a detailed text, the hide/show details" button will be added.

           :icon (str):             For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  title = title,
                                  text = text,
                                  detail_text = detail_text,
                                  icon = icon)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        self.close()

    def activate_item_customer_profile(self):
        #self.active_customer_menu_item()
        return
    #--------------------------------------------------------------------------------------
    def set_ui_language(self):
        self.ui_customer_login.lineEdit_password.textEdited.connect(self.check_inputs)
        self.ui_customer_login.lineEdit_password.setStyleSheet('background-color: #fff79a;')
        self.ui_customer_login.lineEdit_password.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.lineEdit_password))
# ()[]{}?!$%&/=*+~,.;:<>-_
        self.lineEdit_nickname.textChanged.connect(self.check_inputs)
        self.lineEdit_nickname.textEdited.connect(self.check_inputs)
        self.ui_customer_login.lineEdit_nickname.setStyleSheet('background-color: #fff79a;')
        self.ui_customer_login.lineEdit_nickname.setValidator(QRegExpValidator(QRegExp("[r'\w'aA-zZ0-9]+"), self.lineEdit_nickname)) # r'\w' Umlaute

        return

    def create_action_button(self):
        self.ui_customer_login.pushButton_logIn.clicked.connect(
            lambda: self.sign_up_user(
                username = unicode(self.ui_customer_login.lineEdit_nickname.text()),
                password = unicode(self.ui_customer_login.lineEdit_password.text()),
                kwargs = self._kwargs))

        self.ui_customer_login.pushButton_close.clicked.connect(self.close)
        self.ui_customer_login.pushButton_to_sign_in.clicked.connect(self.create_regist_window)
        #self.ui_customer_login.pushButton_to_sign_in.clicked.connect(self.activate_item_customer_profile)
#--------------------------------------------------------------------------------------
    def create_regist_window(self):
        self.register_form = Register_Window(self.get_app_info, self)
        #self.custom_logger.info("he instance of the class of Register_Window() is created - (" + __name__ + ")")
        self.register_form.show()
        self.close()
#--------------------------------------------------------------------------------------
    def create_md5(self):
        lorem = self.ui_customer_login.lineEdit_password.text()
        h = hashlib.md5()
        h.update(lorem)

        loremMD5 = h.hexdigest()

        a = hashlib.sha1()
        a.update(loremMD5)

        loremSHA1 = a.hexdigest()

        c = hashlib.sha256()
        c.update(loremSHA1)
        print "Mit SHA256: ", c.hexdigest()
#--------------------------------------------------------------------------------------
    def check_inputs(self):
        #-------------------------------------------------------------------------------
        sender = self.sender()
        validator = sender.validator()
        state = validator.validate(sender.text(), 0)[0]
        #-------------------------------------------------------------------------------
        if state == QValidator.Acceptable:
            self.ui_customer_login.pushButton_logIn.setEnabled(
                self.lineEdit_nickname.hasAcceptableInput()
                and self.lineEdit_password.hasAcceptableInput())
         #-------------------------------------------------------------------------------
            color = '#c4df9b' # green
            self.ui_customer_login.lineEdit_password.setEnabled(True)
        #-------------------------------------------------------------------------------
        elif state == QValidator.Intermediate:

            color = '#fff79a' # yellow
            self.ui_customer_login.pushButton_logIn.setEnabled(False)

        else:
            color = '#f6989d' # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

#    def check_input_lineEdit_password(self, *args, **kwargs):
#        sender = self.sender()
#        validator = sender.validator()
#        state = validator.validate(sender.text(), 0)[0]
#        if state == QValidator.Acceptable:
#            color = '#c4df9b' # green
#            self.ui_customer_login.pushButton_logIn.setEnabled(True)
#        elif state == QValidator.Intermediate:
#            color = '#fff79a' # yellow
#            self.ui_customer_login.pushButton_logIn.setEnabled(False)
#        else:
#            color = '#f6989d' # red
#        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)
#--------------------------------------------------------------------------------------
    def send_login(self):
        self.abus = self.ui_customer_login.lineEdit_nickname.text()
        self.bbus = self.ui_customer_login.lineEdit_password.text()

        if not self.bbus:
            print "leer"
        else:
            print "belegt"
            print self.abus, self.bbus

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._list_threads. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close all opened threads")

        for thread in self._list_threads:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        self.check_threads_alive()

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in place.
        del self._list_threads[:]

        #   And then we want the programm to delete the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def closeEvent(self, event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event (class):  By default the given event is accepted and the widget is closed. We
                            get this event when Qt receives a window close request for a top-level
                            widget from the window system. That means, first it sends the widget a QCloseEvent.
                            The widget is closed if it accepts the close event. The default implementation of
                            QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:         Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.ui_customer_login.close()

        return

if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator


    dict_custom_config = ConfigurationSaverDict()

    convert_current_time = get_time_now()

    #LOG_FILE_PATH = path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')

    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

    if not result_suffix == True:
        LOG_FILE_FOLDER_PATH = result_suffix

    else:
        pass

    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)


    app = QApplication(sys.argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)

    window = CustomerLogin_Window()
    window.show()
    sys.exit(app.exec_())
