#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import path
from sys import argv, exit, exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QFile, Qt, pyqtSignal, QRegExp, QTimer, QString, QThread, \
                         pyqtSlot
from PyQt4.uic import loadUi
from PyQt4.QtGui import QWidget, QMdiSubWindow, QTreeWidget, QFileDialog, QPixmap, \
                        QLineEdit, QTextEdit, QPlainTextEdit, QTreeView, QComboBox, \
                        QStandardItemModel, QStandardItem, QLineEdit, QPushButton, \
                        QMenu, QAction, QIcon, QToolButton, QSortFilterProxyModel, \
                        QCompleter

try:
    from xarphus.core.manage_calculation import calculate_cover_poster
    from xarphus.core.manage_folder import is_folder_exists
    from xarphus.core.manage_path import extract_directory
    from xarphus.core.manage_input_controller import InputController
    from xarphus.core.managed_drag_drop import ManagedDragDrop
    from xarphus.core.manage_file import get_file_extension
    from xarphus.q_line_edit_button_right import ButtonLineEdit
    from xarphus.core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate
    from xarphus.frm_waiting import Waiting_Window
    from xarphus.core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from xarphus.core.managed_functions import remove_tuple_from_list
    from xarphus.subclass_master_data_load_data_item import TaskDataManipulation, TaskModel
    from xarphus.core.completer_combobox import CompleterComboBox


except ImportError:
    from core.manage_path import is_specifically_suffix

    from core.manage_ini_file import get_configuration, DefaultINIFile

    from core.config import ConfigurationSaverDict

    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from core.manage_calculation import calculate_cover_poster
    from core.manage_folder import is_folder_exists
    from core.manage_path import extract_directory
    from core.manage_input_controller import InputController
    from core.managed_drag_drop import ManagedDragDrop
    from core.manage_file import get_file_extension
    from q_line_edit_button_right import ButtonLineEdit
    from core.managed_pyqt4_threads import TaskProxyModel, TaskFilterUpdate
    from frm_waiting import Waiting_Window
    from core.manage_qt import custom_message_box, flag_dialog_titlehint_customizewondowhint
    from xarphus.core.managed_functions import remove_tuple_from_list
    from subclass_master_data_load_data_item import TaskDataManipulation, TaskModel
    from core.completer_combobox import CompleterComboBox

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'update.ui')
#UI_PATH = QFile(":/ui_file/update.ui")

class MovieGeneral_Window(QWidget):

    open_view_form_signal = pyqtSignal(object)
    handle_text_changed_signal = pyqtSignal(object, object)
    set_window_title_signal = pyqtSignal(unicode)
    save_temp_adding_list_signal = pyqtSignal(str, list)
    save_temp_deleting_list_signal = pyqtSignal(str, list)
    get_current_scoped_session_signal = pyqtSignal()
    interrupt_working_thread_signal = pyqtSignal()
    open_master_data_signal = pyqtSignal(int, bool)

    def __init__(self,
                configuration_saver_dict,
                image_path_collection,
                standard_item_model,
                general_proxy_model,
                app,
                parent,
                movie_id = None):

        QWidget.__init__(self, parent)
        '''
           PARAMETERS
           ==========
           :image_path_collection (dict):               In the given dictionary there a paths to several images.

           :configuration_saver_dict (dict):            We need the saved settings, which they are saved in the
                                                        given dictionary.

           :standard_item_model (QStandardItemModel):   This program based on the MVC-concept, so we need
                                                        the model as a controller.

           :parent (class):                             The class supports passing a parent arguments
                                                        to its base class. Its more felxible. By default,
                                                        the parent is set to None.

           :page_number (int):                          To change the pages. By default
                                                        the page is set to Zero, which means,
                                                        the first page is be shown.

        '''

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        UI_PATH = QFile(":/ui_file/movie_general.ui")

        self._configuration_saver_dict = configuration_saver_dict
        self._image_path_collection = image_path_collection

        self._movie_id = movie_id

        # Create an empty lists
        self._list_threads = []
        self._list_timer = []
        self._list_film_general_company_production = []

        #   Create an instance of imported class.
        self.input_controller = InputController()

        self._standard_item_model = standard_item_model
        self._general_proxy_model = general_proxy_model
        self._app = app

        print "MovieGeneral_Window, self._app", self._app

        self._general_proxy_model.set_sort_filter_proxy_model()

        self.set_temporary_standard_item_model(general_standard_item_model = self._standard_item_model)

        self._current_selected_id = None
        self._selected_record = None

        self.messagebox_called = False
        self.is_focused = None
        self._film_image_url_old = None
        self._film_image_url = None
        self._managed_drag_drop = None
        self._file_extension = None
        self._scoped_session = None

        self._temp_allocation_film_general_general_country_list_adding = []
        self._temp_allocation_film_general_general_country_list_deleting = []

        self._temp_allocation_filmgeneral_filmgenre_list_adding = []
        self._temp_allocation_filmgeneral_filmgenre_list_deleting = []

        # Set all pyqt signals from parent widget

        try:

            parent.send_current_scoped_session_combine_movie_general_details_buttons_form_signal.connect(self.save_scoped_session)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.start_thread_movie_general_signal.connect(
            lambda:
            self.start_threads(
              show_messagebox = True,
              general_proxy_model = self._general_proxy_model,
              general_standard_item_model = self._standard_item_model,
              pyqt_signal = self.get_current_scoped_session_signal,
              category = "all"))

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.set_all_widget_on_default_signal.connect(self.set_all_widget_on_default)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.set_translated_line_edit_focus_in_signal.connect(
            lambda:
            self.set_focus(
              widget_object = self.ui_movie_general.lineEdit_title_translated,
              bool_value = True))

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.set_all_combobox_default_signal.connect(self.set_all_combobox_default)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.get_all_current_value_combobox_signal.connect(self.send_current_combobox_items)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        try:

          parent.quite_threads_signal.connect(self.quite_threads)

        except AttributeError:
            #   Ups, an error has occurred. Maybe the given parent has no  signal like this.
            #   We write this error in the log file.

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)


        self.load_ui_file(UI_PATH)
        self.init_ui()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__


    def load_ui_file(self,
                     UI_PATH):
        '''
           SUMMARY
           =======
           For this class object to remain clear, the UI file is loaded in this method.

           PARAMETERS
           ==========
           :ui_path (QFile):    Expected an argument, that contains a
                                compiled path to the directory where
                                the .*ui-file is stored.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_movie_general = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        self.set_visible_treeWidget_header(find_children = QTreeWidget,
                                           parent_container_widget = self.ui_movie_general.tabWidget_general_information,
                                           widget_index = 0,
                                           bool_value = True)

        return

    def init_ui(self):
        '''
           SUMMARY
           =======
           This method initializes the user interface of master_data-window.

           RETURNS
           =======

           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the user interface")

        self.init_line_edit()
        self.init_timer()

        self.create_popmenu_on_push_button()

        self.init_tree_view()

        self.init_extended_combobox()

        self.init_clicked_signal_push_buttons()
        self.init_textChanged_signal_line_edit()
        self.init_current_index_changed_signal_combobox()
        self.init_activated_signal_combobox()
        self.init_tool_tip()
        self.init_returnPressed_signal_line_edit()

        self._managed_drag_drop = ManagedDragDrop(widget = self.ui_movie_general.label_poster)
        self.ui_movie_general.setAcceptDrops(True)

        return

    def send_current_combobox_items(self):
        '''
           SUMMARY
           =======
           This method, connected with the pyqtSignal(), sends the current items of QComboBox(es),
           when the user opens this form.

           RETURNS
           =======

           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Send current items of QComboBoxes.")

        self.handle_text_changed_signal.emit(self.ui_movie_general.comboBox_genre_evaluation.currentIndex(), 'general_film_genre_evaluation')

        self.handle_text_changed_signal.emit(self.ui_movie_general.comboBox_total_evaluation.currentIndex(), 'general_film_total_evaluation')

        self.handle_text_changed_signal.emit(unicode(self.ui_movie_general.film_sound_system_completer_combobox.currentText()), 'general_film_sound_system')

        self.handle_text_changed_signal.emit(unicode(self.ui_movie_general.film_color_format_completer_combobox.currentText()), 'general_film_colour_format')

        return

    def init_extended_combobox(self):
        '''
           SUMMARY
           =======
           This method is used to Initialize the extended QComboBox().

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the custom QCompleter")

        ###################################################
        #################  Film Soundsystem  ##############
        ###################################################
        self.film_sound_system_completer_combobox = CompleterComboBox(app = self._app,
                                                                      standard_item_model = self._standard_item_model.standard_item_model_film_soundsystem,
                                                                      sort_filter_proxy_model = self._general_proxy_model.proxy_model_film_general_film_soundsystem)

        self.ui_movie_general.horizontalLayout_22.addWidget(self.film_sound_system_completer_combobox)
        self.ui_movie_general.horizontalLayout_22.addWidget(self.ui_movie_general.pushButton_edit_soundsystem)


        ###################################################
        #################  Film Soundsystem  ##############
        ###################################################
        self.film_color_format_completer_combobox = CompleterComboBox(app = self._app,
                                                                      standard_item_model = self._standard_item_model.standard_item_model_film_colour_format,
                                                                      sort_filter_proxy_model = self._general_proxy_model.proxy_model_film_general_film_colour_format)

        self.ui_movie_general.horizontalLayout_9.addWidget(self.film_color_format_completer_combobox)
        self.ui_movie_general.horizontalLayout_9.addWidget(self.ui_movie_general.pushButton_edit_color_format)

        return

    def del_item_in_combo(self, text):
        # currentTextChanged signal will pass selected text to slot
        index = self.combo2.findText(text)  # find the index of text
        self.ui_movie_general.horizontalLayout_22.removeItem(index)  # remove item from index

    def save_scoped_session(self,
                            scoped_session):
        '''
           SUMMARY
           =======
           This method, connected with the pyqtSignal(), ensures that the scoped session is stored class-wide.

           PARAMETERS
           ==========
           :scoped_session (class):    We need the given scoped session, so that we can communicate with the database later.

           RETURNS
           =======

           :return:                   Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Save the given scoped session class wide.")

        self._scoped_session = scoped_session

        return

    def clear_selection(self,
                        widget):
        '''
           SUMMARY
           =======
           This method is used to deselect all selected items in the given QTreeView.

           PARAMETERS
           ==========
           :widget (class):    We need QTreeView to deselect all selected items on that.

           RETURNS
           =======
           :return:            Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                               me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Deselect all selected item")

        widget.selectionModel().clearSelection()

        return

    def change_page(self,
                    stacked_widget,
                    page_index):
        '''
           SUMMARY
           =======
           This function is used to change the view of stacked widget.

           PARAMETERS
           ==========
           :stacked_widget (class): We except a class of QStackedWidget. We need it because
                                    we want to change the index on current QStackedWidget.

           :page_index (int):       To change the index of current stacked widget we also need
                                    an current index.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Change the view of stacked widget")

        stacked_widget.setCurrentIndex(page_index)

        return

    def open_context_menu(self,
                          position,
                          tree_view,
                          pushbutton,
                          pushbutton_caption,
                          icon):
        '''
           SUMMARY
           =======
           This method is used to create Custom context menu for QTreeView.

           PARAMETERS
           ==========
           :position (class):           This method needs QPoint value, representing the position of the menu request in
                                        local given QTreeView coordinates.

           :tree_view (class):          QTreeView is necessary, because the user should be able to right-click
                                        on given QTreeView.

           :pushbutton (class):         We need QPushButton, when the user triggers the QAction, then the signal
                                        clicks on the given QPushButton.

           :pushbutton_caption (class): We need as text that discrptes the QAction. We get it as
                                        QString, because we want to program to translate the givent caption.

           :icon (str):                 To set icon on QAction we need the path to that icon.

           RETURNS
           =======
           :return:                     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Open the context menu on given QTreeView")

        indexes = tree_view.selectedIndexes()

        if len(indexes) > 0:

            pushbutton_action = QAction(QIcon(icon), pushbutton_caption, self)
            pushbutton_action.setObjectName("pushbutton_action_master_data")
            pushbutton_action.triggered.connect(pushbutton.click)

            menu = QMenu()
            menu.setObjectName("context_menu_master_data")
            menu.addAction(pushbutton_action)

            menu.exec_(tree_view.viewport().mapToGlobal(position))

        return

    def set_temporary_standard_item_model(self,
                                         general_standard_item_model):
        '''
           SUMMARY
           =======
           This method is used to setup QStandardItemModel temporary.

           PARAMETERS
           ==========
           :general_standard_item_model (class):  We need the general model as instance to access
                                                  certain methods. In this case, we want the method
                                                  to set up all QStandardItemModel temporary.

           RETURNS
           =======
           :return:                               Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set up all QStandardItemModel for temporary")

        general_standard_item_model.set_temporary_standard_item_model()

        return

    def apped_row_to_model(self,
                           category,
                           tree_view_widget,
                           record_tuple,
                           columns,
                           general_standard_item_model,
                           certain_standard_item_model,
                           adding_temp_list,
                           parent):
        logger.info('\n')
        logger.info("MovieGeneral_Window apped_row_to_model is calles successfully")
        logger.info("MovieGeneral_Window in apped_row_to_model / tree_view_widget.objectName(): --> \n {}".format(tree_view_widget.objectName()))
        logger.info("MovieGeneral_Window in apped_row_to_model / general_standard_item_model.objectName(): --> \n {}".format(general_standard_item_model))
        logger.info("MovieGeneral_Window in apped_row_to_model / certain_standard_item_model.objectName(): --> \n {}".format(certain_standard_item_model.objectName()))
        logger.info('\n')
        '''
           SUMMARY
           =======
           This function is used to add row from model.

           EXTENTED DISCRIPTION
           ====================
           This function not only removes row from model, manages the given lists,
           cleares the selection of given QTreeView and hides the corresponding columns
           of given QTreeView and last but not least, its deletes the current selected id.

           PARAMETERS
           ==========
           :category (str):           The category says us which list should be saved.

           :tree_view_widget (class): We need the QTreeView for the clear_selection()-method
                                      and set_hide_column()-method later.

           :record_tuple (tuple):     Here we except a pair of id and record for manging given
                                      model and lists.

           :general_model (class):    Its important that we get just a general model (QStandardItemModel) - without specification.
                                      This model is for accesing to the given general model.

           :certain_model (class):    And now we need the specific model (QStandardItemModel). This models let us know which model
                                      should be managed.

           :adding_temp_list (list):  This list contains all the records that we want to save to the database later.

           RETURNS
           =======
           :return:                   Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Add the given row from given model")

        try:

            result = general_standard_item_model.find_text_model(
              list_tuple = record_tuple,
              model = certain_standard_item_model)

            if not result:

                general_standard_item_model.populate_model(
                  model = certain_standard_item_model,
                  tuple_items = record_tuple)

                adding_temp_list.append(record_tuple)

                self.save_temp_adding_list_signal.emit(category, adding_temp_list)

                self._current_selected_id = None

                self.clear_selection(widget = tree_view_widget)

            self.set_hide_column(column = 0,
                                 bool_value = True,
                                 tree_view_widget = tree_view_widget)

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def remove_row_from_model(self,
                              category,
                              tree_view_widget,
                              record_tuple,
                              general_standard_item_model,
                              certain_standard_item_model,
                              adding_temp_list,
                              deleting_temp_list):
        '''
           SUMMARY
           =======
           This function is used to remove row from model.

           EXTENTED DISCRIPTION
           ====================
           This function not only removes row from model, manages the given lists,
           cleares the selection of given QTreeView and hides the corresponding columns
           of given QTreeView and last but not least, its deletes the current selected id.

           PARAMETERS
           ==========
           :tree_view_widget (class):   We need the QTreeView for the clear_selection()-method
                                        and set_hide_column()-method later.

           :record_tuple (tuple):       Here we except a pair of id and record for manging given
                                        model and lists.

           :general_model (class):      Its important that we get just a general model (QStandardItemModel)
                                        - without specification. This model is for accesing to the given general model.

           :certain_model (class):      And now we need the specific model (QStandardItemModel).
                                        This models let us know which model should be managed.

           :adding_temp_list (list):    This list contains all the records that we want to save to the database later.

           :deleting_temp_list (list):  This list contains all the records that we want to delete from the database later.

           RETURNS
           =======
           :return:                     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Remove the given row from given model")

        try:

            if not record_tuple[0] is None:

                general_standard_item_model.remove_row(
                  standard_item_model = certain_standard_item_model,
                  item_id = record_tuple[0])

                # We also have to update the list with a slice operator.
                adding_temp_list[:] = remove_tuple_from_list(list_tuple = adding_temp_list,
                                                             delete_element = record_tuple[0])

                # We also have to add the element we want to delete it later
                deleting_temp_list.append(record_tuple)

                self.save_temp_adding_list_signal.emit(category, adding_temp_list)
                self.save_temp_deleting_list_signal.emit(category, deleting_temp_list)

            self._current_selected_id = None

            self.clear_selection(widget = tree_view_widget)

            self.set_hide_column(column = 0,
                                 bool_value = True,
                                 tree_view_widget = tree_view_widget)

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def init_current_index_changed_signal_combobox (self):
        '''
           SUMMARY
           =======
           This method is used to set the connection for pyqtSignal with QComboBox by using
           the currentIndexChanged()-signal.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize connection for pyqtSignal with QComboBox by using currentIndexChanged()-signal")

        self.ui_movie_general.comboBox_genre_evaluation.currentIndexChanged.connect(lambda int_value:
          self.handle_text_changed_signal.emit(int_value, 'general_film_genre_evaluation'))

        self.ui_movie_general.comboBox_total_evaluation.currentIndexChanged.connect(lambda int_value:
          self.handle_text_changed_signal.emit(int_value, 'general_film_total_evaluation'))

        self.ui_movie_general.film_sound_system_completer_combobox.currentIndexChanged[unicode].connect(lambda item :
          self.handle_text_changed_signal.emit(unicode(item), 'general_film_sound_system'))

        self.ui_movie_general.film_color_format_completer_combobox.currentIndexChanged[unicode].connect(lambda current_text:
          self.handle_text_changed_signal.emit(unicode(current_text), 'general_film_colour_format'))

        self.ui_movie_general.film_color_format_completer_combobox.currentIndexChanged[unicode].connect(lambda current_text:
          self.handle_text_changed_signal.emit(unicode(current_text), 'general_film_colour_format'))

        return

    def init_activated_signal_combobox(self):

        self.ui_movie_general.comboBox_genre_evaluation.activated .connect(lambda int_value:
          self.handle_text_changed_signal.emit(int_value, 'general_film_genre_evaluation'))

        self.ui_movie_general.comboBox_total_evaluation.activated .connect(lambda int_value:
          self.handle_text_changed_signal.emit(int_value, 'general_film_total_evaluation'))

        self.ui_movie_general.film_sound_system_completer_combobox.activated [unicode].connect(lambda item :
          self.handle_text_changed_signal.emit(unicode(item), 'general_film_sound_system'))

        self.ui_movie_general.film_color_format_completer_combobox.activated [unicode].connect(lambda current_text:
          self.handle_text_changed_signal.emit(unicode(current_text), 'general_film_colour_format'))

        '''
           SUMMARY
           =======
           This method is used to set the connection for pyqtSignal with QComboBox by using
           the currentIndexChanged()-signal.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize connection for pyqtSignal with QComboBox by using activated ()-signal")

        return

    def create_wait_window(self,
                           text):
        '''
           SUMMARY
           =======
           This method just creates the wait window.

           PARAMETERS
           ==========
           :text (str):    The given text should be shown on the wait window.

           RETURNS
           =======
           :return:         Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create the wait window")

        self.waiting_window = Waiting_Window(text_message = text)
        self.waiting_window.show()

        return

    def close_wait_message(self):
        '''
           SUMMARY
           =======
           This method just closes the created wait window.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("closed the created wait window")

        try:

            self.waiting_window.close()

        except AttributeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

        return

    def set_focus(self,
                  widget_object,
                  bool_value = True):

        '''
           SUMMARY
           =======
           This method is used to set the focus on given QLineEdit()-object.

           EXTENTED DISCRIPTION
           ====================
           This method is necessary, because the setFocus()-function is used
           in several places.

           PARAMETERS
           ==========
           :widget_object (class):  We need the QLineEdit()-object to set the focus in.

           :bool_value (bool):      By default, its on True, which menas, the given Qt-object
                                    will be visible.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the focus on given QLineEdit()-object.")

        if not widget_object is None:
            widget_object.setFocus(bool_value)

        return

    def create_generally_crritcal_msg(self,
                                      err_title,
                                      err_msg,
                                      set_flag = False,
                                      detail_msg = None,
                                      icon = None):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for critical.

           PARAMETERS
           ==========
           :err_title (class):    We except a text (Qstring) for the title bar of the message box.

           :err_msg (class) :     We need the text (Qstring) for the main message to be displayed.

           :detail_msg (class):   When we get a detailed text (Qstring), the hide/show details" button will be added.

           :icon (str):           For better understanding, we should show an icon on message box by using path to icon file.

        '''
        logger.info("Create a message box for critical situation")

        mBox = custom_message_box(self,
                                  text = err_msg,
                                  title = err_title,
                                  detail_text = detail_msg,
                                  icon = icon,
                                  set_flag = set_flag)

        mBox.addButton(
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_information_msg(self,
                                         title,
                                         msg ,
                                         detail_msg ,
                                         icon,
                                         line_edit_object_get_focus,
                                         set_flag = False,
                                         yes_no_button = False,
                                         only_yes_button = False):
        '''
           SUMMARY
           =======
           This method creates a MessageBox for information.

           PARAMETERS
           ==========
           :title (str):                        This methods excepts a text for the title bar of the message box.

           :msg (str) :                         We also need the text for the main message to be displayed.

           :detail_msg (str):                   When we get a detailed text, the hide/show details" button will be added.

           :icon (str):                         A icon path to display that on message box.

           :line_edit_object_get_focus (class): By closing the message box the given QLineEdit() get focus in.

           :yes_no_button (bool):               By default its False. We don't show yes and no button, otherwise we show both.

           :only_yes_button (bool):             By defaul, its False, we don't show onlly yes button, otherwise we show it.

        '''
        logger.info("Create a message box for information")

        if not self.messagebox_called:

            if yes_no_button:

                yes_no_msg_box = custom_message_box(self, text = msg,
                                                          title = title,
                                                          icon = icon,
                                                          set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_no_msg_box.setInformativeText(detail_msg)

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Yes")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                yes_no_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("No")),
                    #QPushButton(self.tr(" No")),
                    yes_no_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole

                result_button = yes_no_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window
                # execution and allows a developer to wait for user
                # input before continuing.

                # 1 means No
                if result_button == 1:
                    self.close()
                # 0 means Yes
                elif result_button == 0:
                    self._image_path = None
                    self.pixmap = None
                    self.ui_person_profile.label_photo.clear()
                    self.set_all_widget_on_default()
                    self.quite_threads()

            if only_yes_button:

                yes_msg_box = custom_message_box(self,
                                                  text = msg,
                                                  title = title,
                                                  icon = icon,
                                                  set_flag = set_flag)
                #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
                #yes_no_msg_box.setDetailedText(detail_msg)
                yes_msg_box.setInformativeText(detail_msg)

                yes_msg_box.addButton(
                    #QPushButton(setIc_OK, self.tr(" Ok")),
                    QPushButton(self.tr("Ok")),
                    #QPushButton(self.tr(" No")),
                    yes_msg_box.AcceptRole) # An "OK" button defined with the AcceptRole
                yes_msg_box.exec_()    # QDialog.exec_ is the way that it blocks window

                self.set_focus(widget_object = line_edit_object_get_focus, bool_value = True)

    def update_proxy_model(self,
                           **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Update the certain proxy model")

        #self.quite_threads()

        self.task_thread = QThread()
        self.task_thread.work = TaskProxyModel(parent = None,
                                              **kwargs)

        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        # self.task_thread.work.notify_item.connect(lambda standard_item:
        #     general_proxy_model.set_source_model(model = standard_item))

        # self.task_thread.work.notify_progress.connect(lambda certain_proxy_model, category:
        #     general_proxy_model.refresh_proxy_model(widget_tree_view = widget_tree_view,
        #                                             certain_proxy_model = certain_proxy_model,
        #                                             category = category,
        #                                             call_back_init_selectionChanged_signal_treeview = self.init_selectionChanged_signal_treeview,
        #                                             call_back_close_wait_message = self.close_wait_message))

        self.task_thread.work.Set_model_on_treeview_signal.connect(
          lambda kwargs:
          self.Set_model_on_treeview(**kwargs))


        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_work)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def set_hide_column(self,
                        column,
                        bool_value,
                        list_tree_view_widget = None,
                        tree_view_widget = None):
        '''
           SUMMARY
           =======
           The given column will be hidden, otherwise it will be shown

           PARAMETERS
           ==========
           :list_tree_view_widget (list):   This argument contains a list of QTreeView()-objects

           :tree_view_widget (class):       It contains a single QTreeView()-object

           :column (int):                   We need a number to hide a special column.

           :bool_value (bool):              Boolean to hide, when it is True.

           RETURNS
           =======
           :return:                         Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                            me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set column hidden on given Qt-object")

        if not tree_view_widget is None:
            tree_view_widget.setColumnHidden(column, bool_value)

        if isinstance(list_tree_view_widget, list):
            for tree_view in list_tree_view_widget:
                tree_view.setColumnHidden(column, bool_value)

        return

    def filter_update(self,
                      **kwargs):
        '''
           SUMMARY
           =======
           This method starts a thread to update certain proxy model.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
        '''
        #self.quite_threads()
        kwargs['image_path_collection'] = self._image_path_collection

        self.task_thread = QThread()
        self.task_thread.work = TaskFilterUpdate(parent = None,
                                                **kwargs)

        self._list_threads.append(self.task_thread)

        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message)

        self.task_thread.work.create_generally_information_msg_signal.connect(
          lambda
          title,
          msg,
          detail_msg,
          yes_no_button,
          only_yes_button,
          line_edit_object_get_focus,
          icon,
          set_flag:
          self.create_generally_information_msg(
            title = title,
            msg = msg,
            detail_msg = detail_msg,
            yes_no_button = yes_no_button,
            only_yes_button = only_yes_button,
            line_edit_object_get_focus = line_edit_object_get_focus,
            icon = icon,
            set_flag = set_flag))

        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        self.task_thread.started.connect(self.task_thread.work.start_thread)

        self.task_thread.finished.connect(self.task_thread.deleteLater)

        self.task_thread.start()

        return

    def handle_timer(self,
                     timer_widget,
                     text,
                     show_wait_message = False):
        '''
           SUMMARY
           =======
           TThis method is used to handle the given QTimer.

           EXTENTED DISCRIPTION
           ====================
           This function not only handles given QTimer, but also
           creates another waiting-window.

           PARAMETERS
           ==========
           :timer_widget (class):       selectionChanged() sends new QItemSelection.

           :text (class):               We get a QString, because the given text will be translated.

           :show_wait_message (bool):   Let us know should we show the wait-window?

           RETURNS
           =======
           :return:                     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Handle the certain QTimer")

        if show_wait_message:
            self.create_wait_window(text = text)

        timer_widget.stop()
        timer_widget.start()

        return

    def handle_selection_changed(self,
                                 new_index,
                                 old_index,
                                 numbers_columns):
        '''
           SUMMARY
           =======
           TThis method is used to get selected item.

           PARAMETERS
           ==========
           :new_index (class):      selectionChanged() sends new QItemSelection.

           :old_index (class):      selectionChanged() sends old QItemSelection.

           :numbers_columns (int):  We need to know how many coulmns should be accessed.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Get the selected item")

        try: #if QItemSelection
            #   We need access to special date of selected item#.
            #   By using indexes() with the number in square brackets we can
            #   extract it. With data() we automatically convert a QVariant back to a
            #   Python object

            self.selected_be_edited_id = int(new_index.indexes()[0].data()) #    We want the selected ID of the record

            self._current_selected_id = int(new_index.indexes()[0].data()) #    We want the selected ID of the record

            if numbers_columns == 2:
                self._selected_record = unicode(new_index.indexes()[1].data()) # We want fill the given varable with record.

        except Exception as er: #if qModelIndex

            pass

        return

    def manage_standart_item_model(self,
                                   general_model,
                                   category,
                                   columns):
        '''
           SUMMARY
           =======
           This method set a certain QStandardItemModel.

           PARAMETERS
           ==========
           :general_model:  We need the instantiated model of QStandardItemModel for
                            accessing certain methods.

           :category (str): There are several QStandardItemModel, which of one should be set up?

           :columns (int):  We need a coumnn for setting a QStandardItemModel:

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set up a certain QStandardItemModel")

        general_model.standard_item_model(category = category, rows = 0, columns = columns, parent = self)

        return

    def fetch_all(self,
                  scoped_session,
                  general_standard_item_model,
                  certain_standard_item_model,
                  certain_proxy_model,
                  general_proxy_model,
                  show_messagebox = True,
                  hide_column = False,
                  list_header = None,
                  work_mode = None,
                  bulk_the_list = None,
                  notify_on_widget = None,
                  widget_label = None,
                  widget_tree_view = None,
                  widget_line_edit = None,
                  widget_stack_widget = None,
                  set_page = False,
                  page_index = None,
                  sort_column = 0,
                  category = None):
        '''
           SUMMARY
           =======
           This method is used to start each thread to be fetching all data.

           EXTENTED DISCRIPTION
           ====================
           First we create a new QThread()-object, second we have to create another
           instance of a class that inherits from QObject. Third, we move the created
           instance of QObject in the first created QThread()-object. Fourth all signals
           are connected to methods and the thread-process can begin.

           PARAMETERS
           ==========
           :scoped_session (class):               The scoped session is important. This allows us to communicate with the database.

           :general_standard_item_model (class):  We need the given general model (QStandardItemModel) for accessing other
                                                  specific models later.

           :certain_standard_item_model (class):  We need the given certain model (QStandardItemModel) for populating it.

           :certain_proxy_model (class):          We get the certain model for updating it.

           :general_proxy_model (class):          We also need the instantiated proxy model for accessing to certain
                                                  methods.

           :show_messagebox (bool):               Should the message box be displayed? Its true it will be displaed,
                                                  it will be not displaed

           :hide_column (bool):                   Should the column of QTreeView() be hidden? Its on True it will be hidden,
                                                  otherwise it will be not hidden.

           :list_header (list):                   We expect a list to name the heading of the tree column

           :work_mode (str):                      This argument lets us know what we should do.

           :bulk_the_list (list):                 If we have to save a large number of records, we expect a list
                                                  of all records.

           :notify_on_widget:                     In this argument we know on which widget the notification
                                                  should be issued - for example, during an update process.

           :widget_label (class):                 On given QLabel, we show the counter.

           :widget_tree_view (class):             All populated records should be shown on QTreeView.

           :widget_line_edit (class):             The given QLineEdit gets the focus in. QlineEdit

           :widget_stack_widget (class):          For example, the user wants to edit the records so he has to
                                                  change the page, and then go back to the last page. QStackedWidget

           :set_page (bool):                      By default, its False. No page on stacked widget should be changed. Otherwise
                                                  it changes it.

           :page_index (int):                     We get the index of the page we want to change to.

           :sort_column (int):                    Which column should be sorted?

           :category (str):                       Say what category we are using in.

           RETURNS
           =======
           :return:                               Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start a thread for fetching all records from database.")

        #self._proxy_model = None
        #   First, before we start a thread, we have to quite all running threads.
        #self.interrupt_working_thread_signal.emit()

        #self.quite_threads()

        # #   Second, we try to remove the busy progressbar
        # self.delete_widget_from_layout()

        #   Third we have to set a proxy model for filtering
        general_proxy_model.set_sort_filter_proxy_model()

        # if work_mode == "fetch_all": #   not equal
        #     #   Foruth we needd a item model with columns and rows for QTreeView
        #     self.init_custom_progres_bar()

        #   Fith we have to delete all selected IDs they are saved in the attributes
        # self.remove_saved_record_id(record_id = self.current_selected_id)
        # self.remove_saved_record_id(record_id = self.selected_be_edited_id)

        self.current_selected_id = None
        self.selected_be_edited_id = None


        # if not notify_on_widget is None:
        #     #   When given argument is NOT None, we have to
        #     #   append text to given widget.
        #     self.append_text(widget = notify_on_widget,
        #                      clear_widget = True)

        #   We create QThread() as container.
        self.task_thread = QThread()

        #   Workaround - we bind the `work_object` - namend TaskDataManipulation() -
        #   to an attribute of the thread object, named self.task_thread,
        #   since it belongs semantically as well. It is then cleared after
        #   thread end with the `deleteLater`.
        self.task_thread.work = TaskDataManipulation(scoped_session = scoped_session,
                                                     category = category,
                                                     work_mode = work_mode,
                                                     bulk_the_list = bulk_the_list,
                                                     configuration_saver_dict = self._configuration_saver_dict,
                                                     certain_standard_item_model = certain_standard_item_model,
                                                     general_standard_item_model = general_standard_item_model,
                                                     general_proxy_model = general_proxy_model,
                                                     certain_proxy_model = certain_proxy_model,
                                                     show_messagebox = show_messagebox,
                                                     widget_tree_view = widget_tree_view,
                                                     hide_column = hide_column,
                                                     populate_timer_interval = 1000)

        #   The current created QThread object is saved to a list.
        self._list_threads.append(self.task_thread)

        #   TaskDataManipulation() is a separate class (we are talking about
        #   (Subclassing QObject), which inherits from QObject().
        #   So we have to use moveToThread(). In this case we change the
        #   thread affinity of TaskDataManipulation()
        self.task_thread.work.moveToThread(self.task_thread)

        self.task_thread.work.populate_proxy_model_signal.connect(lambda kwargs:
                                                             self.update_proxy_model(**kwargs))

        # self.task_thread.work.close_wait_message_signal.connect(self.close_wait_message)

        # if not widget_label is None:
        #     self.task_thread.work.fire_label.connect(lambda count_value:
        #         self.display_counting(widget = widget_label,
        #                               count = count_value))

        # if set_page:
        #     self.task_thread.work.change_page_signal.connect(lambda:
        #         self.change_page(index = page_index,
        #                          stack_widget_object = widget_stack_widget,
        #                          clear_widget = True,
        #                          current_obj_clear_selection = True,
        #                          delete_selected_id = True,
        #                          #load_data = True,
        #                          current_obj_clear = True))


        # self.task_thread.work.count_progress_signal.connect(lambda value:
        #                                            self.set_maximum_value(value = value))

        # if work_mode == "fetch_all":

            # self.task_thread.work.delete_widget_signal.connect(self.delete_widget_from_layout)

            # self.task_thread.work.update_progress_bar_signal.connect(lambda process_value, row_count:
            #     self.update_progress_bar(process_value = process_value,
            #                              progress_bar_object = self.busy_progress_bar,
            #                              final_count = row_count))

        # if work_mode != "fetch_all":

        #     self.task_thread.work.sort_model_signal.connect(lambda:
        #         self.task_model(model = model,
        #                         column = sort_column,
        #                         line_edit_object_get_focus = widget_line_edit,
        #                         change_page = True,
        #                         stack_widget_object = widget_stack_widget,
        #                         page_index = page_index,
        #                         clear_widget = True,
        #                         current_obj_clear_selection = True,
        #                         delete_selected_id = True,
        #                         current_obj_clear = True,
        #                         category = category))

        #     self.task_thread.work.create_wait_window_signal.connect(lambda text:
        #         self.create_wait_window(text = text))

        self.task_thread.work.hide_column_signal.connect(lambda widget_tree_view:
                                                    self.set_hide_column(tree_view_widget = widget_tree_view,
                                                                         column = 0,
                                                                         bool_value = True))


        # self.task_thread.work.count_model_items_signal.connect(lambda:
        #     self.count_model_items(widget = widget_label,
        #                            standard_item_model = certain_standard_item_model))

        # self.task_thread.work.create_wait_window_signal.connect(lambda text:
        #     self.create_wait_window(text = text))

        # self.task_thread.work.text_notify_update_signal.connect(lambda text:
        #                                                    self.append_text(widget = notify_on_widget,
        #                                                                     clear_widget = False,
        #                                                                     text = text))

        # if show_messagebox:

        self.task_thread.work.confirm_saving_successful_signal.connect(lambda msg_text, detail_msg:
            self.create_generally_information_msg(title = self.tr('Confirmation'),
                                                  msg = self.tr(msg_text),
                                                  detail_msg = detail_msg,
                                                  yes_no_button = False,
                                                  line_edit_object_get_focus = widget_line_edit,
                                                  only_yes_button = True,
                                                  icon = self._image_path_collection.info_48x48_default,
                                                  set_flag = True))

        self.task_thread.work.error_message_signal.connect(lambda title_text, conn_err, detail_msg:
            self.create_generally_crritcal_msg(err_title = title_text,
                                               err_msg = conn_err,
                                               detail_msg = detail_msg,
                                               icon = self._image_path_collection.warning_48x48_default,
                                               set_flag = True))


        # self.interrupt_working_thread_signal.connect(self.task_thread.work.abort_task_thread)

        # #   We want the thread to stop after the worker (TaskDataManipulation) is done,
        # #   on this way we can always call thread.start() again later. Also the intention is
        # #   for  quit() to be executed before deleteLater().
        self.task_thread.work.quit_thread_signal.connect(self.task_thread.quit)
        self.task_thread.work.wait_thread_signal.connect(self.task_thread.wait)

        # self.task_thread.work.notify_item.connect(lambda tuple_items:
        #                                      self._standard_item_model.populate_model(tuple_items = tuple_items, model = certain_standard_item_model))


        #   This signal is emitted when the thread starts executing.
        self.task_thread.started.connect(self.task_thread.work.start_work)

        #   The clean-up: when the worker instance emits finished() it will signal the thread
        #   to quit, i.e. shut down. We then mark the worker instance using the same finished()
        #   signal for deletion. Finally, to prevent nasty crashes because the thread hasn’t fully
        #   shut down yet when it is deleted, we connect the finished() of the thread
        #   (not the worker!) to its own deleteLater() slot. T
        #   his will cause the thread to be deleted only after it has fully shut down.
        self.task_thread.finished.connect(self.task_thread.deleteLater)

        #   Finally let us start the QThread
        self.task_thread.start()

        return

    def create_popmenu_on_push_button(self):
        '''
           SUMMARY
           =======
           This method creates a popmenu on a Qpushbutton(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Create popup menu for QPushButton")

        #   First we save the menu item labels in the variables, because
        #   this label will often be needed.
        edit_menu_item = self.tr("Edit")
        reload_menu_item = self.tr("Reload")

        #   Second we have to create a QMenu.
        menu_general_country = QMenu()
        menu_film_genre = QMenu()
        menu_film_colour_format = QMenu()
        menu_film_soundsystem = QMenu()

        #   addAction that will add menu items to your newly created QMenu.
        #   The actions are created by calling addAction and giving.
        #   Its a string that will be used as the menu item
        #   text and a method that will be called if that menu item is selected.

        ###################################################
        #################  General Country  ###############
        ###################################################
        edit_general_country_action = menu_general_country.addAction(QIcon(self._image_path_collection.edit_default), edit_menu_item)
        edit_general_country_action.triggered.connect(lambda:
          self.open_master_data_signal.emit(60, True))

        reload_general_country_action = menu_general_country.addAction(QIcon(self._image_path_collection.refresh_default), reload_menu_item)
        reload_general_country_action.setObjectName("reload_general_country_action")
        reload_general_country_action.triggered.connect(lambda:
           self.start_threads(pyqt_signal = self.get_current_scoped_session_signal,
                              general_proxy_model = self._general_proxy_model,
                              general_standard_item_model = self._standard_item_model,
                              category = "fetch_all_general_country",
                              show_messagebox = True))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        edit_film_genre_action = menu_film_genre.addAction(QIcon(self._image_path_collection.edit_default), edit_menu_item)
        edit_film_genre_action.triggered.connect(lambda:
          self.open_master_data_signal.emit(2, True))

        reload_film_genre_action = menu_film_genre.addAction(QIcon(self._image_path_collection.refresh_default), reload_menu_item)
        reload_film_genre_action.triggered.connect(lambda:
          self.start_threads(pyqt_signal = self.get_current_scoped_session_signal,
                             general_proxy_model = self._general_proxy_model,
                             general_standard_item_model = self._standard_item_model,
                             category = "fetch_all_film_genre",
                             show_messagebox = True))

        ###################################################
        #################  Film Colour Format  ############
        ###################################################

        edit_film_colour_format_action = menu_film_colour_format.addAction(QIcon(self._image_path_collection.edit_default), edit_menu_item)
        edit_film_colour_format_action.triggered.connect(lambda:
          self.open_master_data_signal.emit(7, True))

        reload_film_colour_format_action = menu_film_colour_format.addAction(QIcon(self._image_path_collection.refresh_default), reload_menu_item)
        reload_film_colour_format_action.triggered.connect(lambda:
          self.start_threads(pyqt_signal = self.get_current_scoped_session_signal,
                             general_proxy_model = self._general_proxy_model,
                             general_standard_item_model = self._standard_item_model,
                             category = "fetch_all_film_colour_format",
                             show_messagebox = True))

        ###################################################
        #################  Film soundsystem  ##############
        ###################################################

        edit_film_soundsystem_action = menu_film_soundsystem.addAction(QIcon(self._image_path_collection.edit_default), edit_menu_item)
        edit_film_soundsystem_action.triggered.connect(lambda:
          self.open_master_data_signal.emit(2, True))

        reload_film_soundsystem_action = menu_film_soundsystem.addAction(QIcon(self._image_path_collection.refresh_default), reload_menu_item)
        reload_film_soundsystem_action.triggered.connect(lambda:
          self.start_threads(pyqt_signal = self.get_current_scoped_session_signal,
                             general_proxy_model = self._general_proxy_model,
                             general_standard_item_model = self._standard_item_model,
                             category = "fetch_all_film_soundsystem",
                             show_messagebox = True))

        #   Last but not least, we use the setMenu method to attach it to our push button.
        self.ui_movie_general.pushButton_setting_general_film_film_genre.setMenu(menu_film_genre)
        self.ui_movie_general.pushButton_setting_general_film_production_country.setMenu(menu_general_country)
        self.ui_movie_general.pushButton_edit_color_format.setMenu(menu_film_colour_format)
        self.ui_movie_general.pushButton_edit_soundsystem.setMenu(menu_film_soundsystem)

        return

    def start_threads(self,
                     **kwargs):
        '''
           SUMMARY
           =======
           This method starts all saved thrads or a certain saved threads.

           EXTENTED DISCRIPTION
           ====================
           This function not only starts threads, but also tries to get current
           scoped session for communicating with database.

           RETURNS
           =======
           :return:   Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                      me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start all or certain saved thread")

        kwargs.get('pyqt_signal').emit()

        dict_fetch_all_threads = {
            ###################################################
            #################  General Country  ###############
            ###################################################
            "fetch_all_general_country": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                certain_proxy_model = kwargs.get('general_proxy_model').proxy_model_film_general_general_country,
                                                                general_proxy_model = kwargs.get('general_proxy_model'),
                                                                general_standard_item_model = kwargs.get('general_standard_item_model'),
                                                                work_mode = 'fetch_all',
                                                                certain_standard_item_model = kwargs.get('general_standard_item_model').standard_item_model_general_country,
                                                                widget_tree_view = self.ui_movie_general.treeView_show_film_general_production_country,
                                                                show_messagebox = kwargs.get('show_messagebox', True),
                                                                hide_column = True,
                                                                category = 'general_country'),
          ###################################################
          #################  Film Genre  ####################
          ###################################################
            "fetch_all_film_genre": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                           certain_proxy_model = kwargs.get('general_proxy_model').proxy_model_film_general_film_genre,
                                                           general_proxy_model = kwargs.get('general_proxy_model'),
                                                           general_standard_item_model = kwargs.get('general_standard_item_model'),
                                                           work_mode = 'fetch_all',
                                                           certain_standard_item_model = kwargs.get('general_standard_item_model').standard_item_model_film_genre,
                                                           widget_tree_view = self.ui_movie_general.treeView_show_film_general_film_genre,
                                                           show_messagebox = kwargs.get('show_messagebox', True),
                                                           hide_column = True,
                                                           category = 'film_genre'),

          ###################################################
          #################  Film Colour Format  ############
          ###################################################
            "fetch_all_film_colour_format": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                   certain_proxy_model = kwargs.get('general_proxy_model').proxy_model_film_general_film_colour_format,
                                                                   general_proxy_model = kwargs.get('general_proxy_model'),
                                                                   general_standard_item_model = kwargs.get('general_standard_item_model'),
                                                                   work_mode = 'fetch_all',
                                                                   certain_standard_item_model = kwargs.get('general_standard_item_model').standard_item_model_film_colour_format,
                                                                   #widget_tree_view = self.ui_movie_general.tree_view_color_format,
                                                                   show_messagebox = kwargs.get('show_messagebox', True),
                                                                   hide_column = True,
                                                                   category = 'film_colour_format'),

          ###################################################
          #################  Film Soundsystem  ##############
          ###################################################
            "fetch_all_film_soundsystem": lambda: self.fetch_all(scoped_session = self._scoped_session,
                                                                 certain_proxy_model = kwargs.get('general_proxy_model').proxy_model_film_general_film_soundsystem,
                                                                 general_proxy_model = kwargs.get('general_proxy_model'),
                                                                 general_standard_item_model = kwargs.get('general_standard_item_model'),
                                                                 work_mode = 'fetch_all',
                                                                 certain_standard_item_model = kwargs.get('general_standard_item_model').standard_item_model_film_soundsystem,
                                                                 #widget_tree_view = self.ui_movie_general.treeView_show_film_general_film_genre,
                                                                 show_messagebox = kwargs.get('show_messagebox', True),
                                                                 hide_column = True,
                                                                 category = 'film_soundsystem')

                                  }


        if not kwargs.get('category') == "all":

            dict_fetch_all_threads[category]()

        elif kwargs.get('category') == "all":

            for _, value in dict_fetch_all_threads.iteritems():

                value()

        return

    def init_selectionChanged_signal_treeview(self):
        '''
           SUMMARY
           =======
           We need to initialize the selectionChanged-signals for QTreeView .

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize selectionChanged-signal for QTreeView")

        ###################################################
        #################  General Region  ################
        ###################################################
        self.ui_movie_general.treeView_show_film_general_production_country.selectionModel().selectionChanged.connect(lambda new_index, old_index:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, numbers_columns = 2))
        self.ui_movie_general.treeView_current_film_general_production_country.selectionModel().selectionChanged.connect(lambda new_index, old_index:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, numbers_columns = 2))


        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_movie_general.treeView_show_film_general_film_genre.selectionModel().selectionChanged.connect(lambda new_index, old_index:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, numbers_columns = 2))
        self.ui_movie_general.treeView_current_film_general_film_genre.selectionModel().selectionChanged.connect(lambda new_index, old_index:
            self.handle_selection_changed(new_index = new_index, old_index = old_index, numbers_columns = 2))

        return

    def init_timer(self):
        '''
           SUMMARY
           =======
           This method initializes all QTimer(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize all QTimer(s)")

        ###################################################
        #################  General Country  ###############
        ###################################################

        self.update_proxy_model_timer = QTimer()
        self.update_proxy_model_timer.setSingleShot(True)
        self.update_proxy_model_timer.setInterval(1)
        self.update_proxy_model_timer.timeout.connect(lambda:
        self.update_proxy_model(
          widget_tree_view = self.ui_movie_general.treeView_current_film_general_production_country,
          certain_standard_item_model = self._standard_item_model.temporary_standard_item_model_film_general_film_production_company,
          general_proxy_model = self._general_proxy_model,
          certain_proxy_model = self._general_proxy_model.temporary_proxy_model_film_general_general_country,
          category = "film_production_company"))
        self.update_proxy_model_timer.start()


        self.search_delay_timer_film_general_production_country_show = QTimer()
        self.search_delay_timer_film_general_production_country_show.setSingleShot(True)
        self.search_delay_timer_film_general_production_country_show.setInterval(1000)
        self.search_delay_timer_film_general_production_country_show.timeout.connect(lambda:
          self.filter_update(line_edit_widget = self.ui_movie_general.lineEdit_search_film_general_company_production_show,
                              certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
                              column = 2))

        self.search_delay_timer_film_general_production_country_edit = QTimer()
        self.search_delay_timer_film_general_production_country_edit.setSingleShot(True)
        self.search_delay_timer_film_general_production_country_edit.setInterval(1000)
        self.search_delay_timer_film_general_production_country_edit.timeout.connect(lambda:
          self.filter_update(line_edit_widget = self.ui_movie_general.lineEdit_search_film_general_company_production_edit,
                              certain_proxy_model = self._general_proxy_model.temporary_proxy_model_film_general_general_country,
                              column = 2))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.search_delay_timer_film_general_film_genre = QTimer()
        self.search_delay_timer_film_general_film_genre.setSingleShot(True)
        self.search_delay_timer_film_general_film_genre.setInterval(1000)
        self.search_delay_timer_film_general_film_genre.timeout.connect(lambda:
          self.filter_update(line_edit_widget = self.ui_movie_general.lineEdit_search_film_general_film_genre,
                              certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                              column = 2))

        self.timer_start_threads = QTimer()
        self.timer_start_threads.setSingleShot(True)
        self.timer_start_threads.setInterval(5)
        self.timer_start_threads.timeout.connect(lambda:
          self.start_threads(show_messagebox = False,
                             general_proxy_model = self._general_proxy_model,
                             general_standard_item_model = self._standard_item_model,
                             pyqt_signal = self.get_current_scoped_session_signal,
                             category = "all"))
        self._list_timer.append(self.timer_start_threads)
        self.timer_start_threads.start()

        return

    def init_line_edit(self):
        '''
           SUMMARY
           =======
           This method initializes the custom QLineEdit.

           EXTENTED DISCRIPTION
           ====================
           In this custom QLineEdit there is a QPushButton on right site for clear the content.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize the custom QLineEdit with QPushButton on right site")

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_movie_general.lineEdit_search_film_general_company_production_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_company_production_edit.setObjectName("lineEdit_search_film_general_company_production_edit")
        self.ui_movie_general.lineEdit_search_film_general_company_production_edit.setPlaceholderText("Search by company production")
        self.ui_movie_general.lineEdit_search_film_general_company_production_edit.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.temporary_proxy_model_film_general_general_country,
                             column = 2))

        self.ui_movie_general.horizontalLayout_24.addWidget(self.ui_movie_general.lineEdit_search_film_general_company_production_edit)

        self.ui_movie_general.lineEdit_search_film_general_company_production_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_company_production_show.setObjectName("lineEdit_search_film_general_company_production_show")
        self.ui_movie_general.lineEdit_search_film_general_company_production_show.setPlaceholderText("Search by company production")
        self.ui_movie_general.lineEdit_search_film_general_company_production_show.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_general_country,
                             column = 2))

        self.ui_movie_general.horizontalLayout_25.addWidget(self.ui_movie_general.lineEdit_search_film_general_company_production_show)

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_movie_general.lineEdit_search_film_general_film_genre_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_genre_edit.setObjectName("lineEdit_search_film_general_film_genre_edit")
        self.ui_movie_general.lineEdit_search_film_general_film_genre_edit.setPlaceholderText("Search by genre")
        self.ui_movie_general.lineEdit_search_film_general_film_genre_edit.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_26.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_genre_edit)

        self.ui_movie_general.lineEdit_search_film_general_film_genre_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_genre_show.setObjectName("lineEdit_search_film_general_film_genre_show")
        self.ui_movie_general.lineEdit_search_film_general_film_genre_show.setPlaceholderText("Search by genre")
        self.ui_movie_general.lineEdit_search_film_general_film_genre_show.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_38.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_genre_show)

        ###################################################
        ##########  Film Translated Alternativ Title  #####
        ###################################################
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_edit.setObjectName("lineEdit_search_film_general_film_translated_alternativ_title_edit")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_edit.setPlaceholderText("Search by translated alternativ title")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_edit.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_53.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_edit)

        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_show.setObjectName("lineEdit_search_film_general_film_translated_alternativ_title_show")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_show.setPlaceholderText("Search by translated alternativ title")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_show.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_49.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_translated_alternativ_title_show)

        ###################################################
        ##########  Film Original Alternativ Title  #######
        ###################################################
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_edit = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_edit.setObjectName("lineEdit_search_film_general_film_translated_original_title_edit")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_edit.setPlaceholderText("Search by translated original title")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_edit.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_57.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_edit)

        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_show = ButtonLineEdit(image_path_collection = self._image_path_collection)
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_show.setObjectName("lineEdit_search_film_general_film_translated_original_title_show")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_show.setPlaceholderText("Search by translated original title")
        self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_show.set_default_widget_signal.connect(lambda:
          self.filter_update(certain_proxy_model = self._general_proxy_model.proxy_model_film_general_film_genre,
                             column = 2))

        self.ui_movie_general.horizontalLayout_43.addWidget(self.ui_movie_general.lineEdit_search_film_general_film_translated_original_title_show)

        return

    def init_returnPressed_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize returnPressed-Signal for QLineEdit()")

        ###################################################
        #################  General Region  ################
        ###################################################
        #self.ui_master_data.lineEdit_general_region.returnPressed.connect(self.ui_master_data.pushButton_save_general_region.click)
        #self.ui_master_data.lineEdit_edit_general_region.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_region.click)
        self.ui_movie_general.lineEdit_search_film_general_company_production_edit.returnPressed.connect(lambda:
          self.handle_timer(timer_widget = self.search_delay_timer_film_general_production_country_edit,
                            show_wait_message = True,
                            text = self.tr("Searching for records. This should only take a moment.")))

        self.ui_movie_general.lineEdit_search_film_general_company_production_show.returnPressed.connect(lambda:
          self.handle_timer(timer_widget = self.search_delay_timer_film_general_production_country_show,
                            show_wait_message = True,
                            text = self.tr("Searching for records. This should only take a moment.")))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_movie_general.lineEdit_search_film_general_film_genre_show.returnPressed.connect(lambda:
          self.handle_timer(timer_widget = self.search_delay_timer_film_general_film_genre,
                            show_wait_message = True,
                            text = self.tr("Searching for records. This should only take a moment.")))

        self.ui_movie_general.lineEdit_search_film_general_film_genre_edit.returnPressed.connect(lambda:
          self.handle_timer(timer_widget = self.search_delay_timer_film_general_film_genre,
                            show_wait_message = True,
                            text = self.tr("Searching for records. This should only take a moment.")))

        return

    def init_tree_view(self):
        '''
           SUMMARY
           =======
           Here we initialize the QTreeView()-object for all categories.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QTreeView for all categories")

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_movie_general.treeView_show_film_general_production_country.setModel(self._standard_item_model.standard_item_model_general_country)
        self.ui_movie_general.treeView_show_film_general_production_country.sortByColumn(1, Qt.AscendingOrder)

        self.ui_movie_general.treeView_current_film_general_production_country.setModel(self._standard_item_model.temporary_standard_item_model_film_general_film_production_company)

        self.ui_movie_general.treeView_show_film_general_production_country.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_movie_general.treeView_show_film_general_production_country.customContextMenuRequested.connect(lambda position,
            tree_view = self.ui_movie_general.treeView_show_film_general_production_country,
            pushbutton = self.ui_movie_general.pushButton_add_general_film_production_country:
        self.open_context_menu(position = position,
                               tree_view = tree_view,
                               pushbutton = pushbutton,
                               pushbutton_caption = self.tr("Add"),
                               icon = self._image_path_collection.add_default))

        self.ui_movie_general.treeView_current_film_general_production_country.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_movie_general.treeView_current_film_general_production_country.customContextMenuRequested.connect(lambda position,
            tree_view = self.ui_movie_general.treeView_current_film_general_production_country,
            pushbutton = self.ui_movie_general.pushButton_del_current_general_film_production_country:
        self.open_context_menu(position = position,
                               tree_view = tree_view,
                               pushbutton = pushbutton,
                               pushbutton_caption = self.tr("Remove"),
                               icon = self._image_path_collection.delete_default))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_movie_general.treeView_show_film_general_film_genre.setModel(self._standard_item_model.standard_item_model_film_genre)
        self.ui_movie_general.treeView_show_film_general_film_genre.sortByColumn(1, Qt.AscendingOrder)
        self.ui_movie_general.treeView_current_film_general_film_genre.setModel(self._standard_item_model.temporary_standard_item_model_film_general_film_genre)

        self.ui_movie_general.treeView_show_film_general_film_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_movie_general.treeView_show_film_general_film_genre.customContextMenuRequested.connect(lambda position,
            tree_view = self.ui_movie_general.treeView_show_film_general_film_genre,
            pushbutton = self.ui_movie_general.pushButton_add_general_film_film_genre:
        self.open_context_menu(position = position,
                               tree_view = tree_view,
                               pushbutton = pushbutton,
                               pushbutton_caption = self.tr("Add"),
                               icon = self._image_path_collection.add_default))

        self.ui_movie_general.treeView_current_film_general_film_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_movie_general.treeView_current_film_general_film_genre.customContextMenuRequested.connect(lambda position,
            tree_view = self.ui_movie_general.treeView_current_film_general_film_genre,
            pushbutton = self.ui_movie_general.pushButton_del_current_general_film_film_genre:
        self.open_context_menu(position = position,
                               tree_view = tree_view,
                               pushbutton = pushbutton,
                               pushbutton_caption = self.tr("Remove"),
                               icon = self._image_path_collection.delete_default))

        return

    def init_tool_tip(self):
        '''
           SUMMARY
           =======
           This method is used to set a widget's tool tip.
           Then the tool tip is shown whenever the cursor points at the widget.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set a widget's tool tip")
        # QOushButton
        self.ui_movie_general.pushButton_addpage_general_film_production_country.setToolTip(self.tr('Add more countries'))
        self.ui_movie_general.pushButton_del_current_general_film_production_country.setToolTip(self.tr('Delete the selected country'))

        self.ui_movie_general.pushButton_addpage_general_film_production_country.setToolTip(self.tr('Add more countries'))
        self.ui_movie_general.pushButton_del_current_general_film_production_country.setToolTip(self.tr('Delete the selected country'))

        self.ui_movie_general.pushButton_addpage_general_film_film_genre.setToolTip(self.tr('Add more genre'))
        self.ui_movie_general.pushButton_del_current_general_film_film_genre.setToolTip(self.tr('Delete the selected genre'))

        return

    def init_clicked_signal_push_buttons(self):
        '''
           SUMMARY
           =======
           This method is used to set the signal and slots for QpuschButton()-object(s).

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize clicked-signal for QPushButton")

        ###################################################
        #################  General Country  ###############
        ###################################################
        self.ui_movie_general.pushButton_add_general_film_production_country.clicked.connect(lambda:
          self.apped_row_to_model(category = 'allocation_film_general_general_country',
                                  tree_view_widget = self.ui_movie_general.treeView_current_film_general_production_country,
                                  record_tuple = (self._current_selected_id, self._selected_record),
                                  columns = 2,
                                  general_standard_item_model = self._standard_item_model,
                                  certain_standard_item_model = self._standard_item_model.temporary_standard_item_model_film_general_film_production_company,
                                  adding_temp_list = self._temp_allocation_film_general_general_country_list_adding,
                                  parent = self
                                  ))

        self.ui_movie_general.pushButton_del_current_general_film_production_country.clicked.connect(lambda:
          self.remove_row_from_model(category = 'allocation_film_general_general_country',
                                     tree_view_widget = self.ui_movie_general.treeView_current_film_general_production_country,
                                     record_tuple = (self._current_selected_id, self._selected_record),
                                     adding_temp_list = self._temp_allocation_film_general_general_country_list_adding,
                                     deleting_temp_list = self._temp_allocation_film_general_general_country_list_deleting,
                                     general_standard_item_model = self._standard_item_model,
                                     certain_standard_item_model = self._standard_item_model.temporary_standard_item_model_film_general_film_production_company))

        self.ui_movie_general.pushButton_addpage_general_film_production_country.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_production_country,
                           page_index = 1))

        self.ui_movie_general.pushButton_back_general_film_production_country.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_production_country,
                           page_index = 0))

        ###################################################
        #################  Film Genre  ####################
        ###################################################
        self.ui_movie_general.pushButton_add_general_film_film_genre.clicked.connect(lambda:
          self.apped_row_to_model(category = 'allocation_filmgeneral_filmgenre',
                                  tree_view_widget = self.ui_movie_general.treeView_current_film_general_film_genre,
                                  record_tuple = (self._current_selected_id, self._selected_record),
                                  columns = 2,
                                  general_standard_item_model = self._standard_item_model,
                                  certain_standard_item_model = self._standard_item_model.temporary_standard_item_model_film_general_film_genre,
                                  adding_temp_list = self._temp_allocation_filmgeneral_filmgenre_list_adding,
                                  parent = self
                                  ))

        self.ui_movie_general.pushButton_del_current_general_film_film_genre.clicked.connect(lambda:
          self.remove_row_from_model(category = 'allocation_filmgeneral_filmgenre',
                                     tree_view_widget = self.ui_movie_general.treeView_current_film_general_film_genre,
                                     record_tuple = (self._current_selected_id, self._selected_record),
                                     adding_temp_list = self._temp_allocation_filmgeneral_filmgenre_list_adding,
                                     deleting_temp_list = self._temp_allocation_filmgeneral_filmgenre_list_deleting,
                                     general_standard_item_model = self._standard_item_model,
                                     certain_standard_item_model = self._standard_item_model.temporary_standard_item_model_film_general_film_genre))

        self.ui_movie_general.pushButton_addpage_general_film_film_genre.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_film_genre,
                           page_index = 1))
        self.ui_movie_general.pushButton_back_general_film_film_genre.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_film_genre,
                           page_index = 0))

        ###################################################
        #################              ####################
        ###################################################




        self.ui_movie_general.pushButton_addpage_general_film_alternate_translated_title.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_alternate_translated_title,
                           page_index = 1))
        self.ui_movie_general.pushButton_back_general_film_alternate_translated_title.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_alternate_translated_title,
                           page_index = 0))

        self.ui_movie_general.pushButton_addpage_general_film_alternate_original_title.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_alternate_original_title,
                           page_index = 1))

        self.ui_movie_general.pushButton_back_general_film_alternate_original_title.clicked.connect(lambda:
          self.change_page(stacked_widget = self.ui_movie_general.stackedWidget_general_film_alternate_original_title,
                           page_index = 0))


        self.ui_movie_general.pushButton_del_film_poster.clicked.connect(self.del_image_from_label)
        self.ui_movie_general.pushButton_add_film_poster.clicked.connect(self.open_file_dialog)
        self.ui_movie_general.pushButton_zoom_film_poster.clicked.connect(lambda: self.open_view_form_signal.emit(self._film_image_url_old))


        return

    def find_children_widget_on_container(self,
                                          find_children,
                                          parent_container_widget,
                                          widget_index):
        '''
           SUMMARY
           =======
           In this method we want to get all certain Qt-widgets that are children of parentWidget.

           EXTENTED DISCRIPTION
           ====================
           It yields the tab page at index position index or 0 if the index is out of range.


           PARAMETERS
           ==========
           :find_children (Qt widget):                              The given argumnent contains instance of a QTabWidget.
                                                                    In this case, QTabWidget is a parentWidget.

           :parent_container_widget (QTabWidget / QStackedWidget):  Here, we expect an integer. The integer is an index.
                                                                    We need the index, because the QTabWidget has a method to
                                                                    access any tab by its index, sensibly called widget(index).
                                                                    Therefore, if you want to access the n-th widget,
                                                                    you can get it by calling self.tabWidget.widget(n).

          :widget_index (int):                                      Here, we expect an integer. The integer is an index.
                                                                    We need the index, because the QTabWidget has a method to
                                                                    access any tab by its index, sensibly called widget(index).
                                                                    Therefore, if you want to access the n-th widget,
                                                                    you can get it by calling self.tabWidget.widget(n).


           RETURNS
           =======
           :yield:                                                  It will return a generator.
        '''
        logger.info("Yield back all children of certain parentWidget")

        for widget in parent_container_widget.widget(int(widget_index)).findChildren(find_children):
            yield widget

    def set_visible_treeWidget_header(self,
                                      parent_container_widget,
                                      find_children,
                                      widget_index = 0,
                                      bool_value = None):
        '''
           SUMMARY
           =======
           In this method we want to get all certain Qt-widgets that are children of parentWidget.


           PARAMETERS
           ==========
           :instance_name_tabWidget_object (class): The given argumnent contains instance of a QTabWidget.
                                                    In this case, QTabWidget is a parentWidget.

           :widghet_n (int):                        Here, we expect an integer. The integer is an index.
                                                    We need the index, because the QTabWidget has a method to
                                                    access any tab by its index, sensibly called widget(index).
                                                    Therefore, if you want to access the n-th widget,
                                                    you can get it by calling self.tabWidget.widget(n).

           :bool_value (bool):                      In this keyword argument we want a special value: boolean value.


           RETURNS
           =======
           :return:                                 Nothing is returned. Well we know a simple 'return' isn't necessary,
                                                    but this statement gives me a better feeling, that says: 'return'
                                                    terminates a function definitely.
        '''
        logger.info("Set all hheaders hidden of QTreeView")

        for tree_widget_object in self.find_children_widget_on_container(find_children = find_children, parent_container_widget = parent_container_widget, widget_index = widget_index):
            tree_widget_object.setHeaderHidden(bool_value)

        return

    def set_focus(self,
                  widget_object,
                  bool_value):
        '''
           SUMMARY
           =======
           This method is used to set the focus on given QLineEdit()-object.

           EXTENTED DISCRIPTION
           ====================
           This method is necessary, because the setFocus()-function is used
           in several places.

           PARAMETERS
           ==========
           :widget_object (Qt-object):  We need the QLineEdit()-object to set the focus in.

           :bool_value (bool):          By default, its on True, which menas, the given Qt-object
                                        will be visible.

           RETURNS
           =======
           :return:                     Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the focus on given QLineEdit()-object.")

        widget_object.setFocus(bool_value)

        return

    def set_attribute_on_none(self):
        '''
           SUMMARY
           =======
           This method is used to set certain attributes to None
           after the completion of a task.

           EXTENTED DISCRIPTION
           ====================
           This task is needed in several places, hence this method.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set certain attributes to None.")

        self._current_selected_id = None
        self._film_image_url_old = None
        self._film_image_url = None
        self._file_extension = None
        self._selected_record = None

        return

    def set_all_widget_on_default(self):

        for widget in self.ui_movie_general.findChildren((QLineEdit, QTextEdit, QPlainTextEdit)):
            widget.clear()

        self.set_attribute_on_none()

        #   We want to set poster on default
        self.show_image_on_label(img_path = self._image_path_collection.default_film_poster)

        self._standard_item_model.clear_all_standard_item_model(
            list_certain_item_models = [self._standard_item_model.temporary_standard_item_model_film_general_film_production_company])

        return

    def set_all_combobox_default(self):

      for combobox in self.ui_movie_general.findChildren(QComboBox):
        combobox.clear()

        return

    def init_textChanged_signal_line_edit(self):
        '''
           SUMMARY
           =======
           This method is used to set signal and slots for QLineEdit()-objects.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the signal and slots for QLineEdit()-object(s)")

        self.ui_movie_general.lineEdit_title_translated.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_movie_general.lineEdit_title_translated,
                                     category = "general_film_title_translated",
                                     max_length = 100,
                                     allow_trailing_spaces = True,
                                     change_windows_title = True))


        self.ui_movie_general.lineEdit_title_original.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_movie_general.lineEdit_title_original,
                                     category = "general_film_title_original",
                                     max_length = 100,
                                     allow_trailing_spaces = True,
                                     change_windows_title = False))


        self.ui_movie_general.lineEdit_year_production.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj = self.ui_movie_general.lineEdit_year_production,
                                     allow_trailing_spaces = False,
                                     allow_only_number = True,
                                     category = "general_film_release_year",
                                     change_windows_title = False,
                                     max_length = 4))

        return

    def Set_model_on_treeview(self,
                              **kwargs):
        '''
           SUMMARY
           =======
           Sets the model for the view to present.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Sets the model for the view to present.")

        try:


          kwargs.get('widget_tree_view').setModel(kwargs.get('certain_proxy_model'))

          self.init_selectionChanged_signal_treeview()
          self.close_wait_message()

          self.set_hide_column(
            tree_view_widget = kwargs.get('widget_tree_view'),
            column = 0,
            bool_value = True)

        except AttributeError:

          pass

        return

    def open_file_dialog(self):
        '''
            NOTICE:
            =======
            In this method we select a file with a QFileDialog - its saves
            in the variables named (self.file_path_front_cover or self.file_path_back_cover).
            Its enables the user to navigate through the file system and select a file to open

            PARAMETERS:
            ===========
            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        logger.info("Open the file dialog")


        '''
            We save the caption of the title in the variable named (qfile_dialog_title),
            because we need it often.
        '''
        file_dialog_title = self.tr("Select")

        home_path = path.expanduser(self._configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"])

        file_extensions_filter = "*.png *.jpg *.bmp"


        '''

            In this condition, we want a variable (in this case a string) named (self.file_path_front_cover).
            Then we ask Qt to open a QFileDialog by using the function named (getOpenFileName), that returns us the
            name of the file that is selected.

            well, we have to pass a couple arguments to it:
            First, we give it the parent widget (in this case, our main window - self),
            then we give it a title, that is saved in the variable above named (qfile_dialog_title)
            for the dialog, then we give it a place to open up to (in this case, the directory is saved in in a dictionary,
            if the path is not saved in the dictionary its opens the user's home directory).
            non-native dialog
        '''


        '''
            Before we pop up a file dialog we have to check if the folder is already exists.
            In the variable named (home_path) the path to the folder is saved.
        '''
        if is_folder_exists(home_path):
            '''
                Yes the folder already exists. We don't have to do anything.
            '''
            pass
        else:
            '''
                There is a problem. The folder doesn't exists. We have to overwirte
                the variable named (home_path). In this case we save a simple
                path seperator. So the file dialoge will pop up witha home path.
            '''
            home_path = path.sep

        self._film_image_url = unicode(QFileDialog.getOpenFileName(self,
                                                                  file_dialog_title,
                                                                  home_path,
                                                                  "Images ({file_extensions})".format(file_extensions=file_extensions_filter)))

        '''
            we check if the user selected a file. If the user did it,
            the path is returned, otherwise if not
            (ie, the user cancels the operation) None is returned.
        '''
        if self._film_image_url:
            '''
                Yes the path is returned. We set enabled on True.
                User can click on the QPushButtons
            '''

            self._film_image_url_old = self._film_image_url

            self.film_image_url_new = self._film_image_url

            self.ui_movie_general.pushButton_zoom_film_poster.setEnabled(True)
            self.ui_movie_general.pushButton_del_film_poster.setEnabled(True)

            self._configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"] = extract_directory(self._film_image_url)
            home_path = self._configuration_saver_dict.dict_set_general_settings["LastPathFolderDialog"]

            '''
                We invoke a methode and pass them two arguments.
            '''
            self.show_image_on_label(img_path = self._film_image_url)

            self._file_extension = get_file_extension(file_path = unicode(self._film_image_url))

            self.handle_text_changed_signal.emit(unicode(self._file_extension), 'film_poster_extension')
            self.handle_text_changed_signal.emit(unicode(self._film_image_url), 'film_poster_path')

        else:
            '''
                No. The user cancels the operation. All QPushButtons are disabled.
            '''
            if self._film_image_url_old:
                self.ui_movie_general.pushButton_zoom_film_poster.setEnabled(True)
                self.ui_movie_general.pushButton_del_film_poster.setEnabled(True)
            else:
                self.ui_movie_general.pushButton_zoom_film_poster.setEnabled(False)
                self.ui_movie_general.pushButton_del_film_poster.setEnabled(False)

        return

    def show_image_on_label(self,
                            img_path):
        '''
            NOTICE:
            =======
            In this method we load and stack_set_current_index the image on QLabel()-object.

            PARAMETERS:
            ===========
            :img_path       -       The given argument contains a path to the
                                    image file that we want to load and stack_set_current_index.

            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        pixmap = QPixmap(img_path)

        self.ui_movie_general.label_poster.clear()
        self.ui_movie_general.label_poster.setPixmap(pixmap)

        return

    def del_image_from_label(self):
        '''
            NOTICE:
            =======
            This method is used to clear the QLabel()-object that contains
            the poster of a film.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''

        '''
            We don't need the content of these variables anymore.
            Lets us clear these variables.
        '''
        self._film_image_url = None
        self._film_image_url_old = None

        self.ui_movie_general.label_poster.clear()

        '''
            When the film poster is removed we also disable the zoom-button
            and the delete-button.
        '''
        self.ui_movie_general.pushButton_zoom_film_poster.setEnabled(False)
        self.ui_movie_general.pushButton_del_film_poster.setEnabled(False)

        '''
            The user decides to delete the current film poster. He gets
            the default image. Its a standard iamge that says "there is no film poster".
        '''
        self.ui_movie_general.label_poster.setPixmap(QPixmap(":/img_156x220/img_156x220/default.png"))

        return

    def set_text(self,
                 widget = None,
                 text = None):
        '''
           SUMMARY
           =======
           Updates the input each time a key is pressed.

           PARAMETERS
           ==========
           :widget (Qt-Object): On which qt object should be shown the text?

           :text (unicode):     We need text to set it.

           RETURNS
           =======
           :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the text given Qt-object.")

        if isinstance(widget, QLineEdit):
            widget.setText(text)
        elif isinstance(widget, QProgressBar):
            widget.setText(text)

        return

    def set_cursor_position(self,
                            widget,
                            pos):
        '''
           SUMMARY
           =======
           This method is used to set the cursor position.

           PARAMETERS
           ==========
           :widget (QLineEdit):     We expect a given QLineEdit()-Widget where we set the cursor position.

           :pos (int):              We need a int value to set the cursor position.

           RETURNS
           =======
           :return:                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set the cursor position in QLineEdit()")

        if isinstance(widget, QLineEdit):
            widget.setCursorPosition(pos)

        return

    def handle_text_changed(self,
                            category,
                            line_edit_obj,
                            max_length,
                            change_windows_title = False,
                            allow_trailing_spaces = True,
                            allow_only_number = False):
        '''
           SUMMARY
           =======
           This method is used to control the input in given QLineEdit().

           PARAMETERS
           ==========
           :line_edit_obj (QLineEdit):              Here, we want a QWidget()-object: QLineEdit.
                                                    Well, we need this object to get the current position
                                                    of the cursor.

           :max_length (int):                       This keyword-argument gets an integer for setting a maximum length
                                                    of a given text.


           :change_windows_title (bool):            In this keyword argument we except a special value: boolean value.
                                                    By default, the value is set to False, because we don't want the
                                                    window title to change.

           :allow_trailing_spaces (bool):           We except a boolean value. By default, its set to True.
                                                    True means that spaces between the each words are allowed.
                                                    It is very rare that we want to ban trailing space.

           :allow_only_number (bool):               We except a boolean value. When we get True we
                                                    want to know we shoukd allow only numbers. By default
                                                    the value is set to False, because we want the user to
                                                    enter all characters.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Control the input")

        created_text = None

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text = line_edit_obj.text(),
                                                                      pos = line_edit_obj.cursorPosition(),
                                                                      allow_trailing_spaces = allow_trailing_spaces,
                                                                      allow_only_number = allow_only_number,
                                                                      max_length = max_length)
        '''
            We know that user typed a translated film title. In this case we
            take a look if the string is empty or not.
        '''

        self.handle_text_changed_signal.emit(get_text, category)

        if get_text:
            '''
                No, the string isn't empty
            '''
            self.set_text(widget = line_edit_obj, text = get_text)
            self.set_cursor_position(widget = line_edit_obj, pos = get_pos)

        else:

            line_edit_obj.setText('')

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if change_windows_title:

            self.set_window_title_signal.emit((get_text))
            self._film_title = get_text


        #if not change_windows_title:
        #    pass

            # if created_text:
            #     print "created_text is voll"
            #     pass

            # else:
            #     '''
            #         Bad news, the string is emptyx.
            #     '''
            #     created_text = "Add movie " + "".format(''.join(''.split()))
            #     self.set_window_title_signal.emit(created_text)
            #     self._film_title = ''.join(''.split())

        return

    @pyqtSlot()
    def stop_timers(self):

        for timer in self._list_timer:

            try:

                timer.stop()

            except AttributeError:

                desired_trace = format_exc(exc_info())

                logger.error(desired_trace)

                pass

            except TypeError:

                desired_trace = format_exc(exc_info())

                logger.error(desired_trace)

                if unicode(err).lower().find("'NoneType' object is not iterable") != -1:
                    pass

    @pyqtSlot()
    def quite_threads(self):
        '''
           SUMMARY
           =======
           In this method we close all (opened) threads that have been saved in the list named
           self._list_threads. Currently, this method is only called when the window is closed.

           EXTENTED DISCRIPTION
           ====================
           Explicitly mark a python method as a Qt slot and specify a C++ signature for it
           (most commonly in order to select a particular overload). pyqtSlot has a much more pythonic API.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "MasterData_Window quite_threads is called successfully"
        print ""

        logger.info("Close all opened threads")

        for thread in self._list_threads:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
                del thread
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

        #   When all threads are closed, we don't need the elements anymore.
        #   So we have to clear the contents of the list in place.
        del self._list_threads[:]

        #   And then we want the programm to delete the contents itself from memory,
        #   that means this will entirely delete the list
        del self._list_threads

        self.task_thread = None

        #   But we need the list again, so we reinitialize it.
        self._list_threads = []

        return

    def resizeEvent(self,
                    resizeEvent):
        '''
            NOTICE:
            =======
            This method is reimplemented and modified to receive widget resize events which are passed in the event parameter.
            When the widget is resized it already has its new geometry.

            Here its looks if the variable named (self.is_focused) is NOT True. By default this variable has the value None.
            If there is None do nothing.

            Next step its calls the function named (calculate_cover_poster), that returns a result, and
            sets the width (w) and the height (h) of the widget everytime: setFixedSize(width, height)

            PARAMETERS:
            ===========
            :event      -       This method gets a resize events.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info("Set the fixed size of the widget")

        if not self.is_focused:
            self.set_focus(widget_object = self.ui_movie_general.lineEdit_title_translated,
                           bool_value = True)

        result_groupBox_poster = calculate_cover_poster(int(self.ui_movie_general.width()), 6)

        self.ui_movie_general.groupBox_poster.setFixedSize(result_groupBox_poster, self.ui_movie_general.tabWidget_general_information.height())

        return

    def dragEnterEvent(self,
                       event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method, because it provides an event
           which is sent to the target widget as dragging action enters it.

           PARAMETERS
           ==========
           :event (QDropEvent):   We  get this event when Qt receives a drop request for a top-level
                                  widget from the window system. That means, first it sends the widget a QDropEvent.
                                  The widget is droped if it accepts the drop event. The default implementation of
                                  QWidget.dropEvent() accepts the drop event.

           RETURNS
           =======
           :return:               Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Perform action drag")

        self._managed_drag_drop.drag_enter_event(event)

        return

    def dropEvent(self,
                  event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method. It occurs
           when the drop is completed. The event’s proposed action can be accepted or rejected conditionally.

           PARAMETERS
           ==========
           :event (QDropEvent):   We  get this event when Qt receives a drop request for a top-level
                                  widget from the window system. That means, first it sends the widget a QDropEvent.
                                  The widget is droped if it accepts the drop event. The default implementation of
                                  QWidget.dropEvent() accepts the drop event.

           RETURNS
           =======
           :return:               Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                                  me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Complet the drop.")

        self._film_image_url = self._managed_drag_drop.drop_event(event)

        self._film_image_url_old = self._film_image_url

        self.film_image_url_new = self._film_image_url

        self._new_on_image = True

        if not unicode(self._film_image_url) == '':
            self._file_extension = get_file_extension(file_path = unicode(self._film_image_url))

            self.handle_text_changed_signal.emit(unicode(self._file_extension), 'film_poster_extension')
            self.handle_text_changed_signal.emit(unicode(self._film_image_url), 'film_poster_path')

            self.pixmap = QPixmap(self._film_image_url)
            self.ui_movie_general.label_poster.setPixmap(self.pixmap)
            self.ui_movie_general.pushButton_zoom_film_poster.setEnabled(True)
            self.ui_movie_general.pushButton_del_film_poster.setEnabled(True)

        return

    def closeEvent(self,
                    event):
        '''
           SUMMARY
           =======
           This is a overwirtten and reimplemented method.

           PARAMETERS
           ==========
           :event :     By default the given event is accepted and the widget is closed. We
                        get this event when Qt receives a window close request for a top-level
                        widget from the window system. That means, first it sends the widget a QCloseEvent.
                        The widget is closed if it accepts the close event. The default implementation of
                        QWidget.closeEvent() accepts the close event.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Close the current window.")

        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):

            #   The isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
            #   that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
            #   Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
            #   This is the case when the window is a subwindow in the mdi. We want to know if
            #   the actual window is an instance attribute inside in this class.

            self.set_flat_signal.emit('master_data', False)
            self.parent().close()
        else:

            #   No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
            #   That means, the window is not a subwindow.
            self.close()

if __name__ == "__main__":
    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    ## Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

    print  """NOTICE: \n======= \nYou can also leave the inputs blank - by pressing just enter.
              """

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()


    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)



    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    LOG_FOLDER_PATH = path.join(BASE_PATH, LOG_FILE_PATH)

    print ""
    print "The log file is saved in", LOG_FOLDER_PATH

    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))

    app = QApplication(argv)

    translator = QTranslator()
    translator_path = ":/translators/german_sie.qm"
    translator.load(translator_path)
    app.installTranslator(translator)

    window = MovieGeneral_Window(dict_custom_config)


    #window.resize(600, 400)
    window.showMaximized()
    exit(app.exec_())
