#!/usr/bin/env python
#-*- coding:utf-8 -*-

import traceback
import time
from datetime import datetime
import requests
from requests.auth import HTTPBasicAuth
import tempfile

from sys import exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from sqlalchemy.exc import SQLAlchemyError, UnboundExecutionError
from sqlalchemy import exc

from PyQt4.QtCore import Qt, QTimer, QObject, pyqtSignal, QThread

from requests.exceptions import ConnectionError, Timeout, SSLError, RequestException, \
                                HTTPError, URLRequired

try:

    from xarphus.core.manage_data_manipulation_master_data import MasterDataManipulation
    from xarphus.core.standard_item_model import StandardItemModel
    from xarphus.core.normalize_uni_str import normalize_record_list, normalize_record_list_two_pair_tuple
    from xarphus.core.remove_case_insensitive_duplicates import remove_case_insensitive_duplicates_list, \
                                                                remove_case_insensitive_duplicates_list_two_pair_tuple, \
                                                                compaire_list_two_pair_tuple, compare_two_simple_listss

    from xarphus.core.manage_categories import analyze_categorie
    from xarphus.core.manage_partition import partition_text
    from xarphus.core.web_scraper import scrape_no_name_taple

except ImportError:

    from manage_data_manipulation_master_data import MasterDataManipulation
    from managed_defined_session import *
    from normalize_uni_str import normalize_record_list, normalize_record_list_two_pair_tuple
    from remove_case_insensitive_duplicates import remove_case_insensitive_duplicates_list, \
                                                        remove_case_insensitive_duplicates_list_two_pair_tuple, \
                                                        compaire_list_two_pair_tuple, compare_two_simple_listss
    from manage_categories import analyze_categorie
    from manage_partition import partition_text
    from web_scraper import scrape_no_name_taple

#from core.managed_defined_session import *

class TaskModel(QObject):

    send_model_signal = pyqtSignal(object)
    quit_thread_signal = pyqtSignal()
    close_wait_message_signal = pyqtSignal()

    def __init__(self,
                parent = None,
                **kwargs):
        QObject.__init__(self, parent)

        self.standard_item_model = StandardItemModel()

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.model = kwargs['certain_standard_item_model']
        self.column = kwargs.get('column', 1)
        self.order = kwargs.get('order', Qt.AscendingOrder)
        self.category = kwargs['category']

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def start_thread(self):

        model = self.standard_item_model.sort_model(
                    model = self.model,
                    column = self.column)

        #self.send_model_signal.emit(model)

        #self.standard_item_model.refresh_model(
        #    model = model,
        #    category = self.category)

        self.close_wait_message_signal.emit()

        self.quit_thread_signal.emit()

    def stop_task_thread(self):
        '''
            NOTICE:
            =======
            This method is used to stop the still running generator by just stopping the timer.
            No Exception is raised. Its a simple stopper.

            The user decided to break or interrupt this loop - for instance when
            the user just closes the window while the generator is still running.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        self.quit_thread_signal.emit()

class TaskDataManipulation(QObject):
    ''' Defines signals '''
    clear_widget_object_content_signal = pyqtSignal()
    confirm_saving_successful_signal = pyqtSignal(str, str, str)
    error_message_signal = pyqtSignal(str, str, object)
    remove_selected_be_edited_id_signal = pyqtSignal()
    remove_current_selected_id_signal = pyqtSignal()
    remove_item_model_signal = pyqtSignal(object, object)
    change_page_signal = pyqtSignal()
    sort_model_signal = pyqtSignal(dict)
    count_model_items_signal = pyqtSignal(object, int)
    set_text_label_signal = pyqtSignal(object, str)
    create_wait_window_signal = pyqtSignal(str)
    close_wait_message_signal = pyqtSignal()
    manage_standart_item_model_signal = pyqtSignal()

    delete_widget_signal = pyqtSignal()

    notify_progress = pyqtSignal(str)
    finish_progress_signal = pyqtSignal()
    counting_label_signal = pyqtSignal(object, int)
    set_horizontal_header_labels_signal = pyqtSignal(dict)
    update_progress_bar_signal = pyqtSignal(int, int)
    #update_progress_bar_online_signal = pyqtSignal(int, int)
    count_progress_signal = pyqtSignal(int)
    hide_column_signal = pyqtSignal(object)
    set_source_model_signal = pyqtSignal()
    populate_proxy_model_signal = pyqtSignal(dict)
    resize_to_contents_signal = pyqtSignal(object, int)
    text_notify_update_signal = pyqtSignal(str)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()

    set_model_signal = pyqtSignal(object, object)

    set_header_hidden_signal = pyqtSignal(dict)

    populate_standard_item_model_in_gui_signal = pyqtSignal(list, int, object, object)

    Set_model_on_treeview_signal = pyqtSignal(dict)

    set_enable_widget_signal = pyqtSignal(bool)

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        ''' Create instance(s) '''
        self._master_data_manipulation = MasterDataManipulation(scoped_session = kwargs.get('scoped_session'),
                                                               configuration_saver_dict = kwargs.get('configuration_saver_dict'))

        ''' Create attribute(s) '''
        self.count = 0
        self._count_minus = -1
        self._total_row_count = 0
        self._run_semaphore = 1

        kwargs['master_data_manipulation'] = self._master_data_manipulation
        self._kwargs = kwargs


        #self._kwargs.get('general_standard_item_model').set_temporary_standard_item_model()

        print ""
        print "TaskDataManipulation is created successfully"
        print "TaskDataManipulation / self._kwargs: -> \n {}".format(self._kwargs)

        #   We have to pass the requested default values for the keys not given in kwargs
        self._populate_timer_interval = kwargs.get('populate_timer_interval', 10)
        self._bulk_the_list = kwargs.get('bulk_the_list', False)
        self._show_messagebox = kwargs.get('show_messagebox', False)
        self._hide_column = kwargs.get('hide_column', False)
        self._bulk_the_list_of_dict = kwargs.get('bulk_the_list_of_dict', False)

        self._configuration_saver_dict = kwargs.get('configuration_saver_dict')
        self._category = kwargs.get('category')
        self._work_mode = kwargs.get('work_mode')
        self._certain_standard_item_model = kwargs.get('certain_standard_item_model')
        self._general_standard_item_model = kwargs.get('general_standard_item_model')
        self._scoped_session = kwargs.get('scoped_session')
        self._resize_column = kwargs.get('resize_column')

        self._widget_tree_view = kwargs.get('widget_tree_view')
        self._general_proxy_model = kwargs.get('general_proxy_model')
        self._certain_proxy_model = kwargs.get('certain_proxy_model')


        self._online_count = 0

        self.timer_list = []

        ''' Create constants. '''
        self.MSG_ERROR_TITLE = self.tr('Error...')

        self._kwargs.get('general_standard_item_model').populate_standard_item_model_in_gui_signal.connect(
            lambda
            append_row,
            count_items,
            img_size,
            IMAGE_PATH:
            self.populate_standard_item_model_in_gui_signal.emit(append_row, count_items, img_size, IMAGE_PATH))

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def start_work(self):
        '''
           SUMMARY
           =======
           This method is used to start the work, when the thread is started.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Start the work")

        # start to stopp the time
        self.start_time = datetime.now()

        #   Right at the beginning we have to check first, if the user wants the program
        #   to fetch all records from own database (fetch_all) or if he wants the
        #   program to fetch all records from online service (fetch_online_data).
        if self._work_mode == "fetch_all":

            self._check_model(**self._kwargs)

        elif self._work_mode == "fetch_online_data":
            self.open_temp_file(tree_view = self._widget_tree_view,
                                certain_proxy_model = self._certain_proxy_model,
                                general_proxy_model = self._general_proxy_model,
                                certain_standard_item_model = self._certain_standard_item_model,
                                **kwargs)

        return

    def _check_model(self,
                     **kwargs):
        '''
           SUMMARY
           =======
           This method is used to check if the given model is empty.

           DETAILS:
           ========
           All changes are made both in the model and in the database individually.

           PARAMETERS:
           ===========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Check the given model")

        #   Before we fetch all records from database, we firstly have to take a look
        #   if there is less than 1 record in the model, because  We don't want the program loads
        #   all records from the database every time and populate it. In general the records should
        #   be loaded in the model once. So we know, if the given model is empty we have to populate
        #   in the given model by fetching all records from datanbase.

        #   Let us see if the user wants to load the records anyway. In this case, the user don't
        #   want the program to force loading of all items.
        if not kwargs.get('force_fetch_all'):

            #   So, how many items are in the certain given model of QStandardItemModel?
            if kwargs['certain_standard_item_model'].rowCount() < 1:
                #   Ok, there is really less than 1 record, we have to load the
                #   records from database.

                self.create_wait_window_signal.emit(self.tr("Please wait while record(s) is/are being fetched."))

                self.set_horizontal_header_labels_signal.emit(kwargs)

                self.set_header_hidden_signal.emit(kwargs)

                if kwargs.get('hide_column', False):

                    self.hide_column_signal.emit(kwargs.get('widget_tree_view'))

                self._fetch_all_records_from_database(**kwargs)

        else:
                #   There are more than 0 records in the given model.
                #   Let us see if the user wants to load the records anyway. Yes, the user
                #   wants force theo program to load all items from database in the given certain model.
            if kwargs.get('force_fetch_all'):

                #   Ok, the user wants to load the records again.
                self.create_wait_window_signal.emit(self.tr("RRRRRRELOOOOAD!! Please wait while fetching record(s)"))

                self.set_horizontal_header_labels_signal.emit(kwargs)

                self.set_header_hidden_signal.emit(kwargs)

                if kwargs.get('hide_column', False):

                    self.hide_column_signal.emit(kwargs.get('widget_tree_view'))

                kwargs.get('general_standard_item_model').dlear_stnandard_item_model(
                    certain_standard_item_model = kwargs.get('certain_standard_item_model'))

                kwargs.get('general_standard_item_model').reset_standard_item_model(
                    category = kwargs.get('category'))

                # self.Set_model_on_treeview_signal.emit(kwargs)

                # kwargs.get('general_standard_item_model').create_standard_item_model(
                #     category = kwargs.get('category'),
                #     rows = 0,
                #     columns = 2,
                #     parent = None)

                self._fetch_all_records_from_database(**kwargs)

            else:
                #   We don't need fetch all records from database and populate it.
                self.delete_widget_signal.emit()

                if kwargs['hide_column']:
                    self.hide_column_signal.emit(kwargs['widget_tree_view'])

                self.finish_task_thread()

        return

    def _gen_queried_item(self,
                         tuple_list = None):
        '''
            NOTICE:
            =======
            This method is used to populate all item later.

            PARAMETERS:
            ===========
            :tuple_list -   We except a list with tuple.

            :return     -   Username and Password is returned.
        '''
        for tuple_item in tuple_list:
            yield tuple_item

    def _fetch_support_login_data(self):
        '''
            NOTICE:
            =======
            In this method we want to get all access data to support account.

            PARAMETERS:
            ===========
            :return     -   Username and Password is returned.
        '''
        support_username = self._configuration_saver_dict.dict_set_general_settings["htaccess_username"]
        support_password = self._configuration_saver_dict.dict_set_general_settings["htpassword"]

        return support_username, support_password

    def _fetch_support_link(self,
                            support_category = None):
        '''
            NOTICE:
            =======
            This method creates the complete link to the support account.

            PARAMETERS:
            ===========
            :support_category   -   We have to know which category we work with,
                                    based on this on this information we create a
                                    complete suport-link.

            :return             -   Complete support linke is returned.
        '''
        file_name_dict = {
                          "general_region": "GeneralRegionFileName",
                          "general_country": "GeneralCountryFileName",
                          "general_language": "GeneralLanguageFileName",
                          "general_media_type": "GeneralMediaType",
                          "general_award": "GeneralAwardFileName",
                          "general_packaging": "GeneralPackagingFileName",
                          "person_nationality": "PersonNationalityFileName",
                          "film_genre": "FilmGenreFileName",
                          "music_genre": "MusicGenreFileName",
                          "film_production_company": "FilmProductionCompanyFileName",
                          "film_rentals": "FilmRentalsFileName",
                          "person_relationship": "PersonRelationshipFileName",
                          "film_distribution": "FilmDistributionFileName",
                          "film_studio": "FilmStudioFileName",
                          "film_resolution": "FilmResolutionFileName",
                          "general_manufacturer": "GeneralManufacturer",
                          "film_regional_code": "FilmRegionalCodeFileName",
                          "film_aspect_ratio": "FilmAspectRatioFileName",
                          "film_dimension": "FilmDimensionFileName",
                          "film_edition": "FilmEditionFileName",
                          "film_version": "FilmVersionFileName",
                          "film_videonorm": "FilmVideonormFileName",
                          "film_audioformat": "FilmAudioformatFileName",
                          "film_audio_channel": "FilmAudiochannelFileName",
                          "film_colour_format": "FilmColourformatFileName",
                          "film_videoformat": "FilmVideoformatFileName",
                          "film_soundsystem": "FilmSoundsystemFileName"
                          }

        file_name = file_name_dict[support_category]

        basic_support_url = self._configuration_saver_dict.dict_set_general_settings["URLSupport"]
        support_language = self._configuration_saver_dict.dict_set_general_settings["SupportLanguage"]
        support_file = self._configuration_saver_dict.dict_set_general_settings[file_name]

        return "{basic_support_url}/{support_file}".format(basic_support_url = basic_support_url,
                                                                             support_file = support_file)

    def _fetch_filtered_item(self,
                             tree_view,
                             certain_proxy_model,
                             general_proxy_model,
                             certain_standard_item_model,
                             resize_colum,
                             category,
                             bulk_list):


        self.gen_timer = QTimer()
        # assoziiert increment() mit TIMEOUT Ereignis
        self.gen_timer.setSingleShot(False)
        self.gen_timer.setInterval(self._populate_timer_interval)
        self.gen_timer.timeout.connect(lambda:
            self.populate_items(tree_view = tree_view,
                                certain_proxy_model = certain_proxy_model,
                                general_proxy_model = general_proxy_model,
                                category = category,
                                resize_colum = resize_colum,
                                certain_standard_item_model = certain_standard_item_model))



        queried_item_list = []

        master_data_manipulation = MasterDataManipulation(scoped_session = self._scoped_session)

        self.text_notify_update_signal.emit(self.tr("Processing records for population."))

        if any(element == category for element in analyze_categorie(list_name = "simple_list")):


            for item in bulk_list:

                record = master_data_manipulation.fetch_filtered_item(category = category,
                                                                      filter_text = item)

                queried_item_list.append(record)

        elif any(element == category for element in analyze_categorie(list_name = "list_two_pair_tuple")):

            for item in bulk_list:

                first_record, second_record = item

                record = master_data_manipulation.fetch_filtered_item(category = category,
                                                                      filter_text = first_record)

                queried_item_list.append(record)

        self.element = self._gen_queried_item(tuple_list = queried_item_list)

        self.gen_timer.start()

        self.text_notify_update_signal.emit(self.tr("Populating all missing records."))

        queried_item_list[:]

        self.text_notify_update_signal.emit(self.tr("The records have been populated."))

        return

    def init_populate_timer(self,
                            **kwargs):
        '''
           SUMMARY
           =======
           This method is used to create an generator object and then to fetch
           all records from database.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.
           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''
        self.populate_timer = QTimer()

        self.timer_list.append(self.populate_timer)

        self.populate_timer.setSingleShot(False)
        self.populate_timer.setInterval(kwargs['populate_timer_interval'])
        self.populate_timer.timeout.connect(lambda:
            self.populate_items(**kwargs))
        self.populate_timer.start()

        return

    def _fetch_all_records_from_database(self,
                                         **kwargs):
        '''
           SUMMARY
           =======
           This method is used to create an generator object and then to fetch
           all records from database.

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.
           RETURNS
           =======
           :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
        '''

        try:

            query_data = kwargs['master_data_manipulation'].fetch_all_items

            self.element = query_data(category = kwargs['category'])

            self._total_row_count = kwargs['master_data_manipulation'].count_rows(category = kwargs['category'])

            self.count_progress_signal.emit(self._total_row_count)

            self.init_populate_timer(**kwargs)

        except UnboundExecutionError:
            self.delete_widget_signal.emit()
            logger.info("Before sending populate_proxy_model_signal --> \n {}".format(kwargs))
            self.populate_proxy_model_signal.emit(kwargs)
            self.finish_task_thread()
            self.close_wait_message_signal.emit()

            if hide_column:
                self.hide_column_signal.emit(tree_view)

            if kwargs['show_messagebox']:

                self.error_message_signal.emit('No database connection', "You can't work without a database connection. Please connect to the database and try again.", '' )

        except AttributeError:

            self.delete_widget_signal.emit()
            #self.populate_proxy_model_signal.emit(tree_view, certain_proxy_model, general_proxy_model, certain_standard_item_model, kwargs['category'])
            self.finish_task_thread()
            self.close_wait_message_signal.emit()

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            logger.info("An error has occurred")

            conn_err = None
            msg_title = None

            # How to use find()
            if unicode(desired_trace).find("'NoneType' object has no attribute 'commit'") != -1 or \
                unicode(desired_trace).find("'NoneType' object has no attribute 'scoped_session'") != -1 or \
                unicode(desired_trace).find("object has no attribute '_session'") != -1:

                conn_err = self.tr("You can't work without a database connection. Please connect to the database and try again.")
                msg_title = self.tr("No database connection")

            if kwargs['show_messagebox']:

                self.error_message_signal.emit(msg_title, conn_err, '' )

        except KeyError:

            self.delete_widget_signal.emit()
            #self.populate_proxy_model_signal.emit(tree_view, certain_proxy_model, general_proxy_model, certain_standard_item_model, kwargs['category'])
            self.finish_task_thread()
            self.close_wait_message_signal.emit()

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            internal_err = self.tr("Oops! We are sorry, something went wrong. " \
                               "An internal error has occurred. \n" \
                               "The error has been logged to the log-file. \n\n" \
                               "Choose 'Ignore' to continue running in an inconsistent state. Choose 'Break' to halt the application. " \
                               " By choosing 'Break' it will result in the loss of all unsaved data.")

            msg_title = self.tr("An internal error")

            if kwargs['show_messagebox']:

                self.error_message_signal.emit(msg_title, internal_err, '' )

    def _bulking_list_records(self,
                              tree_view,
                              certain_proxy_model,
                              general_proxy_model,
                              certain_standard_item_model,
                              category = None,
                              replaced_category = None,
                              work_area = None,
                              list_records = None,
                              set_flush = False):
        '''
            NOTICE:
            =======
            In this method we want it to perform a bulk save of the given list of records
            in the database.

            PARAMETERS:
            ===========
            :category       -       The category says us what records it is.

            :work_area      -       We need to know where we are working.

            :list_records   -       We need the list of records to bulk all of them.

            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        count = 0
        master_data_manipulation = MasterDataManipulation(scoped_session = self._scoped_session)

        try:

            result_record = master_data_manipulation.bulk_insert_mapped_record(bulk_list = list_records,
                                                                               set_flush = set_flush,
                                                                               category = category)

            self.text_notify_update_signal.emit(self.tr('The missing records were saved to the database successfully.'))

            if set_flush:
                self._fetch_filtered_item(category = category,
                                          tree_view = tree_view,
                                          certain_proxy_model = certain_proxy_model,
                                          general_proxy_model = general_proxy_model,
                                          certain_standard_item_model = certain_standard_item_model,
                                          resize_colum = resize_colum,
                                          bulk_list = list_records)


        except exc.IntegrityError as e:

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            self.close_wait_message_signal.emit()

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("An error has occurred"), conn_err, None)

        return



    def _fetch_all_records_in_list(self,
                                  category = None):
        '''
            NOTICE:
            =======
            This method is used to fetch all records
            and save them to the list that will be returned.

            PARAMETERS:
            ===========
            :category       -       We have to know which category we work with.

            :return         -       Return a list
        '''
        master_data_manipulation = MasterDataManipulation(scoped_session = self._scoped_session)
        query_data = master_data_manipulation.fetch_all_items
        element = query_data(category = category)

        if any(element == category for element in analyze_categorie(list_name = "simple_list")):

            return [db_item for _, db_item in element]

        if any(element == category for element in analyze_categorie(list_name = "list_two_pair_tuple")):

            return [(db_item_first, db_item_second) for _, db_item_first, db_item_second in element]

    def _fetch_all_online_data(self,
                               url = None,
                               support_username = None,
                               support_password = None,
                               category = None,
                               stream = True,
                               timeout = 10):
        '''
            NOTICE:
            =======
            This method is used to fetch all records from online service.
            All fechted records from online service should be saved in a list that will be returned -
            within a counter.

            PARAMETERS:
            ===========
            :url                -   The category says us what records it is.

            :support_username   -   So we can access a protected area we need a username at first.

            :support_password   -   We also need a password for protected area.

            :category           -   We have to know which category we work with.

            :stream             -   By default the streaming requests is set to True. That says to the program
                                    it shouldn't download the body of the response immediately.

            :timeout            -   By default or no timeout is specified explicitly, requests do not time out.
                                    So we need a given number of seconds to tell Request to stop waiting for a response.
                                    By default this function takes 10 seconds. With timeout we ensure that the program
                                    doesn't hang indefinitely.

            :return             -   Return a counter within a list.
        '''
        online_items_list = []

        #online_counter = None

        #normalized_online_record_list = []

        #   Use 'with' to ensure the session context is closed after use.
        #   We need to use a session object and send the authentication each request.
        with requests.Session() as s:
            # An authorised request.

            self.text_notify_update_signal.emit(self.tr("Connecting to the support-server") + ": " + url)
            self.text_notify_update_signal.emit(self.tr("Try to log in with username") + " '" + support_username + "'")

            #   By default we set stream to True, beccause we don't want the requests to download the the body of the response immediately.
            #   What did we do? So we have to override this behaviour.
            #   That means, we defer downloading the response body until we access the Response.content / Response.text.
            #   By setting stream to True, we know that requests can't release the connection back to the pool. Don't worry about,
            #   We are using the Session of requests within a with-statement to ensure it's always closed.
            r = s.get(url, verify = True, auth = HTTPBasicAuth(support_username, support_password), timeout = timeout, stream = stream)
            #   We test the response object in a boolean context

            if r:
                self.text_notify_update_signal.emit(self.tr("Logged in successfully with username") + "'" + support_username + "'")
                self.text_notify_update_signal.emit(self.tr("Connected to the support-server") + ": " + url)

                self.text_notify_update_signal.emit(self.tr('Online records are loaded and compared with the records in your database'))

                if any(element == category for element in analyze_categorie(list_name = "simple_list")):

                    online_items_list = scrape_no_name_taple(html_content = r.text)

                    #unicoded_list = unicode_record_list(item_list = online_items_list)

                    online_counter, online_items_list = remove_case_insensitive_duplicates_list(list_records = online_items_list)

                    #normalized_online_record_list = normalize_record_list(item_list = online_items_list)

                elif any(element == category for element in analyze_categorie(list_name = "list_two_pair_tuple")):

                    online_items_list = scrape_no_name_taple(html_content = r.text)

                    online_counter, online_items_list = remove_case_insensitive_duplicates_list_two_pair_tuple(list_tuple = online_items_list)

                    normalized_online_record_list = normalize_record_list_two_pair_tuple(list_tuple = online_items_list)

                self.text_notify_update_signal.emit(self.tr("Matching the records is done."))

            else:
                r.raise_for_status()

        return online_counter, online_items_list

    def populate_items(self,
                       **kwargs):
        '''
           SUMMARY
           =======
           This method is used to populate record(s).

           PARAMETERS
           ==========
           :**kwargs (dict):    The data structure dictionary is used to pass keyworded variable length
                                of arguments. We used it because we want to handle named arguments in
                                this method.

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return'
                              isn't necessary, but this statement gives me a better
                              feeling, that says: 'return' terminates a function definitely.
        '''
        try:

            if self._run_semaphore == 0:

                self._run_semaphore = 1

                raise StopIteration

            else:
                kwargs['general_standard_item_model'].populate_model(
                  tuple_items = next(self.element),
                  model = kwargs['certain_standard_item_model'],
                  show_image= kwargs.get('show_image'))

                if kwargs.get('hide_column', False):
                    self.hide_column_signal.emit(
                      kwargs['widget_tree_view'])

                self.count += 1
                self._count_minus += 1

                if not kwargs.get('widget_label', None) is None:
                    self.counting_label_signal.emit(
                        kwargs['widget_label'],
                        self.count)

                self.update_progress_bar_signal.emit(self._count_minus, self._total_row_count)

        except StopIteration:
            # end with to stopp the time
            end_time = datetime.now()

            # Show how much time has been elapsed.
            print('Duration: {}'.format(end_time - self.start_time))

            if self._resize_column:
                self.resize_to_contents_signal.emit(
                  kwargs['widget_tree_view'],
                  kwargs['resize_column'])

            logger.info("Before sending populate_proxy_model_signal --> \n {}".format(kwargs))

            self.set_enable_widget_signal.emit(False)

            self.populate_proxy_model_signal.emit(kwargs)

            result_row_count = self._kwargs.get('general_standard_item_model').row_count(
                model = self._kwargs.get('certain_standard_item_model'))

            self.set_text_label_signal.emit(
                self._kwargs.get('label_object'),
                result_row_count)

            self.sort_model_signal.emit(kwargs)

            self._kwargs.get('general_standard_item_model').sort_model(
                model = self._kwargs.get('certain_standard_item_model'))

            if kwargs.get('hide_column', False):
                self.hide_column_signal.emit(kwargs['widget_tree_view'])

            self.finish_task_thread()

    def open_temp_file(self,
                      tree_view,
                      certain_proxy_model,
                      general_proxy_model,
                      certain_standard_item_model,
                      **kwargs):

        support_username, support_password = self._fetch_support_login_data()

        url = self._fetch_support_link(support_category = self._category)

        is_are = self.tr("There is")
        result_results = self.tr("Result")
        item_items = self.tr("record")

        online_counter = None
        missing_item_counter = None
        #count_record_online_db = 0
        error_occurred = False

        category = self._category.split("_")[1].capitalize()
        replaced_category = category.replace("_", " ")

        master_data_manipulation = MasterDataManipulation(scoped_session = self._scoped_session)
        query_data = master_data_manipulation.fetch_all_items
        self.element = query_data(category = self._category)

        start_updating = self.tr("Start updating the master data of")
        start_update_text = start_updating + " '" + replaced_category + "'"

        self.text_notify_update_signal.emit(start_update_text)

        db_record_list = None
        online_items_list = None
        missing_items_list = []
        nfd_list = []

        try:

            self.create_wait_window_signal.emit(self.tr("Please wait while manipulating record(s)"))

            db_record_list = self._fetch_all_records_in_list(category = self._category)

            online_counter, online_items_list = self._fetch_all_online_data(category = self._category,
                                                                            url = url,
                                                                            support_username = support_username,
                                                                            support_password = support_password)

            missing_item_counter, missing_items_list = compare_two_simple_listss(category = self._category,
                                                                                 online_record_list = online_items_list,
                                                                                 database_record_list = db_record_list)

            nfd_list = missing_items_list

            # if any(element == self._category for element in analyze_categorie(list_name = "categories_simple_list")):

            #     nfd_list = normalize_record_list(item_list = missing_items_list)

            # elif any(element == self._category for element in analyze_categorie(list_name = "categories_list_two_pair_tuple")):

            #     nfd_list = normalize_record_list_two_pair_tuple(list_tuple = missing_items_list)

            #     print "subclass, open_temp_file, nfd_list", nfd_list

        except requests.exceptions.ConnectionError:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            connection_message = self.tr("Connection error when connecting to the HTTPS site. Unable to connect to remote host.")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("Connection Error"), connection_message, None)

            logger.error(desired_trace)

        except requests.exceptions.Timeout:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            timeout_message = self.tr("Timeout", "Request timed out. The remote host did not respond timely.")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("Timeout"), timeout_message, None)

            logger.error(desired_trace)

        except requests.exceptions.SSLError as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            message = self.tr("Unable to connect to remote host because of a SSL error. "
                        "It is likely that your system cannot verify the validity"
                        "of the certificate. The remote certificate is either "
                        "self-signed, or the remote server uses SNI. See the wiki for "
                        "more information on this topic.")

            if kwargs["verify"]:

                if self._show_messagebox:

                    self.error_message_signal.emit(self.tr("SSL Error"), message, None)

                logger.error(desired_trace)

            else:

                certificate_message = ("SSL error raised during connection, with certificate "
                                      "verification turned off: {e}".format(e = unicode(e)))

                if self._show_messagebox:

                    self.error_message_signal.emit(self.tr("SSL Error"), certificate_message, None)

                logger.error(desired_trace)

        except requests.exceptions.URLRequired as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            url_required_message = self.tr("A valid URL is required to make a request.")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("Missing URL"), url_required_message, None)

            logger.error(desired_trace)

        except requests.exceptions.InvalidURL as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            invalid_url_message = self.tr("The URL provided is invalid.")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("Invalid URL"), invalid_url_message, None)

            logger.error(desired_trace)

        except requests.exceptions.InvalidSchema as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            invalid_schema_message = self.tr("There is a URL without HTTP or HTTPS schema!")

            if self._show_messagebox:

                self.error_message_signal.emit(self.tr("Invalid Schema"), invalid_schema_message, None)

            logger.error(desired_trace)

        except requests.exceptions.HTTPError as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            title = "HTTP Error"

            if e.response is not None:

                if e.response.status_code >= 500:

                    if self._show_messagebox:

                        self.error_message_signal.emit(title, self.tr("Remote server error"), None)

                elif e.response.status_code >= 400:

                    if unicode(desired_trace).lower().find('unauthorized') != -1:

                        unauthorized_title = title + " " + self.tr("Unauthorized")

                        if self._show_messagebox:

                            self.error_message_signal.emit(unauthorized_title,
                                                       self.tr("Something is wrong. The log-in to your support account has failed."),
                                                       self.tr("Maybe the username and/or password you provided are incorrect. Please try again."))

                    else:

                        if self._show_messagebox:

                            self.error_message_signal.emit(title, self.tr("Local client error"), None)
                else:

                    if self._show_messagebox:
                        # I don't think we will end up here, but for completeness
                        self.error_message_signal.emit(title, "Unknow error: {e}".format(e = e), None)

                logger.error(desired_trace)

            else:

                raised_http_error_message = self.tr("Request raised HTTP error.")

                if self._show_messagebox:
                    self.error_message_signal.emit(title, raised_http_error_message, None)

                logger.error(desired_trace)

        except RequestException as e:

            error_occurred = True

            self.close_wait_message_signal.emit()

            desired_trace = format_exc(exc_info())

            title = "Unknown Error"

            raised_exception_message = "Request raised exception: {e}".format(e = e)

            if self._show_messagebox:

                self.error_message_signal.emit(title, raised_exception_message, None)

            logger.error(desired_trace)

        if not error_occurred:

            online_counter = 0

            if missing_item_counter > 1:
                is_are = self.tr("There are")
                item_items = self.tr("records")

            self.text_notify_update_signal.emit(self.tr('Checking is done.'))

            self.text_notify_update_signal.emit(is_are + " " + str(missing_item_counter) + " " + self.tr("new") + " " + item_items + " " + self.tr("in the online data service"))

            if missing_item_counter > 0:

                self.text_notify_update_signal.emit(self.tr('Saving all missing records to the database.'))

                if self._bulk_the_list:

                    self._bulking_list_records(tree_view,
                                               certain_proxy_model = certain_proxy_model,
                                               general_proxy_model = general_proxy_model,
                                               certain_standard_item_model = certain_standard_item_model,
                                               category = self._category,
                                               work_area = 'master_data',
                                               list_records = nfd_list,
                                               replaced_category = replaced_category,
                                               set_flush = True)

            else:
                self.close_wait_message_signal.emit()

                no_updates_for = self.tr("No updates for")
                avaible = self.tr("availble")

                no_update_text = no_updates_for + " '" + replaced_category + "' " + avaible

                if self._show_messagebox:

                    self.confirm_saving_successful_signal.emit(no_update_text, self.tr(""), self.tr(""))

                if kwargs.get('set_page', False):
                    self.change_page_signal.emit()

    def finish_task_thread(self):
        '''
           SUMMARY
           =======
           Here in this method we break/interrupt a running generator.
           This method leads to raise the GeneratorExit-Exception.

           The user decided to break or interrupt this loop - for instance when
           the user just closes the window while the generator is still running.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Finish the started task thread")

        self.stop_running_timer()

        self.quit_thread_signal.emit()
        self.wait_thread_signal.emit()

        self._run_semaphore = 0
        self._element = None

        return

    def stop_running_timer(self):
        '''
           SUMMARY
           =======
           This methos is used to stop running timer, which are saved in a list.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Stop all started timer")

        for timer in self.timer_list:
            timer.stop()

        #   When all timer are stopped, we don't need the elements anymore.
        #   So we have to clear the contents of the list.
        del self.timer_list[:]

        #   But we need the list again, so we reinitialize it.
        self.timer_list = []

        return

    '''
        #######################################
        ######   Manipulate Data items  ########
        #######################################
    '''
    def add_record(self,
                   **kwargs):
        '''
           SUMMARY
           =======
           This method is used to add new record in database.

           PARAMETERS
           ==========
           :set_flush (bool):   ...

           :**kwargs: (dict):   ...

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "TaskDataManipulation add_record is called successfully"
        print "TaskDataManipulation in add_record / **kwargs: -> \n {}".format(kwargs)
        try:
            self.create_wait_window_signal.emit(self.tr("Please wait while manipulating record(s)"))

            result_record = self._master_data_manipulation.add_record(**self._kwargs)

            self.clear_widget_object_content_signal.emit()

            self.remove_selected_be_edited_id_signal.emit()
            self.remove_current_selected_id_signal.emit()

            if self._kwargs.get('set_flush'):

                self._kwargs.get('general_standard_item_model').populate_model(
                  tuple_items = result_record,
                  model = self._kwargs.get('certain_standard_item_model'))

            if self._resize_column:
                self.resize_to_contents_signal.emit(
                    self._widget_tree_view,
                    self._resize_column)

            #self.close_wait_message_signal.emit()

            if self._show_messagebox:

                title = self.tr('Confirmation')
                msg = self.tr("The movie has been saved into the database successfully.")
                detail_msg = self.tr('Would you like to save another movie?')

                self.confirm_saving_successful_signal.emit(title, msg, detail_msg)

            self.finish_task_thread()

        except SQLAlchemyError as err:

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            self.close_wait_message_signal.emit()

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed!")
            fill_out_notice = self.tr("Please fill out the missing field and try again.")

            # How to use find()
            if unicode(err).lower().find('is not unique') != -1:

                self.MSG_ERROR_TITLE = self.tr("Error: Duplicate entry")

                conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            elif  unicode(desired_trace).find('1062') != -1:
                if unicode(desired_trace).lower().find('duplicate entry') != -1:

                    self.MSG_ERROR_TITLE = self.tr("Error: Duplicate entry")
                    conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            elif unicode(desired_trace).find('cannot be null') != -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n\n"
                    "The field named 'name normally used' must be filled! \n\n" +
                    fill_out_notice)

            elif unicode(desired_trace).find('may not be NULL') != -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n\n"
                    "The enter may not be empty or contain only spaces. \n\n" +
                    fill_out_notice)

            elif unicode(desired_trace).find('database is locked') != -1:

                self.MSG_ERROR_TITLE = self.tr("Error: Database is blocked")

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                    "This application can't insert your record, because the database (or one of its segments) is locked. That means, the database isn't accessible.\n\n" +
                    "Its possible that another application is accessing this database currently. "
                    "Please verify you have write permissions to this database and try again.")

            if self._show_messagebox:

                self.error_message_signal.emit(self.MSG_ERROR_TITLE, conn_err, None)

            self.finish_task_thread()

        except (AttributeError, TypeError):

            logger.info("An error has occurred")

            conn_err = "We are sorry, but your enter attempt failed!"
            msg_title = "An error has occurred"

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            self.close_wait_message_signal.emit()

            if desired_trace.find('self._session_scope') != -1  or \
               desired_trace.find('object is not callable') != -1 :

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

            # How to use find()
            if unicode(desired_trace).find("'NoneType' object has no attribute 'commit'") != -1 or \
                unicode(desired_trace).find("'NoneType' object has no attribute 'scoped_session'") != -1 :

                conn_err = self.tr("You can't work without a database connection. Please connect to the database and try again.")
                msg_title = self.tr("No database connection")

            if self._show_messagebox:

                self.error_message_signal.emit(msg_title, conn_err, '' )

            self.delete_widget_signal.emit()
            self.populate_proxy_model_signal.emit(self._kwargs)
            self.finish_task_thread()
            self.hide_column_signal.emit(self._widget_tree_view)
            self.close_wait_message_signal.emit()

        except:

            self.close_wait_message_signal.emit()

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            error_text = self.tr("We are sorry, but an unknown error has occurred.")
            msg_title = self.tr("Unknown error")

            if self._show_messagebox:

                self.error_message_signal.emit(msg_title, error_text, '' )

            self.delete_widget_signal.emit()
            self.populate_proxy_model_signal.emit(self._kwargs)
            self.finish_task_thread()
            self.hide_column_signal.emit(self._widget_tree_view)

        return

    def edit_record(self):
        '''
           SUMMARY
           =======
           This method is used to edit the existing record

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('Edit the existing record')

        try:

            self.create_wait_window_signal.emit(self.tr("Please wait while manipulating record(s)"))

            result_record = self._master_data_manipulation.edit_record(**self._kwargs)

            self.clear_widget_object_content_signal.emit()

            self.remove_selected_be_edited_id_signal.emit()
            self.remove_current_selected_id_signal.emit()

            self._kwargs.get('general_standard_item_model').remove_row(
                standard_item_model = self._kwargs.get('certain_standard_item_model'),
                item_id = int(self._kwargs.get('id')))

            if self._kwargs.get('set_flush', False):

                self._kwargs.get('general_standard_item_model').populate_model(
                  tuple_items = result_record,
                  model = self._kwargs.get('certain_standard_item_model'))

                self._kwargs.get('general_standard_item_model').synchronize_temporary_model(
                    work_around = "change_row",
                    model = self._kwargs.get('temp_model'),
                    list_tuple = result_record)

            self.count_model_items_signal.emit(
                self._kwargs.get('label_object'),
                str(self._kwargs.get('certain_standard_item_model').rowCount()))

            self.change_page_signal.emit()

            # Create a QTimer
            #model_timer = QTimer()
            #model_timer.singleShot(20, lambda: self.change_page_signal.emit(self._kwargs))
            #model_timer.singleShot(60, lambda: self.count_model_items_signal.emit(
            #    self._kwargs.get('label_object'),
            #    self._kwargs.get('certain_standard_item_model')))

            self.close_wait_message_signal.emit()

            if self._kwargs.get('show_messagebox', False):

                title = self.tr('Confirmation')
                msg = self.tr("The record has been changed into the database successfully.")
                detail_msg = ''

                self.confirm_saving_successful_signal.emit(title, msg, detail_msg)

        except SQLAlchemyError as err:

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            msg_title = "An error has occurred"
            conn_err = self.tr("We are sorry, but your enter attempt failed!")

            fill_out_notice = self.tr("Please fill out the missing field and try again.")

            # How to use find()
            if unicode(err).lower().find('is not unique') != -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            elif  unicode(desired_trace).find('1062') != -1:
                if unicode(desired_trace).lower().find('duplicate entry') != -1:

                    conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            elif unicode(desired_trace).find('cannot be null') != -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n\n"
                    "The field named 'name normally used' must be filled! \n\n" +
                    fill_out_notice)

            elif unicode(desired_trace).find('may not be NULL') != -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n\n"
                    "The enter may not be empty or contain only spaces. \n\n" +
                    fill_out_notice)

            self.close_wait_message_signal.emit()

            if self._kwargs.get('show_messagebox', False):

                self.error_message_signal.emit(msg_title, conn_err, '' )

            self.finish_task_thread()
            self.hide_column_signal.emit(self._widget_tree_view)

        except (AttributeError, TypeError) as AttrTypeErr:

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            conn_err = ""

            fill_out_notice = self.tr("Please fill out the missing field and try again.")

            self.close_wait_message_signal.emit()

            if not desired_trace.find('self._session_scope') == -1 or \
               not desired_trace.find('object is not callable') == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "You aren't connected to the database. Please, connect to the database and try again.")

            elif not desired_trace.find('Invalid Input') == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "An input is invalid. The enter may not be empty or contain only spaces. \n\n" +
                                    fill_out_notice)

            elif not desired_trace.find('object has no attribute') == -1:

                conn_err = self.tr("Oops, something went wrong. \n"
                                    "\n"
                                    "An internal error was experienced. ")


            if self._kwargs.get('show_messagebox', False):

                self.error_message_signal.emit(self.MSG_ERROR_TITLE, conn_err, None)

            self.finish_task_thread()
            self.hide_column_signal.emit(self._widget_tree_view)

        except:

            self.close_wait_message_signal.emit()

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            error_text = self.tr("We are sorry, but an unknown error has occurred.")
            msg_title = self.tr("Unknown error")

            if self._show_messagebox:

                self.error_message_signal.emit(msg_title, error_text, '' )

            self.delete_widget_signal.emit()
            self.populate_proxy_model_signal.emit(self._kwargs)
            self.finish_task_thread()
            self.hide_column_signal.emit(self._widget_tree_view)

        return

    def delete_record(self):
        '''
           SUMMARY
           =======
           This method is used to delete the existing record.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        try:

            self._master_data_manipulation.delete_record(**self._kwargs)

            self.remove_current_selected_id_signal.emit()
            self.remove_selected_be_edited_id_signal.emit()
            self.clear_widget_object_content_signal.emit()

            if self._kwargs.get('set_flush'):

                self._kwargs.get('general_standard_item_model').remove_row(
                    standard_item_model = self._kwargs.get('certain_standard_item_model'),
                    item_id = int(self._kwargs.get('id')))

                self._kwargs.get('general_standard_item_model').synchronize_temporary_model(
                    work_around = "remove_row",
                    model = self._kwargs.get('temp_model'),
                    list_tuple = (self._kwargs.get('id'),))

            self.count_model_items_signal.emit(
                self._kwargs.get('label_object'),
                str(self._kwargs.get('certain_standard_item_model').rowCount()))


            if self._kwargs.get('resize_column'):
                self.resize_to_contents_signal.emit(self._kwargs.get('widget_tree_view'), self._kwargs.get('resize_column'))

            if self._kwargs.get('close_window'):
                self.close_wait_message_signal.emit()

            self.finish_task_thread()

        except SQLAlchemyError as err:

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed!")

            server_said = "The server said: {server_said}".format(server_said=str(err[0]))

            if self._kwargs.get('show_messagebox'):

                self.error_message_signal.emit(self.MSG_ERROR_TITLE, conn_err, server_said)

            self.finish_task_thread()

        except (AttributeError, TypeError, ValueError):

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            self.close_wait_message_signal.emit()

            if not desired_trace.find('self._session_scope') == -1 or \
               not desired_trace.find('object is not callable') == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

                if self._kwargs.get('show_messagebox'):

                    self.error_message_signal.emit(self.MSG_ERROR_TITLE, conn_err, None)

            if not desired_trace.find("invalid literal for int() with base 10:") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                if self._kwargs.get('show_messagebox'):

                    self.error_message_signal.emit(self.MSG_ERROR_TITLE, conn_err, None)

            self.finish_task_thread()

        return

class LoadDataItem(QObject):
    '''
        NOTICE
        ======
        This subclass, which is using moveToThread, communicates
        with controler named MasterDataManipulation. This controler knows
        the Database model.

        INSTANCE ATTRIBUTES
        ==========
        self.query_data = query_data
        self.tuples_list_push_button_boolean = tuples_list_push_button_boolean


        self._count             -   By default its set on zero. When we populate
                                    the TreeView()-object, the user should know
                                    how much data item has been populated.

        :self._run_semaphore    -   By default its set on 1. Its like a flag
                                    for lock mechanism. If the falg is set to 0
                                    it will block and stop working this subclass.


        self.master_data_manipulation = MasterDataManipulation(session_object=self._session_scope)

        SIGNALS
        =======

    '''

    count_row_signal = pyqtSignal(int)
    query_failed_signal = pyqtSignal(str, str, str)
    confirm_saving_successful_signal = pyqtSignal(str, str)
    select_all_signal = pyqtSignal(str)
    set_hide_column_signal = pyqtSignal(object)
    set_header_data_signal = pyqtSignal()
    standard_item_model_signal = pyqtSignal()

    close_window_signal = pyqtSignal()

    combo_box_set_enable_signal = pyqtSignal()

    clear_widget_object_content_signal = pyqtSignal()
    remove_current_selected_id_signal = pyqtSignal()
    remove_selected_be_edited_id_signal = pyqtSignal()

    set_column_hide_signal = pyqtSignal()
    populate_item_signal = pyqtSignal(object)
    populate_input_signal = pyqtSignal(object)

    set_hide_widget_signal = pyqtSignal(bool)

    select_all_general_country_signal = pyqtSignal()

    set_enabled_widget_signal = pyqtSignal()
    set_focus_signal = pyqtSignal()
    set_source_model_signal = pyqtSignal()

    quit_thread_signal = pyqtSignal()

    remove_item_list_signal = pyqtSignal()

    update_progress_bar_signal = pyqtSignal(int, int)

    #delete_activated_thread_signal = pyqtSignal()

    delete_activated_thread_signal = pyqtSignal(object, str)

    count_progress_signal = pyqtSignal(int)

    close_window_signal = pyqtSignal()


    def __init__(self, category = None,
                       configuration_saver_dict = None,
                       timer_interval = 1,
                       query_by_id = None,
                       data_id = None,
                       parent = None):

        QObject.__init__(self, parent)

        self._category = category
        self._configuration_saver_dict = configuration_saver_dict
        self.timer_interval = timer_interval
        self._query_by_id = query_by_id
        self._data_id = data_id

        '''
            Create constants.
        '''
        self.MSG_ERROR_TITLE = self.tr('Error...')

        ''' Create attributes '''
        self._run_semaphore = 1
        self._count = 0
        self._hidden_widget = False
        self._count_minus = -1
        self._total_row_count = 0

        self._element = None


    def init_object(self):

        self.timer = QTimer()

        '''
            Storing new generator object, will reuse it.
            That means you have to create one generator.
        '''
        master_data_manipulation = MasterDataManipulation()

        if not self._query_by_id:

            try:

                query_data = master_data_manipulation.fetch_all_items

                self._element = query_data(category = self._category)

                self._total_row_count = master_data_manipulation.count_rows(category = self._category)

                self.standard_item_model_signal.emit()

                self.set_header_data_signal.emit()

                self.count_progress_signal.emit(self._total_row_count)

                # assoziiert select_all_data() mit TIMEOUT Ereignis
                self.timer.setSingleShot(False)
                self.timer.setInterval(self.timer_interval)
                self.timer.timeout.connect(self.populate_item)
                self.timer.start()

            except UnboundExecutionError as UnExErr:

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "Unable to connect to the database. \n\n"
                 "Be sure your are connected to the database. Please, connect to the database and try again.")

                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, '')

        else:
            # Select objects
            person = master_data_manipulation.query_by_id(id = self._data_id, category = self._category)

            self.populate_input_signal.emit(person)


        # assoziiert select_all_data() mit TIMEOUT Ereignis
        #self.timer.setSingleShot(False)
        #self.timer.setInterval(self.timer_interval)
        #self.timer.timeout.connect(self.populate_item)
        #self.timer.start()

    def count_result(self):
        self._count += 1

        self.count_row_signal.emit(self._count)

    def populate_item(self):

        try:

            if not self._hidden_widget:

                self._hidden_widget = True

                self.set_hide_widget_signal.emit(True)


            if self._run_semaphore == 0:

                self._run_semaphore = 1

                raise StopIteration

            else:
                self.populate_item_signal.emit(next(self._element))
                self.set_hide_column_signal.emit()
                self.set_header_data_signal.emit()
                self.count_result()
                self.update_progress_bar_signal.emit(self._count, self._total_row_count)

        except StopIteration:
            self.set_enable_widget_signal.emit()
            self._hidden_widget = False
            self.set_hide_widget_signal.emit(False)
            self.set_source_model_signal.emit()
            master_data_manipulation = None
            self.stop_task_thread()

            #self._session.close()
            # self.set_column_hide_signal.emit()
            # self.timer.stop()
            # self._element = None
            # self.quit_thread_signal.emit()

        # except GeneratorExit:

        #     '''

        #     '''
        #     print "GeneratorExit is raised"
        #     self.stop_timer()#self.timer.stop()
        #     self._element = None

        #     '''
        #         After the timer was stopped successfully, we
        #         emit the close_window_signal()-signal. This is important:
        #         Reason: When we close the current window on main-thread (MasterData_Window),
        #         there a RuntimeError was raised, that says a object has been deleted in c++.
        #         That means the object has already been deleted while this sub-thread is
        #         still trying to access on some objects in the main-thread (MasterData_Window).
        #         So we close the the current window by sending a signal.
        #     '''

        #     #self.close_window_signal.emit()

        except UnboundExecutionError as UnExErr:

            title_text = None

            if not unicode(UnExErr).find("Could not locate a bind configured on mapper Mapper") == -1:

                title_text = self.tr("No database connection!")

                conn_err = self.tr("We are sorry for this error, but your connect attempt failed! \n\n"
                 "Unable to connect to the database. \n\n"
                 "Be sure your are connected to the database. Please, connect to the database and try again.")

            detail = "Detail: {detail}".format(detail=str(UnExErr[0]))

            self.query_failed_signal.emit(self.tr(title_text), conn_err, "")

            logger.error(format_exc(exc_info()))

        except SQLAlchemyError as err:

            conn_err = self.tr("Ups, an error has occurred. We are sorry about, but your query attempt failed! \n "
             "Unable to connect to the database. This program losts the connection. \n\n "
             "Please... \n "
             "... check that MySQL is still running. \n"
             "... check that your network connection online. \n "
             "... restart your MySQL server and try again. \n")

            server_said = "The server said: {server_said}".format(server_said=str(err[0]))

            self.query_failed_signal.emit(self.tr("ERROR: Lost connection"), conn_err, server_said)

        except (AttributeError, TypeError) as AttTypErr:

            title_text = None
            conn_err = ""

            if not unicode(AttTypErr).find('self._session_scope') == int(-1) or \
               not unicode(AttTypErr).find('None bject is not callable') == int(-1):

                title_text = "No database connection!"

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

            detail = "Detail: {detail}".format(detail=str(AttTypErr[0]))

            self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, '')

            logger.error(format_exc(exc_info()))


    def abort_task_thread(self):
        '''
            NOTICE:
            =======
            Here in this method we break/interrupt a running generator.
            This method leads to raise the GeneratorExit-Exception.

            The user decided to break or interrupt this loop - for instance when
            the user just closes the window while the generator is still running.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        self._hidden_widget = False
        self.set_hide_widget_signal.emit(False)
        master_data_manipulation = None
        self.timer.stop()
        self.quit_thread_signal.emit()
        self._run_semaphore = 0
        #self.timer = None
        self._element = None

        return

    def stop_task_thread(self):
        '''
            NOTICE:
            =======
            This method is used to stop the still running generator by just stopping the timer.
            No Exception is raised. Its a simple stopper.

            The user decided to break or interrupt this loop - for instance when
            the user just closes the window while the generator is still running.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        #self.combo_box_set_enable_signal.emit()

        self._hidden_widget = False

        self.set_hide_widget_signal.emit(False)

        master_data_manipulation = None

        self.quit_thread_signal.emit()

        self.timer.stop()#self.timer.stop()

        self._count = 0

        #self.delete_activated_thread_signal.emit()

        self._element = None

        #self.quit_thread_signal.emit()

        #self.set_enabled_widget_signal.emit(self.tuples_list_push_button_boolean)

        self.set_enabled_widget_signal.emit()

        self.set_focus_signal.emit()

        return

    '''
        #######################################
        ######   Manipulate Data items  ########
        #######################################
    '''
    def add_record(self,
                   film_genre = None,
                   general_country = None,
                   general_language = None,
                   general_region = None,
                   general_award = None,
                   person_gender = None,
                   person_nationality = None,
                   person_salutation = None,
                   person_title = None,
                   person_hair_color = None,
                   person_eye_color = None,
                   person_religion = None,
                   person_relationship_status = None,
                   person_relationship = None,
                   person_name_normally_used = None,
                   person_nickname = None,
                   person_artist_name = None,
                   person_first_middle_name = None,
                   person_birth_name = None,
                   person_birth_date = None,
                   person_death_date = None,
                   person_wedding_date = None,
                   person_last_name = None,
                   person_note = None,
                   person_body_height = None,
                   person_file_extension_photo = None,
                   combo_person_salutation = None,
                   combo_person_gender = None,
                   combo_person_title = None,
                   combo_person_religion = None,
                   combo_person_relationsship_status = None,
                   combo_person_eye_color = None,
                   combo_person_hair_color= None,
                   combo_place = None,
                   category = None,
                   send_signal = True,
                   image_path = None,
                   record_added_day = None,
                   work_area = None):

        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :film_genre    -        We except a content to add it
                                    to the database.

            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        try:
            master_data_manipulation = MasterDataManipulation(configuration_saver_dict = self._configuration_saver_dict)
            master_data_manipulation.add_record(film_genre = unicode(film_genre),
                                                general_country = unicode(general_country),
                                                general_language = unicode(general_language),
                                                general_region = unicode(general_region),
                                                general_award = unicode(general_award),
                                                person_gender = unicode(person_gender),
                                                person_nationality = unicode(person_nationality),
                                                person_salutation = unicode(person_salutation),
                                                person_title = unicode(person_title),
                                                person_hair_color = unicode(person_hair_color),
                                                person_eye_color = unicode(person_eye_color),
                                                person_religion = unicode(person_religion),
                                                person_relationship_status = unicode(person_relationship_status),
                                                person_relationship = unicode(person_relationship),
                                                person_name_normally_used = unicode(person_name_normally_used),
                                                person_nickname = unicode(person_nickname),
                                                person_artist_name = unicode(person_artist_name),
                                                person_first_middle_name = unicode(person_first_middle_name),
                                                person_birth_name = unicode(person_birth_name),
                                                person_birth_date = person_birth_date,
                                                person_death_date = person_death_date,
                                                person_wedding_date = person_wedding_date,
                                                person_last_name = unicode(person_last_name),
                                                person_note = unicode(person_note),
                                                person_body_height = unicode(person_body_height),
                                                person_file_extension_photo = unicode(person_file_extension_photo),
                                                combo_person_salutation = unicode(combo_person_salutation),
                                                combo_person_gender = unicode(combo_person_gender),
                                                combo_person_title = unicode(combo_person_title),
                                                combo_person_religion = unicode(combo_person_religion),
                                                combo_person_relationsship_status = unicode(combo_person_relationsship_status),
                                                combo_person_eye_color = unicode(combo_person_eye_color),
                                                combo_person_hair_color = unicode(combo_person_hair_color),
                                                image_path = unicode(image_path),
                                                combo_place = unicode(combo_place),
                                                record_added_day = record_added_day,
                                                category = category,
                                                work_area = work_area)


            self.select_all_signal.emit(category)

            self.clear_widget_object_content_signal.emit()

            self.remove_selected_be_edited_id_signal.emit()

            if self._show_messagebox:

                self.confirm_saving_successful_signal.emit()

        except SQLAlchemyError as err:

            desired_trace = traceback.format_exc(exc_info())

            logger.error(desired_trace)

            #print "desired_trace", desired_trace

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed!")

            # How to use find()
            if unicode(err).lower().find('is not unique') != -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            if  unicode(desired_trace).find('1062') != -1:
                if unicode(desired_trace).lower().find('duplicate entry') != -1:

                    conn_err = self.tr("We are sorry, but your enter attempt failed! \nThere is a duplicate entry!")

            elif unicode(desired_trace).find('cannot be null') != -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n\n"
                    "The field named 'name normally used' must be filled! \n\n"
                    "Please fill the missing field and try again.")

            if send_signal:
                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, '')

        except (AttributeError, TypeError):

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            if desired_trace.find('self._session_scope') != -1  or \
               desired_trace.find('object is not callable') != -1 :

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, '')

        return

    def edit_record(self,
                    id = None,
                    film_genre = None,
                    general_country = None,
                    general_language = None,
                    general_region = None,
                    general_award = None,
                    person_gender = None,
                    person_nationality = None,
                    person_salutation = None,
                    person_title = None,
                    person_hair_color = None,
                    person_eye_color = None,
                    person_religion = None,
                    person_relationship_status = None,
                    person_relationship = None,
                    person_name_normally_used = None,
                    person_nickname = None,
                    person_artist_name = None,
                    person_first_middle_name = None,
                    person_birth_name = None,
                    person_birth_date = None,
                    person_death_date = None,
                    person_wedding_date = None,
                    person_last_name = None,
                    person_note = None,
                    person_body_height = None,
                    person_file_extension_photo = None,
                    combo_person_salutation = None,
                    combo_person_gender = None,
                    combo_person_title = None,
                    combo_person_religion = None,
                    combo_person_relationsship_status = None,
                    combo_person_eye_color = None,
                    combo_person_hair_color= None,
                    combo_place = None,
                    category = None,
                    image_path = None,
                    save_image = False,
                    record_edited_day = None):
        '''
            NOTICE:
            =======
            This method is used to edit the existing record: here in Film-GEnre-Table.

            PARAMETERS:
            ===========
            :film_genre_id  -       We except an ID to finde it in
                                    the database.

            :film_genre     -       We get a content to edit the existing
                                    record.

            :return         -       Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        try:
            master_data_manipulation = MasterDataManipulation(configuration_saver_dict = self._configuration_saver_dict)
            master_data_manipulation.edit_record(id = unicode(id),
                                                film_genre = unicode(film_genre),
                                                general_country = unicode(general_country),
                                                general_language = unicode(general_language),
                                                general_region = unicode(general_region),
                                                general_award = unicode(general_award),
                                                person_gender = unicode(person_gender),
                                                person_nationality = unicode(person_nationality),
                                                person_salutation = unicode(person_salutation),
                                                person_title = unicode(person_title),
                                                person_hair_color = unicode(person_hair_color),
                                                person_eye_color = unicode(person_eye_color),
                                                person_religion = unicode(person_religion),
                                                person_relationship_status = unicode(person_relationship_status),
                                                person_relationship = unicode(person_relationship),
                                                person_name_normally_used = unicode(person_name_normally_used),
                                                person_nickname = unicode(person_nickname),
                                                person_artist_name = unicode(person_artist_name),
                                                person_first_middle_name = unicode(person_first_middle_name),
                                                person_birth_name = unicode(person_birth_name),
                                                person_birth_date = person_birth_date,
                                                person_death_date = person_death_date,
                                                person_wedding_date = person_wedding_date,
                                                person_last_name = unicode(person_last_name),
                                                person_note = unicode(person_note),
                                                person_body_height = unicode(person_body_height),
                                                person_file_extension_photo = unicode(person_file_extension_photo),
                                                combo_person_salutation = unicode(combo_person_salutation),
                                                combo_person_gender = unicode(combo_person_gender),
                                                combo_person_title = unicode(combo_person_title),
                                                combo_person_religion = unicode(combo_person_religion),
                                                combo_person_relationsship_status = unicode(combo_person_relationsship_status),
                                                combo_person_eye_color = unicode(combo_person_eye_color),
                                                combo_person_hair_color = unicode(combo_person_hair_color),
                                                image_path = unicode(image_path),
                                                save_image = save_image,
                                                combo_place = unicode(combo_place),
                                                record_edited_day = record_edited_day,
                                                category = category)

            self.select_all_signal.emit(category)


            if self._show_messagebox:

                self.confirm_saving_successful_signal.emit()

            self.remove_current_selected_id_signal.emit()
            self.remove_selected_be_edited_id_signal.emit()
            self.clear_widget_object_content_signal.emit()

        except SQLAlchemyError as err:

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed!")

            server_said = "The server said: {server_said}".format(server_said=str(err[0]))

            self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, server_said)

        except (AttributeError, TypeError):

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            if not desired_trace.find('self._session_scope') == -1 or \
               not desired_trace.find('object is not callable') == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, "")

        return

    def delete_record(self, id=None, category=None):
        '''
            NOTICE:
            =======
            This method is used to delete the existing record: here in Film-Genre-Table.

            PARAMETERS:
            ===========
            :id         -       We except an ID to delete it from the database.

            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        try:
            master_data_manipulation = MasterDataManipulation()
            master_data_manipulation.delete_record(id = unicode(id),
                                                   category = category)

            self.select_all_signal.emit(category)

            self.remove_current_selected_id_signal.emit()
            self.remove_selected_be_edited_id_signal.emit()
            self.clear_widget_object_content_signal.emit()
            self.close_window_signal.emit()

        except SQLAlchemyError as err:

            logger.info("An error has occurred")

            conn_err = self.tr("We are sorry, but your enter attempt failed!")

            server_said = "The server said: {server_said}".format(server_said=str(err[0]))

            self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, server_said)

        except (AttributeError, TypeError, ValueError):

            desired_trace = traceback.format_exc(exc_info())
            logger.error(desired_trace)

            if not desired_trace.find('self._session_scope') == -1 or \
               not desired_trace.find('object is not callable') == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Reason: You aren't connected to the database. Please, connect to the database and try again.")

                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, "")

            if not desired_trace.find("invalid literal for int() with base 10:") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item. You must select an item to delete.")

                self.query_failed_signal.emit(self.MSG_ERROR_TITLE, conn_err, "")


        return




