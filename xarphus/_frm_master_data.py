#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from os import path
from sys import argv, exit, exc_info
from traceback import format_exc

from sqlalchemy.exc import SQLAlchemyError

from PyQt4.QtCore import QFile, Qt, QTimer, QObject, pyqtSignal, \
                        QThread, QRegExp, QTime, QDateTime, QDate, QSize, \
                        pyqtSlot

from PyQt4.uic import loadUi

from PyQt4.QtGui import QWidget, QMdiSubWindow, QIcon, QMessageBox, QPushButton, QTreeWidgetItem, \
                        QLineEdit, QStandardItemModel, QAbstractItemView, QAction, QMenu, \
                        QTextEdit, QPlainTextEdit, QTreeView, QStandardItem, QPixmap, QItemDelegate

try:

    from xarphus.core.manage_data_manipulation_master_data import MasterDataManipulation
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    from xarphus.core.manage_input_controller import InputController
    from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel
    from xarphus.core.manage_prase import parsing_str_to_bool
    #from xarphus.core

    from xarphus.subclass_master_data_load_data_item import LoadDataItem

except ImportError:

    from core.manage_data_manipulation_master_data import MasterDataManipulation
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint, custom_message_box
    from core.manage_input_controller import InputController
    from core.custom_sort_filter_model import MySortFilterProxyModel
    from core.manage_prase import parsing_str_to_bool

    from subclass_master_data_load_data_item import LoadDataItem

BASE_PATH = path.dirname(path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

UI_PATH = path.join(BASE_PATH, 'images', 'xarphus_x_logo.png')
print "UI_PATH", UI_PATH
#UI_PATH = QFile(":/ui_file/update.ui")

class MyDelegate(QItemDelegate):      
   def __init__(self):    
       QItemDelegate.__init__(self)  

   def sizeHint(self, option, index):  
       return QSize(32,32)

class MasterData_Window(QWidget):

    sotp_working_thread_signal = pyqtSignal()
    interrupt_working_thread_signal = pyqtSignal()

    set_flat_signal = pyqtSignal(str, bool)

    def __init__(self, 
                 image_path_collection=None,
                 dict_custom_config=None,
                 create_error_form=None,
                 session_scope=None,
                 stack_widget_page= None, 
                 parent=None):

        QWidget.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.stack_widget_page = stack_widget_page
        self.image_path_collection = image_path_collection
        self._session_scope = session_scope
        self._dict_custom_config = dict_custom_config

        '''
            Store constance
        '''
        self.LIST_HEADER_GENRE_FILM = [self.tr("ID"), self.tr("Genre")]
        self.LIST_HEADER_GENERAL_COUNTRY = [self.tr("ID"), self.tr("Country")]
        self.LIST_HEADER_GENERAL_LANGUAGE = [self.tr("ID"), self.tr("Language")]
        self.LIST_HEADER_PERSON_GENDER = [self.tr("ID"), self.tr("Gender")]
        self.LIST_HEADER_PERSON_NATIONALITY = [self.tr("ID"), self.tr("Nationality")]
        self.LIST_HEADER_PERSON_SALUTATION = [self.tr("ID"), self.tr("Salutation")]
        self.LIST_HEADER_PERSON_TITLE = [self.tr("ID"), self.tr("Title")]
        self.LIST_HEADER_PERSON_HAIR_COLOR = [self.tr("ID"), self.tr("Hair Color")]
        self.LIST_HEADER_PERSON_EYE_COLOR = [self.tr("ID"), self.tr("Eye Color")]
        self.LIST_HEADER_PERSON_RELIGION = [self.tr("ID"), self.tr("Religion")]
        self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS = [self.tr("ID"), self.tr("Relationship Status")]
        self.LIST_HEADER_PERSON_RELATIONSHIP = [self.tr("ID"), self.tr("Relationship")]
        self.LIST_HEADER_GENERAL_REGION = [self.tr("ID"), self.tr("Region")]
        self.LIST_HEADER_GENERAL_AWARD = [self.tr("ID"), self.tr("Award")]

        self.NAVI_DATA = [

            (self.tr("Film / Serie"), [
                (self.tr("Genre"), []),
                (self.tr("Regional code"), []),
                (self.tr("Aspect ratio"), []),
                (self.tr("Audio format"), []),
                (self.tr("Color format"), []),
                (self.tr("TV standard"), []),
                (self.tr("Resolution"), []),
                (self.tr("Video format"), []),
                (self.tr("Type of version"), []),
                (self.tr("Dimension"), []),
                (self.tr("Version / Edition"), []),
                (self.tr("Manufacturer"), []),
                (self.tr("Distribution"), []),
                (self.tr("Movie rentals"), []),
                (self.tr("Label"), []),
                ]),
            (self.tr("Music"), [
                (self.tr("Genre"), []),
                (self.tr("Studio"), []),
                (self.tr("Label"), []),
                ]),

            (self.tr("Book"), [
                (self.tr("Genre"), []),
                (self.tr("Publisher"), []),
                (self.tr("Binding"), []),
                (self.tr("Font size"), []),
                (self.tr("Color format"), []),
                ]),

            (self.tr("Video game"), [
                (self.tr("Genre"), []),
                ]),

            (self.tr("Person"), [
                (self.tr("Gender"), []),
                (self.tr("Salutation"), []),
                (self.tr("Title"), []),
                (self.tr("Nationality"), []),
                (self.tr("Eye color"), []),
                (self.tr("Hair color"), []),
                (self.tr("Religion"), []),
                (self.tr("Relationship status"), []),
                (self.tr("Relationship"), []),
                ]),

            (self.tr("Stamp"), [
                ("Blah", []),
                ]),

            (self.tr("Coin"), [
                (self.tr("Type of coin"), []),
                (self.tr("Alloy"), []),
                (self.tr("Currency"), []),
                ]),

            (self.tr("Age release"), [
                ([], []),
                ]),

            (self.tr("Packaging"), [
                ([], []),
                ]),

            (self.tr("Place of storage"), [
                ([], []),
                ]),

            (self.tr("Type of media"), [
                ([], []),
                ]),

            (self.tr("Language"), [
                ([], []),
                ]),

            (self.tr("Award"), [
                ([], []),
                ]),

            (self.tr("Country"), [
                ([], []),
                ]),

            (self.tr("Region / Place"), [
                ([], []),
                ]),

            (self.tr("Collection"), [
                ([], []),
                ]),
            ]

        '''
            Create an instance of imported class.
        '''
        self.input_controller = InputController()

        '''
            Create all attributes
        '''
        self.flags=None
        self.current_selected_id = None
        self.selected_be_edited_id = None
        self.activated_thread = None
        self._new_stack_page = None
        self._old_stack_page = None

        self.navi_count = 0

        self._list_threads = []


        self._proxy_model = None
        self._navigate_proxy_model = None
        self.set_proxy_model()

        self._standard_item_model = None

        self._standard_item_model_person_salutation = None
        self._standard_item_model_person_title = None
        self._standard_item_model_person_gender = None
        self._standard_item_model_person_religion = None
        self._standard_item_model_person_eye_color = None
        self._standard_item_model_person_hair_color = None
        self._standard_item_model_person_relationship_status = None
        self._standard_item_model_person_relationship = None
        self._standard_item_model_person_nationality = None
        self._standard_item_model_film_genre = None
        self._standard_item_model_general_language = None
        self._standard_item_model_general_award = None
        self._standard_item_model_general_country = None
        self._standard_item_model_general_region = None

        self._navigate_standard_model = None

        self.set_standard_item_model()

        UI_PATH = QFile(":/ui_file/master_data.ui")

        self.load_ui_file(UI_PATH)

        self.init_ui()

        self.stack_set_current_index(index=self.stack_widget_page, 
                                     stack_widget_object=self.ui_master_data.stackedWidget,
                                     column_index=1)

        #self.set_source_model(model=self._standard_item_model, mode="database")
        #self.set_source_model(model=self._navigate_standard_model, mode="navigation")

#        self.set_header_data(list_header_data=["ID", "Genre"])

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_master_data = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def get_name_back(self):
        print "get_name_back is called"
        return "Master-Dings"

    def init_ui(self):
        '''
            NOTE:
            ======
            This method initializes the user interface of master_data-window. 

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.setInterval(800)
        self.timer.timeout.connect(self.filter_update)

        self.flags = flag_dialog_titlehint_customizewondowhint()

        self.set_hide_column(tree_view_object=self.ui_master_data.treeView_navigation,
                             column=1,
                             bool_value=True)

        #self.set_visible_tree_widget_column(tree_widget_object=self.ui_master_data.treeWidget_navigation,
        #                                    column_integer=1)

        self.set_window_icon(image=self.image_path_collection.masta_data_icon_16x16_default)

        self.set_window_title(title=self.tr("Manage master data"))

        self.init_tree_view()

        self.init_navi_treeview()

        self.init_clicked_signal_push_buttons()
        self.init_item_clicked_signal_tree_widget()

        self.init_selectionChanged_signal_treeview()
        
        self.init_lineEdit_pixmap()

        self.init_textChanged_signal_line_edit()

        self.init_returnPressed_signal_line_edit()

        return

    def init_navi_treeview(self):

        self.populate_navi_data(model_obj=self._navigate_standard_model, data=self.NAVI_DATA)

        self.set_source_model(model=self._navigate_standard_model, mode="navigation")

        self.set_hide_column(tree_view_object=self.ui_master_data.treeView_navigation, 
                            column=1, 
                            bool_value=True)

        self.ui_master_data.treeView_navigation.expandAll()

    def populate_navi_data(self, model_obj=None, data=None):

        try:
 
            for parent_text, child_text in data:
                count_item = QStandardItem('{}'.format(self.navi_count))
                item = QStandardItem(parent_text)
                model_obj.appendRow([item, count_item]) 
                self.navi_count += 1
                
                if child_text:
                    self.populate_navi_data(item, child_text)
 
            # for parent_text, child_text in data:
            #     count_item = QStandardItem('{}'.format(self.navi_count))
            #     item = QStandardItem(parent_text)
            #     model_obj.appendRow([item, count_item])
            #     self.navi_count += 1
               
            #     if child_text:
            #         for sub_children_text, _ in child_text:
            #             count_item_1 = QStandardItem('{}'.format(self.navi_count))
            #             item_1 = QStandardItem(sub_children_text)
            #             item.appendRow([item_1, count_item_1])
            #             self.navi_count += 1
        except TypeError:
            pass

    def open_context_menu(self, position,  
                                tree_view_obj=None,
                                pushbutton_object_edit=None,
                                pushbutton_object_delete=None):
    
        indexes = tree_view_obj.selectedIndexes()

        if len(indexes) > 0:

            delete_action = QAction(QIcon(self.image_path_collection.delete_default), self.tr("Delete"), self)        
            delete_action.triggered.connect(pushbutton_object_delete.click)

            edit_action = QAction(QIcon(self.image_path_collection.edit_default), self.tr("Edit"), self)        
            edit_action.triggered.connect(pushbutton_object_edit.click)
        
        
            menu = QMenu()
            menu.addAction(edit_action)
            menu.addAction(delete_action)
            
            menu.exec_(tree_view_obj.viewport().mapToGlobal(position))

        return

    def create_generally_crritcal_msg(self, err_title, err_msg, detail_msg):
        mBox = custom_message_box(self, err_msg, err_title, QMessageBox.Critical)
        #setIc_OK = QIcon(self.image_pathn_collection.check_icon)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_msg)
        mBox.addButton(
            #QPushButton(setIc_OK, self.tr(" Ok")),
            QPushButton(self.tr(" Ok")),
            mBox.AcceptRole) # An "OK" button defined with the AcceptRole

        mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

    def create_generally_question_msg(self, text = None, title = None, icon = None, detail_text = None):

        mBox = custom_message_box(self, text = text, title = title, icon = icon)
        setIc_OK = QIcon(self.image_path_collection.check_icon_default)
        setIc_No = QIcon(self.image_path_collection.no_default)
        #mBox.setDetailedText(detail_msg)
        mBox.setInformativeText(detail_text)

        mBox.addButton(QPushButton(setIc_OK, self.tr("Yes")),
                    mBox.YesRole)  # An "OK" button defined with the AcceptRole
        
        mBox.addButton(
                    QPushButton(setIc_No, self.tr("No")),
                    mBox.NoRole)  # An "OK" button defined with the AcceptRole

        clicked_result = mBox.exec_()    # QDialog.exec_ is the way that it blocks window
        # execution and allows a developer to wait for user
        # input before continuing.

        if clicked_result == 0:

            return True

        elif clicked_result == 1:

            return False


    def handle_selection_changed(self, new_index=None, old_index=None, edit_line_obj=None):
        '''
            NOTICE:
            =======

            This method gets two QItemSelection, 
            the new selection and the old. 
        '''
        tuples_list_push_push_button = [(self.ui_master_data.pushButton_delete_general_country, True)]

        try: #if qItemSelection
            self.selected_be_edited_id = new_index.indexes()[0].data()
            self.current_selected_id = new_index.indexes()[0].data()

            print "selected_be_edited_id", self.selected_be_edited_id
            print "current_selected_id", self.current_selected_id
            
            edit_line_obj.setText(new_index.indexes()[1].data())

            if self.selected_be_edited_id:


                self.set_enabled_widget(two_pair_tuples_in_list_widget_boolean=tuples_list_push_push_button)

        except Exception as er: #if qModelIndex
            pass

    def init_item_clicked_signal_tree_widget(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots of clicked for QTreeWidget()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QTreeWidget()-object(s)")

        # self.ui_master_data.treeWidget_navigation.itemSelectionChanged.connect(lambda: 
        #     self.stack_set_current_index(tree_widget_object=self.ui_master_data.treeWidget_navigation, 
        #         stack_widget_object=self.ui_master_data.stackedWidget, column_index=1))

        self.ui_master_data.treeView_navigation.selectionModel().selectionChanged.connect(lambda new_index: 
            self.stack_set_current_index(new_index=new_index, tree_view_object=self.ui_master_data.treeView_navigation, 
                stack_widget_object=self.ui_master_data.stackedWidget, column_index=1))

        # self.ui_master_data.comboBox_film_genre.view().selectionModel().selectionChanged.connect(
        #     lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_film_genre:
        #     self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        #self.ui_master_data.comboBox_film_genre.view().selectionModel().selectionChanged.connect(
        #    lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_film_genre:
        #    self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=self.ui_master_data.lineEdit_film_genre))

        #self.ui_master_data.comboBox_film_genre.setItemData(1, 1, Qt.UserRole)
        #self.ui_master_data.comboBox_film_genre.itemData(0, Qt.UserRole)

    def init_selectionChanged_signal_treeview(self):
        '''
            ###################################################
            ###################  Genre Film  ##################
            ###################################################
        '''
        self.ui_master_data.tree_view_film_genre.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_film_genre:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            #################  General Country ################
            ###################################################
        '''
        self.ui_master_data.tree_view_general_country.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_general_country:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            #################  General Language ###############
            ###################################################
        '''
        self.ui_master_data.tree_view_general_language.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_general_language:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ##################  Person Gender  ################
            ###################################################
        '''
        self.ui_master_data.tree_view_person_gender.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_gender:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ###############  Person Nationality  ##############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_nationality.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_nationality:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ################  Person Salutation  ##############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_salutation.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_salutation:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ##################  Person Title  #################
            ###################################################
        '''
        self.ui_master_data.tree_view_person_title.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_title:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ################  Person Hair Color  ##############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_hair_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_hair_color:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ################  Person Eye Color  ###############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_eye_color.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_eye_color:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            #################  Person Religion  ###############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_religion.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_religion:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ############  Person Relationship Status ##########
            ###################################################
        '''
        self.ui_master_data.tree_view_person_relationship_status.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_relationship_status:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            ################  Person Relationship #############
            ###################################################
        '''
        self.ui_master_data.tree_view_person_relationship.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_person_relationship:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            #################  General Region  ################
            ###################################################
        '''
        self.ui_master_data.tree_view_general_region.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_general_region:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        '''
            ###################################################
            #################  General Award  ################
            ###################################################
        '''
        self.ui_master_data.tree_view_general_award.selectionModel().selectionChanged.connect(
            lambda new_index, old_index, edit_line_obj=self.ui_master_data.lineEdit_general_award:
            self.handle_selection_changed(new_index=new_index, old_index=old_index, edit_line_obj=edit_line_obj))

        return

    def init_returnPressed_signal_line_edit(self):
        '''
            NOTE:
            ======
            This method is used to set the signal and slots of returnPressed for QLineEdit()-object(s).  

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        '''
            ###################################################
            ###################  Genre Film  ##################
            ###################################################
        '''
        self.ui_master_data.lineEdit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_save_film_genre.click)
        self.ui_master_data.lineEdit_edit_film_genre.returnPressed.connect(self.ui_master_data.pushButton_apply_change_film_genre.click)

        '''
            ###################################################
            #################  General Country  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_general_country.returnPressed.connect(self.ui_master_data.pushButton_save_general_country.click)
        self.ui_master_data.lineEdit_edit_general_country.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_country.click)

        '''
            ###################################################
            ################  General Language  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_general_language.returnPressed.connect(self.ui_master_data.pushButton_save_general_language.click)
        self.ui_master_data.lineEdit_edit_general_language.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_language.click)

        '''
            ###################################################
            ##################  Person Gender  ################
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_save_person_gender.click)
        self.ui_master_data.lineEdit_edit_person_gender.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_gender.click)

        '''
            ###################################################
            ###############  Person Nationality  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_save_person_nationality.click)
        self.ui_master_data.lineEdit_edit_person_nationality.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_nationality.click)

        '''
            ###################################################
            ################  Person Salutation  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_save_person_salutation.click)
        self.ui_master_data.lineEdit_edit_person_salutation.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_salutation.click)

        '''
            ###################################################
            ##################  Person Title  #################
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_title.returnPressed.connect(self.ui_master_data.pushButton_save_person_title.click)
        self.ui_master_data.lineEdit_edit_person_title.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_title.click)

        '''
            ###################################################
            ################  Person Hair Color  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_hair_color.click)
        self.ui_master_data.lineEdit_edit_person_hair_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_hair_color.click)

        '''
            ###################################################
            ################  Person Eye Color  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_save_person_eye_color.click)
        self.ui_master_data.lineEdit_edit_person_eye_color.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_eye_color.click)

        '''
            ###################################################
            ################  Person Religion  ################
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_save_person_religion.click)
        self.ui_master_data.lineEdit_edit_person_religion.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_religion.click)

        '''
            ###################################################
            ###########  Person Relationship Status ###########
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship_status.click)
        self.ui_master_data.lineEdit_edit_person_relationship_status.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship_status.click)

        '''
            ###################################################
            ##############  Person Relationship  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_save_person_relationship.click)
        self.ui_master_data.lineEdit_edit_person_relationship.returnPressed.connect(self.ui_master_data.pushButton_apply_change_person_relationship.click)

        '''
            ###################################################
            #################  General Region  ################
            ###################################################
        '''
        self.ui_master_data.lineEdit_general_region.returnPressed.connect(self.ui_master_data.pushButton_save_general_region.click)
        self.ui_master_data.lineEdit_edit_general_region.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_region.click)

        '''
            ###################################################
            #################  General Award  #################
            ###################################################
        '''
        self.ui_master_data.lineEdit_general_award.returnPressed.connect(self.ui_master_data.pushButton_save_general_award.click)
        self.ui_master_data.lineEdit_edit_general_award.returnPressed.connect(self.ui_master_data.pushButton_apply_change_general_award.click)

        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTICE:
            =======
            This method is used to set the signal and slots for QpuschButton()-object(s). 

            PARAMETERS:
            ===========
            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''
        logger.info("Set the signal and slots for QpuschButton()-object(s)")

        '''
        ##############################################
        ############### Film Genre ###################
        ##############################################
        '''
        self.ui_master_data.pushButton_save_film_genre.clicked.connect(lambda: 
            self.add_record(line_edit_obj_film_genre=self.ui_master_data.lineEdit_film_genre,
                            counter_label_object=self.ui_master_data.label_genre_film_counter,
                            label_object=self.ui_master_data.label_genre_film_counter,
                            tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_film_genre, False),
                                                             (self.ui_master_data.lineEdit_film_genre, False)],
                            tree_view_object=self.ui_master_data.tree_view_film_genre,
                            line_edit_object_get_focus=self.ui_master_data.lineEdit_film_genre,
                            list_header=self.LIST_HEADER_GENRE_FILM,
                            model = self._standard_item_model_film_genre,
                            category='film_genre'))

        self.ui_master_data.pushButton_edit_film_genre.clicked.connect(lambda:
            self.handle_edit(index=1, 
                             stack_widget_object=self.ui_master_data.stackedWidget_film_genre,
                             set_text=self.ui_master_data.lineEdit_film_genre.text(),
                             lineEdit_object=self.ui_master_data.lineEdit_edit_film_genre,
                             set_focus=True,
                             clear_widget=True,
                             load_data=False))

        self.ui_master_data.pushButton_apply_change_film_genre.clicked.connect(lambda:
            self.edit_record(index=0,                              
                             stackWidget_obj=self.ui_master_data.stackedWidget_film_genre, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_film_genre, 
                             line_edit_obj_film_genre=self.ui_master_data.lineEdit_edit_film_genre,
                             counter_label_object=self.ui_master_data.label_genre_film_counter,
                             label_object=self.ui_master_data.label_genre_film_counter,
                             tree_view_object=self.ui_master_data.tree_view_film_genre,
                             list_header=self.LIST_HEADER_GENRE_FILM,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                             model = self._standard_item_model_film_genre,
                             category='film_genre'))

        self.ui_master_data.pushButton_cancel_edit_film_genre.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_film_genre, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        self.ui_master_data.pushButton_refresh_film_genre.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_film_genre, False)],
                                        tree_view_object=self.ui_master_data.tree_view_film_genre,
                                        list_header=self.LIST_HEADER_GENRE_FILM, 
                                        label_object=self.ui_master_data.label_genre_film_counter,
                                        model = self._standard_item_model_film_genre,
                                        category='film_genre'))

        self.ui_master_data.pushButton_delete_film_genre.clicked.connect(lambda:
            self.delete_record(id=self.current_selected_id,
                                    line_edit_obj=self.ui_master_data.lineEdit_film_genre,
                                    counter_label_object=self.ui_master_data.label_genre_film_counter,
                                    label_object=self.ui_master_data.label_genre_film_counter,
                                    tree_view_object=self.ui_master_data.tree_view_film_genre,
                                    list_header=self.LIST_HEADER_GENRE_FILM, 
                                    tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_film_genre, False)],
                                    model = self._standard_item_model_film_genre,
                                    category='film_genre'))

        '''
        ###################################################
        ############### General Country ###################
        ###################################################
        '''
        self.ui_master_data.pushButton_save_general_country.clicked.connect(lambda:
            self.add_record(line_edit_obj_general_country = self.ui_master_data.lineEdit_general_country,
                            counter_label_object = self.ui_master_data.label_general_country_counter,
                            label_object = self.ui_master_data.label_general_country_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                            tree_view_object = self.ui_master_data.tree_view_general_country,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                            list_header = self.LIST_HEADER_GENERAL_COUNTRY,
                            model = self._standard_item_model_general_country,
                            category = 'general_country'))


        self.ui_master_data.pushButton_edit_general_country.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_general_country,
                             set_text = self.ui_master_data.lineEdit_general_country.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_general_country,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))

        self.ui_master_data.pushButton_apply_change_general_country.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_general_country,
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country, 
                             line_edit_obj_general_country = self.ui_master_data.lineEdit_edit_general_country,
                             counter_label_object = self.ui_master_data.label_general_country_counter,
                             label_object = self.ui_master_data.label_general_country_counter,
                             tree_view_object = self.ui_master_data.tree_view_general_country,
                             list_header = self.LIST_HEADER_GENERAL_COUNTRY,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                             model = self._standard_item_model_general_country,
                             category = 'general_country'))

        self.ui_master_data.pushButton_cancel_edit_general_country.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_general_country, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_general_country.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                                      (self.ui_master_data.pushButton_save_general_country, False),
                                                                      (self.ui_master_data.pushButton_edit_general_country, False),
                                                                      (self.ui_master_data.pushButton_delete_general_country, False),
                                                                      (self.ui_master_data.tree_view_general_country, False),
                                                                      (self.ui_master_data.lineEdit_search_general_country, False),
                                                                      (self.ui_master_data.lineEdit_general_country, False)],
                                        tree_view_object = self.ui_master_data.tree_view_general_country,
                                        list_header = self.LIST_HEADER_GENERAL_COUNTRY, 
                                        label_object = self.ui_master_data.label_general_country_counter,
                                        model = self._standard_item_model_general_country,
                                        category = 'general_country'))

        self.ui_master_data.pushButton_delete_general_country.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_general_country,
                                    counter_label_object = self.ui_master_data.label_general_country_counter,
                                    label_object = self.ui_master_data.label_general_country_counter,
                                    tree_view_object = self.ui_master_data.tree_view_general_country,
                                    list_header = self.LIST_HEADER_GENERAL_COUNTRY, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                                  (self.ui_master_data.pushButton_save_general_country, False),
                                                                  (self.ui_master_data.pushButton_edit_general_country, False),
                                                                  (self.ui_master_data.pushButton_delete_general_country, False),
                                                                  (self.ui_master_data.tree_view_general_country, False),
                                                                  (self.ui_master_data.lineEdit_search_general_country, False),
                                                                  (self.ui_master_data.lineEdit_general_country, False)],
                                    model = self._standard_item_model_general_country,
                                    category = 'general_country'))

        '''
        ###################################################
        ############## General Language ###################
        ###################################################
        '''
        self.ui_master_data.pushButton_save_general_language.clicked.connect(lambda:
            self.add_record(line_edit_obj_general_language = self.ui_master_data.lineEdit_general_language,
                            counter_label_object = self.ui_master_data.label_general_language_counter,
                            label_object = self.ui_master_data.label_general_language_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_language, False),
                                                          (self.ui_master_data.pushButton_save_general_language, False),
                                                          (self.ui_master_data.pushButton_edit_general_language, False),
                                                          (self.ui_master_data.pushButton_delete_general_language, False),
                                                          (self.ui_master_data.tree_view_general_language, False),
                                                          (self.ui_master_data.lineEdit_search_general_language, False),
                                                          (self.ui_master_data.lineEdit_general_language, False)],
                            tree_view_object = self.ui_master_data.tree_view_general_language,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                            list_header = self.LIST_HEADER_GENERAL_LANGUAGE,
                            model = self._standard_item_model_general_language,
                            category = 'general_language'))

        self.ui_master_data.pushButton_edit_general_language.clicked.connect(lambda:
        self.handle_edit(index = 1, 
                         stack_widget_object = self.ui_master_data.stackedWidget_general_language,
                         set_text = self.ui_master_data.lineEdit_general_language.text(),
                         lineEdit_object = self.ui_master_data.lineEdit_edit_general_language,
                         set_focus = True,
                         clear_widget = True,
                         load_data = False))
        
        self.ui_master_data.pushButton_apply_change_general_language.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_general_language, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language, 
                             line_edit_obj_general_language = self.ui_master_data.lineEdit_edit_general_language,
                             counter_label_object = self.ui_master_data.label_general_language_counter,
                             label_object = self.ui_master_data.label_general_language_counter,
                             tree_view_object = self.ui_master_data.tree_view_general_language,
                             list_header = self.LIST_HEADER_GENERAL_LANGUAGE,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                             model = self._standard_item_model_general_language,
                             category = 'general_language'))

        self.ui_master_data.pushButton_cancel_edit_general_language.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_general_language, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_general_language.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_language, False),
                                                                      (self.ui_master_data.pushButton_save_general_language, False),
                                                                      (self.ui_master_data.pushButton_edit_general_language, False),
                                                                      (self.ui_master_data.pushButton_delete_general_language, False),
                                                                      (self.ui_master_data.tree_view_general_language, False),
                                                                      (self.ui_master_data.lineEdit_search_general_language, False),
                                                                      (self.ui_master_data.lineEdit_general_language, False)],
                                        tree_view_object = self.ui_master_data.tree_view_general_language,
                                        list_header = self.LIST_HEADER_GENERAL_LANGUAGE, 
                                        label_object = self.ui_master_data.label_general_language_counter,
                                        model = self._standard_item_model_general_language,
                                        category = 'general_language'))

        self.ui_master_data.pushButton_delete_general_language.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_general_language,
                                    counter_label_object = self.ui_master_data.label_general_language_counter,
                                    label_object = self.ui_master_data.label_general_language_counter,
                                    tree_view_object = self.ui_master_data.tree_view_general_language,
                                    list_header = self.LIST_HEADER_GENERAL_LANGUAGE, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_language, False),
                                                                  (self.ui_master_data.pushButton_save_general_language, False),
                                                                  (self.ui_master_data.pushButton_edit_general_language, False),
                                                                  (self.ui_master_data.pushButton_delete_general_language, False),
                                                                  (self.ui_master_data.tree_view_general_language, False),
                                                                  (self.ui_master_data.lineEdit_search_general_language, False),
                                                                  (self.ui_master_data.lineEdit_general_language, False)],
                                    model = self._standard_item_model_general_language,
                                    category = 'general_language'))

        '''
        ###################################################
        ################ Person Gender ####################
        ###################################################
        '''
        self.ui_master_data.pushButton_save_person_gender.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_gender=self.ui_master_data.lineEdit_person_gender,
                            counter_label_object=self.ui_master_data.label_person_gender_counter,
                            label_object=self.ui_master_data.label_person_gender_counter,
                            tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_person_gender, False),
                                                             (self.ui_master_data.pushButton_save_person_gender, False),
                                                             (self.ui_master_data.pushButton_edit_person_gender, False),
                                                             (self.ui_master_data.pushButton_delete_person_gender, False),
                                                             (self.ui_master_data.tree_view_person_gender, False),
                                                             (self.ui_master_data.lineEdit_search_person_gender, False),
                                                             (self.ui_master_data.lineEdit_person_gender, False)],
                            tree_view_object=self.ui_master_data.tree_view_person_gender,
                            line_edit_object_get_focus=self.ui_master_data.lineEdit_person_gender,
                            list_header=self.LIST_HEADER_PERSON_GENDER,
                            model = self._standard_item_model_person_gender,
                            category='person_gender'))

        self.ui_master_data.pushButton_edit_person_gender.clicked.connect(lambda:
            self.handle_edit(index=1, 
                             stack_widget_object=self.ui_master_data.stackedWidget_person_gender,
                             set_text=self.ui_master_data.lineEdit_person_gender.text(),
                             lineEdit_object=self.ui_master_data.lineEdit_edit_person_gender,
                             set_focus=True,
                             clear_widget=True,
                             load_data=False))
        
        self.ui_master_data.pushButton_apply_change_person_gender.clicked.connect(lambda:
            self.edit_record(index=0,                              
                             stackWidget_obj=self.ui_master_data.stackedWidget_person_gender, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_gender, 
                             line_edit_obj_person_gender=self.ui_master_data.lineEdit_edit_person_gender,
                             counter_label_object=self.ui_master_data.label_person_gender_counter,
                             label_object=self.ui_master_data.label_person_gender_counter,
                             tree_view_object=self.ui_master_data.tree_view_person_gender,
                             list_header=self.LIST_HEADER_PERSON_GENDER,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_gender, False),
                                                          (self.ui_master_data.pushButton_save_person_gender, False),
                                                          (self.ui_master_data.pushButton_edit_person_gender, False),
                                                          (self.ui_master_data.pushButton_delete_person_gender, False),
                                                          (self.ui_master_data.tree_view_person_gender, False),
                                                          (self.ui_master_data.lineEdit_search_person_gender, False),
                                                          (self.ui_master_data.lineEdit_person_gender, False)],
                             model = self._standard_item_model_person_gender,
                             category='person_gender'))

        self.ui_master_data.pushButton_cancel_edit_person_gender.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_person_gender, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        self.ui_master_data.pushButton_refresh_person_gender.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_person_gender, False),
                                                                       (self.ui_master_data.pushButton_save_person_gender, False),
                                                                       (self.ui_master_data.pushButton_edit_person_gender, False),
                                                                       (self.ui_master_data.pushButton_delete_person_gender, False),
                                                                       (self.ui_master_data.lineEdit_person_gender, False),
                                                                       (self.ui_master_data.lineEdit_search_person_gender, False),
                                                                       (self.ui_master_data.tree_view_person_gender, False)],
                                        tree_view_object=self.ui_master_data.tree_view_person_gender,
                                        list_header=self.LIST_HEADER_PERSON_GENDER, 
                                        label_object=self.ui_master_data.label_person_gender_counter,
                                        model = self._standard_item_model_person_gender,
                                        category='person_gender'))

        self.ui_master_data.pushButton_delete_person_gender.clicked.connect(lambda:
            self.delete_record(id=self.current_selected_id,
                                    line_edit_obj=self.ui_master_data.lineEdit_person_gender,
                                    counter_label_object=self.ui_master_data.label_person_gender_counter,
                                    label_object=self.ui_master_data.label_person_gender_counter,
                                    tree_view_object=self.ui_master_data.tree_view_person_gender,
                                    list_header=self.LIST_HEADER_PERSON_GENDER, 
                                    tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_person_gender, False),
                                                                    (self.ui_master_data.pushButton_save_person_gender, False),
                                                                    (self.ui_master_data.pushButton_edit_person_gender, False),
                                                                    (self.ui_master_data.pushButton_delete_person_gender, False),
                                                                    (self.ui_master_data.tree_view_person_gender, False),
                                                                    (self.ui_master_data.lineEdit_search_person_gender, False),
                                                                    (self.ui_master_data.lineEdit_person_gender, False)],
                                    model = self._standard_item_model_person_gender,
                                    category='person_gender'))

        '''
        ########################################################
        ################ Person Nationality ####################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_nationality.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_nationality = self.ui_master_data.lineEdit_person_nationality,
                            counter_label_object = self.ui_master_data.label_person_nationality_counter,
                            label_object = self.ui_master_data.label_person_nationality_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_nationality, False),
                                                        (self.ui_master_data.pushButton_save_person_nationality, False),
                                                         (self.ui_master_data.pushButton_edit_person_nationality, False),
                                                         (self.ui_master_data.pushButton_delete_person_nationality, False),
                                                         (self.ui_master_data.tree_view_person_nationality, False),
                                                         (self.ui_master_data.lineEdit_search_person_nationality, False),
                                                         (self.ui_master_data.lineEdit_person_nationality, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_nationality,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                            list_header = self.LIST_HEADER_PERSON_NATIONALITY,
                            model = self._standard_item_model_person_nationality,
                            category = 'person_nationality'))

        self.ui_master_data.pushButton_edit_person_nationality.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_nationality,
                             set_text = self.ui_master_data.lineEdit_person_nationality.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_nationality,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_nationality.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_nationality, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                             line_edit_obj_person_nationality = self.ui_master_data.lineEdit_edit_person_nationality,
                             counter_label_object = self.ui_master_data.label_person_nationality_counter,
                             label_object = self.ui_master_data.label_person_nationality_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_NATIONALITY,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_nationality, False),
                                                        (self.ui_master_data.pushButton_save_person_nationality, False),
                                                         (self.ui_master_data.pushButton_edit_person_nationality, False),
                                                         (self.ui_master_data.pushButton_delete_person_nationality, False),
                                                         (self.ui_master_data.tree_view_person_nationality, False),
                                                         (self.ui_master_data.lineEdit_search_person_nationality, False),
                                                         (self.ui_master_data.lineEdit_person_nationality, False)],
                             model = self._standard_item_model_person_nationality,
                             category = 'person_nationality'))

        self.ui_master_data.pushButton_cancel_edit_person_nationality.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_nationality, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_nationality.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_nationality, False),
                                                                    (self.ui_master_data.pushButton_save_person_nationality, False),
                                                                     (self.ui_master_data.pushButton_edit_person_nationality, False),
                                                                     (self.ui_master_data.pushButton_delete_person_nationality, False),
                                                                     (self.ui_master_data.tree_view_person_nationality, False),
                                                                     (self.ui_master_data.lineEdit_search_person_nationality, False),
                                                                     (self.ui_master_data.lineEdit_person_nationality, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_nationality,
                                        list_header = self.LIST_HEADER_PERSON_NATIONALITY, 
                                        label_object = self.ui_master_data.label_person_nationality_counter,
                                        model = self._standard_item_model_person_nationality,
                                        category = 'person_nationality'))

        self.ui_master_data.pushButton_delete_person_nationality.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_nationality,
                                    counter_label_object = self.ui_master_data.label_person_nationality_counter,
                                    label_object = self.ui_master_data.label_person_nationality_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_nationality,
                                    list_header = self.LIST_HEADER_PERSON_NATIONALITY, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_nationality, False),
                                                                (self.ui_master_data.pushButton_save_person_nationality, False),
                                                                 (self.ui_master_data.pushButton_edit_person_nationality, False),
                                                                 (self.ui_master_data.pushButton_delete_person_nationality, False),
                                                                 (self.ui_master_data.tree_view_person_nationality, False),
                                                                 (self.ui_master_data.lineEdit_search_person_nationality, False),
                                                                 (self.ui_master_data.lineEdit_person_nationality, False)],
                                    model = self._standard_item_model_person_nationality,
                                    category = 'person_nationality'))

        '''
        ########################################################
        ################ Person Salutation #####################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_salutation.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_salutation = self.ui_master_data.lineEdit_person_salutation,
                            counter_label_object = self.ui_master_data.label_person_salutation_counter,
                            label_object = self.ui_master_data.label_person_salutation_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_salutation, False),
                                                          (self.ui_master_data.pushButton_save_person_salutation, False),
                                                          (self.ui_master_data.pushButton_edit_person_salutation, False),
                                                          (self.ui_master_data.pushButton_delete_person_salutation, False),
                                                          (self.ui_master_data.tree_view_person_salutation, False),
                                                          (self.ui_master_data.lineEdit_search_person_salutation, False),
                                                          (self.ui_master_data.lineEdit_person_salutation, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_salutation,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                            list_header = self.LIST_HEADER_PERSON_SALUTATION,
                            model = self._standard_item_model_person_salutation,
                            category = 'person_salutation'))

        self.ui_master_data.pushButton_edit_person_salutation.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_salutation,
                             set_text = self.ui_master_data.lineEdit_person_salutation.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_salutation,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_salutation.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_salutation, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                             line_edit_obj_person_salutation = self.ui_master_data.lineEdit_edit_person_salutation,
                             counter_label_object = self.ui_master_data.label_person_salutation_counter,
                             label_object = self.ui_master_data.label_person_salutation_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_SALUTATION,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_salutation, False),
                                                          (self.ui_master_data.pushButton_save_person_salutation, False),
                                                          (self.ui_master_data.pushButton_edit_person_salutation, False),
                                                          (self.ui_master_data.pushButton_delete_person_salutation, False),
                                                          (self.ui_master_data.tree_view_person_salutation, False),
                                                          (self.ui_master_data.lineEdit_search_person_salutation, False),
                                                          (self.ui_master_data.lineEdit_person_salutation, False)],
                             model = self._standard_item_model_person_salutation,
                             category = 'person_salutation'))

        self.ui_master_data.pushButton_cancel_edit_person_salutation.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_salutation, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_title.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_salutation, False),
                                                                    (self.ui_master_data.pushButton_save_person_salutation, False),
                                                                    (self.ui_master_data.pushButton_edit_person_salutation, False),
                                                                    (self.ui_master_data.pushButton_delete_person_salutation, False),
                                                                    (self.ui_master_data.tree_view_person_salutation, False),
                                                                    (self.ui_master_data.lineEdit_search_person_salutation, False),
                                                                    (self.ui_master_data.lineEdit_person_salutation, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_salutation,
                                        list_header = self.LIST_HEADER_PERSON_SALUTATION, 
                                        label_object = self.ui_master_data.label_person_salutation_counter,
                                        model = self._standard_item_model_person_salutation,
                                        category = 'person_salutation'))

        self.ui_master_data.pushButton_delete_person_salutation.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_salutation,
                                    counter_label_object = self.ui_master_data.label_person_salutation_counter,
                                    label_object = self.ui_master_data.label_person_salutation_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_salutation,
                                    list_header = self.LIST_HEADER_PERSON_SALUTATION, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_salutation, False),
                                                                  (self.ui_master_data.pushButton_save_person_salutation, False),
                                                                  (self.ui_master_data.pushButton_edit_person_salutation, False),
                                                                  (self.ui_master_data.pushButton_delete_person_salutation, False),
                                                                  (self.ui_master_data.tree_view_person_salutation, False),
                                                                  (self.ui_master_data.lineEdit_search_person_salutation, False),
                                                                  (self.ui_master_data.lineEdit_person_salutation, False)],
                                    model = self._standard_item_model_person_salutation,
                                    category = 'person_salutation'))

        '''
        ########################################################
        ################### Person Title #######################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_title.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_title = self.ui_master_data.lineEdit_person_title,
                            counter_label_object = self.ui_master_data.label_person_title_counter,
                            label_object = self.ui_master_data.label_person_title_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_title, False),
                                                          (self.ui_master_data.pushButton_save_person_title, False),
                                                          (self.ui_master_data.pushButton_edit_person_title, False),
                                                          (self.ui_master_data.pushButton_delete_person_title, False),
                                                          (self.ui_master_data.tree_view_person_title, False),
                                                          (self.ui_master_data.lineEdit_search_person_title, False),
                                                          (self.ui_master_data.lineEdit_person_title, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_title,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                            list_header = self.LIST_HEADER_PERSON_TITLE,
                            model = self._standard_item_model_person_title,
                            category = 'person_title'))

        self.ui_master_data.pushButton_edit_person_title.clicked.connect(lambda:
        self.handle_edit(index = 1, 
                         stack_widget_object = self.ui_master_data.stackedWidget_person_title,
                         set_text = self.ui_master_data.lineEdit_person_title.text(),
                         lineEdit_object = self.ui_master_data.lineEdit_edit_person_title,
                         set_focus = True,
                         clear_widget = True,
                         load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_title.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_title, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_title,
                             line_edit_obj_person_title = self.ui_master_data.lineEdit_edit_person_title,
                             counter_label_object = self.ui_master_data.label_person_title_counter,
                             label_object = self.ui_master_data.label_person_title_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_TITLE,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_title, False),
                                                          (self.ui_master_data.pushButton_save_person_title, False),
                                                          (self.ui_master_data.pushButton_edit_person_title, False),
                                                          (self.ui_master_data.pushButton_delete_person_title, False),
                                                          (self.ui_master_data.tree_view_person_title, False),
                                                          (self.ui_master_data.lineEdit_search_person_title, False),
                                                          (self.ui_master_data.lineEdit_person_title, False)],
                             model = self._standard_item_model_person_title,
                             category = 'person_title'))

        self.ui_master_data.pushButton_cancel_edit_person_title.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_title, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_title.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_title, False),
                                                                      (self.ui_master_data.pushButton_save_person_title, False),
                                                                      (self.ui_master_data.pushButton_edit_person_title, False),
                                                                      (self.ui_master_data.pushButton_delete_person_title, False),
                                                                      (self.ui_master_data.tree_view_person_title, False),
                                                                      (self.ui_master_data.lineEdit_search_person_title, False),
                                                                      (self.ui_master_data.lineEdit_person_title, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_title,
                                        list_header = self.LIST_HEADER_PERSON_TITLE, 
                                        label_object = self.ui_master_data.label_person_title_counter,
                                        model = self._standard_item_model_person_title,
                                        category = 'person_title'))

        self.ui_master_data.pushButton_delete_person_title.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_title,
                                    counter_label_object = self.ui_master_data.label_person_title_counter,
                                    label_object = self.ui_master_data.label_person_title_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_title,
                                    list_header = self.LIST_HEADER_PERSON_TITLE, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_title, False),
                                                                  (self.ui_master_data.pushButton_save_person_title, False),
                                                                  (self.ui_master_data.pushButton_edit_person_title, False),
                                                                  (self.ui_master_data.pushButton_delete_person_title, False),
                                                                  (self.ui_master_data.tree_view_person_title, False),
                                                                  (self.ui_master_data.lineEdit_search_person_title, False),
                                                                  (self.ui_master_data.lineEdit_person_title, False)],
                                    model = self._standard_item_model_person_title,
                                    category = 'person_title'))

        '''
        ########################################################
        ################ Person Hair Color #####################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_hair_color.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_hair_color = self.ui_master_data.lineEdit_person_hair_color,
                            counter_label_object = self.ui_master_data.label_person_hair_color_counter,
                            label_object = self.ui_master_data.label_person_hair_color_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_save_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_edit_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_delete_person_hair_color, False),
                                                        (self.ui_master_data.tree_view_person_hair_color, False),
                                                        (self.ui_master_data.lineEdit_search_person_hair_color, False),
                                                        (self.ui_master_data.lineEdit_person_hair_color, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_hair_color,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                            list_header = self.LIST_HEADER_PERSON_HAIR_COLOR,
                            model = self._standard_item_model_person_hair_color,
                            category = 'person_hair_color'))

        self.ui_master_data.pushButton_edit_person_hair_color.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color,
                             set_text = self.ui_master_data.lineEdit_person_hair_color.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_hair_color,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_hair_color.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_hair_color, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                             line_edit_obj_person_hair_color = self.ui_master_data.lineEdit_edit_person_hair_color,
                             counter_label_object = self.ui_master_data.label_person_hair_color_counter,
                             label_object = self.ui_master_data.label_person_hair_color_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_HAIR_COLOR,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_save_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_edit_person_hair_color, False),
                                                        (self.ui_master_data.pushButton_delete_person_hair_color, False),
                                                        (self.ui_master_data.tree_view_person_hair_color, False),
                                                        (self.ui_master_data.lineEdit_search_person_hair_color, False),
                                                        (self.ui_master_data.lineEdit_person_hair_color, False)],
                             model = self._standard_item_model_person_hair_color,
                             category = 'person_hair_color'))

        self.ui_master_data.pushButton_cancel_edit_person_hair_color.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_hair_color, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_hair_color.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_save_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_edit_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_delete_person_hair_color, False),
                                                                    (self.ui_master_data.tree_view_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_search_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_person_hair_color, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_hair_color,
                                        list_header = self.LIST_HEADER_PERSON_HAIR_COLOR, 
                                        label_object = self.ui_master_data.label_person_hair_color_counter,
                                        model = self._standard_item_model_person_hair_color,
                                        category = 'person_hair_color'))

        self.ui_master_data.pushButton_delete_person_hair_color.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_hair_color,
                                    counter_label_object = self.ui_master_data.label_person_hair_color_counter,
                                    label_object = self.ui_master_data.label_person_hair_color_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_hair_color,
                                    list_header = self.LIST_HEADER_PERSON_HAIR_COLOR, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_save_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_edit_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_delete_person_hair_color, False),
                                                                    (self.ui_master_data.tree_view_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_search_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_person_hair_color, False)],
                                    model = self._standard_item_model_person_hair_color,
                                    category = 'person_hair_color'))

        '''
        ########################################################
        ################ Person Eye Color ######################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_eye_color.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_eye_color = self.ui_master_data.lineEdit_person_eye_color,
                            counter_label_object = self.ui_master_data.label_person_eye_color_counter,
                            label_object = self.ui_master_data.label_person_eye_color_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_save_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_edit_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_delete_person_eye_color, False),
                                                          (self.ui_master_data.tree_view_person_eye_color, False),
                                                          (self.ui_master_data.lineEdit_search_person_eye_color, False),
                                                          (self.ui_master_data.lineEdit_person_eye_color, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_eye_color,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                            list_header = self.LIST_HEADER_PERSON_EYE_COLOR,
                            model = self._standard_item_model_person_eye_color,
                            category = 'person_eye_color'))

        self.ui_master_data.pushButton_edit_person_eye_color.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color,
                             set_text = self.ui_master_data.lineEdit_person_eye_color.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_eye_color,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_eye_color.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_eye_color, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                             line_edit_obj_person_eye_color = self.ui_master_data.lineEdit_edit_person_eye_color,
                             counter_label_object = self.ui_master_data.label_person_eye_color_counter,
                             label_object = self.ui_master_data.label_person_eye_color_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_EYE_COLOR,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_save_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_edit_person_eye_color, False),
                                                          (self.ui_master_data.pushButton_delete_person_eye_color, False),
                                                          (self.ui_master_data.tree_view_person_eye_color, False),
                                                          (self.ui_master_data.lineEdit_search_person_eye_color, False),
                                                          (self.ui_master_data.lineEdit_person_eye_color, False)],
                             model = self._standard_item_model_person_eye_color,
                             category = 'person_eye_color'))

        self.ui_master_data.pushButton_cancel_edit_person_eye_color.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_eye_color, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_eye_color.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_eye_color, False),
                                                                      (self.ui_master_data.pushButton_save_person_eye_color, False),
                                                                      (self.ui_master_data.pushButton_edit_person_eye_color, False),
                                                                      (self.ui_master_data.pushButton_delete_person_eye_color, False),
                                                                      (self.ui_master_data.tree_view_person_eye_color, False),
                                                                      (self.ui_master_data.lineEdit_search_person_eye_color, False),
                                                                      (self.ui_master_data.lineEdit_person_eye_color, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_eye_color,
                                        list_header = self.LIST_HEADER_PERSON_EYE_COLOR, 
                                        label_object = self.ui_master_data.label_person_eye_color_counter,
                                        model = self._standard_item_model_person_eye_color,
                                        category = 'person_eye_color'))

        self.ui_master_data.pushButton_delete_person_eye_color.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_eye_color,
                                    counter_label_object = self.ui_master_data.label_person_eye_color_counter,
                                    label_object = self.ui_master_data.label_person_eye_color_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_eye_color,
                                    list_header = self.LIST_HEADER_PERSON_EYE_COLOR, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_save_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_edit_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_delete_person_eye_color, False),
                                                                  (self.ui_master_data.tree_view_person_eye_color, False),
                                                                  (self.ui_master_data.lineEdit_search_person_eye_color, False),
                                                                  (self.ui_master_data.lineEdit_person_eye_color, False)],
                                    model = self._standard_item_model_person_eye_color,
                                    category = 'person_eye_color'))

        '''
        ########################################################
        ################ Person Religion #######################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_religion.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_religion = self.ui_master_data.lineEdit_person_religion,
                            counter_label_object = self.ui_master_data.label_person_religion_counter,
                            label_object = self.ui_master_data.label_person_religion_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_religion, False),
                                                          (self.ui_master_data.pushButton_save_person_religion, False),
                                                          (self.ui_master_data.pushButton_edit_person_religion, False),
                                                          (self.ui_master_data.pushButton_delete_person_religion, False),
                                                          (self.ui_master_data.tree_view_person_religion, False),
                                                          (self.ui_master_data.lineEdit_search_person_religion, False),
                                                          (self.ui_master_data.lineEdit_person_religion, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_religion,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                            list_header = self.LIST_HEADER_PERSON_RELIGION,
                            model = self._standard_item_model_person_religion,
                            category = 'person_religion'))

        self.ui_master_data.pushButton_edit_person_religion.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_religion,
                             set_text = self.ui_master_data.lineEdit_person_religion.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_religion,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_religion.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_religion, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                             line_edit_obj_person_religion = self.ui_master_data.lineEdit_edit_person_religion,
                             counter_label_object = self.ui_master_data.label_person_religion_counter,
                             label_object = self.ui_master_data.label_person_religion_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_RELIGION,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_religion, False),
                                                          (self.ui_master_data.pushButton_save_person_religion, False),
                                                          (self.ui_master_data.pushButton_edit_person_religion, False),
                                                          (self.ui_master_data.pushButton_delete_person_religion, False),
                                                          (self.ui_master_data.tree_view_person_religion, False),
                                                          (self.ui_master_data.lineEdit_search_person_religion, False),
                                                          (self.ui_master_data.lineEdit_person_religion, False)],
                             model = self._standard_item_model_person_religion,
                             category = 'person_religion'))

        self.ui_master_data.pushButton_cancel_edit_person_religion.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_religion, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_religion.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_religion, False),
                                                                      (self.ui_master_data.pushButton_save_person_religion, False),
                                                                      (self.ui_master_data.pushButton_edit_person_religion, False),
                                                                      (self.ui_master_data.pushButton_delete_person_religion, False),
                                                                      (self.ui_master_data.tree_view_person_religion, False),
                                                                      (self.ui_master_data.lineEdit_search_person_religion, False),
                                                                      (self.ui_master_data.lineEdit_person_religion, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_religion,
                                        list_header = self.LIST_HEADER_PERSON_RELIGION, 
                                        label_object = self.ui_master_data.label_person_religion_counter,
                                        model = self._standard_item_model_person_religion,
                                        category = 'person_religion'))

        self.ui_master_data.pushButton_delete_person_religion.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_religion,
                                    counter_label_object = self.ui_master_data.label_person_religion_counter,
                                    label_object = self.ui_master_data.label_person_religion_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_religion,
                                    list_header = self.LIST_HEADER_PERSON_RELIGION, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_religion, False),
                                                                  (self.ui_master_data.pushButton_save_person_religion, False),
                                                                  (self.ui_master_data.pushButton_edit_person_religion, False),
                                                                  (self.ui_master_data.pushButton_delete_person_religion, False),
                                                                  (self.ui_master_data.tree_view_person_religion, False),
                                                                  (self.ui_master_data.lineEdit_search_person_religion, False),
                                                                  (self.ui_master_data.lineEdit_person_religion, False)],
                                    model = self._standard_item_model_person_religion,
                                    category = 'person_religion'))

        '''
        ########################################################
        ############ Person Relationship Stauts ################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_relationship_status.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_relationship_status = self.ui_master_data.lineEdit_person_relationship_status,
                            counter_label_object = self.ui_master_data.label_person_relationship_status_counter,
                            label_object = self.ui_master_data.label_person_relationship_status_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_save_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_edit_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_delete_person_relationship_status, False),
                                                        (self.ui_master_data.tree_view_person_relationship_status, False),
                                                        (self.ui_master_data.lineEdit_search_person_relationship_status, False),
                                                        (self.ui_master_data.lineEdit_person_relationship_status, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_relationship_status,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                            list_header = self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS,
                            model = self._standard_item_model_person_relationship_status,
                            category = 'person_relationship_status'))

        self.ui_master_data.pushButton_edit_person_relationship_status.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status,
                             set_text = self.ui_master_data.lineEdit_person_relationship_status.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship_status,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_relationship_status.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_relationship_status, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                             line_edit_obj_person_relationship_status = self.ui_master_data.lineEdit_edit_person_relationship_status,
                             counter_label_object = self.ui_master_data.label_person_relationship_status_counter,
                             label_object = self.ui_master_data.label_person_relationship_status_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_save_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_edit_person_relationship_status, False),
                                                        (self.ui_master_data.pushButton_delete_person_relationship_status, False),
                                                        (self.ui_master_data.tree_view_person_relationship_status, False),
                                                        (self.ui_master_data.lineEdit_search_person_relationship_status, False),
                                                        (self.ui_master_data.lineEdit_person_relationship_status, False)],
                             model = self._standard_item_model_person_relationship_status,
                             category = 'person_relationship_status'))

        self.ui_master_data.pushButton_cancel_edit_person_relationship_status.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_relationship_status, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_relationship_status.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship_status, False),
                                                                    (self.ui_master_data.tree_view_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship_status, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_relationship_status,
                                        list_header = self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS, 
                                        label_object = self.ui_master_data.label_person_relationship_status_counter,
                                        model = self._standard_item_model_person_relationship_status,
                                        category = 'person_relationship_status'))

        self.ui_master_data.pushButton_delete_person_relationship_status.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_relationship_status,
                                    counter_label_object = self.ui_master_data.label_person_relationship_status_counter,
                                    label_object = self.ui_master_data.label_person_relationship_status_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_relationship_status,
                                    list_header = self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship_status, False),
                                                                    (self.ui_master_data.tree_view_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship_status, False)],
                                    model = self._standard_item_model_person_relationship_status,
                                    category = 'person_relationship_status'))


        '''
        ########################################################
        ############### Person Relationship ####################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_person_relationship.clicked.connect(lambda:
            self.add_record(line_edit_obj_person_relationship = self.ui_master_data.lineEdit_person_relationship,
                            counter_label_object = self.ui_master_data.label_person_relationship_counter,
                            label_object = self.ui_master_data.label_person_relationship_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship, False),
                                                        (self.ui_master_data.pushButton_save_person_relationship, False),
                                                        (self.ui_master_data.pushButton_edit_person_relationship, False),
                                                        (self.ui_master_data.pushButton_delete_person_relationship, False),
                                                        (self.ui_master_data.tree_view_person_relationship, False),
                                                        (self.ui_master_data.lineEdit_search_person_relationship, False),
                                                        (self.ui_master_data.lineEdit_person_relationship, False)],
                            tree_view_object = self.ui_master_data.tree_view_person_relationship,
                            line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                            list_header = self.LIST_HEADER_PERSON_RELATIONSHIP,
                            model = self._standard_item_model_person_relationship,
                            category = 'person_relationship'))

        self.ui_master_data.pushButton_edit_person_relationship.clicked.connect(lambda:
            self.handle_edit(index = 1, 
                             stack_widget_object = self.ui_master_data.stackedWidget_person_relationship,
                             set_text = self.ui_master_data.lineEdit_person_relationship.text(),
                             lineEdit_object = self.ui_master_data.lineEdit_edit_person_relationship,
                             set_focus = True,
                             clear_widget = True,
                             load_data = False))
        
        self.ui_master_data.pushButton_apply_change_person_relationship.clicked.connect(lambda:
            self.edit_record(index = 0,                              
                             stackWidget_obj = self.ui_master_data.stackedWidget_person_relationship, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                             line_edit_obj_person_relationship = self.ui_master_data.lineEdit_edit_person_relationship,
                             counter_label_object = self.ui_master_data.label_person_relationship_counter,
                             label_object = self.ui_master_data.label_person_relationship_counter,
                             tree_view_object = self.ui_master_data.tree_view_person_gender,
                             list_header = self.LIST_HEADER_PERSON_RELATIONSHIP,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship, False),
                                                        (self.ui_master_data.pushButton_save_person_relationship, False),
                                                        (self.ui_master_data.pushButton_edit_person_relationship, False),
                                                        (self.ui_master_data.pushButton_delete_person_relationship, False),
                                                        (self.ui_master_data.tree_view_person_relationship, False),
                                                        (self.ui_master_data.lineEdit_search_person_relationship, False),
                                                        (self.ui_master_data.lineEdit_person_relationship, False)],
                             model = self._standard_item_model_person_relationship,
                             category = 'person_relationship'))

        self.ui_master_data.pushButton_cancel_edit_person_relationship.clicked.connect(lambda:
            self.stack_set_current_index(index = 0, 
                                         stack_widget_object = self.ui_master_data.stackedWidget_person_relationship, 
                                         clear_widget = True,
                                         current_obj_clear_selection = True,
                                         delete_selected_id = True,
                                         current_obj_clear = True))

        self.ui_master_data.pushButton_refresh_person_relationship.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship, False),
                                                                    (self.ui_master_data.tree_view_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship, False)],
                                        tree_view_object = self.ui_master_data.tree_view_person_relationship,
                                        list_header = self.LIST_HEADER_PERSON_RELATIONSHIP, 
                                        label_object = self.ui_master_data.label_person_relationship_counter,
                                        model = self._standard_item_model_person_relationship,
                                        category = 'person_relationship'))

        self.ui_master_data.pushButton_delete_person_relationship.clicked.connect(lambda:
            self.delete_record(id = self.current_selected_id,
                                    line_edit_obj = self.ui_master_data.lineEdit_person_relationship,
                                    counter_label_object = self.ui_master_data.label_person_relationship_counter,
                                    label_object = self.ui_master_data.label_person_relationship_counter,
                                    tree_view_object = self.ui_master_data.tree_view_person_relationship,
                                    list_header = self.LIST_HEADER_PERSON_RELATIONSHIP, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship, False),
                                                                    (self.ui_master_data.tree_view_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship, False)],
                                    model = self._standard_item_model_person_relationship,
                                    category = 'person_relationship'))


        '''
        ########################################################
        ################ General Region ########################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_general_region.clicked.connect(lambda:
            self.add_record(line_edit_obj_general_region=self.ui_master_data.lineEdit_general_region,
                            counter_label_object=self.ui_master_data.label_general_region_counter,
                            label_object=self.ui_master_data.label_general_region_counter,
                            tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_region, False),
                                                                    (self.ui_master_data.pushButton_save_general_region, False),
                                                                    (self.ui_master_data.pushButton_edit_general_region, False),
                                                                    (self.ui_master_data.pushButton_delete_general_region, False),
                                                                    (self.ui_master_data.tree_view_general_region, False),
                                                                    (self.ui_master_data.lineEdit_search_general_region, False),
                                                                    (self.ui_master_data.lineEdit_general_region, False)],
                            tree_view_object=self.ui_master_data.tree_view_general_region,
                            line_edit_object_get_focus=self.ui_master_data.lineEdit_general_region,
                            list_header=self.LIST_HEADER_GENERAL_REGION,
                            model = self._standard_item_model_general_region,
                            category='general_region'))

        self.ui_master_data.pushButton_edit_general_region.clicked.connect(lambda:
            self.handle_edit(index=1, 
                             stack_widget_object=self.ui_master_data.stackedWidget_general_region,
                             set_text=self.ui_master_data.lineEdit_general_region.text(),
                             lineEdit_object=self.ui_master_data.lineEdit_edit_general_region,
                             set_focus=True,
                             clear_widget=True,
                             load_data=False))
        
        self.ui_master_data.pushButton_apply_change_general_region.clicked.connect(lambda:
            self.edit_record(index=0,                              
                             stackWidget_obj=self.ui_master_data.stackedWidget_general_region, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                             line_edit_obj_general_region=self.ui_master_data.lineEdit_edit_general_region,
                             counter_label_object=self.ui_master_data.label_general_region_counter,
                             label_object=self.ui_master_data.label_general_region_counter,
                             tree_view_object=self.ui_master_data.tree_view_person_gender,
                             list_header=self.LIST_HEADER_GENERAL_REGION,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_region, False),
                                                                    (self.ui_master_data.pushButton_save_general_region, False),
                                                                    (self.ui_master_data.pushButton_edit_general_region, False),
                                                                    (self.ui_master_data.pushButton_delete_general_region, False),
                                                                    (self.ui_master_data.tree_view_general_region, False),
                                                                    (self.ui_master_data.lineEdit_search_general_region, False),
                                                                    (self.ui_master_data.lineEdit_general_region, False)],
                             model = self._standard_item_model_general_region,
                             category='general_region'))

        self.ui_master_data.pushButton_cancel_edit_general_region.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_region, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        self.ui_master_data.pushButton_refresh_general_region.clicked.connect(lambda:
        self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_region, False),
                                                                (self.ui_master_data.pushButton_save_general_region, False),
                                                                (self.ui_master_data.pushButton_edit_general_region, False),
                                                                (self.ui_master_data.pushButton_delete_general_region, False),
                                                                (self.ui_master_data.tree_view_general_region, False),
                                                                (self.ui_master_data.lineEdit_search_general_region, False),
                                                                (self.ui_master_data.lineEdit_general_region, False)],
                                        tree_view_object=self.ui_master_data.tree_view_general_region,
                                        list_header=self.LIST_HEADER_GENERAL_REGION, 
                                        label_object=self.ui_master_data.label_general_region_counter,
                                        model = self._standard_item_model_general_region,
                                        category='general_region'))

        self.ui_master_data.pushButton_delete_general_region.clicked.connect(lambda:
            self.delete_record(id=self.current_selected_id,
                                    line_edit_obj=self.ui_master_data.lineEdit_general_region,
                                    counter_label_object=self.ui_master_data.label_general_region_counter,
                                    label_object=self.ui_master_data.label_general_region_counter,
                                    tree_view_object=self.ui_master_data.tree_view_general_region,
                                    list_header=self.LIST_HEADER_GENERAL_REGION, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_region, False),
                                                                (self.ui_master_data.pushButton_save_general_region, False),
                                                                (self.ui_master_data.pushButton_edit_general_region, False),
                                                                (self.ui_master_data.pushButton_delete_general_region, False),
                                                                (self.ui_master_data.tree_view_general_region, False),
                                                                (self.ui_master_data.lineEdit_search_general_region, False),
                                                                (self.ui_master_data.lineEdit_general_region, False)],
                                    model = self._standard_item_model_general_region,
                                    category='general_region'))


        '''
        ########################################################
        ################# General Award ########################
        ########################################################
        '''
        self.ui_master_data.pushButton_save_general_award.clicked.connect(lambda:
            self.add_record(line_edit_obj_general_award=self.ui_master_data.lineEdit_general_award,
                            counter_label_object=self.ui_master_data.label_general_award_counter,
                            label_object=self.ui_master_data.label_general_award_counter,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                            tree_view_object=self.ui_master_data.tree_view_general_award,
                            line_edit_object_get_focus=self.ui_master_data.lineEdit_general_award,
                            list_header=self.LIST_HEADER_GENERAL_AWARD,
                            model = self._standard_item_model_general_award,
                            category='general_award'))

        self.ui_master_data.pushButton_edit_general_award.clicked.connect(lambda:
            self.handle_edit(index=1, 
                             stack_widget_object=self.ui_master_data.stackedWidget_general_award,
                             set_text=self.ui_master_data.lineEdit_general_award.text(),
                             lineEdit_object=self.ui_master_data.lineEdit_edit_general_award,
                             set_focus=True,
                             clear_widget=True,
                             load_data=False))
        
        self.ui_master_data.pushButton_apply_change_general_award.clicked.connect(lambda:
            self.edit_record(index=0,                              
                             stackWidget_obj=self.ui_master_data.stackedWidget_general_award, 
                             line_edit_object_get_focus = self.ui_master_data.lineEdit_general_award,
                             line_edit_obj_general_award=self.ui_master_data.lineEdit_edit_general_award,
                             counter_label_object=self.ui_master_data.label_general_award_counter,
                             label_object=self.ui_master_data.label_general_award_counter,
                             tree_view_object=self.ui_master_data.tree_view_person_gender,
                             list_header=self.LIST_HEADER_GENERAL_AWARD,
                             tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                             model = self._standard_item_model_general_award,
                             category='general_award'))

        self.ui_master_data.pushButton_cancel_edit_general_award.clicked.connect(lambda:
            self.stack_set_current_index(index=0, 
                                         stack_widget_object=self.ui_master_data.stackedWidget_general_award, 
                                         clear_widget=True,
                                         current_obj_clear_selection=True,
                                         delete_selected_id = True,
                                         current_obj_clear=True))

        self.ui_master_data.pushButton_refresh_general_award.clicked.connect(lambda:
            self.fetch_all(tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                                      (self.ui_master_data.pushButton_save_general_country, False),
                                                                      (self.ui_master_data.pushButton_edit_general_country, False),
                                                                      (self.ui_master_data.pushButton_delete_general_country, False),
                                                                      (self.ui_master_data.tree_view_general_country, False),
                                                                      (self.ui_master_data.lineEdit_search_general_country, False),
                                                                      (self.ui_master_data.lineEdit_general_country, False)],
                                        tree_view_object=self.ui_master_data.tree_view_general_award,
                                        list_header=self.LIST_HEADER_GENERAL_AWARD, 
                                        label_object=self.ui_master_data.label_general_award_counter,
                                        model = self._standard_item_model_general_award,
                                        category='general_award'))

        self.ui_master_data.pushButton_delete_general_award.clicked.connect(lambda:
            self.delete_record(id=self.current_selected_id,
                                    line_edit_obj=self.ui_master_data.lineEdit_general_award,
                                    counter_label_object=self.ui_master_data.label_general_award_counter,
                                    label_object=self.ui_master_data.label_general_award_counter,
                                    tree_view_object=self.ui_master_data.tree_view_general_award,
                                    list_header=self.LIST_HEADER_GENERAL_AWARD, 
                                    tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                          (self.ui_master_data.pushButton_save_general_country, False),
                                                          (self.ui_master_data.pushButton_edit_general_country, False),
                                                          (self.ui_master_data.pushButton_delete_general_country, False),
                                                          (self.ui_master_data.tree_view_general_country, False),
                                                          (self.ui_master_data.lineEdit_search_general_country, False),
                                                          (self.ui_master_data.lineEdit_general_country, False)],
                                    model = self._standard_item_model_general_award,
                                    category='general_award'))



        #self.ui_master_data.pushButton_close.clicked.connect(lambda: self.stop_current_task_thread(stop_mode="interrupt"))
        self.ui_master_data.pushButton_master_data_close.clicked.connect(self.close)

        return

    def init_lineEdit_pixmap(self):

        search_glass_pixmap_style_shett = "QLineEdit { \
                                    background-image: url(:/img_16x16_default/default/img_16x16/search_grey.png); \
                                    background-repeat: no-repeat; \
                                    background-position: left; \
                                    font-family: SegoeUI; \
                                    padding: 2 2 2 20; /* left padding (last number) must be more than the icon's width */ \
                                    };"

        '''
            ###################################################
            ###################  Film Genre  ##################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_film_genre.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            #################  General Country  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_general_country.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            #################  General Language  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_general_language.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ##################  Person Gender  ################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_gender.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ###############  Person Nationality  ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_nationality.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ###############  Person Salutation  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_salutation.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ##################  Person Title  #################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_title.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ###############  Person Hair Color  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_hair_color.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ################  Person Eye Color  ###############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_eye_color.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ################  Person Religion  ################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_religion.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ###########  Person Relationship Status ###########
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_relationship_status.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ###############  Person Relationship ##############
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_person_relationship.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ################  General Region  #################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_general_region.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ################  General Award  ##################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_general_award.setStyleSheet(search_glass_pixmap_style_shett)

        '''
            ###################################################
            ##################  Navigation  ###################
            ###################################################
        '''
        self.ui_master_data.lineEdit_search_navigation.setStyleSheet(search_glass_pixmap_style_shett)

        return

    def init_textChanged_signal_line_edit(self):
        '''
            NOTICE:
            =======
            This method is used to set signal and slots of textChanged for QLineEdit()-objects. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set the signal and slots for QLineEdit()-object(s)")

        '''
            ###################################################
            ###################  Film Genre  ##################
            ###################################################
        '''

        self.ui_master_data.lineEdit_film_genre.textChanged.connect(lambda: 
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_film_genre))

        self.ui_master_data.lineEdit_edit_film_genre.textChanged.connect(lambda: 
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_film_genre,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_film_genre.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_film_genre.text(),
                               model=self._proxy_model))
        '''
            ###################################################
            ################  General Country   ###############
            ###################################################
        '''

        self.ui_master_data.lineEdit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_general_country))

        self.ui_master_data.lineEdit_edit_general_country.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_general_country,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_general_country.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_general_country.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  General Language  ###############
            ###################################################
        '''

        self.ui_master_data.lineEdit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_general_language))

        self.ui_master_data.lineEdit_edit_general_language.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_general_language,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_general_language.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_general_language.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ##################  Person Gender  ################
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_gender))

        self.ui_master_data.lineEdit_edit_person_gender.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_gender,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_gender.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_gender.text(),
                               model=self._proxy_model))


        '''
            ###################################################
            ################  Person Nationality  #############
            ###################################################
        '''
        self.ui_master_data.lineEdit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_nationality))

        self.ui_master_data.lineEdit_edit_person_nationality.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_nationality,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_nationality.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_nationality.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  Person Salutation  ##############
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_salutation))

        self.ui_master_data.lineEdit_edit_person_salutation.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_salutation,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_salutation.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_salutation.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ##################  Person Title  #################
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_title))

        self.ui_master_data.lineEdit_edit_person_title.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_title,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_title.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_title.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  Person Hair Color  ##############
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_hair_color))

        self.ui_master_data.lineEdit_edit_person_hair_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_hair_color,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_hair_color.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_hair_color.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  Person Eye Color  ###############
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_eye_color))

        self.ui_master_data.lineEdit_edit_person_eye_color.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_eye_color,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_eye_color.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_eye_color.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  Person Religion  ################
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_religion))

        self.ui_master_data.lineEdit_edit_person_religion.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_religion,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_religion.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_religion.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ###########  Person Relationship Status ###########
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_relationship_status))

        self.ui_master_data.lineEdit_edit_person_relationship_status.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_relationship_status,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_relationship_status.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_relationship_status_status.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ##############  Person Relationship ###############
            ###################################################
        '''

        self.ui_master_data.lineEdit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_person_relationship))

        self.ui_master_data.lineEdit_edit_person_relationship.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_person_relationship,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_person_relationship.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_person_relationship_status.text(),
                               model=self._proxy_model))

        '''
            ###################################################
            ################  General Region  #################
            ###################################################
        '''

        self.ui_master_data.lineEdit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_general_region,))

        self.ui_master_data.lineEdit_edit_general_region.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_general_region,
                                     check_selected_id=False))

        # self.ui_master_data.lineEdit_search_general_region.textChanged.connect(lambda: 
        #     self.filter_update(string_text=self.ui_master_data.lineEdit_search_general_region.text(),
        #                        model=self._proxy_model))

        self.ui_master_data.lineEdit_search_general_region.textChanged.connect(self.handle_text_changed_new)

        '''
            ###################################################
            ################  General Award  ##################
            ###################################################
        '''

        self.ui_master_data.lineEdit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_general_award))

        self.ui_master_data.lineEdit_edit_general_award.textChanged.connect(lambda:
            self.handle_text_changed(line_edit_obj=self.ui_master_data.lineEdit_edit_general_award,
                                     check_selected_id=False))

        self.ui_master_data.lineEdit_search_general_award.textChanged.connect(lambda: 
            self.filter_update(string_text=self.ui_master_data.lineEdit_search_general_award.text(),
                               model=self._proxy_model))

        # '''
        #     ###################################################
        #     ###################  Navigation  ##################
        #     ###################################################
        # '''

        # self.ui_master_data.lineEdit_search_navigation.textChanged.connect(lambda: 
        #     self.filter_update(string_text=self.ui_master_data.lineEdit_search_navigation.text(),
        #                        model=self._navigate_proxy_model))

        return

    def set_window_icon(self, image=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowIcon(QIcon(image))

        return

    def set_window_title(self, title=None):
        '''
            NOTICE:
            =======
            This method is used to set title of the window. 

            PARAMETERS:
            ===========
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Set title of the window")

        self.setWindowTitle(title)

        return

    def set_visible_tree_widget_column(self,
                                        tree_widget_object=None,
                                        column_integer=None):

        '''
            NOTICE:
            =======
            This method hides the given column of the given QTreeWidget()-object.

            PARAMETERS:
            ===========
            :tree_widget_object     -   Hiere we need an QTreeWidget()-object to access
                                        the items.

            :column_integer         -   We except an integer so that we know 
                                        which column is to be hidden.
            
            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        logger.info("Hide the given column of QTreeWidget()-object")
        
        tree_widget_object.hideColumn(column_integer)

        return

    def set_cursor_position(self, line_edit_obj=None, pos=None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -
            
            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(line_edit_obj, QLineEdit):
            line_edit_obj.setCursorPosition(pos)

        return

    def set_text(self, line_edit_obj=None, text=None):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj  -

            :text           -

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        if isinstance(line_edit_obj, QLineEdit):
            line_edit_obj.setText(text)

        return

    def generator_widget(self,
                        two_pair_tuples_in_list_widget_boolean = None):

        '''
            NOTICE:
            =======
            Each element of the list is a two-pair tuple.
        '''

        '''
            NOTICE:
            =======
            Iterate over each element of the list, which contains a two-pair tuple.

            PARAMETERS:
            ===========
            :two_pair_tuples_in_list_widget_boolean  -  In this argument there is a list,
                                                        which contains tuples of two pair.
                                                        First elemtn in a tuple is a widget,
                                                        like a OushButton oder QLineEdit, and the
                                                        second element is a boolean value.

            :yield                                  -   Return each widget with a boolean value.
        '''

        if isinstance(two_pair_tuples_in_list_widget_boolean, list):
            for push_button, boolean_value in two_pair_tuples_in_list_widget_boolean:
                yield push_button, boolean_value

    def set_enabled_widget(self, 
                                two_pair_tuples_in_list_widget_boolean=None,
                                 signle_widget=None, 
                                 boolean_value=None):


        if isinstance(two_pair_tuples_in_list_widget_boolean, list):

            try:

                for widget, boolean_value in self.generator_widget(two_pair_tuples_in_list_widget_boolean=two_pair_tuples_in_list_widget_boolean):

                    widget.setEnabled(boolean_value)

            except AttributeError as ArrErr:
                
                desired_trace = format_exc(exc_info())

        elif not signle_widget is None:

            try:

                signle_widget.setEnabled(boolean_value)

            except AttributeError as ArrErr:

                pass

        return

    def generator_widget_object(self, list_widget_object):

        for widget in list_widget_object:
            yield widget

    def clear_widget_object_content(self, list_widget_object=None, widget_object=None):
        '''
            NOTICE:
            =======
            Clear the content of given widget-objects, that supports clear()-method.

            PARAMETERS:
            ===========
            :list_widget_object     -   Here, we get a list of widget-objects.

            :widget_object          -   We get a single widget object.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        if isinstance(list_widget_object, list):

            for widget in self.generator_widget_object(list_widget_object):
                widget.clear()

        if not widget_object is None:
            widget_object.clear()

        return

    def handle_text_changed(self,
                            line_edit_obj=None,
                            push_button_obj_save=None,
                            push_button_obj_delete=None,
                            push_button_obj_edit=None,
                            push_button_obj_refresh=None,
                            change_windows_title=False,
                            allow_trailing_spaces=True,
                            allow_only_number=False, 
                            max_length=None,
                            check_selected_id=True):
        '''
            NOTICE:
            =======
            Updates the input each time a key is pressed.

            PARAMETERS:
            ===========
            :line_edit_obj              -   Here, we want a QWidget()-object: QLineEdit.
                                            Well, we need this object to get the current position
                                            of the cursor.

            :push_button_obj_save       -   specific pushButton()-object for saving data.

            :push_button_obj_delete     -   specific pushButton()-object for deleting data.

            :push_button_obj_edit       -   specific pushButton()-object for editing data.

            :push_button_obj_refresh    -   specific pushButton()-object for refreshing data.


            :change_windows_title       -   In this keyword argument we except a special value: boolean value.
                                            By default, the value is set to False, because we don't want the
                                            window title to change.

            :allow_trailing_spaces      -   We except a boolean value. By default, its set to True.
                                            True means that spaces between the each words are allowed.
                                            It is very rare that we want to ban trailing space.

            :allow_only_number          -   We except a boolean value. When we get True we
                                            want to know we shoukd allow only numbers. By default
                                            the value is set to False, because we want the user to
                                            enter all characters.    

            :max_length                 -   This keyword-argument gets an integer for  setting a maximum length 
                                            of a given text.

            :return                     -   Nothing is returned. The statement 'return'
                                            terminates a function. That makes sure that the
                                            function is definitely finished.
        '''

        get_pos, get_text = self.input_controller.avoid_double_spaces(original_text=unicode(line_edit_obj.text()),
                                                                      widget_object=line_edit_obj, 
                                                                      allow_trailing_spaces=allow_trailing_spaces,
                                                                      allow_only_number=allow_only_number,
                                                                      max_length=max_length)


        self.set_text(line_edit_obj=line_edit_obj, text=get_text)
        self.set_cursor_position(line_edit_obj=line_edit_obj, pos=get_pos)

        '''
            When the user types the translated title of the film
            the method is set the windows title.
        '''
        if get_text:
            '''
                No, the string isn't empty
            '''
            self.set_text(line_edit_obj=line_edit_obj, text=get_text)
            self.set_cursor_position(line_edit_obj=line_edit_obj, pos=get_pos)

            # self.set_enabled_widget(signle_widget=push_button_obj_save,
            #                              boolean_value=True)

        else:
            '''
                Bad news, the string is emptyx.
            '''

            self.current_selected_id=None

        #     self.set_enabled_widget(signle_widget=push_button_obj_save,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                  boolean_value=False)

        #     self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                  boolean_value=False)


        # if check_selected_id:

        #     '''
        #         Its possible that the variable named (self.selected_be_edited_id) is still
        #         not empty. For instance the user clicked on a TreeView()-object. In 
        #         this case special pushButton()-obejcts must be enabled oder disabled.
        #     '''
        #     if not self.selected_be_edited_id is None:


        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=True)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=True)

        #     else:

        #         self.set_enabled_widget(signle_widget=push_button_obj_edit,
        #                                      boolean_value=False)

        #         self.set_enabled_widget(signle_widget=push_button_obj_delete,
        #                                      boolean_value=False)

        return

    def manage_master_data_page(self, stack_page_id):

        self.remove_current_selected_id()
        self.remove_selected_be_edited_id()

        dict_stack_page={

                        01: (lambda: self.set_focus(widget_object=self.ui_master_data.lineEdit_film_genre),
                              lambda: self.fetch_all(index=stack_page_id,
                                      tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_film_genre, False),
                                                                        (self.ui_master_data.lineEdit_film_genre, False)],
                                      line_edit_object_get_focus=self.ui_master_data.lineEdit_film_genre,
                                      list_header=self.LIST_HEADER_GENRE_FILM,
                                      tree_view_object=self.ui_master_data.tree_view_film_genre, 
                                      label_object=self.ui_master_data.label_genre_film_counter,
                                      model = self._standard_item_model_film_genre,
                                      category='film_genre')),

                        29: (lambda: self.set_focus(widget_object=self.ui_master_data.lineEdit_person_gender),
                              lambda: self.fetch_all(index=stack_page_id,
                                      tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_person_gender, False), 
                                                                        (self.ui_master_data.pushButton_delete_person_gender, False),
                                                                        (self.ui_master_data.pushButton_save_person_gender, False),
                                                                        (self.ui_master_data.pushButton_edit_person_gender, False),
                                                                        (self.ui_master_data.lineEdit_search_person_gender, False),
                                                                        (self.ui_master_data.tree_view_person_gender, False),
                                                                        (self.ui_master_data.lineEdit_person_gender, False)],
                                      line_edit_object_get_focus=self.ui_master_data.lineEdit_person_gender,
                                      list_header=self.LIST_HEADER_PERSON_GENDER,
                                      tree_view_object=self.ui_master_data.tree_view_person_gender, 
                                      label_object=self.ui_master_data.label_person_gender_counter,
                                      model = self._standard_item_model_person_gender,
                                      category='person_gender')),

                        30: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_salutation),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_salutation, False),
                                                                   (self.ui_master_data.pushButton_save_person_salutation, False),
                                                                   (self.ui_master_data.pushButton_edit_person_salutation, False),
                                                                   (self.ui_master_data.pushButton_delete_person_salutation, False),
                                                                   (self.ui_master_data.tree_view_person_salutation, False),
                                                                   (self.ui_master_data.lineEdit_search_person_salutation, False),
                                                                   (self.ui_master_data.lineEdit_person_salutation, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_salutation,
                                      list_header = self.LIST_HEADER_PERSON_SALUTATION,
                                      tree_view_object = self.ui_master_data.tree_view_person_salutation, 
                                      label_object = self.ui_master_data.label_person_salutation_counter,
                                      model = self._standard_item_model_person_salutation,
                                      category = 'person_salutation')),

                        31: (lambda: self.set_focus(widget_object=self.ui_master_data.lineEdit_person_title),
                              lambda: self.fetch_all(index=stack_page_id,
                                      tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_person_title, False),
                                                                  (self.ui_master_data.pushButton_save_person_title, False),
                                                                  (self.ui_master_data.pushButton_edit_person_title, False),
                                                                  (self.ui_master_data.pushButton_delete_person_title, False),
                                                                  (self.ui_master_data.tree_view_person_title, False),
                                                                  (self.ui_master_data.lineEdit_search_person_title, False),
                                                                  (self.ui_master_data.lineEdit_person_title, False)],
                                      line_edit_object_get_focus=self.ui_master_data.lineEdit_person_title,
                                      list_header=self.LIST_HEADER_PERSON_TITLE,
                                      tree_view_object=self.ui_master_data.tree_view_person_title, 
                                      label_object=self.ui_master_data.label_person_title_counter,
                                      model = self._standard_item_model_person_title,
                                      category='person_title')),

                        32: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_nationality),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_nationality, False),
                                                                    (self.ui_master_data.pushButton_save_person_nationality, False),
                                                                     (self.ui_master_data.pushButton_edit_person_nationality, False),
                                                                     (self.ui_master_data.pushButton_delete_person_nationality, False),
                                                                     (self.ui_master_data.tree_view_person_nationality, False),
                                                                     (self.ui_master_data.lineEdit_search_person_nationality, False),
                                                                     (self.ui_master_data.lineEdit_person_nationality, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_nationality,
                                      list_header = self.LIST_HEADER_PERSON_NATIONALITY,
                                      tree_view_object = self.ui_master_data.tree_view_person_nationality, 
                                      label_object = self.ui_master_data.label_person_nationality_counter,
                                      model = self._standard_item_model_person_nationality,
                                      category = 'person_nationality')),

                        33: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_eye_color),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_save_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_edit_person_eye_color, False),
                                                                  (self.ui_master_data.pushButton_delete_person_eye_color, False),
                                                                  (self.ui_master_data.tree_view_person_eye_color, False),
                                                                  (self.ui_master_data.lineEdit_search_person_eye_color, False),
                                                                  (self.ui_master_data.lineEdit_person_eye_color, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_eye_color,
                                      list_header = self.LIST_HEADER_PERSON_EYE_COLOR,
                                      tree_view_object = self.ui_master_data.tree_view_person_eye_color, 
                                      label_object = self.ui_master_data.label_person_eye_color_counter,
                                      model = self._standard_item_model_person_eye_color,
                                      category = 'person_eye_color')),

                        34: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_hair_color),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_save_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_edit_person_hair_color, False),
                                                                    (self.ui_master_data.pushButton_delete_person_hair_color, False),
                                                                    (self.ui_master_data.tree_view_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_search_person_hair_color, False),
                                                                    (self.ui_master_data.lineEdit_person_hair_color, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_hair_color,
                                      list_header = self.LIST_HEADER_PERSON_HAIR_COLOR,
                                      tree_view_object = self.ui_master_data.tree_view_person_hair_color, 
                                      label_object = self.ui_master_data.label_person_hair_color_counter,
                                      model = self._standard_item_model_person_hair_color,
                                      category = 'person_hair_color')),

                        35: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_religion),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_religion, False),
                                                                  (self.ui_master_data.pushButton_save_person_religion, False),
                                                                  (self.ui_master_data.pushButton_edit_person_religion, False),
                                                                  (self.ui_master_data.pushButton_delete_person_religion, False),
                                                                  (self.ui_master_data.tree_view_person_religion, False),
                                                                  (self.ui_master_data.lineEdit_search_person_religion, False),
                                                                  (self.ui_master_data.lineEdit_person_religion, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_religion,
                                      list_header = self.LIST_HEADER_PERSON_RELIGION,
                                      tree_view_object = self.ui_master_data.tree_view_person_religion, 
                                      label_object = self.ui_master_data.label_person_religion_counter,
                                      model = self._standard_item_model_person_religion,
                                      category = 'person_religion')),

                        36: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_relationship_status),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship_status, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship_status, False),
                                                                    (self.ui_master_data.tree_view_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship_status, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship_status, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship_status,
                                      list_header = self.LIST_HEADER_PERSON_RELATIONSHIP_STATUS,
                                      tree_view_object = self.ui_master_data.tree_view_person_relationship_status, 
                                      label_object = self.ui_master_data.label_person_relationship_status_counter,
                                      model = self._standard_item_model_person_relationship_status,
                                      category = 'person_relationship_status')),

                        37: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_person_relationship),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_save_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_edit_person_relationship, False),
                                                                    (self.ui_master_data.pushButton_delete_person_relationship, False),
                                                                    (self.ui_master_data.tree_view_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_search_person_relationship, False),
                                                                    (self.ui_master_data.lineEdit_person_relationship, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_person_relationship,
                                      list_header = self.LIST_HEADER_PERSON_RELATIONSHIP,
                                      tree_view_object = self.ui_master_data.tree_view_person_relationship, 
                                      label_object = self.ui_master_data.label_person_relationship_counter,
                                      model = self._standard_item_model_person_relationship,
                                      category = 'person_relationship')),

                        48: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_language),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_language, False),
                                                                  (self.ui_master_data.pushButton_save_general_language, False),
                                                                  (self.ui_master_data.pushButton_edit_general_language, False),
                                                                  (self.ui_master_data.pushButton_delete_general_language, False),
                                                                  (self.ui_master_data.tree_view_general_language, False),
                                                                  (self.ui_master_data.lineEdit_search_general_language, False),
                                                                  (self.ui_master_data.lineEdit_general_language, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_general_language,
                                      list_header = self.LIST_HEADER_GENERAL_LANGUAGE,
                                      tree_view_object = self.ui_master_data.tree_view_general_language, 
                                      label_object = self.ui_master_data.label_general_language_counter,
                                      model = self._standard_item_model_general_language,
                                      category = 'general_language')),

                        49: (lambda: self.set_focus(widget_object=self.ui_master_data.lineEdit_general_award),
                              lambda: self.fetch_all(index=stack_page_id,
                                      tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_general_award, False),
                                                                        (self.ui_master_data.lineEdit_general_award, False)],
                                      line_edit_object_get_focus=self.ui_master_data.lineEdit_general_award,
                                      list_header=self.LIST_HEADER_GENERAL_AWARD,
                                      tree_view_object=self.ui_master_data.tree_view_general_award, 
                                      label_object=self.ui_master_data.label_general_award_counter,
                                      model = self._standard_item_model_general_award,
                                      category='general_award')),

                        50: (lambda: self.set_focus(widget_object = self.ui_master_data.lineEdit_general_country),
                              lambda: self.fetch_all(index = stack_page_id,
                                      tuples_list_widget_boolean = [(self.ui_master_data.pushButton_refresh_general_country, False),
                                                                      (self.ui_master_data.pushButton_save_general_country, False),
                                                                      (self.ui_master_data.pushButton_edit_general_country, False),
                                                                      (self.ui_master_data.pushButton_delete_general_country, False),
                                                                      (self.ui_master_data.tree_view_general_country, False),
                                                                      (self.ui_master_data.lineEdit_search_general_country, False),
                                                                      (self.ui_master_data.lineEdit_general_country, False)],
                                      line_edit_object_get_focus = self.ui_master_data.lineEdit_general_country,
                                      list_header = self.LIST_HEADER_GENERAL_COUNTRY,
                                      tree_view_object = self.ui_master_data.tree_view_general_country, 
                                      label_object = self.ui_master_data.label_general_country_counter,
                                      model = self._standard_item_model_general_country,
                                      category = 'general_country')),

                        51: (lambda: self.set_focus(widget_object=self.ui_master_data.lineEdit_general_region),
                              lambda: self.fetch_all(index=stack_page_id,
                                      tuples_list_widget_boolean=[(self.ui_master_data.pushButton_refresh_general_region, False),
                                                                        (self.ui_master_data.lineEdit_general_region, False)],
                                      line_edit_object_get_focus=self.ui_master_data.lineEdit_general_region,
                                      list_header=self.LIST_HEADER_GENERAL_REGION,
                                      tree_view_object=self.ui_master_data.tree_view_general_region, 
                                      label_object=self.ui_master_data.label_general_region_counter,
                                      model = self._standard_item_model_general_region,
                                      category='general_region'))}


        try:
            function_first, function_second = dict_stack_page[int(stack_page_id)]
            function_first()
            function_second()

        except KeyError as key_err:
            print "key_err", key_err

        return

    def handle_edit(self, index=None, 
                          new_index=None,
                          stack_widget_object=None,
                          tree_widget_object=None, 
                          tree_view_object=None,
                          column_index=None,
                          set_text=None, 
                          lineEdit_object=None,
                          set_focus=None,
                          clear_widget=True,
                          load_data=True,
                          current_obj_clear_selection=False,
                          current_obj_clear=False):

        try:

            print "int(self.selected_be_edited_id)", int(self.selected_be_edited_id)

            self.stack_set_current_index(index=index, 
                                        new_index=new_index,
                                        stack_widget_object=stack_widget_object,
                                        tree_widget_object=tree_widget_object, 
                                        tree_view_object=tree_view_object,
                                        column_index=column_index,
                                        set_text=set_text, 
                                        lineEdit_object=lineEdit_object,
                                        set_focus=set_focus,
                                        clear_widget=clear_widget,
                                        load_data=load_data,
                                        current_obj_clear_selection=current_obj_clear_selection,
                                        current_obj_clear=current_obj_clear)

        except TypeError:

            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or\
            not desired_trace.find("int() argument must be a string or a number, not") == -1:

                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to edit an item. You must select an item to edit.")

                self.create_generally_crritcal_msg(title_text, conn_err, "")


    def stack_set_current_index(self, 
                                index=None, 
                                new_index=None,
                                stack_widget_object=None,
                                tree_widget_object=None, 
                                tree_view_object=None,
                                column_index=None,
                                set_text=None, 
                                lineEdit_object=None,
                                set_focus=None,
                                clear_widget=True,
                                load_data=True,
                                current_obj_clear_selection=False,
                                delete_selected_id = False,
                                current_obj_clear=False):

        '''
            NOTICE:
            ======
            This functions changes widget of QStackedWidget and its fades 
            between widgets on different pages - dependent on the index. 

            PARAMETERS:
            ===========
            :index -        This function excepts an index of an QListWidget()-object.
                            This method is connected with an QListWidget()-object over 
                            currentRowChanged()-signal. This object emits the index of 
                            the chosen element if the current item hase been changed and 
                            the row is different to the row of the previous current item.
                            This method gets the current index of changed item.

            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("change the widget of QStackedWidget")

        self.quite_threads()

        if not isinstance(new_index, bool):

            if not new_index is None:

                index = new_index.indexes()[1].data()#.toPyObject()
                print "index", index

        self.stack_widget_page = int(index)

        '''
            When the user changes the page of QStackedWidget all QLineEdit()-objects,
            which are children of current widget, are deleted or cleared. That means,
            when the user leaves the current page and goes to the other new page, 
            all QLineEdit()-object, which are childrem from old pages, should be 
            deleted or cleared.

            First, we check, if the variables named (self._new_stack_page) is not None.
        '''
        if not self._new_stack_page is None:
            '''
                Yes, its not None. The variable isn't empty.
                Great. Ntext step, we save the current index in the variable named 
                (self._new_stack_page).
            '''

            self._new_stack_page = index

            '''
                After the new index has been saved, first we create a list of all QLineEdit()-objects,
                which are children of the old stack page, that the user was left. In this case we
                need to use the old page number, which is saved in the variable named (self._old_stack_page).
                The created list we sent it to the method called self.clear_widget_object_content().
            '''
            if clear_widget:

                self.clear_widget_object_content(
                    list_widget_object=self.ui_master_data.stackedWidget.widget(int(self._old_stack_page)).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

                # TODO: Think about what to do with selected ID

                #self.selected_be_edited_id = None

            '''
                After all QLineEdit()-objects has beend deleted or cleared we set the old page with the new page. 
                That means, we overwrite the old page number with the given new page number. We create a copy of them.
            '''
            self._old_stack_page = self._new_stack_page

        else:

            '''
                Maybe the user starts this window at first time. Now we have to set the new and old page at the
                same time.
            '''
            self._new_stack_page = index
            self._old_stack_page = index

            if clear_widget:

                self.clear_widget_object_content(
                    list_widget_object=self.ui_master_data.stackedWidget.widget(self._old_stack_page).findChildren((QLineEdit, QTextEdit, QPlainTextEdit)))

        if not lineEdit_object is None:
            self.set_text(line_edit_obj=lineEdit_object, text=set_text)

        if set_focus:
            self.set_focus(widget_object=lineEdit_object)

        if load_data:
            self.manage_master_data_page(index)


        stack_widget_object.setCurrentIndex(int(index))

        if current_obj_clear_selection:

            # self.clear_widget_object_content(
            #     list_widget_object=

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QTreeView))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clearSelection()

        if current_obj_clear:

            current_stacked_id = self.ui_master_data.stackedWidget.currentIndex()
            clear_selection_list = self.ui_master_data.stackedWidget.widget(current_stacked_id).findChildren((QLineEdit, QTextEdit, QPlainTextEdit))
            for widget in self.generator_widget_object(clear_selection_list):

                widget.clear()

        if delete_selected_id:
            self.remove_current_selected_id()
            self.remove_selected_be_edited_id()


        return

    def get_current_item(self, 
                         tree_widget_object=None,
                         column=None):
        '''
            NOTICE:
            ======
            This method access to specific cell of treeWidget()-object. 

            PARAMETERS:
            ===========
            :tree_widget_object -   We except an treeWidget()-object for 
                                    getting data from special item.

            :column             -   We need to know from which column we 
                                    should take data-item.

            :return             -   Return the desired data-item.

        '''

        logger.info("Return the current item")

        return tree_widget_object.currentItem().text(column)

    def remove_current_selected_id(self):
        '''
            NOTICE:
            ======
            This method deletes the variable named (self.current_selected_id),
            which we don't need it no longer.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        logger.info("Set the variable (current_selected_id) on None")

        self.current_selected_id=None

        return

    def remove_selected_be_edited_id(self):
        '''
            NOTICE:
            ======
            This method deletes the variable named (self.selected_be_edited_id),
            which we don't need it no longer.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''

        logger.info("Set the variable (selected_be_edited_id) on None")

        self.selected_be_edited_id = None

        return

    def init_tree_view(self):
        '''
            NOTICE:
            =======
            Here we initialize the QTreeView()-object.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Initialize the QTreeView()-object.")

        #self.ui_master_data.treeView_film_genre_copy.setModel(self._proxy_model)
        #self.ui_master_data.treeView_film_genre_copy.sortByColumn(1, Qt.AscendingOrder)


        delegate = MyDelegate()   

        ''' 
            #####################################
            #########   Film Genre   ############
            #####################################
        '''
        #self.ui_master_data.tree_view_film_genre.setItemDelegate(delegate)

        self.ui_master_data.tree_view_film_genre.setModel(self._proxy_model)
        self.ui_master_data.tree_view_film_genre.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_film_genre.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_film_genre.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_film_genre,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_film_genre,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_film_genre:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            #####################################
            ######   General Country   ##########
            #####################################
        ''' 
        self.ui_master_data.tree_view_general_country.setModel(self._proxy_model)
        self.ui_master_data.tree_view_general_country.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_country.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_country.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_general_country,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_general_country,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_general_country:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))


        ''' 
            #####################################
            ######   General Language   #########
            #####################################
        ''' 
        self.ui_master_data.tree_view_general_language.setModel(self._proxy_model)
        self.ui_master_data.tree_view_general_language.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_language.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_language.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_general_language,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_general_language,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_general_language:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))


        ''' 
            #####################################
            #######   Person Gender   ###########
            #####################################
        ''' 
        self.ui_master_data.tree_view_person_gender.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_gender.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_gender.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_gender.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_gender,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_gender,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_gender:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   Person Nationalität   ###########
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_nationality.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_nationality.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_nationality.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_nationality.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_nationality,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_nationality,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_nationality:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   Person Salutation   #############
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_salutation.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_salutation.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_salutation.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_salutation.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_salutation,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_salutation,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_salutation:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            ##########   Person Title   ###############
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_title.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_title.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_title.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_title.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_title,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_title,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_title:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   Person Hair Color   #############
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_hair_color.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_hair_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_hair_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_hair_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_hair_color,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_hair_color,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_hair_color:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   Person Eye Color   ##############
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_eye_color.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_eye_color.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_eye_color.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_eye_color.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_eye_color,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_eye_color,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_eye_color:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   Person Religion   ###############
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_religion.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_religion.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_religion.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_religion.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_religion,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_religion,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_religion:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            ##   Person Relationship  Status ##########
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_relationship_status.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_relationship_status.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship_status.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship_status.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_relationship_status,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_relationship_status,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_relationship_status:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            ##########  Person Relationship ###########
            ###########################################
        ''' 
        self.ui_master_data.tree_view_person_relationship.setModel(self._proxy_model)
        self.ui_master_data.tree_view_person_relationship.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_person_relationship.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_person_relationship.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_person_relationship,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_person_relationship,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_person_relationship:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            #######   General Region   ################
            ###########################################
        ''' 
        #self.ui_master_data.tree_view_general_region.setModel(self._proxy_model)
        self.ui_master_data.tree_view_general_region.setModel(self._standard_item_model_general_region)
        self.ui_master_data.tree_view_general_region.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_region.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_region.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_general_region,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_general_region,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_general_region:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            ########   General Award   ################
            ###########################################
        ''' 
        self.ui_master_data.tree_view_general_award.setModel(self._proxy_model)
        self.ui_master_data.tree_view_general_award.sortByColumn(1, Qt.AscendingOrder)

        self.ui_master_data.tree_view_general_award.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui_master_data.tree_view_general_award.customContextMenuRequested.connect(lambda position,
            tree_view_obj=self.ui_master_data.tree_view_general_award,
            pushbutton_object_edit=self.ui_master_data.pushButton_edit_general_award,
            pushbutton_object_delete=self.ui_master_data.pushButton_delete_general_award:
        self.open_context_menu(position, tree_view_obj, pushbutton_object_edit, pushbutton_object_delete))

        ''' 
            ###########################################
            ##########   Navigation  ##################
            ###########################################
        ''' 
        self.ui_master_data.treeView_navigation.setModel(self._navigate_proxy_model)
        self.ui_master_data.treeView_navigation.sortByColumn(1, Qt.AscendingOrder)



        
        #self.ui_master_data.comboBox_film_genre.setView(self.ui_master_data.treeView_film_genre_copy)
        #self.ui_master_data.comboBox_film_genre.setModel(self._proxy_model)

        #self.ui_master_data.comboBox_film_genre.setModelColumn(1)

        #self.ui_master_data.treeView_film_genre_copy.hide()

        return

    def set_proxy_later(self):
        print "set_proxy_later_signal is called"
        dict_ex = {'1': lambda: self.set_source_model(model = self._standard_item_model_general_region, mode = "database")}

        dict_ex['1']()
        self.ui_master_data.tree_view_general_region.setModel(self._proxy_model)
        #self._standard_item_model_general_region.clear()

    def set_focus(self, widget_object=None):

        '''
            NOTICE:
            =======
            This method is used to set the focus on given QLineEdit()-object.

            This method is necessary, because the setFocus()-function is used 
            in several places.

            PARAMETERS:
            ===========
            :line_edit_obj  -   We need the QLineEdit()-object to set the focus.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''

        logger.info("Set the focus on given QLineEdit()-object.")

        if not widget_object is None:
            widget_object.setFocus(True)

        return

    # def stop_current_task_thread(self, stop_mode=None):

    #     print "LOOK", self.activated_thread
    #     '''
    #         NOTICE:
    #         =======
    #         This method is used to set the focus on given QLineEdit()-object.

    #         This method is necessary. When the user decided to just stop the 
    #         work (iterating items) of the current task thread by clicking on 
    #         other pages of StackWidget(), we just break the generator -  by raising the StopIteration.
    #         The generator will finish iterating with raising StopIteration.


    #         But, when the user decided to close the window
    #         while task thread runs, we have to interrupt this thread -
    #         without raising the StopIteration. In this case we only stop the timer.
    #         The reason why the timer is just stopped: When we stop the task thread by 
    #         raising the StopIteration we get an RuntimeError, that says the object 
    #         of type QPushButton is being deleted. Because by using StopIteration
    #         the running task thread will try to access the QPushButton()-object(s).

    #         PARAMETERS:
    #         ===========
    #         :stop_mode      -   We need to know in which mode 
    #                             the task thread should stop.

    #         :return         -   Nothing is returned. The statement 'return'
    #                             terminates a function. That makes sure that the
    #                             function is definitely finished.
    #     '''

    #     logger.info("Stop the current Task-Thread")

    #     stop_mode_dict = {'stop': self.sotp_working_thread_signal,
    #                      'interrupt': self.interrupt_working_thread_signal}        

    #     if isinstance(self.activated_thread, QThread):


    #         stop_mode_dict[stop_mode].emit()


    #     elif stop_mode == "interrupt":
    #         '''

    #             Its possible that self.activated_thread no longer contains the 
    #             QThread()-object

    #             The given 'stop_mode' contains 'interrupt', that
    #             means the user is about to close the window. In this case
    #             we just call the closeEvent to close the window.
    #         '''

    #         self.close()

    #     return

    def set_text_count_label(self, 
                             label_object = None, 
                             count = None):
        '''
            NOTICE:
            =======
            Here we display the name in QLineEdit()-object its comes from treeWidget()-object.

            PARAMETERS:
            ===========
            :label_object   -   We need the QLabel()-object to set the text.

            :count          -   We the current counted digit.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''

        logger.info("Set the QLabel for counting")

        label_object.setText("{results}: {count}".format(results=self.tr("Results"), count=count))

        return

    def get_name(self, 
                 tree_widget_obj=None,
                 line_edit_obj=None,
                 list_push_push_button=None):
        '''
            NOTICE:
            =======
            Here we display the name in QLineEdit()-object its comes from treeWidget()-object.

            PARAMETERS:
            ===========
            :tree_widget_obj        -

            :line_edit_obj          -

            :list_push_push_button  -

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        logger.info("Display the given name")

        line_edit_obj.setText(self.get_current_item(tree_widget_object=tree_widget_obj,
                                                    column=1))

        self.set_enabled_widget(two_pair_tuples_in_list_widget_boolean=list_push_push_button, boolean_value=True)


        return        

    def get_id(self, 
               tree_widget_obj=None,
               line_edit_obj=None,
               list_push_push_button=None,
               column=None):
        '''
            NOTICE:
            =======
            Here we save selected id from treeWidget()-object.

            PARAMETERS:
            ===========
            :tree_widget_obj        -

            :line_edit_obj          -

            :list_push_push_button  -

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        current_selected_id = self.get_current_item(tree_widget_object=tree_widget_obj,
                                                    column=column)

        if current_selected_id:
            self.current_selected_id = current_selected_id
            self.selected_be_edited_id = current_selected_id

            self.get_name(tree_widget_obj=tree_widget_obj,
                          line_edit_obj=line_edit_obj,
                          list_push_push_button=list_push_push_button)

        return

    def set_source_model(self, model=None, mode=None):
        '''
            NOTICE:
            =======
            Set main model as source for the proxy model.

            PARAMETERS:
            ===========
            :model      -   We except a model for the proxy model.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        if mode=="database":
            self._proxy_model.setSourceModel(model)

        if mode == "navigation":
            self._navigate_proxy_model.setSourceModel(model)

        return

    def set_proxy_model(self):
        '''
            NOTICE:
            =======
            Create proxy model for filtering and main model for data.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        self._proxy_model = MySortFilterProxyModel(self)
        self._proxy_model.setDynamicSortFilter(True)

        self._navigate_proxy_model = MySortFilterProxyModel(self)
        self._navigate_proxy_model.setDynamicSortFilter(True)

        return

    def set_standard_item_model(self):

        category_list = ['person_salutation',
                         'person_title',
                         'person_nationality',
                         'person_gender',
                         'person_religion',
                         'person_relationship_status',
                         'person_eye_color',
                         'person_hair_color',
                         'person_relationship',
                         'film_genre',
                         'general_region',
                         'general_award',
                         'general_country',
                         'general_language']

        for category in category_list:
            self.standard_item_model(category = category, rows = 0, columns = 3, parent = self)

    def standard_item_model(self, category=None, rows = None, columns = None, parent = None):
        '''
            NOTICE:
            =======
            Create an empty model for the TreeViews' data

            This method constructs a new item model that initially 
            has rows and columns, and that has the given parent.

            QTreeView needs a model to manage its data.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        if category=="person_nationality":
            if not self._standard_item_model_person_nationality is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_nationality.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_nationality = QStandardItemModel(rows, columns, parent)

        if category=="person_salutation":
            if not self._standard_item_model_person_salutation is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_salutation.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_salutation = QStandardItemModel()

        if category=="person_title":
            if not self._standard_item_model_person_title is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_title.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_title = QStandardItemModel()

        if category=="person_gender":
            print "gender is selected"
            if not self._standard_item_model_person_gender is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_gender.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_gender = QStandardItemModel()

        if category=="person_religion":
            if not self._standard_item_model_person_religion is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_religion.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_religion = QStandardItemModel()

        if category=="person_eye_color":
            if not self._standard_item_model_person_eye_color is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_eye_color.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_eye_color = QStandardItemModel()

        if category=="person_hair_color":
            if not self._standard_item_model_person_hair_color is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_hair_color.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_hair_color = QStandardItemModel()

        if category=="person_relationship_status":
            if not self._standard_item_model_person_relationship_status is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_relationship_status.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_relationship_status = QStandardItemModel()

        if category=="person_relationship":
            if not self._standard_item_model_person_relationship is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_person_relationship.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_person_relationship = QStandardItemModel()

        if category=="film_genre":
            if not self._standard_item_model_film_genre is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_film_genre.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_film_genre = QStandardItemModel()

        if category=="general_award":
            if not self._standard_item_model_general_award is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_general_award.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_general_award = QStandardItemModel()

        if category=="general_region":
            if not self._standard_item_model_general_region is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_general_region.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_general_region = QStandardItemModel()

        if category=="general_country":
            if not self._standard_item_model_general_country is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_general_country.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_general_country = QStandardItemModel()

        if category=="general_language":
            if not self._standard_item_model_general_language is None:
                ''' When model already exists only clear the model for adding new items '''
                self._standard_item_model_general_language.clear()
            else:
                ''' The model doesn't exists, create a new one '''
                self._standard_item_model_general_language = QStandardItemModel()

        self._navigate_standard_model = QStandardItemModel()

    def generator_header_data(self, list_header):

        for header_data in list_header:
            yield header_data
        

    def set_header_data(self, model = None, list_header_data = None):
        '''
            NOTICE:
            =======
            This method sets the data for the given role and section 
            in the header with the specified orientation to the value supplied.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        count_column = 0

        for header_data in self.generator_header_data(list_header_data):
            model.setHeaderData(count_column, Qt.Horizontal, header_data)

            count_column +=1 

        return


    def handle_text_changed_new(self, line_edit_obj = None):
        self.timer.stop()
        self.timer.start()

    def filter_update(self, string_text=None, model=None):
        '''
            NOTICE:
            =======
            This method updates the filter of the given model.

            PARAMETERS:
            ===========
            :string_text    -   we except a text für updating the filter.

            :model          -   We need the model where we should update the filter.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        ''' 
        string_text = self.ui_master_data.lineEdit_search_general_region.text()
        model = self._proxy_model
        syntax = QRegExp.RegExp
        caseSensitivity = Qt.CaseInsensitive
        regExp = QRegExp(string_text, caseSensitivity, syntax)
        model.setFilterRegExp(regExp)
        
        return

    def populate_data_item(self, tuple_items = None, model = None):
        '''
            NOTICE:
            =======
            This method populates the given model with the items.

            PARAMETERS:
            ===========
            :tuple_items    -   we need a tuple of item to unpack it later.

            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        
        count_items = len(tuple_items)


        if count_items == 2:

            item_first, item_second = tuple_items

            #img_size = QSize(24, 24)
            #setIcon(QIcon(QPixmap(UI_PATH).scaled(img_size, Qt.KeepAspectRatio)))

            #item2.setIcon(QIcon(QPixmap('rsz_criminal-activities.jpg').scaled(img_size, Qt.KeepAspectRatio)))

            #self._standard_item_model.insertRow(0)

        two_columns_item = [QStandardItem(str(item_first)), QStandardItem(item_second)]
        #item00 = [QtGui.QStandardItem('Title00'), QtGui.QStandardItem('Summary00'), QtGui.QStandardItem('Summary0')]

        model.appendRow(two_columns_item)

            #self._standard_item_model.setItem (0, 0, str(item1))
            #self._standard_item_model.setItem (0, 1, item2)
            #self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            #self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            # self._standard_item_model.setData(self._standard_item_model.index(0, 1), QIcon("boldText.png"))


        if count_items == 3:

            item_first, item_second, item_third = tuple_items

            self._standard_item_model.insertRow(0)
            self._standard_item_model.setData(self._standard_item_model.index(0, 0), str(item_first))
            self._standard_item_model.setData(self._standard_item_model.index(0, 1), str(item_second))
            self._standard_item_model.setData(self._standard_item_model.index(0, 2), str(item_third))

        return

    def set_hide_column(self, 
                        list_tree_view_object=None,
                        tree_view_object=None, 
                        column=None, 
                        bool_value=None):

        if not tree_view_object is None:
            tree_view_object.setColumnHidden(column, bool_value)

        if isinstance(list_tree_view_object, list):
            for tree_view in list_tree_view_object:
                tree_view.setColumnHidden(column, bool_value)


        return

    def cast_list_to_tuple(self, given_list=None):

        '''
            NOTICE:
            =======
            In this method we cast the given list back to tuple.

            PARAMETERS:
            ===========
            :given_list         -       We get a list for casting back to tuple

            :return             -       Return the tuple, where the list was cast in tuple.
        '''
        logger.info("Cast the list back to tuple.")

        return tuple(given_list)

    def create_new_tuples_list(self, push_button=None,
                            boolean_value=None):

        '''
            NOTICE:
            =======
            This method is used to cast the given push_button to a list, 
            insert the second given item for boolean value, then cast the created
            list back to a tuple and then appended the new created tuple to a new list
            and return the result back.

            PARAMETERS:
            ===========
            :push_button        -       We get a QPushButton()-object.

            :boolean_value      -       Here we need the boolean value.

            :return             -       Return the tuple, where the list was cast in tuple.
        '''
        logger.info("CCreate a list with tuples.")


        '''
            First, we need an empty list.
        '''
        list_push_button= []

        '''
            Second, append the given push button to a list.
        '''
        list_push_button.append(push_button)

        '''
            Third, we have to insert the second given item to created list.
        '''
        list_push_button.insert(1, boolean_value)

        '''
            Foruth, we convert the new list to tuple and save it in 
            the variable named list_push_button, so we call the function
            named self.cast_list_to_tuple() by passing the list as argument.
        '''
        tuple_pair_push_button_boolean_value = self.cast_list_to_tuple(given_list=list_push_button)

        '''
            Fifth, we create another new empty list named result_tuples_list_push_button_boolean_value
        '''
        result_tuples_list_push_button_boolean_value = []

        '''
            Sixth, we take the tuple and append it to the new list.
        '''
        result_tuples_list_push_button_boolean_value.append(tuple_pair_push_button_boolean_value)

        '''
            last but not least, we return the list, where is a tuple.
        '''

        return tuple_pair_push_button_boolean_value

    def convert_string_to_bool(self, value):
        '''
            NOTICE:
            ======
            This method converts a given string in boolean value.
            We need this method for all QCheckBox()-objects. In the ini file, where all 
            settings are saved, there are only strings (for example: 'True' or 'False'). 
            All values aren't boolean value. When we want to change the state of 
            QCheckBox()-object, we have to convert all corresponding strings before.

            PARAMETERS:
            ===========
            :return     -   In the try block its returns the converted value. But, when 
                            the operation fails its returns False. In this case its
                            possible that the user or a other software has manipulated 
                            the contents of the ini file. 
        '''

        try:
            '''
                Let us try to invoke the function by passing an argument. In this case, we access
                to a dictionary at first, because the loaded settings are saved in the dictionary.
                We use the key (its the argument named value) to get the value, that is saved behind
                the key. With the obtained value we try to convert it in the bool-
            '''
            logger.info("Try to invoke a parsing_str_to_bool()-function by passing an {}-argument".format(value))

            return parsing_str_to_bool(value)
        except ValueError as VaErr:
            '''
                Ups, an error has occurred. Maybe the user or other program has changed 
                the content of this ini file. We write this error in the log file and
                return False. That means, the current QCheckBox()-object isn't check. The
                user will see an uncheck QCheckBox()-object.
            '''
            desired_trace = traceback.format_exc(sys.exc_info())
            logger.error(desired_trace)

            return False

    def get_current_index(self, widget=None):
        '''
            NOTICE:
            =======
            This method is used to hold the index position of the widget that is visible.

            PARAMETERS:
            ===========
            :widget     -       We need the widget to determine the current index.

            :return     -       Its returns the current index of the given widget.
        '''
        return widget.currentIndex()

    def clear_tree_view_selection(self, tree_view = None):
        tree_view.selectionModel().clearSelection()

    def fetch_all(self, index=None,
                                    list_header=None,
                                    tuples_list_widget_boolean=None,
                                    line_edit_object_get_focus=None,
                                    tree_view_object=None,
                                    label_object=None,
                                    model = None,
                                    category=None):

        '''
            NOTICE:
            =======
            In this method we load the entire genre of movies.

            PARAMETERS:
            ===========
            :list_header:                       -

            :tuples_list_widget_boolean    -

            :label_object                       -

            :return                             -       Nothing is returned. The statement 'return'
                                                        terminates a function. That makes sure that the
                                                        function is definitely finished.
        '''
        logger.info("Start the thread.")

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        if not index is None:
            stackwidget_current_idx = int(index)

        self.standard_item_model(category = category, rows = 0, columns = len(list_header), parent = self)

        self.set_header_data(model = model, list_header_data=list_header)

        self.set_enabled_widget(two_pair_tuples_in_list_widget_boolean=tuples_list_widget_boolean)

        new_tuples_list_widget_boolean = list()

        for push_button, value in tuples_list_widget_boolean:

            new_tuples_list_widgets_boolean = self.create_new_tuples_list(push_button = push_button, boolean_value = True)

            new_tuples_list_widget_boolean.append(new_tuples_list_widgets_boolean)


        self.set_text_count_label(label_object = label_object, count = "")

        task_thread = QThread(self)

        task_thread.work = LoadDataItem(category = category,
                                        timer_interval = 2)

        self._list_threads.append(task_thread)

        task_thread.work.moveToThread(task_thread)

        task_thread.work.quit_thread_signal.connect(task_thread.quit)

        task_thread.work.set_proxy_later_signal.connect(self.set_proxy_later)
        
        task_thread.work.populate_item_signal.connect(lambda tuple_items: self.populate_data_item(tuple_items = tuple_items, model = model))

        task_thread.work.count_row_signal.connect(lambda count: self.set_text_count_label(label_object = label_object, count = count))

        task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)

        task_thread.work.set_hide_column_signal.connect(lambda:
            self.set_hide_column(list_tree_view_object = self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTreeView)), 
                                 column = 0, 
                                 bool_value = True))

        self.sotp_working_thread_signal.connect(task_thread.work.stop_task_thread)

        self.interrupt_working_thread_signal.connect(task_thread.work.abort_task_thread)

        task_thread.work.close_window_signal.connect(self.close)

        task_thread.work.set_enabled_widget_signal.connect(lambda: self.set_enabled_widget(new_tuples_list_widget_boolean))

        task_thread.work.set_focus_signal.connect(lambda: self.set_focus(widget_object=line_edit_object_get_focus))

        task_thread.started.connect(task_thread.work.init_object)

        task_thread.finished.connect(task_thread.deleteLater)

        task_thread.start()
        
        return

    def read_only(self, widget_object):

        pass

    def delete_selected_id(self):
        self.current_selected_id = None
        self.selected_be_edited_id = None

    def add_record(self, 
                   line_edit_obj_film_genre=None,
                   line_edit_obj_general_country=None,
                   line_edit_obj_general_language=None,
                   line_edit_obj_general_region=None,
                   line_edit_obj_general_award=None,
                   line_edit_obj_person_gender=None,
                   line_edit_obj_person_nationality=None,
                   line_edit_obj_person_salutation=None,
                   line_edit_obj_person_title=None,
                   line_edit_obj_person_hair_color=None,
                   line_edit_obj_person_eye_color=None,
                   line_edit_obj_person_religion=None,
                   line_edit_obj_person_relationship_status=None,
                   line_edit_obj_person_relationship=None,
                   counter_label_object=None,
                   label_object=None,
                   tuples_list_widget_boolean=None,
                   tree_view_object=None,
                   line_edit_object_get_focus=None,
                   list_header=None,
                   data_query=None,
                   model = None,
                   category=None):
        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :line_edit_obj      -       We except a QLineEdit()-object for getting
                                        the content, that we want to save in the database.

            :return             -       Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        dict_store_add_records = {'film_genre':                 lambda: task_thread.work.add_record(film_genre=unicode(line_edit_obj_film_genre.text()),
                                                                        category = category, work_area = "master_data"),
                                  'general_country':            lambda: task_thread.work.add_record(general_country=unicode(line_edit_obj_general_country.text()),
                                                                        category=category, work_area = "master_data"),
                                  'general_language':           lambda: task_thread.work.add_record(general_language=unicode(line_edit_obj_general_language.text()),
                                                                        category=category, work_area = "master_data"),
                                  'general_region':             lambda: task_thread.work.add_record(general_region=unicode(line_edit_obj_general_region.text()),
                                                                        category=category, work_area = "master_data"),
                                  'general_award':              lambda: task_thread.work.add_record(general_award=unicode(line_edit_obj_general_award.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_gender':              lambda: task_thread.work.add_record(person_gender=unicode(line_edit_obj_person_gender.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_nationality':         lambda: task_thread.work.add_record(person_nationality=unicode(line_edit_obj_person_nationality.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_salutation':          lambda: task_thread.work.add_record(person_salutation=unicode(line_edit_obj_person_salutation.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_title':               lambda: task_thread.work.add_record(person_title=unicode(line_edit_obj_person_title.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_hair_color':          lambda: task_thread.work.add_record(person_hair_color=unicode(line_edit_obj_person_hair_color.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_eye_color':           lambda: task_thread.work.add_record(person_eye_color=unicode(line_edit_obj_person_eye_color.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_religion':            lambda: task_thread.work.add_record(person_religion=unicode(line_edit_obj_person_religion.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_relationship_status': lambda: task_thread.work.add_record(person_relationship_status=unicode(line_edit_obj_person_relationship_status.text()),
                                                                        category=category, work_area = "master_data"),
                                  'person_relationship':        lambda: task_thread.work.add_record(person_relationship=unicode(line_edit_obj_person_relationship.text()),
                                                                        category=category, work_area = "master_data"),
                                }

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        task_thread = QThread(self)

        task_thread.work = LoadDataItem()

        task_thread.work.moveToThread(task_thread)

        task_thread.work.select_all_signal.connect(lambda: 
            self.fetch_all(tuples_list_widget_boolean=tuples_list_widget_boolean,
                           line_edit_object_get_focus=line_edit_object_get_focus,
                           tree_view_object=tree_view_object,
                           list_header=list_header, 
                           label_object=label_object,
                           model = model,
                           category=category))


        task_thread.work.clear_widget_object_content_signal.connect(lambda: 
            self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit))))

        task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)

        task_thread.work.delete_selected_id_signal.connect(self.delete_selected_id)

        task_thread.started.connect(dict_store_add_records[category])

        task_thread.finished.connect(task_thread.deleteLater)

        task_thread.start()

        return

    def edit_record(self, 
                   index=None, 
                   stackWidget_obj=None,
                   line_edit_object_get_focus = None,
                   line_edit_obj_film_genre=None,
                   line_edit_obj_general_country=None,
                   line_edit_obj_general_language=None,
                   line_edit_obj_general_region=None,
                   line_edit_obj_general_award=None,
                   line_edit_obj_person_gender=None,
                   line_edit_obj_person_nationality=None,
                   line_edit_obj_person_salutation=None,
                   line_edit_obj_person_title=None,
                   line_edit_obj_person_hair_color=None,
                   line_edit_obj_person_eye_color=None,
                   line_edit_obj_person_religion=None,
                   line_edit_obj_person_relationship_status=None,
                   line_edit_obj_person_relationship=None,
                   counter_label_object=None,
                   label_object=None,
                   tree_view_object=None,
                   list_header=None,
                   tuples_list_widget_boolean=None,
                   query_data=None,
                   model = None,
                   category=None):

        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :index              -   We need an index as a type of integer to
                                    change the page of the given stackWidget()-object later.

            :StackWidget_obj    -   We except a stackWidget()-object.    

            :line_edit_obj      -   We except a QLineEdit()-object for getting
                                    the content, that we want to save in the database.

            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''
        dict_store_add_records = {'film_genre':                 lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             film_genre=unicode(line_edit_obj_film_genre.text()),
                                                                                             category=category),
                                  'general_country':            lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             general_country=unicode(line_edit_obj_general_country.text()),
                                                                                             category=category),
                                  'general_language':           lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             general_language=unicode(line_edit_obj_general_language.text()),
                                                                                             category=category),
                                  'general_region':             lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             general_region=unicode(line_edit_obj_general_region.text()),
                                                                                             category=category),
                                  'general_award':              lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             general_award=unicode(line_edit_obj_general_award.text()),
                                                                                             category=category),
                                  'person_gender':              lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_gender=unicode(line_edit_obj_person_gender.text()),
                                                                                             category=category),
                                  'person_nationality':         lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_nationality=unicode(line_edit_obj_person_nationality.text()),
                                                                                             category=category),
                                  'person_salutation':          lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_salutation=unicode(line_edit_obj_person_salutation.text()),
                                                                                             category=category),
                                  'person_title':               lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_title=unicode(line_edit_obj_person_title.text()),
                                                                                             category=category),
                                  'person_hair_color':          lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_hair_color=unicode(line_edit_obj_person_hair_color.text()),
                                                                                             category=category),
                                  'person_eye_color':           lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_eye_color=unicode(line_edit_obj_person_eye_color.text()),
                                                                                             category=category),
                                  'person_religion':            lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_religion=unicode(line_edit_obj_person_religion.text()),
                                                                                             category=category),
                                  'person_relationship_status': lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_relationship_status=unicode(line_edit_obj_person_relationship_status.text()),
                                                                                             category=category),
                                  'person_relationship':        lambda: task_thread.work.edit_record(id=unicode(self.selected_be_edited_id),
                                                                                             person_relationship=unicode(line_edit_obj_person_relationship.text()),
                                                                                             category=category),
                                }

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        task_thread = QThread(self)

        task_thread.work = LoadDataItem()

        task_thread.work.moveToThread(task_thread)

        task_thread.work.select_all_signal.connect(lambda: 
            self.fetch_all(tuples_list_widget_boolean=tuples_list_widget_boolean,
                                        line_edit_object_get_focus=line_edit_object_get_focus,
                                        tree_view_object=tree_view_object,
                                        list_header=list_header, 
                                        label_object=label_object,
                                        model = model,
                                        category=category))


        task_thread.work.clear_widget_object_content_signal.connect(lambda: 
            self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QLineEdit, QPlainTextEdit))))

        task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)
        task_thread.work.remove_current_selected_id_signal.connect(self.remove_current_selected_id)
        task_thread.work.remove_selected_be_edited_id_signal.connect(self.remove_selected_be_edited_id)

        try:

            int(self.selected_be_edited_id)

            task_thread.started.connect(dict_store_add_records[category])

            task_thread.finished.connect(task_thread.deleteLater)

            task_thread.start()

        except (ValueError, TypeError):

            desired_trace = format_exc(exc_info())

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or \
            not desired_trace.find("int() argument must be a string or a number") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                self.create_generally_crritcal_msg(title_text, conn_err, "")



        self.stack_set_current_index(index=index, stack_widget_object=stackWidget_obj, clear_widget=False)


        return

    def delete_record(self, 
                          id=None, 
                          line_edit_obj=None,
                          counter_label_object=None,
                          label_object=None,
                          tree_view_object=None,
                          list_header=None,
                          tuples_list_widget_boolean=None,
                          query_data=None,
                          model = None,
                          category=None):
        '''
            NOTICE:
            =======
            Here in this method we want add a new genre of movies.

            PARAMETERS:
            ===========
            :id                 -   We need an index as an integer. 
                                    With this ID, we want to uniquely identify 
                                    and delete a record.

            :line_edit_obj      -   We except a QLineEdit()-object for getting
                                    the content, that we want to save in the database.

            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.
        '''

        stackwidget_current_idx = self.get_current_index(widget=self.ui_master_data.stackedWidget)

        task_thread = QThread(self)

        task_thread.work = LoadDataItem()

        task_thread.work.moveToThread(task_thread)

        task_thread.work.select_all_signal.connect(lambda: 
            self.fetch_all(tuples_list_widget_boolean=tuples_list_widget_boolean,
                                        tree_view_object=tree_view_object,
                                        list_header=list_header,
                                        label_object=label_object,
                                        model = model,
                                        category=category))


        task_thread.work.clear_widget_object_content_signal.connect(lambda: 
            self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit))))

        task_thread.work.query_failed_signal.connect(self.create_generally_crritcal_msg)
        task_thread.work.remove_current_selected_id_signal.connect(self.remove_current_selected_id)
        task_thread.work.remove_selected_be_edited_id_signal.connect(self.remove_selected_be_edited_id)
        
        converted_bool = self.convert_string_to_bool(self._dict_custom_config.dict_set_general_settings["AskBeforDelete"])

        try:

            int(id)

            if converted_bool:

                result_msg_box = self.create_generally_question_msg(title = self.tr('Conformation'),
                                                                    text = self.tr('Are you sure you want to permamently delete this item?'),
                                                                    detail_text = self.tr('NOTICE: By deleting, the selected item will be irrevocably removed!'),
                                                                    icon = self.image_path_collection.question_32x32_default)

            if result_msg_box:

                task_thread.work.delete_record(id=int(id), category=category)

            task_thread.finished.connect(task_thread.deleteLater)

            task_thread.start()

        except (ValueError, TypeError):

            desired_trace = format_exc(exc_info())

            title_text = self.tr('Error Deleting an item')

            if not desired_trace.find("invalid literal for int() with base 10:") == -1 or \
            not desired_trace.find("int() argument must be a string or a number") == -1:
                conn_err = self.tr("We are sorry, but your enter attempt failed! \n"
                                    "\n"
                                    "Unable to delete an item.. You must select an item to delete.")

                self.create_generally_crritcal_msg(title_text, conn_err, "")

        self.clear_widget_object_content(
                list_widget_object=self.ui_master_data.stackedWidget.widget(stackwidget_current_idx).findChildren((QTextEdit, QLineEdit, QPlainTextEdit)))

        self.clear_tree_view_selection(tree_view = tree_view_object)

        self.set_focus(widget_object=line_edit_obj)

        return



    # def keyPressEvent(self, event):
    #     if not event.key() == Qt.Key_Escape:
    #         super(MasterData_Window, self).keyPressEvent(event)

    # A key has been pressed!
    #def keyPressEvent(self, event):
        # Did the user press the Escape key?
        #if event.key() == Qt.Key_Escape: 
            #super(MasterData_Window, self).keyPressEvent(event)
            #print "huhu"
        # QtCore.Qt.Key_Escape is a value that equates to what the operating system passes to python from the keyboard when the escape key is pressed.
            # Yes: Close the window
            #self.close()
        # No:  Do nothing.

    @pyqtSlot()
    def quite_threads(self):
        '''
            Explicitly mark a python method as a Qt slot and specify a C++ signature for it 
            (most commonly in order to select a particular overload). 
            pyqtSlot has a much more pythonic API.
        '''

        self.interrupt_working_thread_signal.emit()

        for thread in self._list_threads:
            try:
                # It will quit **as soon as thread event loop unblocks**
                thread.quit()
                 # We need to wait for it to *actually* quit
                thread.wait()
            except RuntimeError as RunErr:
                if not str(RunErr).find("QThread has been deleted") == -1:
                    pass

    def closeEvent(self, 
                    event):
        '''
            NOTICE:
            =======
            This method closes the form.

            PARAMETERS:
            ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the current form.")

        self.quite_threads()

        if isinstance(self.parent(), QMdiSubWindow):
            '''
                the isinstance()-function caters for inheritance (an instance of a derived class is an instance of a base class, too),
                that means, its returns a Boolean stating whether the object is an instance or subclass of another object.
                Here we check if self.parent()-object is an instance of QMdiSubWindow()-class.
                This is the case when the window is a subwindow in the mdi.
            '''
            self.set_flat_signal.emit('master_data', False)
            self.parent().close()
        else:
            '''
                No, the self.parent()-object is not a instance of the QMdiSubWindow()-class.
                That means, the window is not a subwindow.
            '''
            self.close()

        return

if __name__ == "__main__":


    '''
        We import missing classes of PyQt4
    '''
    from PyQt4.QtCore import QTranslator
    from PyQt4.QtGui import QApplication
    from PyQt4 import QtCore

    from PyQt4.QtCore import QCoreApplication

    #   Import core codes
    from core.manage_path import is_specifically_suffix
    from core.manage_ini_file import get_configuration, DefaultINIFile
    from core.config import ConfigurationSaverDict
    from core.manage_time import get_time_now
    from core.manage_logging import configure_logging
    from collection_paths_image import ImagePathnCollection
    from core.manage_db_connection import SessionScope
   
    #   Import all compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc
    
    print  """NOTICE: \n======= \nYou can also leave the inputs blank - by pressing just enter.
              """

    USER_INI_FILE_PATH = raw_input('Enter ini file path: ')

    dict_custom_config = ConfigurationSaverDict()

    default_ini_file = DefaultINIFile()

    convert_current_time = get_time_now()


    dict_custom_config.dict_set_general_settings["PathToINISetting"] = USER_INI_FILE_PATH

    get_configuration(dict_custom_config.dict_set_general_settings["PathToINISetting"], dict_custom_config, default_ini_file)


    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)



    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    LOG_FOLDER_PATH = path.join(BASE_PATH, LOG_FILE_PATH)

    image_path_collection = ImagePathnCollection()

    print ""
    print "The log file is saved in", LOG_FOLDER_PATH

    dbms = raw_input('Enter database type: ')
    dbdriver = raw_input('Enter database driver: ')
    dbuser = raw_input('Enter user name: ')
    dbuser_pwd = raw_input('Enter user password: ')
    db_server_host = raw_input('Enter server host: ')
    dbport = raw_input('Enter port: ')
    db_name = raw_input('Enter database name: ')
    admin_database = None

    session_scope = SessionScope(dbms=dbms,
                             dbdriver=dbdriver,
                             dbuser=unicode(dbuser),
                             dbuser_pwd=unicode(dbuser_pwd),
                             db_server_host=unicode(db_server_host),
                             dbport=unicode(dbport),
                             db_name=unicode(db_name),
                             admin_database=admin_database)

    try:

        with session_scope as session:

            session_scope.test_connection()

            print "Connection was successful. You are connected."

    except SQLAlchemyError as err:

        server_said = "The server said: {server_said}".format(server_said=str(err[0]))

        print "Can't connect to database server", server_said

    QCoreApplication.addLibraryPath(path.join(path.dirname(QtCore.__file__), "plugins"))
    
    app = QApplication(argv)

    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)   

    window = MasterData_Window(image_path_collection=image_path_collection,
                                stack_widget_page=0,
                                session_scope=session_scope,
                                dict_custom_config=dict_custom_config)


    #window.resize(600, 400)
    window.showMaximized()
    exit(app.exec_())
