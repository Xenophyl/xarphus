#-*- coding:latin1 -*-

"""
tmdbsimple.movies
~~~~~~~~~~~~~~~~~
This module implements the Movies, Collections, Companies, Keywords, and 
Reviews functionality of tmdbsimple.
Created by Celia Oakley on 2013-10-31.
:copyright: (c) 2013-2014 by Celia Oakley
:license: GPLv3, see LICENSE for more details
"""

FILE_NAME = __name__

import os
import sys
from os.path import expanduser

from PyQt4.QtCore import Qt, QFile
from PyQt4.uic import loadUi
from PyQt4.QtGui import QMdiSubWindow, QFileDialog, QPixmap, QIcon, QWidget

from xarphus.frm_pushbuttons_close_print_delete_save import PushbuttonsClosePrintDeleteSave_Window
from xarphus.frm_movie_general_details import MovieGeneralDetails_Window

from xarphus.core import get_news_in_version



BASE_PATH = os.path.dirname(os.path.abspath(__file__))
# TEMP_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
#DOWNLOAD_URL = 'http://sophus.bplaced.net/download/example-app-0.3.win32.zip'

#UI_PATH = os.path.join(BASE_PATH, 'gui', 'new_in_version.ui')
#UI_PATH = QFile(":/ui_file/new_in_version.ui")

class Movie_Window(QWidget):
    def __init__(self, choice_function, func_up_to_date, update_sublist, cover_viewer,
                 save_config, parent):

        QWidget.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/movie_general.ui")



        self.func = func_up_to_date
        self.up_to_date_sublist = update_sublist
        self.cover_viewer = cover_viewer
        self.choice_function = choice_function

        self.set_get_settings = save_config

        UI_PATH.open(QFile.ReadOnly)
        self.ui_movie_general = loadUi(UI_PATH, self)
        UI_PATH.close()


        self.chosen_function()
        self.set_action_pushbutton()

    def set_ui_language(self):
        my_path = ":/img_16x16/img_16x16/Information.png"

    def set_prominent(self):
        pass
    #--------------------------------------------------------------------------------------
    def set_contact(self):
        pass
    #--------------------------------------------------------------------------------------
    def set_only_button(self):
        pass
    #--------------------------------------------------------------------------------------
    def set_movie_details_buttons(self):
        self.pushbutton_close_print_delete_save = PushbuttonsClosePrintDeleteSave_Window(self.func, self.up_to_date_sublist, self)
        #self.movie_general = MovieGeneral_Window(self.func, self.up_to_date_sublist, self)
        self.movie_general_details = MovieGeneralDetails_Window(self.func, self.up_to_date_sublist, self)

        #self.ui_movie_general.verticalLayout_8.addWidget(self.movie_general)
        self.ui_movie_general.verticalLayout_12.addWidget(self.movie_general_details)
        self.ui_movie_general.verticalLayout_12.addWidget(self.pushbutton_close_print_delete_save)
        self.ui_movie_general.setWindowTitle(self.tr("Film hinzuf�gen"))
    #--------------------------------------------------------------------------------------
    def create_function_list(self, choice_number):
        list_functions = [self.set_movie_details_buttons,
                          self.set_contact,
                          self.set_prominent
                            ]

        list_functions[choice_number]()
    #--------------------------------------------------------------------------------------
    def chosen_function(self):
        self.create_function_list(self.choice_function)
    #--------------------------------------------------------------------------------------
    def open_file_dialog(self):
        self.file_path_photo = unicode(QFileDialog.getOpenFileName(self, self.tr(" Cover w�hlen "),
                                                                             os.path.expanduser(self.set_get_settings.dict_set_settings["LastPathFolderDialog"]),
                                                                              "*.png *.jpg *.bmp"))

        if unicode(self.file_path_photo) == '':
            self.ui_movie_general.pushButton_zoom_photo.setEnabled(False)
            self.ui_movie_general.pushButton_del_photo.setEnabled(False)
        else:
            self.ui_movie_general.pushButton_zoom_photo.setEnabled(True)
            self.ui_movie_general.pushButton_del_photo.setEnabled(True)
            #self.set_get_settings.dict_set_settings["LastPathFolderDialog"] = unicode(self.file_path_front_cover)
            self.show_photo_cover(self.file_path_photo)
    #--------------------------------------------------------------------------------------
    def combo_currentIndexChanged(self, idx):
        print "currentID", self.comboBox_total_evaluation.currentIndex()
    #--------------------------------------------------------------------------------------
    def set_combobox_total_evaluation(self):

        self.items = (
            '',
            (self.tr("Mangelhaft"), ":/stars/stars/one.png"),
            (self.tr("Ausreichend"), ":/stars/stars/two.png"),
            (self.tr("Befriedigend"), ":/stars/stars/three.png"),
            (self.tr("Gut"), ":/stars/stars/four.png"),
            (self.tr("Sehr gut"), ":/stars/stars/five.png")
        )
        for i in self.items:
            if isinstance(i, tuple):
                text, icon_path = i[0], i[1]
                self.comboBox_total_evaluation.addItem(QIcon(icon_path), text)
            else:
                self.comboBox_total_evaluation.addItem(i)
    #--------------------------------------------------------------------------------------
    def create_cover_viewer(self, cover_path):
        self.cover_viewer(cover_path)
    #--------------------------------------------------------------------------------------
    def show_photo_cover(self, img_path):
        self.ui_movie_general.label_photo.setPixmap(QPixmap(""))
        self.ui_movie_general.label_photo.setPixmap(QPixmap(img_path))
    #--------------------------------------------------------------------------------------
    def del_photo(self):
        self.ui_movie_general.label_photo.setPixmap(QPixmap(""))
        self.file_path_photo = ""
        self.ui_movie_general.pushButton_zoom_photo.setEnabled(False)
        self.ui_movie_general.pushButton_del_photo.setEnabled(False)
        self.ui_movie_general.label_photo.setPixmap(QPixmap(":/img_156x220/img_156x220/default.jpg"))
    #--------------------------------------------------------------------------------------
    def set_action_pushbutton(self):
        self.pushbutton_close_print_delete_save.pushButton_close.clicked.connect(self.close_form)
        self.ui_movie_general.pushButton_add_photo.clicked.connect(self.open_file_dialog)
        self.ui_movie_general.pushButton_close.clicked.connect(self.close_form)
        self.ui_movie_general.pushButton_del_photo.clicked.connect(self.del_photo)
        self.ui_movie_general.pushButton_zoom_photo.clicked.connect(lambda: self.create_cover_viewer(self.file_path_photo))
    #--------------------------------------------------------------------------------------
    def set_action_combox(self):
        self.comboBox_total_evaluation.currentIndexChanged.connect(self.combo_currentIndexChanged)
    #--------------------------------------------------------------------------------------
    def set_gui(self):
        pass
    #--------------------------------------------------------------------------------------
    def list_all_groupbox(self):
        list_groupbox = (self.ui_movie_general.groupBox_cover,
                         self.ui_movie_general.groupBox_general_info,
                         self.ui_movie_general.groupBox_film_cast,
                         self.ui_movie_general.groupBox_crew,
                         self.ui_movie_general.groupBox_film_cast_sync,
                         self.ui_movie_general.groupBox_voice_actor,
                         self.ui_movie_general.groupBox_film_production_company,
                         self.ui_movie_general.groupBox_filmstudio,
                         self.ui_movie_general.groupBox_review,
                         self.ui_movie_general.groupBox_comment,
                         self.ui_movie_general.groupBox_conent,
                         self.ui_movie_general.groupBox_film_version,
                         self.ui_movie_general.groupBox_film_edition,
                        )

        return list_groupbox
    #--------------------------------------------------------------------------------------
    def list_all_qtextedit(self):
        list_qedittext = (self.ui_movie_general.textEdit_review,
                              self.ui_movie_general.textEdit_comment,
                              self.ui_movie_general.textEdit_content,

                            )
        return list_qedittext
    #--------------------------------------------------------------------------------------
    def list_all_qpushbutton(self):
        list_qpushbutton = (self.ui_movie_general.pushButton_del_back_cover,
                            self.ui_movie_general.pushButton_zoom_back,
                            self.ui_movie_general.pushButton_add_back_cover,
                            self.ui_movie_general.pushButton_add_front_cover,
                            self.ui_movie_general.pushButton_close,
                            self.ui_movie_general.pushButton_del_front_cover,
                            self.ui_movie_general.pushButton_delete_film,
                            self.ui_movie_general.pushButton_print_film,
                            self.ui_movie_general.pushButton_save_film,
                            self.ui_movie_general.pushButton_zoom_front,
                            self.ui_movie_general.pushButton_add_genre,
                            self.ui_movie_general.pushButton_del_genre,
                            self.ui_movie_general.pushButton_add_cast,
                            self.ui_movie_general.pushButton_del_cast,
                            self.ui_movie_general.pushButton_add_crew,
                            self.ui_movie_general.pushButton_del_crew,
                            self.ui_movie_general.pushButton_edit_country_of_production,
                            self.ui_movie_general.pushButton_edit_color_format,
                            self.ui_movie_general.pushButton_add_sync,
                            self.ui_movie_general.pushButton_del_sync,
                            self.ui_movie_general.pushButton_add_production_company,
                            self.ui_movie_general.pushButton_del_production_company,
                            self.ui_movie_general.pushButton_add_film_studio,
                            self.ui_movie_general.pushButton_del_film_studio,
                            self.ui_movie_general.pushButton_add_film_version,
                            self.ui_movie_general.pushButton_del_film_version,
                            self.ui_movie_general.pushButton_add_film_edition,
                            self.ui_movie_general.pushButton_del_film_edition,

                            )
        return list_qpushbutton
    #--------------------------------------------------------------------------------------
    def list_all_qtabWidget(self):
        list_tabWidget = (self.ui_movie_general.tabWidget_details,
                          self.ui_movie_general.tabWidget_cover,
                          self.ui_movie_general.tabWidget_more_detail,

                        )
        return list_tabWidget
    #--------------------------------------------------------------------------------------
    def list_all_qcombobox(self):
        list_qcombobox = (self.ui_movie_general.comboBox_evaluation_genre,
                          self.ui_movie_general.comboBox_total_evaluation,
                          self.ui_movie_general.comboBox_country_of_production,
                          self.ui_movie_general.comboBox_color_format,
                        )

        return list_qcombobox
    #--------------------------------------------------------------------------------------
    def list_all_treewidget(self):
        list_treewidget = (self.ui_movie_general.treeWidget_cast,
                           self.ui_movie_general.treeWidget_crew,
                           self.ui_movie_general.treeWidget_sync_cast,
                           self.ui_movie_general.treeWidget_sync,
                           self.ui_movie_general.treeWidget_production_company,
                           self.ui_movie_general.treeWidget_film_studio,
                           self.ui_movie_general.treeWidget_film_version,
                           self.ui_movie_general.treeWidget_film_edition,
                           self.ui_movie_general.treeWidget_genre,
                            )
        return  list_treewidget
    #--------------------------------------------------------------------------------------
    def list_all_qlineedit(self):
        list_qlineedit = (self.ui_movie_general.lineEdit_translate_title,
                          self.ui_movie_general.lineEdit_original_title,
                          self.ui_movie_general.lineEdit_year_production

                        )

        return  list_qlineedit
    #--------------------------------------------------------------------------------------
    def list_all_qlabel(self):
        list_qlabel = (self.ui_movie_general.label_total_evaluation,
                       self.ui_movie_general.label_evaluation_genre,
                       self.ui_movie_general.label_production_year,
                       self.ui_movie_general.label_original_title,
                       self.ui_movie_general.label_genre,
                       self.ui_movie_general.label_color_format,
                       self.ui_movie_general.label_country_of_production,
                       self.ui_movie_general.label_translate_title
                        )
        return list_qlabel
    #--------------------------------------------------------------------------------------
    def on_lineEdit_changed(self):
        qline_edit_list = self.list_all_qlineedit()

        for element_line_edit in qline_edit_list:
            element_line_edit.textChanged.connect(self.on_text_change)
    #--------------------------------------------------------------------------------------
    def on_textedit_changed(self):
        qtext_edit_list = self.list_all_qtextedit()

        for element_text_edit in qtext_edit_list:
            element_text_edit.textChanged.connect(self.on_text_edit_change)
    #--------------------------------------------------------------------------------------
    def on_text_change(self):
        self.qline_edit_list = self.list_all_qlineedit()

        for self.element_line_edit in self.qline_edit_list:
            if not self.element_line_edit.text() != "":
                self.element_line_edit.setStyleSheet(self.css.set_css_text_field_color_normal)  # textfield is empty
            else:
                self.element_line_edit.setStyleSheet(self.css.set_css_text_field_color_accept)  # textfield is full
    #--------------------------------------------------------------------------------------
    def on_text_edit_change(self):
        self.qtext_edit_list = self.list_all_qtextedit()

        for self.element_text_edit in self.qtext_edit_list:
            if not self.element_text_edit.toPlainText() != "":
                self.element_text_edit.setStyleSheet(self.css.set_css_text_edit_field_color_normal)  # textfield is empty
            else:
                #pass
                self.element_text_edit.setStyleSheet(self.css.set_css_text_edit_field_color_accept)  # textfield is full
    #--------------------------------------------------------------------------------------
    def closeEvent(self, event):
        pass
    #--------------------------------------------------------------------------------------
    def close_form(self):
        if isinstance(self.parent(), QMdiSubWindow):
            self.parent().close()
            self.up_to_date_sublist()
            self.func()
