#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
The modules of required libraries are imported.
'''
from os import path
import webbrowser

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

'''
The modules for Qt are imported.
PyQt are a set of Python bindings for Qt.
'''
from PyQt4.QtGui import QDialog
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile
from PyQt4 import QtCore

'''
Private modules are imported.
'''
try:
    from xarphus.core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from xarphus.core.qt_info import get_qt_version
    from xarphus.core.manage_browser import open_url_homepage
except ImportError:
    '''
    Private modules are imported.
    '''
    from core.manage_path import is_specifically_suffix
    from core.manage_qt import flag_dialog_titlehint_customizewondowhint
    from core.manage_logging import configure_logging
    from core.config import ConfigurationSaverDict
    from info import info_app
    from core.qt_info import get_version_qt
    from core.manage_time import get_time_now
    from core.qt_info import get_qt_version
    from core.manage_browser import open_url_homepage
    
    ## Import compiled files
    from gui import ui_rc
    from licence import licence_rc
    from images import images_rc
    from localization_files import translate_files_rc

BASE_PATH = path.dirname(path.abspath(__file__))
#UI_PATH = path.join(BASE_PATH, 'gui', 'about.ui')
#UI_PATH = QFile(":/ui_file/about.ui")
#self.path_mdi_form = path.abspath(".")
#self.getPath_about = path.join(path.abspath("."), 'files', "qt_ui", 'pp_about.ui')

class About_Window(QDialog):
    '''
        NOTICE:
        =======
        This class loads and shows interfaces for an about form.

    '''
    def __init__(self,
                 app_info=None,
                 parent=None):
        '''
            PARAMETERS
            ==========
            :app_info                        -  We except a created instance to access the dictionary.

            :parent                         -   This class does support passing a parent argument, because 
                                                we know that we will need the parent argument - its more flexible.
        '''
        QDialog.__init__(self, parent)

        UI_PATH = QFile(":/ui_file/about.ui")

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        '''
            Create all instance variables
        '''
        self.app_info = app_info

        self.load_ui_file(UI_PATH)
        self.init_gui()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -   It returns the name of the current class.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def load_ui_file(self, UI_PATH):
        '''
            NOTICE:
            =======
            For this class object to remain clear, the UI file is loaded in this method.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        logger.info(" Open the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.open(QFile.ReadOnly)

        logger.info("The *.ui-files ({}) is opened successfully ".format(UI_PATH.fileName()))

        logger.info("Load the *.ui-files ({})".format(UI_PATH.fileName()))

        self.ui_about = loadUi(UI_PATH, self)

        logger.info("The *.ui-files ({}) is loaded - ".format(UI_PATH.fileName()))
        logger.info("Close the *.ui-files ({})".format(UI_PATH.fileName()))

        UI_PATH.close()

        logger.info("The *.ui-files ({}) is closed successfully".format(UI_PATH.fileName()))

        return

    def init_gui(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently the title 
            of the window is set and the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Inistialize the GUI")

        self.ui_about.setWindowModality(Qt.ApplicationModal)

        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_about.setWindowFlags(flags)

        self.init_clicked_signal_push_buttons()
        self.create_actions_label()
        self.open_licence_text(file_path=":/gpl_v1/LICENCE_GPL_v1.txt")
        self.set_the_text()

        return

    def set_the_text(self):
        '''
            NOTICE:
            =======
            In this method the gui is inistialized. Currently the title 
            of the window is set and the flag of this window is flagged.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Fill all widgets")

        self.ui_about.label_product_name.setText(self.app_info.dict_info["product_name"] + " (" + self.app_info.dict_info["product_version"] + " - " + self.app_info.dict_info["product_built"] + ")")

        self.ui_about.label_copyright.setText(self.app_info.dict_info["product_copyright"] +
                                                self.app_info.dict_info["product_copyright_characters"] +
                                                " " +
                                                self.app_info.dict_info["product_year_from"] +
                                                " - " +
                                                self.app_info.dict_info["product_year_till"] +
                                                " " +
                                                self.app_info.dict_info["product_by"] +
                                                " " +
                                                self.app_info.dict_info["product_author"])
        
        self.ui_about.label_gui.setText(self.tr("User interface (QT): ") + str(get_qt_version()))

        self.ui_about.textEdit_philosophy.setText(self.app_info.dict_info["product_name"] +
                                                self.tr(" verfolgt einen Grundsatz: Ordnung ist das halbe Leben (zitiert von Heinrich Boell). " +
                                                "Dieses Programm erhebt den Anspruch FAST alles zu verwalten, was man "
                                                "verwalten kann. "
                                                "In dieser Version lassen sich folgende Medien verwalten: ") +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["movie"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["books"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["musics"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["coins"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["contacts"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["notes"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["videogames"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["quotations"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["stamps"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["idioms"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info["persons"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info ["articles"] +
                                                "<br>" +
                                                "- " + self.app_info.dict_manager_info ["miscellany"] +
                                                ""
                                                "<br><br> " +
                                                self.tr("Darueber hinaus versteht sich dieses Programm in erster Linie als reine Verwaltungssoftware. "
                                                "Sie bekommen die komplette Kontrolle ueber die Verwaltung. "
                                                "Das bedeutet, dass Sie die Daten manuell verwalten. "
                                                "Jedoch ist das Pogramm lernfaehig. "
                                                "Stammdaten werden ein einziges Mal in die Datenbank gepflegt. Je umfangreicher "
                                                "die Stammdaten, desto schneller lassen sich die (neuen) Medien und Daten verwalten. "
                                                "Durch die strikte manuelle Pflege behalten Sie den ueberblick."))

        return

    def create_actions_label(self):
        '''
            NOTICE:
            =======
            This method is to used to set the text and 
            and the signal for QLabel()-object.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        homepage_link = self.app_info.dict_info["product_site"]

        link_tag = "Website: <a href={homepage_link}>{homepage_link}</a>".format(homepage_link=homepage_link)

        self.ui_about.label_homepage_url.setText(link_tag)
        '''
            This signal is emitted when the user clicks a link. 
            The URL referred to by the anchor is passed in link.
        '''
        self.ui_about.label_homepage_url.linkActivated.connect(open_url_homepage)


        return

    def init_clicked_signal_push_buttons(self):
        '''
            NOTE:
            ======
            This method initializes the PushButton signals: clicked().
            It makes an action when the QPushButton()-object is clicked.
            When the user clicks on the QPushButton()-object
            the clicked-signal of QPushButton()-object emitters.
            In this case, the signal connects with a method - by passing some arguments.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        logger.info("Initialize all QPushButton()-object with clicked-signal")

        self.ui_about.pushButton_close.clicked.connect(self.close_window)

        return

    def open_licence_text(self, file_path=None):
        '''
        NOTICE:
        =======
            This method is to used to load and open the licence file.

        PARAMETERS:
        ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        logger.info("Load the licence file")

        fd = QtCore.QFile(file_path)
        if fd.open(QtCore.QIODevice.ReadOnly | QtCore.QFile.Text):
            text = QtCore.QTextStream(fd).readAll()
            self.ui_about.textEdit_licence.setText(text)
            fd.close()

        return

    def close_window(self):
        '''
        NOTICE:
        =======
            This method closes the form.

        PARAMETERS:
        ===========
            :return                         -       Nothing is returned. The statement 'return'
                                                    terminates a function. That makes sure that the
                                                    function is definitely finished.
        '''
        logger.info("Close the window")

        self.close()

        return
      
if __name__ == "__main__":
    import sys
    from PyQt4.QtGui import QApplication
    from PyQt4.QtCore import QTranslator


    dict_custom_config = ConfigurationSaverDict()
    program_info = info_app()

    FILE_NAME = "frm_about.py"

    convert_current_time = get_time_now()

    #LOG_FILE_PATH = path.join("D:\\Dan\\Python\\xarphus\\log\\", convert_current_time + '.log')

    LOG_FILE_FOLDER_PATH = raw_input('Enter log folder path: ')

    result_suffix = is_specifically_suffix(LOG_FILE_FOLDER_PATH)

    if not result_suffix == True:
        LOG_FILE_FOLDER_PATH = result_suffix

    else:
        pass

    LOG_FILE_PATH = path.join("{folder_path}{file_name}.log".format(file_name=convert_current_time, folder_path=LOG_FILE_FOLDER_PATH))
    configure_logging(LOG_FILE_PATH)

    
    app = QApplication(sys.argv)
    
    translator = QTranslator()
    path = ":/translators/german_sie.qm"
    translator.load(path)
    app.installTranslator(translator)
    
    window = About_Window(get_version_qt,
                            program_info,
                          None)
    window.show()
    sys.exit(app.exec_())
