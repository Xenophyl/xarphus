#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__

import sys

def to_set_general_settings(config_path, setting_folder_path, zip_file_path, setSetting):
    # Here, the dictionaries are changed accordingly
    m1 = setSetting.dict_set_general_settings["PathToINISetting"] = config_path
    m2 = setSetting.dict_set_general_settings["PathToSettingFolder"] = setting_folder_path
    m3 = setSetting.dict_set_general_settings["PathToZipFile"] = zip_file_path

    return m1, m2, m3


if __name__ == '__main__':
    import os
    from config import ConfigurationSaver

    BASE_PATH = os.path.dirname(os.path.abspath(FILE_NAME))
    SETTING_FOLDER_PATH = os.path.join(BASE_PATH, 'settings')
    CONFIG_PATH = os.path.join(BASE_PATH, 'settings', 'config.ini')
    ZIP_FILE_PATH = os.path.join(BASE_PATH, 'temppor', 'example-app-0.3.win32.zip')
    LINCENCE_PATH = os.path.join(BASE_PATH, 'LICENCE_GPL_v2.txt')
    LOG_FOLDER_PATH = os.path.join(BASE_PATH, 'log')

    save_configuration = ConfigurationSaver()

    m1, m2, m4, m5 = to_set_general_settings(CONFIG_PATH, SETTING_FOLDER_PATH, ZIP_FILE_PATH,
                                         LINCENCE_PATH, save_configuration)

    print "m1", m1
    print "m2", m2
    print "m4", m4
    print "m5", m5
