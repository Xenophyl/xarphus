def analyze_categorie(list_name = None):
    '''
        NOTICE:
        =======
        TIn this function certain categories are stored.

        PARAMETERS:
        ===========
        :list_name  -   We need the name of the list which should be returned.

        :return     -   Return the requested list.
    '''
    if list_name == "simple_list":

        return ['film_studio',
                'film_distribution',
                'film_rentals',
                'film_production_company',
                'general_packaging',
                'general_award',
                'general_media_type',
                'general_manufacturer',
                'general_region',
                'film_genre',
                'music_genre',
                'general_country',
                'general_language',
                'person_nationality',
                'person_relationship',
                'film_edition',
                'film_version',
                'film_videonorm',
                'film_audioformat',
                'film_audio_channel',
                'film_colour_format',
                'film_videoformat',
                'film_soundsystem',
                'film_dimension']

    if list_name == "list_two_pair_tuple":

        return ['film_resolution',
                'film_regional_code',
                'film_aspect_ratio']