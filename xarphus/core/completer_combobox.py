# -*- coding: cp1252 -*-
from sys import exit, argv

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from PyQt4.QtCore import Qt, QVariant, SIGNAL, QEvent, QTimer
from PyQt4.QtGui import QApplication, QStandardItemModel, QStandardItem, QTreeView, QComboBox, QDialog, \
                        QVBoxLayout, QPushButton, QAbstractItemView, QCompleter, QSortFilterProxyModel, \
                        QKeyEvent, QComboBox

class CompleterComboBox(QComboBox):
    '''
        SUMMARY
        =======
        Class definition for the custom and extended QComboBox that can add auto-completion
        and show additional information.

        EXTENTED DISCRIPTION
        ====================
        For the autocompleter, the QTreeView () classes are used. The first QTreeView()
        is for the QComboBox. It takes the QTreeView as a ownership of the view. That
        means, QComboBox() sets the view to be used in popup to the given itemView.
        The second is for the QCompleter() to set custom popup view. Because its allows
        you to provide a custom widget for showing/displaying completion/results hints.
    '''
    def __init__(self,
                 app,
                 standard_item_model,
                 sort_filter_proxy_model,
                 parent = None):
        '''
        SUMMARY
        =======
        Initialization for CompleteComboBox class.
        '''
        QComboBox.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        #   First we have to create an instance variables.
        self.app = app
        self._standard_item_model = standard_item_model #QStandardItemModel(0,2)
        self._sort_filter_proxy_model = sort_filter_proxy_model #QSortFilterProxyModel()

        self._sort_filter_proxy_model_saved = sort_filter_proxy_model #QSortFilterProxyModel()

        # Second, we need our two QTreeView() and the settings
        self.init_tree_view()

        self.init_timer(model = self._standard_item_model)

        self.setInsertPolicy(QComboBox.NoInsert) #The string will not be inserted into the combobox.

        self.init_complete(model = self._standard_item_model)

        self.editTextChanged.connect(lambda text: self.handle_changed_text(text = text))

        #self.textChanged.connect(lambda text: self.handle_changed_text(text = text))

    def set_it(self, model):
        print "set_it is started"
        #self._sort_filter_proxy_model.clear()
        self._sort_filter_proxy_model = self._sort_filter_proxy_model_saved


    def handle_changed_text(self, text):
        '''
           SUMMARY
           =======
           This method is used to emit when the text in the combobox's line edit widget is changed.
           The new text is specified by text.

           RETURNS
           =======

           :return:               When the user already exists, its returns False,
                                  otherwise True.
        '''
        logger.info("Return the name of the current class")

        #print "text", text

        self.timer.stop()
        self.timer.start()

        return

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======

           :return:               When the user already exists, its returns False,
                                  otherwise True.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def init_timer(self, model, set_single_shot = True, set_intervall = 800):

        self.timer = QTimer()

        self.timer.setSingleShot(set_single_shot)
        self.timer.setInterval(set_intervall)
        self.timer.timeout.connect(lambda: self.set_it(model = model))

    def init_tree_view(self):
        '''
           SUMMARY
           =======
           Here we initialize the QTreeView()-object.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QTreeView()")

        self.treeview_set_view = QTreeView()
        self.treeview_set_view.setRootIsDecorated(False)
        self.treeview_set_view.setWordWrap(True)
        self.treeview_set_view.setAlternatingRowColors(True)
        self.treeview_set_view.setSelectionMode(QTreeView.ExtendedSelection)
        self.treeview_set_view.header().hide()

        self.treeview_set_popup = QTreeView()
        self.treeview_set_popup.setRootIsDecorated(False)
        self.treeview_set_popup.setWordWrap(True)
        self.treeview_set_popup.setAlternatingRowColors(True)
        self.treeview_set_popup.setSelectionMode(QTreeView.ExtendedSelection)
        self.treeview_set_popup.header().hide()

    def init_complete(self, model=None):

        print "init_complete is started"
        '''
           SUMMARY
           =======
           Here we initialize the QCompleter()-object.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Initialize QCompleter()")

        # add and set a filter model to filter matching items
        self._sort_filter_proxy_model.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self._sort_filter_proxy_model.setSourceModel(model)
        self._sort_filter_proxy_model.setFilterKeyColumn(1)

        # add a completer, which uses the filter model
        self.completer = QCompleter(self)

        # Set the model that the QCompleter uses
        # on model column change, update the model
        # column of the filter and completer as well
        self.completer.setModel(self._sort_filter_proxy_model)
        self.completer.setCompletionColumn(1)

        # always show all (filtered) completions
        self.completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        self.completer.setModelSorting(QCompleter.CaseInsensitivelySortedModel)
        self.completer.setWrapAround(False)

        self.completer.setPopup(self.treeview_set_popup)

        self.treeview_set_popup.setColumnHidden(0, True)

        self.setEditable(True)
        self.setCompleter(self.completer)

        # on model column change, update the model
        # column of the filter and completer as well
        self.setModel(model)
        self.setView(self.treeview_set_view)

        # on model column change, update the model column of
        # the filter and completer as well
        self.setModelColumn(1)

        self.treeview_set_view.resizeColumnToContents(0)

        self.treeview_set_view.setColumnHidden(0, True)

        if self.isEditable():
            pass#self.app.connect(self.lineEdit(), SIGNAL('textEdited(QString)'), self._sort_filter_proxy_model.setFilterFixedString)

        return

def populate_data_item(items_tuple, model):

    count_items = len(items_tuple)

    if count_items == 2:

        item_first, item_second = items_tuple

    two_columns_item = [QStandardItem(item_second), QStandardItem(str(item_first))]

    model.appendRow(two_columns_item)

    return model

def populate(model, list_items):

    returned_model = None

    for single_tuple in list_items:
        returned_model = populate_data_item(items_tuple = single_tuple, model = model)

        print "returned_model", returned_model

    print ""
    print "returned_model", returned_model

    return returned_model

def main():
    qstandard_item_model = QStandardItemModel(0,2)
    sort_filter_proxy_model = QSortFilterProxyModel()

    food_list = [
                ("0", 'Cookie dough'),
                ("5", "hackfleisch"),
                ("1", 'Hummus'),
                ("2", 'Spaghetti'),
                ("3", 'Dal makhani'),
                ("6", 'Blolonese'),
                ("4", 'Hachfleisch'),
                ("3", 'Nudeln'),
                ("666", 'Fl�sch'),
                ("4", 'Chocolate whipped cream')
                ]

    result_populated_qstandard_item_model = populate(model = qstandard_item_model,
                                                     list_items = food_list)

    app = QApplication(argv)
    window = CompleterComboBox(app = app,
                               standard_item_model = result_populated_qstandard_item_model,
                               sort_filter_proxy_model = sort_filter_proxy_model)

    window.resize(300, 50)
    window.show()
    try:
        sip.setdestroyonexit(False)
    except:
        # missing in older versions
        pass
    exit(app.exec_())

if __name__ == "__main__":
    main()
