#!/usr/bin/env python
#-*- coding:utf-8 -*-

import requests

def get_version_news(url_version, time_out):
    return requests.get(url_version, timeout=time_out)
