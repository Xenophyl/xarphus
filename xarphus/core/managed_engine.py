#!/usr/bin/env python
#-*- coding:utf-8 -*-

from sqlalchemy import create_engine
from sqlalchemy.pool import StaticPool
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import inspect
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from os import makedirs, path

#   setting up root class for declarative declaration,
#   that means we create a base class for our classes definitions.
Base = declarative_base()

# Important note: It is important that this import of models (manage_table_model)
# after initializing the declarative_base object. Don't import before!
try:
    from xarphus.core.manage_table_model import *

except ImportError:
    from manage_table_model import *

BASE_PATH = path.dirname(path.abspath(__file__))

class ManagedEngine(object):
    def __init__(self,
                 dbms = None,
                 dbuser = None,
                 dbuser_pwd = None,
                 db_server_host = None,
                 dbport = None,
                 db_name = None,
                 echo_verbose = True,
                 configuration_saver_dict = None,
                 admin_database = None):

        self.dbms = dbms
        self.dbuser = dbuser
        self.dbuser_pwd = dbuser_pwd
        self.db_server_host = db_server_host
        self.dbport = dbport
        self.db_name = db_name
        self.echo_verbose = echo_verbose
        self.admin_database = admin_database
        self._configuration_saver_dict = configuration_saver_dict

        self.me = None

        self.dburi = None#'{}+{}://{}:{}@{}:{}/{}'.format(
            #self.dbms, self.dbdriver, self.dbuser, self.dbuser_pwd, self.db_server_host, self.dbport, self.db_name)

        self._engine = self.build_engine()
        #self.build_engine(dburi=url, echo_verbose=self.echo_verbose)

        #self._session_factory = self.create_session_maker(engine = self._engine)

        #self._scoped_session = self.create_scope_session()

        self.check_adminsitrate_database()

    def create_tables(self, db_name=None, connection_obj=None):
        '''
            Build tables upon first startup.
            That means, its creates the table and tells it to create it in the
            database engine that is passed. So we create all the tables we need.
        '''
        if connection_obj is None:
            '''
                The engine is used directly to issue SQL to the database.
                First we procure a connection resource, which we get it via the Engine.connect() method.
                The connection resource is necessary, on this way we connect to the database directly.
                In this case its important that we pass the obtained connection resource to the function
                named create_all().
            '''
            conn_obj = self._engine.connect()
            conn_obj.execute('COMMIT')
            conn_obj.execute("USE {db_name}".format(db_name=db_name)) # select new db

            #   generate database schema
            Base.metadata.create_all(conn_obj)

            conn_obj.close()
        else:
            #   generate database schema
            Base.metadata.create_all(connection_obj)

    def create_data_base(self, db_name=None):
        conn_obj = self._engine.connect()

        if 'mysql' == self.dbms:
            conn_obj.execute('COMMIT')
            conn_obj.execute("CREATE DATABASE {db_name}".format(db_name=db_name)) #create db
            conn_obj.execute("USE {db_name}".format(db_name=db_name)) # select new db

        elif 'mssql' == self.dbms:
            pass

        elif 'oracle' == self.dbms:
            pass

        elif 'postgresql' == self.dbms:
            pass

        # Database is created successfully, build tables upon first startup
        self.create_tables(connection_obj=conn_obj)

        conn_obj.close()

        return

    def user_info(self, engine=None, connection=None):

        if 'mysql' == self.dbms:
            try:

                #conn_obj.execute("SHOW GRANTS FOR '{db_name}'@'localhost';".format(db_name='sophus')) #create db

               #connection.execute('SHOW GRANTS FOR CURRENT_USER;') #create db
               for i in connection.execute("SHOW GRANTS FOR '{db_name}'@'{localhost}';".format(db_name='nando', localhost='localhost')):
                    print "i", i

                    if not unicode(i).find('GRANT ALL PRIVILEGES ON') == -1:
                        if not unicode(i).find('WITH GRANT OPTION') == -1:
                            print "i is an admin"
                    connection.commit()

                #SHOW GRANTS FOR 'root'@'localhost';

            except:
                pass

    def test_connection(self, connection=None):

        if 'mysql' == self.dbms:
            connection.execute('SELECT Version();')
            #self.user_info(connection=connection)
        if 'sqlite' == self.dbms:
            connection.execute('SELECT sqlite_version();')

    def check_adminsitrate_database(self):
        if self.admin_database:
            self.data_base_exists()

    def data_base_exists(self):
        database_exists = None

        if 'mysql' == self.dbms:

            inspector = inspect(self._engine)

            for name_schema in iter(inspector.get_schema_names()):
                if name_schema == "xarphus":
                    database_exists = True

            if database_exists:
                # Database already exists, just create tables.
                self.create_tables(db_name='xarphus')
            else:
                # Database is missing, create missing database and all tables.
                self.create_data_base(db_name='xarphus')

        elif 'sqlite' == self.dbms:
            self.create_data_base()

    def make_folder(self, db_file=None):
        makedirs(os.path.dirname(db_file), exist_ok=True)

    def create_scope_session(self, engine):
        #   Create the scoped_session, now self._Session registry is established
        return scoped_session(sessionmaker(bind = engine, expire_on_commit=False))

    def create_session_maker(self, engine):
        #Create the session factory
        return sessionmaker(bind = engine)

    def build_engine(self):
        '''
            NOTICE:
            =======
            This method is used to create a SQLAlchemy Engine that will interact with a desired database.

            PARAMETERS:
            ===========
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        count_fail_conditional = 0

        print "build_engine", self.dbms

        if 'sqlite' == self.dbms:

            BASE_PATH = self._configuration_saver_dict.dict_set_general_settings.get('BasePath')

            current_profile = self._configuration_saver_dict.dict_set_general_settings.get('current_profile')

            SQLITE_PATH = path.join(BASE_PATH, 'profiles', current_profile, 'database')

            self._configuration_saver_dict.dict_set_general_settings["PathToSQLite"] = SQLITE_PATH


            # isspace() returns True when there are only whitespaces chars
            sqlite_path = self._configuration_saver_dict.dict_set_general_settings["PathToSQLite"]

            if not sqlite_path.isspace():

                SQLITE_PATH = sqlite_path


            engine = create_engine(u'{dbms}+{db_driver}:///{file_path}{sep}{file_name}.{file_extension}'.format(
                dbms = self.dbms,
                db_driver = u'pysqlite',
                file_name= 'xarphus',
                file_path = SQLITE_PATH,
                sep = path.sep,
                file_extension = 'sqlite'),
            encoding = 'utf8',
            connect_args = {'check_same_thread': False},
            poolclass = StaticPool,
            #convert_unicode=True,
            echo = self.echo_verbose)

                #   NOTICE for SQLite in about connect_args with 'check_same_thread':
                #   The main problem is that you can't have multiple connections
                #   to a SQLite database that only exists in memory, because each
                #   connection will create a new empty database. So we
                #   need to create the engine like this to make sure that is
                #   only one instance that can be shared across threads.
                #   Next, a database in a multithreaded scenario, the
                #   same connection object must be shared among threads,
                #   since the database exists only within the scope of that connection.
                #   The StaticPool implementation will maintain a single connection globally,
                #   and the check_same_thread flag can be passed to Pysqlite as False:
                #   StaticPool is scope is needed within multiple threads for this case,
                #   that means StaticPool is a Pool of exactly one connection, used for all requests.

            self.test_connection(connection = engine.connect())

            return engine

        else:
            count_fail_conditional += 1


        if 'mysql' == self.dbms:
            self.dburi = u'{}+{}://{}:{}@{}:{}/{}'.format(self.dbms,
                                                         u'pymysql',
                                                         self.dbuser,
                                                         self.dbuser_pwd,
                                                         self.db_server_host,
                                                         self.dbport,
                                                         self.db_name)
            engine =  create_engine(self.dburi,
                                 pool_size=20,
                                 encoding='utf8',
                                 #convert_unicode=True,
                                 echo=self.echo_verbose)

            self.user_info(engine=engine, connection=engine.connect())

            self.test_connection(connection = engine.connect())

            return engine

        else:
            count_fail_conditional += 1

        if 'mssql' == self.dbms:
            pass
        else:
            count_fail_conditional += 1

        if 'oracle' == self.dbms:
            pass
        else:
            count_fail_conditional += 1

        if 'postgresql' == self.dbms:
            pass
        else: count_fail_conditional += 1

        if count_fail_conditional == 5:
            raise Exception('No such defined DBMS found')

        return

    def __enter__(self):
        return self._engine#, self._scoped_session

    def __exit__(self, exception, exc_value, traceback):
        # Make sure the dbconnection gets closed
        pass#self._engine.dispose()

def main():
    from config import ConfigurationSaverDict

    configuration_saver_dict = ConfigurationSaverDict()

    dbms = "mysql"
    dbuser = "root"
    dbuser_pwd = "danny5658"
    db_server_host = "localhost"
    dbport = 3306
    db_name = "test"
    echo_verbose = True
    admin_database = True

    with ManagedEngine(dbms = dbms,
                       dbuser = dbuser,
                       dbuser_pwd = dbuser_pwd,
                       db_server_host = db_server_host,
                       dbport = dbport,
                       db_name = db_name,
                       echo_verbose = echo_verbose,
                       configuration_saver_dict = configuration_saver_dict,
                       admin_database = admin_database) as engine:

        pass

if __name__ == "__main__":
    main()
