import webbrowser


def open_web_browser(url):
    # open in a new tab, if possible
    new = 2
    # open a public URL, in this case, the webbrowser docs
    webbrowser.open(url, new=new)