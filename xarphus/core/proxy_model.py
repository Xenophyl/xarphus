#!/usr/bin/env python
#-*- coding:utf-8 -*-

from sys import exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QObject, Qt

try:
    from xarphus.core.custom_sort_filter_model import MySortFilterProxyModel

except ImportError:

    from custom_sort_filter_model import MySortFilterProxyModel

class ProxyModel(QObject):


    def __init__(self,
                parent = None):
        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self.proxy_model_film_general_film_genre = None

        self.proxy_model_master_data_general_country = None
        self.temporary_proxy_model_film_general_general_country = None

        self.proxy_model_filter_person_profile = None


        self.proxy_model_master_data_film_soundsystem = None
        self.proxy_model_film_general_general_country = None
        self.proxy_model_film_general_film_colour_format = None
        self.proxy_model_film_general_film_soundsystem = None
        self.proxy_model_film_general_film_audio_channel = None
        self.proxy_model_film_general_film_audioformat = None
        self.proxy_model_film_general_film_videoformat = None
        self.proxy_model_film_general_film_videonorm = None
        self.proxy_model_film_general_film_version = None
        self.proxy_model_film_general_film_edition = None
        self.proxy_model_film_general_film_dimension = None
        self.proxy_model_film_general_film_aspect_ratio = None
        self.proxy_model_film_general_film_regional_code = None
        self.proxy_model_film_general_general_manufacturer = None
        self.proxy_model_film_general_film_resolution = None
        self.proxy_model_film_general_film_studio = None
        self.proxy_model_film_general_film_distribution = None
        self.proxy_model_film_general_film_rentals = None
        self.proxy_model_film_general_general_packaging = None
        self.proxy_model_film_general_general_media_type = None
        self.proxy_model_film_general_general_age_release = None
        self.proxy_model_film_general_person_relationship = None
        self.proxy_model_film_general_person_relationship_status = None
        self.proxy_model_film_general_person_religion = None
        self.proxy_model_film_general_person_hair_color = None
        self.proxy_model_film_general_person_eye_color = None
        self.proxy_model_film_general_person_salutation = None
        self.proxy_model_film_general_person_title = None
        self.proxy_model_film_general_person_gender = None
        self.proxy_model_film_general_music_genre = None
        self.proxy_model_film_general_general_award = None
        self.proxy_model_film_general_person_nationality = None
        self.proxy_model_film_general_general_language = None
        self.proxy_model_film_general_general_region = None
        self.proxy_model_film_general_music_studio = None
        self.proxy_model_film_general_music_spars_code = None
        self.proxy_model_film_general_music_publisher = None
        self.proxy_model_film_general_music_style = None
        self.proxy_model_film_general_music_label = None
        self.proxy_model_film_general_music_distribution = None
        self.proxy_model_film_general_book_genre = None
        self.proxy_model_film_general_book_publisher = None
        self.proxy_model_film_general_book_binding = None
        self.proxy_model_film_general_coin_alloy = None
        self.proxy_model_film_general_coin_currency = None
        self.proxy_model_film_general_coin_type = None
        self.proxy_model_film_general_coin_manufacturing_procedure = None
        self.proxy_model_film_general_coin_degree_preservation = None
        self.proxy_model_film_general_coin_mint = None
        self.proxy_model_film_general_videogame_genre = None
        self.proxy_model_profile_list = None

        self.set_sort_filter_proxy_model()

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def delete_certain_sort_filter_proxy_model(self,
                                                category):

        if category == "general_region":

            self.proxy_model_film_general_general_region = None
            del self.proxy_model_film_general_general_region

            self.set_sort_filter_proxy_model()

        return

    def set_sort_filter_proxy_model(self):
        '''
           SUMMARY
           =======
           Initialize the overwritten proxy model for filtering
           and main model for data.

           EXTENTED DISCRIPTION
           ====================
           The setDynamicSortFilter()-method is set to True on proxy model.
           This property holds whether the proxy model is dynamically sorted
           and filtered whenever the contents of the source model change.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set proxy model for filtering records in model")

        #   Film
        self.proxy_model_master_data_general_country = MySortFilterProxyModel(self)
        self.proxy_model_master_data_general_country.setObjectName("proxy_model_master_data_general_country")
        self.proxy_model_master_data_general_country.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_country = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_country.setObjectName("proxy_model_film_general_general_country")
        self.proxy_model_film_general_general_country.setDynamicSortFilter(True)

        self.temporary_proxy_model_film_general_general_country = MySortFilterProxyModel(self)
        self.temporary_proxy_model_film_general_general_country.setObjectName("temporary_proxy_model_film_general_general_country")
        self.temporary_proxy_model_film_general_general_country.setDynamicSortFilter(True)

        #   Person
        self.proxy_model_filter_person_profile = MySortFilterProxyModel(self)
        self.proxy_model_filter_person_profile.setObjectName("proxy_model_filter_person_profile")
        self.proxy_model_filter_person_profile.setDynamicSortFilter(True)


        self.proxy_model_film_general_film_genre = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_genre.setObjectName("proxy_model_film_general_film_genre")
        self.proxy_model_film_general_film_genre.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_videoformat = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_videoformat.setObjectName("proxy_model_film_general_film_videoformat")
        self.proxy_model_film_general_film_videoformat.setDynamicSortFilter(True)

        self.proxy_model_master_data_film_genre = MySortFilterProxyModel(self)
        self.proxy_model_master_data_film_genre.setObjectName("proxy_model_master_data_film_genre")
        self.proxy_model_master_data_film_genre.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_age_release = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_age_release.setObjectName("proxy_model_film_general_general_age_release")
        self.proxy_model_film_general_general_age_release.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_colour_format = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_colour_format.setObjectName("proxy_model_film_general_film_colour_format")
        self.proxy_model_film_general_film_colour_format.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_soundsystem = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_soundsystem.setObjectName("proxy_model_film_general_film_soundsystem")
        self.proxy_model_film_general_film_soundsystem.setDynamicSortFilter(True)

        self.proxy_model_master_data_film_soundsystem = MySortFilterProxyModel(self)
        self.proxy_model_master_data_film_soundsystem.setObjectName("proxy_model_master_data_film_soundsystem")
        self.proxy_model_master_data_film_soundsystem.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_audio_channel = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_audio_channel.setObjectName("proxy_model_film_general_film_audio_channel")
        self.proxy_model_film_general_film_audio_channel.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_audioformat = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_audioformat.setObjectName("proxy_model_film_general_film_audioformat")
        self.proxy_model_film_general_film_audioformat.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_videonorm = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_videonorm.setObjectName("proxy_model_film_general_film_videonorm")
        self.proxy_model_film_general_film_videonorm.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_version = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_version.setObjectName("proxy_model_film_general_film_version")
        self.proxy_model_film_general_film_version.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_edition = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_edition.setObjectName("proxy_model_film_general_film_edition")
        self.proxy_model_film_general_film_edition.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_dimension = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_dimension.setObjectName("proxy_model_film_general_film_dimension")
        self.proxy_model_film_general_film_dimension.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_aspect_ratio = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_aspect_ratio.setObjectName("proxy_model_film_general_film_aspect_ratio")
        self.proxy_model_film_general_film_aspect_ratio.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_regional_code = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_regional_code.setObjectName("proxy_model_film_general_film_regional_code")
        self.proxy_model_film_general_film_regional_code.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_manufacturer = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_manufacturer.setObjectName("proxy_model_film_general_general_manufacturer")
        self.proxy_model_film_general_general_manufacturer.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_resolution = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_resolution.setObjectName("proxy_model_film_general_film_resolution")
        self.proxy_model_film_general_film_resolution.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_studio = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_studio.setObjectName("proxy_model_film_general_film_studio")
        self.proxy_model_film_general_film_studio.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_distribution = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_distribution.setObjectName("proxy_model_film_general_film_distribution")
        self.proxy_model_film_general_film_distribution.setDynamicSortFilter(True)

        self.proxy_model_film_general_film_rentals = MySortFilterProxyModel(self)
        self.proxy_model_film_general_film_rentals.setObjectName("proxy_model_film_general_film_rentals")
        self.proxy_model_film_general_film_rentals.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_packaging = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_packaging.setObjectName("proxy_model_film_general_general_packaging")
        self.proxy_model_film_general_general_packaging.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_media_type = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_media_type.setObjectName("proxy_model_film_general_general_media_type")
        self.proxy_model_film_general_general_media_type.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_relationship = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_relationship.setObjectName("proxy_model_film_general_general_media_type")
        self.proxy_model_film_general_person_relationship.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_relationship_status = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_relationship_status.setObjectName("proxy_model_film_general_person_relationship_status")
        self.proxy_model_film_general_person_relationship_status.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_religion = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_religion.setObjectName("proxy_model_film_general_person_religion")
        self.proxy_model_film_general_person_religion.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_hair_color = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_hair_color.setObjectName("proxy_model_film_general_person_hair_color")
        self.proxy_model_film_general_person_hair_color.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_eye_color = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_eye_color.setObjectName("proxy_model_film_general_person_eye_color")
        self.proxy_model_film_general_person_eye_color.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_salutation = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_salutation.setObjectName("proxy_model_film_general_person_salutation")
        self.proxy_model_film_general_person_salutation.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_title = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_title.setObjectName("proxy_model_film_general_person_title")
        self.proxy_model_film_general_person_title.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_gender = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_gender.setObjectName("proxy_model_film_general_person_gender")
        self.proxy_model_film_general_person_gender.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_genre = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_genre.setObjectName("proxy_model_film_general_music_genre")
        self.proxy_model_film_general_music_genre.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_award = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_award.setObjectName("proxy_model_film_general_general_award")
        self.proxy_model_film_general_general_award.setDynamicSortFilter(True)

        self.proxy_model_film_general_person_nationality = MySortFilterProxyModel(self)
        self.proxy_model_film_general_person_nationality.setObjectName("proxy_model_film_general_person_nationality")
        self.proxy_model_film_general_person_nationality.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_language = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_language.setObjectName("proxy_model_film_general_general_language")
        self.proxy_model_film_general_general_language.setDynamicSortFilter(True)

        self.proxy_model_film_general_general_region = MySortFilterProxyModel()
        self.proxy_model_film_general_general_region.setObjectName("proxy_model_film_general_general_region")
        self.proxy_model_film_general_general_region.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_studio = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_studio.setObjectName("proxy_model_film_general_music_studio")
        self.proxy_model_film_general_music_studio.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_spars_code = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_spars_code.setObjectName("proxy_model_film_general_music_spars_code")
        self.proxy_model_film_general_music_spars_code.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_publisher = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_publisher.setObjectName("proxy_model_film_general_music_publisher")
        self.proxy_model_film_general_music_publisher.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_style = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_style.setObjectName("proxy_model_film_general_music_style")
        self.proxy_model_film_general_music_style.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_label = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_label.setObjectName("proxy_model_film_general_music_label")
        self.proxy_model_film_general_music_label.setDynamicSortFilter(True)

        self.proxy_model_film_general_music_distribution = MySortFilterProxyModel(self)
        self.proxy_model_film_general_music_distribution.setObjectName("proxy_model_film_general_music_distribution")
        self.proxy_model_film_general_music_distribution.setDynamicSortFilter(True)

        self.proxy_model_film_general_book_genre = MySortFilterProxyModel(self)
        self.proxy_model_film_general_book_genre.setObjectName("proxy_model_film_general_book_genre")
        self.proxy_model_film_general_book_genre.setDynamicSortFilter(True)

        self.proxy_model_film_general_book_publisher = MySortFilterProxyModel(self)
        self.proxy_model_film_general_book_publisher.setObjectName("proxy_model_film_general_book_publisher")
        self.proxy_model_film_general_book_publisher.setDynamicSortFilter(True)

        self.proxy_model_film_general_book_binding = MySortFilterProxyModel(self)
        self.proxy_model_film_general_book_binding.setObjectName("proxy_model_film_general_book_binding")
        self.proxy_model_film_general_book_binding.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_alloy = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_alloy.setObjectName("proxy_model_film_general_coin_alloy")
        self.proxy_model_film_general_coin_alloy.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_currency = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_currency.setObjectName("proxy_model_film_general_coin_currency")
        self.proxy_model_film_general_coin_currency.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_type = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_type.setObjectName("proxy_model_film_general_coin_type")
        self.proxy_model_film_general_coin_type.setDynamicSortFilter(True)

        self.proxy_model_film_general_videogame_genre = MySortFilterProxyModel(self)
        self.proxy_model_film_general_videogame_genre.setObjectName("proxy_model_film_general_videogame_genre")
        self.proxy_model_film_general_videogame_genre.setDynamicSortFilter(True)

        self.proxy_model_profile_list = MySortFilterProxyModel(self)
        self.proxy_model_profile_list.setObjectName("proxy_model_profile_list")
        self.proxy_model_profile_list.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_manufacturing_procedure = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_manufacturing_procedure.setObjectName("proxy_model_film_general_coin_manufacturing_procedure")
        self.proxy_model_film_general_coin_manufacturing_procedure.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_degree_preservation = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_degree_preservation.setObjectName("proxy_model_film_general_coin_degree_preservation")
        self.proxy_model_film_general_coin_degree_preservation.setDynamicSortFilter(True)

        self.proxy_model_film_general_coin_mint = MySortFilterProxyModel(self)
        self.proxy_model_film_general_coin_mint.setObjectName("proxy_model_film_general_coin_mint")
        self.proxy_model_film_general_coin_mint.setDynamicSortFilter(True)

        return

    def create_temp_sourcE_model(self,
                                ):

        self.blach = MySortFilterProxyModel(self)
        self.blach.setObjectName("blach")
        self.blach.setDynamicSortFilter(True)

    def create_proxy_model(self):
        print ""
        print "ProxyModel create_proxy_model is called"

        self.proxy_model_film_general_general_region = MySortFilterProxyModel(self)
        self.proxy_model_film_general_general_region.setObjectName("proxy_model_film_general_general_region")
        self.proxy_model_film_general_general_region.setDynamicSortFilter(True)

        return


    def reset_proxy_model(self):
        print ""
        print "ProxyModel reset_proxy_model is called"
        #print "ProxyModel in reset_proxy_model / certain_proxy_model.objectName(): \n ->  {}".format(certain_proxy_model.objectName())
        #print "ProxyModel in reset_proxy_model / certain_standard_item_model.objectName(): \n ->  {}".format(certain_standard_item_model.objectName())

        del self.proxy_model_film_general_general_region

        #self.proxy_model_film_general_general_region = None
        #self.proxy_model_film_general_general_region = MySortFilterProxyModel(self)
        #self.proxy_model_film_general_general_region.setObjectName("proxy_model_film_general_general_region")
        #self.proxy_model_film_general_general_region.setDynamicSortFilter(True)

    def create_source_model(self,
                           certain_proxy_model,
                           certain_standard_item_model):
        '''
           SUMMARY
           =======
           Set main model as source for the proxy model
           to be processed by the proxy model.

           PARAMETERS
           ==========
            :model:     We except a model for the proxy model.

            :mode:      Tell us which model should be set? Currently
                        you have two options, "database" or "navigation"

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info('\n\n')
        logger.info("ProxyModel create_source_model is called successfully")
        logger.info("ProxyModel in create_source_model / certain_standard_item_model.objectName(): \n ->  {}".format(certain_standard_item_model.objectName()))
        logger.info('ProxyModel in create_source_model / certain_standard_item_model.rowCount(): --> \n {}'.format(certain_standard_item_model.rowCount()))
        logger.info("ProxyModel in create_source_model / certain_proxy_model.objectName() \n ->  {}".format(certain_proxy_model.objectName()))
        logger.info('\n\n')

        certain_proxy_model.setSourceModel(certain_standard_item_model)

        return

    def set_source_model(self,
                        **kwargs):

        logger.info("category: {}".format(kwargs.get('category')))
        logger.info("certain_proxy_model.objectName(): {}".format(kwargs.get('certain_proxy_model').objectName()))
        logger.info("certain_standard_item_model.objectName(): {}".format(kwargs.get('certain_standard_item_model').objectName()))

        if kwargs.get('category') == 'film_production_company':

            if not kwargs.get('certain_proxy_model') is None:
                #   When model already exists only clear the model for adding new items
                kwargs.get('certain_proxy_model').clear()
            else:
                #   The model doesn't exists, create a new one
                kwargs.get('certain_proxy_model').setSourceModel(kwargs.get('certain_standard_item_model'))

        if kwargs.get('category') == 'film_soundsystem':

            if not kwargs.get('certain_proxy_model') is None:
                #   When model already exists only clear the model for adding new items
                kwargs.get('certain_proxy_model').clear()
            else:
                #   The model doesn't exists, create a new one
                kwargs.get('certain_proxy_model').setSourceModel(kwargs.get('certain_standard_item_model'))

        if kwargs.get('category') == 'film_colour_format':

            if not kwargs.get('certain_proxy_model') is None:
                #   When model already exists only clear the model for adding new items
                kwargs.get('certain_proxy_model').clear()
            else:
                #   The model doesn't exists, create a new one
                kwargs.get('certain_proxy_model').setSourceModel(kwargs.get('certain_standard_item_model'))

        if kwargs.get('category') == 'general_country':

            if not kwargs.get('certain_proxy_model') is None:
                #   When model already exists only clear the model for adding new items
                kwargs.get('certain_proxy_model').clear()
            else:
                #   The model doesn't exists, create a new one
                kwargs.get('certain_proxy_model').setSourceModel(kwargs.get('certain_standard_item_model'))

        return

    def _set_source_model(self,
                         model):
        '''
           SUMMARY
           =======
           Set main model as source for the proxy model
           to be processed by the proxy model.

           PARAMETERS
           ==========
            :model:     We except a model for the proxy model.

            :mode:      Tell us which model should be set? Currently
                        you have two options, "database" or "navigation"

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        print ""
        print "ProxyModel set_source_model is called"
        print "ProxyModel in set_source_model / model.objectName(): \n ->  {}".format(model.objectName())
        logger.info("Set source model for proxy model")

        if not self.proxy_model_film_general_film_genre is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_genre.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_genre.setSourceModel(model)

        if not self.proxy_model_master_data_general_country is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_master_data_general_country.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_master_data_general_country.setSourceModel(model)

        if not self.proxy_model_film_general_general_country is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_country.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_country.setSourceModel(model)

        if not self.proxy_model_film_general_general_age_release is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_age_release.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_age_release.setSourceModel(model)

        if not self.proxy_model_film_general_film_colour_format is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_colour_format.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_colour_format.setSourceModel(model)

        if not self.proxy_model_film_general_film_soundsystem is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_soundsystem.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_soundsystem.setSourceModel(model)

        if not self.proxy_model_film_general_film_videoformat is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_videoformat.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_videoformat.setSourceModel(model)

        if not self.proxy_model_film_general_film_audioformat is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_audioformat.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_audioformat.setSourceModel(model)

        if not self.proxy_model_film_general_film_audio_channel is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_audio_channel.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_audio_channel.setSourceModel(model)

        if not self.proxy_model_film_general_film_videonorm is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_videonorm.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_videonorm.setSourceModel(model)

        if not self.proxy_model_film_general_film_version is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_version.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_version.setSourceModel(model)

        if not self.proxy_model_film_general_film_edition is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_edition.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_edition.setSourceModel(model)

        if not self.proxy_model_film_general_film_dimension is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_dimension.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_dimension.setSourceModel(model)

        if not self.proxy_model_film_general_film_aspect_ratio is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_aspect_ratio.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_aspect_ratio.setSourceModel(model)

        if not self.proxy_model_film_general_film_regional_code is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_regional_code.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_regional_code.setSourceModel(model)

        if not self.proxy_model_film_general_general_manufacturer is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_manufacturer.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_manufacturer.setSourceModel(model)

        if not self.proxy_model_film_general_film_resolution is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_resolution.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_resolution.setSourceModel(model)

        if not self.proxy_model_film_general_film_studio is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_studio.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_studio.setSourceModel(model)

        if not self.proxy_model_film_general_film_distribution is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_distribution.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_distribution.setSourceModel(model)

        if not self.proxy_model_film_general_film_rentals is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_film_rentals.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_film_rentals.setSourceModel(model)

        if not self.temporary_proxy_model_film_general_general_country is None:
            #   When model already exists only clear the model for adding new items
            self.temporary_proxy_model_film_general_general_country.clear()
        else:
            #   The model doesn't exists, create a new one
            self.temporary_proxy_model_film_general_general_country.setSourceModel(model)

        if not self.proxy_model_film_general_general_packaging is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_packaging.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_packaging.setSourceModel(model)

        if not self.proxy_model_film_general_general_media_type is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_media_type.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_media_type.setSourceModel(model)

        if not self.proxy_model_film_general_person_relationship is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_relationship.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_relationship.setSourceModel(model)

        if not self.proxy_model_film_general_person_relationship_status is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_relationship_status.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_relationship_status.setSourceModel(model)

        if not self.proxy_model_film_general_person_religion is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_religion.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_religion.setSourceModel(model)

        if not self.proxy_model_film_general_person_hair_color is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_hair_color.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_hair_color.setSourceModel(model)

        if not self.proxy_model_film_general_person_eye_color is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_eye_color.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_eye_color.setSourceModel(model)

        if not self.proxy_model_film_general_person_salutation is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_salutation.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_salutation.setSourceModel(model)

        if not self.proxy_model_film_general_person_title is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_title.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_title.setSourceModel(model)

        if not self.proxy_model_film_general_person_gender is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_gender.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_gender.setSourceModel(model)

        if not self.proxy_model_film_general_music_genre is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_genre.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_genre.setSourceModel(model)

        if not self.proxy_model_film_general_general_award is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_award.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_award.setSourceModel(model)

        if not self.proxy_model_film_general_person_nationality is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_person_nationality.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_person_nationality.setSourceModel(model)

        if not self.proxy_model_film_general_general_language is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_language.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_language.setSourceModel(model)

        if not self.proxy_model_film_general_general_region is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_general_region.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_general_region.setSourceModel(model)

        if not self.proxy_model_film_general_music_studio is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_studio.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_studio.setSourceModel(model)

        if not self.proxy_model_film_general_music_spars_code is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_spars_code.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_spars_code.setSourceModel(model)

        if not self.proxy_model_film_general_music_publisher is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_publisher.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_publisher.setSourceModel(model)

        if not self.proxy_model_film_general_music_style is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_style.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_style.setSourceModel(model)

        if not self.proxy_model_film_general_music_label is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_label.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_label.setSourceModel(model)

        if not self.proxy_model_film_general_music_distribution is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_music_distribution.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_music_distribution.setSourceModel(model)

        if not self.proxy_model_film_general_book_genre is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_book_genre.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_book_genre.setSourceModel(model)

        if not self.proxy_model_film_general_book_publisher is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_book_publisher.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_book_publisher.setSourceModel(model)

        if not self.proxy_model_film_general_book_binding is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_book_binding.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_book_binding.setSourceModel(model)

        if not self.proxy_model_film_general_coin_alloy is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_alloy.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_alloy.setSourceModel(model)

        if not self.proxy_model_film_general_coin_currency is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_currency.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_currency.setSourceModel(model)

        if not self.proxy_model_film_general_coin_type is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_type.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_type.setSourceModel(model)

        if not self.proxy_model_film_general_videogame_genre is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_videogame_genre.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_videogame_genre.setSourceModel(model)

        if not self.proxy_model_profile_list is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_profile_list.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_profile_list.setSourceModel(model)

        if not self.proxy_model_film_general_coin_manufacturing_procedure is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_manufacturing_procedure.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_manufacturing_procedure.setSourceModel(model)

        if not self.proxy_model_film_general_coin_degree_preservation is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_degree_preservation.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_degree_preservation.setSourceModel(model)

        if not self.proxy_model_film_general_coin_mint is None:
            #   When model already exists only clear the model for adding new items
            self.proxy_model_film_general_coin_mint.clear()
        else:
            #   The model doesn't exists, create a new one
            self.proxy_model_film_general_coin_mint.setSourceModel(model)

        return

    def set_model(self,
                 widget = None,
                 model = None):
        print ""
        print "ProxyModel set_model is called"
        print "ProxyModel set_model in / Widget.objectName(): \n ->  {}".format( widget.objectName())
        print "ProxyModel in set_model / model.objectName(): \n ->  {}".format(model.objectName())
        '''
           SUMMARY
           =======
           Sets the model for the view to present.

           PARAMETERS
           ==========
           :widget (Qt-object):     we except a widget for the view.

           :model (QStandardItemModel):      We required a model to set it to the widget.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Set given model on given Qt-object")

        try:

          pass#widget.setModel(model)

        except AttributeError:

          pass

        return

    def refresh_proxy_model(self,
                            category,
                            certain_proxy_model):
        '''
           SUMMARY
           =======
           This method is used to replace any proxy model.

           PARAMETERS
           ==========
           :certain_proxy_model:    We except a widget for the view.

           :category:               Blah

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Replace the current proxy model with new one")

        if category == "film_genre":

            del self.proxy_model_film_general_film_genre
            self.proxy_model_film_general_film_genre = None
            self.proxy_model_film_general_film_genre = certain_proxy_model

        if category == "general_country":

            del self.proxy_model_master_data_general_country
            self.proxy_model_master_data_general_country = None
            self.proxy_model_master_data_general_country = certain_proxy_model

            del self.proxy_model_film_general_general_country
            self.proxy_model_film_general_general_country = None
            self.proxy_model_film_general_general_country = certain_proxy_model

        if category == "film_colour_format":

            del self.proxy_model_film_general_film_colour_format
            self.proxy_model_film_general_film_colour_format = None
            self.proxy_model_film_general_film_colour_format = certain_proxy_model

        if category == "film_soundsystem":

            del self.proxy_model_film_general_film_soundsystem
            self.proxy_model_film_general_film_soundsystem = None
            self.proxy_model_film_general_film_soundsystem = certain_proxy_model

        if category == 'film_audio_channel':

            del self.proxy_model_film_general_film_audio_channel
            self.proxy_model_film_general_film_audio_channel = None
            self.proxy_model_film_general_film_audio_channel = certain_proxy_model

        if category == 'film_audioformat':

            del self.proxy_model_film_general_film_audioformat
            self.proxy_model_film_general_film_audioformat = None
            self.proxy_model_film_general_film_audioformat = certain_proxy_model

        if category == 'film_videoformat':

            del self.proxy_model_film_general_film_videoformat
            self.proxy_model_film_general_film_videoformat = None
            self.proxy_model_film_general_film_videoformat = certain_proxy_model

        if category == 'film_videonorm':

            del self.proxy_model_film_general_film_videonorm
            self.proxy_model_film_general_film_videonorm = None
            self.proxy_model_film_general_film_videonorm = certain_proxy_model

        if category == 'film_version':

            del self.proxy_model_film_general_film_version
            self.proxy_model_film_general_film_version = None
            self.proxy_model_film_general_film_version = certain_proxy_model

        if category == 'film_edition':

            del self.proxy_model_film_general_film_edition
            self.proxy_model_film_general_film_edition = None
            self.proxy_model_film_general_film_edition = certain_proxy_model

        if category == 'film_dimension':

            del self.proxy_model_film_general_film_dimension
            self.proxy_model_film_general_film_dimension = None
            self.proxy_model_film_general_film_dimension = certain_proxy_model

        if category == 'film_aspect_ratio':

            del self.proxy_model_film_general_film_aspect_ratio
            self.proxy_model_film_general_film_aspect_ratio = None
            self.proxy_model_film_general_film_aspect_ratio = certain_proxy_model

        if category == 'film_regional_code':

            del self.proxy_model_film_general_film_regional_code
            self.proxy_model_film_general_film_regional_code = None
            self.proxy_model_film_general_film_regional_code = certain_proxy_model

        if category == 'general_manufacturer':

            del self.proxy_model_film_general_general_manufacturer
            self.proxy_model_film_general_general_manufacturer = None
            self.proxy_model_film_general_general_manufacturer = certain_proxy_model

        if category == 'general_age_release':

            del self.proxy_model_film_general_general_age_release
            self.proxy_model_film_general_general_age_release = None
            self.proxy_model_film_general_general_age_release = certain_proxy_model

        if category == 'film_resolution':

            del self.proxy_model_film_general_film_resolution
            self.proxy_model_film_general_film_resolution = None
            self.proxy_model_film_general_film_resolution = certain_proxy_model

        if category == 'film_studio':

            del self.proxy_model_film_general_film_studio
            self.proxy_model_film_general_film_studio = None
            self.proxy_model_film_general_film_studio = certain_proxy_model

        if category == 'film_distribution':

            del self.proxy_model_film_general_film_distribution
            self.proxy_model_film_general_film_distribution = None
            self.proxy_model_film_general_film_distribution = certain_proxy_model

        if category == 'film_rentals':

            del self.proxy_model_film_general_film_rentals
            self.proxy_model_film_general_film_rentals = None
            self.proxy_model_film_general_film_rentals = certain_proxy_model

        if category == 'film_production_company':

            del self.temporary_proxy_model_film_general_general_country
            self.temporary_proxy_model_film_general_general_country = None
            self.temporary_proxy_model_film_general_general_country = certain_proxy_model

        if category == 'general_packaging':

            del self.proxy_model_film_general_general_packaging
            self.proxy_model_film_general_general_packaging = None
            self.proxy_model_film_general_general_packaging = certain_proxy_model

        if category == 'general_media_type':

            del self.proxy_model_film_general_general_media_type
            self.proxy_model_film_general_general_media_type = None
            self.proxy_model_film_general_general_media_type = certain_proxy_model

        if category == 'person_relationship':

            del self.proxy_model_film_general_person_relationship
            self.proxy_model_film_general_person_relationship = None
            self.proxy_model_film_general_person_relationship = certain_proxy_model

        if category == 'person_relationship_status':

            del self.proxy_model_film_general_person_relationship_status
            self.proxy_model_film_general_person_relationship_status = None
            self.proxy_model_film_general_person_relationship_status = certain_proxy_model

        if category == 'person_religion':

            del self.proxy_model_film_general_person_religion
            self.proxy_model_film_general_person_religion = None
            self.proxy_model_film_general_person_religion = certain_proxy_model

        if category == 'person_hair_color':

            del self.proxy_model_film_general_person_hair_color
            self.proxy_model_film_general_person_hair_color = None
            self.proxy_model_film_general_person_hair_color = certain_proxy_model

        if category == 'person_eye_color':

            del self.proxy_model_film_general_person_eye_color
            self.proxy_model_film_general_person_eye_color = None
            self.proxy_model_film_general_person_eye_color = certain_proxy_model

        if category == 'person_salutation':

            del self.proxy_model_film_general_person_salutation
            self.proxy_model_film_general_person_salutation = None
            self.proxy_model_film_general_person_salutation = certain_proxy_model

        if category == 'person_title':

            del self.proxy_model_film_general_person_title
            self.proxy_model_film_general_person_title = None
            self.proxy_model_film_general_person_title = certain_proxy_model

        if category == 'person_gender':

            del self.proxy_model_film_general_person_gender
            self.proxy_model_film_general_person_gender = None
            self.proxy_model_film_general_person_gender = certain_proxy_model

        if category == 'music_genre':

            del self.proxy_model_film_general_music_genre
            self.proxy_model_film_general_music_genre = None
            self.proxy_model_film_general_music_genre = certain_proxy_model

        if category == 'general_award':

            del self.proxy_model_film_general_general_award
            self.proxy_model_film_general_general_award = None
            self.proxy_model_film_general_general_award = certain_proxy_model

        if category == 'person_nationality':

            del self.proxy_model_film_general_person_nationality
            self.proxy_model_film_general_person_nationality = None
            self.proxy_model_film_general_person_nationality = certain_proxy_model

        if category == 'general_language':

            del self.proxy_model_film_general_general_language
            self.proxy_model_film_general_general_language = None
            self.proxy_model_film_general_general_language = certain_proxy_model

        if category == 'general_region':

            del self.proxy_model_film_general_general_region
            self.proxy_model_film_general_general_region = None
            self.proxy_model_film_general_general_region = certain_proxy_model

        if category == 'music_studio':

            del self.proxy_model_film_general_music_studio
            self.proxy_model_film_general_music_studio = None
            self.proxy_model_film_general_music_studio = certain_proxy_model

        if category == 'music_spars_code':

            del self.proxy_model_film_general_music_spars_code
            self.proxy_model_film_general_music_spars_code = None
            self.proxy_model_film_general_music_spars_code = certain_proxy_model

        if category == 'music_publisher':

            del self.proxy_model_film_general_music_publisher
            self.proxy_model_film_general_music_publisher = None
            self.proxy_model_film_general_music_publisher = certain_proxy_model

        if category == 'music_style':

            del self.proxy_model_film_general_music_style
            self.proxy_model_film_general_music_style = None
            self.proxy_model_film_general_music_style = certain_proxy_model

        if category == 'music_label':

            del self.proxy_model_film_general_music_label
            self.proxy_model_film_general_music_label = None
            self.proxy_model_film_general_music_label = certain_proxy_model

        if category == 'music_distribution':

            del self.proxy_model_film_general_music_distribution
            self.proxy_model_film_general_music_distribution = None
            self.proxy_model_film_general_music_distribution = certain_proxy_model

        if category == 'book_genre':

            del self.proxy_model_film_general_book_genre
            self.proxy_model_film_general_book_genre = None
            self.proxy_model_film_general_book_genre = certain_proxy_model

        if category == 'book_publisher':

            del self.proxy_model_film_general_book_publisher
            self.proxy_model_film_general_book_publisher = None
            self.proxy_model_film_general_book_publisher = certain_proxy_model

        if category == 'book_binding':

            del self.proxy_model_film_general_book_binding
            self.proxy_model_film_general_book_binding = None
            self.proxy_model_film_general_book_binding = certain_proxy_model

        if category == 'coin_alloy':

            del self.proxy_model_film_general_coin_alloy
            self.proxy_model_film_general_coin_alloy = None
            self.proxy_model_film_general_coin_alloy = certain_proxy_model

        if category == 'coin_currency':

            del self.proxy_model_film_general_coin_currency
            self.proxy_model_film_general_coin_currency = None
            self.proxy_model_film_general_coin_currency = certain_proxy_model

        if category == 'coin_type':

            del self.proxy_model_film_general_coin_type
            self.proxy_model_film_general_coin_type = None
            self.proxy_model_film_general_coin_type = certain_proxy_model

        if category == 'videogame_genre':

            del self.proxy_model_film_general_videogame_genre
            self.proxy_model_film_general_videogame_genre = None
            self.proxy_model_film_general_videogame_genre = certain_proxy_model

        if category == 'profile_list':

            del self.proxy_model_profile_list
            self.proxy_model_profile_list = None
            self.proxy_model_profile_list = certain_proxy_model

        if category == 'coin_manufacturing_procedure':

            del self.proxy_model_film_general_coin_manufacturing_procedure
            self.proxy_model_film_general_coin_manufacturing_procedure = None
            self.proxy_model_film_general_coin_manufacturing_procedure = certain_proxy_model

        if category == 'coin_degree_preservation':

            del self.proxy_model_film_general_coin_degree_preservation
            self.proxy_model_film_general_coin_degree_preservation = None
            self.proxy_model_film_general_coin_degree_preservation = certain_proxy_model

        if category == 'coin_mint':

            del self.proxy_model_film_general_coin_mint
            self.proxy_model_film_general_coin_mint = None
            self.proxy_model_film_general_coin_mint = certain_proxy_model
