def chain_funcs(*funcs):
    """This closure returns a callable to call multiple functions"""
    # *args is used to send a non-keyworded variable
    # length argument list to the function.

    # chain_funcs skips this block and returns call_funcs
    
    def call_funcs(*args, **kwargs):
        """ Here a nested function is created. """
        # *args is used to send a non-keyworded variable
        # length argument list to the function and **kwargs
        # allows you to pass keyworded variable length of arguments
        # to a function

        for f in funcs:
            
            f(*args, **kwargs)  

    return call_funcs