#!/usr/bin/env python
#-*- coding:utf-8 -*-

import requests
from requests import session, exceptions, head
from requests.auth import HTTPBasicAuth

from pkg_resources import parse_version

def compare_parser(current_version, new_version):
    print "current_version", current_version
    print "new_version", new_version

    return parse_version(current_version) < parse_version(new_version)

def get_response(url,
                 http_auth_user_name = "",
                 http_auth_pwd = "",
                 time_out = "",
                 protected_area = False):

    if not protected_area:
        response = requests.get(url, stream = True)
        return response.raw.read(decode_content = True)
    else:
        request_session = session()
        http_basic_auth = HTTPBasicAuth(http_auth_user_name, http_auth_pwd)

        status_response = request_session.get(url = url, stream = True, auth = http_basic_auth, timeout = time_out)
        http_status_code = status_response.status_code

        if str(http_status_code) == '401':
            return False

        elif str(http_status_code) == '200':
            resGet = request_session.get(url, auth = http_basic_auth, stream = True)
            return resGet.content

if __name__ == "__main__":

    from config import ConfigurationSaver

    dict_custom_config = ConfigurationSaver()

    version = get_response(url = dict_custom_config.dict_set_general_settings["URLServerVersion"])

    print "VER", version

    print dict_custom_config.dict_set_general_settings["URLServerVersion"]

    print compare_parser("2.0", version)


