#!/usr/bin/env python
#-*- coding:utf-8 -*-

import webbrowser

def open_url_homepage(link):
    print "YOP"
    '''
        NOTICE:
        =======
        In this method the gui is inistialized. Currently the title 
        of the window is set and the flag of this window is flagged.

        PARAMETERS:
        ===========
        :link      -    We get a given url to open it in a browser.

        :return    -    Nothing is returned. The statement 'return'
                        terminates a function. That makes sure that the
                        function is definitely finished.
    '''

    '''
        Open in a new tab, if possible
    '''
    new = 2
    
    '''
        Open a public URL, in this case, the webbrowser docs
    '''
    webbrowser.open(link, new=new)

    return

if __name__ == "__main__":
    open_url_homepage("www.meine_domain.de")
