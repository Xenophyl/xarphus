#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG)

logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

'''
    Important note: It is important that this import of models (manage_table_model)
    after initializing the declarative_base object. Don't import before!
'''
try:
    from xarphus.core.manage_table_model import *

except ImportError:

    from manage_table_model import *

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session
from sqlalchemy.pool import Pool, NullPool
from sqlalchemy.ext.associationproxy import association_proxy

from sqlalchemy.orm import relationship, backref

from sqlalchemy import inspect

from sqlalchemy import Table, MetaData 

class SessionScope(object):
    def __init__(self, dbms=None, dbdriver=None,
                 dbuser=None, dbuser_pwd=None,
                 db_server_host=None, dbport=None, db_name=None,
                 admin_database=None):
 
        self.dbms = dbms
        self.dbdriver = dbdriver
        self.dbuser = dbuser
        self.dbuser_pwd = dbuser_pwd
        self.db_server_host = db_server_host
        self.dbport = dbport
        self.db_name = db_name
        self.admin_database = admin_database
       
        url = '{}+{}://{}:{}@{}:{}/{}'.format(
           self.dbms, self.dbdriver, self.dbuser, self.dbuser_pwd, self.db_server_host, self.dbport, self.db_name)
 
        '''
           Currently the echo is turned on to see the auto-generated SQL.
 
           That is, the Engine is a factory for connections as well as a pool of connections,
           not the connection itself. When you say in this case close(),
           the connection is returned to the connection pool within the Engine, not actually closed.
 
           So the self._Engine will not use connection pool if you set poolclass=NullPool.
           So the connection (SQLAlchemy session) will close directly after session.close()
           that means, if you set poolclass=NullPool each call to close() will close the underlying DBAPI connection.
       '''
        self._Engine = create_engine(url, pool_size=30, encoding='utf8', echo=False)
 
        '''
           Set up the session and store a sessionmaker for this db connection object
       '''
 
        self.session = None

        ''' Create the session factory '''
        self._session_factory = sessionmaker(bind=self._Engine)
 
        ''' Create the scoped_session, now self._Session registry is established '''
        self._Session = scoped_session(sessionmaker(bind=self._Engine, expire_on_commit=False))

        self.check_adminsitrate_database()

    def check_adminsitrate_database(self):

        if self.admin_database:
            print "You want to administrate it!"
            self.data_base_exists()
        else:
            print "Naa, you don't want to administrate the database."

    def test_connection(self):

        self.session.execute("SELECT Version();")

        return

    def user_info(self):

        print "user_info is called"

        conn_obj = self._Engine.connect()

        try:   

            #conn_obj.execute("SHOW GRANTS FOR '{db_name}'@'localhost';".format(db_name='sophus')) #create db

           conn_obj.execute('SHOW GRANTS FOR CURRENT_USER;') #create db
           conn_obj.commit()
           conn_obj.execute("SELECT Version();")

            

            #SHOW GRANTS FOR 'root'@'localhost';

        except:
            pass

    def create_data_base(self, db_name=None):

        conn_obj = self._Engine.connect()

        conn_obj.execute('COMMIT')
        conn_obj.execute("CREATE DATABASE {db_name}".format(db_name=db_name)) #create db
        conn_obj.execute("USE {db_name}".format(db_name=db_name)) # select new db

        # Database is created successfully, build tables upon first startup
        self.create_tables(connection_obj=conn_obj) #Base.metadata.create_all(conn_obj)

        conn_obj.close()

        return

    def create_tables(self, db_name=None, connection_obj=None):
        '''
            Build tables upon first startup.
            That means, its creates the table and tells it to create it in the 
            database engine that is passed. So we create all the tables we need.
        '''
        if connection_obj is None:
            '''
                The engine is used directly to issue SQL to the database. 
                First we procure a connection resource, which we get it via the Engine.connect() method.
                The connection resource is necessary, on this way we connect to the database directly.
                In this case its important that we pass the obtained connection resource to the function
                named create_all().
            '''
            conn_obj = self._Engine.connect()
            conn_obj.execute('COMMIT')
            conn_obj.execute("USE {db_name}".format(db_name=db_name)) # select new db

            Base.metadata.create_all(conn_obj)

            conn_obj.close()
        else:
            Base.metadata.create_all(connection_obj)


    def data_base_exists(self):


        inspector = inspect(self._Engine)

        database_exists = None

        for name_schema in iter(inspector.get_schema_names()):
            if name_schema == "xarphus":

                database_exists = True

        if database_exists:
            '''
                Database exists, just create tables.
            '''
            self.create_tables(db_name='xarphus')
        else:
            '''
                Database is missing, create missing database and all tables.
            '''
            self.create_data_base(db_name='xarphus')

        #self.user_info()

        #return

    def __enter__(self):
        '''
           Now all calls to Session() will create a thread-local session.
           That means, you can now use self.session to run multiple queries, etc.
           The registry is *optionally* starts called upon explicitly to create
           a Session local to the thread and/or request. That why we return self.session
       '''
        self.session = self._Session()
        return self.session
 
    def __exit__(self, exception, exc_value, traceback):
        print ""
 
        try:
            if exception:
 
                self.session.rollback()
            else:
 
                self.session.commit()
 
        finally:
 
            self.session.close()

    def disconnect(self):
        '''
            Make sure the dbconnection gets closed
        '''
        self.session = self._Session()

        '''
            .close() will give the connection back to the connection 
            pool of Engine and doesn't close the connection.
        '''
        self.session.close()

        '''
            dispose() will close all connections of the connection pool.
        '''
        self._Engine.dispose()

        self._Session = None
        self._Engine = None
        self.session = None





