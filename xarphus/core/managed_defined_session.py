from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

session_factory = sessionmaker()
ScopedSession = scoped_session(session_factory)

def remove_scope_session():
	print "remove_scope_session is called"
	#ScopedSession.close()
	#ScopedSession.remove()
	session_factory = None
	ScopedSession = None
	print "ScopedSession", ScopedSession

class DefinedSessionScope(object):
    def __init__(self, 
    			 scoped_session,
    			 engine):

    	self.scoped_session = scoped_session

    	self._engine = engine

    def disconnect(self):
    	print "Database is disconnected"

        '''
            Make sure the dbconnection gets closed
        '''
        #	close() will give the connection back to the connection 
        #	pool of Engine and doesn't close the connection.
        self.scoped_session.close()

        #	dispose() will close all connections of the connection pool.
        self._engine.dispose()

        #	We need to empty all attributes
    	self.scoped_session = None
    	self._engine = None


