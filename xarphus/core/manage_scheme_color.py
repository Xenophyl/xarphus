#!/usr/bin/env python
#-*- coding:utf-8 -*-

from xarphus.core.config import ConfigurationSaverDict

class Scheme_Color_Manager(object):
    def __init__(self):
        object.__init__(self)

        self.set_get_settings = ConfigurationSaver()

    #------------------------------------------------------------------------------------------------------------------
    def set_scheme_color(self, sheet_style):
        self.set_get_settings.dict_set_general_settings["CssStyleSheet"] = sheet_style

