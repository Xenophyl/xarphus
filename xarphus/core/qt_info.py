#!/usr/bin/env python

from __future__ import print_function
import sys

from PyQt4.Qt import PYQT_VERSION_STR as pyqt_str
from PyQt4.Qt import QT_VERSION_STR as qt_str

usage = 'usage: {0} [--pyqt|--qt]'.format(sys.argv[0])

from PyQt4.QtCore import QT_VERSION_STR
from PyQt4.Qt import PYQT_VERSION_STR
from sip import SIP_VERSION_STR



def get_versio_pyqt():
    return PYQT_VERSION_STR

def get_version_qt():
    return QT_VERSION_STR

def get_pyqt_version():
    ver = pyqt_str.split('-')
    major, minor = map(int, ver[0].split('.'))
    if len(ver) == 3:
        micro = int(ver[2])
        vtype = ver[1]
    else:
        micro = 0
        vtype = ''
    return major, minor, micro, vtype


def get_qt_version():
    ver = qt_str.split('.')
    major, minor, micro = map(int, ver)
    return major, minor, micro


def main(args):
    if len(args) >= 2:
        if args[1] == '--pyqt':
            return pyqt_str
        elif args[1] == '--qt':
            return qt_str
        else:
            raise ValueError, 'Invalid argument: {0}'.format(args[1])
    return 'PyQt {0}\nQt {1}'.format(pyqt_str, qt_str)