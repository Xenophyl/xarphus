from PyQt4.QtCore import Qt
from PyQt4.QtGui import QMessageBox, QPixmap, QProgressBar, QIcon, QLabel, QPushButton, QHBoxLayout, QSizePolicy


try:

    from core.manage_calculation import calculate_percentage

except ImportError:

    from manage_calculation import calculate_percentage


def flag_dialog_titlehint_customizewondowhint():
    '''
    This window has no additional buttons in the title bar:
    That means no:
        - minimize-button
        - maximize-button
        - help-button
        - close-button
    '''
    return Qt.Dialog | Qt.WindowTitleHint | Qt.CustomizeWindowHint



def custom_message_box(self,
                       text,
                       windows_title,
                       detail_text = None,
                       icon = None,
                       set_flag = False,
                       title_icon = None):

    '''
       SUMMARY
       =======
       This method is used to enable or disable the given Qt-Object .

       PARAMETERS
       ==========
       :windows_title (str):    Used to set the title of the message window.
       :detail_text (str):      Used to display the additional message.
       :text: (str):            Used to set the text of the main message.
       :icon (str):             Used to set an icon in the message box.
       :title_icon (str):       Used to set an icon in window title of the message box.
       :set_flag (boolean):     Sets the window flag on the widget.

The QMessageBox class has many methods for creating various types of message boxes.


       RETURNS
       =======
       :return:                 Returns the class from the modified QMessageBox.
    '''
    msgBox = QMessageBox()
    msgBox.setWindowTitle(unicode(windows_title))
    msgBox.setText(unicode(text))

    if not title_icon is None:
        msgBox.setWindowIcon(QIcon(title_icon))

    if set_flag:
        msgBox.setWindowFlags(Qt.Dialog | Qt.WindowTitleHint | Qt.CustomizeWindowHint)

    if not icon is None:
        msgBox.setIconPixmap(QPixmap(icon))

    if not detail_text is None:
        msgBox.setInformativeText(detail_text)

    return msgBox # Returns the object.

def custom_message_box_1(text_error, m_title, mIcon):
    mBox = QMessageBox()
    mBox.setWindowTitle(unicode(m_title))
    mBox.setText(unicode(text_error))
    mBox.setWindowFlags(flags)
    mBox.setIcon(mIcon)
    return mBox # Returns the object.


class PushButtonRightIcon(QPushButton):
    '''
       SUMMARY
       =======
       On the QPushButton its aligns icon on the right and sets the text center

       EXTENTED DISCRIPTION
       ====================


        DETAILS:
        ========
        By default the QPushButton() doesn't support to show you a text center and set the icon on the right.
        The only way: change the behavior is overriding the QPushButton().
        A possible solution is to place on top and on the right side a QLabel that shows the icon.
    '''
    def __init__(self, *args, **kwargs):

        QPushButton.__init__(self, parent = None, *args, **kwargs)

        icon = self.icon()
        icon_size = self.iconSize()
        # remove icon
        self.setIcon(QIcon())
        label_icon = QLabel()
        label_icon.setAttribute(Qt.WA_TranslucentBackground)
        label_icon.setAttribute(Qt.WA_TransparentForMouseEvents)
        lay = QHBoxLayout(self)
        lay.setContentsMargins(0, 0, 4, 0) #    setContentsMargins(int left, int top, int right, int bottom)
        lay.addWidget(label_icon, alignment = Qt.AlignRight) #  set the icon on the right
        label_icon.setPixmap(icon.pixmap(icon_size))

class ProgressBarText(QProgressBar):
    '''
        NOTICE:
        =======
        Progress bar in busy mode with text displayed at the center.

        DETAILS:
        ========
        By default the QProgressBar()-object doesn't show you a text when the range
        is set to minimu = 0 and maximum = 0, because QProgressBar().text()-method
        returns an empty string in that case. The only way: change the behavior is
        overriding the QProgressBar().text()-method as it is declared to be a virtual
        function.
    '''

    def __init__(self,
                 parent = None):
        QProgressBar.__init__(self, parent)

        self.setAlignment(Qt.AlignCenter)
        self._text = None
        self._maximum = None

    def setText(self, text):
        self._text = text

    def setMaximum(self, value):
        self._maximum = value

    def setValue(self, value):
        self.prefixFloat = value
        self.setText('{}%'.format(calculate_percentage(value = value, total = self._maximum)))
        QProgressBar.setValue(self, int(value))

    def text(self):
        return self._text
