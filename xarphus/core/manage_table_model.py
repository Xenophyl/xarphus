#from sqlalchemy.orm import relationship, backref


from sqlalchemy import Column, Integer, Text, String, Date, DECIMAL, ForeignKey
from sqlalchemy import Index
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import collate

#   setting up root class for declarative declaration
Base = declarative_base()

#   In general we will create and map all classes.
#   First we start by making this class extend the Base class
class AllocationPersonNationality(Base):

   #    A __tablename__ to indicate what is the name of the table that will support this class.
    __tablename__ = 'allocation_person_nationality'

    #   An id to represent the primary key in the table.
    person_id = Column(Integer, ForeignKey('person.id'), primary_key = True)
    nationality_id = Column(Integer, ForeignKey('person_nationality.id'), primary_key = True)

    # Use cascade='delete,all' to propagate the deletion
    person = relationship("Person", backref = backref("allocation_person_nationality", lazy='dynamic', cascade = "all, delete-orphan" ))
    person_nationality = relationship("PersonNationality", backref = backref("allocation_person_nationality", lazy='dynamic', cascade = "all, delete-orphan" ))

class AllocationPersonLaguage(Base):

    __tablename__ = 'allocation_person_language'
    person_id = Column(Integer, ForeignKey('person.id'), primary_key = True)
    language_id = Column(Integer, ForeignKey('general_language.id'), primary_key = True)
    status = Column(String(length = 255))

    person = relationship("Person", backref = backref("allocation_person_language", lazy='dynamic', cascade = "all, delete-orphan" ))
    general_language = relationship("GeneralLanguage", backref = backref("allocation_person_language", lazy='dynamic', cascade = "all, delete-orphan" ))

class AllocationPersonRelationship(Base):

    __tablename__ = 'allocation_person_relationship'
    person_id = Column(Integer, ForeignKey('person.id'), primary_key = True)
    relationship_id = Column(Integer, ForeignKey('person_relationship.id'), primary_key = True)

    person = relationship("Person", backref = backref("allocation_person_relationship", lazy='dynamic', cascade = "all, delete-orphan" ))
    person_relationship = relationship("PersonRelationsship", backref = backref("allocation_person_relationship", lazy='dynamic', cascade = "all, delete-orphan" ))

class AllocationFilmGeneralFilmGenre(Base):

    __tablename__ = 'allocation_film_general_film_genre'
    filmgeneral_id = Column(Integer, ForeignKey('film_general.id'), primary_key = True)
    filmgenre_id = Column(Integer, ForeignKey('film_genre.id'), primary_key = True)

    film_general = relationship("FilmGeneral", backref = backref("allocation_film_general_film_genre", lazy='dynamic', cascade = "all, delete-orphan" ))
    film_genre = relationship("FilmGenre", backref = backref("allocation_film_general_film_genre", lazy='dynamic', cascade = "all, delete-orphan" ))

class AllocationFilmGeneralGeneralCountry(Base):

    __tablename__ = 'allocation_filmgeneral_general_country'
    filmgeneral_id = Column(Integer, ForeignKey('film_general.id'), primary_key = True)
    general_country_id = Column(Integer, ForeignKey('general_country.id'), primary_key = True)

    film_general = relationship("FilmGeneral", backref = backref("allocation_filmgeneral_general_country", lazy='dynamic', cascade = "all, delete-orphan" ))
    general_country = relationship("GeneralCountry", backref = backref("allocation_filmgeneral_general_country", lazy='dynamic', cascade = "all, delete-orphan" ))

class Family(Base):

    __tablename__ = "family"

    id = Column(Integer, primary_key=True, unique = True, autoincrement = True)
    status = Column(String(length = 255), nullable = False)

    # We have referred PERSON as foreign key twice, so we have to
    # explicitly tell SQLAlchemy how to join the PERSON. In general,
    # SQLAlchemy has no problem with two foreigns pointing to the same key,
    # but it isn't psychic. We set foreign_keys parameter
    # for each relationship. We you have to define which foreign key to use
    # in a relationship.
    person_id = Column(Integer, ForeignKey('person.id'))
    person = relationship("Person", foreign_keys=[person_id])

    family_person_id = Column(Integer, ForeignKey('person.id'))
    family_person = relationship("Person", foreign_keys=[family_person_id])

class Person(Base):

    __tablename__ = "person"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    nickname = Column(String(length = 255))
    alias_name  = Column(String (length = 255))
    photo_file_extension = Column(String(length = 5))
    name_normally_used = Column(String(length = 50), nullable = False)
    first_middle_name = Column(String(length = 255))
    last_name = Column(String(length = 100))
    birth_name = Column(String(length = 100))
    body_height = Column(String(length = 10))
    wedding_anniversary = Column(Date)
    birthday = Column(Date)
    day_of_death = Column(Date)
    notice = Column(Text())
    record_added_day = Column(Date)
    record_edited_day = Column(Date)

    gender_id = Column(Integer, ForeignKey('person_gender.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    gender = relationship("PersonGender", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    hair_color_id = Column(Integer, ForeignKey('person_hair_color.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    hair_color = relationship("PersonHairColor", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    eye_color_id = Column(Integer, ForeignKey('person_eye_color.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    eye_color = relationship("PersonEyeColor", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    title_id = Column(Integer, ForeignKey('person_title.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    title = relationship("PersonTitle", single_parent = True, cascade = "all, delete-orphan")

    salutation_id = Column(Integer, ForeignKey('person_salutation.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    salutation = relationship("PersonSalutation", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    place_id = Column(Integer, ForeignKey('general_place.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    place = relationship("GeneralPlace", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    religion_id = Column(Integer, ForeignKey('person_religion.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    religion = relationship("PersonReligion", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    relationship_status_id = Column(Integer, ForeignKey('person_relationship_status.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    relationship_status = relationship("PersonRelationsshipStatus", single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

class FilmGeneral(Base):

    __tablename__ = "film_general"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    translated_title = Column(String(length = 1000), nullable = False)
    original_title = Column(String(length = 1000))
    original_alternative_title = Column(String(length = 1000))
    translated_alternative_title = Column(String(length = 1000))
    release_year = Column(Integer)
    poster_file_extension = Column(String(length = 5))

    total_evaluation = Column(Integer)
    genre_evaluation = Column(Integer)

    colour_format_id = Column(Integer, ForeignKey('film_colourformat.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    colour_format = relationship("FilmColourformat", foreign_keys=[colour_format_id], single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

    sound_system_id = Column(Integer, ForeignKey('film_soundsystem.id', onupdate = "cascade", ondelete = 'SET NULL'), nullable = True)
    sound_system = relationship("FilmSoundsystem", foreign_keys=[sound_system_id], single_parent = True, cascade = "all, delete-orphan")#passive_deletes=True)

class FilmVideoformat(Base):

    __tablename__ = "film_videoformat"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    format = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmColourformat(Base):

    __tablename__ = "film_colourformat"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    format = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmSoundsystem(Base):

    __tablename__ = "film_soundsystem"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    system = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmAudiochannel(Base):

    __tablename__ = "film_audiochannel"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    channel = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class FilmAudioformat(Base):

    __tablename__ = "film_audioformat"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    format = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class FilmVideonorm(Base):

    __tablename__ = "film_videonorm"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    norm = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmVersion(Base):

    __tablename__ = "film_version"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    version = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmEdition(Base):

    __tablename__ = "film_edition"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    edition = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmDimension(Base):

    __tablename__ = "film_dimension"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    dimension = Column(String(length = 20, collation = 'NOCASE'), nullable = False, unique = True)

class FilmAspectRatio(Base):

    __tablename__ = "film_aspect_ratio"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    aspect_ratio = Column(String(length = 5, collation = 'NOCASE'), nullable = False, unique = True)
    format = Column(String(length = 60), nullable = False)

class FilmRegionalCode(Base):

    __tablename__ = "film_regional_code"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    regional_code = Column(String(length = 5, collation = 'NOCASE'), nullable = False, unique = True)
    region = Column(Text(length = 500), nullable = False)

class GeneralManufacturer(Base):

    __tablename__ = "general_manufacturer"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    manufacturer = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class FilmResolution(Base):

    __tablename__ = "film_resolution"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    resolution = Column(String(length =  20, collation = 'NOCASE'), nullable = False, unique = True)
    presenting_format = Column(String(length = 150), nullable = False)

class FilmStudio(Base):

    __tablename__ = "film_studio"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    studio = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class FilmDistribution(Base):

    __tablename__ = "film_distribution"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    distribution = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class FilmRentals(Base):

    __tablename__ = "film_rentals"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    rentals = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class FilmProductionCompany(Base):

    __tablename__ = "film_production_company"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    production_company = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralPackaging(Base):

    __tablename__ = "general_packaging"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    packaging = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralMediaType(Base):

    __tablename__ = "general_media_type"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    media_type = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralPlace(Base):

    __tablename__ = "general_place"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    place = Column(String(length = 255, collation = 'NOCASE'), nullable = False, unique = True)

#Index('place', GENERAL_PLACE.place, mysql_length=10)

class GeneralRegion(Base):

    __tablename__ = "general_region"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    region = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralAgeRelease(Base):

    __tablename__ = "general_age_release"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    age_release = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class BookGenre(Base):

    __tablename__ = "book_genre"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    genre = Column(String(length = 150, collation = 'NOCASE'), nullable = False, unique = True)

class BookPublisher(Base):

    __tablename__ = "book_publisher"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    publisher = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class BookBinding(Base):

    __tablename__ = "book_binding"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    binding = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class CoinType(Base):

    __tablename__ = "coin_type"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    typeof = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class CoinAlloy(Base):

    __tablename__ = "coin_alloy"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    alloy = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class CoinCurrency(Base):

    __tablename__ = "coin_currency"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    currency = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class CoinManufacturingProcedure(Base):

    __tablename__ = "coin_manufacturing_procedure"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    shortcut = Column(String(length = 50))
    manufacturing_procedure = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class CoinDegreePreservation(Base):

    __tablename__ = "coin_degree_preservation"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    shortcut = Column(String(length = 50))
    degree_preservation = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class CoinMint(Base):

    __tablename__ = "coin_mint"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    mint_mark = Column(String(length = 50))
    mint = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class FilmGenre(Base):

    __tablename__ = "film_genre"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    genre = Column(String(length = 150, collation = 'NOCASE'), nullable = False, unique = True)

class MusicGenre(Base):

    __tablename__ = "music_genre"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    genre = Column(String(length = 150, collation = 'NOCASE'), nullable = False, unique = True)

class MusicStyle(Base):

    __tablename__ = "music_style"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    style = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class MusicLabel(Base):

    __tablename__ = "music_label"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    label = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class MusicStudio(Base):

    __tablename__ = "music_studio"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    studio = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class MusicSPARSCode(Base):

    __tablename__ = "music_spars_code"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    spars_code = Column(String(length = 10, collation = 'NOCASE'), nullable = False, unique = True)

class MusicPublisher(Base):

    __tablename__ = "music_publisher"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    publisher = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class MusicDistribution(Base):

    __tablename__ = "music_distribution"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    distribution = Column(String(length = 200, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralCountry(Base):

    __tablename__ = "general_country"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    country = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralLanguage(Base):

    __tablename__ = "general_language"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    language = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonGender(Base):

    __tablename__ = "person_gender"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)

    gender = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonNationality(Base):

    __tablename__ = "person_nationality"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    nationality = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class PersonSalutation(Base):

    __tablename__ = "person_salutation"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    salutation = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonTitle(Base):

    __tablename__ = "person_title"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    title = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonHairColor(Base):

    __tablename__ = "person_hair_color"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    hair_color = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonEyeColor(Base):

    __tablename__ = "person_eye_color"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    eye_color = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonReligion(Base):

    __tablename__ = "person_religion"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    religion = Column(String(length = 50, collation = 'NOCASE'), nullable = False, unique = True)

class PersonRelationsshipStatus(Base):

    __tablename__ = "person_relationship_status"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    relationship_status = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class PersonRelationsship(Base):

    __tablename__ = "person_relationship"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    relationship = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class GeneralAward(Base):

    __tablename__ = "general_award"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    award = Column(String(length = 100, collation = 'NOCASE'), nullable = False, unique = True)

class VideogameGenre(Base):

    __tablename__ = "videogame_genre"

    id = Column(Integer, primary_key = True, unique = True, autoincrement = True)
    genre = Column(String(length = 150, collation = 'NOCASE'), nullable = False, unique = True)

'''
#######################################################
############# Non-Identifying Relationships ##########
#######################################################
'''

# left_right = Table('left_right', Base.metadata,
#     Column('left_id', Integer, ForeignKey('left.id')),
#     Column('right_id', Integer, ForeignKey('right.id'))
# )


# # class Association(Base):
# #     """"""
# #     __tablename__ = 'association'
# #     left_id = Column(Integer, ForeignKey('left.id'), primary_key=True)
# #     right_id = Column(Integer, ForeignKey('right.id'), primary_key=True)
# #     extra_data = Column(String(50))
# #     #child = relationship("Child")

# #     def __init__(self, LeftID, RightID, ExtraData):
# #         """"""
# #         self.left_id = LeftID
# #         self.right_id = RightID
# #         self.extra_data = ExtraData

# class Parent(Base):
#     """"""
#     __tablename__ = 'left'
#     id = Column(Integer, primary_key=True)
#     name = Column(String(30))
#     #children = relationship("Association")

#     # def __init__(self, Country):
#     #     """"""
#     #     self.country = Country

# class Child(Base):
#     """"""
#     __tablename__ = 'right'
#     id = Column(Integer, primary_key=True)
#     name = Column(String(30))

#     toppings = relationship('Parent', secondary=left_right,
#                             backref=backref('right', lazy='dynamic'))

#     # def __init__(self, Country):
#     #     """"""
#     #     self.country = Country

'''
#######################################################
############### Identifying Relationships ############
#######################################################
'''
# class Association_1(Base):
#     __tablename__ = 'association_1'
#     left_id = Column(Integer, ForeignKey('left.id'), primary_key=True)
#     right_id = Column(Integer, ForeignKey('right.id'), primary_key=True)
#     extra_data = Column(String(50))
#     child = relationship("Child")

# class Parent(Base):
#     __tablename__ = 'left'
#     id = Column(Integer, primary_key=True)
#     children = relationship("Association_1")

# class Child(Base):
#     __tablename__ = 'right'
#     id = Column(Integer, primary_key=True)
# '''
# #############################################
# '''

# class Order_Product(Base):
#   __tablename__ = 'order_product'
#   order_id = Column(Integer, ForeignKey('orders.id'), primary_key=True)
#   product_id = Column(Integer, ForeignKey('products.id'), primary_key=True)
#   quantity = Column(String(30))

    # '''
    # :ForeignKey('orders.id')    -   Here we have to model the relationship ourself.
    #                                 First we need to declare the foreign key.
    #                                 It has to have the same type as the primary key
    #                                 of the table we want to point to and additionally
    #                                 the column needs to be given a ForeignKey instance
    #                                 with the first argument being the dotted name to
    #                                 the column referenced. Note that this is the table
    #                                 name, not the class name.

    # :relationship()             -   The relationship is then declared on Order_Product with relationship.
    #                                 The first argument is a class or the name of a class we want to have
    #                                 the relationship with. By default it will try to find a valid join
    #                                 condition automatically.

    # :lazy='dynamic              -   The backref argument automatically declares the reverse.
    #                                 It will attach a cars property on the manufacturer.
    #                                 The lazy='dynamic' tells SQLAlchemy to make the backref
    #                                 lazy and a dynamic loading one. In that case accessing
    #                                 order.order_product or product.order_product will be a
    #                                 query object you can further refine instead of directly
    #                                 firing the query and returning a list. It returns a query
    #                                 object instead of firing the query. The lazy settings
    #                                 can also be set on relationship and not just backref.
    # '''
#   order = relationship("Order", backref=backref("order_product", lazy='dynamic', cascade="all, delete-orphan" ))
#   product = relationship("Product", backref=backref("order_product", lazy='dynamic', cascade="all, delete-orphan" ))

#   def __init__(self, order, product, quantity):
#     self.order = order
#     self.product =  product
#     self.quantity = quantity

# class Product(Base):
#   __tablename__ = 'products'
#   id = Column(Integer,  primary_key=True, unique=True)
#   name = Column(String(80))

#   orders = relationship("Order", secondary="order_product")

#   def __init__(self, name):
#     self.name = name

# class Order(Base):
#   __tablename__ = 'orders'
#   id = Column(Integer,  primary_key=True, unique=True)
#   name = Column(String(80))

#   orders = relationship("Product", secondary="order_product")

#   def __init__(self, name):
#     self.name = name
