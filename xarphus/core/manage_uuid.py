import uuid
import hashlib

def get_uuid4():
    # uuid is used to generate a random number
    uuid_4 = uuid.uuid4().hex
    return uuid_4

if __name__ == "__main__":
    uuid_4 = get_uuid4()
    print "UUID_4: ", uuid_4
