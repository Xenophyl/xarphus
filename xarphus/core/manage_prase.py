#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

import json

def parsing_str_to_bool(mode_str):
    logger.info("parsing_str_to_bool")
    print "mode_str", mode_str
    return json.loads(mode_str.lower())


def parsing_str_to_bool_database(mode_str):
    try:
        parsing_dict = {
                    "mysql": True
                        }
        return parsing_dict[mode_str.lower()]
    except:
        return False

if __name__ == '__main__':
    result = parsing_str_to_bool("44555kk")
    print result
    print type(result)
