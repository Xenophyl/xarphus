from PIL import Image

print "SAG", dir(Image)
print "VERSION", Image.VERSION


def create_thumbnail(image_path, image_name):

    size = 150, 150
    
    im = Image.open(image_path)
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(image_name + ".png")

    return True

# When I call the function!!!
create_thumbnail("icon.png","GERDA")
