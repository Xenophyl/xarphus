from mechanize import Browser

try:
    from xarphus.core.manage_prase import parsing_str_to_bool
except ImportError:
    from manage_prase import parsing_str_to_bool


from mechanize import Browser
    
def upload_file_basic_auth(protected_url,
                           upload_url,
                           user,
                           pwd,
                           file_path):
    '''
       SUMMARY
       =======
       This function is used to upload file.

       EXTENTED DISCRIPTION
       ====================
       This function not only uploads files, but also access a HTTP Basic authenticated page.

       PARAMETERS
       ==========
       :protected_url (str):    Here we just the clear url to the protected area, not to the upload-page.

       :upload_url (str):       We ecxept a url to the formular, which initialize the upload. 

       :user (unicode):         The user name of this protected area.

       :pwd str (unicode):      Password is import to login to protected area successfully.

       :file_path (unicode):    We need the path to the image file which we would to upload it.

       RETURNS
       =======
       :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                me a better feeling, that says: 'return' terminates a function definitely.
    '''
    br = Browser()
    # Ignore robots.txt.  Do not do this without thought and consideration.
    br.set_handle_robots(False)
    br.add_password(protected_url, user, pwd)
    response = br.open(upload_url)

    # There is a form without name.
    br.select_form(nr=0)
    br.form.add_file(open(file_path,'rb'), "text/plain", file_path, name = 'datei') # Name is here the name of input field on webpahe
                                                                                # In this case the input field is just called 'datei' 
    res = br.submit()
    
    return

def register_user_support(url,
                          user_name,
                          password,
                          secret_key):
    '''
       NOTICE
       ======
       This function is used to sign up a user.

       PARAMETERS
       ==========
       :url (str):            BLAH  

       :user_name (unicode):  BLAH

       :password (unicode):   BLAH

       :secret_key (str):     BLAH

       RETURNS
       =======
       
       :return:               When the registration was successful, 
                              the number one will be returned, otherwise None.
    '''
    browser = Browser()
    browser.set_handle_robots(False) # ignore robots

    browser.open(url)
    browser.select_form(nr=0)
    #   Fill in the login form.
    browser.form['user'] = unicode(user_name)
    browser.form['pass'] = unicode(password)
    browser.form['secret_key'] = unicode(secret_key)
    result = browser.submit()

    result_submit = result.read()

    if result_submit == "True":
      return 1
      browser.close()
    else:
      browser.close()
      return None


def user_exists_online_support(url,
                               user_name):
    '''
       NOTICE
       ======
       This function checks if a user already exists.

       PARAMETERS
       ==========
       :url (str):            BLAH  

       :user_name (unicode):  BLAH

       RETURNS
       =======
       
       :return:               When the user already exists, its returns False, 
                              otherwise True.
    '''
    booled_string = None
    browser = Browser()
    browser.set_handle_robots(False) # ignore robots

    browser.open(url)

    #   Force try using the to select the first form with no name
    browser.select_form(nr = 0)

    #   Fill in the login form.
    browser["user"] = user_name.strip()
    response = browser.submit()

    content = response.get_data()
    booled_string = parsing_str_to_bool(mode_str = content)

    browser.close()

    return booled_string

if __name__ == "__main__":
    user_name = raw_input("Enter username: ")
    url = "http://sophus.bplaced.net/form_check_user.php"

    result = user_exists_online_support(url = url,
                                       user_name = user_name)
    print "result", result

    # upload_file_basic_auth(protected_url = "http://sophus.bplaced.net/xarphus_support/support_account/test/",
    #                    upload_url = "http://sophus.bplaced.net/xarphus_support/support_account/test/upload_form.php",
    #                    user = u"test",
    #                    pwd = u"test",
    #                    file_path = u'test_2.rar')
