#!/usr/bin/env python
#-*- coding:latin1 -*-
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from hashlib import sha512
from sys import exc_info
from traceback import format_exc
from os import path
import errno

from requests import session, exceptions, head
from requests.exceptions import HTTPError, Timeout, TooManyRedirects, ConnectionError, URLRequired, ConnectTimeout, ReadTimeout, RequestException
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
from shutil import copyfileobj

from PyQt4.QtCore import QThread, pyqtSignal, Qt, QSemaphore

try:

    from xarphus.core.manage_checksum import create_checksum
    from xarphus.core.managed_request import connect_server
    from xarphus.core.manage_folder import create_folder
except ImportError:
    from core.manage_checksum import create_checksum
    from core.managed_request import connect_server
    from core.manage_folder import create_folder

BASE_PATH = path.dirname(path.abspath(__file__))

class Download_Thread(QThread):
    """ A worker thread that takes download. """
    finished_thread = pyqtSignal()

    finished_download = pyqtSignal()
    abort_downloading = pyqtSignal()
    notify_progress = pyqtSignal(int)

    #   New signals
    create_folder_signal = pyqtSignal()
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()
    close_window_signal = pyqtSignal()
    stop_work_signal = pyqtSignal()
    set_engabled_signal = pyqtSignal(bool)
    error_http_signal = pyqtSignal(str, str, str, str)
    download_complete_message_signal = pyqtSignal(str, str, str, str, str)
    download_error_message_signal = pyqtSignal(str, str, str, str, str)
    on_progress_ready_signal = pyqtSignal(object)
    set_max_value_signal = pyqtSignal()
    change_progress_signal = pyqtSignal(int)
    change_pushbutton_signal = pyqtSignal(bool)

    def __init__(self, parent = None, **kwargs):
        """ Initialize the worker thread."""
        QThread.__init__(self, parent)

        self._kwargs = kwargs

        self._run_semaphore = QSemaphore(1)

        self._stopped = False

    def compare_checksum(self, **kwargs):
        '''
           SUMMARY
           =======
           In this method, the checksum values ​​are to be compared with one another..

           RETURNS
           =======

           :return: Return True if online_checksum and offline_checksum are the same, otherwise
                    its returns False
        '''
        #logger.info("Compare the checksum values")

        offline_checksum = create_checksum(file_name = kwargs['update_folder'], algorithm = sha512())

        online_checksum = kwargs['online_checksum']

        if online_checksum == offline_checksum:
            return True
        else:
            return False

    def start_thread(self):
        '''
           SUMMARY
           =======
           This method is like a reimplemented 'run()'-method. 'start_thread()' is connected
           with the thread's started signal, so the the thread starts, this method will run automatically
           This method is used to download the file from online server.

           PARAMETERS:
           ===========
           :return    -     Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        self.create_folder_signal.emit()
        self.set_engabled_signal.emit(True)

        try:
            url = self._kwargs['configuration_saver_dict'].dict_set_general_settings['URLServerUpdate']
            time_out = int(self._kwargs['configuration_saver_dict'].dict_set_general_settings['TimeOutUpdate'])

            http_auth_user_name = self._kwargs['configuration_saver_dict'].ht_access['htaccess_username']
            http_auth_pwd = self._kwargs['configuration_saver_dict'].ht_access['htpassword']

            http_auth, request_session, status_response = connect_server(
                HTTPAuth = 'HTTPBasicAuth',
                url = url,
                http_auth_user_name = http_auth_user_name,
                http_auth_pwd = http_auth_pwd,
                time_out = time_out)

            if str(status_response) == '404':
                raise exceptions.HTTPError

            if str(status_response) == '401':
                self.error_http_signal.emit(
                    self.tr('Log In error'),
                    self.tr('Password is wrong (Unauthorized). Please try again.'),
                    '',
                    '')

            elif str(status_response) == '200':
                file_name = 'stellenangebot.zip'

                base_path = self._kwargs['configuration_saver_dict'].dict_set_general_settings['BasePath']

                current_profile = self._kwargs['configuration_saver_dict'].dict_set_general_settings['current_profile']
                if not current_profile:
                    #   for example the program is running as stand alone.
                    self._kwargs['update_folder'] = path.join(base_path, 'update', file_name)

                elif current_profile:
                    self._kwargs['update_folder'] = path.join(base_path, 'profiles', current_profile, 'update', file_name)

                resGet = request_session.get(url, auth = http_auth, stream = True)
                file_size = int(resGet.headers['Content-length']) # output 121291539

                downloaded_bytes = 0

                self.on_progress_ready_signal.emit(file_size)
                self.set_max_value_signal.emit()

                result = file_size / (1024*2)

                chunk_size = int(result)


                update_file = request_session.get(url = url, auth = http_auth, stream = True)

                with open(self._kwargs['update_folder'], 'wb') as fd:

                    for chunk in update_file.iter_content(chunk_size):
                        fd.write(chunk)
                        downloaded_bytes = fd.tell()
                        download_percentage = float(downloaded_bytes)/file_size*100

                        self.change_progress_signal.emit(download_percentage)

                        if self._run_semaphore.available() == 0:
                            self._run_semaphore.release(1)
                            break

                if downloaded_bytes == file_size:
                    compare_result = self.compare_checksum(**self._kwargs)
                    if compare_result:
                        self.download_complete_message_signal.emit(
                            self.tr('Confirmation'),
                            self.tr('The update package has been downloaded completely.'),
                            '',
                            self._kwargs['image_path_collection'].info_32x32,
                            self._kwargs['image_path_collection'].info_32x32)

                        self.change_pushbutton_signal.emit(False)
                        self.quit_thread_signal.emit()
                        self.wait_thread_signal.emit()

                        self.close_window_signal.emit()

                    else:
                        self.download_error_message_signal.emit(
                            self.tr('Unable to verify Update'),
                            self.tr('It is failed verification, because your downloaded update package is wrrong.'),
                            '',
                            self._kwargs['image_path_collection'].warning_48x48,
                            self._kwargs['image_path_collection'].warning_48x48)

                else:
                    if not self._stopped:
                        self.download_error_message_signal.emit(
                            self.tr('Download error'),
                            self.tr('There was a problem downloading the update.'),
                            self.tr('Press "Download" to try again or contact customer support.'),
                            self._kwargs['image_path_collection'].warning_48x48,
                            self._kwargs['image_path_collection'].warning_48x48)

        except Timeout as exc:
            # Maybe set up for a retry, or continue in a retry loop
            self.download_error_message_signal.emit(
                self.tr('Error - Timeout'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except TooManyRedirects as exc:
            # Tell the user their URL was bad and try a different one
            self.download_error_message_signal.emit(
                self.tr('Error - Too many redirects'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except HTTPError as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - HTTP'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except ConnectionError as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - Connection'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except URLRequired as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - URL required'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except ConnectTimeout as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - Connect Timeout'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except ReadTimeout as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - Read Timeout'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except RequestException as exc:
            self.download_error_message_signal.emit(
                self.tr('Error - General'),
                self.tr("An error has occurred: "),
                unicode(exc),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)
        except IOError as exc:
            if exc.errno != errno.EEXIST:
                #   Check if file and directory does not exist.
                create_folder(self._kwargs['update_folder'])
                #   After the missing folder is created successfully,
                #   we have to ecursively call itself.
                self.start_thread()
        except exc:  # catch *all* exceptions
            desired_trace = format_exc(exc_info())
            logger.error(desired_trace)
            self.download_error_message_signal.emit(
                self.tr('Error - General'),
                self.tr("An error has occurred: "),
                unicode(desired_trace),
                self._kwargs['image_path_collection'].warning_48x48,
                self._kwargs['image_path_collection'].warning_48x48)

        finally:

            self.quit_thread_signal.emit()
            self.wait_thread_signal.emit()

    def stop(self):
        self._stopped = True
        self._run_semaphore.acquire(1)
