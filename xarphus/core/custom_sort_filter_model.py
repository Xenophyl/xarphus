#!/usr/bin/env python
#-*- coding:utf-8 -*-

#############################################################################
##
## Copyright (C) 2010 Hans-Peter Jansen <hpj@urpla.net>.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################


# This is only needed for Python v2 but is harmless for Python v3.

# try:
#     import sip
#     sip.setapi('QVariant', 2)
# except ImportError:
#     pass
# except ValueError:
#     print "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu"


from PyQt4.QtCore import QDate, QRegExp, QDateTime, Qt
from PyQt4.QtGui import QSortFilterProxyModel

class MySortFilterProxyModel(QSortFilterProxyModel):
    """
    Implements a QSortFilterProxyModel that allows for custom
    filtering. 
    New functions should accept two arguments, the column to be
    filtered and the currently set filter string, and should
    return True to accept the row, False otherwise.

    The filterString is used as the main pattern matching
    string for filter functions. This could easily be expanded
    to handle regular expressions if needed.
    """
    def __init__(self, parent=None):
        super(MySortFilterProxyModel, self).__init__(parent)

        self.columns = 2

        self.minDate = QDate()
        self.maxDate = QDate()

    def setFilterMaximumColumns(self, column = None):
        self.columns = column

    def setFilterMinimumDate(self, date):
        self.minDate = date
        self.invalidateFilter()

    def filterMinimumDate(self):
        return self.minDate

    def setFilterMaximumDate(self, date):
        self.maxDate = date
        self.invalidateFilter()
 
    def filterMaximumDate(self):
        return self.maxDate

    def filterAcceptsRow(self, sourceRow, sourceParent):

        if self.columns == 2:
            index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
            index1 = self.sourceModel().index(sourceRow, 1, sourceParent)
            index2 = self.sourceModel().index(sourceRow, 2, sourceParent)

            return (   (self.filterRegExp().indexIn(self.sourceModel().data(index0)) >= 0
                        or self.filterRegExp().indexIn(self.sourceModel().data(index1)) >= 0)
                    and self.dateInRange(self.sourceModel().data(index2)))

        if self.columns == 3:
            index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
            index1 = self.sourceModel().index(sourceRow, 1, sourceParent)
            index2 = self.sourceModel().index(sourceRow, 2, sourceParent)
            index3 = self.sourceModel().index(sourceRow, 3, sourceParent)

            return (   (self.filterRegExp().indexIn(self.sourceModel().data(index0)) >= 0
                        or self.filterRegExp().indexIn(self.sourceModel().data(index1)) >= 0
                        or self.filterRegExp().indexIn(self.sourceModel().data(index2)) >= 0)
                    and self.dateInRange(self.sourceModel().data(index3)))

    def lessThan(self, left, right):
        leftData = self.sourceModel().data(left)
        rightData = self.sourceModel().data(right)

        if not isinstance(leftData, QDate):
            emailPattern = QRegExp("([\\w\\.]*@[\\w\\.]*)")

            if left.column() == 1 and emailPattern.indexIn(leftData) != -1:
                leftData = emailPattern.cap(1)

            if right.column() == 1 and emailPattern.indexIn(rightData) != -1:
                rightData = emailPattern.cap(1)

        return leftData < rightData

    def dateInRange(self, date):
        if isinstance(date, QDateTime):
            date = date.date()

        return (    (not self.minDate.isValid() or date >= self.minDate)
                and (not self.maxDate.isValid() or date <= self.maxDate))