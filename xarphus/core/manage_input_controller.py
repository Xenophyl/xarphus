import re

class InputController(object):
    '''
            NOTIC:
            ======
            Remove the trailing space of the string.
    '''
    def __init__(self):
        object.__init__(self)

        '''
            First we save the  the regular expression pattern
            in a variable named regex.
            Well the expression r"\s\s+" means: one whitespace character,
            followed by one or more whitespaces chatacters
        '''
        regex = r"\s\s+"

        '''
            Now we compile the pattern.
            After then we save the compiled pattern
            as result in a variable named compiled_re.
        '''
        self.compiled_re = re.compile(regex)

    def remove_trailing_characters(self,
                                    content):
        '''
            NOTIC:
            ======
            Remove the trailing space of the string.

            PARAMETERS
            ==========
            :text          -

            :max_char               -

            :allow_trailing_spaces  -

            :allow_only_number      -

            :max_length             -

            :return                 -

        '''
        content = unicode(content)

        return content.rstrip()

    def get_num_from_string(self,
                            text):
        '''
            NOTIC:
            ======
            Remove all spaces..

            PARAMETERS
            ==========
            :text           -

            :return         -

        '''

        return ''.join(i for i in text if i.isdigit())

    def avoid_all_spaces(self,
                        original_text):
        '''
            NOTIC:
            ======
            Remove all spaces..

            PARAMETERS
            ==========
            :original_text          -

            :return                 -

        '''
        original_text = unicode(original_text)

        return original_text.replace(" ", "")

    def set_max_length(self,
                        original_text,
                        max_char):
        '''
            NOTIC:
            ======
            Remove all double space.

            PARAMETERS
            ==========
            :original_text          -

            :max_char               -

            :return                 -

        '''
        return original_text[:max_char]

    def avoid_double_spaces(self,
                           original_text,
                           pos,
                           allow_trailing_spaces = True,
                           allow_only_number = False,
                           max_length = None):

        '''
            NOTIC:
            ======
            Remove all double space.

            PARAMETERS
            ==========
            :original_text          -

            :widget_object          -

            :allow_trailing_spaces  -

            :allow_only_number      -

            :max_length             -

            :return                 -

        '''

        '''
            We know that given text is a QString-object.
            So we have to convert the given text
            into a python-string, because we want to work
            with them in python.
        '''
        original_text = unicode(original_text)

        if allow_only_number:
            original_text = self.get_num_from_string(original_text)

        '''
            We don't want to allow that text starts
            with wihtespace.
        '''
        if original_text.startswith(' '):

            '''
                We overwrite the original text.
                Here we are spezifying that we want to start
                at position 1 in the original text. Next step
                we save the copy of original text - without space
                at first position in the string.
            '''
            original_text = original_text[1:]

        '''
            We take a look if the user wants to remove
            the trailing spaces of the string. In default
            we get always True, because in genereal we
            write information with parts of words, that needs
            whitespace.
        '''
        if not allow_trailing_spaces:
            '''
                User doesn't want to allow the trailing spaces.
                We have to invoke theself.avoidavoid_all_spaces()-method.
            '''
            original_text = self.avoid_all_spaces(original_text)

        if not max_length is None:
            original_text = self.set_max_length(original_text, max_length)



        '''
            We want to search the string to see if it
            contains any runs of multiple spaces
        '''
        if self.compiled_re.search(original_text):
            '''
               We know that given text is a QString-object.
               So we have to convert the given text
               into a python-string, because we want to work
               with them in python.
            #original_text = unicode(original_text)
            '''

            '''
                NOTICE: Do replacements before and after cursor pos

                We save the current and correct cursor position
                of a QLineEdit()-object in the variable named pos.
            '''
            # pos = widget_object.cursorPosition()

            # print "pos", pos
            # print "my_pos", my_pos

            '''
                Search and Replace: Here the sub()-method
                replaces all occurrences of the RE pattern
                in string with text.
                And then it returns modified string and saves
                it in the variables prefix and suffix.
                And then we use the slice.
                The slicing works like string[start:end]:
                The text[:pos] means from
                the start of the string up to current pos (the prefix); and
                text[pos:] means from current pos up to the end of the string
                (the suffix)
            '''
            prefix = self.compiled_re.sub(' ', original_text[:pos])
            suffix = self.compiled_re.sub(' ', original_text[pos:])

            '''
                NOTICE: Cursor might be between spaces
                Imagine that the string is "A |B C" (with the cursor
                shown as "|"). If "B" is deleted, we will get "A | C"
                with the cursor left between multiple spaces. But
                when the string is split into prefix and suffix,
                each part will contain only *one* space, so the
                regexp won't replace them.
                So we take a look if the varbiale prefix ends
                with a whitespace and we check if suffix starts
                with a whitespace.
            '''
            if prefix.endswith(' ') and suffix.startswith(' '):

                '''
                    Yes its True, so we overwrite the variable named
                    suffix and slice it. suffix[1:] means, we starts
                    at 1 until open end.
                    This removes the extra space at the start of the
                    suffix that was missed by the regexp (see above).
                    Removing it from the suffix means the cursor
                    stays closest to its original position
                '''
                suffix = suffix[1:]

                ''
                #   It doesn't matter we can also use prefix. But here
                #   we have to remove the extra space at the end
                #   of the prefix.
                # prefix = prefix[:1]

            '''
                Now we have to set the text of the QLineEdit()-object,
                so we put the both varialbes named prefix and suffix
                together.
            '''
            modified_text = (prefix + suffix)

            '''
                After then, we have to set the cursor position.
                The len()-method returns the length of the
                variable named prefix.
                When the text is set, it will clear the cursor. The
                prefix and suffix gives the text before and after the
                old cursor position. Removing spaces may have shifted
                the old position, so the new postion is calculated
                from the length of the current prefix
            '''
            return len(prefix), modified_text

        else:
            '''
                No, there aren't whitespaces, so we return default data - no modification!
            '''
            #return widget_object.cursorPosition(), original_text
            return pos, original_text
            #return original_text

