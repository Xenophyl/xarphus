#!/usr/bin/python
#-*- coding:utf-8 -*-

class ConfigurationSaverDict(object):
        def __init__(self):
        #--------------------------------------------------------------------------------------
            self.dict_set_general_settings = {
                                'current_profile': '',
                                'language': '', ###
                                'language_id': '',  ###
                                'AskBeforClose': '',    ###
                                'AskBeforDelete':'',    ###
                                'BasePath': '',
                                'SQLiteFileName': 'xarphus',
                                'WindowState': '',
                                'PathToINISetting': '',
                                'PathToProfileFolder': '',
                                'BaseSelectedProfileFolder': '',
                                'PathToSQLite': 'D:\\Dan\\Python\\xarphus_project\\Xarphus\\database\\',
                                'PathToCopyINISetting_Temp': '',
                                'PathToCopyProfileFolder_Temp': '',
                                'PathToSettingFolder': '',
                                'PathToLogFolder': '',
                                'PathToTempFolder': '',
                                'PathToZipFile': '',
                                'TempImagePathFilmPoster': '',
                                'TempImagePathCoinValueSide': '',
                                'PathToExecuteFile': '',
                                'PathToProgramFolder':'',
                                'LastPathFolderDialog': '',
                                'LastPathFileDialog': '',
                                'VisibleToolBar': '',   ###
                                'VisibleStatusBar': '', #ää
                                'URLServerRegistration': 'http://sophus.bplaced.net/form_register_user.php',
                                'URLServerUpdate': 'http://xarphus.bplaced.net/xarphus_update/DVDProInstall.zip',
                                'URLGeneralUpdateCheckSum': 'http://xarphus.bplaced.net/xarphus_update/check_sum.txt',
                                #'URLSupport': 'http://sophus.bplaced.net/xarphus_support/support_account/xarphus',
                                'URLCheckUserSuppoer': 'http://sophus.bplaced.net/form_check_user.php',
                                'SupportLanguage': 'ger',
                                'GeneralRegionFileName': 'general_region.php',
                                'GeneralCountryFileName': 'general_country.php',
                                'GeneralLanguageFileName': 'general_language.php',
                                'PersonNationalityFileName': 'person_nationality.php',
                                'GeneralAwardFileName': 'general_award.php',
                                'FilmGenreFileName': 'film_genre.php',
                                'FilmProductionCompanyFileName': 'film_production_company.php',
                                'FilmRentalsFileName': 'film_rentals.php',
                                'MusicGenreFileName': 'music_genre.php',
                                'GeneralPackagingFileName': 'general_packaging.php',
                                'GeneralMediaType': 'general_media_type.php',
                                'GeneralManufacturer': 'general_manufacturer.php',
                                'PersonRelationshipFileName': 'person_relationship.php',
                                'FilmDistributionFileName': 'film_distribution.php',
                                'FilmStudioFileName': 'film_studio.php',
                                'FilmResolutionFileName': 'general_resolution.php',
                                'FilmRegionalCodeFileName': 'film_regional_code.php',
                                'FilmAspectRatioFileName': 'film_aspect_ratio.php',
                                'FilmDimensionFileName': 'film_dimension.php',
                                'FilmEditionFileName': 'film_edition.php',
                                'FilmVersionFileName': 'film_version.php',
                                'FilmVideonormFileName': 'film_videonorm.php',
                                'FilmAudioformatFileName': 'film_audioformat.php',
                                'FilmAudiochannelFileName': 'film_audiochannel.php',
                                'FilmColourformatFileName': 'film_colourformat.php',
                                'FilmVideoformatFileName': 'film_videoformat.php',
                                'FilmSoundsystemFileName': 'film_soundsystem.php',
                                'URLServerVersion': 'http://xarphus.bplaced.net/update_version.txt',
                                'URLServerVersionNews': '',
                                'URLServerVersionHistory': '',
                                'TimeOutUpdate': '10',    ###
                                'TimeOutGetVersionHistory': '', ###
                                'Database': '', ###
                                'DatabaseSystem_Temp': '',
                                'LinkColor': '#306AF8',
                                'RedColorMDI': '32',
                                'GreenColorMDI': '31',
                                'BlueColorMDI': '31',
                                'TextFieldFontColor': '',
                                'GeneralFontColor': '',
                                'TextFieldBackgroundColor': '',
                                'TextFieldBackgroundColorWarning': '',
                                'TextFieldBackgroundColorAccept': '',
                                'TeXtLabelFontColor': '',
                                'DialogBackground': '',
                                'ComboBoxBackgroundColor': '',
                                'ComboBoxFontColor': '',
                                'PushButtonBackgroundColor': '',
                                'PushButtonFontColor': '',
                                'TextEditBackgroundColor': '',
                                'TextEditFontColor': '',
                                'GroupBoxFontColor': '',
                                'GroupBoxBackgroundColor': '',
                                'GroupBoxBorderColor': '',
                                'TabWidgetBackgroundColor': '',
                                'TabWidgetFontColor': '',
                                'TabBarBorderColor': '',
                                'treeWidgetBackgroundColor': '',
                                'zip_pwd': '',
                                'GUIStyle': '', ###
                                'GUIStiyleID': '',  ###
                                'SchemeColor': '',   ###
                                'SchemeColorID': '',    ###
                                'CssStyleSheet': '',
                                'VisibleMovie': '', ###
                                'VisiblePerson': '',    ###
                                'VisibleBook': '',  ###
                                'VisibleNote': '',  ###
                                'VisibleStamp': '', ###
                                'VisibleRent': '',  ###
                                'VisibleMusic': '', ###
                                'VisibleQuotation': '', ###
                                'VisibleVideoGame': '', ###
                                'VisibleWine': '',  ###
                                'VisibleCoin': '',  ###
                                'VisibleContact': '',  ###
                                'visible': '',
                                'thumbnail_aspect_ratio_height': 95,
                                'thumbnail_aspect_ratio_width': 80,
        }

            self.ht_access = {
                        'htaccess_username': '',
                        'htpassword': ''
        }

            self.dict_program_info = {
                      "product_name": "Xarphus",
                      "exe_name": "Xarphus",
                      "product_description": "Collection Manager",
                      "product_by": "by",
                      "product_author": "Sophus",
                      "product_team": "Xarphus-Team",
                      "product_copyright": "Copyright",
                      "product_copyright_characters": "©",
                      "product_year_from": "2015",
                      "product_year_till": "2015",
                      "product_version": "1.0.0",
                      "product_GUI_Version": "Qt4",
                      "product_built": "Pre-Alpha",
                      "product_faq": "http://docs.python.org/library/webbrowser.html",
                      "product_site": "http://www.web.de",
                      "product_release_note": "www.web.de",
                      "db_name": "xarphus",
                      "xarphus_installer":"xarphus_installer",
        }
