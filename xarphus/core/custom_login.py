import requests
import sys

# Fill in your details here to be posted to the login form.
payload = {
    'username': 'Sophuss',
    'password': 'danny02522871'
}
url = 'http://www.metalflirt.de/community/login'

# Use 'with' to ensure the session context is closed after use.
# Session used to handle cookies
with requests.Session() as c:
    # here is the login URL, and the data we want to submit
    c.post(url, data=payload)
    
    # An authorised request.
    # test it by accessing hidden page
    r = c.get('http://www.metalflirt.de/community/moshpit')
    
    # Here it will tests the login, to make sure
    # we already logged in into the system
    print payload["username"] in r.content
    print r.content
        # etc...
