#!/usr/bin/env python
#-*- coding:utf-8 -*-
from os import path
from sys import exc_info
from traceback import format_exc

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QObject, QModelIndex, Qt, QSize, pyqtSignal
from PyQt4.QtGui import QStandardItemModel, QStandardItem, QIcon, QPixmap

class StandardItemModel(QObject):

    populate_standard_item_model_in_gui_signal = pyqtSignal(object, int, object, object)

    def __init__(self,
                parent = None,
                **kwargs):
        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        ''' Attributes '''
        self._configuration_saver_dict = kwargs.get('configuration_saver_dict')

        ###################################################
        #################  Original StandardItemModel #####
        ###################################################
        #   Person
        self.standard_item_model_person_profile = None

        self.standard_item_model_person_salutation = None
        self.standard_item_model_person_title = None
        self.standard_item_model_person_gender = None
        self.standard_item_model_person_religion = None
        self.standard_item_model_person_eye_color = None
        self.standard_item_model_person_hair_color = None
        self.standard_item_model_person_relationship_status = None
        self.standard_item_model_person_relationship = None
        self.standard_item_model_person_nationality = None

        #   Film
        self.standard_item_model_film_genre = None


        self.standard_item_model_film_general_general_country = None
        self.temporary_standard_item_model_film_general_production_country = None
        self.temporary_standard_item_model_film_general_general_country = None


        self.standard_item_model_film_production_company = None

        self.standard_item_model_film_rentals = None
        self.standard_item_model_film_distribution = None
        self.standard_item_model_film_studio = None
        self.standard_item_model_film_resolution = None
        self.standard_item_model_film_regional_code = None
        self.standard_item_model_film_aspect_ratio = None
        self.standard_item_model_film_dimension = None
        self.standard_item_model_film_edition = None
        self.standard_item_model_film_version = None
        self.standard_item_model_film_videonorm = None
        self.standard_item_model_film_audioformat = None
        self.standard_item_model_film_audio_channel = None
        self.standard_item_model_film_colour_format = None
        self.standard_item_model_film_videoformat = None
        self.standard_item_model_film_soundsystem = None
        #   General
        self.standard_item_model_general_language = None
        self.standard_item_model_general_award = None

        self.standard_item_model_general_country = None

        self.standard_item_model_general_region = None
        self.standard_item_model_general_media_type = None
        self.standard_item_model_general_packaging = None
        self.standard_item_model_general_age_release = None
        self.standard_item_model_general_manufacturer = None
        #   Music
        self.standard_item_model_music_genre = None
        self.standard_item_model_music_studio = None
        self.standard_item_model_music_spars_code = None
        self.standard_item_model_music_publisher = None
        self.standard_item_model_music_style = None
        self.standard_item_model_music_label = None
        self.standard_item_model_music_distribution = None
        #   Book
        self.standard_item_model_book_genre = None
        self.standard_item_model_book_publisher = None
        self.standard_item_model_book_binding = None
        #   Coin
        self.standard_item_model_coin_alloy = None
        self.standard_item_model_coin_currency = None
        self.standard_item_model_coin_type = None
        self.standard_item_model_coin_manufacturing_procedure =None
        self.standard_item_model_coin_degree_preservation = None
        self.standard_item_model_coin_mint = None
        #   Video Game
        self.standard_item_model_videogame_genre = None
        #   Profile List
        self.standard_item_model_profile_list = None

        ###############################################################
        ####  Temporary StandardItemModel for saving/editing Movie ####
        ###############################################################
        #   Film

        self.temporary_standard_item_model_film_general_film_genre = None
        self.temporary_standard_item_model_film_general_film_soundsystem = None
        self.temporary_standard_item_model_film_general_film_videoformat = None
        self.temporary_standard_item_model_film_general_film_videonorm = None
        self.temporary_standard_item_model_film_general_film_version = None
        self.temporary_standard_item_model_film_general_film_dimension = None
        self.temporary_standard_item_model_film_general_film_edition = None
        self.temporary_standard_item_model_film_general_film_colour_format = None
        self.temporary_standard_item_model_film_general_film_audio_channel = None
        self.temporary_standard_item_model_film_general_film_audioformat = None
        self.temporary_standard_item_model_film_general_film_aspect_ratio = None
        self.temporary_standard_item_model_film_general_film_regional_code = None
        self.temporary_standard_item_model_film_general_film_resolution = None
        self.temporary_standard_item_model_film_general_film_studio = None
        self.temporary_standard_item_model_film_general_film_distribution = None
        self.temporary_standard_item_model_film_general_film_rentals = None
        self.temporary_standard_item_model_film_general_film_production_company = None
        self.temporary_standard_item_model_film_general_film_general_packaging = None
        self.temporary_standard_item_model_film_general_film_general_media_type = None
        self.temporary_standard_item_model_film_general_film_person_relationship = None
        self.temporary_standard_item_model_film_general_film_person_relationship_status = None
        self.temporary_standard_item_model_film_general_film_person_religion = None

        self.temporary_standard_item_model_film_general_person_hair_color = None
        self.temporary_standard_item_model_film_general_person_religion = None
        self.temporary_standard_item_model_film_general_person_eye_color = None
        self.temporary_standard_item_model_film_general_person_salutation = None
        self.temporary_standard_item_model_film_general_person_title = None
        self.temporary_standard_item_model_film_general_person_gender = None
        self.temporary_standard_item_model_film_general_person_nationality = None
        self.temporary_standard_item_model_film_general_person_relationship_status = None
        self.temporary_standard_item_model_film_general_person_relationship = None
        self.temporary_standard_item_model_film_general_general_language = None
        self.temporary_standard_item_model_film_general_general_region = None
        self.temporary_standard_item_model_film_general_general_manufacturer = None
        self.temporary_standard_item_model_film_general_general_media_type = None
        self.temporary_standard_item_model_film_general_general_packaging = None
        self.temporary_standard_item_model_film_general_general_age_release = None
        self.temporary_standard_item_model_film_general_general_award = None
        self.temporary_standard_item_model_film_general_music_genre = None
        self.temporary_standard_item_model_film_general_music_studio = None
        self.temporary_standard_item_model_film_general_music_spars_code = None
        self.temporary_standard_item_model_film_general_music_publisher = None
        self.temporary_standard_item_model_film_general_music_style = None
        self.temporary_standard_item_model_film_general_music_label = None
        self.temporary_standard_item_model_film_general_music_distribution = None
        self.temporary_standard_item_model_film_general_book_genre = None
        self.temporary_standard_item_model_film_general_book_publisher = None
        self.temporary_standard_item_model_film_general_book_binding = None
        self.temporary_standard_item_model_film_general_coin_alloy = None
        self.temporary_standard_item_model_film_general_coin_currency = None
        self.temporary_standard_item_model_film_general_coin_type = None
        self.temporary_standard_item_model_film_general_coin_manufacturing_procedure = None
        self.temporary_standard_item_model_film_general_coin_degree_preservation = None
        self.temporary_standard_item_model_film_general_coin_mint = None
        self.temporary_standard_item_model_film_general_videogame_genre = None

        self.set_standard_item_model()
        self.set_temporary_standard_item_model()

    def get_class_name(self):
        '''
            NOTICE:
            =======
            This method is used to determine the names of the classes automatically.

            PARAMETERS:
            ===========
            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def reset_standard_item_model(self,
                                 category):

        if category == "general_region":

            del self.standard_item_model_general_region
            self.standard_item_model_general_region = None

        return

    def dlear_stnandard_item_model(self,
                                   certain_standard_item_model):
        '''
           SUMMARY
           =======
           This method is identical to the clear_all_standard_item_model()-method.
           With the difference that items of a single model are deleted here.

           PARAMETERS
           ==========
           :certain_standard_item_model (class):   We except a models to remove all items of it.

           RETURNS
           =======
           :return:                                 Nothing is returned. Well I know a simple 'return' isn't necessary,
                                                    but this statement gives me a better feeling, that says: 'return'
                                                    terminates a function definitely.
        '''
        print ""
        print "StandardItemModel clear_standard_item_model is called successfully"
        print "StandardItemModel in clear_standard_item_model / certain_standard_item_model.objectName(): --> \n {}".format(certain_standard_item_model.objectName())
        print ""
        logger.info("Remove all items from the single model.")

        certain_standard_item_model.clear()

        return

    def clear_all_standard_item_model(self,
                                     list_certain_item_models):
        '''
           SUMMARY
           =======
           This method is used to remove all items from models, which is given in the list.

           PARAMETERS
           ==========
           :list_certain_item_models (class):   We except a list of models to remove all items of them.

           RETURNS
           =======
           :return:                                     Nothing is returned. Well I know a simple 'return' isn't necessary,
                                                        but this statement gives me a better feeling, that says: 'return'
                                                        terminates a function definitely.
        '''
        logger.info("Remove all items from all given models.")

        for certain_standard_item_model in list_certain_item_models:

            certain_standard_item_model.clear()

        return

    def remove_row(self,
                    standard_item_model,
                    item_id,
                    flags = Qt.MatchExactly,
                    column = 0):
        '''
           SUMMARY
           =======
           This method is used to remove certain row from given model.

           PARAMETERS
           ==========
           :standard_item_model (class):    We need a certain model to manipulate the records, which
                                            are saved in there.

           :item_id (int):                  We need a ID to find the record in the given model.

           :flags (MatchFlag):              By default the flag is set on MatchExactly, because we are about
                                            only search after IDs of saved records.

           :column (int):                   This coulmn says where we should search. By default its the first
                                            column, because all IDs are located in forst columns.

           RETURNS
           =======
           :return:                         Nothing is returned. Well I know a simple 'return' isn't necessary,
                                            but this statement gives me a better feeling, that says: 'return'
                                            terminates a function definitely.
        '''
        logger.info("Remove row vom model")

        #   We will first need to find the items with the matching text
        #   by iterating through the returned list, and then remove them from the model.
        for item in standard_item_model.findItems(str(item_id), flags, column):
            standard_item_model.removeRow(item.row())

        return

    def populate_model(self,
                       model,
                       tuple_items,
                       show_image = False):
        '''
           SUMMARY
           =======
           This method is used to populate the given model, that shows the containt on QTreeView.

           EXTENTED DISCRIPTION
           ====================
           First this this method just unpacks the given tuple and second it creates
           a new QStandardItem-class for each unpacked data. Third a new list is created
           containing the newly created QStandardItem-class(es). Fourth the created list
           is added to the given model.

           PARAMETERS
           ==========
           :model (QStandardItemModel): We need a model where the data should be loaded.

           :tuple_items (tuple):        The given data are in a tuple.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        count_items = len(tuple_items)

        append_row = None

        if count_items == 2:

            item_first, item_second = tuple_items

            append_row = [QStandardItem(str(item_first)), QStandardItem(item_second)]

            model.appendRow(append_row)

        elif count_items == 3:

            item_first, item_second, item_third = tuple_items

            append_row = [QStandardItem(str(item_first)), QStandardItem(item_second), QStandardItem(item_third)]

            model.appendRow(append_row)

        # elif count_items == 4:

        #     item_first, item_second, item_third, item_fourth = tuple_items

        #     append_row = [QStandardItem(str(item_first)), QStandardItem(item_second), QStandardItem(item_third), QStandardItem(item_fourth)]


        elif count_items == 4:

            if show_image:

                item_first, item_second, item_third, item_fourth = tuple_items

                item = QStandardItem()

                created_file_name = "{id}.{extension}".format(id = item_first, extension = item_fourth)
                BASE_PATH = self._configuration_saver_dict.dict_set_general_settings["BasePath"]

                IMAGE_PATH = path.join(BASE_PATH, 'images', 'person', created_file_name)

                thumbnail_width = int(self._configuration_saver_dict.dict_set_general_settings['thumbnail_aspect_ratio_width'])

                thumbnail_height = int(self._configuration_saver_dict.dict_set_general_settings['thumbnail_aspect_ratio_height'])

                img_size = QSize(thumbnail_width, thumbnail_height) # (int width, int height)


#                item.setIcon(QIcon(QPixmap(IMAGE_PATH).scaled(img_size)))

                try:
                    with open(IMAGE_PATH) as file:
                        #item.setIcon(QIcon(QPixmap(IMAGE_PATH).scaled(img_size)))#, Qt.KeepAspectRatio)))
                        IMAGE_PATH = IMAGE_PATH

                except IOError as e: #Unable to open file - Does not exist OR no read permissions
                    #item.setIcon(QIcon(QPixmap(':/img_150x200_default/default/img_150x200/default_photo.png').scaled(img_size)))

                    IMAGE_PATH = ':/img_150x200_default/default/img_150x200/default_photo.png'

                append_row = [QStandardItem(str(item_first)), QStandardItem(item), QStandardItem(item_second), QStandardItem(item_third)]

                self.populate_standard_item_model_in_gui_signal.emit(append_row, count_items, img_size, IMAGE_PATH)

            else:

                item_first, item_second, item_third, item_fourth = tuple_items

                append_row = [QStandardItem(str(item_first)), QStandardItem(item_second), QStandardItem(item_third), QStandardItem(item_fourth)]

                model.appendRow(append_row)

        return

    def find_text_model(self,
                        list_tuple,
                        model,
                        flags = Qt.MatchExactly,
                        search_index = 0,
                        column = 0):
        print ""
        print "StandardItemModel find_text_model is calles successfully"
        print "StandardItemModel in find_text_model / list_tuple: {}".format(list_tuple)
        print "StandardItemModel in find_text_model / search_index: {}".format(search_index)
        print "StandardItemModel in find_text_model / model: {}".format(model.objectName())
        print "StandardItemModel in find_text_model / list_tuple[search_index]: {}".format(list_tuple[search_index])
        item = None
        if not list_tuple[search_index] is None:
            print "Isn't None"

            try:
                print "Starting loop"

                for item in model.findItems(str(list_tuple[search_index]), flags , column):

                    if item:

                        return True

                    else:
                        print "Nothing in Model Temp"

                print "Item contains:", item
                print "Loop is started"

            except AttributeError:

                desired_trace = format_exc(exc_info())

                logger.error(desired_trace)

                if not desired_trace.find("'NoneType' object has no attribute 'findItems'") == -1:

                    pass

        else:

            raise TypeError

    def synchronize_temporary_model(self,
                                    work_around,
                                    model,
                                    list_tuple,
                                    flags = Qt.MatchExactly,
                                    column = 0):
        print ""
        print "StandardItemModel synchronize_temporary_model is called successfully"
        print "StandardItemModel in synchronize_temporary_model / work_around: {}".format(work_around)
        print "StandardItemModel in synchronize_temporary_model / model: {}".format(model.objectName())
        print "StandardItemModel in synchronize_temporary_model / list_tuple: {}".format(list_tuple)

        result = self.find_text_model(list_tuple = list_tuple,
                                     model = model,
                                     flags = flags,
                                     column = column)

        if result:

            print "Found"

            if work_around == "change_row":

                self.remove_row(standard_item_model = model,
                                item_id = str(list_tuple[0]))

                self.populate_model(model = model,
                                    tuple_items = list_tuple)

            elif work_around == "remove_row":

                self.remove_row(standard_item_model = model,
                                item_id = str(list_tuple[0]))

        #for irow in xrange(self.standard_item_model_film_production_company.rowCount()):

            # item = self.standard_item_model_film_production_company.index(irow, 1, QModelIndex())

            # try:
            #     #print "item", item.data(Qt.DisplayRole)

            #     print "item", unicode(item.data(Qt.DisplayRole))
            # except UnicodeEncodeError:
            #     print "item", item.data(Qt.DisplayRole)

            #print "TEXT",  u"%s " % unicode(self.standard_item_model_film_production_company.data(item))#.toString()


            # row = []
            # for icol in xrange(self.standard_item_model_film_production_company.columnCount()):
            #     cell = self.standard_item_model_film_production_company.data(self.standard_item_model_film_production_company.createIndex(irow, icol))
            #     row.append(cell)

            #     count += 1

            # # print all elems per row
            # print ', '.join(str(c) for c in row)

            # print "count", count

    def set_temporary_standard_item_model(self):

        category_list = [
                        ('film_general_company_production', 0, 2 ),
                        ('film_general_film_genre', 0, 2 ),
                        ('film_general_film_soundsystem', 0, 2),
                        ('film_general_film_videoformat', 0, 2),
                        ('film_general_film_colour_format', 0, 2),
                        ('film_general_film_audio_channel', 0, 2),
                        ('film_general_film_audioformat', 0, 2),
                        ('film_general_film_videonorm', 0, 2),
                        ('film_general_film_version', 0, 2),
                        ('film_general_film_edition', 0, 2),
                        ('film_general_film_dimension', 0, 2),
                        ('film_general_film_aspect_ratio', 0, 3),
                        ('film_general_film_regional_code', 0, 3),
                        ('film_general_film_resolution', 0, 2),
                        ('film_general_film_studio', 0, 2),
                        ('film_general_film_distribution', 0, 2),
                        ('film_general_film_rentals', 0, 2),
                        ('film_general_film_production_company', 0, 2),
                        ('film_general_general_manufacturer', 0, 2),
                        ('film_general_general_age_release', 0, 2),
                        ('film_general_general_packaging', 0, 2),
                        ('film_general_general_media_type', 0, 2),
                        ('film_general_general_award', 0, 2),
                        ('film_general_general_language', 0, 2),
                        ('film_general_general_region', 0, 2),
                        ('film_general_general_country', 0, 2),
                        ('film_general_person_relationship', 0, 2),
                        ('film_general_person_relationship_status', 0, 2),
                        ('film_general_person_religion', 0, 2),
                        ('film_general_person_hair_colour', 0, 2),
                        ('film_general_person_eye_colour', 0, 2),
                        ('film_general_person_salutation', 0, 2),
                        ('film_general_person_title', 0, 2),
                        ('film_general_person_gender', 0, 2),
                        ('film_general_person_nationality', 0, 2),
                        ('film_general_music_studio', 0, 2),
                        ('film_general_music_spars_code', 0, 2),
                        ('film_general_music_genre', 0, 2),
                        ('film_general_music_publisher', 0, 2),
                        ('film_general_music_style', 0, 2),
                        ('film_general_music_label', 0, 2),
                        ('film_general_music_distribution', 0, 2),
                        ('film_general_book_genre', 0, 2),
                        ('film_general_book_publisher', 0, 2),
                        ('film_general_book_binding', 0, 2),
                        ('film_general_coin_alloy', 0, 2),
                        ('film_general_coin_currency', 0, 2),
                        ('film_general_coin_type', 0, 2),
                        ('film_general_coin_manufacturing_procedure', 0, 3),
                        ('film_general_coin_degree_preservation', 0, 3),
                        ('film_general_coin_mint', 0, 3),
                        ('film_general_videogame_genre', 0, 2),
                        ]

        for category, rows, columns in category_list:
            self.temporary_standard_item_model(category = category, rows = rows, columns = columns, parent = self)

    def temporary_standard_item_model(self,
                                      category = None,
                                      rows = None,
                                      columns = None,
                                      parent = None):
        '''
            NOTICE:
            =======
            Create an empty model for the TreeViews' data

            This method constructs a new item model that initially
            has rows and columns, and that has the given parent.

            QTreeView needs a model to manage its data.

            PARAMETERS:
            ===========
            :rows       -   we except an intger for row.

            :columns    -   An integer for column is required.

            :parent     -   Given parent, for example self

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''

        # if category == "film_general_company_production":
        #     if not self.temporary_standard_item_model_film_general_production_country is None:
        #         #   When model already exists only clear the model for adding new items
        #         self.temporary_standard_item_model_film_general_production_country.clear()
        #     else:
        #         #   The model doesn't exists, create a new one
        #         self.temporary_standard_item_model_film_general_production_country = QStandardItemModel(rows, columns, parent)
        #         self.temporary_standard_item_model_film_general_production_country.setObjectName('temporary_standard_item_model_film_general_production_country')

        if category == "film_general_film_genre":
            if not self.temporary_standard_item_model_film_general_film_genre is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_genre.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_genre = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_genre.setObjectName('temporary_standard_item_model_film_general_film_genre')

        elif category == "film_general_film_soundsystem":
            if not self.temporary_standard_item_model_film_general_film_soundsystem is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_soundsystem.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_soundsystem = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_soundsystem.setObjectName('temporary_standard_item_model_film_general_film_soundsystem')

        elif category == "film_general_film_videoformat":
            if not self.temporary_standard_item_model_film_general_film_videoformat is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_videoformat.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_videoformat = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_videoformat.setObjectName('temporary_standard_item_model_film_general_film_videoformat')

        elif category == "film_general_film_videonorm":
            if not self.temporary_standard_item_model_film_general_film_videonorm is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_videonorm.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_videonorm = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_videonorm.setObjectName('temporary_standard_item_model_film_general_film_videonorm')

        elif category == "film_general_film_colour_format":
            if not self.temporary_standard_item_model_film_general_film_colour_format is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_colour_format.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_colour_format = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_colour_format.setObjectName('temporary_standard_item_model_film_general_film_colour_format')

        elif category == "film_general_film_audio_channel":
            if not self.temporary_standard_item_model_film_general_film_audio_channel is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_audio_channel.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_audio_channel = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_audio_channel.setObjectName('temporary_standard_item_model_film_general_film_audio_channel')

        elif category == "film_general_film_audioformat":
            if not self.temporary_standard_item_model_film_general_film_audioformat is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_audioformat.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_audioformat = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_audioformat.setObjectName('temporary_standard_item_model_film_general_film_audioformat')

        elif category == "film_general_film_version":
            if not self.temporary_standard_item_model_film_general_film_version is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_version.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_version = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_version.setObjectName('temporary_standard_item_model_film_general_film_version')

        elif category == "film_general_film_edition":
            if not self.temporary_standard_item_model_film_general_film_edition is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_edition.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_edition = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_edition.setObjectName('temporary_standard_item_model_film_general_film_edition')

        elif category == "film_general_film_dimension":
            if not self.temporary_standard_item_model_film_general_film_dimension is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_dimension.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_dimension = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_dimension.setObjectName('temporary_standard_item_model_film_general_film_dimension')

        elif category == "film_general_film_aspect_ratio":
            if not self.temporary_standard_item_model_film_general_film_aspect_ratio is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_aspect_ratio.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_aspect_ratio = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_aspect_ratio.setObjectName('temporary_standard_item_model_film_general_film_aspect_ratio')

        elif category == "film_general_film_regional_code":
            if not self.temporary_standard_item_model_film_general_film_regional_code is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_regional_code.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_regional_code = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_regional_code.setObjectName('temporary_standard_item_model_film_general_film_regional_code')

        elif category == "film_general_general_manufacturer":
            if not self.temporary_standard_item_model_film_general_general_manufacturer is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_manufacturer.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_manufacturer = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_manufacturer.setObjectName('temporary_standard_item_model_film_general_general_manufacturer')

        elif category == "film_general_general_media_type":
            if not self.temporary_standard_item_model_film_general_general_media_type is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_media_type.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_media_type = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_media_type.setObjectName('temporary_standard_item_model_film_general_general_media_type')

        elif category == "film_general_general_packaging":
            if not self.temporary_standard_item_model_film_general_general_packaging is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_packaging.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_packaging = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_packaging.setObjectName('temporary_standard_item_model_film_general_general_packaging')

        elif category == "film_general_general_age_release":
            if not self.temporary_standard_item_model_film_general_general_age_release is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_age_release.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_age_release = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_age_release.setObjectName('temporary_standard_item_model_film_general_general_age_release')

        elif category == "film_general_film_resolution":
            if not self.temporary_standard_item_model_film_general_film_resolution is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_resolution.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_resolution = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_resolution.setObjectName('temporary_standard_item_model_film_general_film_resolution')

        elif category == "film_general_film_studio":
            if not self.temporary_standard_item_model_film_general_film_studio is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_studio.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_studio = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_studio.setObjectName('temporary_standard_item_model_film_general_film_studio')

        elif category == "film_general_film_distribution":
            if not self.temporary_standard_item_model_film_general_film_distribution is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_distribution.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_distribution = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_distribution.setObjectName('temporary_standard_item_model_film_general_film_distribution')

        elif category == "film_general_film_rentals":
            if not self.temporary_standard_item_model_film_general_film_rentals is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_rentals.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_rentals = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_rentals.setObjectName('temporary_standard_item_model_film_general_film_rentals')

        elif category == "film_general_film_production_company":
            if not self.temporary_standard_item_model_film_general_film_production_company is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_production_company.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_production_company = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_production_company.setObjectName('temporary_standard_item_model_film_general_film_production_company')

        elif category == "film_general_general_packaging":
            if not self.temporary_standard_item_model_film_general_film_general_packaging is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_general_packaging.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_general_packaging = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_general_packaging.setObjectName('temporary_standard_item_model_film_general_film_general_packaging')

        elif category == "film_general_general_media_type":
            if not self.temporary_standard_item_model_film_general_film_general_media_type is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_general_media_type.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_general_media_type = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_general_media_type.setObjectName('temporary_standard_item_model_film_general_film_general_media_type')

        elif category == "film_general_person_relationship":
            if not self.temporary_standard_item_model_film_general_film_person_relationship is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_person_relationship.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_person_relationship = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_person_relationship.setObjectName('temporary_standard_item_model_film_general_film_person_relationship')

        elif category == "film_general_person_relationship":
            if not self.temporary_standard_item_model_film_general_film_person_relationship_status is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_person_relationship_status.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_person_relationship_status = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_person_relationship_status.setObjectName('temporary_standard_item_model_film_general_film_person_relationship_status')

        elif category == "film_general_person_relationship":
            if not self.temporary_standard_item_model_film_general_film_person_religion is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_film_person_religion.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_film_person_religion = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_film_person_religion.setObjectName('temporary_standard_item_model_film_general_film_person_religion')

        elif category == "film_general_person_hair_colour":
            if not self.temporary_standard_item_model_film_general_person_hair_color is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_hair_color.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_hair_color = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_hair_color.setObjectName('temporary_standard_item_model_film_general_person_hair_color')

        elif category == "film_general_person_religion":
            if not self.temporary_standard_item_model_film_general_person_religion is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_religion.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_religion = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_religion.setObjectName('temporary_standard_item_model_film_general_person_religion')

        elif category == "film_general_person_eye_color":
            if not self.temporary_standard_item_model_film_general_person_eye_color is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_eye_color.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_eye_color = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_eye_color.setObjectName('temporary_standard_item_model_film_general_person_eye_color')

        elif category == "film_general_person_salutation":
            if not self.temporary_standard_item_model_film_general_person_salutation is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_salutation.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_salutation = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_salutation.setObjectName('temporary_standard_item_model_film_general_person_salutation')

        elif category == "film_general_person_title":
            if not self.temporary_standard_item_model_film_general_person_title is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_title.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_title = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_title.setObjectName('temporary_standard_item_model_film_general_person_title')

        elif category == "film_general_person_gender":
            if not self.temporary_standard_item_model_film_general_person_gender is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_gender.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_gender = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_gender.setObjectName('temporary_standard_item_model_film_general_person_gender')

        elif category == "film_general_music_genre":
            if not self.temporary_standard_item_model_film_general_music_genre is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_genre.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_genre = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_genre.setObjectName('temporary_standard_item_model_film_general_music_genre')

        elif category == "film_general_general_award":
            if not self.temporary_standard_item_model_film_general_general_award is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_award.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_award = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_award.setObjectName('temporary_standard_item_model_film_general_general_award')

        elif category == "film_general_general_country":
            print "film_general_general_country is set"
            if not self.temporary_standard_item_model_film_general_general_country is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_country.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_country = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_country.setObjectName('temporary_standard_item_model_film_general_general_country')

        elif category == "film_general_person_nationality":
            if not self.temporary_standard_item_model_film_general_person_nationality is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_nationality.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_nationality = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_nationality.setObjectName('temporary_standard_item_model_film_general_person_nationality')

        elif category == "film_general_person_relationship_status":
            if not self.temporary_standard_item_model_film_general_person_relationship_status is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_relationship_status.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_relationship_status = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_relationship_status.setObjectName('temporary_standard_item_model_film_general_person_relationship_status')

        elif category == "film_general_person_relationship":
            if not self.temporary_standard_item_model_film_general_person_relationship is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_person_relationship.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_person_relationship = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_person_relationship.setObjectName('temporary_standard_item_model_film_general_person_relationship')


        elif category == "film_general_general_language":
            if not self.temporary_standard_item_model_film_general_general_language is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_language.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_language = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_language.setObjectName('temporary_standard_item_model_film_general_general_language')

        elif category == "film_general_general_region":
            if not self.temporary_standard_item_model_film_general_general_region is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_general_region.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_general_region = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_general_region.setObjectName('temporary_standard_item_model_film_general_general_region')

        elif category == "film_general_music_studio":
            if not self.temporary_standard_item_model_film_general_music_studio is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_studio.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_studio = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_studio.setObjectName('temporary_standard_item_model_film_general_music_studio')

        elif category == "film_general_music_spars_code":
            if not self.temporary_standard_item_model_film_general_music_spars_code is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_spars_code.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_spars_code = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_spars_code.setObjectName('temporary_standard_item_model_film_general_music_spars_code')

        elif category == "film_general_music_publisher":
            if not self.temporary_standard_item_model_film_general_music_publisher is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_publisher.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_publisher = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_publisher.setObjectName('temporary_standard_item_model_film_general_music_publisher')

        elif category == "film_general_music_style":
            if not self.temporary_standard_item_model_film_general_music_style is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_style.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_style = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_style.setObjectName('temporary_standard_item_model_film_general_music_style')

        elif category == "film_general_music_label":
            if not self.temporary_standard_item_model_film_general_music_label is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_label.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_label = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_label.setObjectName('temporary_standard_item_model_film_general_music_label')

        elif category == "film_general_music_distribution":
            if not self.temporary_standard_item_model_film_general_music_distribution is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_music_distribution.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_music_distribution = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_music_distribution.setObjectName('temporary_standard_item_model_film_general_music_distribution')

        elif category == "film_general_book_genre":
            if not self.temporary_standard_item_model_film_general_book_genre is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_book_genre.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_book_genre = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_book_genre.setObjectName('temporary_standard_item_model_film_general_book_genre')

        elif category == "film_general_book_publisher":
            if not self.temporary_standard_item_model_film_general_book_publisher is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_book_publisher.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_book_publisher = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_book_publisher.setObjectName('temporary_standard_item_model_film_general_book_publisher')

        elif category == "film_general_book_binding":
            if not self.temporary_standard_item_model_film_general_book_binding is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_book_binding.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_book_binding = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_book_binding.setObjectName('temporary_standard_item_model_film_general_book_binding')

        elif category == "film_general_coin_alloy":
            if not self.temporary_standard_item_model_film_general_coin_alloy is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_alloy.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_alloy = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_alloy.setObjectName('temporary_standard_item_model_film_general_coin_alloy')

        elif category == "film_general_coin_currency":
            if not self.temporary_standard_item_model_film_general_coin_currency is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_currency.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_currency = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_currency.setObjectName('temporary_standard_item_model_film_general_coin_currency')

        elif category == "film_general_coin_type":
            if not self.temporary_standard_item_model_film_general_coin_type is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_type.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_type = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_type.setObjectName('temporary_standard_item_model_film_general_coin_type')

        elif category == "film_general_coin_manufacturing_procedure":
            if not self.temporary_standard_item_model_film_general_coin_manufacturing_procedure is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_manufacturing_procedure.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_manufacturing_procedure = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_manufacturing_procedure.setObjectName('temporary_standard_item_model_film_general_coin_manufacturing_procedure')

        elif category == "film_general_coin_degree_preservation":
            if not self.temporary_standard_item_model_film_general_coin_degree_preservation is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_degree_preservation.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_degree_preservation = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_degree_preservation.setObjectName('temporary_standard_item_model_film_general_coin_degree_preservation')

        elif category == "film_general_coin_mint":
            if not self.temporary_standard_item_model_film_general_coin_mint is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_coin_mint.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_coin_mint = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_coin_mint.setObjectName('temporary_standard_item_model_film_general_coin_mint')

        elif category == "film_general_videogame_genre":
            if not self.temporary_standard_item_model_film_general_videogame_genre is None:
                #   When model already exists only clear the model for adding new items
                self.temporary_standard_item_model_film_general_videogame_genre.clear()
            else:
                #   The model doesn't exists, create a new one
                self.temporary_standard_item_model_film_general_videogame_genre = QStandardItemModel(rows, columns, parent)
                self.temporary_standard_item_model_film_general_videogame_genre.setObjectName('temporary_standard_item_model_film_general_videogame_genre')

        return

    def set_standard_item_model(self):

        category_list = [('person_profile', 0, 4),
                         ('person_salutation', 0, 2),
                         ('person_title', 0, 2),
                         ('person_nationality', 0, 2),
                         ('person_gender', 0, 2),
                         ('person_religion', 0, 2),
                         ('person_relationship_status', 0, 2),
                         ('person_eye_color', 0, 2),
                         ('person_hair_color', 0, 2),
                         ('person_relationship', 0, 2),
                         ('music_genre', 0, 2),
                         ('music_studio', 0, 2),
                         ('music_spars_code', 0, 2),
                         ('music_publisher', 0, 2),
                         ('music_style', 0, 2),
                         ('music_label', 0, 2),
                         ('music_distribution', 0, 2),
                         ('general_region', 0, 2),
                         ('general_award', 0, 2),
                         ('general_country', 0, 2 ),
                         ('general_media_type', 0, 2),
                         ('general_manufacturer', 0, 2),
                         ('general_packaging', 0, 2),
                         ('general_age_release', 0, 2),
                         ('general_language', 0, 2),
                         ('film_genre', 0, 2),
                         ('film_production_company', 0, 2),
                         ('film_rentals', 0, 2),
                         ('film_distribution', 0, 2),
                         ('film_studio', 0, 2),
                         ('film_resolution', 0, 3),
                         ('film_regional_code', 0, 3),
                         ('film_aspect_ratio', 0, 3),
                         ('film_dimension', 0, 2),
                         ('film_edition', 0, 2),
                         ('film_version', 0, 2),
                         ('film_videonorm', 0, 2),
                         ('film_audioformat', 0, 2),
                         ('film_audio_channel', 0, 2),
                         ('film_colour_format', 0, 2),
                         ('film_videoformat', 0, 2),
                         ('film_soundsystem', 0, 2),
                         ('book_genre', 0, 2),
                         ('book_publisher', 0, 2),
                         ('book_binding', 0, 2),
                         ('coin_alloy', 0, 2),
                         ('coin_currency', 0, 2),
                         ('coin_type', 0, 2),
                         ('coin_manufacturing_procedure', 0, 3),
                         ('coin_degree_preservation', 0, 3),
                         ('coin_mint', 0, 3),
                         ('videogame_genre', 0, 2),
                         ('profile_list', 0, 2),
                         ]

        for category, rows, columns in category_list:
            self.create_standard_item_model(category = category, rows = rows, columns = columns, parent = self)

    def create_standard_item_model(self,
                                  category = None,
                                  rows = None,
                                  columns = None,
                                  parent = None):
        '''
           SUMMARY
           =======
           Create an empty model for the TreeViews' data

           This method constructs a new item model that initially
           has rows and columns, and that has the given parent.

           QTreeView needs a model to manage its data.

           PARAMETERS
           ==========
           :rows:       we except an intger for row.

           :columns:    An integer for column is required.

           :parent:     Given parent, for example self

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        if category == "person_profile":
            if not self.standard_item_model_person_profile is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_profile.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_profile = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_profile.setObjectName('standard_item_model_person_profile')

        elif category == "person_nationality":
            if not self.standard_item_model_person_nationality is None:
                #   When model already exists only clear the model for adding new items
                self.standard_item_model_person_nationality.clear()
            else:
                #   The model doesn't exists, create a new one
                self.standard_item_model_person_nationality = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_nationality.setObjectName('standard_item_model_person_nationality')

        elif category == "person_salutation":
            if not self.standard_item_model_person_salutation is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_salutation.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_salutation = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_salutation.setObjectName('standard_item_model_person_salutation')

        elif category == "person_title":
            if not self.standard_item_model_person_title is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_title.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_title = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_title.setObjectName('standard_item_model_person_title')

        elif category == "person_gender":
            if not self.standard_item_model_person_gender is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_gender.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_gender = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_gender.setObjectName('standard_item_model_person_gender')

        elif category == "person_religion":
            if not self.standard_item_model_person_religion is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_religion.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_religion = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_religion.setObjectName('standard_item_model_person_religion')

        elif category == "person_eye_color":
            if not self.standard_item_model_person_eye_color is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_eye_color.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_eye_color = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_eye_color.setObjectName('standard_item_model_person_eye_color')

        elif category == "person_hair_color":
            if not self.standard_item_model_person_hair_color is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_hair_color.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_hair_color = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_hair_color.setObjectName('standard_item_model_person_hair_color')

        elif category == "person_relationship_status":
            if not self.standard_item_model_person_relationship_status is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_relationship_status.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_relationship_status = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_relationship_status.setObjectName('standard_item_model_person_relationship_status')

        elif category == "person_relationship":
            if not self.standard_item_model_person_relationship is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_person_relationship.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_person_relationship = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_person_relationship.setObjectName('standard_item_model_person_relationship')

        elif category == "film_genre":
            if not self.standard_item_model_film_genre is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_genre.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_film_genre = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_genre.setObjectName("standard_item_model_film_genre")

        elif category == "film_general":
            if not self.standard_item_model_film_general is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_general.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_film_general = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_general.setObjectName("standard_item_model_film_general")

        elif category == "film_production_company":
            if not self.standard_item_model_film_production_company is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_production_company.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_film_production_company = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_production_company.setObjectName("standard_item_model_film_production_company")

        elif category == "film_rentals":
            if not self.standard_item_model_film_rentals is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_rentals.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_film_rentals = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_rentals.setObjectName("standard_item_model_film_rentals")

        elif category == "film_distribution":
            if not self.standard_item_model_film_distribution is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_distribution.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_film_distribution = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_distribution.setObjectName("standard_item_model_film_distribution")

        elif category == "music_genre":
            if not self.standard_item_model_music_genre is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_genre.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_music_genre = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_genre.setObjectName("standard_item_model_music_genre")

        elif category == "music_studio":
            if not self.standard_item_model_music_studio is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_studio.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_music_studio = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_studio.setObjectName("standard_item_model_music_studio")

        elif category == "music_spars_code":
            if not self.standard_item_model_music_spars_code is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_spars_code.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_music_spars_code = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_spars_code.setObjectName("standard_item_model_music_spars_code")

        elif category == "music_publisher":
            if not self.standard_item_model_music_publisher is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_publisher.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_music_publisher = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_publisher.setObjectName("standard_item_model_music_publisher")

        elif category == "music_style":
            if not self.standard_item_model_music_style is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_style.clear()
            else:
                ### The model doesn't exists, create a new one
                self.standard_item_model_music_style = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_style.setObjectName("standard_item_model_music_style")

        elif category == "music_label":
            if not self.standard_item_model_music_label is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_label.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_music_label = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_label.setObjectName("standard_item_model_music_label")

        elif category == "music_distribution":
            if not self.standard_item_model_music_distribution is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_music_distribution.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_music_distribution = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_music_distribution.setObjectName("standard_item_model_music_distribution")

        elif category == "general_award":
            if not self.standard_item_model_general_award is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_general_award.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_general_award = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_award.setObjectName("standard_item_model_general_award")

        elif category == "general_region":
            if not self.standard_item_model_general_region is None:
                #   When model already exists only clear the model for adding new items
                self.standard_item_model_general_region.clear()
            else:
                #   The model doesn't exists, create a new one
                self.standard_item_model_general_region = QStandardItemModel(rows, columns, None)
                self.standard_item_model_general_region.setObjectName("standard_item_model_general_region")

        elif category == "general_age_release":
            if not self.standard_item_model_general_age_release is None:
                #   When model already exists only clear the model for adding new items
                self.standard_item_model_general_age_release.clear()
            else:
                #   The model doesn't exists, create a new one
                self.standard_item_model_general_age_release = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_age_release.setObjectName("standard_item_model_general_age_release")

        elif category == "general_country":
            if not self.standard_item_model_general_country is None:
                #   When model already exists only clear the model for adding new items
                self.standard_item_model_general_country.clear()
            else:
                #   The model doesn't exists, create a new one
                self.standard_item_model_general_country = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_country.setObjectName("standard_item_model_general_country")

        elif category == "general_language":
            if not self.standard_item_model_general_language is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_general_language.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_general_language = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_language.setObjectName("standard_item_model_general_language")

        elif category == "general_media_type":
            if not self.standard_item_model_general_media_type is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_general_media_type.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_general_media_type = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_media_type.setObjectName("standard_item_model_general_media_type")

        elif category == "general_manufacturer":
            if not self.standard_item_model_general_manufacturer is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_general_manufacturer.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_general_manufacturer = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_manufacturer.setObjectName("standard_item_model_general_manufacturer")

        elif category == "general_packaging":
            if not self.standard_item_model_general_packaging is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_general_packaging.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_general_packaging = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_general_packaging.setObjectName("standard_item_model_general_packaging")

        elif category == "film_studio":
            if not self.standard_item_model_film_studio is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_studio.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_studio = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_studio.setObjectName("standard_item_model_film_studio")

        elif category == "film_resolution":
            if not self.standard_item_model_film_resolution is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_resolution.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_resolution = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_resolution.setObjectName("standard_item_model_film_resolution")

        elif category == "film_regional_code":
            if not self.standard_item_model_film_regional_code is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_regional_code.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_regional_code = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_regional_code.setObjectName("standard_item_model_film_regional_code")

        elif category == "film_aspect_ratio":
            if not self.standard_item_model_film_aspect_ratio is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_aspect_ratio.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_aspect_ratio = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_aspect_ratio.setObjectName("standard_item_model_film_aspect_ratio")

        elif category == "film_dimension":
            if not self.standard_item_model_film_dimension is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_dimension.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_dimension = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_dimension.setObjectName("standard_item_model_film_dimension")

        elif category == "film_edition":
            if not self.standard_item_model_film_edition is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_edition.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_edition = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_edition.setObjectName("standard_item_model_film_edition")

        elif category == "film_version":
            if not self.standard_item_model_film_version is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_version.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_version = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_version.setObjectName("standard_item_model_film_version")

        elif category == "film_videonorm":
            if not self.standard_item_model_film_videonorm is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_videonorm.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_videonorm = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_videonorm.setObjectName("standard_item_model_film_videonorm")

        elif category == "film_audioformat":
            if not self.standard_item_model_film_audioformat is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_audioformat.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_audioformat = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_audioformat.setObjectName("standard_item_model_film_audioformat")

        elif category == "film_audio_channel":
            if not self.standard_item_model_film_audio_channel is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_audio_channel.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_audio_channel = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_audio_channel.setObjectName("standard_item_model_film_audio_channel")

        elif category == "film_colour_format":
            if not self.standard_item_model_film_colour_format is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_colour_format.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_colour_format = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_colour_format.setObjectName("standard_item_model_film_colour_format")

        elif category == "film_videoformat":
            if not self.standard_item_model_film_videoformat is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_videoformat.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_videoformat = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_videoformat.setObjectName("standard_item_model_film_videoformat")

        elif category == "film_soundsystem":
            if not self.standard_item_model_film_soundsystem is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_film_soundsystem.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_film_soundsystem = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_film_soundsystem.setObjectName("standard_item_model_film_soundsystem")

        elif category == "book_genre":
            if not self.standard_item_model_book_genre is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_book_genre.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_book_genre = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_book_genre.setObjectName("standard_item_model_book_genre")

        elif category == "book_publisher":
            if not self.standard_item_model_book_publisher is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_book_publisher.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_book_publisher = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_book_publisher.setObjectName("standard_item_model_book_publisher")

        elif category == "book_binding":
            if not self.standard_item_model_book_binding is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_book_binding.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_book_binding = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_book_binding.setObjectName("standard_item_model_book_binding")

        elif category == "coin_alloy":
            if not self.standard_item_model_coin_alloy is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_alloy.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_alloy = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_alloy.setObjectName("standard_item_model_coin_alloy")

        elif category == "coin_currency":
            if not self.standard_item_model_coin_currency is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_currency.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_currency = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_currency.setObjectName("standard_item_model_coin_currency")

        elif category == "coin_type":
            if not self.standard_item_model_coin_type is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_type.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_type = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_type.setObjectName("standard_item_model_coin_type")

        elif category == "coin_manufacturing_procedure":
            if not self.standard_item_model_coin_manufacturing_procedure is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_manufacturing_procedure.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_manufacturing_procedure = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_manufacturing_procedure.setObjectName("standard_item_model_coin_manufacturing_procedure")

        elif category == "coin_degree_preservation":
            if not self.standard_item_model_coin_degree_preservation is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_degree_preservation.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_degree_preservation = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_degree_preservation.setObjectName("standard_item_model_coin_degree_preservation")

        elif category == "coin_mint":
            if not self.standard_item_model_coin_mint is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_coin_mint.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_coin_mint = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_coin_mint.setObjectName("standard_item_model_coin_mint")

        elif category == "videogame_genre":
            if not self.standard_item_model_videogame_genre is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_videogame_genre.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_videogame_genre = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_videogame_genre.setObjectName("standard_item_model_videogame_genre")

        elif category == "profile_list":
            if not self.standard_item_model_profile_list is None:
                # When model already exists only clear the model for adding new items
                self.standard_item_model_profile_list.clear()
            else:
                # The model doesn't exists, create a new one
                self.standard_item_model_profile_list = QStandardItemModel(rows, columns, parent)
                self.standard_item_model_profile_list.setObjectName("standard_item_model_profile_list")

        return

    def refresh_model(self,
                      model = None,
                      category = None):
        '''
           SUMMARY
           =======
           This method is used to refresh the model.

           PARAMETERS
           ==========
           :model (QStandardItemModel): We expect a model which should be sorted.

           :category (str):             Say what category we are using in.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Refresh the model")

        if category == "general_region":
            self.standard_item_model_general_region = model

        if category == "general_age_release":
            self.standard_item_model_general_age_release = model

        elif category == "general_country":
            self.standard_item_model_general_country = model
            self.standard_item_model_film_general_general_country = model
            self.temporary_standard_item_model_film_general_general_country = model

        elif category == "person_nationality":
            self.standard_item_model_person_nationality = model

        elif category == "film_genre":
            self.standard_item_model_film_genre = model

        elif category == "film_general":
            self.standard_item_model_film_general = model

        elif category == "film_production_company":
            self.standard_item_model_film_production_company = model

        elif category == "film_rentals":
            self.standard_item_model_film_rentals = model

        elif category == "film_distribution":
            self.standard_item_model_film_distribution = model

        elif category == "film_studio":
            self.standard_item_model_film_studio = model

        elif category == "film_regional_code":
            self.standard_item_model_film_regional_code = model

        elif category == "film_resolution":
            self.standard_item_model_film_resolution = model

        elif category == "general_award":
            self.standard_item_model_general_award = model

        elif category == "music_genre":
            self.standard_item_model_music_genre = model

        elif category == "music_studio":
            self.standard_item_model_music_studio = model

        elif category == "music_spars_code":
            self.standard_item_model_music_spars_code = model

        elif category == "music_publisher":
            self.standard_item_model_music_publisher = model

        elif category == "music_style":
            self.standard_item_model_music_style = model

        elif category == "music_label":
            self.standard_item_model_music_label = model

        elif category == "music_distribution":
            self.standard_item_model_music_distribution = model

        elif category == "person_profile":
            self.standard_item_model_person_profile = model

        elif category == "person_gender":
            self.standard_item_model_person_gender = model

        elif category == "person_salutation":
            self.standard_item_model_person_salutation = model

        elif category == "person_title":
            self.standard_item_model_person_title = model

        elif category == "person_eye_color":
            self.standard_item_model_person_eye_color = model

        elif category == "person_hair_color":
            self.standard_item_model_person_hair_color = model

        elif category == "person_religion":
            self.standard_item_model_person_religion = model

        elif category == "person_relationship_status":
            self.standard_item_model_person_relationship_status = model

        elif category == "person_relationship":
            self.standard_item_model_person_relationship = model

        elif category == "general_media_type":
            self.standard_item_model_general_media_type = model

        elif category == "general_manufacturer":
            self.standard_item_model_general_manufacturer = model

        elif category == "general_packaging":
            self.standard_item_model_general_packaging = model

        elif category == "general_packaging":
            self.standard_item_model_general_packaging = model

        elif category == "film_aspect_ratio":
            self.standard_item_model_film_aspect_ratio = model

        elif category == "film_dimension":
            self.standard_item_model_film_dimension = model

        elif category == "film_edition":
            self.standard_item_model_film_edition = model

        elif category == "film_version":
            self.standard_item_model_film_version = model

        elif category == "film_videonorm":
            self.standard_item_model_film_videonorm = model

        elif category == "film_audioformat":
            self.standard_item_model_film_audioformat = model

        elif category == "film_audio_channel":
            self.standard_item_model_film_audio_channel = model

        elif category == "film_colour_format":
            self.standard_item_model_film_colour_format = model

        elif category == "film_videoformat":
            self.standard_item_model_film_videoformat = model

        elif category == "film_soundsystem":
            self.standard_item_model_film_soundsystem = model

        elif category == "book_genre":
            self.standard_item_model_book_genre = model

        elif category == "book_publisher":
            self.standard_item_model_book_publisher = model

        elif category == "book_binding":
            self.standard_item_model_book_binding = model

        elif category == "coin_alloy":
            self.standard_item_model_coin_alloy = model

        elif category == "coin_currency":
            self.standard_item_model_coin_currency = model

        elif category == "coin_type":
            self.standard_item_model_coin_type = model

        elif category == "coin_manufacturing_procedure":
            self.standard_item_model_coin_manufacturing_procedure = model

        elif category == "coin_degree_preservation":
            self.standard_item_model_coin_degree_preservation = model

        elif category == "coin":
            self.standard_item_model_coin_mint = model

        elif category == "videogame_genre":
            self.standard_item_model_videogame_genre = model

        elif category == "profile_list":
            self.standard_item_model_profile_list = model

        return

    def clear_standard_item_model(self,
                                  certain_standard_item_model):

        certain_standard_item_model.clear()

        return

    def row_count(self,
                 model):
        '''
           SUMMARY
           =======
           This method is used to get the count of items in given model.

           PARAMETERS
           ==========
           :model (class):       We need a model, where we get the count of saved items.

           RETURNS
           =======
           :return:                 Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Get count of items in given model")

        return str(model.rowCount())

    def sort_model(self,
                   model,
                   column = 1,
                   order = Qt.AscendingOrder):
        '''
           SUMMARY
           =======
           This method is used to refresh the model.

           PARAMETERS
           ==========
           :model (QStandardItemModel): We expect a model which should be sorted.

           :category (str):             Say what category we are using in.

           RETURNS
           =======
           :return:                     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        logger.info("Sort the given model")

        return model.sort(column, order)
