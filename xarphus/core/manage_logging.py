import logging

def configure_logging(log_file_path, reconfigure_logging_handler = False):

    if not reconfigure_logging_handler:

        logging.basicConfig(
             filename = log_file_path,
             level = logging.INFO,
             format = '%(asctime)s - '
                      #'%(filename)s - '
                      '%(funcName)s() - '
                      '%(lineno)s - '
                      '%(name)s - '
                      '%(levelname)s - '
                      '%(message)s',
                      datefmt='%Y-%m-%d-%H:%M:%S')
    else:

          remove_logging_handler(log_file_path = log_file_path)

    return

def remove_logging_handler(log_file_path):
    # Remove all root logger handlers and reconfigure
    # Remove all handlers associated with the root logger object.
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    # Reconfigure logging again, this time with a file.
    configure_logging(log_file_path = log_file_path)
