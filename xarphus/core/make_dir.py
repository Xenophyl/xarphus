import os
import errno

def list_top_sub_folders():

    profile_path = os.path.abspath(os.path.join('profile'))

    #images_path = os.path.abspath(os.path.join(profile_path, 'images'))

    setting_path = os.path.abspath(os.path.join(profile_path, 'settings'))

    images_movie_path = os.path.abspath(os.path.join(profile_path, 'images', 'movie'))
    images_movie_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'movie', 'original'))
    images_movie_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'movie', 'thumbnail'))

    images_series_path = os.path.abspath(os.path.join(profile_path, 'images', 'series'))
    images_series_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'series', 'original'))
    images_series_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'series', 'thumbnail'))

    images_music_path = os.path.abspath(os.path.join(profile_path, 'images', 'music'))
    images_music_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'music', 'original'))
    images_music_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'music', 'thumbnail'))

    images_record_path = os.path.abspath(os.path.join(profile_path, 'images', 'record'))
    images_record_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'record', 'original'))
    images_record_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'record', 'thumbnail'))

    images_addessbook_path = os.path.abspath(os.path.join(profile_path, 'images', 'addessbook'))
    images_addessbook_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'addessbook', 'original'))
    images_addessbook_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'addessbook', 'thumbnail'))

    images_person_path = os.path.abspath(os.path.join(profile_path, 'images', 'person'))
    images_person_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'person', 'original'))
    images_person_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'person', 'thumbnail'))

    images_book_path = os.path.abspath(os.path.join(profile_path, 'images', 'book'))
    images_book_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'book', 'original'))
    images_book_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'book', 'thumbnail'))

    images_comic_path = os.path.abspath(os.path.join(profile_path, 'images', 'comic'))
    images_comic_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'comic', 'original'))
    images_comic_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'comic', 'thumbnail'))

    images_news_path = os.path.abspath(os.path.join(profile_path, 'images', 'news'))
    images_news_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'news', 'original'))
    images_news_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'news', 'thumbnail'))

    images_magazine_path = os.path.abspath(os.path.join(profile_path, 'images', 'magazine'))
    images_magazine_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'magazine', 'original'))
    images_magazine_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'magazine', 'thumbnail'))

    images_coin_path = os.path.abspath(os.path.join(profile_path, 'images', 'coin'))
    images_coin_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'coin', 'original'))
    images_coin_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'coin', 'thumbnail'))

    images_videogame_path = os.path.abspath(os.path.join(profile_path, 'images', 'videogame'))
    images_videogame_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'videogame', 'original'))
    images_videogame_test_path = os.path.abspath(os.path.join(profile_path, 'images', 'videogame', 'test'))
    images_videogame_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'videogame', 'thumbnail'))

    images_stamp_path = os.path.abspath(os.path.join(profile_path, 'images', 'stamp'))
    images_stamp_original_path = os.path.abspath(os.path.join(profile_path, 'images', 'stamp', 'original'))
    images_stamp_thumbnail_path = os.path.abspath(os.path.join(profile_path, 'images', 'stamp', 'thumbnail'))

    my_list = [profile_path,
               settings_path,
               #images_path,
               images_movie_path,
               images_movie_original_path,
               images_movie_thumbnail_path,
               images_series_path,
               images_series_original_path,
               images_series_thumbnail_path,
               images_music_path,
               images_music_original_path,
               images_music_thumbnail_path,
               images_record_path,
               images_record_original_path,
               images_record_thumbnail_path,
               images_addessbook_path,
               images_addessbook_original_path,
               images_addessbook_thumbnail_path,
               images_person_path,
               images_person_original_path,
               images_person_thumbnail_path,
               images_book_path,
               images_book_original_path,
               images_book_thumbnail_path,
               images_comic_path,
               images_comic_original_path,
               images_comic_thumbnail_path,
               images_news_path,
               images_news_original_path,
               images_news_thumbnail_path,
               images_magazine_path,
               images_magazine_original_path,
               images_magazine_thumbnail_path,
               images_coin_path,
               images_coin_original_path,
               images_coin_thumbnail_path,
               images_videogame_path,
               images_videogame_original_path,
               images_videogame_test_path,
               images_videogame_thumbnail_path,
               images_stamp_path,
               images_stamp_original_path,
               images_stamp_thumbnail_path]

    return my_list

def mkdir_top_sub_folder():
    path = list_top_sub_folders()
    for i in path:
        try:
            os.makedirs(i)
        except os.error, e:
            print "Ausgabe. Error_Number", e.errno
            if e.errno != errno.EEXIST:
                raise

def mkdir_top_level_folder(path):
    try:
        os.makedirs(path)
    except os.error, e:
        print "Ausgabe. Error_Number", e.errno
        if e.errno != errno.EEXIST:
            raise

