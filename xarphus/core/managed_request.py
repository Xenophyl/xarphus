#!/usr/bin/env python
#-*- coding:utf-8 -*-

from requests import session
from requests.auth import HTTPBasicAuth, HTTPDigestAuth

def connect_server(HTTPAuth,
                    url,
                    http_auth_user_name,
                    http_auth_pwd,
                    time_out,
                    stream = True):

    if HTTPAuth == 'HTTPBasicAuth':
        HTTPAuth = HTTPBasicAuth(http_auth_user_name, http_auth_pwd)
    elif HTTPAuth == 'HTTPDigestAuth':
        HTTPAuth = HTTPDigestAuth(http_auth_user_name, http_auth_pwd)

    request_session = session()
    status_response = request_session.get(url = url, stream = True, auth = HTTPAuth, timeout = time_out)
    status_response.raise_for_status()

    #   We you want requests to raise an exception for error code
    return HTTPAuth, request_session, status_response.status_code
