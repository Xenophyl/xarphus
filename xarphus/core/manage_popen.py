#!/usr/bin/env python
#-*- coding:utf-8 -*-

import subprocess
from subprocess import CalledProcessError, check_output

def execute_exe_file(executed_file,
                     input_path,
                     output_path,
                     config_file_path,
                     exe_file_path):
    
    executed_file = executed_file
    input_path = input_path
    input_arg = "-i" + input_path
    output_path = output_path
    output_arg = "-o" + output_path
    config_file_path = config_file_path
    sys_arg_path = "-s" + config_file_path
    exe_path = exe_file_path
    arg_exe = "-e" + exe_path

    DETACHED_PROCESS = 0x00000008
    CREATE_NO_WINDOW = 0x08000000

    #check_output([executed_file, input_arg, output_arg, sys_arg_path], shell=True, stderr=subprocess.STDOUT)
    subprocess.Popen([executed_file, input_arg, output_arg, sys_arg_path, arg_exe], shell=True)

    return
