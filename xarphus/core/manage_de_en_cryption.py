#!/usr/bin/env python

from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto import Random

import errno
from sys import exit

from os import path, remove
from hashlib import sha256

BASE_PATH = path.dirname(path.abspath(__file__))

class EncryptionDecryption(object):

    def read_in_chunks(self, file_object=None, chunk_size=65536):
        """Lazy function (generator) to read a file piece by piece.
        Default chunk size: 65536."""
        while True:
            data = file_object.read(chunk_size)
            if not data:
                break
            yield data
    
    def sha256_checksum(self, file_name=None, chunksize=65536, file_mode='rb'):
        '''
            NOTE:
            ======
            It calculates the SHA256 digest of the file.

            PARAMETERS:
            ===========
            :file_name      -   Name of the input file. Here we get a path
                                of the file.
            
            :chunksize      -   Sets the size of the chunk.
            
            :file_mode      -   By default the file is opened in rb mode, which means that you are going
                                to read the file in binary mode. This is because the SHA256 function needs
                                to read the file as a sequence of bytes. This will make sure that
                                you can hash any type of file, not only text files.
            
            :return         -   Return the digest as a string of double length,
                                containing only hexadecimal digits.
        '''
        sha_256 = sha256()
        
        with open(file_name, file_mode) as file_obj:

            for block in self.read_in_chunks(file_object=file_obj, chunk_size=chunksize):

                sha_256.update(block)
                
        return sha_256.hexdigest()

    def file_size(self, file_path=None, width=16):
        '''
            NOTE:
            ======
            The method returns a padded string.


            PARAMETERS:
            ===========
            :width      -   A string is filled from the left with zeros
                            up to the length "width". The string is not truncated.

            :return     -   Return the flled string.
        '''
        return self.zero_fill(str(path.getsize(file_path)), width=width)

    def zero_fill(self, string=None, width=None):
        '''
            NOTE:
            ======
            The method pads the given string on the left with zeros to fill width.


            PARAMETERS:
            ===========
            :string     -   We need a string to pad it.
            
            :width      -   A string is filled from the left with zeros
                            up to the length "width". The string is not truncated.

            :return     -   Return the flled string.
        '''
        
        return string.zfill(width)

    def encrypt(self, key=None,
                file_name_to_encrypt=None,
                create_crypted_file=None,
                chunksize = 64 * 1024):

        '''
            NOTE:
            ======
            Encrypts a file using AES (CBC mode) with the given key.


            PARAMETERS:
            ===========
            :key                    -   The encryption key - a string that must be
                                        either 16, 24 or 32 bytes long. Longer keys
                                        are more secure.

            :file_name_to_encrypt   -   Name of the input file. file path we wish to encrypt
                                        including filename.


            :create_crypted_file    -   Name of the output file. Path for encrypt
                                        file to write. file path for encrypted file
                                        included filename.

            :chunksize              -   Sets the size of the chunk which the function
                                        uses to read and encrypt the file. Larger chunk
                                        sizes can be faster for some files and machines.
                                        chunksize must be divisible by 16.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''

        key = SHA256.new(key).digest()        
        filesize = self.file_size(file_path=file_name_to_encrypt)

        #   The initialization vector (IV) is an important part of block
        #   encryption algorithms that work in chained modes like CBC.
        #   The IV is as important as the salt in hashed passwords,
        #   and the lack of correct IV usage led to the cracking of
        #   the WEP encryption for wireless LAN.
        #   In this case PyCrypto allows one to pass an IV into
        #   the AES.new creator function.
        #   For maximal security, the IV should be randomly
        #   generated for every new encryption and can be
        #   stored together with the ciphertext.
        #   Knowledge of the IV won't help the attacker crack your encryption.
        #   What can help him, however, is your reusing the same IV
        #   with the same encryption key for multiple encryptions.
        IV = Random.new().read(AES.block_size )

        
        encryptor = AES.new(key, AES.MODE_CBC, IV)

        with open(file_name_to_encrypt, "rb") as input_file:
            with open(create_crypted_file, "wb") as output_file:
                output_file.write(filesize)
                output_file.write(IV)
                while True:
                        chunk = input_file.read(chunksize)
                        
                        if len(chunk) == 0:
                                break

                        elif len(chunk) % 16 !=0:
                            chunk += ' ' *  (16 - (len(chunk) % 16))

                        output_file.write(encryptor.encrypt(chunk))
                        
        self.write_in_file(create_crypted_file, self.sha256_checksum(file_name=file_name_to_encrypt))

        return

    def decrypt(self, key=None,
                file_name_to_decrypt=None,
                create_crypted_file=None,
                chunksize = 64 * 1024):

        '''
            NOTE:
            ======
            Encrypts a file using AES (CBC mode) with the given key.


            PARAMETERS:
            ===========
            :key                    -   The encryption key - a string that must be
                                        either 16, 24 or 32 bytes long. Longer keys
                                        are more secure.

            :file_name_to_decrypt   -   Name of the input file. file path we wish to decrypt
                                        including filename.


            :create_crypted_file    -   Name of the output file. Path for encrypt
                                        file to write. file path for encrypted file
                                        included filename.

            :chunksize              -   Sets the size of the chunk which the function
                                        uses to read and encrypt the file. Larger chunk
                                        sizes can be faster for some files and machines.
                                        chunksize must be divisible by 16.

            :return                 -   Nothing is returned. The statement 'return'
                                        terminates a function. That makes sure that the
                                        function is definitely finished.
        '''
        key = SHA256.new(key).digest()
       
        with open(file_name_to_decrypt, "rb") as input_file:
                filesize = input_file.read(16)
                IV = input_file.read(16)

                decryptor = AES.new(key, AES.MODE_CBC, IV)
                
                with open(create_crypted_file, "wb") as output_file:
                        while True:
                                chunk = input_file.read(chunksize)
                                if len(chunk) == 0:
                                        break

                                output_file.write(decryptor.decrypt(chunk))

                        output_file.truncate(int(filesize))

        return self.compare_sha256_sums(decrypted_file=create_crypted_file, sha_value=self.sha256_checksum(file_name=create_crypted_file), encrypted_file=file_name_to_decrypt)


    def write_in_file(self, file_name, sha256_value):
        '''
            NOTE:
            ======
            This method writes in the existing file by given path of the file.
            In this case. We add data (here: sha256_value) always to the end
            of the file.

            PARAMETERS:
            ===========
            :sha256_value  -    Its just a value. This value should be saved
                                in the existing file. 
            
            :return         -   Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.

        '''
        
        with open(file_name, 'a+') as f:
            f.write(sha256_value)

        return

    def read_in_file(self, file_name):
        '''
            NOTE:
            ======
            This method reads the existing file by given path of the file.
            In this case. We move the cursor to the -64 character from the end of the file,
            because 2 means the reference point is the end of the file.

            PARAMETERS:
            ===========
            :file_path  -   We need the path to the existing file to read it. 
            
            :return     -   We return the read content.

        '''
        
        with open(file_name, 'r') as f:
            f.seek(-64,2)
            return f.read()

    def remove_file(self, file_path=None):
        '''
            NOTE:
            ======
            This method deletes the existing file by given path of the file.

            PARAMETERS:
            ===========
            :file_path  -   We need the path to the existing file to delete it. 
            
            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.

        '''
        
        remove(file_path)

        return

    def compare_sha256_sums(self,
                             encrypted_file=None,
                             decrypted_file=None,
                             sha_value=None):
        '''
            NOTE:
            ======
            This method comapres two SHA256 values. When

            If comparison wasn't successful, the decrypted file is deleted.
            Because if the decryption fails, you can not open the file. That means
            we don't need the file no longer.

            PARAMETERS:
            ===========
            :encrypted_file     -   We need a path to an existing encrypted file. 

            :decrypted_file     -   We except a path to decrypted file for
                                    removing it later, when the decryption failed.

            :sha_value          -   We get a value of Sha256. The value SHA256 was
                                    computed by the decrypted file. 
            
            :return             -   Nothing is returned. The statement 'return'
                                    terminates a function. That makes sure that the
                                    function is definitely finished.

        '''
        

        result_read = self.read_in_file(encrypted_file)

        result_decrypt = sha_value

        if not result_read == result_decrypt:
            
            self.remove_file(file_path=decrypted_file)

            return False
        
        else:

            return True
    
def de_crypt():
    
    encrypt_decrypt = EncryptionDecryption()
    
    ################################################
    #   Decryption File
    ################################################

    file_name_to_encrypt = raw_input('Enter file name to decrypt: ')

    create_crypted_file = raw_input('Enter new file name: ')

    password = raw_input('Enter new password: ')

    try:

        result = encrypt_decrypt.decrypt(key=password,
                                file_name_to_decrypt=file_name_to_encrypt,
                                create_crypted_file=create_crypted_file)

        if result:

            print "The file was decrypted successfully"

            print "Would you like to delete the original encrypted file?"
            answer = raw_input('(Y)es or (N)o?: ' )

            converted_anwser = answer.lower()

            if converted_anwser == 'y':

                try:
                    encrypt_decrypt.remove_file(file_path=file_name_to_encrypt)
                    print "Done! The original encrypted file? was deleted successfully."
                except OSError as e: 
                    if e.errno != errno.ENOENT: #   errno.ENOENT = no such file or directory
                        print "No such file or directory!"
                        raise # re-raise exception if a different error occurred
                    
        else:

            print "Error. Decryption failed. Wrong password."
            
    except IOError:
        print "File doesn't appear to exist."

def en_crypt():

    encrypt_decrypt = EncryptionDecryption()

    ################################################
    #   Encryption File
    ################################################

    file_name_to_encrypt = raw_input('Enter file name to encrypt: ')

    create_crypted_file = raw_input('Enter new file name: ')

    print "\nWARNING: There is absolutley no way to reset your password. So make sure you remember it! "
    password = raw_input('Enter new password: ')

    try:

        encrypt_decrypt.encrypt(key=password,
                                file_name_to_encrypt=file_name_to_encrypt,
                                create_crypted_file=create_crypted_file)

    except IOError:
        print "File doesn't appear to exist."

    except WindowsError:
        print "File doesn't appear to exist."
        
if __name__ == '__main__':

    while True:

        try:

            choice = raw_input("(E)ncryption or (D)ecryption or (Ex)it?: ")

            choice_low = choice.lower()
            
            crypt_func_dict = {'d': de_crypt,
                               'e': en_crypt,
                               'ex': exit}

            crypt_func_dict[choice_low]()

        except KeyError:
            print "The function doesn't exist! Try again!"
