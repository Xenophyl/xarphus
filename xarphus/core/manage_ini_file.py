#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

FILE_NAME = __name__

import configparser
from cStringIO import StringIO
import os
import codecs

from io import open as io_open

try:
    from configparser import SafeConfigParser, DuplicateSectionError
except ImportError:
    from ConfigParser import SafeConfigParser, DuplicateSectionError  # ver. < 3.0

class DefaultINIFile(object):
    '''Doc - Inside Class '''
    def __init__(self):
        '''Doc - __init__ Constructor'''

        self.DEFAULT_INI = """; Configuration file for xarphus program. \
        \n; Don't add custom comments, file is often overwrote! \
        \n; ------------------- \
        \n\n[General_Configuration]
current_profile = None
language = german_sie
language_id = 1
database_type = SQLite
ask_befor_close = True
ask_befor_delete = True
dir_path_temp_folder = D:\\Dan\\Python\\xarphus\\temp
url_update = http://www.web.de
time_out_update = 5
time_out_version_history = 5
url_version = http://www.que.de
last_folder_path_remeber = None
visible_tool_bar = True
visible_status_bar = True

[General_Design]
gui_style = automatically
gui_style_id = 0
scheme_color = default
scheme_color_id = 0

[Category_Manager]
visible_movie = True
visible_book = True
visible_music = True
visible_coin = True
visible_person = True
visible_contact = True
visible_note = True
visible_video_game = True
visible_quotation = True
visible_stamp = True
visible_wine = True
visible_rent = True
                            """

    def user_ini(self, dict_custom_config):
        USER_INI ="""; Configuration file for xarphus program. \
        \n; Don't add custom comments, file is often overwrote! \
        \n; -------------------\
        \n\n[General_Configuration]
current_profile = """ + dict_custom_config.dict_set_general_settings["current_profile"] + """
language = """ + dict_custom_config.dict_set_general_settings["language"] + """
language_id = """ + dict_custom_config.dict_set_general_settings["language_id"] + """
database_type = """ + dict_custom_config.dict_set_general_settings["Database"] + """
ask_befor_close = """ + dict_custom_config.dict_set_general_settings["AskBeforClose"] + """
ask_befor_delete = """ + dict_custom_config.dict_set_general_settings["AskBeforDelete"] + """
dir_path_temp_folder = """ + dict_custom_config.dict_set_general_settings["PathToTempFolder"] + """
url_update = """ + dict_custom_config.dict_set_general_settings["URLServerUpdate"] + """
time_out_update = """ + dict_custom_config.dict_set_general_settings["TimeOutUpdate"] + """
time_out_version_history = """ + dict_custom_config.dict_set_general_settings["TimeOutGetVersionHistory"] + """
url_version = """ + dict_custom_config.dict_set_general_settings["URLServerVersion"] + """
last_folder_path_remeber = """ + dict_custom_config.dict_set_general_settings["LastPathFolderDialog"] + """
visible_tool_bar = """ + dict_custom_config.dict_set_general_settings["VisibleToolBar"] + """
visible_status_bar = """ + dict_custom_config.dict_set_general_settings["VisibleStatusBar"] + """

[General_Design]
gui_style = """ + dict_custom_config.dict_set_general_settings["GUIStyle"] + """
gui_style_id = """ + dict_custom_config.dict_set_general_settings["GUIStiyleID"] + """
scheme_color = """ + dict_custom_config.dict_set_general_settings["SchemeColor"] + """
scheme_color_id = """ + dict_custom_config.dict_set_general_settings["SchemeColorID"] + """

[Category_Manager]
visible_movie = """ + dict_custom_config.dict_set_general_settings["VisibleMovie"] + """
visible_book = """ + dict_custom_config.dict_set_general_settings["VisibleBook"] + """
visible_music = """ + dict_custom_config.dict_set_general_settings["VisibleMusic"] + """
visible_coin = """ + dict_custom_config.dict_set_general_settings["VisibleCoin"] + """
visible_person = """ + dict_custom_config.dict_set_general_settings["VisiblePerson"] + """
visible_contact = """ + dict_custom_config.dict_set_general_settings["VisibleContact"] + """
visible_note = """ + dict_custom_config.dict_set_general_settings["VisibleNote"] + """
visible_video_game = """ + dict_custom_config.dict_set_general_settings["VisibleVideoGame"] + """
visible_quotation = """ + dict_custom_config.dict_set_general_settings["VisibleQuotation"] + """
visible_stamp = """ + dict_custom_config.dict_set_general_settings["VisibleStamp"] + """
visible_wine = """ + dict_custom_config.dict_set_general_settings["VisibleWine"] + """
visible_rent = """ + dict_custom_config.dict_set_general_settings["VisibleRent"] + """ """



        return USER_INI

def generate_constants_sections_options(operate_mod=None):
    dict_sections = {"SECTION_GENERAL": "General_Configuration",
                     "SECTION_GUI": "General_Design",
                     "SECTION_CATEGORY_MANAGER": "Category_Manager",
    }

    dict_options = {"OPTION_CURRENT_PROFILE": "current_profile",
                    "OPTION_LANGUAGE": "language",
                    "OPTION_LANGUAGE_ID": "language_id",
                    "OPTION_DATABASE_TYPE": "database_type",
                    "OPTION_ASK_BEFORE_CLOSE": "ask_befor_close",
                    "OPTION_ASK_BEFORE_DELETE": "ask_befor_delete",
                    "OPTION_DIR_PATH_TEMP_FOLDER": "dir_path_temp_folder",
                    "OPTION_URL_UPDATE": "url_update",
                    "OPTION_TIME_OUT_UPDATE": "time_out_update",
                    "OPTION_TIME_OUT_VERSIONHISTORY": "time_out_version_history",
                    "OPTION_URL_VERSION": "url_version",
                    "OPTION_LAST_FOLDER_PATH_REMEMBER": "last_folder_path_remeber",
                    "OPTION_LOG_FOLDER_PATH": "log_folder_path",
                    "OPTION_VISIBLE_TOOL_BAR": "visible_tool_bar",
                    "OPTION_VISIBLE_MOVIE": "visible_movie",
                    "OPTION_VISIBLE_BOOK": "visible_book",
                    "OPTION_VISIBLE_MUSIC": "visible_music",
                    "OPTION_VISIBLE_COIN": "visible_coin",
                    "OPTION_VISIBLE_PERSON": "visible_person",
                    "OPTION_VISIBLE_CONTACT": "visible_contact",
                    "OPTION_VISIBLE_NOTE": "visible_note",
                    "OPTION_VISIBLE_VIDEO_GAME": "visible_video_game",
                    "OPTION_VISIBLE_QUOTATION": "visible_quotation",
                    "OPTION_VISIBLE_STAMP": "visible_stamp",
                    "OPTION_VISIBLE_WINE": "visible_wine",
                    "OPTION_VISIBLE_RENT": "visible_rent",
                    "OPTION_VISIBLE_STATUS_BAR": "visible_status_bar",
                    "OPTION_GUI_STYLE": "gui_style",
                    "OPTION_GUI_STYLE_ID": "gui_style_id",
                    "OPTION_SCHEME_COLOR": "scheme_color",
                    "OPTION_SCHEME_COLOR_ID": "scheme_color_id"
    }


    if operate_mod == "sections":
        return dict_sections

    if operate_mod == "options":
        return dict_options

def read_configuration(filename,
                       default_ini_file,
                       read_only_default = False):

    parser = configparser.SafeConfigParser()

    if read_only_default:
        parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
    else:
        parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
        parser.read(filename)
    return parser

def write_in_configuration(config_parser,
                           file_name):
    '''
       SUMMARY
       =======
       In this method, an given ini file is read in and, if desired, a file-like object.

       PARAMETERS
       ==========
       :config_parser (class):          We get the SafeConfigParser object to work with it..

       :file_name (str):                A path to the ini file.

       RETURNS
       =======
       :return:                         We return the SafeConfigParser object
    '''
    logger.info("Write in the ini file")

##    with open(file_name, 'w') as configfile:
##        config_parser.write(configfile)

    with io_open(file_name, 'w', encoding='utf8') as config_file:
        config_parser.write(config_file)

    return

def set_default_in_configuration(config_parser, section, section_value, value):

    try:
        config_parser.add_section(section)

    except DuplicateSectionError:
        pass

    finally:
        config_parser.set(section, section_value, value)

    return config_parser

def read_in_configuration(config_parser,
                          file_name,
                          default_ini_file = None,
                          read_only_ini_default = False):
    '''
       SUMMARY
       =======
       In this method, an given ini file is read in and, if desired, a file-like object.

       PARAMETERS
       ==========
       :config_parser (class):          We get the SafeConfigParser object to work with it..

       :file_name (str):                A path to the ini file.

       :default_ini_file (str):         Its a file like file object.

       :read_only_ini_default (bool):   When the user decides to read only the given ini file, then its True,
                                        otherwise its False by default.

       RETURNS
       =======
       :return:                         We return the SafeConfigParser object
    '''
    logger.info("Read in the ini file")

    # if read_only_ini_default:
    #     #   We just need the defaults for ini file
    #     config_parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
    # else:
    #     #   We need two of them: default and user settings for comparing
    #     #   First, we read the defaults, which is saved in a
    #     #   file like object
    #     config_parser.readfp(StringIO(default_ini_file.DEFAULT_INI))
    #     #   Second we read the given file object
    config_parser.read(file_name)

    return config_parser

def read_configuration(config_parser, filename, default_ini_file):
    parser = config_parser
    parser.readfp(StringIO(unicode(default_ini_file)))
    parser.read(filename)
    return parser


def fill_missing_value_option(configuration, sec, opt, set_get_settings):

    #   The structure of the contents in dictionary (dict_sec_opt) is:
    #   Key: Name of current options in ini file.
    #   Value: Name of keys in dictionary of core/confiy.py
    dict_sec_opt = {"current_profile": "current_profile",
                    "language": "language",
                    "language_id": "language_id",
                    "database_type": "Database",
                    "ask_befor_close": "AskBeforClose",
                    "ask_befor_delete":"AskBeforDelete",
                    "dir_path_temp_folder": "PathToTempFolder",
                    "url_update": "URLServerUpdate",
                    "time_out_update": "TimeOutUpdate",
                    "time_out_version_history": "TimeOutGetVersionHistory",
                    "url_version": "URLServerVersion",
                    "last_folder_path_remeber": "LastPathFolderDialog",
                    "visible_tool_bar": "VisibleToolBar",
                    "visible_status_bar": "VisibleStatusBar",
                    "gui_style": "GUIStyle",
                    "gui_style_id": "GUIStiyleID",
                    "scheme_color": "SchemeColor",
                    "scheme_color_id": "SchemeColorID",
                    "visible_movie": "VisibleMovie",
                    "visible_book": "VisibleBook",
                    "visible_music": "VisibleMusic",
                    "visible_coin": "VisibleCoin",
                    "visible_person": "VisiblePerson",
                    "visible_contact": "VisibleContact",
                    "visible_note": "VisibleNote",
                    "visible_video_game": "VisibleVideoGame",
                    "visible_quotation": "VisibleQuotation",
                    "visible_stamp": "VisibleStamp",
                    "visible_wine": "VisibleWine",
                    "visible_rent": "VisibleRent",
                    }

    #   In I/O-String-File named (DEFAULT_INI) there is a simulated ini-file.
    #   We take from DEFAULT_INI the values of the appropriate options.
    standard_value = configuration.get(sec, opt)

    #   In the variable its saved the original key
    orig_key = dict_sec_opt[opt]

    #   We take it together. The new value (standard_value) from I/O-String will save in the
    #   dictionary of core/confiy.py
    new = set_get_settings.dict_set_general_settings[orig_key] = standard_value

    return

def check_missing_options_value(file_name, default_ini_file, set_get_settings):
    dict_sec_opt = {}

    parser = configparser.SafeConfigParser()
    parser.read([file_name])

    #   We open and read the I/O string and save the analysed content in the variable (configuration)
    configuration = read_configuration(file_name, default_ini_file, read_only_default=True)

    #   Lets us  iterate over all sections
    for section in parser.sections():
        #   While its iterates over all section,
        #   its  iterates over all options that is tied to section.
        for option in parser.options(section):
            #   Save the value of current option in the variable (value)
            value = parser.get(section,option)
            #   Check if the variable (value) is empty,
            #   that means the option doesn't have a value.
            if value == "":
                #   When the variable (value) is emtpy,
                #   than call the function (fill_missing_values)
                    fill_missing_value_option(configuration, section, option, set_get_settings)

    return

def get_configuration(path_config, set_get_settings, default_ini_file, read_only_default=None):

    configuration = read_configuration(path_config, default_ini_file, read_only_default)

    result_sections = generate_constants_sections_options(operate_mod="sections")

    result_options = generate_constants_sections_options(operate_mod="options")

    '''
    Section for generally configuration
    '''
    set_get_settings.dict_set_general_settings["current_profile"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_CURRENT_PROFILE"]))

    set_get_settings.dict_set_general_settings["language"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LANGUAGE"]))

    set_get_settings.dict_set_general_settings["language_id"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LANGUAGE_ID"]))

    set_get_settings.dict_set_general_settings["Database"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_DATABASE_TYPE"]))

    set_get_settings.dict_set_general_settings["AskBeforClose"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_ASK_BEFORE_CLOSE"]))

    set_get_settings.dict_set_general_settings["AskBeforDelete"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_ASK_BEFORE_DELETE"]))

    set_get_settings.dict_set_general_settings["PathToTempFolder"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_DIR_PATH_TEMP_FOLDER"]))

    set_get_settings.dict_set_general_settings["URLServerUpdate"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_URL_UPDATE"]))

    set_get_settings.dict_set_general_settings["TimeOutUpdate"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_TIME_OUT_UPDATE"]))

    set_get_settings.dict_set_general_settings["TimeOutGetVersionHistory"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_TIME_OUT_VERSIONHISTORY"]))

    set_get_settings.dict_set_general_settings["URLServerVersion"] =  configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_URL_VERSION"]))

    set_get_settings.dict_set_general_settings["LastPathFolderDialog"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_LAST_FOLDER_PATH_REMEMBER"]))

    set_get_settings.dict_set_general_settings["VisibleToolBar"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_VISIBLE_TOOL_BAR"]))

    set_get_settings.dict_set_general_settings["VisibleStatusBar"] = configuration.get(result_sections["SECTION_GENERAL"],
                                                        (result_options["OPTION_VISIBLE_STATUS_BAR"]))


    '''
    Section for designing gui
    '''
    set_get_settings.dict_set_general_settings["GUIStyle"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_GUI_STYLE"]))

    set_get_settings.dict_set_general_settings["GUIStiyleID"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_GUI_STYLE_ID"]))

    set_get_settings.dict_set_general_settings["SchemeColor"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_SCHEME_COLOR"]))

    set_get_settings.dict_set_general_settings["SchemeColorID"] = configuration.get(result_sections["SECTION_GUI"],
                                                        (result_options["OPTION_SCHEME_COLOR_ID"]))

    '''
    Section for viewing category
    '''
    set_get_settings.dict_set_general_settings["VisibleMovie"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_MOVIE"]))

    set_get_settings.dict_set_general_settings["VisiblePerson"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_PERSON"]))

    set_get_settings.dict_set_general_settings["VisibleBook"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_BOOK"]))

    set_get_settings.dict_set_general_settings["VisibleNote"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_NOTE"]))

    set_get_settings.dict_set_general_settings["VisibleStamp"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_STAMP"]))

    set_get_settings.dict_set_general_settings["VisibleRent"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_RENT"]))

    set_get_settings.dict_set_general_settings["VisibleMusic"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_MUSIC"]))

    set_get_settings.dict_set_general_settings["VisibleQuotation"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_QUOTATION"]))

    set_get_settings.dict_set_general_settings["VisibleVideoGame"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_VIDEO_GAME"]))

    set_get_settings.dict_set_general_settings["VisibleWine"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_WINE"]))

    set_get_settings.dict_set_general_settings["VisibleCoin"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_COIN"]))

    set_get_settings.dict_set_general_settings["VisibleContact"] = configuration.get(result_sections["SECTION_CATEGORY_MANAGER"],
                                                        (result_options["OPTION_VISIBLE_CONTACT"]))

    check_missing_options_value(path_config, default_ini_file, set_get_settings)

    return

def read_conf_file(file_path=None,
                   file_mode=None):

    with open(file_path, file_mode) as file_obj:
        return file_obj.read()

def create_write_conf_file(file_path=None,
                           content=None,
                           file_mode=None):

    with open(file_path, file_mode) as file_obj:
        file_obj.write(content)

def create_write_ini_file(dict_custom_config, default_ini_file):
    '''
    This function will create a config-ini-file with sections followed by options
    that will contain some value. Also note that when it writes the config to disk,
    it uses the “w” flag to write it. This is not required, because
    for text files its enough to use plain 'w'. But it’s how the official documentation example does it.

    NOTICE:
    '''
    #parser = configparser.SafeConfigParser()
    try:
        with open(dict_custom_config.dict_set_general_settings["PathToINISetting"], "w") as config_file:
        #with codecs.open('dialog.ini', 'w', encoding='utf-8-sig') as config_file:
            config_file.write(str(StringIO(default_ini_file.user_ini(dict_custom_config)).read()))
    except Exception as err:
        print "err", err

def file_is_empty(file_object):
    return os.stat(file_object).st_size==0


##def copyLargeFile(src, dest, buffer_size=16000):
##    with open(src, 'rb') as fsrc:
##        with open(dest, 'wb') as fdest:
##            shutil.copyfileobj(fsrc, fdest, buffer_size)
def main():
    my_dict = {
               "General_Configuration": [
                                         ('current_profile', 'None'),
                                         ('language', 'german_sie'),
                                         ('language_id', '1'),
                                         ('database_type', 'SQLite'),
                                         ('ask_befor_close', 'True'),
                                         ('ask_befor_delete', 'True'),
                                         ('dir_path_temp_folder', 'D:\\Dan\\Python\\xarphus\\temp'),
                                         ('url_update', 'http://www.web.de'),
                                         ('time_out_update', '5'),
                                         ('time_out_version_history','5'),
                                         ('url_version', 'http://www.que.de'),
                                         ('last_folder_path_remeber', 'None'),
                                         ('visible_tool_bar', 'True'),
                                         ('visible_status_bar', 'True')
                                        ],
               "General_Design":        [
                                         ('gui_style', 'automatically'),
                                         ('gui_style_id', '0'),
                                         ('scheme_color', 'default'),
                                         ('scheme_color_id', '0'),
                                        ],
               "Category_Manager":      [
                                         ('visible_movie', 'True'),
                                         ('visible_book', 'True'),
                                         ('visible_music', 'True'),
                                         ('visible_coin', 'True'),
                                         ('visible_person', 'True'),
                                         ('visible_contact', 'True'),
                                         ('visible_note', 'True'),
                                         ('visible_video_game', 'True'),
                                         ('visible_quotation', 'True'),
                                         ('visible_stamp','True'),
                                         ('visible_wine', 'True'),
                                         ('visible_rent', 'True'),
                                         ('visible_tool_bar', 'True'),
                                         ('visible_status_bar', 'True')
                                         ]
               }
    config_parser = SafeConfigParser()

    print "type(config_parser)", type(config_parser)

    file_path = raw_input("Enter path to file: ")
    ignore_error_result = raw_input("Ignore error?: ")

    print '\nsetting up all dafaults'

    for section, value_list in my_dict.iteritems():
        for section_value, value in value_list:
            config_object = set_default_in_configuration(config_parser = config_parser,
                                                         section = section,
                                                         section_value = section_value,
                                                         value = value)
    print "\nWritting all defaults in tie file"
    write_in_configuration(config_parser = config_parser, file_name = file_path)

if __name__ == '__main__':
    main()
##    from config import ConfigurationSaver
##
##    dict_custom_config = ConfigurationSaver()
##    default_ini_file = DefaultINIFile()
##
##    user_ini_file = "D:\\Dan\\Python\\xarphus\\settings\\config.ini"
##
##    dict_custom_config.dict_set_general_settings["PathToINISetting"] = user_ini_file
##
##    error_msg = "The ini file may have been modified or destroyed. There load default settings."
##    try:
##        get_configuration(user_ini_file, dict_custom_config, default_ini_file, read_only_default=False)
##    except configparser.MissingSectionHeaderError as ex:
##        get_configuration(user_ini_file, dict_custom_config, default_ini_file, read_only_default=True)
##        print error_msg
##    finally:
##        create_write_ini_file(dict_custom_config, default_ini_file)
##
##    #return
