#!/usr/bin/env python
#-*- coding:utf-8 -*-
from PyQt4.QtCore import QSize

from PyQt4.QtGui import QItemDelegate

class Image_Delegate(QItemDelegate):      
   def __init__(self, img_width = None, img_height = None):    
       QItemDelegate.__init__(self)

       self._img_width = img_width
       self._img_height = img_height

   def sizeHint(self, option, index):
      return QSize(self._img_width, self._img_height) #(w, h)
