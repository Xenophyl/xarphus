
#!/usr/bin/env python
#-*- coding:utf-8 -*-

import traceback

from sys import exc_info
from traceback import format_exc

from datetime import date, datetime

def create_date_format(day = None,
					   month = None,
					   year = None):

	print "create_date_format"
	print "day", day, type(day)
	print "month", month, type(month)
	print "year", year, type(year)

	try:

		return date(int(year), int(month), int(day))

	except (TypeError, ValueError): 

		desired_trace = traceback.format_exc(exc_info())

		print "desired_trace", desired_trace

		if not desired_trace.find('an integer is required') == -1:
			return None

		if not desired_trace.find('day is out of range for month') == -1 or \
               not desired_trace.find('month must be in 1..12') == -1 or \
               not desired_trace.find('year is out of range') == -1:

			return None