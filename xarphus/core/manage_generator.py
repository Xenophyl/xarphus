#!/usr/bin/env python
# -*- coding: utf-8 -*-

def list_object_generator(list_object):
    print "list_object_generator is calling"
    '''
        NOTICE:
        =======
        Each time the yield statement is executed the function generates a new value,
        that means, this function produces a sequence of results.

        PARAMETERS:
        ===========
        :list_widgets   -       The function excepts a list that contains widgets. 
                                If there are no such objects in the list
                                its returns nothing.

        :yield          -       This generator yields items of a given  list.

    '''
    for element in list_object:
        yield element