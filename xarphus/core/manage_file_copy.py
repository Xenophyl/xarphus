from shutil import copy2
from os import path, makedirs, sep, remove
import errno
from tempfile import TemporaryFile

def assure_path_exists(dest_path):
    '''
            NOTICE:
            =======
            Checkk if the the given oath already exists. 

            PARAMETERS:
            ===========
            :dest_path        - Contains the destination path.

            :return           - Return True if path refers to an existing path. 
                                Returns False for broken symbolic links.
    '''
    return path.exists(dest_path)

def create_folder(dest_path):
    '''
            NOTICE:
            =======
            Create a missing folder in the given destination path.. 

            PARAMETERS:
            ===========
            :dest_path        - Contains the destination path.

            :return            - Nothing is returned. The statement 'return'
                                 terminates a function. That makes sure that the
                                 function is definitely finished.
    '''
    try:
        makedirs(dest_path)
    except OSError as exc: # Guard against race condition
        if exc.errno == errno.EEXIST and path.isdir(dest_path):
            pass
        else: raise  

    return

def string_io(img_path = None):

    from tempfile import TemporaryFile

    file_name = None

    with open(img_path, 'rb') as file_object: 

        with TemporaryFile() as tempf:
            tempf.write(file_object.read())
            tempf.seek(0)

            file_name = tempf.name

            return file_name

def copy_file(source_file = None, 
              destination_file = None, 
              file_name = None):
    '''
            NOTICE:
            =======
            Copy a file from source path to destination path. 
            Both of them have to be file-like objects, i.e. any
            object with a read or write method. 

            PARAMETERS:
            ===========
            :source_file        - Contains the source path of the given file.

            :destination_file   - Contains the destination path.	

            :file_name          - The given name of the file with file extension.

            :return             - Nothing is returned. The statement 'return'
                                  terminates a function. That makes sure that the
                                  function is definitely finished.
	'''
    print "file_name", file_name
    print "destination", destination_file
    print "source_file", source_file
    temp_file_name = None

    if not assure_path_exists(destination_file):
        
        create_folder((destination_file))

    with open(source_file, 'rb') as file_object:

        #   The default mode used to open the file returned by TempFile is 'w+b'. 
        #   The b in that string means it's opened in binary mode.
        #   For TemporaryFile, we can't use it with a context manager, because
        #   you can't really pass 'temp_file.name' to other processes to do something with it on Windows 
        #   (which seems to be OP's platform). Thats why we use delete=False.
        #   delete=False will not be closed/deleted automatically when it goes out of scope.
        #   From the manual: "Whether the name can be used to open the file a second time, while 
        #   the named temporary file is still open, varies across platforms (it can be so used 
        #   on Unix; it cannot on Windows NT or later).
        #   We have also work with TemporaryFile(), because there are situations, that file is provided 
        #   right into place such that src and destination are the same now and the copy operation would fail
        try:
        
            temp_file = TemporaryFile(mode='w+b', delete=False)
            temp_file.write(file_object.read())
            temp_file_name = temp_file.name
            temp_file.close()
            copy2(unicode(temp_file_name), unicode(path.join(destination_file, file_name)))
        
        finally:
            #   then delete the file manually once you've finished
            #   Clean up a NamedTemporaryFile on our own
            remove(temp_file_name)

    return

if __name__ == "__main__":

        source_path = raw_input("Enter spurce path: ")

        destination_path = raw_input("Enter destination path: ")

        name_file = raw_input("Enter file name: ")

        copy_file(source_file = source_path, destination_file = destination_path, file_name = name_file)
