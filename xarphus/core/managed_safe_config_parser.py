#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


from manage_ini_file import set_default_in_configuration

def store_config_parser_values_in_custom_config_dict(config_parser,
                                                     custom_config_dict):
    '''
       SUMMARY
       =======
       This method is used to set the default settings in the given configuration

       PARAMETERS
       ==========
       :config_parser (class):      We need the Configuration object so that we can save the (default) settings there.

       :custom_config_dict (class): We just load all the settings into the dictionary. The reason:
                                    If we have to change the ConfigParser (for example, the header or the values ​​are changed),
                                    you don't have to adjust them at all points in the program. So you only need to adapt the simple dictionary.
                                    The effort is considerably less.

       RETURNS
       =======
       :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
    '''
    logger.info('Store values from config parser in custom config dictionary')

    # General_Configuration
    header_general_configuration = 'General_Configuration'
    custom_config_dict.dict_set_general_settings["Database"] = config_parser.get(header_general_configuration, 'database_type')
    custom_config_dict.dict_set_general_settings["language"] = config_parser.get(header_general_configuration, 'language')
    custom_config_dict.dict_set_general_settings["language_id"] = config_parser.get(header_general_configuration, 'language_id')
    custom_config_dict.dict_set_general_settings["AskBeforClose"] = config_parser.get(header_general_configuration, 'ask_befor_close')
    custom_config_dict.dict_set_general_settings["AskBeforDelete"] = config_parser.get(header_general_configuration, 'ask_befor_delete')
    custom_config_dict.dict_set_general_settings["TimeOutUpdate"] = config_parser.get(header_general_configuration, 'time_out_update')
    custom_config_dict.dict_set_general_settings["TimeOutGetVersionHistory"] = config_parser.get(header_general_configuration, 'time_out_version_history')
    custom_config_dict.dict_set_general_settings["URLServerVersion"] = config_parser.get(header_general_configuration, 'url_version')
    custom_config_dict.dict_set_general_settings["VisibleToolBar"] = config_parser.get(header_general_configuration, 'visible_tool_bar')
    custom_config_dict.dict_set_general_settings["VisibleStatusBar"] = config_parser.get(header_general_configuration, 'visible_status_bar')

    # General_Design
    header_general_design = 'General_Design'
    custom_config_dict.dict_set_general_settings["GUIStyle"] = config_parser.get(header_general_design, 'gui_style')
    custom_config_dict.dict_set_general_settings["GUIStiyleID"] = config_parser.get(header_general_design, 'gui_style_id')
    custom_config_dict.dict_set_general_settings["SchemeColor"] = config_parser.get(header_general_design, 'scheme_color')
    custom_config_dict.dict_set_general_settings["SchemeColorID"] = config_parser.get(header_general_design, 'gui_style_id')

    # Category_Manager
    header_category_manager = 'Category_Manager'
    custom_config_dict.dict_set_general_settings["VisibleMovie"] = config_parser.get(header_category_manager, 'visible_movie')
    custom_config_dict.dict_set_general_settings["VisibleBook"] = config_parser.get(header_category_manager, 'visible_book')
    custom_config_dict.dict_set_general_settings["VisibleMusic"] = config_parser.get(header_category_manager, 'visible_music')
    custom_config_dict.dict_set_general_settings["VisibleCoin"] = config_parser.get(header_category_manager, 'visible_coin')
    custom_config_dict.dict_set_general_settings["VisiblePerson"] = config_parser.get(header_category_manager, 'visible_person')
    custom_config_dict.dict_set_general_settings["VisibleContact"] = config_parser.get(header_category_manager, 'visible_contact')
    custom_config_dict.dict_set_general_settings["VisibleNote"] = config_parser.get(header_category_manager, 'visible_note')
    custom_config_dict.dict_set_general_settings["VisibleVideoGame"] = config_parser.get(header_category_manager, 'visible_video_game')
    custom_config_dict.dict_set_general_settings["VisibleQuotation"] = config_parser.get(header_category_manager, 'visible_quotation')
    custom_config_dict.dict_set_general_settings["VisibleStamp"] = config_parser.get(header_category_manager, 'visible_stamp')
    custom_config_dict.dict_set_general_settings["VisibleWine"] = config_parser.get(header_category_manager, 'visible_wine')
    custom_config_dict.dict_set_general_settings["VisibleRent"] = config_parser.get(header_category_manager, 'visible_rent')

    return

def set_up_default_in_config(config_parser):
    '''
       SUMMARY
       =======
       This method is used to set the default settings in the given configuration

       PARAMETERS
       ==========
       :config_parser (class): We need the Configuration object so that we can save the (default) settings there.

       RETURNS
       =======
       :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
    '''
    logger.info('Set the default settings')

    default_section_value = {
                             "General_Configuration": [
                                                       ('current_profile', 'None'),
                                                       ('language', 'german_sie'),
                                                       ('language_id', '1'),
                                                       ('database_type', 'SQLite'),
                                                       ('ask_befor_close', 'True'),
                                                       ('ask_befor_delete', 'True'),
                                                       ('dir_path_temp_folder', 'D:\\Dan\\Python\\xarphus\\temp'),
                                                       ('url_update', 'http://www.web.de'),
                                                       ('time_out_update', '5'),
                                                       ('time_out_version_history','5'),
                                                       ('url_version', 'http://www.que.de'),
                                                       ('last_folder_path_remeber', 'None'),
                                                       ('visible_tool_bar', 'True'),
                                                       ('visible_status_bar', 'True')
                                                      ],
                             "General_Design":        [
                                                       ('gui_style', 'automatically'),
                                                       ('gui_style_id', '0'),
                                                       ('scheme_color', 'default'),
                                                       ('scheme_color_id', '0'),
                                                      ],
                             "Category_Manager":      [
                                                       ('visible_movie', 'True'),
                                                       ('visible_book', 'True'),
                                                       ('visible_music', 'True'),
                                                       ('visible_coin', 'True'),
                                                       ('visible_person', 'True'),
                                                       ('visible_contact', 'True'),
                                                       ('visible_note', 'True'),
                                                       ('visible_video_game', 'True'),
                                                       ('visible_quotation', 'True'),
                                                       ('visible_stamp','True'),
                                                       ('visible_wine', 'True'),
                                                       ('visible_rent', 'True'),
                                                       ]
                             }

    for section, value_list in default_section_value.iteritems():
        for section_value, value in value_list:
            set_default_in_configuration(config_parser = config_parser,
                                         section = section,
                                         section_value = section_value,
                                         value = value)
