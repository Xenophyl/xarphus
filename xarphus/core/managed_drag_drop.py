import platform

from PyQt4.QtGui import QWidget, QPixmap
from PyQt4.QtCore import Qt

# Use NSURL as a workaround to pyside/Qt4 behaviour for dragging and dropping on OSx
op_sys = platform.system()
if op_sys == 'Darwin':
    from Foundation import NSURL
    
class ManagedDragDrop(QWidget):
    def __init__(self, widget = None, parent = None):
        QWidget.__init__(self, parent)

        self._widget = widget
        
    def drag_enter_event(self, e):
        print "e", e
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def load_image(self):
        """
        Set the image to the pixmap

        :return:
        """
        pixmap = QPixmap(self.fname)
        pixmap = pixmap.scaled(500, 500, Qt.KeepAspectRatio)
        self._widget.setPixmap(pixmap)

    def drop_event(self, e):
        """
        Drop files directly onto the widget

        File locations are stored in fname
        :param e:
        :return:
        """
        fname = None
        if e.mimeData().hasUrls:
            e.setDropAction(Qt.CopyAction)
            e.accept()
            # Workaround for OSx dragging and dropping
            for url in e.mimeData().urls():
                if op_sys == 'Darwin':
                    fname = str(NSURL.URLWithString_(str(url.toString())).filePathURL().path())
                else:
                    fname = unicode(url.toLocalFile())

            self.fname = fname
            self.load_image()
            return fname
        else:
            e.ignore()
