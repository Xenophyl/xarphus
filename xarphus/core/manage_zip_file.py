#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import zipfile
import glob
from Crypto.Hash import SHA512

# def check_zipfile(zipfile_path):
#     for zip_file_list in ([zipf for zipf in os.listdir(zipfile_path) if zipf.endswith('.zip')], os.listdir(zipfile_path)):
#         for zip_file in zip_file_list:
#             print "Dateien:", zipfile_path
#             try:
#                 with open(zip_file, 'rb') as f:
#                     data = f.read()
#                     print "%s opened ok" % zip_file
#                     print data
#             except Exception as ex:
#                 print "Detail", ex
#
#         print '-'*40

'''
def check_files(zipfilename):
    """ Check and see if all members of a .zip archive can be opened.
        Beware of vacuous truth - all members of an empty archive can be opened
    """
    def can_open(archive, membername):
        try:
            archive.open(membername, 'r')  # return value ignored
        except (RuntimeError, zipfile.BadZipfile, zipfile.LargeZipFile):
            return False
        return True

    with zipfile.ZipFile(zipfilename, 'r') as archive:
        return all(can_open(archive, membername)
                    for membername in (
                        name for name in archive.namelist() if name[-1] != '/'))
'''


def get_zipfile(path_zip_file):
    '''
    Here: a file extension is created.
    '''
    extension_file = 'zip'

    '''
    Save in the variable named (zip_file_path) the path to the zip files,
    including the extension of the file.
    '''
    zip_file_path = os.path.join(path_zip_file, '*.%s') % extension_file

    '''
    Access and grab all zip files in current folder, and save it in the variable
    named (path_files).
    '''
    path_files = glob.glob(zip_file_path)
    print "path_files", path_files

    #sum_zip_files = sum(f for f in os.listdir(path_zip_file) if f.endswith('.zip'))
    #print "sum_zip_files", str(sum_zip_files)

    '''
    Create a flag (i). The flag is zero.
    '''
    i = 0

    '''
    Create a empty dictionary named (filename_arr)
    '''
    filename_arr = {}

    '''
    Create a for-loop
    '''
    for file_element in path_files:
        '''
        Add all files with extension like zip in a dictionary.
        The variable (i) will count for every file in the current folder.

        NOTICE: The keys in the empty dictionary named (filename_arr) will create
        through the for-loop with the variable (i), and then the iterated files (file_element)
        are added as values automatically.
        '''
        filename_arr[i] = file_element
        i = i + 1

    return i, filename_arr

def check_number_files(path_zip_file, file_zip_pwd):
    count_files, path_files = get_zipfile(path_zip_file)

    if count_files > 1:
        print "Too much files"
    else:
        result_check_zip_file = check_zipfile(path_files[0], file_zip_pwd)
        print "[result_check_zip_file]", result_check_zip_file
        return result_check_zip_file

#   The function creates a hash value with iter()-method.
##def sha512_ckecksum(file_object, chunk_size=65536):
##    sha = SHA512.new()
##    # The file must be open in binary mode.
##    with open(file_object, 'rb') as data:
##        # The iter() function returns an iterator on object named data.
##        for chunk in iter(lambda: data.read(chunk_size), b''):
##            sha.update(chunk)
##        digest = sha.hexdigest()
##    return digest


def generator_read_in_chunks(file_object, chunk_size=65536):
    
    while True:

        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

def sha512_ckecksum(file_object):
    """
    Process the file line by line.
    """

    h = SHA512.new()

    with open(file_object, 'rb') as file_handler:
        for chunk in generator_read_in_chunks(file_handler):
            h.update(chunk)

        return h.hexdigest()

def test_zipfile(zipfile_path, zip_pwd=""):

    with zipfile.ZipFile(zipfile_path, 'r') as f:
        f.setpassword(zip_pwd)

        return f.testzip()

if __name__ == "__main__":

    import time
    from datetime import datetime

    zip_file_name = unicode(raw_input("Enter path completly: ")) # Enter path completly with file name with file extension!
    zip_pwd = unicode(raw_input("Enter password: "))
##    try:
##        # start to stopp the time
##        start_time = datetime.now()
##        
##        hashed_value = sha512_ckecksum(zip_file_name)
##
##        #hashed_value = process_file(zip_file_name)
##
##        
##        # end with to stopp the time
##        end_time = datetime.now()
##
##        # Show how much time has been elapsed.
##        print('Duration: {}'.format(end_time - start_time))
##        
##        print "" 
##        original_hash_value = "e6559ad112b028af2fae704cad7d5ae3793301b1fc8ccf11a36fa59d30f75a87a58811ca8c88b804c97cd924690a77d63ae56b14778c8e7725c2f478f58d8171"
##        print "original_hash_value: \n", original_hash_value
##        print ""
##        print "hashed_value: \n", hashed_value
##        
##        if hashed_value == original_hash_value:
##            print "\nSHA512 verified."
##        else:
##            print "\nVerification failed!"
##    except (IOError, OSError):
##      print "\nError: File does not appear to exist."

    try:
        result = test_zipfile(zip_file_name, zip_pwd)

        print "result", result

        if result == None:
            print "Done! The test was done successfully. The zipfile is correct."

    except zipfile.BadZipfile as bad_zip_file:
        print "BAD", bad_zip_file
    except RuntimeError as RuEr:
        print "(RuntimeError)", RuEr
        
        if 'encrypted' in str(RuEr):
            print 'This zip has encrypted files! Try again with a password!'
            #return 2, 0
        if 'Bad password' in str(RuEr):
            print "Bad Guy"
            #return 2, 0
        if 'password required' in str(RuEr):
            #return 2, 0
            print "Missing Password"
            
    except Exception as ex_corrupt:
        print "Exception", ex_corrupt
    #    return False

    
