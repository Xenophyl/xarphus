#!/usr/bin/env python
#-*- coding:utf-8 -*-

from os import sep
from os import path

def is_specifically_suffix(string):
	'''
	huhuhu
	'''

	if string.endswith('\\') or string.endswith('/'):
		return True
	else:
		return "{string}{sep}".format(string=string, sep=sep)

def extract_directory(path_name):

	'''
            NOTICE:
            =======
            Its returns the dir part of path. 
            (result won't have ending slash, unless it's root dir.)

            PARAMETERS:
            ===========
            :return         -       Its returns the dir part of path.
	
	'''
	return path.dirname(path_name)