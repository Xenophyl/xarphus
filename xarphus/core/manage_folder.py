#!/usr/bin/env python
#-*- coding:utf-8 -*-

import glob
from stat import S_IWRITE
from shutil import rmtree
import os
from os import chmod, path as os_path, listdir, remove, makedirs
import errno

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def list_top_sub_folders(path, profile_name):

    '''
       SUMMARY
       =======
       This method starts a thread to sign in.

       PARAMETERS
       ==========
       :path (str):         Path to the file.

       :profile_name (str): Name of the folder.


       RETURNS
       =======
       :return:             Its returns a list of paths.
    '''

    profile_folder_path = os_path.join(path, profile_name)

    setting_folder_path = os_path.join(profile_folder_path, 'settings')

    database_folder_path = os_path.join(profile_folder_path, 'database')
    log_folder_path = os_path.join(profile_folder_path, 'log')
    update_folder_path = os_path.join(profile_folder_path, 'update')

    FOLDER_IMAGES = os_path.join(path, 'images')

    FOLDER_MOVIE = "movie"
    FOLDER_IMAGES = "images"

    images_movie_path = os_path.join(profile_folder_path, FOLDER_IMAGES, FOLDER_MOVIE)

    images_movie_poster_path = os_path.join(profile_folder_path, FOLDER_IMAGES, FOLDER_MOVIE, 'poster')

    images_movie_cover_path = os_path.join(profile_folder_path, FOLDER_IMAGES, FOLDER_MOVIE, 'cover')

    images_series_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'series')

    images_music_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'musicmusic')

    images_record_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'record')

    images_addressbook_path =  os_path.join(profile_folder_path, FOLDER_IMAGES, 'addressbook')

    images_person_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'person')

    images_book_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'book')

    images_comic_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'comic')

    images_news_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'news')

    images_magazine_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'magazine')

    images_coin_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'coin')

    images_videogame_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'videogame')

    images_stamp_path = os_path.join(profile_folder_path, FOLDER_IMAGES, 'stamp')

    path_list = [profile_folder_path,
                 setting_folder_path,
                 database_folder_path,
                 log_folder_path,
                 update_folder_path,
                 FOLDER_IMAGES,
                 images_movie_path,
                 images_movie_poster_path,
                 images_movie_cover_path,
                 images_series_path,
                 images_music_path,
                 images_record_path,
                 images_addressbook_path,
                 images_person_path,
                 images_book_path,
                 images_comic_path,
                 images_news_path,
                 images_magazine_path,
                 images_coin_path,
                 images_videogame_path,
                 images_stamp_path,

                 ]

    return path_list

def is_folder_empty(folder_path):
    '''
       SUMMARY
       =======
       TThis function checks if the folder of the given path is empty or not.
       It isn't empty and there are folder the function returns a list of them, otherwise
       its return False or an empty list.

       PARAMETERS
       ==========
       :folder_path (str): Path to the folder to be checked whether there are subfolders.

       RETURNS
       =======
       :return:             Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                            me a better feeling, that says: 'return' terminates a function definitely.
    '''
    list_folder_path = []
    list_folder_name = []

    #   Check is NOT empty
    if not len(listdir(folder_path)) == 0:

        for folder_name in listdir(folder_path):
            path = os_path.join(folder_path, folder_name)

            #   We need to filter out directories
            if os_path.isdir(path):
                # skip directories
                list_folder_path.append(path)
                list_folder_name.append(folder_name)
            else:
                pass
        return list_folder_path, list_folder_name
    else:
        #   folder is empty
        return False, False

def generator_top_sub_folder(folder_path, profile_name):

    path = list_top_sub_folders(path = folder_path, profile_name = profile_name)

    for folder in path:

        yield folder

def mkdir_top_sub_folder(folder_path, profile_name):

    generator_top_sub_folder = generator_top_sub_folder(folder_path = folder_path, profile_name = profile_name)

    for folder in generator_top_sub_folder():
        makedirs(folder)

#-----------------------------------------------------------------------------
def get_all_folders(path_update_temp_folder):
    '''
    The variable named (all_folders) shows all folders in current folder.
    '''
    all_folders = os_path.join(path_update_temp_folder, '*/')

    '''
    Access and grab all folders in current folder, then save it in the variable
    named (folder_paths)
    '''
    folder_paths = glob.glob(all_folders)

    '''
    Return all grabbed files
    '''
    return folder_paths

def get_all_files(path_update_temp_folder, bool_value):
    '''
    The function (get_all_files) takes two arguments: path_update_temp_folder and
    bool_value. In (path_update_temp_folder) there is the path to the folder, where
    the temporary file for updating is. In (bool_value) there are boolean values.
    That means True or False.
    '''


    '''
    The variable named (all_files) shows all files - everything including hidden files
    '''
    all_files = os_path.join(path_update_temp_folder, '*')

    '''
    Acces and grab all files in current folder. Then save all grabbed files in the variable
    named (files_paths).
    '''
    files_paths = glob.glob(all_files)

    '''
    When you choose the boolean value True, you only get the number of files, that are
    in current folder. The function (get_all_files) will return only number of files.
    Not more or less.
    '''
    if bool_value == True:
        sum_files = sum(os_path.isfile(temp_files) for temp_files in glob.glob(all_files))
        return sum_files

    '''
    When you choose the boolean value False, you get all files in a list. The function
    (get_all_files) will return a list of grabbed files - with path.
    '''
    if bool_value == False:
        return files_paths

def delete_temp_update_files(path_files):
    '''
    Try to delete all grabbed files. All files are
    grabbed in the variable named (path_files)
    '''
    try:
        for f in path_files:

            remove(f)

        return True

    except Exception as ex:
        print "delete_temp_update_files", ex
        return False

def create_folder(folder_path):

    makedirs(folder_path)

    return

def handle_error(function,
                 dir_path,
                 excinfo):
    '''
       SUMMARY
       =======
       This method sets the write permission for owner.

       PARAMETERS
       ==========
       :function (builtin_function_or_method):  Which raised the exception. where func is os.listdir,
                                                os.remove, or os.rmdir.

                                                and exc_info is a tuple returned by sys.exc_info().


       :dir_path (str):                         Path name passed which raised the exception while removal.
                                                Its means ath is the argument to that function that caused it to fail

       :excinfo (tuple):                        Exception information returned by sys.exc_info().

       RETURNS
       =======
       :return:                                 Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                                                statement gives me a better feeling, that says: 'return' terminates a function definitely.
    '''
    #   Owner has write permission. Try to change the permision of file
    chmod(dir_path, S_IWRITE)

    #   Call the calling function again
    delete_folder(folder_path = dir_path)

    return

def delete_folder(folder_path):
    '''
       SUMMARY
       =======
       This method deletes all contents of a directory and handle errors

       PARAMETERS
       ==========
       :folder_path (str):  Path name passed which folder we want to remove.

       RETURNS
       =======
       :return:             Nothing is returned. Well we know a simple 'return' isn't necessary, but this
                            statement gives me a better feeling, that says: 'return' terminates a function definitely.
    '''
    try:
        #   Somethimes there is a file in directory has read only attributes i.e. user can not delete that file,
        #   then it will raise an exception i.e.PermissionError: [WinError 5] Access is denied:
        rmtree(folder_path, ignore_errors = False)

    except Exception as ex:

        try:
            #   If any exception occurs while deleting a file in rmtree()
            #   and onerror callback is provided. Then callback will be
            #   called to handle the error. Afterwards shutil.rmtree() will
            #   continue deleting other files.
            rmtree(folder_path, onerror = handle_error, ignore_errors = False)

        except Exception as ex:

            #create_folder(folder_path = folder_path)
            print "ex", ex

    return


def delete_temp_update_folders(folder_path):
    '''
    Try to delete all grabbed folder. All folders
    are grabbed in the variable named (folder_path)
    '''

    try:
        for folder_element in folder_path:
            shutil.rmtree(folder_element, ignore_errors=True)

        return True
    except Exception as ex:
        print "delete_temp_update_folders", ex
        return False, ex


def check_files_folder(path_update_temp_folder):

    folder_paths = get_all_folders(path_update_temp_folder)

    print "folder_paths", folder_paths

    files = glob.glob(path_update_temp_folder)
    for f in files:
        print "all files/folders", f


    if not folder_paths:
        '''
        The list (folder_paths) is empty, that means there aren't some folders.
        In this case its enough to delete all files - everything including
        hidden files.
        NOTICE: We call the function (get_all_files) with value True. We want to get back
        the number of files.
        '''
        files_paths_true = get_all_files(path_update_temp_folder, bool_value=True)

        '''
        We check if there are more files than zero.
        '''
        if files_paths_true > 0:
            '''
            Zero means here nothing. If the number in the variable (files_paths_true) bigger
            than zero, that means there are files.
            NOTICE: We call the function (get_all_files) again, but with value False. Now
            we want get back all files with path.
            '''
            result_files_false = get_all_files(path_update_temp_folder, bool_value=False)

            '''
            All files with path are saved in the variable (result_files_false). Then we
            call the function (delete_temp_update_files) to delete all listed files with path.
            NOTICE: Was the task in the function (delete_temp_update_files) successfully we
            will get back the value True, otherwise we get back False.
            '''
            result_files = delete_temp_update_files(result_files_false)

            '''
            We result the result of the task of function (delete_temp_update_files).
            '''
            return result_files

        else:
            '''
            In this case, there aren't files to delete. The folder is really empty. No files there.
            We can send the value TRUE return.
            '''
            return True
    else:
        '''
        There are some folders. Before the files are deleted, the folder must be deleted at first.
        '''
        result_deletion_folder = delete_temp_update_folders(folder_paths)

        '''
        We take a look in the variable (result_deletion_folder), and check the
        value that returned from the function (delete_temp_update_folders). The function
        (delete_temp_update_folders) returns two values: True or False.
        '''
        if result_deletion_folder == True:

            '''
            The functions returns the value True. That means, Deleting of the file was
            successful. Next step, we check again if there are folders and call
            the function named (get_all_folders) again. The function (get_all_folders)
            returns a list - either a empty list, when there are no folders or a list of
            folder(s), when there a folders.
            '''
            result_folders = get_all_folders(path_update_temp_folder)

            if not result_folders:
                '''
                The list, that saved in the variable (result_folders), is empty.
                That means, the folders in current folder were deleted. The current folder is
                really empty. Now we can check if there are files.
                '''
                files_paths_true = get_all_files(path_update_temp_folder, bool_value=True)

                '''
                We check if there are more files than zero.
                '''
                if files_paths_true > 0:
                    '''
                    Zero means here nothing. If the number in the variable (files_paths_true) bigger
                    than zero, that means there are files.
                    NOTICE: We call the function (get_all_files) again, but with value False. Now
                    we want get back all files with path.
                    '''
                    result_files_false = get_all_files(path_update_temp_folder, bool_value=False)

                    '''
                    All files with path are saved in the variable (result_files_false). Then we
                    call the function (delete_temp_update_files) to delete all listed files with path.
                    NOTICE: Was the task in the function (delete_temp_update_files) successfully we
                    will get back the value True, otherwise we get back False.
                    '''
                    result_files = delete_temp_update_files(result_files_false)

                    '''
                    We result the result of the task of function (delete_temp_update_files).
                    '''
                    return result_files

                else:
                    '''
                    In this case, there aren't files to delete. The folder is really empty. No files there.
                    We can send the value TRUE return.
                    '''
                    return True
            else:
                '''
                In this case, there are still folders after deleting of the folders. We tried to delete all
                folders, but it doesn't work.
                '''
                return False

def is_folder_exists(dir_path):
        '''
           SUMMARY
           =======
           This method is used to check if the path to the folder already exists or not,
           and then is also used to check if its a directory.

           RETURNS
           =======
           :return: Return True if path refers to an existing directory entry
                    that is a symbolic link. Always False if symbolic links are
                    not supported by the Python runtime.
        '''

        logger.info("Checking folder to given path")

        return os_path.isdir(dir_path)

def main():

    path_folder_check = raw_input('Enter path to folder: ')
    result_check_folder = is_folder_exists(dir_path = path_folder_check)

    print "result_check_folder", result_check_folder

if __name__ == '__main__':
    main()
