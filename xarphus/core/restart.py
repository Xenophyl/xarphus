#!/usr/bin/env python
#-*- coding:utf-8 -*-

FILE_NAME = __name__
import sys

from subprocess import Popen


def restart_program():
    # Erstes Argument: Pfad zum Python-Interpreter
    # Zweites Argument: Anweisung, dass ein Modul geladen werden soll
    # Drittes Argument: Name des zu startenden Moduls (ohne ".py")
    Popen([sys.executable, '-m', 'xarphus_main'])

def start_update(exec_file):
    # Erstes Argument: Pfad zum Python-Interpreter
    # Zweites Argument: Anweisung, dass ein Modul geladen werden soll
    # Drittes Argument: Name des zu startenden Moduls (ohne ".py")
    print "exec_file", exec_file
    #Popen([sys.executable, '-m', exec_file])

    Popen([sys.executable, exec_file])
