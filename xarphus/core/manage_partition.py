def partition_text(text = None,
                   left_separator = None,
                   right_separator = None):
    
    return text.partition(left_separator)[-1].rpartition(right_separator)[0]