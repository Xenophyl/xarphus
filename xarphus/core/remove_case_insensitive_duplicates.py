#!/usr/bin/env python
#-*- coding:utf-8 -*-

from web_scraper import scrape_no_name_taple
from normalize_uni_str import normalize_text, normalize_record_list_two_pair_tuple
from manage_categories import analyze_categorie

def get_pair_number(data_structure = None):

    stop_loop = False
    pair_number = None

    for element in data_structure:

        if not stop_loop:
            pair_number = len(element)
        else:
            return pair_number

        stop_loop = True

def compare_two_simple_listss(category = None,
                              online_record_list = None,
                              database_record_list = None):
    '''
        NOTICE:
        =======
        This method is used to fetch all records
        and save them to the list that will be returned.

        PARAMETERS:
        ===========
        :category       -       The category says us what records it is.

        :return         -       Return a list
    '''
    missing_item_counter = 0
    missing_items_list = []

    if any(element == category for element in analyze_categorie(list_name = "simple_list")):

        for online_item in online_record_list:

            if online_item not in database_record_list:
                missing_items_list.append(online_item)
                missing_item_counter += 1

    elif any(element == category for element in analyze_categorie(list_name = "list_two_pair_tuple")):

        missing_item_counter, missing_items_list = compaire_list_two_pair_tuple(new_list = online_record_list,
                                                                                original_list = database_record_list)

    return missing_item_counter, missing_items_list

def compaire_list_two_pair_tuple(new_list = None,
                                 original_list = None):
    '''
        NOTICE:
        =======
        This function is used to remove case-insensitive duplicates from a list of two pair tuples.
        If tuple pair happens to contain another tuple pair, it still like to retain tuple pair.

        PARAMETERS:
        ===========
        :new_list       -   We need a list were new records are stored.

        :original_list  -   It is an inventory list of records

        :return         -   Return a count and a new list
    '''
    #   We create an empty lost to store  common elements
    temp_new_list = []

    #   We have to define how many pair numbers we work with
    pair_number = 2

    #   Second, we have to create an empty variable.
    #   We need it, because we have to save a list, that stores the
    #   lowercase version of tuple pair we are encountering.
    original_marker  = None

    count = 0

    #if get_pair_number(new_list) and get_pair_number(original_list) == pair_number:

    #   Before we start we have iterate over the original
    #   given list, because we store the lowercase version
    #   of tuple pairs. We used tuple unpacking in the for loop there.
    original_marker = [(first.lower(), second.lower()) for first, second in original_list]

    #   Well we have a other new list.
    #   Let us loop over each element in new_list
    for tuple_pair in new_list:

        #   Ok, we have to unpack the tuple.
        first, second = tuple_pair

        #   We have to create a tuple, wich contains lowercase pair.
        lowercased_tuple_pair_marker = (first.lower(), second.lower())

        # if lowercased tuple pair aren't in original_list, add it to 'temp_new_list'
        if not lowercased_tuple_pair_marker in original_marker:
            temp_new_list.append(tuple_pair)

            count += 1

    #   After we are done, the functions returns the
    #   counter and the temp list - without duplicates.
    return count, temp_new_list



def remove_case_insensitive_duplicates_list_two_pair_tuple(list_tuple = None):
    '''
        NOTICE:
        =======
        This function is used to remove case-insensitive duplicates from a list of tuples.
        If tuple pair happens to contain another tuple pair, it still like to retain tuple pair.

        PARAMETERS:
        ===========
        :new_list       -   We need a list were records are stored.

        :original_list  -   We need a list were records are stored.

        :return         -   Return a count and a new list
    '''
    #   We create an empty lost to store  common elements
    temp_new_list = []

    marker = set()

    count = 0

    #   We have to define how many pair numbers we work with
    pair_number = 2

    #if get_pair_number(new_list) == pair_number:

    #   Well we have a other new list.
    #   Let us loop over each element in list_tuple
    for tuple_pair in list_tuple:

        #   Ok, we have to unpack the tuple.
        first, second = tuple_pair

        #   We have to create a tuple, wich contains lowercase pair.
        lowercased_tuple_pair_marker = (first.lower(), second.lower())

        # if lowercased tuple pair aren't in original_list, add it to 'temp_new_list'
        if not lowercased_tuple_pair_marker in marker:
            marker.add(lowercased_tuple_pair_marker)
            temp_new_list.append(tuple_pair)

            count += 1

    #   After we are done, the functions returns the
    #   temp list - without duplicates.
    return count, temp_new_list

def remove_case_insensitive_duplicates_list(list_records = None):
    '''
        NOTICE:
        =======
        This method is used to remove case-insensitive duplicates from a list.
        If an item - for example : "Hello World" - happens to contain another
        item - for example: "World", it still like to retain both items.

        PARAMETERS:
        ===========
        :list_records   -       We need a list were records are stored.

        :return         -       Return a count and a new list
    '''
    count = 0

    #   First, we create an empty
    result = []

    #   Second, we have to create a set().
    #   We need it, because we store the lowercase version
    #   of the strings we are encountering.
    marker = set()

    #   Third, we iterate over the given list
    for item in list_records:

        #   Well before we starts to work with the given list,
        #   we have to check if there aren't a tuple. Sometimes its
        #   possible that we get a list wich contains tuples.

        if not isinstance(item, tuple):

            #   Ok, there aren't a tuple, so We save the
            #   lowercased version of the string
            #   in the variable named 'lower_cased_string'.
            lower_cased_string = item.lower()

            #   We need to check if the lower cased string
            #   not in the marker (test presence)
            if lower_cased_string not in marker:
                #   No, there isn't a lower cased string,
                #   we add it
                marker.add(lower_cased_string)

                #   We can therefore assume that there aren't duplicates of case-insensitive in the list.
                #   That why we also add the item to the other list.
                result.append(item)

                count += 1

        elif isinstance(item, tuple):

            count, result = remove_case_insensitive_duplicates_tuple_list(new_list = list_records)

    return count, result

def _fetch_all_online_data(url = 'http://sophus.bplaced.net/xarphus_support/support_account/xarphus/general_resolution.php',
                           support_username = 'sophus',
                           support_password = 'danny',
                           timeout = 10):

    from requests import Session
    from requests.auth import HTTPBasicAuth

    online_items_list = []

    #   Use 'with' to ensure the session context is closed after use.
    #   We need to use a session object and send the authentication each request.
    with Session() as s:
        # An authorised request.
        r = s.get(url, verify = True, auth = HTTPBasicAuth(support_username, support_password), timeout = timeout)
        #   We test the response object in a boolean context

        if r:

            online_items_list = scrape_no_name_taple(html_content = r.text)

            online_counter, online_items_list = remove_case_insensitive_duplicates_list(list_records = online_items_list)

            #print "online_counter", online_counter
            #print "online_items_list", online_items_list

            normalized_online_record_list = normalize_record_list_two_pair_tuple(item_list = online_items_list)

        else:
            r.raise_for_status()

    return online_counter, normalized_online_record_list

def _compare_records(online_record_list = None,
                     database_record_list = None):
    '''
        NOTICE:
        =======
        This method is used to fetch all records
        and save them to the list that will be returned.

        PARAMETERS:
        ===========
        :category       -       The category says us what records it is.

        :return         -       Return a list
    '''
    #import sys
    missing_item_counter = 0
    missing_items_list = []

    if get_pair_number(online_record_list) and get_pair_number(database_record_list) == 2:
        count, item = remove_case_insensitive_duplicates_list(online_record_list)

        print "Done!"
        print "count", count


    #return missing_item_counter, missing_items_list

if __name__ == "__main__":

    #list_records = ['Hello', 'HELLO', 'WORLD', 'test']

    list_records = [('Sam', 'Kevin'),
                    ('kevin', 'sam'),
                    ('ROBIN', 'Batman'),
                    ('robin', 'BATMAN'),
                    ('Jason', 'Freddy'),
                    ('Chucky', 'Michael'),
                    ('CoDY', 'BEN'),
                    ('cody', 'ben')]

    #count, result_list = remove_case_insensitive_duplicates_list(list_records = list_records)
    #print "count", count, "result_list", result_list
    count, tuple_list = _fetch_all_online_data()
    print "count", count
    print "tuple_list", tuple_list
    print "Start _compare_records"
    _compare_records(online_record_list = tuple_list, database_record_list = tuple_list)
