#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from shutil import copy2, copytree
from glob import glob
from os import chdir, remove, unlink, path, SEEK_END
from pathlib2 import Path
from sys import exc_info
from traceback import format_exc

import errno


def read_in_chunks(file_object, chunk_size = 1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data

def is_file_empty(file_name):
    '''
       SUMMARY
       =======
       In this method we take a look, if the given file is really empty.

       DETAILS:
       ========
       The reason why we read the given file is that we don't want to
       test from outside whether a file is actually empty, by checking the size of the file.
       Because a file with only full spaces, tabs and line breaks is still larger than 0 kilobytes.

       PARAMETERS
       ==========
       :file_name (str):    Its a path to the given file.

       RETURNS
       =======
       :return:             We return the result. True means, its a empty file.
                            Otherwise, False means isn't a empty file.
    '''
    logger.info("Read a given file")

    is_empty = True #   By default we set the variable on True, that means,
                    #   we assume that the file is empty.
                    #   Because if the file is absolutely empty,
                    #   there is no iteration after opening the file.
                    #   As soon as the file is completely empty, it is not
                    #   possible to set a flag within the for loop.

    with open(file_name, 'r') as file_object:
        print "file_object", file_object.readline()
        for char in read_in_chunks(file_object = file_object):
            print "char", char
            content = char.replace(' ', '').replace('', '').replace('\t', '').replace('\n', '').replace('\r', '').replace('\r\n', '').replace('\v', '')
            print "content", content
            if not content:
                print "File is empty"
                #   The file is empty
                is_empty = True
            elif content:
                print "File sin't empty"
                #   The file is  NOT empty
                is_empty = False
                break # We can stop here because we found a
                      # character in the file.
                      # That means the file is NOT empty.
    return is_empty



##    try:
##        #   It actually checks whether the file has any contents at all.
##        with open(file_name) as file_object:
##            file_object.seek(0, SEEK_END) # Ensure you're at the start of the file and go to end of file
##            print "file_object.tell():", file_object.tell()
##            if file_object.tell(): # if current position is truish (i.e != 0)
##                file_object.seek(0) # rewind the file for later use.
##                                    # ensure you're at the start of the file.
##                                    # #first character wasn't empty, return to start of file.
##                                    # use file now
##                print "It seems, the file isn't empty, so let us see the size of the file"
##
##                if path.getsize(file_name):
##                    print "file isn't really empty"
##                else:
##                    print "Ok, the file is empty - absolutly"
##
##            else:
##                print "file is empty"
##    except (IOError, TypeError):
##        print "Problem"

##        try:
##            with open(file_name) as my_file:
##                # I already have file open at this point.. now what?
##                my_file.seek(0) #ensure you're at the start of the file..
##                first_char = my_file.read(1) #  Get only the first character, because
##                                             #  What if the file is 5 Terabytes big?
##                                             #  Yes it will try to read all the file...
##                print "first character", first_char
##
##                myString = first_char.replace(' ', '').replace('\t', '').replace('\n', '')
##
##                if not myString:
##                    print "file is empty" #first character is the empty string.
##                else:
##                    print "The file isn't empty"
##        except (IOError, TypeError):
##            print "Problem"



def file_exists(file_path):

    return path.exists(file_path)

def copy_folder_to(source_path, destination_path):

    copytree(unicode(source_path), unicode(destination_path))

    return

def copy_file_to(source_path, destination_path):

    from distutils.dir_util import copy_tree

    copy_tree(source_path, destination_path)

##	copy2(unicode(source_path), unicode(destination_path))
##
##	return

def find_files(file_path = None, file_extension = None):

    chdir(file_path)

    return glob('*.{ext_file}'.format(ext_file = file_extension))

def get_file_extension(file_path = None):

    return Path(file_path).suffix[1:]

def delete_file(file_list):

    for myfile in file_list:

        try:
            remove(myfile)

        except OSError as (errno, strerror):


            if errno == 5:

                #   Handle errors while calling os.ulink()
                try:
                    unlink(myfile)

                except OSError as (errno, strerror):

                    print "UNLIKE -- OSError error({0}): {1}".format(errno, strerror)

                    return False

if __name__ == "__main__":

##    result_source_path = raw_input("Enter sourfe path: ")
##    last_folder = path.basename(result_source_path)
##    result_destination_path = raw_input("Enter destination path: ")
##
##    path.join(result_destination_path, last_folder)
##
##    copy_folder_to(source_path = result_source_path, destination_path = path.join(result_destination_path, last_folder))
    while True:
        file_path = raw_input("Enter path to file: ")

        try:
            print "RESULT", is_file_empty(file_name = file_path)
        except (IOError, TypeError) as err:

            print "There is a probleme", err


