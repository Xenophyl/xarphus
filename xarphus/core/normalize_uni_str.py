#!/usr/bin/env python
#-*- coding:utf-8 -*-

from unicodedata import name, normalize

def normalize_text(text = None,
				   form = 'NFD' ):
	'''
	    NOTICE:
	    =======
	    This function is used to  to normalize a unicode string, 
	    which comprehends unicode entities that can be used to represent it. 

	    PARAMETERS:
	    ===========
	    :form   -   By default values for form ‘NFD’. But
	    			there are ‘NFC’, ‘NFKC’ and ‘NFKD’.

	    :text 	-	We need a unicode string

	    :return -   Return the normal form for the given (unicode) string
	'''
	try:

		return normalize(form, text)

	except TypeError:

		return normalize(form, unicode(text))

def normalize_record_list(item_list = None):
	'''
	    NOTICE:
	    =======
	    See the function normalize_text. This function iterates over 
	    the list and normalizes the elements.

	    PARAMETERS:
	    ===========
	    :item_list   -       We need a list were records are stored.

	    :return      -       Return a list with normalized elements
	'''
	normalized_list = []

	for elem in item_list:

		normalized_list.append(normalize_text(text = elem))

	return normalized_list

def normalize_record_list_two_pair_tuple(list_tuple = None):
	'''
	    NOTICE:
	    =======
	    See the function normalize_text. This function iterates over 
	    the list and normalizes the elements.

	    PARAMETERS:
	    ===========
	    :list_tuple  -       We need a list were records are stored.

	    :return      -       Return a list with normalized elements
	'''
	normalized_list = []

	for elem in list_tuple:

		first, second = elem

		normalized_first = normalize_text(text = first)
		normalized_second = normalize_text(text = second)

		normalized_list.append((normalized_first, normalized_second))

	return normalized_list