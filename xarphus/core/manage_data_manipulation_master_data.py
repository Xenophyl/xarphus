#!/usr/bin/env python
#-*- coding:utf-8 -*-

from datetime import date

from sys import exc_info
from os import path
from traceback import format_exc
from shutil import Error as shutil_error

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

try:
    #from xarphus.core.managed_defined_session import *

    from xarphus.core.manage_db_connection import FilmGenre, FilmProductionCompany, \
                                                  FilmRentals, FilmDistribution, FilmStudio, \
                                                  FilmResolution, FilmRegionalCode, FilmAspectRatio, \
                                                  FilmDimension, FilmEdition, FilmVersion, FilmVideonorm, \
                                                  FilmAudioformat, FilmAudiochannel, FilmColourformat, \
                                                  FilmVideoformat, FilmSoundsystem, FilmGeneral, \
                                                  PersonGender, PersonNationality, PersonSalutation, \
                                                  PersonTitle, PersonHairColor, PersonEyeColor, \
                                                  PersonReligion, PersonRelationsshipStatus, \
                                                  PersonRelationsship, Person, \
                                                  GeneralRegion, GeneralAward, GeneralCountry, \
                                                  GeneralLanguage, GeneralPlace,  GeneralMediaType, \
                                                  GeneralPackaging, GeneralAgeRelease, \
                                                  GeneralManufacturer, \
                                                  MusicGenre, MusicStudio, MusicSPARSCode, MusicPublisher, \
                                                  MusicStyle, MusicLabel, MusicDistribution, \
                                                  BookGenre, BookPublisher, BookBinding, \
                                                  CoinAlloy, CoinCurrency, CoinType, CoinManufacturingProcedure, \
                                                  CoinDegreePreservation, CoinMint, \
                                                  VideogameGenre, \
                                                  AllocationFilmGeneralGeneralCountry, AllocationFilmGeneralFilmGenre

    from xarphus.core.manage_categories import analyze_categorie
    from xarphus.core.manage_file_copy import copy_file
    from xarphus.core.normalize_uni_str import normalize_text

except ImportError:
    #from managed_defined_session import *

    from manage_db_connection import FilmGenre, FilmProductionCompany, \
                                                  FilmRentals, FilmDistribution, FilmStudio, \
                                                  FilmResolution, FilmRegionalCode, FilmAspectRatio, \
                                                  FilmDimension, FilmEdition, FilmVersion, FilmVideonorm, \
                                                  FilmAudioformat, FilmAudiochannel, FilmColourformat, \
                                                  FilmVideoformat, FilmSoundsystem, FilmGeneral, \
                                                  PersonGender, PersonNationality, PersonSalutation, \
                                                  PersonTitle, PersonHairColor, PersonEyeColor, \
                                                  PersonReligion, PersonRelationsshipStatus, \
                                                  PersonRelationsship, Person, \
                                                  GeneralRegion, GeneralAward, GeneralCountry, \
                                                  GeneralLanguage, GeneralPlace,  GeneralMediaType, \
                                                  GeneralPackaging, GeneralAgeRelease, \
                                                  GeneralManufacturer, \
                                                  MusicGenre, MusicStudio, MusicSPARSCode, MusicPublisher, \
                                                  MusicStyle, MusicLabel, MusicDistribution, \
                                                  BookGenre, BookPublisher, BookBinding, \
                                                  CoinAlloy, CoinCurrency, CoinType, CoinManufacturingProcedure, \
                                                  CoinDegreePreservation, CoinMint, \
                                                  VideogameGenre, \
                                                  AllocationFilmGeneralGeneralCountry, AllocationFilmGeneralFilmGenre

    from normalize_uni_str import normalize_text
    from manage_categories import analyze_categorie

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import SQLAlchemyError, UnboundExecutionError
from sqlalchemy import Date, asc
from sqlalchemy import exc

class MasterDataManipulation(object):

    def __init__(self,
                 scoped_session,
                 configuration_saver_dict = None):

        try:
          # Trying to provide a transactional scope around a series of operations.
          self._session = scoped_session.scoped_session
          self._session.commit()
        except AttributeError:
          pass

        self._configuration_saver_dict = configuration_saver_dict

        self._store_mapped_table = {
                             'film_genre':                    FilmGenre,
                             'film_production_company':       FilmProductionCompany,
                             'film_rentals':                  FilmRentals,
                             'film_distribution':             FilmDistribution,
                             'film_studio':                   FilmStudio,
                             'film_resolution':               FilmResolution,
                             'film_regional_code':            FilmRegionalCode,
                             'film_aspect_ratio':             FilmAspectRatio,
                             'film_dimension':                FilmDimension,
                             'film_edition':                  FilmEdition,
                             'film_version':                  FilmVersion,
                             'film_videonorm':                FilmVideonorm,
                             'film_audioformat':              FilmAudioformat,
                             'film_audio_channel':            FilmAudiochannel,
                             'film_colour_format':            FilmColourformat,
                             'film_videoformat':              FilmVideoformat,
                             'film_soundsystem':              FilmSoundsystem,
                             'music_genre':                   MusicGenre,
                             'music_studio':                  MusicStudio,
                             'music_spars_code':              MusicSPARSCode,
                             'music_publisher':               MusicPublisher,
                             'music_style':                   MusicStyle,
                             'music_label':                   MusicLabel,
                             'music_distribution':            MusicDistribution,
                             'book_genre':                    BookGenre,
                             'book_publisher':                BookPublisher,
                             'book_binding':                  BookBinding,
                             'coin_alloy':                    CoinAlloy,
                             'coin_currency':                 CoinCurrency,
                             'coin_type':                     CoinType,
                             'coin_manufacturing_procedure':  CoinManufacturingProcedure,
                             'coin_degree_preservation':      CoinDegreePreservation,
                             'coin_mint':                     CoinMint,
                             'general_country':               GeneralCountry,
                             'general_language':              GeneralLanguage,
                             'general_region':                GeneralRegion,
                             'general_award':                 GeneralAward,
                             'person_profile':                Person,
                             'person_gender':                 PersonGender,
                             'person_nationality':            PersonNationality,
                             'person_salutation':             PersonSalutation,
                             'person_title':                  PersonTitle,
                             'person_hair_color':             PersonHairColor,
                             'person_eye_color':              PersonEyeColor,
                             'person_religion':               PersonReligion,
                             'person_relationship_status':    PersonRelationsshipStatus,
                             'person_relationship':           PersonRelationsship,
                             'general_packaging':             GeneralPackaging,
                             'general_media_type':            GeneralMediaType,
                             'general_manufacturer':          GeneralManufacturer,
                             'general_age_release':           GeneralAgeRelease,
                             'videogame_genre':               VideogameGenre,

                            }

        self._dict_store_session_query = {
                                     'film_genre':                  lambda: (self._session.query(FilmGenre)
                                                                                          .order_by(asc(
                                                                                            FilmGenre.genre))),
                                     'music_genre':                 lambda: (self._session.query(MusicGenre)
                                                                                          .order_by(asc(
                                                                                            MusicGenre.genre))),
                                     'music_studio':                lambda: (self._session.query(MusicStudio)
                                                                                          .order_by(asc(
                                                                                            MusicStudio.studio))),
                                     'music_spars_code':            lambda: (self._session.query(MusicSPARSCode)
                                                                                          .order_by(asc(
                                                                                            MusicSPARSCode.spars_code))),
                                     'music_publisher':             lambda: (self._session.query(MusicPublisher)
                                                                                          .order_by(asc(
                                                                                            MusicPublisher.publisher))),
                                     'music_style':                 lambda: (self._session.query(MusicStyle)
                                                                                          .order_by(asc(
                                                                                            MusicStyle.style))),
                                     'music_label':                 lambda: (self._session.query(MusicLabel)
                                                                                          .order_by(asc(
                                                                                            MusicLabel.label))),
                                     'music_distribution':          lambda: (self._session.query(MusicDistribution)
                                                                                          .order_by(asc(
                                                                                            MusicDistribution.distribution))),
                                     'book_genre':                  lambda: (self._session.query(BookGenre)
                                                                                          .order_by(asc(
                                                                                            BookGenre.genre))),
                                     'book_publisher':              lambda: (self._session.query(BookPublisher)
                                                                                          .order_by(asc(
                                                                                            BookPublisher.publisher))),
                                     'book_binding':                lambda: (self._session.query(BookBinding)
                                                                                          .order_by(asc(
                                                                                            BookBinding.binding))),
                                     'coin_alloy':                  lambda: (self._session.query(CoinAlloy)
                                                                                          .order_by(asc(
                                                                                            CoinAlloy.alloy))),
                                     'coin_currency':               lambda: (self._session.query(CoinCurrency)
                                                                                          .order_by(asc(
                                                                                            CoinCurrency.currency))),
                                     'coin_type':                   lambda: (self._session.query(CoinType)
                                                                                          .order_by(asc(
                                                                                            CoinType.typeof))),
                                     'coin_manufacturing_procedure':lambda: (self._session.query(CoinManufacturingProcedure)
                                                                                          .order_by(asc(
                                                                                            CoinManufacturingProcedure.manufacturing_procedure))),
                                     'coin_degree_preservation':    lambda: (self._session.query(CoinDegreePreservation)
                                                                                          .order_by(asc(
                                                                                            CoinDegreePreservation.degree_preservation))),
                                     'coin_mint':                   lambda: (self._session.query(CoinMint)
                                                                                          .order_by(asc(
                                                                                            CoinMint.mint))),
                                     'general_country':             lambda: (self._session.query(GeneralCountry)
                                                                                          .order_by(asc(
                                                                                            GeneralCountry.country))),
                                     'general_language':            lambda: (self._session.query(GeneralLanguage)
                                                                                          .order_by(asc(
                                                                                            GeneralLanguage.language))),
                                     'general_region':              lambda: (self._session.query(GeneralRegion)
                                                                                          .order_by(asc(
                                                                                            GeneralRegion.region))),
                                     'general_award':               lambda: (self._session.query(GeneralAward)
                                                                                          .order_by(asc(
                                                                                            GeneralAward.award))),
                                     'general_age_release':         lambda: (self._session.query(GeneralAgeRelease)
                                                                                          .order_by(asc(
                                                                                            GeneralAgeRelease.age_release))),
                                     'person_profile':              lambda: (self._session.query(Person)
                                                                                          .order_by(asc(
                                                                                            Person.name_normally_used))),
                                     'person_gender':               lambda: (self._session.query(PersonGender)
                                                                                          .order_by(asc(
                                                                                            PersonGender.gender))),
                                     'person_nationality':          lambda: (self._session.query(PersonNationality)
                                                                                          .order_by(asc(
                                                                                            PersonNationality.nationality))),
                                     'person_salutation':           lambda: (self._session.query(PersonSalutation)
                                                                                          .order_by(asc(
                                                                                            PersonSalutation.salutation))),
                                     'person_title':                lambda: (self._session.query(PersonTitle)
                                                                                          .order_by(asc(
                                                                                            PersonTitle.title))),
                                     'person_hair_color':           lambda: (self._session.query(PersonHairColor)
                                                                                          .order_by(asc(
                                                                                            PersonHairColor.hair_color))),
                                     'person_eye_color':            lambda: (self._session.query(PersonEyeColor)
                                                                                          .order_by(asc(
                                                                                            PersonEyeColor.eye_color))),
                                     'person_religion':             lambda: (self._session.query(PersonReligion)
                                                                                          .order_by(asc(
                                                                                            PersonReligion.religion))),
                                     'person_relationship_status':  lambda: (self._session.query(PersonRelationsshipStatus)
                                                                                          .order_by(asc(
                                                                                            PersonRelationsshipStatus.relationship_status))),
                                     'person_relationship':         lambda: (self._session.query(PersonRelationsship)
                                                                                          .order_by(asc(
                                                                                            PersonRelationsship.relationship))),
                                     'general_media_type':          lambda: (self._session.query(GeneralMediaType)
                                                                                          .order_by(asc(
                                                                                            GeneralMediaType.media_type))),
                                     'general_manufacturer':        lambda: (self._session.query(GeneralManufacturer)
                                                                                          .order_by(asc(
                                                                                            GeneralManufacturer.manufacturer))),
                                     'general_packaging':           lambda: (self._session.query(GeneralPackaging)
                                                                                          .order_by(asc(
                                                                                            GeneralPackaging.packaging))),
                                     'film_production_company':     lambda: (self._session.query(FilmProductionCompany)
                                                                                          .order_by(asc(
                                                                                            FilmProductionCompany.production_company))),
                                     'film_rentals':                lambda: (self._session.query(FilmRentals)
                                                                                          .order_by(asc(
                                                                                            FilmRentals.rentals))),
                                     'film_distribution':           lambda: (self._session.query(FilmDistribution)
                                                                                          .order_by(asc(
                                                                                            FilmDistribution.distribution))),
                                     'film_studio':                 lambda: (self._session.query(FilmStudio)
                                                                                          .order_by(asc(
                                                                                            FilmStudio.studio))),
                                     'film_resolution':             lambda: (self._session.query(FilmResolution)
                                                                                          .order_by(asc(
                                                                                            FilmResolution.resolution))),
                                     'film_regional_code':          lambda: (self._session.query(FilmRegionalCode)
                                                                                          .order_by(asc(
                                                                                            FilmRegionalCode.regional_code))),
                                     'film_aspect_ratio':           lambda: (self._session.query(FilmAspectRatio)
                                                                                          .order_by(asc(
                                                                                            FilmAspectRatio.aspect_ratio))),
                                     'film_dimension':              lambda: (self._session.query(FilmDimension)
                                                                                          .order_by(asc(
                                                                                            FilmDimension.dimension))),
                                     'film_edition':                lambda: (self._session.query(FilmEdition)
                                                                                          .order_by(asc(
                                                                                            FilmEdition.edition))),
                                     'film_version':                lambda: (self._session.query(FilmVersion)
                                                                                          .order_by(asc(
                                                                                            FilmVersion.version))),
                                     'film_videonorm':              lambda: (self._session.query(FilmVideonorm)
                                                                                          .order_by(asc(
                                                                                            FilmVideonorm.norm))),
                                     'film_audioformat':            lambda: (self._session.query(FilmAudioformat)
                                                                                          .order_by(asc(
                                                                                            FilmAudioformat.format))),
                                     'film_audio_channel':          lambda: (self._session.query(FilmAudiochannel)
                                                                                          .order_by(asc(
                                                                                            FilmAudiochannel.channel))),
                                     'film_colour_format':          lambda: (self._session.query(FilmColourformat)
                                                                                          .order_by(asc(
                                                                                            FilmColourformat.format))),
                                     'film_videoformat':            lambda: (self._session.query(FilmVideoformat)
                                                                                          .order_by(asc(
                                                                                            FilmVideoformat.format))),
                                     'film_soundsystem':            lambda: (self._session.query(FilmSoundsystem)
                                                                                          .order_by(asc(
                                                                                            FilmSoundsystem.system))),
                                     'videogame_genre':             lambda: (self._session.query(VideogameGenre)
                                                                                          .order_by(asc(
                                                                                            VideogameGenre.genre)))
                                     }

    def delete_allocation_records(self,
                                  **kwargs):

            for del_row in kwargs.get('temp_del_list_allocation_film_general_general_country'):

                filmgeneral_id = kwargs.get('filmgeneral_id')
                delete_allocation = session.query(AllocationFilmGeneralGeneralCountry).join(FilmGeneral).join(GeneralCountry).filter(
                  FilmGeneral.id == filmgeneral_id, GeneralCountry.id == del_row[0]).one()

                self._session.delete(delete_allocation)

            self._session.commit()

    def add_allocation_records(self,
                              **kwargs):

        # if kwargs.get('source_record'):

        #     allocation_filmgeneral_filmgenre_list = []

        #     model = kwargs.get('model').temporary_standard_item_model_film_general_production_country

        #     for row in xrange(model.rowCount()):

        #         item = model.index(row, 0, kwargs.get('q_model_index')())

        #         record = self.query_by_id(id = int(item.data()), category = kwargs.get('allocation_category'))

        #         allocation_filmgeneral_filmgenre_list.append(AllocationFilmGeneralGeneralCountry(film_general = kwargs.get('source_record'),
        #                                                                                          general_country = record))
        logger.info('Save all allocation records')
        print ""
        print "add_allocation_records in MasterDataManipulation is called"
        print "add_allocation_records in MasterDataManipulation /  allocation_category: {}".format(kwargs.get('allocation_category'))
        print "add_allocation_records in MasterDataManipulation /  temp_add_list_allocation_film_general_film_genre: {}".format(kwargs.get('temp_add_list_allocation_film_general_film_genre'))
        print ""

        if kwargs.get('allocation_category') == "general_country":

            allocation_film_general_general_country_add_list = []

            for add_row in kwargs.get('temp_add_list_allocation_film_general_general_country'):

              add_record = self.query_by_id(id = int(add_row[0]), category = kwargs.get('allocation_category'))

              allocation_film_general_general_country_add_list.append(AllocationFilmGeneralGeneralCountry(film_general = kwargs.get('source_record'),
                                                                                                        general_country = add_record))

            self._session.add_all(allocation_film_general_general_country_add_list)
            self._session.commit()

        if kwargs.get('allocation_category') == "film_genre":

            allocation_filmgeneral_filmgenre_add_list = []

            for add_row in kwargs.get('temp_add_list_allocation_film_general_film_genre'):

              add_record = self.query_by_id(id = int(add_row[0]), category = kwargs.get('allocation_category'))

              allocation_filmgeneral_filmgenre_add_list.append(AllocationFilmGeneralFilmGenre(film_general = kwargs.get('source_record'),
                                                                                              film_genre = add_record))

            self._session.add_all(allocation_filmgeneral_filmgenre_add_list)
            self._session.commit()

    def bulk_insert_mapped_record(self,
                                  category = None,
                                  set_flush = False,
                                  bulk_list = None):

        model_table_object = self._store_mapped_table[category]

        object_list = []

        try:

            if category == 'film_resolution':

                # We create a list with dictionaryies as mappings by usung a list comprehension.
                object_list = ([{'resolution': resolution,
                                 'presenting_format': presenting_format} for resolution, presenting_format in bulk_list ])

            elif category == 'film_genre':
                # We create a list with dictionaryies as mappings by usung a list comprehension.
                object_list = ([{'genre': genre} for genre in bulk_list ])

            elif category == 'general_media_type':
                # We create a list with dictionaryies as mappings by usung a list comprehension.
                object_list = ([{'media_type': media_type} for media_type in bulk_list ])

            elif category == 'music_genre':

              object_list = ([{'genre': genre} for genre in bulk_list ])

            elif category == 'music_studio':

              object_list = ([{'studio': studio} for studio in bulk_list ])

            elif category == 'music_spars_code':

              object_list = ([{'spars_code': spars_code} for spars_code in bulk_list ])

            elif category == 'music_publisher':

              object_list = ([{'publisher': publisher} for publisher in bulk_list ])

            elif category == 'music_style':

              object_list = ([{'style': style} for style in bulk_list ])

            elif category == 'music_label':

              object_list = ([{'label': label} for label in bulk_list ])

            elif category == 'music_distribution':

              object_list = ([{'distribution': distribution} for distribution in bulk_list ])

            elif category == 'book_genre':

              object_list = ([{'genre': genre} for genre in bulk_list ])

            elif category == 'book_publisher':

              object_list = ([{'publisher': publisher} for publisher in bulk_list ])

            elif category == 'book_binding':

              object_list = ([{'binding': binding} for binding in bulk_list ])

            elif category == 'coin_type':

              object_list = ([{'typeof': typeof} for typeof in bulk_list ])

            elif category == 'coin_alloy':

              object_list = ([{'alloy': alloy} for alloy in bulk_list ])

            elif category == 'coin_currency':

              object_list = ([{'currency': currency} for currency in bulk_list ])

            elif category == 'coin_manufacturing_procedure':

              object_list = ([{'manufacturing_procedure': manufacturing_procedure,
                               'shortcut': shortcut} for manufacturing_procedure, shortcut in bulk_list ])

            elif category == 'coin_degree_preservation':

              object_list = ([{'degree_preservation': degree_preservation,
                               'shortcut': shortcut} for degree_preservation, shortcut in bulk_list ])

            elif category == 'coin_mint':

              object_list = ([{'mint': mint,
                               'mint_mark': mint_mark} for mint, mint_mark in bulk_list ])

            elif category == 'general_country':

              object_list = ([{'country': country} for country in bulk_list ])

            elif category == 'general_language':

              object_list = ([{'language': language} for language in bulk_list ])

            elif category == 'general_age_release':

              object_list = ([{'age_release': age_release} for age_release in bulk_list ])

            elif category == 'general_region':

              object_list = ([{'region': region} for region in bulk_list ])

            elif category == 'general_award':

              object_list = ([{'award': award} for award in bulk_list ])

            elif category == 'general_packaging':

              object_list = ([{'packaging': packaging} for packaging in bulk_list ])

            elif category == 'general_manufacturer':

              object_list = ([{'manufacturer': manufacturer} for manufacturer in bulk_list ])

            elif category == 'person_nationality':

              object_list = ([{'nationality': nationality} for nationality in bulk_list ])

            elif category == 'film_production_company':

              object_list = ([{'production_company': production_company} for production_company in bulk_list ])

            elif category == 'film_rentals':

              object_list = ([{'rentals': rentals} for rentals in bulk_list ])

            elif category == 'film_distribution':

              object_list = ([{'distribution': distribution} for distribution in bulk_list ])

            elif category == 'film_studio':

              object_list = ([{'studio': studio} for studio in bulk_list ])

            elif category == 'person_relationship':

              object_list = ([{'relationship': relationship} for relationship in bulk_list ])

            elif category == 'film_regional_code':

              object_list = ([{'regional_code': regional_code,
                               'region': region} for regional_code, region in bulk_list ])

            elif category == 'film_aspect_ratio':

              object_list = ([{'aspect_ratio': aspect_ratio,
                               'format': format} for aspect_ratio, format in bulk_list ])

            elif category == 'film_dimension':

              object_list = ([{'dimension': dimension} for dimension in bulk_list ])

            elif category == 'film_edition':

              object_list = ([{'edition': edition} for edition in bulk_list ])

            elif category == 'film_version':

              object_list = ([{'version': version} for version in bulk_list ])

            elif category == 'film_videonorm':

              object_list = ([{'norm': norm} for norm in bulk_list ])

            elif category == 'film_audioformat':

              object_list = ([{'format': format} for format in bulk_list ])

            elif category == 'film_audio_channel':

              object_list = ([{'channel': channel} for channel in bulk_list ])

            elif category == 'film_colour_format':

              object_list = ([{'format': format} for format in bulk_list ])

            elif category == 'film_videoformat':

              object_list = ([{'format': format} for format in bulk_list ])

            elif category == 'film_soundsystem':

              object_list = ([{'system': system} for system in bulk_list ])

            elif category == 'videogame_genre':

              object_list = ([{'genre': genre} for genre in bulk_list ])

            self._session.bulk_insert_mappings(model_table_object, object_list)

            #   It will handle the instances one at a time, but
            #   with a large number of records, it just takes too long.
            #self._session.add_all(object_list)
            self._session.commit()

        except SQLAlchemyError as err:

              # Before we rise an exception, we call close()
              # on the current session which, has the effect of
              # releasing any connection/transactional resources
              # owned by the Session first, then discarding the
              # Session itself. After that, we empty the attribute,
              # in next step we save the class of sqlalchemy.orm.session.Session
              # in the attribute.

              self._session.close()
              self._session = None
              #self._session = ScopedSession()

              raise SQLAlchemyError(format_exc(exc_info()))

        return

    def _get_model_attribute(self,
                             **kwargs):

      if kwargs.get('category') == 'general_country':
        return kwargs.get('record_id'), kwargs.get('model').country
      elif kwargs.get('category') == 'general_region':
        return kwargs.get('record_id'), kwargs.get('model').region
      elif kwargs.get('category') == 'general_age_release':
        return kwargs.get('record_id'), kwargs.get('model').age_release
      elif kwargs.get('category') == 'general_language':
        return kwargs.get('record_id'), kwargs.get('model').language
      elif kwargs.get('category') == 'general_award':
        return kwargs.get('record_id'), kwargs.get('model').award
      elif kwargs.get('category') == 'general_media_type':
        return kwargs.get('record_id'), kwargs.get('model').media_type
      elif kwargs.get('category') == 'general_manufacturer':
        return kwargs.get('record_id'), kwargs.get('model').manufacturer
      elif kwargs.get('category') == 'person_nationality':
        return kwargs.get('record_id'), kwargs.get('model').nationality
      elif kwargs.get('category') == 'film_genre':
        return kwargs.get('record_id'), kwargs.get('model').genre
      elif kwargs.get('category') == 'music_genre':
        return kwargs.get('record_id'), kwargs.get('model').genre
      elif kwargs.get('category') == 'music_studio':
        return kwargs.get('record_id'), kwargs.get('model').studio
      elif kwargs.get('category') == 'music_spars_code':
        return kwargs.get('record_id'), kwargs.get('model').spars_code
      elif kwargs.get('category') == 'music_publisher':
        return kwargs.get('record_id'), kwargs.get('model').publisher
      elif kwargs.get('category') == 'music_style':
        return kwargs.get('record_id'), kwargs.get('model').style
      elif kwargs.get('category') == 'music_label':
        return kwargs.get('record_id'), kwargs.get('model').label
      elif kwargs.get('category') == 'music_distribution':
        return kwargs.get('record_id'), kwargs.get('model').distribution
      elif kwargs.get('category') == 'person_gender':
        return kwargs.get('record_id'), kwargs.get('model').gender
      elif kwargs.get('category') == 'person_salutation':
        return kwargs.get('record_id'), kwargs.get('model').salutation
      elif kwargs.get('category') == 'person_profile':
        return kwargs.get('record_id'), kwargs.get('model').person_name_normally_used
      elif kwargs.get('category') == 'person_title':
        return kwargs.get('record_id'), kwargs.get('model').title
      elif kwargs.get('category') == 'person_eye_color':
        return kwargs.get('record_id'), kwargs.get('model').eye_color
      elif kwargs.get('category') == 'person_hair_color':
        return kwargs.get('record_id'), kwargs.get('model').hair_color
      elif kwargs.get('category') == 'person_religion':
        return kwargs.get('record_id'), kwargs.get('model').religion
      elif kwargs.get('category') == 'person_relationship_status':
        return kwargs.get('record_id'), kwargs.get('model').relationship_status
      elif kwargs.get('category') == 'person_relationship':
        return kwargs.get('record_id'), kwargs.get('model').relationship
      elif kwargs.get('category') == 'general_packaging':
        return kwargs.get('record_id'), kwargs.get('model').packaging
      elif kwargs.get('category') == 'film_production_company':
        return kwargs.get('record_id'), kwargs.get('model').production_company
      elif kwargs.get('category') == 'film_rentals':
        return kwargs.get('record_id'), kwargs.get('model').rentals
      elif kwargs.get('category') == 'film_distribution':
        return kwargs.get('record_id'), kwargs.get('model').distribution
      elif kwargs.get('category') == 'film_studio':
        return kwargs.get('record_id'), kwargs.get('model').studio
      elif kwargs.get('category') == 'film_resolution':
        return kwargs.get('record_id'), kwargs.get('model').resolution, kwargs.get('model').presenting_format
      elif kwargs.get('category') == 'film_regional_code':
        return kwargs.get('record_id'), kwargs.get('model').regional_code, kwargs.get('model').region
      elif kwargs.get('category') == 'film_aspect_ratio':
        return kwargs.get('record_id'), kwargs.get('model').aspect_ratio, kwargs.get('model').format
      elif kwargs.get('category') == 'film_dimension':
        return kwargs.get('record_id'), kwargs.get('model').dimension
      elif kwargs.get('category') == 'film_edition':
        return kwargs.get('record_id'), kwargs.get('model').edition
      elif kwargs.get('category') == 'film_version':
        return kwargs.get('record_id'), kwargs.get('model').version
      elif kwargs.get('category') == 'film_videonorm':
        return kwargs.get('record_id'), kwargs.get('model').norm
      elif kwargs.get('category') == 'film_audioformat':
        return kwargs.get('record_id'), kwargs.get('model').format
      elif kwargs.get('category') == 'film_audio_channel':
        return kwargs.get('record_id'), kwargs.get('model').channel
      elif kwargs.get('category') == 'film_colour_format':
        return kwargs.get('record_id'), kwargs.get('model').format
      elif kwargs.get('category') == 'film_videoformat':
        return kwargs.get('record_id'), kwargs.get('model').format
      elif kwargs.get('category') == 'film_soundsystem':
        return kwargs.get('record_id'), kwargs.get('model').system
      elif kwargs.get('category') == 'book_genre':
        return kwargs.get('record_id'), kwargs.get('model').genre
      elif kwargs.get('category') == 'book_publisher':
        return kwargs.get('record_id'), kwargs.get('model').publisher
      elif kwargs.get('category') == 'book_binding':
        return kwargs.get('record_id'), kwargs.get('model').binding
      elif kwargs.get('category') == 'coin_alloy':
        return kwargs.get('record_id'), kwargs.get('model').alloy
      elif kwargs.get('category') == 'coin_currency':
        return kwargs.get('record_id'), kwargs.get('model').currency
      elif kwargs.get('category') == 'coin_type':
        return kwargs.get('record_id'), kwargs.get('model').typeof
      elif kwargs.get('category') == 'coin_manufacturing_procedure':
        return kwargs.get('record_id'), kwargs.get('model').manufacturing_procedure, kwargs.get('model').shortcut
      elif kwargs.get('category') == 'coin_degree_preservation':
        return kwargs.get('record_id'), kwargs.get('model').degree_preservation, kwargs.get('model').shortcut
      elif kwargs.get('category') == 'coin_mint':
        return kwargs.get('record_id'), kwargs.get('model').mint, kwargs.get('model').mint_mark
      elif kwargs.get('category') == 'videogame_genre':
        return kwargs.get('record_id'), kwargs.get('model').genre

      return

    #def

    def add_record(self,
                   **kwargs):
        # NOTICE:
        # =======
        # We neeed this ternary' expressions. For instance, when a user doesn't enter anything or only spaces,
        # we get the empty string ('"), not None, which satisfies the NOT NULL constraint. So we have
        # to check if the given arguments are empty or contain only spaces.

        # if not kwargs.get('film_genre') else normalize_text(text = kwargs.get('film_genre'))
        film_genre = None if not kwargs.get('film_genre') else normalize_text(
          text = unicode(kwargs.get('film_genre')))
        film_production_company = None if not kwargs.get('film_production_company') else normalize_text(
          text = unicode(kwargs.get('film_production_company')))
        film_rentals = None if not kwargs.get('film_rentals') else normalize_text(
          text = unicode(kwargs.get('film_rentals')))
        film_distribution = None if not kwargs.get('film_distribution') else normalize_text(
          text = unicode(kwargs.get('film_distribution')))
        film_dimension = None if not kwargs.get('film_dimension') else normalize_text(
          text = unicode(kwargs.get('film_dimension')))
        film_edition = None if not kwargs.get('film_edition') else normalize_text(
          text = unicode(kwargs.get('film_edition')))
        film_version = None if not kwargs.get('film_version') else normalize_text(
          text = unicode(kwargs.get('film_version')))
        film_videonorm = None if not kwargs.get('film_videonorm') else normalize_text(
          text = unicode(kwargs.get('film_videonorm')))
        film_audioformat = None if not kwargs.get('film_audioformat') else normalize_text(
          text = unicode(kwargs.get('film_audioformat')))
        film_soundsystem = None if not kwargs.get('film_soundsystem') else normalize_text(
          text = unicode(kwargs.get('film_soundsystem')))
        film_audio_channel = None if not kwargs.get('film_audio_channel') else normalize_text(
          text = unicode(kwargs.get('film_audio_channel')))
        film_colour_format = None if not kwargs.get('film_colour_format') else normalize_text(
          text = unicode(kwargs.get('film_colour_format')))
        film_videoformat = None if not kwargs.get('film_videoformat') else normalize_text(
          text = unicode(kwargs.get('film_videoformat')))
        film_studio = None if not kwargs.get('film_studio') else normalize_text(
          text = unicode(kwargs.get('film_studio')))
        film_resolution = None if not kwargs.get('film_resolution') else normalize_text(
          text = unicode(kwargs.get('film_resolution')))
        film_presenting_format = None if not kwargs.get('film_presenting_format') else normalize_text(
          text = unicode(kwargs.get('film_presenting_format')))
        film_aspect_ratio = None if not kwargs.get('film_aspect_ratio') else normalize_text(
          text = unicode(kwargs.get('film_aspect_ratio')))
        film_aspect_ratio_format = None if not kwargs.get('film_aspect_ratio_format') else normalize_text(
          text = unicode(kwargs.get('film_aspect_ratio_format')))
        film_regional_code = None if not kwargs.get('film_regional_code') else normalize_text(
          text = unicode(kwargs.get('film_regional_code')))
        film_region = None if not kwargs.get('film_region') else normalize_text(
          text = unicode(kwargs.get('film_region')))
        film_realse_year = None if not kwargs.get('film_general_release_year') else int(
          kwargs.get('film_general_release_year'))
        film_file_extension_poster = None if not kwargs.get('film_file_extension_poster') else unicode(
          kwargs.get('film_file_extension_poster'))
        film_general_original_title = None if not kwargs.get('film_general_original_title') else unicode(
          kwargs.get('film_general_original_title'))
        film_general_translated_title = None if not kwargs.get('film_general_translated_title') else unicode(
          kwargs.get('film_general_translated_title'))
        #film_general_original_title = None if not kwargs.get('film_general_original_title') else unicode(kwargs.get('film_general_original_title'))

        book_genre = None if not kwargs.get('book_genre') else normalize_text(
          text = unicode(kwargs.get('book_genre')))
        book_publisher = None if not kwargs.get('book_publisher') else normalize_text(
          text = unicode(kwargs.get('book_publisher')))
        book_binding = None if not kwargs.get('book_binding') else normalize_text(
          text = unicode(kwargs.get('book_binding')))

        coin_alloy = None if not kwargs.get('coin_alloy') else normalize_text(
          text = unicode(kwargs.get('coin_alloy')))
        coin_currency = None if not kwargs.get('coin_currency') else normalize_text(
          text = unicode(kwargs.get('coin_currency')))
        coin_type = None if not kwargs.get('coin_type') else normalize_text(
          text = unicode(kwargs.get('coin_type')))
        coin_manufacturing_procedure = None if not kwargs.get('coin_manufacturing_procedure') else normalize_text(
          text = unicode(kwargs.get('coin_manufacturing_procedure')))
        coin_manufacturing_procedure_shortcut = '' if not kwargs.get('coin_manufacturing_procedure_shortcut') else normalize_text(
          text = unicode(kwargs.get('coin_manufacturing_procedure_shortcut')))
        coin_degree_preservation = None if not kwargs.get('coin_degree_preservation') else normalize_text(
          text = unicode(kwargs.get('coin_degree_preservation')))
        coin_degree_preservation_shortcut = '' if not kwargs.get('coin_degree_preservation_shortcut') else normalize_text(
          text = unicode(kwargs.get('coin_degree_preservation_shortcut')))
        coin_mint = None if not kwargs.get('coin_mint') else normalize_text(
          text = unicode(kwargs.get('coin_mint')))
        coin_mint_mark = '' if not kwargs.get('coin_mint_mark') else normalize_text(
          text = unicode(kwargs.get('coin_mint_mark')))

        music_genre = None if not kwargs.get('music_genre') else normalize_text(
          text = unicode(kwargs.get('music_genre')))
        music_studio = None if not kwargs.get('music_studio') else normalize_text(
          text = unicode(kwargs.get('music_studio')))
        music_spars_code = None if not kwargs.get('music_spars_code') else normalize_text(
          text = unicode(kwargs.get('music_spars_code')))
        music_publisher = None if not kwargs.get('music_publisher') else normalize_text(
          text = unicode(kwargs.get('music_publisher')))
        music_style = None if not kwargs.get('music_style') else normalize_text(
          text = unicode(kwargs.get('music_style')))
        music_label = None if not kwargs.get('music_label') else normalize_text(
          text = unicode(kwargs.get('music_label')))
        music_distribution = None if not kwargs.get('music_distribution') else normalize_text(
          text = unicode(kwargs.get('music_distribution')))

        general_country = None if not kwargs.get('general_country') else normalize_text(
          text = unicode(kwargs.get('general_country')))
        general_media_type = None if not kwargs.get('general_media_type') else normalize_text(
          text = unicode(kwargs.get('general_media_type')))
        general_manufacturer = None if not kwargs.get('general_manufacturer') else normalize_text(
          text = unicode(kwargs.get('general_manufacturer')))
        general_language = None if not kwargs.get('general_language') else normalize_text(
          text = unicode(kwargs.get('general_language')))
        general_region = None if not kwargs.get('general_region') else normalize_text(
          text = unicode(kwargs.get('general_region')))
        general_award = None if not kwargs.get('general_award') else normalize_text(
          text = unicode(kwargs.get('general_award')))
        general_packaging = None if not kwargs.get('general_packaging') else normalize_text(
          text = unicode(kwargs.get('general_packaging')))
        general_age_release = None if not kwargs.get('general_age_release') else normalize_text(
          text = unicode(kwargs.get('general_age_release')))

        person_gender = None if not kwargs.get('person_gender') else normalize_text(
          text = unicode(kwargs.get('person_gender')))
        person_nationality = None if not kwargs.get('person_nationality') else normalize_text(
          text = unicode(kwargs.get('person_nationality')))
        person_salutation = None if not kwargs.get('person_salutation') else normalize_text(
          text = unicode(kwargs.get('person_salutation')))
        person_title = None if not kwargs.get('person_title') else normalize_text(
          text = unicode(kwargs.get('person_title')))
        person_hair_color = None if not kwargs.get('person_hair_color') else normalize_text(
          text = unicode(kwargs.get('person_hair_color')))
        person_eye_color = None if not kwargs.get('person_eye_color') else normalize_text(
          text = unicode(kwargs.get('person_eye_color')))
        image_path = None if not kwargs.get('image_path') else kwargs.get('image_path')
        person_religion = None if not kwargs.get('person_religion') else normalize_text(
          text = unicode(kwargs.get('person_religion')))
        person_relationship_status = None if not kwargs.get('person_relationship_status') else normalize_text(
          text = unicode(kwargs.get('person_relationship_status')))
        person_relationship = None if not kwargs.get('person_relationship') else normalize_text(
          text = unicode(kwargs.get('person_relationship')))
        person_file_extension_photo = None if not kwargs.get('person_file_extension_photo') else unicode(
          kwargs.get('person_file_extension_photo'))
        person_name_normally_used = None if not kwargs.get('person_name_normally_used') else unicode(
          kwargs.get('person_name_normally_used'))
        person_first_middle_name = None if not kwargs.get('person_first_middle_name') else unicode(
          kwargs.get('person_first_middle_name'))
        person_last_name = None if not kwargs.get('person_last_name') else unicode(
          kwargs.get('person_last_name'))
        person_nickname = None if not kwargs.get('person_nickname') else unicode(
          kwargs.get('person_nickname'))
        person_artist_name = None if not kwargs.get('person_artist_name') else unicode(
          kwargs.get('person_artist_name'))
        person_birth_name = None if not kwargs.get('person_birth_name') else unicode(
          kwargs.get('person_birth_name'))
        person_note = None if not kwargs.get('person_note') else unicode(
          kwargs.get('person_note'))
        person_birth_date = None if not kwargs.get('person_birth_date') else kwargs.get('person_birth_date')
        person_death_date = None if not kwargs.get('person_death_date') else kwargs.get('person_death_date')
        person_wedding_date = None if not kwargs.get('person_wedding_date') else kwargs.get('person_wedding_date')
        person_body_height = None if not kwargs.get('person_body_height') else kwargs.get('person_body_height')
        combo_person_salutation = None if not kwargs.get('combo_person_salutation') else unicode(
          kwargs.get('combo_person_salutation'))
        combo_person_gender = None if not kwargs.get('combo_person_gender') else unicode(
          kwargs.get('combo_person_gender'))
        combo_person_religion = None if not kwargs.get('combo_person_religion') else unicode(
          kwargs.get('combo_person_religion'))
        combo_person_title = None if not kwargs.get('combo_person_title') else unicode(
          kwargs.get('combo_person_title'))
        combo_person_relationsship_status = None if not kwargs.get('combo_person_relationsship_status') else unicode(
          kwargs.get('combo_person_relationsship_status'))
        combo_person_eye_color = None if not kwargs.get('combo_person_eye_color') else unicode(
          kwargs.get('combo_person_eye_color'))
        combo_person_hair_color = None if not kwargs.get('combo_person_hair_color') else unicode(
          kwargs.get('combo_person_hair_color'))
        record_added_day = None if not kwargs.get('record_added_day') else unicode(
          kwargs.get('record_added_day'))

        videogame_genre = None if not kwargs.get('videogame_genre') else unicode(
          kwargs.get('videogame_genre'))

        dict_store_models = {'film_genre':                    FilmGenre(
                                                                genre = film_genre),
                             'film_production_company':       FilmProductionCompany(
                                                                production_company = film_production_company),
                             'film_rentals':                  FilmRentals(
                                                                rentals = film_rentals),
                             'film_distribution':             FilmDistribution(
                                                                distribution = film_distribution),
                             'film_studio':                   FilmStudio(
                                                                studio = film_studio),
                             'film_resolution':               FilmResolution(
                                                                resolution = film_resolution,
                                                                presenting_format = film_presenting_format),
                             'film_regional_code':            FilmRegionalCode(
                                                                regional_code = film_regional_code,
                                                                region = film_region),
                             'music_genre':                   MusicGenre(
                                                                genre = music_genre),
                             'music_studio':                  MusicStudio(
                                                                studio = music_studio),
                             'music_spars_code':              MusicSPARSCode(
                                                                spars_code = music_spars_code),
                             'music_publisher':               MusicPublisher(
                                                                publisher = music_publisher),
                             'music_style':                   MusicStyle(
                                                                style = music_style),
                             'music_label':                   MusicLabel(
                                                                label = music_label),
                             'music_distribution':            MusicDistribution(
                                                                distribution = music_distribution),
                             'general_country':               GeneralCountry(
                                                                country = general_country),
                             'general_media_type':            GeneralMediaType(
                                                                media_type = general_media_type),
                             'general_manufacturer':          GeneralManufacturer(
                                                                manufacturer = general_manufacturer),
                             'general_language':              GeneralLanguage(
                                                                language = general_language),
                             'general_region':                GeneralRegion(
                                                                region = general_region),
                             'general_award':                 GeneralAward(
                                                                award = general_award),
                             'general_age_release':           GeneralAgeRelease(
                                                                age_release = general_age_release),
                             'book_genre':                    BookGenre(
                                                                genre = book_genre),
                             'book_publisher':                BookPublisher(
                                                                publisher = book_publisher),
                             'book_binding':                  BookBinding(
                                                                binding = book_binding),
                             'coin_alloy':                    CoinAlloy(
                                                                alloy= coin_alloy),
                             'coin_currency':                 CoinCurrency(
                                                                currency= coin_currency),
                             'coin_type':                     CoinType(
                                                                typeof= coin_type),
                             'coin_manufacturing_procedure':  CoinManufacturingProcedure(
                                                                manufacturing_procedure = coin_manufacturing_procedure,
                                                                shortcut = coin_manufacturing_procedure_shortcut),
                             'coin_degree_preservation':      CoinDegreePreservation(
                                                                degree_preservation = coin_degree_preservation,
                                                                shortcut = coin_degree_preservation_shortcut),
                             'coin_mint':                     CoinMint(
                                                                mint = coin_mint,
                                                                mint_mark = coin_mint_mark),
                             'person_gender':                 PersonGender(
                                                                gender = person_gender),
                             'person_nationality':            PersonNationality(
                                                                nationality = person_nationality),
                             'person_salutation':             PersonSalutation(
                                                                salutation = person_salutation),
                             'person_title':                  PersonTitle(
                                                                title = person_title),
                             'person_hair_color':             PersonHairColor(
                                                                hair_color = person_hair_color),
                             'person_eye_color':              PersonEyeColor(
                                                                eye_color = person_eye_color),
                             'person_religion':               PersonReligion(
                                                                religion = person_religion),
                             'person_relationship_status':    PersonRelationsshipStatus(
                                                                relationship_status = person_relationship_status),
                             'person_relationship':           PersonRelationsship(
                                                                relationship = person_relationship),
                             'general_packaging':             GeneralPackaging(
                                                                packaging = general_packaging),
                             'film_aspect_ratio':             FilmAspectRatio(
                                                                aspect_ratio = film_aspect_ratio,
                                                                format = film_aspect_ratio_format),
                             'film_dimension':                FilmDimension(
                                                                dimension = film_dimension),
                             'film_edition':                  FilmEdition(
                                                                edition = film_edition),
                             'film_version':                  FilmVersion(
                                                                version = film_version),
                             'film_videonorm':                FilmVideonorm(
                                                                norm = film_videonorm),
                             'film_audioformat':              FilmAudioformat(
                                                                format = film_audioformat),
                             'film_audio_channel':            FilmAudiochannel(
                                                                channel = film_audio_channel),
                             'film_colour_format':            FilmColourformat(
                                                                format = film_colour_format),
                             'film_videoformat':              FilmVideoformat(
                                                                format = film_videoformat),
                             'film_soundsystem':              FilmSoundsystem(
                                                                system = film_soundsystem),
                             'videogame_genre':               VideogameGenre(
                                                                genre = videogame_genre),

                            }

        dict_store_models_person_profile = {'person': Person(name_normally_used = person_name_normally_used,
                                                             alias_name = person_artist_name,
                                                             first_middle_name = person_first_middle_name,
                                                             birth_name = person_birth_name,
                                                             birthday = person_birth_date,
                                                             wedding_anniversary = person_wedding_date,
                                                             day_of_death = person_death_date,
                                                             last_name = person_last_name,
                                                             notice = person_note,
                                                             photo_file_extension = person_file_extension_photo,
                                                             nickname = person_nickname,
                                                             body_height = person_body_height,
                                                             record_added_day = record_added_day,
                                                             salutation  = self.query_by_filter(category = 'person_salutation',
                                                                    filter_text = combo_person_salutation),
                                                             gender = self.query_by_filter(category = 'person_gender',
                                                                    filter_text = combo_person_gender),
                                                             title = self.query_by_filter(category = 'person_title',
                                                                    filter_text = combo_person_title),
                                                             hair_color = self.query_by_filter(category = 'person_hair_color',
                                                                    filter_text = combo_person_hair_color),
                                                             eye_color = self.query_by_filter(category = 'person_eye_color',
                                                                    filter_text = combo_person_eye_color),
                                                             religion = self.query_by_filter(category = 'person_religion',
                                                                    filter_text = combo_person_religion),
                                                             relationship_status = self.query_by_filter(category = 'person_relationship_status',
                                                                    filter_text = combo_person_relationsship_status),)
                                            }

        dict_store_models_film_general = {'film_general': FilmGeneral(translated_title = film_general_translated_title,
                                                                      original_title = film_general_original_title,
                                                                      release_year = film_realse_year,
                                                                      genre_evaluation = kwargs.get('film_general_genre_evalution'),
                                                                      total_evaluation = kwargs.get('film_general_total_evaluation'),
                                                                      poster_file_extension = film_file_extension_poster,
                                                                      sound_system = self.query_by_filter(category = 'film_soundsystem',
                                                                                                          filter_text = film_soundsystem),
                                                                      colour_format = self.query_by_filter(category = 'film_colour_format',
                                                                                                          filter_text = film_colour_format),
                                                                      )
                                         }

        if kwargs.get('work_area') == 'person_profile':
            self.model_name = dict_store_models_person_profile[kwargs.get('category')]
        elif kwargs.get('work_area') =='film_general':
            self.model_name = dict_store_models_film_general[kwargs.get('category')]
        elif kwargs.get('work_area') == 'master_data':
            self.model_name = dict_store_models[kwargs.get('category')]

        try:

            self._session.add(self.model_name)

            self._session.commit()

            try:

              list_category = ['film_genre', 'general_country']

              for single_category in list_category:
                  logger.info("single_category: {}".format(single_category))
                  # self.add_allocation_records(temp_add_list_allocation_film_general_general_country = kwargs.get('temp_add_list_allocation_film_general_general_country'),
                  #                             temp_add_list_allocation_film_general_film_genre = kwargs.get('temp_add_list_allocation_film_general_film_genre'),
                  #                             source_record = self.model_name,
                  #                             allocation_category = single_category)

            except AttributeError:

              desired_trace = format_exc(exc_info())

              logger.error(desired_trace)

            except TypeError:

              desired_trace = format_exc(exc_info())

              if desired_trace.find("'NoneType' object is not iterable") != -1 :
                print "PRRRRROOOOBLEEEM"



            if not image_path is None:

                folder = None

                file_extension = path.splitext(image_path)[1]

                base_path = self._configuration_saver_dict.dict_set_general_settings["BasePath"]

                if kwargs.get('category') == 'film_general':

                    folder = path.join(base_path, 'images', 'movie', 'poster')

                copy_file(source_file = image_path,
                          destination_file = folder ,
                          file_name = '{id}{file_extension}'.format(id = self.model_name.id,
                                                                    file_extension = file_extension))
            if kwargs.get('set_flush'):

              print "flush is set on True"

              model_attribute = self._get_model_attribute(record_id = self.model_name.id,
                                                          category = kwargs.get('category'),
                                                          model = self.model_name)

              return model_attribute


        except SQLAlchemyError as e:
            #print "master_data_manipuöation, SQLAlchemyError", e
            # Before we rise an exception, we call close()
            # on the current session which, has the effect of
            # releasing any connection/transactional resources
            # owned by the Session first, then discarding the
            # Session itself. After that, we empty the attribute,
            # in next step we save the class of sqlalchemy.orm.session.Session
            # in the attribute.

            self._session.close()
            self._session = None

            #print "format_exc(exc_info())", format_exc(exc_info())

            raise SQLAlchemyError(format_exc(exc_info()))

        except shutil_error:

          pass

        return

    def edit_record(self,
                   **kwargs):

        '''
            Select the item you are trying to update and store it in a variable:

            Like any ordinary object in python, you can always access an orm
            object's properties (classvar.classproperty) and modify its values
            (classvar.classproperty = newvalue).
        '''
        '''
            NOTICE:
            =======
            In this method we want to get all rows from a table. This is a generator,
            it is especially useful when you expect your SELECT query to return a huge result
            set that would be too large to load into memory:
            the for loop will only fetch one row at a time from the database.

            PARAMETERS:
            ===========
            :id         -

            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        print""
        print "MasterDataManipulation edit_record is called successfully"
        print "MasterDataManipulation in edit_record / **kwargs: -> \n {}".format(kwargs)
        record = None

        if kwargs.get('id'):
            record = self.query_by_id(id = int(kwargs.get('id')), category = kwargs.get('category'))
        else:
            raise AttributeError('Invalid Input, id')

        if kwargs.get('category') == 'film_genre':
            if kwargs.get('film_genre'):
                record.genre = normalize_text(text = unicode(kwargs.get('film_genre')))

                try:
                  self._session.commit()
                except SQLAlchemyError as e:
                    # Before we rise an exception, we call close()
                    # on the current session which, has the effect of
                    # releasing any connection/transactional resources
                    # owned by the Session first, then discarding the
                    # Session itself. After that, we empty the attribute,
                    # in next step we save the class of sqlalchemy.orm.session.Session
                    # in the attribute.

                    self._session.close()
                    self._session = None

                    raise SQLAlchemyError(format_exc(exc_info()))

            else:
                raise AttributeError('Invalid Input, film_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.genre

        elif kwargs.get('category') == 'film_production_company':
            if kwargs.get('film_production_company'):
                record.production_company = normalize_text(text = unicode(kwargs.get('film_production_company')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, film_production_company')

            if kwargs.get('set_flush', False):
                return record.id, record.production_company

        elif kwargs.get('category') == 'film_rentals':
            if kwargs.get('film_rentals'):
                record.rentals = normalize_text(text = unicode(kwargs.get('film_rentals')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, film_rentals')

            if kwargs.get('set_flush', False):
                return record.id, record.rentals

        elif kwargs.get('category') == 'film_distribution':
            if kwargs.get('film_distribution'):
                record.distribution = normalize_text(text = unicode(kwargs.get('film_distribution')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, film_distribution')

            if kwargs.get('set_flush', False):
                return record.id, record.distribution

        elif kwargs.get('category') == 'film_studio':
            if kwargs.get('film_studio'):
                record.studio = normalize_text(text = unicode(kwargs.get('film_studio')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, film_studio')

            if kwargs.get('set_flush', False):
                return record.id, record.studio

        elif kwargs.get('category') == 'film_resolution':
            if kwargs.get('film_resolution'):
                record.resolution = normalize_text(text = unicode(kwargs.get('film_resolution')))
                record.presenting_format = normalize_text(text = unicode(kwargs.get('film_presenting_format')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.resolution, record.presenting_format

        elif kwargs.get('category') == 'film_regional_code':
            if kwargs.get('film_regional_code'):
                record.regional_code = normalize_text(text = unicode(kwargs.get('film_regional_code')))
                record.region = normalize_text(text = unicode(kwargs.get('film_region')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.regional_code, record.region

        elif kwargs.get('category') == 'book_genre':
            if kwargs.get('book_genre'):
                record.genre = normalize_text(text = unicode(kwargs.get('book_genre')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.genre

        elif kwargs.get('category') == 'book_publisher':
            if kwargs.get('book_publisher'):
                record.publisher = normalize_text(text = unicode(kwargs.get('book_publisher')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.publisher

        elif kwargs.get('category') == 'book_binding':
            if kwargs.get('book_binding'):
                record.binding = normalize_text(text = unicode(kwargs.get('book_binding')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.binding

        elif kwargs.get('category') == 'coin_type':
            if kwargs.get('coin_type'):
                record.typeof = normalize_text(text = unicode(kwargs.get('coin_type')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.typeof

        elif kwargs.get('category') == 'coin_alloy':
            if kwargs.get('coin_alloy'):
                record.alloy = normalize_text(text = unicode(kwargs.get('coin_alloy')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.alloy

        elif kwargs.get('category') == 'coin_currency':
            if kwargs.get('coin_currency'):
                record.currency = normalize_text(text = unicode(kwargs.get('coin_currency')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.currency

        elif kwargs.get('category') == 'coin_manufacturing_procedure':
            if kwargs.get('coin_manufacturing_procedure'):
                record.manufacturing_procedure = normalize_text(text = unicode(kwargs.get('coin_manufacturing_procedure')))
                record.shortcut = normalize_text(text = unicode(kwargs.get('coin_manufacturing_procedure_shortcut')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.manufacturing_procedure, record.shortcut

        elif kwargs.get('category') == 'coin_degree_preservation':
            if kwargs.get('coin_degree_preservation'):
                record.coin_degree_preservation = normalize_text(text = unicode(kwargs.get('coin_degree_preservation')))
                record.shortcut = normalize_text(text = unicode(kwargs.get('coin_degree_preservation_shortcut')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.coin_degree_preservation, record.shortcut

        elif kwargs.get('category') == 'coin_mint':
            if kwargs.get('coin_mint'):
                record.mint = normalize_text(text = unicode(kwargs.get('coin_mint')))
                record.mint_mark = normalize_text(text = unicode(kwargs.get('coin_mint_mark')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.mint, record.mint_mark

        elif kwargs.get('category') == 'music_genre':
            if kwargs.get('music_genre'):
                record.genre = normalize_text(text = unicode(kwargs.get('music_genre')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.genre

        elif kwargs.get('category') == 'music_studio':
            if kwargs.get('music_studio'):
                record.studio = normalize_text(text = unicode(kwargs.get('music_studio')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.studio

        elif kwargs.get('category') == 'music_spars_code':
            if kwargs.get('music_spars_code'):
                record.spars_code = normalize_text(text = unicode(kwargs.get('music_spars_code')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.spars_code

        elif kwargs.get('category') == 'music_publisher':
            if kwargs.get('music_publisher'):
                record.publisher = normalize_text(text = unicode(kwargs.get('music_publisher')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.publisher

        elif kwargs.get('category') == 'music_style':
            if kwargs.get('music_style'):
                record.style = normalize_text(text = unicode(kwargs.get('music_style')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.style

        elif kwargs.get('category') == 'music_label':
            if kwargs.get('music_label'):
                record.label = normalize_text(text = unicode(kwargs.get('music_label')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.label

        elif kwargs.get('category') == 'music_distribution':
            if kwargs.get('music_distribution'):
                record.distribution = normalize_text(text = unicode(kwargs.get('music_distribution')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, music_genre')

            if kwargs.get('set_flush', False):
                return record.id, record.distribution

        elif kwargs.get('category') == 'general_country':
            if kwargs.get('general_country'):
                record.country = normalize_text(text = unicode(kwargs.get('general_country')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_country')

            if kwargs.get('set_flush', False):
                return record.id, record.country

        elif kwargs.get('category') == 'general_age_release':
            if kwargs.get('general_age_release'):
                record.age_release = normalize_text(text = unicode(kwargs.get('general_age_release')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_country')

            if kwargs.get('set_flush', False):
                return record.id, record.age_release

        elif kwargs.get('category') == 'general_media_type':
            if kwargs.get('general_media_type'):
                record.media_type = normalize_text(text = unicode(kwargs.get('general_media_type')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_media_type')

            if kwargs.get('set_flush', False):
                return record.id, record.media_type

        elif kwargs.get('category') == 'general_manufacturer':
            if kwargs.get('general_manufacturer'):
                record.manufacturer = normalize_text(text = unicode(kwargs.get('general_manufacturer')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_manufacturer')

            if kwargs.get('set_flush', False):
                return record.id, record.manufacturer

        elif kwargs.get('category') == 'general_language':
            if kwargs.get('general_language'):
                record.language = normalize_text(text = unicode(kwargs.get('general_language')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_language')

            if kwargs.get('set_flush', False):
                return record.id, record.language

        elif kwargs.get('category') == 'general_region':
            if kwargs.get('general_region'):
                record.region = normalize_text(text = unicode(kwargs.get('general_region')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_region')

            if kwargs.get('set_flush', False):
                return record.id, record.region

        elif kwargs.get('category') == 'general_award':
            if kwargs.get('general_award'):
                record.award = normalize_text(text = unicode(kwargs.get('general_award')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input, general_award')

            if kwargs.get('set_flush', False):
                return record.id, record.award

        elif kwargs.get('category') == 'general_packaging':
            if kwargs.get('general_packaging'):
                record.packaging = normalize_text(text = unicode(kwargs.get('general_packaging')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, general_packaging')

            if kwargs.get('set_flush', False):
                return record.id, record.packaging

        elif kwargs.get('category') == 'person_gender':
            if kwargs.get('person_gender'):
                record.gender = normalize_text(text = unicode(kwargs.get('person_gender')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_gender')

            if kwargs.get('set_flush', False):
                return record.id, record.gender

        elif kwargs.get('category') == 'person_nationality':
            if kwargs.get('person_nationality'):
                record.nationality = normalize_text(text = unicode(kwargs.get('person_nationality')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_nationality')

            if kwargs.get('set_flush', False):
                return record.id, record.nationality

        elif kwargs.get('category') == 'person_salutation':
            if kwargs.get('person_salutation'):
                record.salutation = normalize_text(text = unicode(kwargs.get('person_salutation')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_salutation')

            if kwargs.get('set_flush', False):
                return record.id, record.salutation

        elif kwargs.get('category') == 'person_title':
            if kwargs.get('person_title'):
                record.title = normalize_text(text = unicode(kwargs.get('person_title')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_title')

            if kwargs.get('set_flush', False):
                return record.id, record.title

        elif kwargs.get('category') == 'person_hair_color':
            if kwargs.get('person_hair_color'):
                record.hair_color = normalize_text(text = unicode(kwargs.get('person_hair_color')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_hair_color')

            if kwargs.get('set_flush', False):
                return record.id, record.hair_color

        elif kwargs.get('category') == 'person_eye_color':
            if kwargs.get('person_eye_color'):
                record.eye_color = normalize_text(text = unicode(kwargs.get('person_eye_color')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_eye_color')

            if kwargs.get('set_flush', False):
                return record.id, record.eye_color

        elif kwargs.get('category') == 'person_religion':
            if kwargs.get('person_religion'):
                record.religion = normalize_text(text = unicode(kwargs.get('person_religion')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_religion')

            if kwargs.get('set_flush', False):
                return record.id, record.religion

        elif kwargs.get('category') == 'person_relationship_status':
            if kwargs.get('person_relationship_status'):
                record.relationship_status = normalize_text(text = unicode(kwargs.get('person_relationship_status')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_relationship_status')

            if kwargs.get('set_flush', False):
                return record.id, record.relationship_status

        elif kwargs.get('category') == 'person_relationship':
            if kwargs.get('person_relationship'):
                record.relationship = normalize_text(text = unicode(kwargs.get('person_relationship')))
                self._session.commit()

            else:
                raise AttributeError('Invalid Input, person_relationship')

            if kwargs.get('set_flush', False):
                return record.id, record.relationship

        elif kwargs.get('category') == 'film_aspect_ratio':
            if kwargs.get('film_aspect_ratio'):
                record.aspect_ratio = normalize_text(text = unicode(kwargs.get('film_aspect_ratio')))
                record.format = normalize_text(text = unicode(kwargs.get('film_aspect_ratio_format')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.aspect_ratio, record.format

        elif kwargs.get('category') == 'film_dimension':
            if kwargs.get('film_dimension'):
                record.dimension = normalize_text(text = unicode(kwargs.get('film_dimension')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.dimension

        elif kwargs.get('category') == 'film_edition':
            if kwargs.get('film_edition'):
                record.edition = normalize_text(text = unicode(kwargs.get('film_edition')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.edition

        elif kwargs.get('category') == 'film_version':
            if kwargs.get('film_version'):
                record.version = normalize_text(text = unicode(kwargs.get('film_version')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.version

        elif kwargs.get('category') == 'film_videonorm':
            if kwargs.get('film_videonorm'):
                record.norm = normalize_text(text = unicode(kwargs.get('film_videonorm')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.norm

        elif kwargs.get('category') == 'film_audioformat':
            if kwargs.get('film_audioformat'):
                record.format = normalize_text(text = unicode(kwargs.get('film_audioformat')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.format

        elif kwargs.get('category') == 'film_audio_channel':
            if kwargs.get('film_audio_channel'):
                record.channel = normalize_text(text = unicode(kwargs.get('film_audio_channel')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.channel

        elif kwargs.get('category') == 'film_colour_format':
            if kwargs.get('film_colour_format'):
                record.format = normalize_text(text = unicode(kwargs.get('film_colour_format')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.format

        elif kwargs.get('category') == 'film_videoformat':
            if kwargs.get('film_videoformat'):
                record.format = normalize_text(text = unicode(kwargs.get('film_videoformat')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.format

        elif kwargs.get('category') == 'film_soundsystem':
            if kwargs.get('film_soundsystem'):
                record.system = normalize_text(text = unicode(kwargs.get('film_soundsystem')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.system

        elif kwargs.get('category') == 'videogame_genre':
            if kwargs.get('videogame_genre'):
                record.genre = normalize_text(text = unicode(kwargs.get('videogame_genre')))
                self._session.commit()
            else:
                raise AttributeError('Invalid Input')

            if kwargs.get('set_flush', False):
                return record.id, record.genre

        # if category == 'person_profile':
        #     record.nickname = person_nickname
        #     record.alias_name = person_artist_name
        #     record.photo_file_extension = person_file_extension_photo
        #     record.name_normally_used = person_name_normally_used
        #     record.first_middle_name = person_first_middle_name
        #     record.last_name = person_last_name
        #     record.birth_name = person_birth_name
        #     record.body_height = person_body_height
        #     record.wedding_anniversary = person_wedding_date
        #     record.birthday = person_birth_date
        #     record.day_of_death = person_death_date
        #     record.notice = person_note
        #     record.record_edited_day = record_edited_day

        #     if not image_path == "None":

        #       #   Sometimes the user want change the current record,
        #       #   but he doesn't want to change the image. By default,
        #       #   the variable 'save_image' is False, that means, the user
        #       #   doens't want to change the image of the current record.
        #       if save_image:

        #         #file_extension = path.splitext(image_path)[1]

        #         base_path = self._configuration_saver_dict.dict_set_general_settings["BasePath"]

        #         folder = path.join(base_path, 'images', category[0:6])

        #         try:

        #           copy_file(source_file = image_path,
        #                     destination_file = folder ,
        #                     file_name = '{id}.{file_extension}'.format(id = record.id,
        #                                                               file_extension = person_file_extension_photo))

        #         except Error:
        #           #If the source and destination files are the same,
        #           #we don't copy them and notify the user of the failed operation.
        #           pass

        #     self._session.commit()

        # # The commiting the changes
        # #session.commit()

        return

    def delete_record(self,
                      **kwargs):
        print ""
        print "MasterDataManipulation delete_record is called successfully"
        '''
            NOTICE:
            =======
            In this method we want to get all rows from a table. This is a generator,
            it is especially useful when you expect your SELECT query to return a huge result
            set that would be too large to load into memory:
            the for loop will only fetch one row at a time from the database.

            PARAMETERS:
            ===========
            :id         -

            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        try:

            record = self.query_by_id(id = kwargs.get('id'),
                                      category = kwargs.get('category'))
            self._session.delete(record)
            self._session.commit()

        except ValueError:
            desired_trace = format_exc(exc_info())
            # Sometimes the user fogets to fill the field, which
            # must be filled. In this case we raise an ecxeption
            # named SQLAlchemyError and and pass the traceback
            if not unicode(desired_trace).find("invalid literal for int() with base 10:") == -1:
                # Before we rise an exception, we call close()
                # on the current session which, has the effect of
                # releasing any connection/transactional resources
                # owned by the Session first, then discarding the
                # Session itself
                #ScopedSession.close()

                raise ValueError(format_exc(exc_info()))
           #session.query(model_name).filter(model_name.id=="{}".format(int(id))).delete()

        return


    def count_rows(self,
                   **kwargs):
        '''
            NOTICE:
            =======
            This method is used to get the count of final result.

            PARAMETERS:
            ===========
            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return    -    Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        try:

            record = self._dict_store_session_query[kwargs.get('category')]()

            try:

                record = self._dict_store_session_query[kwargs.get('category')]()
                return record.count()

            except UnboundExecutionError as err:
                self._session.close()
                self._session = None
                #self._session = ScopedSession()

                desired_trace = format_exc(exc_info())

                logger.error(desired_trace)

                raise UnboundExecutionError(format_exc(exc_info()))

            except SQLAlchemyError as err:
                # Before we raise an exception, we call close()
                # on the current session which, has the effect of
                # releasing any connection/transactional resources
                # owned by the Session first, then discarding the
                # Session itself. After that, we empty the attribute,
                # in next step we save the class of sqlalchemy.orm.session.Session
                # in the attribute.

                self._session.close()
                self._session = None
                #self._session = ScopedSession()

                raise SQLAlchemyError(format_exc(exc_info()))

        except AttributeError as err:

            raise AttributeError(format_exc(exc_info()))

        except KeyError as err:

            raise KeyError(format_exc(exc_info()))

        return

    def fetch_filtered_item(self,
                            **kwargs):

        '''
            NOTICE:
            =======
            In this method we want to get filtered item from a table. This is a generator,
            it is especially useful when you expect your SELECT query to return a huge result
            set that would be too large to load into memory:
            the for loop will only fetch one row at a time from the database.

            PARAMETERS:
            ===========
            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        try:

            record = self.query_by_filter(category = kwargs.get('category'),
                                          filter_text = kwargs.get('filter_text'))

            try:

                if kwargs.get('category') == 'general_region':

                    return record.id, record.region

                elif kwargs.get('category') == 'general_country':

                    return record.id, record.country

                elif kwargs.get('category') == 'general_age_release':

                    return record.id, record.age_release

                elif kwargs.get('category') == 'general_media_type':

                    return record.id, record.media_type

                elif kwargs.get('category') == 'general_manufacturer':

                    return record.id, record.manufacturer

                elif kwargs.get('category') == 'general_packaging':

                    return record.id, record.packaging

                elif kwargs.get('category') == 'general_language':

                    return record.id, record.language

                elif kwargs.get('category') == 'person_salutation':

                    return record.id, record.salutation

                elif kwargs.get('category') == 'person_title':

                    return record.id, record.title

                elif kwargs.get('category') == 'person_eye_color':

                    return record.id, record.eye_color

                elif kwargs.get('category') == 'person_hair_color':

                    return record.id, record.hair_color

                elif kwargs.get('category') == 'person_religion':

                    return record.id, record.religion

                elif kwargs.get('category') == 'person_relationship_status':

                    return record.id, record.relationship_status

                elif kwargs.get('category') == 'person_relationship':

                    return record.id, record.relationship

                elif kwargs.get('category') == 'general_award':

                    return record.id, record.award

                elif kwargs.get('category') == 'person_nationality':

                    return record.id, record.nationality

                elif kwargs.get('category') == 'film_genre':

                    return record.id, record.genre

                elif kwargs.get('category') == 'film_production_company':

                    return record.id, record.production_company

                elif kwargs.get('category') == 'film_rentals':

                    return record.id, record.rentals

                elif kwargs.get('category') == 'film_distribution':

                    return record.id, record.distribution

                elif kwargs.get('category') == 'film_studio':

                    return record.id, record.studio

                elif kwargs.get('category') == 'film_resolution':

                    return record.id, record.resolution, record.presenting_format

                elif kwargs.get('category') == 'film_regional_code':

                    return record.id, record.regional_code, record.region

                elif kwargs.get('category') == 'book_genre':

                    return record.id, record.genre

                elif kwargs.get('category') == 'book_publisher':

                    return record.id, record.publisher

                elif kwargs.get('category') == 'book_binding':

                    return record.id, record.binding

                elif kwargs.get('category') == 'coin_alloy':

                    return record.id, record.alloy

                elif kwargs.get('category') == 'coin_currency':

                    return record.id, record.currency

                elif kwargs.get('category') == 'coin_type':

                    return record.id, record.typeof

                elif kwargs.get('category') == 'coin_manufacturing_procedure':

                    return record.id, record.manufacturing_procedure, record.shortcut

                elif kwargs.get('category') == 'coin_degree_preservation':

                    return record.id, record.degree_preservation, record.shortcut

                elif kwargs.get('category') == 'coin_mint':

                    return record.id, record.mint, record.mint_mark

                elif kwargs.get('category') == 'music_genre':

                    return record.id, record.genre

                elif kwargs.get('category') == 'music_studio':

                    return record.id, record.studio

                elif kwargs.get('category') == 'music_spars_code':

                    return record.id, record.spars_code

                elif kwargs.get('category') == 'music_publisher':

                    return record.id, record.publisher

                elif kwargs.get('category') == 'music_style':

                    return record.id, record.style

                elif kwargs.get('category') == 'music_label':

                    return record.id, record.label

                elif kwargs.get('category') == 'music_distribution':

                    return record.id, record.distribution

                elif kwargs.get('category') == 'film_aspect_ratio':

                    return record.id, record.aspect_ratio, record.format

                elif kwargs.get('category') == 'film_dimension':

                    return record.id, record.dimension

                elif kwargs.get('category') == 'film_edition':

                    return record.id, record.edition

                elif kwargs.get('category') == 'film_version':

                    return record.id, record.version

                elif kwargs.get('category') == 'film_videonorm':

                    return record.id, record.norm

                elif kwargs.get('category') == 'film_audioformat':

                    return record.id, record.format

                elif kwargs.get('category') == 'film_audio_channel':

                    return record.id, record.channel

                elif kwargs.get('category') == 'film_colour_format':

                    return record.id, record.format

                elif kwargs.get('category') == 'film_videoformat':

                    return record.id, record.format

                elif kwargs.get('category') == 'film_soundsystem':

                    return record.id, record.system

                elif kwargs.get('category') == 'videogame_genre':

                    return record.id, record.genre

            except SQLAlchemyError as err:

                # Before we rise an exception, we call close()
                # on the current session which, has the effect of
                # releasing any connection/transactional resources
                # owned by the Session first, then discarding the
                # Session itself. After that, we empty the attribute,
                # in next step we save the class of sqlalchemy.orm.session.Session
                # in the attribute.

                self._session.close()
                self._session = None
                #self._session = ScopedSession()

                raise SQLAlchemyError(format_exc(exc_info()))

            except UnboundExecutionError as err:

                self._session.close()
                self._session = None
                #self._session = ScopedSession()

                raise SQLAlchemyError(format_exc(exc_info()))

        except AttributeError as err:

            desired_trace = format_exc(exc_info())

        return

    def fetch_all_items(self,
                        **kwargs):
        '''
            NOTICE:
            =======
            In this method we want to get all rows from a table. This is a generator,
            it is especially useful when you expect your SELECT query to return a huge result
            set that would be too large to load into memory:
            the for loop will only fetch one row at a time from the database.

            PARAMETERS:
            ===========
            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''
        print "\n"
        print "MasterDataManipulation fetch_all_items is called"
        print " MasterDataManipulation in fetch_all_items / category: -> \n {}".format(kwargs.get('category'))
        logger.info("MasterDataManipulation fetch_all_items is called")
        logger.info(" MasterDataManipulation in fetch_all_items / category: -> \n {}".format(kwargs.get('category')))

        try:

            for record in self._dict_store_session_query[kwargs.get('category')]():
                try:
                    if kwargs.get('category') == 'person_gender':
                        yield record.id, record.gender
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_nationality':
                        yield record.id, record.nationality
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_salutation':
                        yield record.id, record.salutation
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_title':
                        yield record.id, record.title
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_hair_color':
                        yield record.id, record.hair_color
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_eye_color':
                        yield record.id, record.eye_color
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_religion':
                        yield record.id, record.religion
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_relationship_status':
                        yield record.id, record.relationship_status
                        #self._session.commit()
                    elif kwargs.get('category') == 'person_relationship':
                        yield record.id, record.relationship
                        #self._session.commit()
                    elif kwargs.get('category') == 'general_country':
                        yield record.id, record.country
                        #self._session.commit()
                    elif kwargs.get('category') == 'general_language':
                        yield record.id, record.language
                        #self._session.commit()
                    elif kwargs.get('category') == 'general_media_type':
                        yield record.id, record.media_type
                    elif kwargs.get('category') == 'general_manufacturer':
                        yield record.id, record.manufacturer
                    elif kwargs.get('category') == 'general_region':
                        yield record.id, record.region
                    elif kwargs.get('category') == 'general_age_release':
                        yield record.id, record.age_release
                        #self._session.commit()
                    elif kwargs.get('category') == 'general_award':
                        yield record.id, record.award
                    elif kwargs.get('category') == 'general_packaging':
                        yield record.id, record.packaging
                    elif kwargs.get('category') == 'person_profile':
                        yield record.id, record.name_normally_used, record.last_name, record.photo_file_extension
                    elif kwargs.get('category') == 'film_genre':
                        yield record.id, record.genre
                    elif kwargs.get('category') == 'film_production_company':
                        yield record.id, record.production_company
                    elif kwargs.get('category') == 'film_rentals':
                        yield record.id, record.rentals
                    elif kwargs.get('category') == 'film_distribution':
                        yield record.id, record.distribution
                    elif kwargs.get('category') == 'film_studio':
                        yield record.id, record.studio
                    elif kwargs.get('category') == 'film_resolution':
                        yield record.id, record.resolution, record.presenting_format
                    elif kwargs.get('category') == 'film_regional_code':
                        yield record.id, record.regional_code, record.region
                    elif kwargs.get('category') == 'book_genre':
                        yield record.id, record.genre
                    elif kwargs.get('category') == 'book_publisher':
                        yield record.id, record.publisher
                    elif kwargs.get('category') == 'book_binding':
                        yield record.id, record.binding
                    elif kwargs.get('category') == 'coin_alloy':
                        yield record.id, record.alloy
                    elif kwargs.get('category') == 'coin_currency':
                        yield record.id, record.currency
                    elif kwargs.get('category') == 'coin_type':
                        yield record.id, record.typeof
                    elif kwargs.get('category') == 'coin_manufacturing_procedure':
                        yield record.id, record.manufacturing_procedure, record.shortcut
                    elif kwargs.get('category') == 'coin_degree_preservation':
                        yield record.id, record.degree_preservation, record.shortcut
                    elif kwargs.get('category') == 'coin_mint':
                        yield record.id, record.mint, record.mint_mark
                    elif kwargs.get('category') == 'music_genre':
                        yield record.id, record.genre
                    elif kwargs.get('category') == 'music_studio':
                        yield record.id, record.studio
                    elif kwargs.get('category') == 'music_spars_code':
                        yield record.id, record.spars_code
                    elif kwargs.get('category') == 'music_publisher':
                        yield record.id, record.publisher
                    elif kwargs.get('category') == 'music_style':
                        yield record.id, record.style
                    elif kwargs.get('category') == 'music_label':
                        yield record.id, record.label
                    elif kwargs.get('category') == 'music_distribution':
                        yield record.id, record.distribution
                    elif kwargs.get('category') == 'film_aspect_ratio':
                        yield record.id, record.aspect_ratio, record.format
                    elif kwargs.get('category') == 'film_dimension':
                        yield record.id, record.dimension
                    elif kwargs.get('category') == 'film_edition':
                        yield record.id, record.edition
                    elif kwargs.get('category') == 'film_version':
                        yield record.id, record.version
                    elif kwargs.get('category') == 'film_videonorm':
                        yield record.id, record.norm
                    elif kwargs.get('category') == 'film_audioformat':
                        yield record.id, record.format
                    elif kwargs.get('category') == 'film_audio_channel':
                        yield record.id, record.channel
                    elif kwargs.get('category') == 'film_colour_format':
                        yield record.id, record.format
                    elif kwargs.get('category') == 'film_videoformat':
                        yield record.id, record.format
                    elif kwargs.get('category') == 'film_soundsystem':
                        yield record.id, record.system
                    elif kwargs.get('category') == 'videogame_genre':
                        yield record.id, record.genre

                except ValueError as ValErr:

                  raise ValueError(format_exc(exc_info()))

                except SQLAlchemyError as err:
                    # Before we rise an exception, we call close()
                    # on the current session which, has the effect of
                    # releasing any connection/transactional resources
                    # owned by the Session first, then discarding the
                    # Session itself. After that, we empty the attribute,
                    # in next step we save the class of sqlalchemy.orm.session.Session
                    # in the attribute.

                    self._session.close()
                    self._session = None
                    #self._session = ScopedSession()

                    raise SQLAlchemyError(format_exc(exc_info()))

                except UnboundExecutionError as err:

                    self._session.close()
                    self._session = None
                    #self._session = ScopedSession()

                    raise SQLAlchemyError(format_exc(exc_info()))

        except AttributeError as err:

            desired_trace = format_exc(exc_info())

        return

    def query_by_id(self,
                    **kwargs):
        '''
            NOTICE:
            =======
            In this method we want to get all rows from a table. This is a generator,
            it is especially useful when you expect your SELECT query to return a huge result
            set that would be too large to load into memory:
            the for loop will only fetch one row at a time from the database.

            PARAMETERS:
            ===========
            :id         -

            :category   -   The given category is the key to the
                            subsequent dictionary (dict_store_session_query).
                            There the query-objects are stored with the
                            respective models.

            :return     -   Nothing is returned. The statement 'return'
                            terminates a function. That makes sure that the
                            function is definitely finished.
        '''


        '''
            Select the object model you are trying to get and return it.
        '''

        dict_store_session_query = {'film_genre':                   lambda: self._session.query(FilmGenre).filter(
                                                                            FilmGenre.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_production_company':      lambda: self._session.query(FilmProductionCompany).filter(
                                                                            FilmProductionCompany.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_rentals':                 lambda: self._session.query(FilmRentals).filter(
                                                                            FilmRentals.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_distribution':            lambda: self._session.query(FilmDistribution).filter(
                                                                            FilmDistribution.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_studio':                  lambda: self._session.query(FilmStudio).filter(
                                                                            FilmStudio.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_resolution':              lambda: self._session.query(FilmResolution).filter(
                                                                            FilmResolution.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_regional_code':           lambda: self._session.query(FilmRegionalCode).filter(
                                                                            FilmRegionalCode.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'book_genre':                   lambda: self._session.query(BookGenre).filter(
                                                                            BookGenre.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'book_publisher':               lambda: self._session.query(BookPublisher).filter(
                                                                            BookPublisher.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'book_binding':                 lambda: self._session.query(BookBinding).filter(
                                                                            BookBinding.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_alloy':                   lambda: self._session.query(CoinAlloy).filter(
                                                                            CoinAlloy.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_currency':                lambda: self._session.query(CoinCurrency).filter(
                                                                            CoinCurrency.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_type':                    lambda: self._session.query(CoinType).filter(
                                                                            CoinType.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_manufacturing_procedure': lambda: self._session.query(CoinManufacturingProcedure).filter(
                                                                            CoinManufacturingProcedure.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_degree_preservation':     lambda: self._session.query(CoinDegreePreservation).filter(
                                                                            CoinDegreePreservation.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'coin_mint':                    lambda: self._session.query(CoinMint).filter(
                                                                            CoinMint.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_genre':                  lambda: self._session.query(MusicGenre).filter(
                                                                            MusicGenre.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_studio':                 lambda: self._session.query(MusicStudio).filter(
                                                                            MusicStudio.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_spars_code':             lambda: self._session.query(MusicSPARSCode).filter(
                                                                            MusicSPARSCode.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_publisher':              lambda: self._session.query(MusicPublisher).filter(
                                                                            MusicPublisher.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_style':                  lambda: self._session.query(MusicStyle).filter(
                                                                            MusicStyle.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_label':                  lambda: self._session.query(MusicLabel).filter(
                                                                            MusicLabel.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'music_distribution':           lambda: self._session.query(MusicDistribution).filter(
                                                                            MusicDistribution.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_country':              lambda: self._session.query(GeneralCountry).filter(
                                                                            GeneralCountry.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_media_type':           lambda: self._session.query(GeneralMediaType).filter(
                                                                            GeneralMediaType.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_manufacturer':         lambda: self._session.query(GeneralManufacturer).filter(
                                                                            GeneralManufacturer.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_language':             lambda: self._session.query(GeneralLanguage).filter(
                                                                            GeneralLanguage.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_region':               lambda: self._session.query(GeneralRegion).filter(
                                                                            GeneralRegion.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_packaging':            lambda: self._session.query(GeneralPackaging).filter(
                                                                            GeneralPackaging.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_award':                lambda: self._session.query(GeneralAward).filter(
                                                                            GeneralAward.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'general_age_release':          lambda: self._session.query(GeneralAgeRelease).filter(
                                                                            GeneralAgeRelease.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_gender':                lambda: self._session.query(PersonGender).filter(
                                                                            PersonGender.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_nationality':           lambda: self._session.query(PersonNationality).filter(
                                                                            PersonNationality.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_salutation':            lambda: self._session.query(PersonSalutation).filter(
                                                                            PersonSalutation.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_title':                 lambda: self._session.query(PersonTitle).filter(
                                                                            PersonTitle.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_hair_color':            lambda: self._session.query(PersonHairColor).filter(
                                                                            PersonHairColor.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_eye_color':             lambda: self._session.query(PersonEyeColor).filter(
                                                                            PersonEyeColor.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_religion':              lambda: self._session.query(PersonReligion).filter(
                                                                            PersonReligion.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_relationship_status':   lambda: self._session.query(PersonRelationsshipStatus).filter(
                                                                            PersonRelationsshipStatus.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_relationship':          lambda: self._session.query(PersonRelationsship).filter(
                                                                            PersonRelationsship.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'person_profile':              lambda: self._session.query(Person).filter(
                                                                            Person.id=='{}'.format(int(kwargs.get('id')))).one(),
                                    'film_aspect_ratio':            lambda: self._session.query(FilmAspectRatio).filter(
                                                                            FilmAspectRatio.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_dimension':               lambda: self._session.query(FilmDimension).filter(
                                                                            FilmDimension.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_edition':                 lambda: self._session.query(FilmEdition).filter(
                                                                            FilmEdition.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_version':                 lambda: self._session.query(FilmVersion).filter(
                                                                            FilmVersion.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_videonorm':               lambda: self._session.query(FilmVideonorm).filter(
                                                                            FilmVideonorm.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_audioformat':             lambda: self._session.query(FilmAudioformat).filter(
                                                                            FilmAudioformat.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_audio_channel':           lambda: self._session.query(FilmAudiochannel).filter(
                                                                            FilmAudiochannel.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_colour_format':           lambda: self._session.query(FilmColourformat).filter(
                                                                            FilmColourformat.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_videoformat':             lambda: self._session.query(FilmVideoformat).filter(
                                                                            FilmVideoformat.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'film_soundsystem':             lambda: self._session.query(FilmSoundsystem).filter(
                                                                            FilmSoundsystem.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    'videogame_genre':              lambda: self._session.query(VideogameGenre).filter(
                                                                            VideogameGenre.id == '{}'.format(int(kwargs.get('id')))).one(),
                                    }

        try:
            return dict_store_session_query[kwargs.get('category')]()
        except TypeError:
            pass
        except NoResultFound as NoResErr:
            if not str(NoResErr).find('No row was found for one()') == -1:
                return None
        except ValueError:
            desired_trace = format_exc(exc_info())

            logger.error(desired_trace)


    def query_by_filter(self,
                        **kwargs):

        '''
            NOTICE:
            =======
            This method filters the query by applying a criteria

            PARAMETERS:
            ===========
            :**kwargs (dict): ...

           RETURNS
           =======
           :return:           Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                              me a better feeling, that says: 'return' terminates a function definitely.
        '''

        dict_store_session_query = {'person_salutation':            lambda: self._session.query(PersonSalutation).filter(
                                                                            PersonSalutation.salutation==kwargs.get('filter_text')).one(),
                                    'general_country':              lambda: self._session.query(GeneralCountry).filter(
                                                                            GeneralCountry.country==kwargs.get('filter_text')).one(),
                                    'general_media_type':           lambda: self._session.query(GeneralMediaType).filter(
                                                                            GeneralMediaType.media_type==kwargs.get('filter_text')).one(),
                                    'general_manufacturer':         lambda: self._session.query(GeneralManufacturer).filter(
                                                                            GeneralManufacturer.manufacturer==kwargs.get('filter_text')).one(),
                                    'general_language':             lambda: self._session.query(GeneralLanguage).filter(
                                                                            GeneralLanguage.language==kwargs.get('filter_text')).one(),
                                    'general_region':               lambda: self._session.query(GeneralRegion).filter(
                                                                            GeneralRegion.region==kwargs.get('filter_text')).one(),
                                    'general_packaging':            lambda: self._session.query(GeneralPackaging).filter(
                                                                            GeneralPackaging.packaging==kwargs.get('filter_text')).one(),
                                    'general_award':                lambda: self._session.query(GeneralAward).filter(
                                                                            GeneralAward.award==kwargs.get('filter_text')).one(),
                                    'general_age_release':          lambda: self._session.query(GeneralAgeRelease).filter(
                                                                            GeneralAgeRelease.age_release==kwargs.get('filter_text')).one(),
                                    'person_gender':                lambda: self._session.query(PersonGender).filter(
                                                                            PersonGender.gender==kwargs.get('filter_text')).one(),
                                    'person_nationality':           lambda: self._session.query(PersonNationality).filter(
                                                                            PersonNationality.nationality==kwargs.get('filter_text')).one(),
                                    'film_genre':                   lambda: self._session.query(FilmGenre).filter(
                                                                            FilmGenre.genre==kwargs.get('filter_text')).one(),
                                    'film_production_company':      lambda: self._session.query(FilmProductionCompany).filter(
                                                                            FilmProductionCompany.production_company==kwargs.get('filter_text')).one(),
                                    'film_rentals':                 lambda: self._session.query(FilmRentals).filter(
                                                                            FilmRentals.rentals==kwargs.get('filter_text')).one(),
                                    'film_distribution':            lambda: self._session.query(FilmDistribution).filter(
                                                                            FilmDistribution.distribution==kwargs.get('filter_text')).one(),
                                    'film_studio':                  lambda: self._session.query(FilmStudio).filter(
                                                                            FilmStudio.studio==kwargs.get('filter_text')).one(),
                                    'film_resolution':              lambda: self._session.query(FilmResolution).filter(
                                                                            FilmResolution.resolution==kwargs.get('filter_text')).one(),
                                    'film_regional_code':           lambda: self._session.query(FilmRegionalCode).filter(
                                                                            FilmRegionalCode.regional_code==kwargs.get('filter_text')).one(),
                                    'film_aspect_ratio':            lambda: self._session.query(FilmAspectRatio).filter(
                                                                            FilmAspectRatio.aspect_ratio==kwargs.get('filter_text')).one(),
                                    'book_genre':                   lambda: self._session.query(BookGenre).filter(
                                                                            BookGenre.genre==kwargs.get('filter_text')).one(),
                                    'book_publisher':               lambda: self._session.query(BookPublisher).filter(
                                                                            BookPublisher.publisher==kwargs.get('filter_text')).one(),
                                    'book_binding':                 lambda: self._session.query(BookBinding).filter(
                                                                            BookBinding.binding==kwargs.get('filter_text')).one(),
                                    'coin_alloy':                   lambda: self._session.query(CoinAlloy).filter(
                                                                            CoinAlloy.alloy==kwargs.get('filter_text')).one(),
                                    'coin_currency':                lambda: self._session.query(CoinCurrency).filter(
                                                                            CoinCurrency.currency==kwargs.get('filter_text')).one(),
                                    'coin_type':                    lambda: self._session.query(CoinType).filter(
                                                                            CoinType.typeof==kwargs.get('filter_text')).one(),
                                    'coin_manufacturing_procedure': lambda: self._session.query(CoinManufacturingProcedure).filter(
                                                                            CoinManufacturingProcedure.manufacturing_procedure==kwargs.get('filter_text')).one(),
                                    'coin_mint':                    lambda: self._session.query(CoinMint).filter(
                                                                            CoinMint.mint==kwargs.get('filter_text')).one(),
                                    'coin_degree_preservation':     lambda: self._session.query(CoinDegreePreservation).filter(
                                                                            CoinDegreePreservation.degree_preservation==kwargs.get('filter_text')).one(),
                                    'music_genre':                  lambda: self._session.query(MusicGenre).filter(
                                                                            MusicGenre.genre==kwargs.get('filter_text')).one(),
                                    'music_studio':                 lambda: self._session.query(MusicStudio).filter(
                                                                            MusicStudio.studio==kwargs.get('filter_text')).one(),
                                    'music_spars_code':             lambda: self._session.query(MusicSPARSCode).filter(
                                                                            MusicSPARSCode.spars_code==kwargs.get('filter_text')).one(),
                                    'music_publisher':              lambda: self._session.query(MusicPublisher).filter(
                                                                            MusicPublisher.publisher==kwargs.get('filter_text')).one(),
                                    'music_style':                  lambda: self._session.query(MusicStyle).filter(
                                                                            MusicStyle.style==kwargs.get('filter_text')).one(),
                                    'music_label':                  lambda: self._session.query(MusicLabel).filter(
                                                                            MusicLabel.label==kwargs.get('filter_text')).one(),
                                    'music_distribution':           lambda: self._session.query(MusicDistribution).filter(
                                                                            MusicDistribution.distribution==kwargs.get('filter_text')).one(),
                                    'person_profile':              lambda: self._session.query(Person).filter(
                                                                            Person.country==kwargs.get('filter_text')).one(),
                                    'person_title':                 lambda: self._session.query(PersonTitle).filter(
                                                                            PersonTitle.title==kwargs.get('filter_text')).one(),
                                    'person_hair_color':            lambda: self._session.query(PersonHairColor).filter(
                                                                            PersonHairColor.hair_color==kwargs.get('filter_text')).one(),
                                    'person_eye_color':             lambda: self._session.query(PersonEyeColor).filter(
                                                                            PersonEyeColor.eye_color==kwargs.get('filter_text')).one(),
                                    'person_religion':              lambda: self._session.query(PersonReligion).filter(
                                                                            PersonReligion.religion==kwargs.get('filter_text')).one(),
                                    'person_relationship_status':   lambda: self._session.query(PersonRelationsshipStatus).filter(
                                                                            PersonRelationsshipStatus.relationship_status==kwargs.get('filter_text')).one(),
                                    'person_relationship':          lambda: self._session.query(PersonRelationsship).filter(
                                                                            PersonRelationsship.relationship==kwargs.get('filter_text')).one(),
                                    'film_dimension':               lambda: self._session.query(FilmDimension).filter(
                                                                            FilmDimension.dimension==kwargs.get('filter_text')).one(),
                                    'film_edition':                 lambda: self._session.query(FilmEdition).filter(
                                                                            FilmEdition.edition==kwargs.get('filter_text')).one(),
                                    'film_version':                 lambda: self._session.query(FilmVersion).filter(
                                                                            FilmVersion.version==kwargs.get('filter_text')).one(),
                                    'film_videonorm':               lambda: self._session.query(FilmVideonorm).filter(
                                                                            FilmVideonorm.norm==kwargs.get('filter_text')).one(),
                                    'film_audioformat':             lambda: self._session.query(FilmAudioformat).filter(
                                                                            FilmAudioformat.format==kwargs.get('filter_text')).one(),
                                    'film_audio_channel':           lambda: self._session.query(FilmAudiochannel).filter(
                                                                            FilmAudiochannel.channel==kwargs.get('filter_text')).one(),
                                    'film_colour_format':           lambda: self._session.query(FilmColourformat).filter(
                                                                            FilmColourformat.format==kwargs.get('filter_text')).one(),
                                    'film_videoformat':             lambda: self._session.query(FilmVideoformat).filter(
                                                                            FilmVideoformat.format==kwargs.get('filter_text')).one(),
                                    'film_soundsystem':             lambda: self._session.query(FilmSoundsystem).filter(
                                                                            FilmSoundsystem.system==kwargs.get('filter_text')).one(),
                                    'place':                        lambda: self._session.query(GeneralPlace).filter(
                                                                            GeneralPlace.place==kwargs.get('filter_text')).one(),
                                    'videogame_genre':              lambda: self._session.query(VideogameGenre).filter(
                                                                            VideogameGenre.genre==kwargs.get('filter_text')).one(),
                                    'None':                         None,

                                    }
        try:
            return dict_store_session_query[kwargs.get('category')]()
        except TypeError:
            pass
        except NoResultFound as NoResErr:
            if not str(NoResErr).find('No row was found for one()') == -1:
                return None


if __name__ == "__main__":
    from config import ConfigurationSaverDict
    from manage_db_connection import SessionScope
    from sqlalchemy.exc import SQLAlchemyError
    from managed_engine import ManagedEngine
    import time
    import requests

    configuration_saver_dict = ConfigurationSaverDict()

    manipulation = MasterDataManipulation()

    dbms = raw_input('Enter database type: ')
    dbdriver = raw_input('Enter database driver: ')
    dbuser = raw_input('Enter user name: ')
    dbuser_pwd = raw_input('Enter user password: ')
    db_server_host = raw_input('Enter server host: ')
    dbport = raw_input('Enter port: ')
    db_name = raw_input('Enter database name: ')
    admin_database = None

    url = "http://www.ling.uni-potsdam.de/~kolb/DE-Ortsnamen.txt"

    try:

        response = requests.get(url)

        with ManagedEngine(dbms = dbms,
                           dbuser = unicode(dbuser),
                           dbuser_pwd = unicode(dbuser_pwd),
                           db_server_host = unicode(db_server_host),
                           dbport = unicode(dbport),
                           db_name = unicode(db_name),
                           echo_verbose = False,
                           configuration_saver_dict = configuration_saver_dict,
                           admin_database = admin_database) as engine:


            # We should bind engine to your model.
            ScopedSession.configure(bind = engine)


            try:

                for line in response:
                    manipulation.add_all(line)
                manipulation._session.commit()

            except SQLAlchemyError: pass


    except SQLAlchemyError as err:

        server_said = "The server said: {server_said}".format(server_said = str(err[0]))

        print "Can't connect to database server", server_said



