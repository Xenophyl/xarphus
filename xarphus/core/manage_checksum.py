#import hashlib
from hashlib import sha512

def create_checksum(file_name, algorithm, chunk_size = 4096):
        '''
           SUMMARY
           =======
           This method is used to create sha checksum.

           PARAMETERS
           ==========
           :file_name (str):            We need a path to the file.
           :algorithm (_hashlib.HASH):  Is an algorithm from hashlib.
           :chunk_size (int):           We need a block size, because sometimes we won't be able to fit
                                        the whole file in memory. In that case, we'll have to read chunks of 4096 bytes sequentially.

           RETURNS
           =======
           :return:                     Returns value of hexdigest
        '''
        with open(file_name, "rb") as file_object:
          for chunk in iter(lambda: file_object.read(chunk_size), b""):
            algorithm.update(chunk)
        return algorithm.hexdigest()

if __name__ == "__main__":
  file_name = raw_input("Enter the path to the file for checking sum: ")
  print "\n Your Check sum is:\n", create_checksum(file_name = file_name, algorithm = sha512())
