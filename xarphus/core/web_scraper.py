#!/usr/bin/env python
#-*- coding:utf-8 -*-

from bs4 import BeautifulSoup

try:

    from xarphus.core.manage_partition import partition_text
    
except ImportError:

    from manage_partition import partition_text

def scrape_no_name_taple(html_content = None):
    '''
        NOTE:
        =====
        This method is used to scraping the web automatically. It extracts data. 

        DETAILS:
        ========
        There is a simple PHP-page, which presents special records from the SQLite in a simple table.
        Depending on the records, there are also different numbers of columns. 
        If there are two cells (for example: ID and RECORD) we are talking about simple table. That
        means, we return a simple list. 
        BUT as soon as there are more columns than two, we have to put the records in a tuple. 
        In this case we have to return a list with tuples.
        
        PARAMETERS:
        ===========
        :return     -   It returns s list.

    '''
    online_items_list = []
    count = 0

    soup = BeautifulSoup(html_content, 'html.parser')

    for row in soup.findAll("tr"):
        cells = row.findAll("td")

        #print "cells", len(cells)

        if len(cells) == 2:
            id_item = partition_text(text = str(cells[0]), left_separator = '<td>', right_separator = '</td>')
            second_item  = partition_text(text = str(cells[1]), left_separator = '<td>', right_separator = '</td>')

            new_online_item = second_item.decode("utf8", errors="replace").strip()

            online_items_list.append(new_online_item)

        if len(cells) == 3:
            id_item = partition_text(text = str(cells[0]), left_separator = '<td>', right_separator = '</td>')
            second_item  = partition_text(text = str(cells[1]), left_separator = '<td>', right_separator = '</td>')
            third_item  = partition_text(text = str(cells[2]), left_separator = '<td>', right_separator = '</td>')

            new_online_second_item = second_item.decode("utf8", errors="replace").strip()
            new_online_third_item = third_item.decode("utf8", errors="replace").strip()

            online_items_list.append((new_online_second_item, new_online_third_item))

    return online_items_list