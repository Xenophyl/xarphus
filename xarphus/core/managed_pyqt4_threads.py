#!/usr/bin/env python
#-*- coding:utf-8 -*-

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from PyQt4.QtCore import QObject, pyqtSignal, QRegExp, Qt, QString
from PyQt4.QtGui import QLineEdit

class TaskFilterUpdate(QObject):
    ''' Defines signals '''
    result_filter_update_signal = pyqtSignal(str)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()

    create_generally_information_msg_signal = pyqtSignal(str, str, str, bool, bool, object, object, bool)

    close_wait_message_signal = pyqtSignal()

    def __init__(self,
                parent = None,
                **kwargs):
        QObject.__init__(self, parent)

        self._kwargs = kwargs

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well we know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def start_thread(self):
        '''
           SUMMARY
           =======
           This method updates the filter of the given model.

           RETURNS
           =======
           :return:     Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                        me a better feeling, that says: 'return' terminates a function definitely.
        '''
        #print ""
        #print "FilterUpdate in start_thread / self._kwargs.get('self._kwargs.get('line_edit_widget')').objectName(): --> \n {}".format(self._kwargs.get('line_edit_widget').objectName())
        #print "FilterUpdate in start_thread / self._kwargs.get('self._kwargs.get('line_edit_widget')').text(): --> \n {}", self._kwargs.get('line_edit_widget').text()
        #print "FilterUpdate in start_thread / self._kwargs.get('certain_proxy_model').objectName(): --> \n {}".format(self._kwargs.get('certain_proxy_model').objectName())
        #print "FilterUpdate in start_thread / self._kwargs.get('certain_proxy_model').rowCount() before search: --> \n {}".format(self._kwargs.get('certain_proxy_model').rowCount())

        result_count = 0


        print "self._kwargs.get('certain_proxy_model').rowCount()", self._kwargs.get('certain_proxy_model').rowCount()
        self._kwargs.get('certain_proxy_model').setFilterMaximumColumns(column = self._kwargs.get('column'))

        if self._kwargs.get('line_edit_widget') is None:
            self._kwargs['line_edit_widget'] = QLineEdit()

            result_count = ""

        syntax = QRegExp.RegExp
        caseSensitivity = Qt.CaseInsensitive
        regExp = QRegExp(unicode(self._kwargs.get('line_edit_widget').text()), caseSensitivity, syntax)
        self._kwargs.get('certain_proxy_model').setFilterRegExp(regExp)

        # No results match for your search.
        if self._kwargs.get('certain_proxy_model').rowCount() == 0:

            syntax = QRegExp.RegExp
            caseSensitivity = Qt.CaseInsensitive
            regExp = QRegExp(QString(""), caseSensitivity, syntax)
            self._kwargs.get('certain_proxy_model').setFilterRegExp(regExp)

            result_count = str("0")

            self.create_generally_information_msg_signal.emit(
              self.tr("No matching results"), # title
              self.tr('No results were found for this search'), # msg
              "", # detail_msg
               False, # yes_no_button
               True, # only_yes_button
               self._kwargs.get('line_edit_widget'), # only_yes_button
               self._kwargs.get('image_path_collection').info_32x32_default, # icon
               True # set_flag
              )

        else:

            result_count = str(self._kwargs.get('certain_proxy_model').rowCount())

        #self.close_wait_message_signal.emit()

        self.result_filter_update_signal.emit(result_count)

        self.stop_task_thread()

        return

    def stop_task_thread(self):
        '''
            NOTICE:
            =======
            This method is used to stop the still running generator by just stopping the timer.
            No Exception is raised. Its a simple stopper.

            The user decided to break or interrupt this loop - for instance when
            the user just closes the window while the generator is still running.

            PARAMETERS:
            ===========
            :return     -       Nothing is returned. The statement 'return'
                                terminates a function. That makes sure that the
                                function is definitely finished.
        '''
        logger.info('Stop the running Thread')

        self.quit_thread_signal.emit()

class TaskProxyModel(QObject):
    '''
        NOTICE:
        =======
        Updating the proxyModel. Set the given sourceModel to be processed by the proxy model.

        DETAILS:
        ========
        The main thread freezes when it is setting the model of proxy model,
        so we have to create a new thread for this task to provide the
        responsiveness of the window.
     '''
    finish_progress = pyqtSignal()
    fire_label = pyqtSignal(int)
    quit_thread_signal = pyqtSignal()
    wait_thread_signal = pyqtSignal()

    Set_model_on_treeview_signal = pyqtSignal(object, object)

    close_wait_message_signal = pyqtSignal()

    def __init__(self,
                 parent = None,
                 **kwargs):
        QObject.__init__(self, parent)

        logger.info("The instance of the class of {}() is created successfully".format(self.get_class_name()))

        self._kwargs = kwargs

        print ""
        print "TaskProxyModel / self._kwargs: -> \n {}".format(self._kwargs)

    def get_class_name(self):
        '''
           SUMMARY
           =======
           This method is used to determine the names of the classes automatically.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''

        logger.info("Return the name of the current class")

        return self.__class__.__name__

    def start_work(self):

        logger.info("init_object is called")

        if not self._kwargs.get('force_fetch_all'):

          #self._kwargs.get('general_proxy_model').reset_proxy_model()
          #self._kwargs.get('general_proxy_model').create_proxy_model()
          self._kwargs.get('general_proxy_model').create_source_model(
            certain_standard_item_model = self._kwargs.get('certain_standard_item_model'),
            certain_proxy_model = self._kwargs.get('certain_proxy_model'))

        #self._kwargs.get('general_proxy_model').set_source_model(model = self._kwargs.get('certain_standard_item_model'))

        self._kwargs.get('general_proxy_model').set_source_model(**self._kwargs)

        self._kwargs.get('general_proxy_model').refresh_proxy_model(
         certain_proxy_model = self._kwargs.get('certain_proxy_model'),
         category = self._kwargs.get('category'))


        if not self._kwargs.get('widget_tree_view') is None:
          self.Set_model_on_treeview_signal.emit(
            self._kwargs.get('widget_tree_view'),
            self._kwargs.get('certain_proxy_model'))

        self.close_wait_message_signal.emit()

        self.finish_task_thread()

    def finish_task_thread(self):
        '''
           SUMMARY
           =======
           This method is used to stop the still running object by just stopping the timer.
           No Exception is raised. Its a simple stopper.

           EXTENTED DISCRIPTION
           ====================
           The user decided to break or interrupt this loop - for instance when
           the user just closes the window while the generator is still running.

           RETURNS
           =======
           :return: Nothing is returned. Well I know a simple 'return' isn't necessary, but this statement gives
                    me a better feeling, that says: 'return' terminates a function definitely.
        '''
        self.quit_thread_signal.emit()
        self.wait_thread_signal.emit()
