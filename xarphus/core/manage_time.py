import time

def get_time_now():
    my_time = time.strftime("%Y-%m-%d %H:%M:%S")
    return my_time.replace(":", "_")
