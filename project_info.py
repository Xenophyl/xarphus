#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
tmdbsimple
~~~~~~~~~~
*tmdbsimple* is a wrapper, written in Python, for The Movie Database (TMDb) 
API v3.  By calling the functions available in *tmdbsimple* you can simplify 
your code and easily access a vast amount of movie, tv, and cast data.  To find 
out more about The Movie Database API, check out the overview page 
http://www.themoviedb.org/documentation/api and documentation page 
http://docs.themoviedb.apiary.io.
https://www.themoviedb.org/documentation/api/status-codes
:copyright: (c) 2013-2014 by Celia Oakley.
:license: GPLv3, see LICENSE for more details
"""

__title__ = 'Xarphus'
__version__ = '1.0.0'
__author__ = 'Sophhus'

__copyright__ = 'Copyright (c) 2015-2016 ' + __author__
__license__ = 'GPLv3'

__author__ = "Rob Knight, Gavin Huttley, and Peter Maxwell"
__copyright__ = "Copyright 2015, The Cogent Project"

__credits__ = ["Rob Knight", "Peter Maxwell", "Gavin Huttley",
                    "Matthew Wakefield"]
                    
__maintainer__ = "Rob Knight"
__email__ = "rob@spot.colorado.edu"
__status__ = "Production"

        # self.dict_info = {
        #               "product_name": "Xarphus",
        #               "exe_name": "Xarphus",
        #               "product_description": "Collection Manager",
        #               "product_by": "by",
        #               "product_author": "Sophus",
        #               "product_team": "Xarphus-Team",
        #               "product_copyright": "<br>Copyright",
        #               "product_copyright_characters": " © ",
        #               "product_year_from": "2015",
        #               "product_year_till": "2015",
        #               "product_version": "0.0.1",
        #               "product_GUI_Version": "Qt4",
        #               "product_built": "Pre-Alpha",
        #               "product_faq": "http://docs.python.org/library/webbrowser.html",
        #               "product_site": "http://www.web.de",
        #               "product_release_note": "www.web.de",
        #               "db_name": "xarphus",
        #               "xarphus_installer":"xarphus_installer",
        # }
