#!/usr/bin/env python
#-*- coding:utf-8 -*-

from PyQt4.QtGui import QDialog, QApplication, QMessageBox, QStyleFactory, qApp
from PyQt4.uic import loadUi
from PyQt4.QtCore import Qt, QFile, QThread, pyqtSignal
from PyQt4 import QtCore

from installer.gui import ui_rc
from installer.core.manage_qt import flag_dialog_titlehint_customizewondowhint
from installer.core.manage_zip_file import test_zip_file, valid_zip_file
from installer.core.manage_ini_file import get_configuration, file_is_empty, DefaultINIFile, css_setting, style_factory_setting
from installer.core.manage_popen import execute_exe_file

from installer.css_template import qt_css_style_rc

from time import sleep
import sys, os
import zipfile
import argparse

class UnzipWindow(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        
        #layout = QtGui.QVBoxLayout(self)

        UI_PATH = QFile(":/ui_file/installer.ui")

        #self.custom_logger.info("Trying to open *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        UI_PATH.open(QFile.ReadOnly)

        #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is opened successfully - (" +  FILE_NAME +")")
        #self.custom_logger.info("Loading the *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")

        self.ui_installer = loadUi(UI_PATH, self)

        #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") - (" +  FILE_NAME +")")
        #self.custom_logger.info("Closing the *.ui-files ("+ str(UI_PATH.fileName()) +") is loaded successfully - (" +  FILE_NAME +")")

        UI_PATH.close()

##        parser = argparse.ArgumentParser()
##        parser.add_argument('-i','--input', help='Input file name',required=True)
##        parser.add_argument('-o','--output', help='output file name',required=True)
##
##        args = parser.parse_args()
       
        self.input_file = None
        self.output_file = None
        self.setting_ini = None
        self.exe_file = None

        self.parse_command_arguments()        

        #self.custom_logger.info("The *.ui-files ("+ str(UI_PATH.fileName()) +") is closed successfully - (" +  FILE_NAME +")")

        self.unzip_task = UnzipTask(self.input_file, self.output_file)
        self.unzip_task.taskFinished.connect(self.pope_exe_program)
        self.unzip_task.notifyProgress.connect(self.onProgress)

        self.load_stylesheet()
        self.load_style_factory()
        self.init_ui()
        self.test_valid_zip_file(self.input_file)

    def init_ui(self):
        self.ui_installer.setWindowTitle('Install update')
        flags = flag_dialog_titlehint_customizewondowhint()
        self.ui_installer.setWindowFlags(flags)

    def parse_command_arguments(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i','--input', help='Input file name',required=True)
        parser.add_argument('-o','--output', help='output file name',required=True)
        parser.add_argument('-s','--setting_file', help='output file name',required=False)
        parser.add_argument('-e','--exe_file', help='exe file name',required=True)

        args = parser.parse_args()
        self.input_file = args.input
        self.output_file = args.output
        self.setting_ini = args.setting_file
        self.exe_file = args.exe_file

        return

    def pope_exe_program(self, finish_text):

        executed_file = unicode(self.exe_file)

        

        QMessageBox.information(self, 'Done', str(finish_text))
##
##        try:
        execute_exe_file(executed_file)
        #sys.exit()
        self.close()
##        except CalledProcessError as err:            
##            self.create_generally_crritcal_msg(unicode(self.tr("An internal critical error has occurred.") +
##                                                       self.tr("\n\nDetails: \n") +
##                                                       err.output +
##                                                       self.tr("\nReturncode: ") +
##                                                       str(err.returncode)+
##                                                       "\n\nPlease send us your log file so we can assess the error and provide you with the best solution."))



    def test_valid_zip_file(self, file_descriptor):
        try:
            ret = valid_zip_file(file_descriptor)
            if ret:
                '''
                Yes the zip file is a valid file.
                Lets us take a test of the zipfile.
                '''
                self.test_zip_file(file_descriptor)
                
            
        except (IOError, OSError) as err:
            print "Could not read file:", file_descriptor, "\n The File does not appear to exist."
            print "Detail: ", err

        except zipfile.BadZipfile as bad_file:
            print "test_valid_zip_file", bad_file

    def test_zip_file(self, file_descriptor):
        '''
        Take a path to a zipfile and checks their CRC's and file headers.
        In this case all files in the zip file will read. That means its
        checks the integrity of the file.
        '''
        try:
            ret = test_zip_file(file_descriptor)
            if ret:
                self.onStart()
            
        except (IOError, OSError) as err:
            print "Could not read file:", file_descriptor, "\n The File does not appear to exist."
            print "Detail: ", err

        except zipfile.BadZipfile as bad_file:
            print "test_zip_file", bad_file

    def load_stylesheet(self):
        #self.custom_logger.info("-------------------------------------- START FUNCTION (" + inspect.stack()[0][
        #    3] + ") - (" + FILE_NAME + ")")
        #self.custom_logger.info("Calling the function (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")
        
        # First: Get the current css stylesheet and setup stylesheet

        result_css_style = css_setting(self.setting_ini)
        
        css_file_extension = ".qss"
        css_path = ":/" + result_css_style + "/" + result_css_style + css_file_extension
        
        fd = QtCore.QFile(css_path)
        fd.open(QtCore.QIODevice.ReadOnly | QtCore.QFile.Text)
        style = QtCore.QTextStream(fd).readAll()
        fd.close()
        self.setStyleSheet(style)
        #self.setStyle(QStyleFactory.create(self.save_configuration.dict_set_general_settings["GUIStyle"]))

        #self.custom_logger.info("The function (" + inspect.stack()[0][3] + ") is called successfully - (" + FILE_NAME + ")")
        #self.custom_logger.info(
        #    "-------------------------------------- END FUNCTION (" + inspect.stack()[0][3] + ") - (" + FILE_NAME + ")")
        
        return

    def load_style_factory(self):

        result_style_factory = style_factory_setting(self.setting_ini)

        qApp.setStyle(QStyleFactory.create(result_style_factory))

        return

    def onStart(self):
        self.ui_installer.progressBar_install.setRange(0,0)
        self.unzip_task.start()

    def onFinished(self, finish_text):
        # Stop the pulsation
        self.ui_installer.progressBar_install.setRange(0,1)
        self.ui_installer.progressBar_install.setValue(1)

        QMessageBox.information(self, 'Done', str(finish_text))

        self.close_program()

    def onProgress(self, i):
        self.ui_installer.progressBar_install.setRange(0, 100)
        self.ui_installer.progressBar_install.setValue(i)

    def keyPressEvent(self, event):
        '''
        Disable the ESC-button.
        '''
        pass

    def close_program(self):
        self.close()


class UnzipTask(QThread):
    taskFinished = pyqtSignal(object)
    notifyProgress = pyqtSignal(int)

    def __init__(self, destination_path, zip_file_name):
        QtCore.QThread.__init__(self)

        self.zip_file_name = destination_path
        self.destination_path = zip_file_name
        
        self.file_size = os.path.getsize(self.zip_file_name)
                          
    def run(self):

        try:
            with zipfile.ZipFile(self.zip_file_name) as myzipfile:
                
                uncompress_size = sum((file.file_size for file in myzipfile.infolist()))

                result = uncompress_size / (1024*9)

                chunk_size = int(result)
                        
                extracted_size = 0
                
                for my_zip_file in myzipfile.infolist():
                    extracted_size += my_zip_file.file_size
                    percentage = (float(extracted_size)/uncompress_size*100)
                    myzipfile.extract(my_zip_file, self.destination_path)
                    self.notifyProgress.emit(percentage)
                self.taskFinished.emit("Your install is done. Xarphus is started")
                    
        except zipfile.BadZipfile as bad_file:
            print bad_file

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = UnzipWindow()
    window.resize(440, 80)
    window.show()
    sys.exit(app.exec_())
