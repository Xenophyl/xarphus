from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError

class SessionScope(object):
    def __init__(self, engine):

        self.engine = engine

        # store a sessionmaker for this db connection object
        self._Session = sessionmaker(bind=self.engine)
        self.session = None

    def __enter__(self):
        self.session = self._Session()
        return self._Session()
    
    def __exit__(self, exception, exc_value, traceback):

        try:
            if exception:
                self.session.rollback()
            else:
                self.session.commit()
        finally:

            self.session.close()
            self.session = None
            
class Engine(object):
    def __init__(self, dbms, dbdriver, dbuser, dbuser_pwd, db_server_host, dbport, db_name):

        self.dbms = dbms
        self.dbdriver = dbdriver
        self.dbuser = dbuser
        self.dbuser_pwd = dbuser_pwd
        self.db_server_host = db_server_host
        self.dbport = dbport
        self.db_name = db_name

        self.created_connection = None

        # engine.dispose() will close all connections of the connection pool.
        '''
            NOTICE:
            =======
            Engine will not use connection pool if you set poolclass=NullPool.
            So the connection (SQLAlchemy session) will close directly after session.close().

            If you do want the connection to be actually closed,
            that is, not pooled, disable pooling via NullPool:
        '''
        
        url = '{}+{}://{}:{}@{}:{}/{}'.format(
           self.dbms, self.dbdriver, self.dbuser, self.dbuser_pwd, self.db_server_host, self.dbport, self.db_name)

        self._Engine = create_engine(url, encoding='utf8', echo=True)

        #self.created_connection = engine.connect()

        # build tables upon first startup
        #Base.metadata.create_all(engine)

    def __enter__(self):
        return self._Engine
    
    def __exit__(self, exception, exc_value, traceback):
        '''
            Make sure the dbconnection gets closed
        '''
        self._Engine.dispose()
   
logged_in = True

def main():
            
    dbm_system = raw_input("Which DBMS? (type for e.g. mysql): ")
    dbm_driver = raw_input("Which db-driver? (type for e.g. pymysql): ")
    db_host = raw_input("Server-Host: ")
    db_user = raw_input("Database-user: ")
    db_passwd = raw_input("User-Password: ")
    db_name = raw_input("Database Name: ")
    db_port = raw_input("Port: ")

    try:
        with Engine(dbm_system, dbm_driver, db_user, \
                    db_passwd, db_host, db_port, db_name) as engine:
            
            while logged_in:
                
                with SessionScope(engine) as session:
                    # Its just for testing.
                    print session.execute("SELECT VERSION();")

    except SQLAlchemyError as err:
        print "ERROR", err[0]

    
if __name__ == '__main__':
    main()

