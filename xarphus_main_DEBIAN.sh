echo "***** Delete all *.pyc-files *****"
./run_del_pyc_files_DEBIAN.sh start
echo ""
echo "***** Converte all *.qrc-files in *.py-files *****"
./Convert_qrc_to_py_DEBIAN.sh start
echo ""
echo "***** Delete all file in log-folder *****"
./empty_log_folder_DEBIAN.sh start
echo ""
echo "***** start xarphus_main.py *****"
./xarphus_main.py start
echo "python xarphus_main.py"